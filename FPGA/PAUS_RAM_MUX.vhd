LIBRARY ieee;
USE ieee.std_logic_1164.all;

--  Entity Declaration

ENTITY PAUS_RAM_MUX IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		dualBuffer : IN STD_LOGIC;
		
		RAM_wren : IN STD_LOGIC;
		RAM1_wren : OUT STD_LOGIC;
		RAM2_wren : OUT STD_LOGIC;
		
		RAM_rdaddress : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
		Transfer_rdaddress : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
		RAM1_rdaddress : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		RAM2_rdaddress : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		
		RAM1_q : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
		RAM2_q : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
		RAM_q : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
		Transfer_q : OUT STD_LOGIC_VECTOR(23 DOWNTO 0)
		
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END PAUS_RAM_MUX;

--  Architecture Body 

ARCHITECTURE PAUS_RAM_MUX_architecture OF PAUS_RAM_MUX IS

BEGIN

RAM1_wren <= RAM_wren WHEN dualBuffer = '0' ELSE '0';
RAM2_wren <= RAM_wren WHEN dualBuffer = '1' ELSE '0';

RAM1_rdaddress <= RAM_rdaddress WHEN dualBuffer = '0' ELSE Transfer_rdaddress;
RAM2_rdaddress <= RAM_rdaddress WHEN dualBuffer = '1' ELSE Transfer_rdaddress;
	
RAM_q <= RAM1_q WHEN dualBuffer = '0' ELSE RAM2_q;
Transfer_q <= RAM1_q WHEN dualBuffer = '1' ELSE RAM2_q;

END PAUS_RAM_MUX_architecture;

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY Moteurs IS

	PORT
	(
		RSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;
		
		--Donn�es � transmettre aux moteurs
		motorInstruction : IN STD_LOGIC_VECTOR (47 downto 0);
		
		--Communication avec le contr�lleur
		motorWrite : IN STD_LOGIC;
		motorDone : OUT STD_LOGIC;
		
		--Communication avec le bloc UART
		UART_ADR : OUT STD_LOGIC_VECTOR (1 downto 0);
		UART_DATAR : IN STD_LOGIC_VECTOR (7 downto 0);
		UART_DATAT : BUFFER STD_LOGIC_VECTOR (7 downto 0);
		UART_WE : BUFFER STD_LOGIC;
		UART_STB : BUFFER STD_LOGIC;
		UART_ACK : IN STD_LOGIC;
		UART_IntTx : IN STD_LOGIC;
		UART_IntRx : IN STD_LOGIC;
		
		LED : BUFFER STD_LOGIC_VECTOR (1 downto 0); --Affichage d'une erreur
		LED2 : BUFFER STD_LOGIC_VECTOR (4 downto 0) --Test
	);
	
END Moteurs;

--  Architecture Body 
ARCHITECTURE Moteurs_architecture OF Moteurs IS

SIGNAL state : STD_LOGIC_VECTOR(4 DOWNTO 0) := "11111";
SIGNAL compteur_byte : INTEGER RANGE 0 TO 50;			--Boucle sur les bytes � transmettre et � recevoir (G�n�ralement 12 bytes)
SIGNAL compteur_timeout : INTEGER RANGE 0 TO 500000000;	--Compteur activ� lors de l'attente d'une r�ponse des moteurs

--Signaux pour informer d'une erreur
SIGNAL erreur_long : STD_LOGIC;

SIGNAL moteur_reponse : INTEGER RANGE 0 TO 5;	--Flag indiquant quel moteur est en train de r�pondre
BEGIN

PROCESS(RSTn, clk48MHz)

	VARIABLE erreur : STD_LOGIC;	

BEGIN
	IF (RSTn = '0') THEN
		state <= "11111";
		compteur_byte <= 0;
		UART_DATAT <= (OTHERS => '0');
		UART_WE <= '0';
		UART_STB <= '0';
		UART_ADR <= "00";
		motorDone <= '0';
		erreur := '0'; 
		erreur_long <= '0';
		compteur_timeout <= 0;
		LED <= "00";
		
	ELSIF (clk48MHz'EVENT AND clk48MHz = '0') THEN	
		state <= state;
		compteur_byte <= compteur_byte;
		UART_DATAT <= UART_DATAT;
		UART_WE <= UART_WE;
		UART_STB <= '0';
		UART_ADR <= "00";
		motorDone <= '0';
		compteur_timeout <= compteur_timeout;
		LED <= LED;
		
		CASE state IS
			WHEN "11111" =>						--Attente de la demande de communication avec les moteurs
				IF motorWrite = '1' THEN
					state <= "00000";
				END IF;
			
			WHEN "00000" =>						--Pr�paration � l'�criture aux moteurs
				UART_WE <= '0';		
				IF UART_IntTx = '1' THEN
					state <= "00001";
					compteur_byte <= compteur_byte + 1;
					IF compteur_byte = 0 THEN
						UART_DATAT <= motorInstruction(47 downto 40);
					ELSIF compteur_byte = 1 THEN
						UART_DATAT <= motorInstruction(39 downto 32);
					ELSIF compteur_byte = 2 THEN
						UART_DATAT <= motorInstruction(31 downto 24);
					ELSIF compteur_byte = 3 THEN
						UART_DATAT <= motorInstruction(23 downto 16);
					ELSIF compteur_byte = 4 THEN
						UART_DATAT <= motorInstruction(15 downto 8);
					ELSIF compteur_byte = 5 THEN
						UART_DATAT <= motorInstruction(7 downto 0);									
					ELSE
						UART_DATAT <= "00000000";
						compteur_byte <= 0;
						compteur_timeout <= 0;
						state <= "00010";	--Fin de l'�criture des 6 ou 12 bytes, attente de la r�ponse
					END IF;			
				END IF;
						
			WHEN "00001" =>					--�criture aux moteurs				
				UART_WE <= '1';
				UART_STB <= '1';
				IF UART_IntTx = '0' THEN
					state <= "00000";
				END IF;
				
			WHEN "00010" =>					--Attente de la r�ponse des moteurs
				UART_WE <= '0';
				IF UART_IntRx = '1' THEN			
					state <= "00011";
					compteur_byte <= compteur_byte + 1;
				END IF;	
				compteur_timeout <= compteur_timeout + 1;
				IF compteur_timeout > 96000000 THEN		--Trop long
					erreur_long <= '1';
					compteur_timeout <= 0;
				END IF;
	
			WHEN "00011" =>					--Lecture de la r�ponse des moteurs et confirmation des commandes envoy�es
				UART_WE <= '0';
				UART_STB <= '1';
				state <= "00010";
				IF compteur_byte = 1 THEN
					moteur_reponse <= TO_INTEGER(UNSIGNED(UART_DATAR));	--R�ception du num�ro du moteur qui r�pond
				ELSIF compteur_byte = 2 THEN
					--Wrong instruction response received
					IF motorInstruction(39 downto 32) /= UART_DATAR AND motorInstruction(39 downto 32) = "00000001" THEN
						erreur := '1';
					END IF;				
				--V�rification de la position actuelle apr�s le d�placement (sauf lors d'un Renumber)
				ELSIF compteur_byte = 3 THEN
					IF motorInstruction(39 downto 32) = "00000001" THEN --Home
						IF UART_DATAR /= "00000000" THEN --Busy while homing is not an error
							erreur := '1';
						END IF;
					ELSIF motorInstruction(39 downto 32) = "00010100" THEN --Move
						IF (moteur_reponse = 1 AND UART_DATAR /= motorInstruction(31 downto 24)) THEN
							erreur := '1';
						END IF;
					END IF;
				ELSIF compteur_byte = 4 THEN
					IF motorInstruction(39 downto 32) = "00000001" THEN --Home
						IF UART_DATAR /= "00000000" THEN
							erreur := '1';
						END IF;
					ELSIF motorInstruction(39 downto 32) = "00010100" THEN --Move
						IF (moteur_reponse = 1 AND UART_DATAR /= motorInstruction(23 downto 16)) THEN
							erreur := '1';
						END IF;
					END IF;
				ELSIF compteur_byte = 5 THEN
					IF motorInstruction(39 downto 32) = "00000001" THEN --Home
						IF UART_DATAR /= "00000000" THEN
							erreur := '1';
						END IF;
					ELSIF motorInstruction(39 downto 32) = "00010100" THEN --Move
						IF (moteur_reponse = 1 AND UART_DATAR /= motorInstruction(15 downto 8)) THEN
							erreur := '1';
						END IF;
					END IF;
				ELSIF compteur_byte = 6 THEN
					IF motorInstruction(39 downto 32) = "00000001" THEN --Home
						IF UART_DATAR /= "00000000" THEN
							erreur := '1';
						END IF;
					ELSIF motorInstruction(39 downto 32) = "00010100" THEN --Move
						IF (moteur_reponse = 1 AND UART_DATAR /= motorInstruction(7 downto 0)) THEN
							erreur := '1';
						END IF;
					END IF;

					compteur_byte <= 0;
					IF erreur = '1' THEN --Motor is busy or there was an error, resend the command
						erreur := '0';
						state <= "00010";
					ELSE
						state <= "00100";
						motorDone <= '1';
					END IF;

				END IF;	
				
			WHEN "00100" =>					--Done
				UART_WE <= '0';
				
				IF motorWrite = '0' THEN --Attente que le controlleur aie re�u le signal motorDone	
					state <= "11111";
				END IF;		
															
			WHEN OTHERS =>
	
		END CASE;
		
		--Affichage sur les LEDs en cas d'erreur
		IF erreur = '1' THEN --Mauvais byte re�u (Ne devrait jamais arriver)
			--erreur <= '0';
			LED(1) <= '1';
			--LED2 <= CONV_STD_LOGIC_VECTOR(compteur_byte,5);
		END IF;
		
		--Aucune r�ponse des moteurs dans un temps donn� (Une autre demande sera transmise aux moteurs)
		--Se produit lorsque les moteurs ont trop de d�placement � effectuer
		IF erreur_long = '1' THEN 
			erreur_long <= '0';
			LED(0) <= '1';
			state <= "00000";
			compteur_byte <= 0;
		END IF;
				
	END IF;
END PROCESS;

LED2 <= state;
	
END Moteurs_architecture;

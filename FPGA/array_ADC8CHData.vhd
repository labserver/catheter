LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

PACKAGE array_ADC8CHData IS
	SUBTYPE vector_ADC8CHData IS STD_LOGIC_VECTOR(15 DOWNTO 0); 
	TYPE array_ADC8CHData IS ARRAY (7 DOWNTO 0) OF vector_ADC8CHData; 
END; 
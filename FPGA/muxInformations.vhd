--This is a multiplexer for sending additionnal information to the computer.
--Input informations are multiplexed in a single vector going to the transfer module
--The transfer module send the address to select which data input to send.

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;


--  Entity Declaration

ENTITY muxInformations IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		address : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		in1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in4 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in5 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in6 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in7 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in8 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in9 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in10 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in11 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in12 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in13 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in14 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in15 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in16 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		in17 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
				
		dataout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
		
		
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END muxInformations;


--  Architecture Body 

ARCHITECTURE muxInformations_architecture OF muxInformations IS

BEGIN
	
dataout <= in1 WHEN address = "00000" ELSE
		   in2 WHEN address = "00001" ELSE	
		   in3 WHEN address = "00010" ELSE		
		   in4 WHEN address = "00011" ELSE		
		   in5 WHEN address = "00100" ELSE		
		   in6 WHEN address = "00101" ELSE		
		   in7 WHEN address = "00110" ELSE		
		   in8 WHEN address = "00111" ELSE		
		   in9 WHEN address = "01000" ELSE		
		   in10 WHEN address = "01001" ELSE		
		   in11 WHEN address = "01010" ELSE	
		   in12 WHEN address = "01011" ELSE	
		   in13 WHEN address = "01100" ELSE	
		   in14 WHEN address = "01101" ELSE			   
		   in15 WHEN address = "01110" ELSE	
		   in16 WHEN address = "01111" ELSE	
		   in17;
		   		
END muxInformations_architecture;			
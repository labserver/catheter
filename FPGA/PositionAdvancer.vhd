LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY PositionAdvancer IS
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk1MHz : IN STD_LOGIC;										

		positionAdvanced : IN UNSIGNED(11 DOWNTO 0);
        position : OUT UNSIGNED(11 DOWNTO 0); --Position is delayed to positionAdvanced.
        
        positionChanged : OUT STD_LOGIC

	);
END PositionAdvancer;

--  Architecture Body 
ARCHITECTURE PositionAdvancer_architecture OF PositionAdvancer IS

	TYPE stateMachine IS (INIT, WAIT_POS_CHANGE, DELAY_STATE);
	SIGNAL state : stateMachine;

    SIGNAL positionAdvancedNext : UNSIGNED(11 DOWNTO 0);
    SIGNAL positionAdvancedNext2 : UNSIGNED(11 DOWNTO 0);
    SIGNAL positionAdvancedPrevious : UNSIGNED(11 DOWNTO 0);
	SIGNAL counterDelay : INTEGER RANGE 0 TO 4095 := 1;
        
BEGIN
    
PROCESS(RSTn, clk1MHz)
    VARIABLE secondPositionChanged : INTEGER RANGE 0 TO 4095 := 1;
BEGIN
IF (RSTn = '0') THEN
	state <= INIT;
    position <= (OTHERS => '0');
    positionChanged <= '0';
    positionAdvancedPrevious <= (OTHERS => '0');
    positionAdvancedNext <= (OTHERS => '0');
    positionAdvancedNext2 <= (OTHERS => '0');
    secondPositionChanged := 0;


ELSIF (clk1MHz'EVENT AND clk1MHz = '1') THEN
    positionChanged <= '0';
    positionAdvancedPrevious <= positionAdvanced;
    
	CASE state IS
	
        WHEN INIT => 
            IF positionAdvanced = positionAdvancedPrevious THEN
                state <= WAIT_POS_CHANGE;
            END IF;  
            
		WHEN WAIT_POS_CHANGE => --Trigger pullback. Called the first time after startAcq and at the end of each turn
            IF positionAdvanced /= positionAdvancedPrevious THEN
                positionAdvancedNext <= positionAdvanced;
                state <= DELAY_STATE;
                secondPositionChanged := 0;
                counterDelay <= 0;
            END IF;            

        WHEN DELAY_STATE => --Delay during 160 µs
            --Check if we detect another position changed during the 160 µs (when rotating at more than 20 turns/second)
            IF positionAdvanced /= positionAdvancedPrevious THEN
                secondPositionChanged := 160-counterDelay;
                positionAdvancedNext <= positionAdvancedPrevious; --Assign previous position
                --positionAdvancedNext <= positionAdvancedNext2; --Assign previous position
            END IF;
            
            counterDelay <= counterDelay + 1;
            IF counterDelay >= 160 THEN --TODO: Should be 160, but testing
                
                positionChanged <= '1';
                
                IF secondPositionChanged > 0 THEN
                    position <= positionAdvancedNext;
                    --positionAdvancedNext <= positionAdvancedNext2; 
                    counterDelay <= secondPositionChanged; --Part of the delay has already been done.
                    secondPositionChanged := 0;
                    state <= DELAY_STATE;
                ELSE
                    position <= positionAdvancedNext;
                    state <= WAIT_POS_CHANGE;
                END IF;
            END IF;
            
												
		WHEN OTHERS =>

	END CASE;	
END IF;

END PROCESS;

END PositionAdvancer_architecture;

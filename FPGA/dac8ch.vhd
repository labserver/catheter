LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--  Entity Declaration

ENTITY DAC8CH IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		RSTn : IN STD_LOGIC;
		CLK_SPI : IN STD_LOGIC;
		CLKDAC : IN STD_LOGIC;
        CLKOUT : OUT STD_LOGIC;
		Laser1 : IN STD_LOGIC_VECTOR(11 DOWNTO 0); -- used to control the current of the lasers.
		Laser2 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
		Motor1 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
		Motor2 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
		ExtraDAC2 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
		ExtraDAC1 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
		PMT_Gain2 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
		PMT_Gain1 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);	
		SDO : OUT STD_LOGIC;
		CSn : OUT STD_LOGIC
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END DAC8CH;


--  Architecture Body

ARCHITECTURE DAC8CH_architecture OF DAC8CH IS

CONSTANT DAC_QUANTITY : INTEGER := 8;
CONSTANT BIT_QUANTITY : INTEGER := 24; --modifier aussi width de data si modif

TYPE stateMachine IS (WAIT_TRIGGER, SET_DATA, DELAY0, DELAY1, WRITE_DATA, DONE);
SIGNAL state : stateMachine := WAIT_TRIGGER;

SIGNAL CLK_EN : STD_LOGIC := '0';

SIGNAL data : STD_LOGIC_VECTOR(23 DOWNTO 0) := "001100000100000000000000";	-- C3-C0 A3-A0 D15-D0		To write on all DACs simultanously
BEGIN

CLKOUT <= CLK_SPI WHEN CLK_EN = '1' ELSE '0';
	
PROCESS(RSTn, CLK_SPI)


VARIABLE DAC_NUMBER : INTEGER RANGE 1 TO DAC_QUANTITY+1 := 1;							--8 to write on all DACs simultanously (NO)
VARIABLE BIT_NUMBER : INTEGER RANGE 0 TO BIT_QUANTITY+1 := 1;


BEGIN
	IF (RSTn = '0') THEN
		data <= "001100000100000000000000";
		state <= WAIT_TRIGGER;
		SDO <= '0';
		CSn <= '1';
		DAC_NUMBER := 1;
		BIT_NUMBER := 1;
        CLK_EN <= '0';
        
	ELSIF (CLK_SPI'EVENT AND CLK_SPI = '0') THEN
        data <= data;

		CASE state IS
			WHEN WAIT_TRIGGER =>									--Waiting for CLKDAC
				IF CLKDAC = '1' THEN
					state <= SET_DATA;
				END IF;
                CLK_EN <= '0';
                CSn <= '1';                
                
			WHEN SET_DATA =>									--Set data
				--Generate output data
				IF (DAC_NUMBER = 1) THEN
						data(15 DOWNTO 4) <= (Laser1);	
				ELSIF (DAC_NUMBER = 2) THEN
						data(15 DOWNTO 4) <= (Laser2);
				ELSIF (DAC_NUMBER = 3) THEN
						data(15 DOWNTO 4) <= (Motor1);	
				ELSIF (DAC_NUMBER = 4) THEN
						data(15 DOWNTO 4) <= (Motor2);
				ELSIF (DAC_NUMBER = 5) THEN
						data(15 DOWNTO 4) <= (ExtraDAC2);	
				ELSIF (DAC_NUMBER = 6) THEN
						data(15 DOWNTO 4) <= (ExtraDAC1);	
				ELSIF (DAC_NUMBER = 7) THEN
						data(15 DOWNTO 4) <= (PMT_Gain2);
				ELSIF (DAC_NUMBER = 8) THEN
						data(15 DOWNTO 4) <= (PMT_Gain1);
				END IF;	
				state <= DELAY0;
                CLK_EN <= '0';
                CSn <= '1';
                
            WHEN DELAY0 =>   --More delay
				CSn <= '0';								--Activate the first DAC.
				SDO <= data(BIT_QUANTITY - 1);			--Send the first bit
				BIT_NUMBER := 1;
				DAC_NUMBER := DAC_NUMBER;
				state <= DELAY1;	
                CLK_EN <= '0';
				
			WHEN DELAY1 => 									--Waiting for Enable						
				CSn <= '0';								--Activate the first DAC
				SDO <= data(BIT_QUANTITY - 1);			--Send the first bit
				BIT_NUMBER := 1;
				DAC_NUMBER := DAC_NUMBER;
				state <= WRITE_DATA;	
                CLK_EN <= '1';
									
			WHEN WRITE_DATA => 									--Write data			
				IF BIT_NUMBER >= BIT_QUANTITY THEN			--Group of 24 bits done writing
					BIT_NUMBER := 1;
                    CSn <= '0';
					IF DAC_NUMBER >= DAC_QUANTITY THEN		--All DACs done writing
						DAC_NUMBER := 1;
						state <= DONE;
						data(19 DOWNTO 16) <= "0000";
					ELSE									--One DAC done writing
						data(19 DOWNTO 16) <= STD_LOGIC_VECTOR(TO_UNSIGNED(DAC_NUMBER,4));				
						DAC_NUMBER := DAC_NUMBER + 1;
						state <= SET_DATA;
					END IF;
                    CLK_EN <= '0';
				ELSE
					BIT_NUMBER := BIT_NUMBER + 1;
					DAC_NUMBER := DAC_NUMBER;
					CSn <= '0';
					SDO <= data(BIT_QUANTITY - BIT_NUMBER);				--Send the next bit	
					state <= WRITE_DATA;
                    CLK_EN <= '1';
				END IF;
				
			WHEN DONE =>
				DAC_NUMBER := 1;
				IF CLKDAC = '0' THEN
					state <= WAIT_TRIGGER;
				END IF;
                CLK_EN <= '0';
                CSn <= '1';
									
			WHEN OTHERS =>
				state <= WAIT_TRIGGER;
                CLK_EN <= '0';
                CSn <= '1';
                
		END CASE;
        
        data(3 DOWNTO 0) <= "0000";
		data(23 DOWNTO 20) <= "0011";	
        
	END IF;
		
END PROCESS;

END DAC8CH_architecture;

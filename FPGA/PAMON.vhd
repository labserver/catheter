LIBRARY ieee;
USE ieee.std_logic_1164.all;


--  Entity Declaration

ENTITY PAMON IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		RSTn : IN STD_LOGIC;
		CLK48M : IN STD_LOGIC;
        PATRIG_FROM_LASER2 : IN STD_LOGIC;
		PAMON_START : BUFFER STD_LOGIC;
		PAMON_RESET : BUFFER STD_LOGIC;
        ReadData : BUFFER STD_LOGIC
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END PAMON;


--  Architecture Body

ARCHITECTURE PAMON_architecture OF PAMON IS
	
	SIGNAL state : STD_LOGIC_VECTOR(2 DOWNTO 0); --bit2=sync bit1=reset bit0=start
    SIGNAL counterDurationIntegration : INTEGER RANGE 0 TO 6000000;
	
BEGIN

PROCESS(CLK48M, RSTn)

BEGIN

	IF (RSTn = '0') THEN
		state <= "010";
        counterDurationIntegration <= 0;
        PAMON_START <= '1';
        PAMON_RESET <= '0';   
        ReadData <= '0';
    ELSIF (CLK48M'EVENT AND CLK48M = '1') THEN
		state <= state;
        ReadData <= '0';
		IF state = "110" THEN
            PAMON_START <= '1';
            PAMON_RESET <= '0';        
            IF PATRIG_FROM_LASER2 = '1' THEN 
                state <= "100";		
            END IF;
		ELSIF state = "100" THEN --Start 10us
            counterDurationIntegration <= counterDurationIntegration + 1;
            PAMON_START <= '0';
            PAMON_RESET <= '1';            
            IF counterDurationIntegration > 500 THEN
                counterDurationIntegration <= 0;
                state <= "111";
            END IF;
		ELSIF state = "111" THEN --Hold 300us
            PAMON_START <= '1';
            PAMON_RESET <= '1';       
            ReadData <= '1';
            counterDurationIntegration <= counterDurationIntegration + 1;
            IF counterDurationIntegration > 15000 THEN
                counterDurationIntegration <= 0;
                state <= "011";
            END IF;
		ELSIF state = "011" THEN --Reset 10us
            PAMON_START <= '1';
            PAMON_RESET <= '0';        
            counterDurationIntegration <= counterDurationIntegration + 1;
            IF counterDurationIntegration > 500 THEN
                counterDurationIntegration <= 0;
                state <= "010";
            END IF;
		ELSIF state = "010" THEN --Reset
            PAMON_START <= '1';
            PAMON_RESET <= '0';
            IF PATRIG_FROM_LASER2 = '0' THEN 
                state <= "110";		
            END IF;
		END IF;
	END IF;
		
END PROCESS;


END PAMON_architecture;

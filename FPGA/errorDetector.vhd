LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
--use ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;

--  Entity Declaration

ENTITY ErrorDetector IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		RSTn : IN STD_LOGIC;
		clkFastest : IN STD_LOGIC; --Fastest clock of the system to recover every error signal
		enableTransfer : IN STD_LOGIC; --When this signals goes to 0, it means that the error codes have been transmitted (they will be reset)

		--Multiple input error signals
		errorIn1 : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		errorIn2 : IN STD_LOGIC_VECTOR(4 DOWNTO 0);	
		errorIn3 : IN STD_LOGIC;
		errorIn4 : IN STD_LOGIC;
		errorIn5 : IN STD_LOGIC_VECTOR(1 DOWNTO 0);	
		errorIn6 : IN STD_LOGIC;
		errorIn7 : IN STD_LOGIC;
        errorIn8 : IN STD_LOGIC;
        errorIn9 : IN STD_LOGIC;
		
		errorOut : BUFFER STD_LOGIC_VECTOR(31 DOWNTO 0) --Output 32bit signal
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END ErrorDetector;


--  Architecture Body 

ARCHITECTURE ErrorDetector_architecture OF ErrorDetector IS

	SIGNAL enableTransferPrevious : STD_LOGIC;
    SIGNAL errorSignal : STD_LOGIC_VECTOR(31 DOWNTO 0);
	
BEGIN

--Simple process that takes multiple error information as an input and regroup them in a single 32bits vector
PROCESS(RSTn, clkFastest)
BEGIN
	IF (RSTn = '0') THEN
		errorOut <= (OTHERS => '0');
        errorSignal <= (OTHERS => '0');
		enableTransferPrevious <= '0';
	ELSIF (clkFastest'EVENT AND clkFastest = '1') THEN
		errorOut <= errorOut;
        errorSignal <= errorSignal;
		--Detect rising edges of any errorIn
		IF errorIn1(0) = '1' THEN errorSignal(0) <= '1'; END IF;
		IF errorIn1(1) = '1' THEN errorSignal(1) <= '1'; END IF;
		IF errorIn1(2) = '1' THEN errorSignal(2) <= '1'; END IF;
		IF errorIn1(3) = '1' THEN errorSignal(3) <= '1'; END IF;
		IF errorIn1(4) = '1' THEN errorSignal(4) <= '1'; END IF;
		IF errorIn2(0) = '1' THEN errorSignal(5) <= '1'; END IF;	
		IF errorIn2(1) = '1' THEN errorSignal(6) <= '1'; END IF;	
		IF errorIn2(2) = '1' THEN errorSignal(7) <= '1'; END IF; --128	
		IF errorIn2(3) = '1' THEN errorSignal(8) <= '1'; END IF;	
		IF errorIn2(4) = '1' THEN errorSignal(9) <= '1'; END IF;			
		IF errorIn3 = '1' THEN errorSignal(10) <= '1'; END IF;	
		IF errorIn4 = '1' THEN errorSignal(11) <= '1'; END IF;	--2048
		IF errorIn5(0) = '1' THEN errorSignal(12) <= '1'; END IF;	
		IF errorIn5(1) = '1' THEN errorSignal(13) <= '1'; END IF;
		IF errorIn6 = '1' THEN errorSignal(14) <= '1'; END IF;	
		IF errorIn7 = '1' THEN errorSignal(15) <= '1'; END IF;	
        IF errorIn8 = '1' THEN errorSignal(16) <= '1'; END IF;	
        IF errorIn9 = '1' THEN errorSignal(17) <= '1'; END IF;	
		
		--Reset on falling edge of enableTransfer
		enableTransferPrevious <= enableTransfer;
		IF enableTransferPrevious = '0' AND enableTransfer = '1' THEN
            errorOut <= errorSignal;
			errorSignal <= (OTHERS => '0');
		END IF;
	END IF;

END PROCESS;
	
END ErrorDetector_architecture;
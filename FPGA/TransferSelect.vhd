--COMMENTS:
--
--This module acts as a multiplexer to send data to the computer from multiple VHDL modules
--
--The signal enableTransfer is activated once all the transferReady signals have been received
--It stays enable until all the data has been transfered, then it is disables to stop the transfer

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY TransferSelect IS
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;
		transferPAUS16bits : IN STD_LOGIC; --If '1', there are two samples in one data

		--Signal to inform that a given module is ready to transfer (can be a pulse).
		--The other modules should assign this signal and wait for enableTransfer rising edge to make sure it has been detected.
		transferReady1 : IN STD_LOGIC;
		transferReady2 : IN STD_LOGIC;
		transferReady3 : IN STD_LOGIC;
		transferReady4 : IN STD_LOGIC;
		
		--Number of 32-bits data from each module
		quantity1 : IN INTEGER RANGE 0 TO 2147483647;
		quantity2 : IN INTEGER RANGE 0 TO 2147483647;
		quantity3 : IN INTEGER RANGE 0 TO 2147483647;
		quantity4 : IN INTEGER RANGE 0 TO 2147483647;
		
		--Data to transfer from each module
		data1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		data2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		data3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		data4 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		
		--Index of the data to transfer from each module
		idx1 : OUT INTEGER RANGE 0 TO 2147483647;	
		idx2 : OUT INTEGER RANGE 0 TO 2147483647;	
		idx3 : OUT INTEGER RANGE 0 TO 2147483647;	
		idx4 : OUT INTEGER RANGE 0 TO 2147483647;	


		-- Control signals to the Avalon MM-Master.
		control_go : BUFFER STD_LOGIC; -- Empties FIFO data in the SDRAM.
		control_write_base : BUFFER UNSIGNED(23 DOWNTO 0); -- In bytes.									
		control_write_length : BUFFER UNSIGNED(23 DOWNTO 0); -- In bytes. Don't transfer more data than length, otherwise there will be an overflow.
		control_done : IN STD_LOGIC; -- Only used to make sure the transfer is done before starting a new one.

		-- FIFO signals to the Avalon MM-Master.
		user_buffer_data : BUFFER STD_LOGIC_VECTOR(31 DOWNTO 0); -- UNSIGNED only for test purpose, should be STD_LOGIC_VECTOR.
		user_buffer_full : IN STD_LOGIC;
		user_write_buffer : BUFFER STD_LOGIC;

		-- Pointers used to keep track of the location of the data in the SDRAM.
        addressLastCompleteWrite : BUFFER UNSIGNED(23 DOWNTO 0); -- Pointer corresponding to the last data written on the SDRAM. Must be available to the read module.
        addressLastCompleteRead : IN UNSIGNED(23 DOWNTO 0); -- Pointer corresponding to the last data read on the SDRAM. Used to make sure the SDRAM is not full.
		
		-- Used for debugging.
		writingSDRAM : OUT STD_LOGIC;
        debugOut : OUT UNSIGNED(31 DOWNTO 0);
        debugOut2 : OUT UNSIGNED(31 DOWNTO 0);
        errorFull : OUT STD_LOGIC;			
		LED : OUT STD_LOGIC_VECTOR(4 DOWNTO 0)
		
	);	
END TransferSelect;

--  Architecture Body
ARCHITECTURE TransferSelect_architecture OF TransferSelect IS
	
	-- State machine declaration.	
	TYPE stateMachine IS (State_Init, State_WaitReady, State_CheckSDRAMFull, State_CheckControllerReady, State_CheckFIFOFull, State_Writing, State_Done);
	SIGNAL state : stateMachine;
	
	SIGNAL transferReadyDetector1 : STD_LOGIC;
	SIGNAL transferReadyDetector2 : STD_LOGIC;
	SIGNAL transferReadyDetector3 : STD_LOGIC;
	SIGNAL transferReadyDetector4 : STD_LOGIC;
	
	SIGNAL quantityToSendTotal : INTEGER RANGE 0 TO 2147483647;
	SIGNAL waiting : INTEGER RANGE 0 TO 1010;
	
	-- Internal value of user_write_buffer.
	SIGNAL user_write_buffer_internal : STD_LOGIC;
	
	-- Index of the data to write in the SDRAM.
	SIGNAL idxSend : INTEGER RANGE 0 TO 2147483647;
	
	--PAUS read address must be sent one cycle in advance when in 16 bits data. Also, increment is 2, not 1.
	SIGNAL idxSendPAUS : INTEGER RANGE 0 TO 2147483647;
	
	-- Used only in 16 bits mode
	SIGNAL isFull : STD_LOGIC := '0'; 
	SIGNAL user_buffer_data_backup : STD_LOGIC_VECTOR(31 DOWNTO 0); --Backup data to write if full
	
	-- Internal data that will be transferred on the next available FIFO buffer.
	SIGNAL dataToSDRAM : STD_LOGIC_VECTOR(31 DOWNTO 0);
		
BEGIN
PROCESS(RSTn, clk48MHz)
	-- Variable to test if the SDRAM is full.
	VARIABLE TestFull : INTEGER RANGE 0 TO 50000000;
BEGIN
IF (RSTn = '0') THEN
	quantityToSendTotal <= 0;	
	waiting <= 0;
	state <= State_Init;
	transferReadyDetector1 <= '0'; transferReadyDetector2 <= '0'; transferReadyDetector3 <= '0'; transferReadyDetector4 <= '0';	
	
	control_go <= '0';
	control_write_base <= TO_UNSIGNED(0,24);
	control_write_length <= TO_UNSIGNED(0,24);	
	addressLastCompleteWrite <= TO_UNSIGNED(0,24);
	user_write_buffer_internal <= '0';
	idxSend <= 0;
	idxSendPAUS <= 0;
	errorFull <= '0';
	debugOut <= TO_UNSIGNED(0,32);
    writingSDRAM <= '0';
	isFull <= '0';
	user_buffer_data_backup <= (OTHERS => '0');
    --LED <= (OTHERS => '0');
	
ELSIF (clk48MHz'EVENT AND clk48MHz = '1') THEN	
	control_go <= '0';
	user_write_buffer_internal <= '0';	
	debugOut <= TO_UNSIGNED(idxSend,32);
	debugOut2 <= TO_UNSIGNED(TO_INTEGER(control_write_length),32);	
	CASE state IS
		WHEN State_Init => --Reset
			state <= State_WaitReady;
			transferReadyDetector1 <= '0'; transferReadyDetector2 <= '0'; transferReadyDetector3 <= '0'; transferReadyDetector4 <= '0';
		
		WHEN State_WaitReady => 
		
			IF transferReady1 = '1' THEN transferReadyDetector1 <= '1'; END IF;
			IF transferReady2 = '1' THEN transferReadyDetector2 <= '1'; END IF;
			IF transferReady3 = '1' THEN transferReadyDetector3 <= '1'; END IF;
			IF transferReady4 = '1' THEN transferReadyDetector4 <= '1'; END IF;	

			--Waiting for transferReady from all the modules (a pulse is enough)
			IF transferReadyDetector1 = '1' AND transferReadyDetector2 = '1' AND transferReadyDetector3 = '1' AND transferReadyDetector4 = '1'THEN
				state <= State_CheckSDRAMFull;
				quantityToSendTotal <= quantity1+quantity2+quantity3+quantity4;
                writingSDRAM <= '1';
			END IF;

        WHEN State_CheckSDRAMFull => -- Make sure the SDRAM is not full. There is an error if it is full.
		
			idxSendPAUS <= 0;
            state <= State_CheckControllerReady;
			writingSDRAM <= '1';
			-- Verify if writing loops around the SDRAM (one part at the end of the SDRAM and the rest at the beginning).
            TestFull := TO_INTEGER(control_write_base);
            TestFull := TestFull + quantityToSendTotal + quantityToSendTotal + quantityToSendTotal + quantityToSendTotal; --In bytes
            IF TestFull < 16777216 THEN
                -- Verify if the read pointer is in the range where we want to write data. If it is, we wait.
                IF (addressLastCompleteRead > control_write_base AND addressLastCompleteRead < TestFull) THEN
                    state <= State_CheckSDRAMFull;
                    errorFull <= '1';
                END IF;
            ELSE
                -- Verify if the read pointer is in the range where we want to write data. If it is, we wait.
                IF (addressLastCompleteRead > control_write_base) OR (addressLastCompleteRead < TestFull - 16777216) THEN
                    state <= State_CheckSDRAMFull;
                    errorFull <= '1';
                END IF;
            END IF;   
			
		WHEN State_CheckControllerReady => -- Make sure the controller is ready to receive data.
            writingSDRAM <= '1';
			IF control_done = '1' AND user_buffer_full = '0' THEN
				state <= State_CheckFIFOFull;
				idxSend <= 0;
				idxSendPAUS <= idxSendPAUS + 2;
                control_write_length <= TO_UNSIGNED(quantityToSendTotal + quantityToSendTotal + quantityToSendTotal + quantityToSendTotal,24); -- In bytes.           
			END IF;
			
		WHEN State_CheckFIFOFull => 
            writingSDRAM <= '1';
			IF user_buffer_full = '0' THEN -- Not full.
				idxSendPAUS <= idxSendPAUS + 2;
				state <= State_Writing;		
			END IF;
		
		WHEN State_Writing =>
            writingSDRAM <= '1';
            -- One go pulse just after we begin the transfer. At least one data must be written before asserting this signal.
			IF idxSend = 10 THEN
				control_go <= '1';
			END IF;	
            
            -- Check if buffer is full. If it is, the last data was not written, so we need to decrement the counter.
            IF user_buffer_full = '0' THEN
                -- The last data was written correctly. Now we check if we are done writing. If not, we write and increment the counter.
                IF idxSend < quantityToSendTotal THEN	
					IF isFull = '1' AND transferPAUS16bits = '1' THEN
						isFull <= '0';
						user_buffer_data <= user_buffer_data_backup; --Retreive backup data to write if full
					ELSE	
						user_buffer_data <= dataToSDRAM;
						user_buffer_data_backup <= dataToSDRAM; --Backup data to write if full
					END IF;
                    idxSend <= idxSend + 1; -- In bytes.
					idxSendPAUS <= idxSendPAUS + 2;
                    user_write_buffer_internal <= '1';
                ELSE
					-- Done writing.
					addressLastCompleteWrite <= addressLastCompleteWrite + control_write_length;
					state <= State_Done;
                    writingSDRAM <= '0';
					-- The base address of the next write must be changed.
					control_write_base <= addressLastCompleteWrite + control_write_length + 4; -- In bytes. The sum wraps around if there is an overflow.
                END IF;
            ELSE 
                state <= State_CheckFIFOFull;
                idxSend <= idxSend - 1; -- In bytes.
				idxSendPAUS <= idxSendPAUS - 4;
				isFull <= '1';
            END IF;		
	
        WHEN State_Done =>
 			writingSDRAM <= '0';
			waiting <= waiting + 1;
			IF waiting > 10 THEN
				state <= State_Init;
				waiting <= 0;		
			END IF;       
        
		WHEN OTHERS =>
		
			state <= State_Init;
			
	END CASE;
END IF;					
END PROCESS;	

--Data to write to the SDRAM comes from multiple VHDL modules.
dataToSDRAM <= data1 WHEN idxSend < quantity1 
	ELSE data2 WHEN idxSend < quantity1 + quantity2
	ELSE data3 WHEN idxSend < quantity1 + quantity2 + quantity3
	ELSE data4;

--Index of the data to write to the SDRAM is properly transferred to all the external VHDL modules.	
idx1 <= idxSend WHEN idxSend < quantity1  AND transferPAUS16bits = '0'
	ELSE idxSendPAUS WHEN idxSend < quantity1 AND transferPAUS16bits = '1'
	ELSE 0;
idx2 <= idxSend - quantity1 
	WHEN idxSend < quantity1 + quantity2 AND idxSend > quantity1 
	ELSE 0;
idx3 <= idxSend - quantity1 - quantity2 
	WHEN idxSend < quantity1 + quantity2 + quantity3 AND idxSend > quantity1 + quantity2 
	ELSE 0;
idx4 <= idxSend - quantity1 - quantity2 - quantity3 
	WHEN idxSend < quantity1 + quantity2 + quantity3 + quantity4 AND idxSend > quantity1 + quantity2 + quantity3 
	ELSE 0;

-- user_write_buffer must be in combinational logic.
user_write_buffer <= '1' WHEN (user_buffer_full = '0' AND user_write_buffer_internal = '1') ELSE '0';
	
LED(0) <= transferReady1;
LED(1) <= transferReady2;
LED(2) <= transferReady3;
LED(3) <= transferReady4;
LED(4) <= '0';
END TransferSelect_architecture;

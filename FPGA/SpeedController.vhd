LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY SpeedController IS
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;											--Signal d'horloge principal
		gearRatio : IN UNSIGNED(7 DOWNTO 0);
		encoderLine : IN UNSIGNED(19 DOWNTO 0);					--position du moteur	
		rotatingMotorValue : BUFFER UNSIGNED(11 DOWNTO 0);
        lineDuration : IN INTEGER RANGE 0 TO 16777215;                --48000000/LINES_PER_TURN/SPEED_WANTED. Normal duration between 2 motor positions in number of cycles
        lineDurationTol : IN INTEGER RANGE 0 TO 16777215              --Tolerance on the speed (5%)
	);
END SpeedController;

--  Architecture Body 
ARCHITECTURE SpeedController_architecture OF SpeedController IS
	CONSTANT CLK_FREQ : INTEGER := 48000000;
	
	SIGNAL counterLine : INTEGER RANGE 0 TO 500000000;
	SIGNAL idxTurn : UNSIGNED(10 downto 0);
	SIGNAL turnDone : STD_LOGIC;
	SIGNAL encoderLinePrevious : UNSIGNED(19 DOWNTO 0);	
    SIGNAL skipCheck : INTEGER RANGE 0 TO 1000; 
 
BEGIN


PROCESS(clk48MHz, RSTn)

BEGIN

	IF (RSTn = '0') THEN	
		idxTurn <= (OTHERS => '0');
		turnDone <= '0';	
		rotatingMotorValue <= (OTHERS => '0');
		encoderLinePrevious <= (OTHERS => '0');
        skipCheck <= 0;
	ELSE
		IF (clk48MHz'EVENT AND clk48MHz = '1') THEN
			rotatingMotorValue <= rotatingMotorValue;
			counterLine <= counterLine;	
			encoderLinePrevious <= encoderLine;
				
			IF encoderLinePrevious /= encoderLine THEN        
                skipCheck <= skipCheck + 1;
                IF skipCheck >= 20 THEN
                    counterLine <= 0;
                    skipCheck <= 0;  
                    IF counterLine < lineDuration-lineDurationTol-lineDurationTol-lineDurationTol-lineDurationTol THEN			--Le moteur tourne trop vite
                        IF rotatingMotorValue >= 100 THEN
                            rotatingMotorValue <= rotatingMotorValue - 100;
                        END IF;
                    ELSIF counterLine > lineDuration+lineDurationTol+lineDurationTol+lineDurationTol+lineDurationTol THEN		--Le moteur tourne trop lentement
                        IF rotatingMotorValue <= 3995 THEN
                            rotatingMotorValue <= rotatingMotorValue + 100;
                        END IF;     
                    ELSIF counterLine < lineDuration-lineDurationTol-lineDurationTol THEN			--Le moteur tourne trop vite
                        IF rotatingMotorValue >= 20 THEN
                            rotatingMotorValue <= rotatingMotorValue - 20;
                        END IF;
                    ELSIF counterLine > lineDuration+lineDurationTol+lineDurationTol THEN		--Le moteur tourne trop lentement
                        IF rotatingMotorValue <= 4075 THEN
                            rotatingMotorValue <= rotatingMotorValue + 20;
                        END IF;                    
                    ELSIF counterLine < lineDuration-lineDurationTol THEN			--Le moteur tourne trop vite
                        IF rotatingMotorValue >= 10 THEN
                            rotatingMotorValue <= rotatingMotorValue - 10;
                        END IF;
                    ELSIF counterLine > lineDuration+lineDurationTol THEN		--Le moteur tourne trop lentement
                        IF rotatingMotorValue <= 4085 THEN
                            rotatingMotorValue <= rotatingMotorValue + 10;
                        END IF;
                    END IF;
                END IF;
            ELSE
                counterLine <= counterLine + 1;
                IF counterLine > 24000000 THEN
                    IF rotatingMotorValue <= 4094 THEN
                        rotatingMotorValue <= rotatingMotorValue + 1; --Le moteur est bloqué
                    END IF;				
                END IF;
            END IF;
		
		
		END IF;
	END IF;
END PROCESS;
	
END SpeedController_architecture;				
				
				
				
				
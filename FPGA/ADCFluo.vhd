LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY ADCFluo IS
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;
		clk500kHzFluo : IN STD_LOGIC;

		--Output to the demodulation module array_ADCData
		ADCFluoData1 : BUFFER SIGNED(31 DOWNTO 0);		
		ADCFluoData2 : BUFFER SIGNED(31 DOWNTO 0);
		ADCFluo_ReadDone : BUFFER STD_LOGIC;
				
		--Communication with the ADCs and Muxes
		CONVERT : BUFFER STD_LOGIC;
		SDI : IN STD_LOGIC;
		SCK : OUT STD_LOGIC;
		CSn : BUFFER STD_LOGIC_VECTOR(1 DOWNTO 0);
		
		LED : OUT STD_LOGIC_VECTOR(5 DOWNTO 0)
	);	
END ADCFluo;


--  Architecture Body

ARCHITECTURE ADCFluo_architecture OF ADCFluo IS
	
	TYPE stateMachine IS (INIT, CONV, READ_ADC1, READ_ADC2, SYNC0, SYNC);
	SIGNAL state : stateMachine;	
	
	SIGNAL ADCFluoDataTemp1 : SIGNED(31 DOWNTO 0); --Data received is signed
	SIGNAL ADCFluoDataTemp2 : SIGNED(31 DOWNTO 0); --Data received is signed
	
	SIGNAL BIT_NUMBER : INTEGER RANGE 1 TO 18 := 18;		--To loop on all bits of an ADC

BEGIN
LED <= (OTHERS => '0');
SCK <= clk48MHz;

PROCESS(RSTn, clk48MHz)

	CONSTANT PERIOD : INTEGER := 21;						--Approx 20ns
	CONSTANT BIT_QUANTITY : INTEGER := 18;					--Number of BIT per ADC
	VARIABLE TIMER : INTEGER RANGE 1 TO 1000 := PERIOD;		--Used for conversion time of the ADC
	

BEGIN

	IF (RSTn = '0') THEN
		state <= INIT;
		CONVERT <= '0';	
		CSn <= "11";
		BIT_NUMBER <= BIT_QUANTITY;
		ADCFluoData1 <= (OTHERS => '0');
		ADCFluoData2 <= (OTHERS => '0');
		ADCFluoDataTemp1 <= (OTHERS => '0');
		ADCFluoDataTemp2 <= (OTHERS => '0');
		ADCFluo_ReadDone <= '0';
		
	ELSIF (clk48MHz'EVENT AND clk48MHz = '0') THEN
		CONVERT <= CONVERT;	
		CSn <= "11";		
		ADCFluoData1 <= ADCFluoData1;
		ADCFluoData2 <= ADCFluoData2;
		ADCFluoDataTemp1 <= ADCFluoDataTemp1;
		ADCFluoDataTemp2 <= ADCFluoDataTemp2;
		state <= state;
		ADCFluo_ReadDone <= '0';

		CASE state IS
			WHEN INIT =>									--Sync at 500kHz
				CONVERT <= '0';
				CSn <= "11";			
				IF clk500kHzFluo = '1' THEN
					state <= CONV;
					TIMER := PERIOD;
				ELSE
					state <= INIT;
				END IF;

			WHEN CONV => 									--Start conversion		
				CONVERT <= '1';
				IF TIMER > 750 THEN	--Assert conversion time is at least 750ns	 
					CSn <= "01";
					TIMER := PERIOD;
					state <= READ_ADC1;		
				ELSE
					CSn <= "11";
					TIMER := TIMER + PERIOD;
					state  <= CONV;
				END IF;
				BIT_NUMBER <= BIT_QUANTITY;
				
			WHEN READ_ADC1 => 									--Read data	(bit per bit)	
				ADCFluoDataTemp1(BIT_NUMBER-1) <= SDI;																
					
				IF BIT_NUMBER <= 1 THEN						--Group of 18 bits done reading
					BIT_NUMBER <= BIT_QUANTITY;
					CSn <= "10";
					CONVERT <= '1';
					
					--18 to 32 bits signed
					ADCFluoDataTemp1(31 DOWNTO 18) <= (OTHERS => ADCFluoDataTemp1(17));

					state <= READ_ADC2;
						
				ELSE
					CSn <= "01";
					CONVERT <= '1';
					BIT_NUMBER <= BIT_NUMBER - 1;
					state <= READ_ADC1;
				END IF;	

			WHEN READ_ADC2 => 									--Read data	(bit per bit)	
				ADCFluoDataTemp2(BIT_NUMBER-1) <= SDI;																
					
				IF BIT_NUMBER <= 1 THEN						--Group of 18 bits done reading
					BIT_NUMBER <= BIT_QUANTITY;
					CSn <= "11";
					CONVERT <= '1';
					
					--18 to 32 bits signed
					ADCFluoDataTemp2(31 DOWNTO 18) <= (OTHERS => ADCFluoDataTemp2(17));
					
					state <= SYNC0;
						
				ELSE
					CSn <= "10";
					CONVERT <= '1';
					BIT_NUMBER <= BIT_NUMBER - 1;
					state <= READ_ADC2;
				END IF;	
                
			WHEN SYNC0 =>
				CONVERT <= '0';
                --Convert to make sure there is no negative value.
                ADCFluoData1 <= ADCFluoDataTemp1+131072;
                ADCFluoData2 <= ADCFluoDataTemp2+131072;
                state <= SYNC;
                
			WHEN SYNC =>
				CONVERT <= '0';
				IF clk500kHzFluo = '0' THEN
					state <= INIT;
					ADCFluo_ReadDone <= '1';
				ELSE
					state <= SYNC;
				END IF;
			WHEN OTHERS =>
				CONVERT <= '0';		
				state <= INIT;
		END CASE;
	END IF;
		
END PROCESS;

END ADCFluo_architecture;

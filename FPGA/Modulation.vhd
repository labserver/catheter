LIBRARY ieee;
USE ieee.std_logic_1164.all;


--  Entity Declaration

ENTITY Modulation IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		RSTn : IN STD_LOGIC;
		modulationOn : IN STD_LOGIC;
		clk5kHzFluo : IN STD_LOGIC;
		clk10kHzFluo : IN STD_LOGIC;
		laserPower1 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
		laserPower2 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
		laser1 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
		laser2 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0)
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END Modulation;


--  Architecture Body

ARCHITECTURE Modulation_architecture OF Modulation IS

BEGIN

laser1 <= laserPower1 WHEN ((clk5kHzFluo = '1' OR modulationOn = '0') AND RSTn = '1') ELSE (OTHERS => '0');
laser2 <= laserPower2 WHEN ((clk10kHzFluo = '1' OR modulationOn = '0') AND RSTn = '1') ELSE (OTHERS => '0');

END Modulation_architecture;

--
-- Brief: Controls the Avalon MM-Master Read Module, which is very similar to a normal FIFO buffer.
--

-- Libraries --
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY ReadSDRAM IS

	PORT
	(
		RSTn : IN STD_LOGIC;
		CLK48MHZ : IN STD_LOGIC;

		-- Communication with the transfer module.
		dataToFIFO : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --Data to transfer to the computer.
		idxSend : IN INTEGER RANGE 0 TO 2147483647; --Index of the data to send.
		dataAvailable : OUT STD_LOGIC; --Assert to '1' if data is always available. On the clock cycle following the rising edge, the data is sent to the FIFO and idxSend is incremented (the write delay is done before monitoring dataAvailable).
		enableTransfer : OUT STD_LOGIC; --Signal that enables the transfer.
			
		-- Control signals to the Avalon MM-Master.
		control_go : OUT STD_LOGIC; -- Insert data in the FIFO from the SDRAM.
		control_read_base : OUT UNSIGNED(23 DOWNTO 0); -- In bytes.											
		control_read_length : BUFFER UNSIGNED(23 DOWNTO 0); -- In bytes.
		control_done : IN STD_LOGIC; -- Only used to make sure the transfer is done before starting a new one.
		
		-- FIFO signals to the Avalon MM-Master.
		user_buffer_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		user_read_buffer : OUT STD_LOGIC;
		user_data_available : IN STD_LOGIC;
        
		-- Pointers used to keep track of the location of the data in the SDRAM.
        addressLastCompleteWrite : IN UNSIGNED(23 DOWNTO 0); -- Pointer corresponding to the last data written on the SDRAM. If this is different than the address of the last read, it triggers a reading.
		addressLastCompleteRead : BUFFER UNSIGNED(23 DOWNTO 0);
		
		-- Used for debugging.
		readingDone : OUT STD_LOGIC;
        debugOut : BUFFER UNSIGNED(31 DOWNTO 0);
        errorData : BUFFER UNSIGNED(2 DOWNTO 0)

	);
	
END ReadSDRAM;

--  Architecture Body 
ARCHITECTURE ReadSDRAM_architecture OF ReadSDRAM IS
	
	-- State machine declaration.
	TYPE stateMachine IS (State_CheckReadyAndDataAvailable, READING, DONE);
	SIGNAL state : stateMachine;
    
    -- Previous value of idxSend. Used to detect changes.
    SIGNAL idxSendPrevious : INTEGER RANGE 0 TO 2147483647;
	
BEGIN

PROCESS(RSTn, CLK48MHZ)

BEGIN
IF (RSTn = '0') THEN
	control_go <= '0';
	readingDone <= '0';
	state <= State_CheckReadyAndDataAvailable;
	control_read_base <= TO_UNSIGNED(0,24); -- In bytes
	control_read_length <= TO_UNSIGNED(0,24); -- In bytes	
    addressLastCompleteRead <= TO_UNSIGNED(0,24); -- In bytes
    debugOut <= TO_UNSIGNED(0,32);
    errorData <= (OTHERS => '0'); 
    idxSendPrevious <= 0;
    enableTransfer <= '0';    
	
ELSIF (CLK48MHZ'EVENT AND CLK48MHZ = '1') THEN
	control_go <= '0';
    readingDone <= '0';
	idxSendPrevious <= idxSend; 
    debugOut <= TO_UNSIGNED(0,32);
    --errorData <= (OTHERS => '0'); 	
    
	CASE state IS
	                
		WHEN State_CheckReadyAndDataAvailable => 
            -- Make sure the last transfer is finished.
			IF control_done = '1' AND user_data_available = '0' AND idxSend = 0 THEN 
                -- Verify if data is available by using the read and write pointers.
                IF addressLastCompleteRead /= addressLastCompleteWrite THEN
                    -- Verify if the write address had an overflow, which means that a part of the data is at the end of the SDRAM and the rest is at the beginning.
					-- In either case, the quantity of data to read is calculated with the difference of the read and write pointers.
                    IF addressLastCompleteWrite > addressLastCompleteRead THEN
                        -- Make sure not to send more than 4 MBytes at a time
                        IF addressLastCompleteWrite - addressLastCompleteRead <= 4000000 THEN
                            control_read_length <= addressLastCompleteWrite - addressLastCompleteRead; -- In bytes
                            -- After the reading, the read pointer will be equal to the current write pointer. The write pointer might have changed by that time.
                            addressLastCompleteRead <= addressLastCompleteWrite;                            
                        ELSE
                            control_read_length <= TO_UNSIGNED(4000000,24); -- In bytes
                            -- After the reading, the read pointer will be 4000000 bytes farther.
                            addressLastCompleteRead <= addressLastCompleteRead + 4000000;    
                        END IF;                        
                    ELSE
                        -- Make sure not to send more than 4 MBytes at a time
                        IF (TO_UNSIGNED(16777216,24) - addressLastCompleteRead) + addressLastCompleteWrite <= 4000000 THEN                    
                            control_read_length <= (TO_UNSIGNED(16777216,24) - addressLastCompleteRead) + addressLastCompleteWrite; -- In bytes
                            -- After the reading, the read pointer will be equal to the current write pointer. The write pointer might have changed by that time.
                            addressLastCompleteRead <= addressLastCompleteWrite;                            
                        ELSE
                            control_read_length <= TO_UNSIGNED(4000000,24); -- In bytes
                            -- After the reading, the read pointer will be 4000000 words farther.
                            addressLastCompleteRead <= 4000000 - (TO_UNSIGNED(16777216,24) - addressLastCompleteRead);
                        END IF;
                    END IF;

                    control_go <= '1';
                    state <= READING;
                    enableTransfer <= '1';
                ELSE
					-- No data available from the SDRAM.
                    readingDone <= '1';
                END IF;
            ELSE
                --errorData <= "111";
			END IF;
			
		WHEN READING =>
            IF control_read_length > 4000000 THEN
                errorData <= "111";
            END IF;
            -- Verify if all data has been read.
            IF idxSend + 1 + idxSend + 1 + idxSend + 1 + idxSend + 1 >= control_read_length THEN
                enableTransfer <= '0'; --Stop transfer when the transfer module request the last data.
                state <= DONE;
            END IF;

		WHEN DONE =>
			readingDone <= '1';
            control_read_base <= addressLastCompleteRead + 4; -- In bytes. Next read will start there.
            state <= State_CheckReadyAndDataAvailable;

		WHEN OTHERS =>
			state <= State_CheckReadyAndDataAvailable;

	END CASE;	
END IF;

END PROCESS;

-- user_read_buffer must be in combinational logic.
user_read_buffer <= '1' WHEN (idxSend /= idxSendPrevious) ELSE '0';
dataAvailable <= user_data_available;
dataToFIFO <= user_buffer_data; -- Read data.

END ReadSDRAM_architecture;

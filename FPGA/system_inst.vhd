  --Example instantiation for system 'system'
  system_inst : system
    port map(
      control_done_from_the_master_read => control_done_from_the_master_read,
      control_done_from_the_master_write => control_done_from_the_master_write,
      control_early_done_from_the_master_read => control_early_done_from_the_master_read,
      user_buffer_full_from_the_master_write => user_buffer_full_from_the_master_write,
      user_buffer_output_data_from_the_master_read => user_buffer_output_data_from_the_master_read,
      user_data_available_from_the_master_read => user_data_available_from_the_master_read,
      zs_addr_from_the_sdram => zs_addr_from_the_sdram,
      zs_ba_from_the_sdram => zs_ba_from_the_sdram,
      zs_cas_n_from_the_sdram => zs_cas_n_from_the_sdram,
      zs_cke_from_the_sdram => zs_cke_from_the_sdram,
      zs_cs_n_from_the_sdram => zs_cs_n_from_the_sdram,
      zs_dq_to_and_from_the_sdram => zs_dq_to_and_from_the_sdram,
      zs_dqm_from_the_sdram => zs_dqm_from_the_sdram,
      zs_ras_n_from_the_sdram => zs_ras_n_from_the_sdram,
      zs_we_n_from_the_sdram => zs_we_n_from_the_sdram,
      clk_master_read => clk_master_read,
      clk_master_write => clk_master_write,
      clk_sdram => clk_sdram,
      control_fixed_location_to_the_master_read => control_fixed_location_to_the_master_read,
      control_fixed_location_to_the_master_write => control_fixed_location_to_the_master_write,
      control_go_to_the_master_read => control_go_to_the_master_read,
      control_go_to_the_master_write => control_go_to_the_master_write,
      control_read_base_to_the_master_read => control_read_base_to_the_master_read,
      control_read_length_to_the_master_read => control_read_length_to_the_master_read,
      control_write_base_to_the_master_write => control_write_base_to_the_master_write,
      control_write_length_to_the_master_write => control_write_length_to_the_master_write,
      reset_n => reset_n,
      user_buffer_input_data_to_the_master_write => user_buffer_input_data_to_the_master_write,
      user_read_buffer_to_the_master_read => user_read_buffer_to_the_master_read,
      user_write_buffer_to_the_master_write => user_write_buffer_to_the_master_write
    );



LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;


--  Entity Declaration

ENTITY TransferSpeed IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;
		enableTransfer : IN STD_LOGIC;
		durationTransfer : BUFFER UNSIGNED(31 DOWNTO 0); --Duration of a transfer
		durationBetweenTransfers : BUFFER UNSIGNED(31 DOWNTO 0); --Between 2 starts
        
        writingSDRAM : IN STD_LOGIC;
        durationSDRAMWrite : BUFFER UNSIGNED(31 DOWNTO 0) --Duration of a SDRAM transfer
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END TransferSpeed;


--  Architecture Body 

ARCHITECTURE TransferSpeed_architecture OF TransferSpeed IS
	
	SIGNAL counterTransfer : INTEGER RANGE 0 TO 200000000;
	SIGNAL enableTransferPrevious : STD_LOGIC;

	SIGNAL counterSDRAMWrite : INTEGER RANGE 0 TO 200000000;
	SIGNAL writingSDRAMPrevious : STD_LOGIC;
    
BEGIN

PROCESS(RSTn, clk48MHz)
	
BEGIN
		IF (RSTn = '0') THEN		
			durationTransfer <= (OTHERS => '0');
			durationBetweenTransfers <= (OTHERS => '0');
			counterTransfer <= 0;
			durationSDRAMWrite <= (OTHERS => '0');
			counterSDRAMWrite <= 0;            
		ELSIF (clk48MHz'EVENT AND clk48MHz = '1') THEN
			enableTransferPrevious <= enableTransfer;
			durationTransfer <= durationTransfer;
			durationBetweenTransfers <= durationBetweenTransfers;

			counterTransfer <= counterTransfer + 1;
			IF enableTransferPrevious = '0' AND enableTransfer = '1' THEN --Start of a transfer
				durationBetweenTransfers <= UNSIGNED(CONV_STD_LOGIC_VECTOR(counterTransfer, 32));
				counterTransfer <= 0; --Reset
			ELSIF enableTransferPrevious = '1' AND enableTransfer = '0' THEN --End of a transfer
				durationTransfer <= UNSIGNED(CONV_STD_LOGIC_VECTOR(counterTransfer, 32));
			END IF;
            
            
            --SDRAM
			writingSDRAMPrevious <= writingSDRAM;
			durationSDRAMWrite <= durationSDRAMWrite;

			counterSDRAMWrite <= counterSDRAMWrite + 1;
			IF writingSDRAMPrevious = '0' AND writingSDRAM = '1' THEN --Start of a transfer
				counterSDRAMWrite <= 0; --Reset
			ELSIF writingSDRAMPrevious = '1' AND writingSDRAM = '0' THEN --End of a transfer
				durationSDRAMWrite <= UNSIGNED(CONV_STD_LOGIC_VECTOR(counterSDRAMWrite, 32));
			END IF;            
		END IF;
END PROCESS;
	
END TransferSpeed_architecture;			
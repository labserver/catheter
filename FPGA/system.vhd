--megafunction wizard: %Altera SOPC Builder%
--GENERATION: STANDARD
--VERSION: WM1.0


--Legal Notice: (C)2013 Altera Corporation. All rights reserved.  Your
--use of Altera Corporation's design tools, logic functions and other
--software and tools, and its AMPP partner logic functions, and any
--output files any of the foregoing (including device programming or
--simulation files), and any associated documentation or information are
--expressly subject to the terms and conditions of the Altera Program
--License Subscription Agreement or other applicable license agreement,
--including, without limitation, that your use is for the sole purpose
--of programming logic devices manufactured by Altera and sold by Altera
--or its authorized distributors.  Please refer to the applicable
--agreement for further details.


-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity burstcount_fifo_for_clock_crossing_read_s1_module is 
        port (
              -- inputs:
                 signal clear_fifo : IN STD_LOGIC;
                 signal clk : IN STD_LOGIC;
                 signal data_in : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal read : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal sync_reset : IN STD_LOGIC;
                 signal write : IN STD_LOGIC;

              -- outputs:
                 signal data_out : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal empty : OUT STD_LOGIC;
                 signal fifo_contains_ones_n : OUT STD_LOGIC;
                 signal full : OUT STD_LOGIC
              );
end entity burstcount_fifo_for_clock_crossing_read_s1_module;


architecture europa of burstcount_fifo_for_clock_crossing_read_s1_module is
                signal full_0 :  STD_LOGIC;
                signal full_1 :  STD_LOGIC;
                signal full_10 :  STD_LOGIC;
                signal full_11 :  STD_LOGIC;
                signal full_12 :  STD_LOGIC;
                signal full_13 :  STD_LOGIC;
                signal full_14 :  STD_LOGIC;
                signal full_15 :  STD_LOGIC;
                signal full_16 :  STD_LOGIC;
                signal full_17 :  STD_LOGIC;
                signal full_18 :  STD_LOGIC;
                signal full_19 :  STD_LOGIC;
                signal full_2 :  STD_LOGIC;
                signal full_20 :  STD_LOGIC;
                signal full_21 :  STD_LOGIC;
                signal full_22 :  STD_LOGIC;
                signal full_23 :  STD_LOGIC;
                signal full_24 :  STD_LOGIC;
                signal full_25 :  STD_LOGIC;
                signal full_26 :  STD_LOGIC;
                signal full_27 :  STD_LOGIC;
                signal full_28 :  STD_LOGIC;
                signal full_29 :  STD_LOGIC;
                signal full_3 :  STD_LOGIC;
                signal full_30 :  STD_LOGIC;
                signal full_31 :  STD_LOGIC;
                signal full_32 :  STD_LOGIC;
                signal full_33 :  STD_LOGIC;
                signal full_34 :  STD_LOGIC;
                signal full_35 :  STD_LOGIC;
                signal full_36 :  STD_LOGIC;
                signal full_37 :  STD_LOGIC;
                signal full_38 :  STD_LOGIC;
                signal full_39 :  STD_LOGIC;
                signal full_4 :  STD_LOGIC;
                signal full_40 :  STD_LOGIC;
                signal full_41 :  STD_LOGIC;
                signal full_42 :  STD_LOGIC;
                signal full_43 :  STD_LOGIC;
                signal full_44 :  STD_LOGIC;
                signal full_45 :  STD_LOGIC;
                signal full_46 :  STD_LOGIC;
                signal full_47 :  STD_LOGIC;
                signal full_48 :  STD_LOGIC;
                signal full_49 :  STD_LOGIC;
                signal full_5 :  STD_LOGIC;
                signal full_50 :  STD_LOGIC;
                signal full_51 :  STD_LOGIC;
                signal full_52 :  STD_LOGIC;
                signal full_53 :  STD_LOGIC;
                signal full_54 :  STD_LOGIC;
                signal full_55 :  STD_LOGIC;
                signal full_56 :  STD_LOGIC;
                signal full_57 :  STD_LOGIC;
                signal full_58 :  STD_LOGIC;
                signal full_59 :  STD_LOGIC;
                signal full_6 :  STD_LOGIC;
                signal full_60 :  STD_LOGIC;
                signal full_61 :  STD_LOGIC;
                signal full_62 :  STD_LOGIC;
                signal full_63 :  STD_LOGIC;
                signal full_64 :  STD_LOGIC;
                signal full_65 :  STD_LOGIC;
                signal full_66 :  STD_LOGIC;
                signal full_67 :  STD_LOGIC;
                signal full_68 :  STD_LOGIC;
                signal full_69 :  STD_LOGIC;
                signal full_7 :  STD_LOGIC;
                signal full_70 :  STD_LOGIC;
                signal full_71 :  STD_LOGIC;
                signal full_72 :  STD_LOGIC;
                signal full_73 :  STD_LOGIC;
                signal full_8 :  STD_LOGIC;
                signal full_9 :  STD_LOGIC;
                signal how_many_ones :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal one_count_minus_one :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal one_count_plus_one :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal p0_full_0 :  STD_LOGIC;
                signal p0_stage_0 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p10_full_10 :  STD_LOGIC;
                signal p10_stage_10 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p11_full_11 :  STD_LOGIC;
                signal p11_stage_11 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p12_full_12 :  STD_LOGIC;
                signal p12_stage_12 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p13_full_13 :  STD_LOGIC;
                signal p13_stage_13 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p14_full_14 :  STD_LOGIC;
                signal p14_stage_14 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p15_full_15 :  STD_LOGIC;
                signal p15_stage_15 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p16_full_16 :  STD_LOGIC;
                signal p16_stage_16 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p17_full_17 :  STD_LOGIC;
                signal p17_stage_17 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p18_full_18 :  STD_LOGIC;
                signal p18_stage_18 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p19_full_19 :  STD_LOGIC;
                signal p19_stage_19 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p1_full_1 :  STD_LOGIC;
                signal p1_stage_1 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p20_full_20 :  STD_LOGIC;
                signal p20_stage_20 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p21_full_21 :  STD_LOGIC;
                signal p21_stage_21 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p22_full_22 :  STD_LOGIC;
                signal p22_stage_22 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p23_full_23 :  STD_LOGIC;
                signal p23_stage_23 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p24_full_24 :  STD_LOGIC;
                signal p24_stage_24 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p25_full_25 :  STD_LOGIC;
                signal p25_stage_25 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p26_full_26 :  STD_LOGIC;
                signal p26_stage_26 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p27_full_27 :  STD_LOGIC;
                signal p27_stage_27 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p28_full_28 :  STD_LOGIC;
                signal p28_stage_28 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p29_full_29 :  STD_LOGIC;
                signal p29_stage_29 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p2_full_2 :  STD_LOGIC;
                signal p2_stage_2 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p30_full_30 :  STD_LOGIC;
                signal p30_stage_30 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p31_full_31 :  STD_LOGIC;
                signal p31_stage_31 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p32_full_32 :  STD_LOGIC;
                signal p32_stage_32 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p33_full_33 :  STD_LOGIC;
                signal p33_stage_33 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p34_full_34 :  STD_LOGIC;
                signal p34_stage_34 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p35_full_35 :  STD_LOGIC;
                signal p35_stage_35 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p36_full_36 :  STD_LOGIC;
                signal p36_stage_36 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p37_full_37 :  STD_LOGIC;
                signal p37_stage_37 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p38_full_38 :  STD_LOGIC;
                signal p38_stage_38 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p39_full_39 :  STD_LOGIC;
                signal p39_stage_39 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p3_full_3 :  STD_LOGIC;
                signal p3_stage_3 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p40_full_40 :  STD_LOGIC;
                signal p40_stage_40 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p41_full_41 :  STD_LOGIC;
                signal p41_stage_41 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p42_full_42 :  STD_LOGIC;
                signal p42_stage_42 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p43_full_43 :  STD_LOGIC;
                signal p43_stage_43 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p44_full_44 :  STD_LOGIC;
                signal p44_stage_44 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p45_full_45 :  STD_LOGIC;
                signal p45_stage_45 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p46_full_46 :  STD_LOGIC;
                signal p46_stage_46 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p47_full_47 :  STD_LOGIC;
                signal p47_stage_47 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p48_full_48 :  STD_LOGIC;
                signal p48_stage_48 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p49_full_49 :  STD_LOGIC;
                signal p49_stage_49 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p4_full_4 :  STD_LOGIC;
                signal p4_stage_4 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p50_full_50 :  STD_LOGIC;
                signal p50_stage_50 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p51_full_51 :  STD_LOGIC;
                signal p51_stage_51 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p52_full_52 :  STD_LOGIC;
                signal p52_stage_52 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p53_full_53 :  STD_LOGIC;
                signal p53_stage_53 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p54_full_54 :  STD_LOGIC;
                signal p54_stage_54 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p55_full_55 :  STD_LOGIC;
                signal p55_stage_55 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p56_full_56 :  STD_LOGIC;
                signal p56_stage_56 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p57_full_57 :  STD_LOGIC;
                signal p57_stage_57 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p58_full_58 :  STD_LOGIC;
                signal p58_stage_58 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p59_full_59 :  STD_LOGIC;
                signal p59_stage_59 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p5_full_5 :  STD_LOGIC;
                signal p5_stage_5 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p60_full_60 :  STD_LOGIC;
                signal p60_stage_60 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p61_full_61 :  STD_LOGIC;
                signal p61_stage_61 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p62_full_62 :  STD_LOGIC;
                signal p62_stage_62 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p63_full_63 :  STD_LOGIC;
                signal p63_stage_63 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p64_full_64 :  STD_LOGIC;
                signal p64_stage_64 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p65_full_65 :  STD_LOGIC;
                signal p65_stage_65 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p66_full_66 :  STD_LOGIC;
                signal p66_stage_66 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p67_full_67 :  STD_LOGIC;
                signal p67_stage_67 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p68_full_68 :  STD_LOGIC;
                signal p68_stage_68 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p69_full_69 :  STD_LOGIC;
                signal p69_stage_69 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p6_full_6 :  STD_LOGIC;
                signal p6_stage_6 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p70_full_70 :  STD_LOGIC;
                signal p70_stage_70 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p71_full_71 :  STD_LOGIC;
                signal p71_stage_71 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p72_full_72 :  STD_LOGIC;
                signal p72_stage_72 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p7_full_7 :  STD_LOGIC;
                signal p7_stage_7 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p8_full_8 :  STD_LOGIC;
                signal p8_stage_8 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p9_full_9 :  STD_LOGIC;
                signal p9_stage_9 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_0 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_1 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_10 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_11 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_12 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_13 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_14 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_15 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_16 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_17 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_18 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_19 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_2 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_20 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_21 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_22 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_23 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_24 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_25 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_26 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_27 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_28 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_29 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_3 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_30 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_31 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_32 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_33 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_34 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_35 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_36 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_37 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_38 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_39 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_4 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_40 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_41 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_42 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_43 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_44 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_45 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_46 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_47 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_48 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_49 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_5 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_50 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_51 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_52 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_53 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_54 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_55 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_56 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_57 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_58 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_59 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_6 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_60 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_61 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_62 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_63 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_64 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_65 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_66 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_67 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_68 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_69 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_7 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_70 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_71 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_72 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_8 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal stage_9 :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal updated_one_count :  STD_LOGIC_VECTOR (7 DOWNTO 0);

begin

  data_out <= stage_0;
  full <= full_72;
  empty <= NOT(full_0);
  full_73 <= std_logic'('0');
  --data_72, which is an e_mux
  p72_stage_72 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_73 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, data_in);
  --data_reg_72, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_72 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_72))))) = '1' then 
        if std_logic'(((sync_reset AND full_72) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_73))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_72 <= std_logic_vector'("0000");
        else
          stage_72 <= p72_stage_72;
        end if;
      end if;
    end if;

  end process;

  --control_72, which is an e_mux
  p72_full_72 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_71))), std_logic_vector'("00000000000000000000000000000000")));
  --control_reg_72, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_72 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_72 <= std_logic'('0');
        else
          full_72 <= p72_full_72;
        end if;
      end if;
    end if;

  end process;

  --data_71, which is an e_mux
  p71_stage_71 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_72 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_72);
  --data_reg_71, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_71 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_71))))) = '1' then 
        if std_logic'(((sync_reset AND full_71) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_72))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_71 <= std_logic_vector'("0000");
        else
          stage_71 <= p71_stage_71;
        end if;
      end if;
    end if;

  end process;

  --control_71, which is an e_mux
  p71_full_71 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_70, full_72);
  --control_reg_71, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_71 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_71 <= std_logic'('0');
        else
          full_71 <= p71_full_71;
        end if;
      end if;
    end if;

  end process;

  --data_70, which is an e_mux
  p70_stage_70 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_71 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_71);
  --data_reg_70, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_70 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_70))))) = '1' then 
        if std_logic'(((sync_reset AND full_70) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_71))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_70 <= std_logic_vector'("0000");
        else
          stage_70 <= p70_stage_70;
        end if;
      end if;
    end if;

  end process;

  --control_70, which is an e_mux
  p70_full_70 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_69, full_71);
  --control_reg_70, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_70 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_70 <= std_logic'('0');
        else
          full_70 <= p70_full_70;
        end if;
      end if;
    end if;

  end process;

  --data_69, which is an e_mux
  p69_stage_69 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_70 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_70);
  --data_reg_69, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_69 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_69))))) = '1' then 
        if std_logic'(((sync_reset AND full_69) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_70))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_69 <= std_logic_vector'("0000");
        else
          stage_69 <= p69_stage_69;
        end if;
      end if;
    end if;

  end process;

  --control_69, which is an e_mux
  p69_full_69 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_68, full_70);
  --control_reg_69, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_69 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_69 <= std_logic'('0');
        else
          full_69 <= p69_full_69;
        end if;
      end if;
    end if;

  end process;

  --data_68, which is an e_mux
  p68_stage_68 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_69 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_69);
  --data_reg_68, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_68 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_68))))) = '1' then 
        if std_logic'(((sync_reset AND full_68) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_69))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_68 <= std_logic_vector'("0000");
        else
          stage_68 <= p68_stage_68;
        end if;
      end if;
    end if;

  end process;

  --control_68, which is an e_mux
  p68_full_68 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_67, full_69);
  --control_reg_68, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_68 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_68 <= std_logic'('0');
        else
          full_68 <= p68_full_68;
        end if;
      end if;
    end if;

  end process;

  --data_67, which is an e_mux
  p67_stage_67 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_68 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_68);
  --data_reg_67, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_67 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_67))))) = '1' then 
        if std_logic'(((sync_reset AND full_67) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_68))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_67 <= std_logic_vector'("0000");
        else
          stage_67 <= p67_stage_67;
        end if;
      end if;
    end if;

  end process;

  --control_67, which is an e_mux
  p67_full_67 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_66, full_68);
  --control_reg_67, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_67 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_67 <= std_logic'('0');
        else
          full_67 <= p67_full_67;
        end if;
      end if;
    end if;

  end process;

  --data_66, which is an e_mux
  p66_stage_66 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_67 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_67);
  --data_reg_66, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_66 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_66))))) = '1' then 
        if std_logic'(((sync_reset AND full_66) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_67))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_66 <= std_logic_vector'("0000");
        else
          stage_66 <= p66_stage_66;
        end if;
      end if;
    end if;

  end process;

  --control_66, which is an e_mux
  p66_full_66 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_65, full_67);
  --control_reg_66, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_66 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_66 <= std_logic'('0');
        else
          full_66 <= p66_full_66;
        end if;
      end if;
    end if;

  end process;

  --data_65, which is an e_mux
  p65_stage_65 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_66 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_66);
  --data_reg_65, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_65 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_65))))) = '1' then 
        if std_logic'(((sync_reset AND full_65) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_66))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_65 <= std_logic_vector'("0000");
        else
          stage_65 <= p65_stage_65;
        end if;
      end if;
    end if;

  end process;

  --control_65, which is an e_mux
  p65_full_65 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_64, full_66);
  --control_reg_65, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_65 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_65 <= std_logic'('0');
        else
          full_65 <= p65_full_65;
        end if;
      end if;
    end if;

  end process;

  --data_64, which is an e_mux
  p64_stage_64 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_65 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_65);
  --data_reg_64, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_64 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_64))))) = '1' then 
        if std_logic'(((sync_reset AND full_64) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_65))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_64 <= std_logic_vector'("0000");
        else
          stage_64 <= p64_stage_64;
        end if;
      end if;
    end if;

  end process;

  --control_64, which is an e_mux
  p64_full_64 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_63, full_65);
  --control_reg_64, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_64 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_64 <= std_logic'('0');
        else
          full_64 <= p64_full_64;
        end if;
      end if;
    end if;

  end process;

  --data_63, which is an e_mux
  p63_stage_63 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_64 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_64);
  --data_reg_63, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_63 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_63))))) = '1' then 
        if std_logic'(((sync_reset AND full_63) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_64))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_63 <= std_logic_vector'("0000");
        else
          stage_63 <= p63_stage_63;
        end if;
      end if;
    end if;

  end process;

  --control_63, which is an e_mux
  p63_full_63 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_62, full_64);
  --control_reg_63, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_63 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_63 <= std_logic'('0');
        else
          full_63 <= p63_full_63;
        end if;
      end if;
    end if;

  end process;

  --data_62, which is an e_mux
  p62_stage_62 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_63 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_63);
  --data_reg_62, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_62 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_62))))) = '1' then 
        if std_logic'(((sync_reset AND full_62) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_63))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_62 <= std_logic_vector'("0000");
        else
          stage_62 <= p62_stage_62;
        end if;
      end if;
    end if;

  end process;

  --control_62, which is an e_mux
  p62_full_62 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_61, full_63);
  --control_reg_62, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_62 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_62 <= std_logic'('0');
        else
          full_62 <= p62_full_62;
        end if;
      end if;
    end if;

  end process;

  --data_61, which is an e_mux
  p61_stage_61 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_62 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_62);
  --data_reg_61, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_61 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_61))))) = '1' then 
        if std_logic'(((sync_reset AND full_61) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_62))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_61 <= std_logic_vector'("0000");
        else
          stage_61 <= p61_stage_61;
        end if;
      end if;
    end if;

  end process;

  --control_61, which is an e_mux
  p61_full_61 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_60, full_62);
  --control_reg_61, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_61 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_61 <= std_logic'('0');
        else
          full_61 <= p61_full_61;
        end if;
      end if;
    end if;

  end process;

  --data_60, which is an e_mux
  p60_stage_60 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_61 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_61);
  --data_reg_60, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_60 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_60))))) = '1' then 
        if std_logic'(((sync_reset AND full_60) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_61))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_60 <= std_logic_vector'("0000");
        else
          stage_60 <= p60_stage_60;
        end if;
      end if;
    end if;

  end process;

  --control_60, which is an e_mux
  p60_full_60 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_59, full_61);
  --control_reg_60, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_60 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_60 <= std_logic'('0');
        else
          full_60 <= p60_full_60;
        end if;
      end if;
    end if;

  end process;

  --data_59, which is an e_mux
  p59_stage_59 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_60 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_60);
  --data_reg_59, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_59 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_59))))) = '1' then 
        if std_logic'(((sync_reset AND full_59) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_60))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_59 <= std_logic_vector'("0000");
        else
          stage_59 <= p59_stage_59;
        end if;
      end if;
    end if;

  end process;

  --control_59, which is an e_mux
  p59_full_59 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_58, full_60);
  --control_reg_59, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_59 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_59 <= std_logic'('0');
        else
          full_59 <= p59_full_59;
        end if;
      end if;
    end if;

  end process;

  --data_58, which is an e_mux
  p58_stage_58 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_59 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_59);
  --data_reg_58, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_58 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_58))))) = '1' then 
        if std_logic'(((sync_reset AND full_58) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_59))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_58 <= std_logic_vector'("0000");
        else
          stage_58 <= p58_stage_58;
        end if;
      end if;
    end if;

  end process;

  --control_58, which is an e_mux
  p58_full_58 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_57, full_59);
  --control_reg_58, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_58 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_58 <= std_logic'('0');
        else
          full_58 <= p58_full_58;
        end if;
      end if;
    end if;

  end process;

  --data_57, which is an e_mux
  p57_stage_57 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_58 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_58);
  --data_reg_57, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_57 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_57))))) = '1' then 
        if std_logic'(((sync_reset AND full_57) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_58))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_57 <= std_logic_vector'("0000");
        else
          stage_57 <= p57_stage_57;
        end if;
      end if;
    end if;

  end process;

  --control_57, which is an e_mux
  p57_full_57 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_56, full_58);
  --control_reg_57, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_57 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_57 <= std_logic'('0');
        else
          full_57 <= p57_full_57;
        end if;
      end if;
    end if;

  end process;

  --data_56, which is an e_mux
  p56_stage_56 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_57 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_57);
  --data_reg_56, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_56 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_56))))) = '1' then 
        if std_logic'(((sync_reset AND full_56) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_57))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_56 <= std_logic_vector'("0000");
        else
          stage_56 <= p56_stage_56;
        end if;
      end if;
    end if;

  end process;

  --control_56, which is an e_mux
  p56_full_56 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_55, full_57);
  --control_reg_56, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_56 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_56 <= std_logic'('0');
        else
          full_56 <= p56_full_56;
        end if;
      end if;
    end if;

  end process;

  --data_55, which is an e_mux
  p55_stage_55 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_56 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_56);
  --data_reg_55, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_55 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_55))))) = '1' then 
        if std_logic'(((sync_reset AND full_55) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_56))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_55 <= std_logic_vector'("0000");
        else
          stage_55 <= p55_stage_55;
        end if;
      end if;
    end if;

  end process;

  --control_55, which is an e_mux
  p55_full_55 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_54, full_56);
  --control_reg_55, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_55 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_55 <= std_logic'('0');
        else
          full_55 <= p55_full_55;
        end if;
      end if;
    end if;

  end process;

  --data_54, which is an e_mux
  p54_stage_54 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_55 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_55);
  --data_reg_54, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_54 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_54))))) = '1' then 
        if std_logic'(((sync_reset AND full_54) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_55))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_54 <= std_logic_vector'("0000");
        else
          stage_54 <= p54_stage_54;
        end if;
      end if;
    end if;

  end process;

  --control_54, which is an e_mux
  p54_full_54 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_53, full_55);
  --control_reg_54, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_54 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_54 <= std_logic'('0');
        else
          full_54 <= p54_full_54;
        end if;
      end if;
    end if;

  end process;

  --data_53, which is an e_mux
  p53_stage_53 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_54 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_54);
  --data_reg_53, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_53 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_53))))) = '1' then 
        if std_logic'(((sync_reset AND full_53) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_54))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_53 <= std_logic_vector'("0000");
        else
          stage_53 <= p53_stage_53;
        end if;
      end if;
    end if;

  end process;

  --control_53, which is an e_mux
  p53_full_53 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_52, full_54);
  --control_reg_53, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_53 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_53 <= std_logic'('0');
        else
          full_53 <= p53_full_53;
        end if;
      end if;
    end if;

  end process;

  --data_52, which is an e_mux
  p52_stage_52 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_53 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_53);
  --data_reg_52, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_52 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_52))))) = '1' then 
        if std_logic'(((sync_reset AND full_52) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_53))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_52 <= std_logic_vector'("0000");
        else
          stage_52 <= p52_stage_52;
        end if;
      end if;
    end if;

  end process;

  --control_52, which is an e_mux
  p52_full_52 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_51, full_53);
  --control_reg_52, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_52 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_52 <= std_logic'('0');
        else
          full_52 <= p52_full_52;
        end if;
      end if;
    end if;

  end process;

  --data_51, which is an e_mux
  p51_stage_51 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_52 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_52);
  --data_reg_51, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_51 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_51))))) = '1' then 
        if std_logic'(((sync_reset AND full_51) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_52))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_51 <= std_logic_vector'("0000");
        else
          stage_51 <= p51_stage_51;
        end if;
      end if;
    end if;

  end process;

  --control_51, which is an e_mux
  p51_full_51 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_50, full_52);
  --control_reg_51, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_51 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_51 <= std_logic'('0');
        else
          full_51 <= p51_full_51;
        end if;
      end if;
    end if;

  end process;

  --data_50, which is an e_mux
  p50_stage_50 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_51 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_51);
  --data_reg_50, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_50 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_50))))) = '1' then 
        if std_logic'(((sync_reset AND full_50) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_51))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_50 <= std_logic_vector'("0000");
        else
          stage_50 <= p50_stage_50;
        end if;
      end if;
    end if;

  end process;

  --control_50, which is an e_mux
  p50_full_50 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_49, full_51);
  --control_reg_50, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_50 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_50 <= std_logic'('0');
        else
          full_50 <= p50_full_50;
        end if;
      end if;
    end if;

  end process;

  --data_49, which is an e_mux
  p49_stage_49 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_50 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_50);
  --data_reg_49, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_49 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_49))))) = '1' then 
        if std_logic'(((sync_reset AND full_49) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_50))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_49 <= std_logic_vector'("0000");
        else
          stage_49 <= p49_stage_49;
        end if;
      end if;
    end if;

  end process;

  --control_49, which is an e_mux
  p49_full_49 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_48, full_50);
  --control_reg_49, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_49 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_49 <= std_logic'('0');
        else
          full_49 <= p49_full_49;
        end if;
      end if;
    end if;

  end process;

  --data_48, which is an e_mux
  p48_stage_48 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_49 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_49);
  --data_reg_48, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_48 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_48))))) = '1' then 
        if std_logic'(((sync_reset AND full_48) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_49))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_48 <= std_logic_vector'("0000");
        else
          stage_48 <= p48_stage_48;
        end if;
      end if;
    end if;

  end process;

  --control_48, which is an e_mux
  p48_full_48 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_47, full_49);
  --control_reg_48, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_48 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_48 <= std_logic'('0');
        else
          full_48 <= p48_full_48;
        end if;
      end if;
    end if;

  end process;

  --data_47, which is an e_mux
  p47_stage_47 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_48 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_48);
  --data_reg_47, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_47 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_47))))) = '1' then 
        if std_logic'(((sync_reset AND full_47) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_48))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_47 <= std_logic_vector'("0000");
        else
          stage_47 <= p47_stage_47;
        end if;
      end if;
    end if;

  end process;

  --control_47, which is an e_mux
  p47_full_47 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_46, full_48);
  --control_reg_47, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_47 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_47 <= std_logic'('0');
        else
          full_47 <= p47_full_47;
        end if;
      end if;
    end if;

  end process;

  --data_46, which is an e_mux
  p46_stage_46 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_47 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_47);
  --data_reg_46, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_46 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_46))))) = '1' then 
        if std_logic'(((sync_reset AND full_46) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_47))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_46 <= std_logic_vector'("0000");
        else
          stage_46 <= p46_stage_46;
        end if;
      end if;
    end if;

  end process;

  --control_46, which is an e_mux
  p46_full_46 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_45, full_47);
  --control_reg_46, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_46 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_46 <= std_logic'('0');
        else
          full_46 <= p46_full_46;
        end if;
      end if;
    end if;

  end process;

  --data_45, which is an e_mux
  p45_stage_45 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_46 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_46);
  --data_reg_45, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_45 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_45))))) = '1' then 
        if std_logic'(((sync_reset AND full_45) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_46))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_45 <= std_logic_vector'("0000");
        else
          stage_45 <= p45_stage_45;
        end if;
      end if;
    end if;

  end process;

  --control_45, which is an e_mux
  p45_full_45 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_44, full_46);
  --control_reg_45, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_45 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_45 <= std_logic'('0');
        else
          full_45 <= p45_full_45;
        end if;
      end if;
    end if;

  end process;

  --data_44, which is an e_mux
  p44_stage_44 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_45 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_45);
  --data_reg_44, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_44 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_44))))) = '1' then 
        if std_logic'(((sync_reset AND full_44) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_45))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_44 <= std_logic_vector'("0000");
        else
          stage_44 <= p44_stage_44;
        end if;
      end if;
    end if;

  end process;

  --control_44, which is an e_mux
  p44_full_44 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_43, full_45);
  --control_reg_44, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_44 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_44 <= std_logic'('0');
        else
          full_44 <= p44_full_44;
        end if;
      end if;
    end if;

  end process;

  --data_43, which is an e_mux
  p43_stage_43 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_44 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_44);
  --data_reg_43, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_43 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_43))))) = '1' then 
        if std_logic'(((sync_reset AND full_43) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_44))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_43 <= std_logic_vector'("0000");
        else
          stage_43 <= p43_stage_43;
        end if;
      end if;
    end if;

  end process;

  --control_43, which is an e_mux
  p43_full_43 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_42, full_44);
  --control_reg_43, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_43 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_43 <= std_logic'('0');
        else
          full_43 <= p43_full_43;
        end if;
      end if;
    end if;

  end process;

  --data_42, which is an e_mux
  p42_stage_42 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_43 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_43);
  --data_reg_42, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_42 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_42))))) = '1' then 
        if std_logic'(((sync_reset AND full_42) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_43))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_42 <= std_logic_vector'("0000");
        else
          stage_42 <= p42_stage_42;
        end if;
      end if;
    end if;

  end process;

  --control_42, which is an e_mux
  p42_full_42 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_41, full_43);
  --control_reg_42, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_42 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_42 <= std_logic'('0');
        else
          full_42 <= p42_full_42;
        end if;
      end if;
    end if;

  end process;

  --data_41, which is an e_mux
  p41_stage_41 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_42 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_42);
  --data_reg_41, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_41 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_41))))) = '1' then 
        if std_logic'(((sync_reset AND full_41) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_42))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_41 <= std_logic_vector'("0000");
        else
          stage_41 <= p41_stage_41;
        end if;
      end if;
    end if;

  end process;

  --control_41, which is an e_mux
  p41_full_41 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_40, full_42);
  --control_reg_41, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_41 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_41 <= std_logic'('0');
        else
          full_41 <= p41_full_41;
        end if;
      end if;
    end if;

  end process;

  --data_40, which is an e_mux
  p40_stage_40 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_41 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_41);
  --data_reg_40, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_40 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_40))))) = '1' then 
        if std_logic'(((sync_reset AND full_40) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_41))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_40 <= std_logic_vector'("0000");
        else
          stage_40 <= p40_stage_40;
        end if;
      end if;
    end if;

  end process;

  --control_40, which is an e_mux
  p40_full_40 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_39, full_41);
  --control_reg_40, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_40 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_40 <= std_logic'('0');
        else
          full_40 <= p40_full_40;
        end if;
      end if;
    end if;

  end process;

  --data_39, which is an e_mux
  p39_stage_39 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_40 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_40);
  --data_reg_39, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_39 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_39))))) = '1' then 
        if std_logic'(((sync_reset AND full_39) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_40))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_39 <= std_logic_vector'("0000");
        else
          stage_39 <= p39_stage_39;
        end if;
      end if;
    end if;

  end process;

  --control_39, which is an e_mux
  p39_full_39 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_38, full_40);
  --control_reg_39, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_39 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_39 <= std_logic'('0');
        else
          full_39 <= p39_full_39;
        end if;
      end if;
    end if;

  end process;

  --data_38, which is an e_mux
  p38_stage_38 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_39 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_39);
  --data_reg_38, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_38 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_38))))) = '1' then 
        if std_logic'(((sync_reset AND full_38) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_39))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_38 <= std_logic_vector'("0000");
        else
          stage_38 <= p38_stage_38;
        end if;
      end if;
    end if;

  end process;

  --control_38, which is an e_mux
  p38_full_38 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_37, full_39);
  --control_reg_38, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_38 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_38 <= std_logic'('0');
        else
          full_38 <= p38_full_38;
        end if;
      end if;
    end if;

  end process;

  --data_37, which is an e_mux
  p37_stage_37 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_38 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_38);
  --data_reg_37, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_37 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_37))))) = '1' then 
        if std_logic'(((sync_reset AND full_37) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_38))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_37 <= std_logic_vector'("0000");
        else
          stage_37 <= p37_stage_37;
        end if;
      end if;
    end if;

  end process;

  --control_37, which is an e_mux
  p37_full_37 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_36, full_38);
  --control_reg_37, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_37 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_37 <= std_logic'('0');
        else
          full_37 <= p37_full_37;
        end if;
      end if;
    end if;

  end process;

  --data_36, which is an e_mux
  p36_stage_36 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_37 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_37);
  --data_reg_36, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_36 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_36))))) = '1' then 
        if std_logic'(((sync_reset AND full_36) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_37))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_36 <= std_logic_vector'("0000");
        else
          stage_36 <= p36_stage_36;
        end if;
      end if;
    end if;

  end process;

  --control_36, which is an e_mux
  p36_full_36 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_35, full_37);
  --control_reg_36, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_36 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_36 <= std_logic'('0');
        else
          full_36 <= p36_full_36;
        end if;
      end if;
    end if;

  end process;

  --data_35, which is an e_mux
  p35_stage_35 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_36 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_36);
  --data_reg_35, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_35 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_35))))) = '1' then 
        if std_logic'(((sync_reset AND full_35) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_36))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_35 <= std_logic_vector'("0000");
        else
          stage_35 <= p35_stage_35;
        end if;
      end if;
    end if;

  end process;

  --control_35, which is an e_mux
  p35_full_35 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_34, full_36);
  --control_reg_35, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_35 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_35 <= std_logic'('0');
        else
          full_35 <= p35_full_35;
        end if;
      end if;
    end if;

  end process;

  --data_34, which is an e_mux
  p34_stage_34 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_35 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_35);
  --data_reg_34, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_34 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_34))))) = '1' then 
        if std_logic'(((sync_reset AND full_34) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_35))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_34 <= std_logic_vector'("0000");
        else
          stage_34 <= p34_stage_34;
        end if;
      end if;
    end if;

  end process;

  --control_34, which is an e_mux
  p34_full_34 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_33, full_35);
  --control_reg_34, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_34 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_34 <= std_logic'('0');
        else
          full_34 <= p34_full_34;
        end if;
      end if;
    end if;

  end process;

  --data_33, which is an e_mux
  p33_stage_33 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_34 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_34);
  --data_reg_33, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_33 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_33))))) = '1' then 
        if std_logic'(((sync_reset AND full_33) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_34))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_33 <= std_logic_vector'("0000");
        else
          stage_33 <= p33_stage_33;
        end if;
      end if;
    end if;

  end process;

  --control_33, which is an e_mux
  p33_full_33 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_32, full_34);
  --control_reg_33, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_33 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_33 <= std_logic'('0');
        else
          full_33 <= p33_full_33;
        end if;
      end if;
    end if;

  end process;

  --data_32, which is an e_mux
  p32_stage_32 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_33 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_33);
  --data_reg_32, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_32 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_32))))) = '1' then 
        if std_logic'(((sync_reset AND full_32) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_33))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_32 <= std_logic_vector'("0000");
        else
          stage_32 <= p32_stage_32;
        end if;
      end if;
    end if;

  end process;

  --control_32, which is an e_mux
  p32_full_32 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_31, full_33);
  --control_reg_32, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_32 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_32 <= std_logic'('0');
        else
          full_32 <= p32_full_32;
        end if;
      end if;
    end if;

  end process;

  --data_31, which is an e_mux
  p31_stage_31 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_32 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_32);
  --data_reg_31, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_31 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_31))))) = '1' then 
        if std_logic'(((sync_reset AND full_31) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_32))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_31 <= std_logic_vector'("0000");
        else
          stage_31 <= p31_stage_31;
        end if;
      end if;
    end if;

  end process;

  --control_31, which is an e_mux
  p31_full_31 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_30, full_32);
  --control_reg_31, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_31 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_31 <= std_logic'('0');
        else
          full_31 <= p31_full_31;
        end if;
      end if;
    end if;

  end process;

  --data_30, which is an e_mux
  p30_stage_30 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_31 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_31);
  --data_reg_30, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_30 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_30))))) = '1' then 
        if std_logic'(((sync_reset AND full_30) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_31))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_30 <= std_logic_vector'("0000");
        else
          stage_30 <= p30_stage_30;
        end if;
      end if;
    end if;

  end process;

  --control_30, which is an e_mux
  p30_full_30 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_29, full_31);
  --control_reg_30, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_30 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_30 <= std_logic'('0');
        else
          full_30 <= p30_full_30;
        end if;
      end if;
    end if;

  end process;

  --data_29, which is an e_mux
  p29_stage_29 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_30 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_30);
  --data_reg_29, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_29 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_29))))) = '1' then 
        if std_logic'(((sync_reset AND full_29) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_30))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_29 <= std_logic_vector'("0000");
        else
          stage_29 <= p29_stage_29;
        end if;
      end if;
    end if;

  end process;

  --control_29, which is an e_mux
  p29_full_29 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_28, full_30);
  --control_reg_29, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_29 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_29 <= std_logic'('0');
        else
          full_29 <= p29_full_29;
        end if;
      end if;
    end if;

  end process;

  --data_28, which is an e_mux
  p28_stage_28 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_29 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_29);
  --data_reg_28, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_28 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_28))))) = '1' then 
        if std_logic'(((sync_reset AND full_28) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_29))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_28 <= std_logic_vector'("0000");
        else
          stage_28 <= p28_stage_28;
        end if;
      end if;
    end if;

  end process;

  --control_28, which is an e_mux
  p28_full_28 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_27, full_29);
  --control_reg_28, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_28 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_28 <= std_logic'('0');
        else
          full_28 <= p28_full_28;
        end if;
      end if;
    end if;

  end process;

  --data_27, which is an e_mux
  p27_stage_27 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_28 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_28);
  --data_reg_27, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_27 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_27))))) = '1' then 
        if std_logic'(((sync_reset AND full_27) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_28))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_27 <= std_logic_vector'("0000");
        else
          stage_27 <= p27_stage_27;
        end if;
      end if;
    end if;

  end process;

  --control_27, which is an e_mux
  p27_full_27 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_26, full_28);
  --control_reg_27, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_27 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_27 <= std_logic'('0');
        else
          full_27 <= p27_full_27;
        end if;
      end if;
    end if;

  end process;

  --data_26, which is an e_mux
  p26_stage_26 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_27 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_27);
  --data_reg_26, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_26 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_26))))) = '1' then 
        if std_logic'(((sync_reset AND full_26) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_27))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_26 <= std_logic_vector'("0000");
        else
          stage_26 <= p26_stage_26;
        end if;
      end if;
    end if;

  end process;

  --control_26, which is an e_mux
  p26_full_26 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_25, full_27);
  --control_reg_26, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_26 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_26 <= std_logic'('0');
        else
          full_26 <= p26_full_26;
        end if;
      end if;
    end if;

  end process;

  --data_25, which is an e_mux
  p25_stage_25 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_26 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_26);
  --data_reg_25, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_25 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_25))))) = '1' then 
        if std_logic'(((sync_reset AND full_25) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_26))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_25 <= std_logic_vector'("0000");
        else
          stage_25 <= p25_stage_25;
        end if;
      end if;
    end if;

  end process;

  --control_25, which is an e_mux
  p25_full_25 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_24, full_26);
  --control_reg_25, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_25 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_25 <= std_logic'('0');
        else
          full_25 <= p25_full_25;
        end if;
      end if;
    end if;

  end process;

  --data_24, which is an e_mux
  p24_stage_24 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_25 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_25);
  --data_reg_24, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_24 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_24))))) = '1' then 
        if std_logic'(((sync_reset AND full_24) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_25))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_24 <= std_logic_vector'("0000");
        else
          stage_24 <= p24_stage_24;
        end if;
      end if;
    end if;

  end process;

  --control_24, which is an e_mux
  p24_full_24 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_23, full_25);
  --control_reg_24, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_24 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_24 <= std_logic'('0');
        else
          full_24 <= p24_full_24;
        end if;
      end if;
    end if;

  end process;

  --data_23, which is an e_mux
  p23_stage_23 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_24 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_24);
  --data_reg_23, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_23 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_23))))) = '1' then 
        if std_logic'(((sync_reset AND full_23) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_24))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_23 <= std_logic_vector'("0000");
        else
          stage_23 <= p23_stage_23;
        end if;
      end if;
    end if;

  end process;

  --control_23, which is an e_mux
  p23_full_23 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_22, full_24);
  --control_reg_23, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_23 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_23 <= std_logic'('0');
        else
          full_23 <= p23_full_23;
        end if;
      end if;
    end if;

  end process;

  --data_22, which is an e_mux
  p22_stage_22 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_23 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_23);
  --data_reg_22, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_22 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_22))))) = '1' then 
        if std_logic'(((sync_reset AND full_22) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_23))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_22 <= std_logic_vector'("0000");
        else
          stage_22 <= p22_stage_22;
        end if;
      end if;
    end if;

  end process;

  --control_22, which is an e_mux
  p22_full_22 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_21, full_23);
  --control_reg_22, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_22 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_22 <= std_logic'('0');
        else
          full_22 <= p22_full_22;
        end if;
      end if;
    end if;

  end process;

  --data_21, which is an e_mux
  p21_stage_21 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_22 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_22);
  --data_reg_21, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_21 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_21))))) = '1' then 
        if std_logic'(((sync_reset AND full_21) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_22))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_21 <= std_logic_vector'("0000");
        else
          stage_21 <= p21_stage_21;
        end if;
      end if;
    end if;

  end process;

  --control_21, which is an e_mux
  p21_full_21 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_20, full_22);
  --control_reg_21, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_21 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_21 <= std_logic'('0');
        else
          full_21 <= p21_full_21;
        end if;
      end if;
    end if;

  end process;

  --data_20, which is an e_mux
  p20_stage_20 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_21 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_21);
  --data_reg_20, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_20 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_20))))) = '1' then 
        if std_logic'(((sync_reset AND full_20) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_21))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_20 <= std_logic_vector'("0000");
        else
          stage_20 <= p20_stage_20;
        end if;
      end if;
    end if;

  end process;

  --control_20, which is an e_mux
  p20_full_20 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_19, full_21);
  --control_reg_20, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_20 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_20 <= std_logic'('0');
        else
          full_20 <= p20_full_20;
        end if;
      end if;
    end if;

  end process;

  --data_19, which is an e_mux
  p19_stage_19 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_20 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_20);
  --data_reg_19, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_19 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_19))))) = '1' then 
        if std_logic'(((sync_reset AND full_19) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_20))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_19 <= std_logic_vector'("0000");
        else
          stage_19 <= p19_stage_19;
        end if;
      end if;
    end if;

  end process;

  --control_19, which is an e_mux
  p19_full_19 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_18, full_20);
  --control_reg_19, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_19 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_19 <= std_logic'('0');
        else
          full_19 <= p19_full_19;
        end if;
      end if;
    end if;

  end process;

  --data_18, which is an e_mux
  p18_stage_18 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_19 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_19);
  --data_reg_18, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_18 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_18))))) = '1' then 
        if std_logic'(((sync_reset AND full_18) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_19))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_18 <= std_logic_vector'("0000");
        else
          stage_18 <= p18_stage_18;
        end if;
      end if;
    end if;

  end process;

  --control_18, which is an e_mux
  p18_full_18 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_17, full_19);
  --control_reg_18, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_18 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_18 <= std_logic'('0');
        else
          full_18 <= p18_full_18;
        end if;
      end if;
    end if;

  end process;

  --data_17, which is an e_mux
  p17_stage_17 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_18 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_18);
  --data_reg_17, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_17 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_17))))) = '1' then 
        if std_logic'(((sync_reset AND full_17) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_18))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_17 <= std_logic_vector'("0000");
        else
          stage_17 <= p17_stage_17;
        end if;
      end if;
    end if;

  end process;

  --control_17, which is an e_mux
  p17_full_17 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_16, full_18);
  --control_reg_17, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_17 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_17 <= std_logic'('0');
        else
          full_17 <= p17_full_17;
        end if;
      end if;
    end if;

  end process;

  --data_16, which is an e_mux
  p16_stage_16 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_17 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_17);
  --data_reg_16, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_16 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_16))))) = '1' then 
        if std_logic'(((sync_reset AND full_16) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_17))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_16 <= std_logic_vector'("0000");
        else
          stage_16 <= p16_stage_16;
        end if;
      end if;
    end if;

  end process;

  --control_16, which is an e_mux
  p16_full_16 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_15, full_17);
  --control_reg_16, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_16 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_16 <= std_logic'('0');
        else
          full_16 <= p16_full_16;
        end if;
      end if;
    end if;

  end process;

  --data_15, which is an e_mux
  p15_stage_15 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_16 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_16);
  --data_reg_15, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_15 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_15))))) = '1' then 
        if std_logic'(((sync_reset AND full_15) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_16))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_15 <= std_logic_vector'("0000");
        else
          stage_15 <= p15_stage_15;
        end if;
      end if;
    end if;

  end process;

  --control_15, which is an e_mux
  p15_full_15 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_14, full_16);
  --control_reg_15, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_15 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_15 <= std_logic'('0');
        else
          full_15 <= p15_full_15;
        end if;
      end if;
    end if;

  end process;

  --data_14, which is an e_mux
  p14_stage_14 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_15 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_15);
  --data_reg_14, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_14 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_14))))) = '1' then 
        if std_logic'(((sync_reset AND full_14) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_15))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_14 <= std_logic_vector'("0000");
        else
          stage_14 <= p14_stage_14;
        end if;
      end if;
    end if;

  end process;

  --control_14, which is an e_mux
  p14_full_14 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_13, full_15);
  --control_reg_14, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_14 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_14 <= std_logic'('0');
        else
          full_14 <= p14_full_14;
        end if;
      end if;
    end if;

  end process;

  --data_13, which is an e_mux
  p13_stage_13 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_14 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_14);
  --data_reg_13, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_13 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_13))))) = '1' then 
        if std_logic'(((sync_reset AND full_13) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_14))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_13 <= std_logic_vector'("0000");
        else
          stage_13 <= p13_stage_13;
        end if;
      end if;
    end if;

  end process;

  --control_13, which is an e_mux
  p13_full_13 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_12, full_14);
  --control_reg_13, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_13 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_13 <= std_logic'('0');
        else
          full_13 <= p13_full_13;
        end if;
      end if;
    end if;

  end process;

  --data_12, which is an e_mux
  p12_stage_12 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_13 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_13);
  --data_reg_12, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_12 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_12))))) = '1' then 
        if std_logic'(((sync_reset AND full_12) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_13))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_12 <= std_logic_vector'("0000");
        else
          stage_12 <= p12_stage_12;
        end if;
      end if;
    end if;

  end process;

  --control_12, which is an e_mux
  p12_full_12 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_11, full_13);
  --control_reg_12, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_12 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_12 <= std_logic'('0');
        else
          full_12 <= p12_full_12;
        end if;
      end if;
    end if;

  end process;

  --data_11, which is an e_mux
  p11_stage_11 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_12 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_12);
  --data_reg_11, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_11 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_11))))) = '1' then 
        if std_logic'(((sync_reset AND full_11) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_12))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_11 <= std_logic_vector'("0000");
        else
          stage_11 <= p11_stage_11;
        end if;
      end if;
    end if;

  end process;

  --control_11, which is an e_mux
  p11_full_11 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_10, full_12);
  --control_reg_11, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_11 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_11 <= std_logic'('0');
        else
          full_11 <= p11_full_11;
        end if;
      end if;
    end if;

  end process;

  --data_10, which is an e_mux
  p10_stage_10 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_11 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_11);
  --data_reg_10, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_10 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_10))))) = '1' then 
        if std_logic'(((sync_reset AND full_10) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_11))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_10 <= std_logic_vector'("0000");
        else
          stage_10 <= p10_stage_10;
        end if;
      end if;
    end if;

  end process;

  --control_10, which is an e_mux
  p10_full_10 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_9, full_11);
  --control_reg_10, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_10 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_10 <= std_logic'('0');
        else
          full_10 <= p10_full_10;
        end if;
      end if;
    end if;

  end process;

  --data_9, which is an e_mux
  p9_stage_9 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_10 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_10);
  --data_reg_9, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_9 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_9))))) = '1' then 
        if std_logic'(((sync_reset AND full_9) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_10))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_9 <= std_logic_vector'("0000");
        else
          stage_9 <= p9_stage_9;
        end if;
      end if;
    end if;

  end process;

  --control_9, which is an e_mux
  p9_full_9 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_8, full_10);
  --control_reg_9, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_9 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_9 <= std_logic'('0');
        else
          full_9 <= p9_full_9;
        end if;
      end if;
    end if;

  end process;

  --data_8, which is an e_mux
  p8_stage_8 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_9 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_9);
  --data_reg_8, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_8 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_8))))) = '1' then 
        if std_logic'(((sync_reset AND full_8) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_9))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_8 <= std_logic_vector'("0000");
        else
          stage_8 <= p8_stage_8;
        end if;
      end if;
    end if;

  end process;

  --control_8, which is an e_mux
  p8_full_8 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_7, full_9);
  --control_reg_8, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_8 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_8 <= std_logic'('0');
        else
          full_8 <= p8_full_8;
        end if;
      end if;
    end if;

  end process;

  --data_7, which is an e_mux
  p7_stage_7 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_8 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_8);
  --data_reg_7, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_7 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_7))))) = '1' then 
        if std_logic'(((sync_reset AND full_7) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_8))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_7 <= std_logic_vector'("0000");
        else
          stage_7 <= p7_stage_7;
        end if;
      end if;
    end if;

  end process;

  --control_7, which is an e_mux
  p7_full_7 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_6, full_8);
  --control_reg_7, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_7 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_7 <= std_logic'('0');
        else
          full_7 <= p7_full_7;
        end if;
      end if;
    end if;

  end process;

  --data_6, which is an e_mux
  p6_stage_6 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_7 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_7);
  --data_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_6 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_6))))) = '1' then 
        if std_logic'(((sync_reset AND full_6) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_7))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_6 <= std_logic_vector'("0000");
        else
          stage_6 <= p6_stage_6;
        end if;
      end if;
    end if;

  end process;

  --control_6, which is an e_mux
  p6_full_6 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_5, full_7);
  --control_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_6 <= std_logic'('0');
        else
          full_6 <= p6_full_6;
        end if;
      end if;
    end if;

  end process;

  --data_5, which is an e_mux
  p5_stage_5 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_6 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_6);
  --data_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_5 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_5))))) = '1' then 
        if std_logic'(((sync_reset AND full_5) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_6))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_5 <= std_logic_vector'("0000");
        else
          stage_5 <= p5_stage_5;
        end if;
      end if;
    end if;

  end process;

  --control_5, which is an e_mux
  p5_full_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_4, full_6);
  --control_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_5 <= std_logic'('0');
        else
          full_5 <= p5_full_5;
        end if;
      end if;
    end if;

  end process;

  --data_4, which is an e_mux
  p4_stage_4 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_5 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_5);
  --data_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_4 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_4))))) = '1' then 
        if std_logic'(((sync_reset AND full_4) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_5))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_4 <= std_logic_vector'("0000");
        else
          stage_4 <= p4_stage_4;
        end if;
      end if;
    end if;

  end process;

  --control_4, which is an e_mux
  p4_full_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_3, full_5);
  --control_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_4 <= std_logic'('0');
        else
          full_4 <= p4_full_4;
        end if;
      end if;
    end if;

  end process;

  --data_3, which is an e_mux
  p3_stage_3 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_4 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_4);
  --data_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_3 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_3))))) = '1' then 
        if std_logic'(((sync_reset AND full_3) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_4))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_3 <= std_logic_vector'("0000");
        else
          stage_3 <= p3_stage_3;
        end if;
      end if;
    end if;

  end process;

  --control_3, which is an e_mux
  p3_full_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_2, full_4);
  --control_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_3 <= std_logic'('0');
        else
          full_3 <= p3_full_3;
        end if;
      end if;
    end if;

  end process;

  --data_2, which is an e_mux
  p2_stage_2 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_3 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_3);
  --data_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_2 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_2))))) = '1' then 
        if std_logic'(((sync_reset AND full_2) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_3))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_2 <= std_logic_vector'("0000");
        else
          stage_2 <= p2_stage_2;
        end if;
      end if;
    end if;

  end process;

  --control_2, which is an e_mux
  p2_full_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_1, full_3);
  --control_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_2 <= std_logic'('0');
        else
          full_2 <= p2_full_2;
        end if;
      end if;
    end if;

  end process;

  --data_1, which is an e_mux
  p1_stage_1 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_2 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_2);
  --data_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_1 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_1))))) = '1' then 
        if std_logic'(((sync_reset AND full_1) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_2))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_1 <= std_logic_vector'("0000");
        else
          stage_1 <= p1_stage_1;
        end if;
      end if;
    end if;

  end process;

  --control_1, which is an e_mux
  p1_full_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_0, full_2);
  --control_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_1 <= std_logic'('0');
        else
          full_1 <= p1_full_1;
        end if;
      end if;
    end if;

  end process;

  --data_0, which is an e_mux
  p0_stage_0 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_1 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_1);
  --data_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_0 <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(((sync_reset AND full_0) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_0 <= std_logic_vector'("0000");
        else
          stage_0 <= p0_stage_0;
        end if;
      end if;
    end if;

  end process;

  --control_0, which is an e_mux
  p0_full_0 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), std_logic_vector'("00000000000000000000000000000001"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1)))));
  --control_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'((clear_fifo AND NOT write)) = '1' then 
          full_0 <= std_logic'('0');
        else
          full_0 <= p0_full_0;
        end if;
      end if;
    end if;

  end process;

  one_count_plus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000") & (how_many_ones)) + std_logic_vector'("000000000000000000000000000000001")), 8);
  one_count_minus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000") & (how_many_ones)) - std_logic_vector'("000000000000000000000000000000001")), 8);
  --updated_one_count, which is an e_mux
  updated_one_count <= A_EXT (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND NOT(write)))) = '1'), std_logic_vector'("00000000000000000000000000000000"), (std_logic_vector'("000000000000000000000000") & (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND write))) = '1'), (std_logic_vector'("0000000") & (A_TOSTDLOGICVECTOR(or_reduce(data_in)))), A_WE_StdLogicVector((std_logic'(((((read AND (or_reduce(data_in))) AND write) AND (or_reduce(stage_0))))) = '1'), how_many_ones, A_WE_StdLogicVector((std_logic'(((write AND (or_reduce(data_in))))) = '1'), one_count_plus_one, A_WE_StdLogicVector((std_logic'(((read AND (or_reduce(stage_0))))) = '1'), one_count_minus_one, how_many_ones))))))), 8);
  --counts how many ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      how_many_ones <= std_logic_vector'("00000000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        how_many_ones <= updated_one_count;
      end if;
    end if;

  end process;

  --this fifo contains ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      fifo_contains_ones_n <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        fifo_contains_ones_n <= NOT (or_reduce(updated_one_count));
      end if;
    end if;

  end process;


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity rdv_fifo_for_master_read_avalon_master_to_clock_crossing_read_s1_module is 
        port (
              -- inputs:
                 signal clear_fifo : IN STD_LOGIC;
                 signal clk : IN STD_LOGIC;
                 signal data_in : IN STD_LOGIC;
                 signal read : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal sync_reset : IN STD_LOGIC;
                 signal write : IN STD_LOGIC;

              -- outputs:
                 signal data_out : OUT STD_LOGIC;
                 signal empty : OUT STD_LOGIC;
                 signal fifo_contains_ones_n : OUT STD_LOGIC;
                 signal full : OUT STD_LOGIC
              );
end entity rdv_fifo_for_master_read_avalon_master_to_clock_crossing_read_s1_module;


architecture europa of rdv_fifo_for_master_read_avalon_master_to_clock_crossing_read_s1_module is
                signal full_0 :  STD_LOGIC;
                signal full_1 :  STD_LOGIC;
                signal full_10 :  STD_LOGIC;
                signal full_11 :  STD_LOGIC;
                signal full_12 :  STD_LOGIC;
                signal full_13 :  STD_LOGIC;
                signal full_14 :  STD_LOGIC;
                signal full_15 :  STD_LOGIC;
                signal full_16 :  STD_LOGIC;
                signal full_17 :  STD_LOGIC;
                signal full_18 :  STD_LOGIC;
                signal full_19 :  STD_LOGIC;
                signal full_2 :  STD_LOGIC;
                signal full_20 :  STD_LOGIC;
                signal full_21 :  STD_LOGIC;
                signal full_22 :  STD_LOGIC;
                signal full_23 :  STD_LOGIC;
                signal full_24 :  STD_LOGIC;
                signal full_25 :  STD_LOGIC;
                signal full_26 :  STD_LOGIC;
                signal full_27 :  STD_LOGIC;
                signal full_28 :  STD_LOGIC;
                signal full_29 :  STD_LOGIC;
                signal full_3 :  STD_LOGIC;
                signal full_30 :  STD_LOGIC;
                signal full_31 :  STD_LOGIC;
                signal full_32 :  STD_LOGIC;
                signal full_33 :  STD_LOGIC;
                signal full_34 :  STD_LOGIC;
                signal full_35 :  STD_LOGIC;
                signal full_36 :  STD_LOGIC;
                signal full_37 :  STD_LOGIC;
                signal full_38 :  STD_LOGIC;
                signal full_39 :  STD_LOGIC;
                signal full_4 :  STD_LOGIC;
                signal full_40 :  STD_LOGIC;
                signal full_41 :  STD_LOGIC;
                signal full_42 :  STD_LOGIC;
                signal full_43 :  STD_LOGIC;
                signal full_44 :  STD_LOGIC;
                signal full_45 :  STD_LOGIC;
                signal full_46 :  STD_LOGIC;
                signal full_47 :  STD_LOGIC;
                signal full_48 :  STD_LOGIC;
                signal full_49 :  STD_LOGIC;
                signal full_5 :  STD_LOGIC;
                signal full_50 :  STD_LOGIC;
                signal full_51 :  STD_LOGIC;
                signal full_52 :  STD_LOGIC;
                signal full_53 :  STD_LOGIC;
                signal full_54 :  STD_LOGIC;
                signal full_55 :  STD_LOGIC;
                signal full_56 :  STD_LOGIC;
                signal full_57 :  STD_LOGIC;
                signal full_58 :  STD_LOGIC;
                signal full_59 :  STD_LOGIC;
                signal full_6 :  STD_LOGIC;
                signal full_60 :  STD_LOGIC;
                signal full_61 :  STD_LOGIC;
                signal full_62 :  STD_LOGIC;
                signal full_63 :  STD_LOGIC;
                signal full_64 :  STD_LOGIC;
                signal full_65 :  STD_LOGIC;
                signal full_66 :  STD_LOGIC;
                signal full_67 :  STD_LOGIC;
                signal full_68 :  STD_LOGIC;
                signal full_69 :  STD_LOGIC;
                signal full_7 :  STD_LOGIC;
                signal full_70 :  STD_LOGIC;
                signal full_71 :  STD_LOGIC;
                signal full_72 :  STD_LOGIC;
                signal full_73 :  STD_LOGIC;
                signal full_8 :  STD_LOGIC;
                signal full_9 :  STD_LOGIC;
                signal how_many_ones :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal one_count_minus_one :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal one_count_plus_one :  STD_LOGIC_VECTOR (7 DOWNTO 0);
                signal p0_full_0 :  STD_LOGIC;
                signal p0_stage_0 :  STD_LOGIC;
                signal p10_full_10 :  STD_LOGIC;
                signal p10_stage_10 :  STD_LOGIC;
                signal p11_full_11 :  STD_LOGIC;
                signal p11_stage_11 :  STD_LOGIC;
                signal p12_full_12 :  STD_LOGIC;
                signal p12_stage_12 :  STD_LOGIC;
                signal p13_full_13 :  STD_LOGIC;
                signal p13_stage_13 :  STD_LOGIC;
                signal p14_full_14 :  STD_LOGIC;
                signal p14_stage_14 :  STD_LOGIC;
                signal p15_full_15 :  STD_LOGIC;
                signal p15_stage_15 :  STD_LOGIC;
                signal p16_full_16 :  STD_LOGIC;
                signal p16_stage_16 :  STD_LOGIC;
                signal p17_full_17 :  STD_LOGIC;
                signal p17_stage_17 :  STD_LOGIC;
                signal p18_full_18 :  STD_LOGIC;
                signal p18_stage_18 :  STD_LOGIC;
                signal p19_full_19 :  STD_LOGIC;
                signal p19_stage_19 :  STD_LOGIC;
                signal p1_full_1 :  STD_LOGIC;
                signal p1_stage_1 :  STD_LOGIC;
                signal p20_full_20 :  STD_LOGIC;
                signal p20_stage_20 :  STD_LOGIC;
                signal p21_full_21 :  STD_LOGIC;
                signal p21_stage_21 :  STD_LOGIC;
                signal p22_full_22 :  STD_LOGIC;
                signal p22_stage_22 :  STD_LOGIC;
                signal p23_full_23 :  STD_LOGIC;
                signal p23_stage_23 :  STD_LOGIC;
                signal p24_full_24 :  STD_LOGIC;
                signal p24_stage_24 :  STD_LOGIC;
                signal p25_full_25 :  STD_LOGIC;
                signal p25_stage_25 :  STD_LOGIC;
                signal p26_full_26 :  STD_LOGIC;
                signal p26_stage_26 :  STD_LOGIC;
                signal p27_full_27 :  STD_LOGIC;
                signal p27_stage_27 :  STD_LOGIC;
                signal p28_full_28 :  STD_LOGIC;
                signal p28_stage_28 :  STD_LOGIC;
                signal p29_full_29 :  STD_LOGIC;
                signal p29_stage_29 :  STD_LOGIC;
                signal p2_full_2 :  STD_LOGIC;
                signal p2_stage_2 :  STD_LOGIC;
                signal p30_full_30 :  STD_LOGIC;
                signal p30_stage_30 :  STD_LOGIC;
                signal p31_full_31 :  STD_LOGIC;
                signal p31_stage_31 :  STD_LOGIC;
                signal p32_full_32 :  STD_LOGIC;
                signal p32_stage_32 :  STD_LOGIC;
                signal p33_full_33 :  STD_LOGIC;
                signal p33_stage_33 :  STD_LOGIC;
                signal p34_full_34 :  STD_LOGIC;
                signal p34_stage_34 :  STD_LOGIC;
                signal p35_full_35 :  STD_LOGIC;
                signal p35_stage_35 :  STD_LOGIC;
                signal p36_full_36 :  STD_LOGIC;
                signal p36_stage_36 :  STD_LOGIC;
                signal p37_full_37 :  STD_LOGIC;
                signal p37_stage_37 :  STD_LOGIC;
                signal p38_full_38 :  STD_LOGIC;
                signal p38_stage_38 :  STD_LOGIC;
                signal p39_full_39 :  STD_LOGIC;
                signal p39_stage_39 :  STD_LOGIC;
                signal p3_full_3 :  STD_LOGIC;
                signal p3_stage_3 :  STD_LOGIC;
                signal p40_full_40 :  STD_LOGIC;
                signal p40_stage_40 :  STD_LOGIC;
                signal p41_full_41 :  STD_LOGIC;
                signal p41_stage_41 :  STD_LOGIC;
                signal p42_full_42 :  STD_LOGIC;
                signal p42_stage_42 :  STD_LOGIC;
                signal p43_full_43 :  STD_LOGIC;
                signal p43_stage_43 :  STD_LOGIC;
                signal p44_full_44 :  STD_LOGIC;
                signal p44_stage_44 :  STD_LOGIC;
                signal p45_full_45 :  STD_LOGIC;
                signal p45_stage_45 :  STD_LOGIC;
                signal p46_full_46 :  STD_LOGIC;
                signal p46_stage_46 :  STD_LOGIC;
                signal p47_full_47 :  STD_LOGIC;
                signal p47_stage_47 :  STD_LOGIC;
                signal p48_full_48 :  STD_LOGIC;
                signal p48_stage_48 :  STD_LOGIC;
                signal p49_full_49 :  STD_LOGIC;
                signal p49_stage_49 :  STD_LOGIC;
                signal p4_full_4 :  STD_LOGIC;
                signal p4_stage_4 :  STD_LOGIC;
                signal p50_full_50 :  STD_LOGIC;
                signal p50_stage_50 :  STD_LOGIC;
                signal p51_full_51 :  STD_LOGIC;
                signal p51_stage_51 :  STD_LOGIC;
                signal p52_full_52 :  STD_LOGIC;
                signal p52_stage_52 :  STD_LOGIC;
                signal p53_full_53 :  STD_LOGIC;
                signal p53_stage_53 :  STD_LOGIC;
                signal p54_full_54 :  STD_LOGIC;
                signal p54_stage_54 :  STD_LOGIC;
                signal p55_full_55 :  STD_LOGIC;
                signal p55_stage_55 :  STD_LOGIC;
                signal p56_full_56 :  STD_LOGIC;
                signal p56_stage_56 :  STD_LOGIC;
                signal p57_full_57 :  STD_LOGIC;
                signal p57_stage_57 :  STD_LOGIC;
                signal p58_full_58 :  STD_LOGIC;
                signal p58_stage_58 :  STD_LOGIC;
                signal p59_full_59 :  STD_LOGIC;
                signal p59_stage_59 :  STD_LOGIC;
                signal p5_full_5 :  STD_LOGIC;
                signal p5_stage_5 :  STD_LOGIC;
                signal p60_full_60 :  STD_LOGIC;
                signal p60_stage_60 :  STD_LOGIC;
                signal p61_full_61 :  STD_LOGIC;
                signal p61_stage_61 :  STD_LOGIC;
                signal p62_full_62 :  STD_LOGIC;
                signal p62_stage_62 :  STD_LOGIC;
                signal p63_full_63 :  STD_LOGIC;
                signal p63_stage_63 :  STD_LOGIC;
                signal p64_full_64 :  STD_LOGIC;
                signal p64_stage_64 :  STD_LOGIC;
                signal p65_full_65 :  STD_LOGIC;
                signal p65_stage_65 :  STD_LOGIC;
                signal p66_full_66 :  STD_LOGIC;
                signal p66_stage_66 :  STD_LOGIC;
                signal p67_full_67 :  STD_LOGIC;
                signal p67_stage_67 :  STD_LOGIC;
                signal p68_full_68 :  STD_LOGIC;
                signal p68_stage_68 :  STD_LOGIC;
                signal p69_full_69 :  STD_LOGIC;
                signal p69_stage_69 :  STD_LOGIC;
                signal p6_full_6 :  STD_LOGIC;
                signal p6_stage_6 :  STD_LOGIC;
                signal p70_full_70 :  STD_LOGIC;
                signal p70_stage_70 :  STD_LOGIC;
                signal p71_full_71 :  STD_LOGIC;
                signal p71_stage_71 :  STD_LOGIC;
                signal p72_full_72 :  STD_LOGIC;
                signal p72_stage_72 :  STD_LOGIC;
                signal p7_full_7 :  STD_LOGIC;
                signal p7_stage_7 :  STD_LOGIC;
                signal p8_full_8 :  STD_LOGIC;
                signal p8_stage_8 :  STD_LOGIC;
                signal p9_full_9 :  STD_LOGIC;
                signal p9_stage_9 :  STD_LOGIC;
                signal stage_0 :  STD_LOGIC;
                signal stage_1 :  STD_LOGIC;
                signal stage_10 :  STD_LOGIC;
                signal stage_11 :  STD_LOGIC;
                signal stage_12 :  STD_LOGIC;
                signal stage_13 :  STD_LOGIC;
                signal stage_14 :  STD_LOGIC;
                signal stage_15 :  STD_LOGIC;
                signal stage_16 :  STD_LOGIC;
                signal stage_17 :  STD_LOGIC;
                signal stage_18 :  STD_LOGIC;
                signal stage_19 :  STD_LOGIC;
                signal stage_2 :  STD_LOGIC;
                signal stage_20 :  STD_LOGIC;
                signal stage_21 :  STD_LOGIC;
                signal stage_22 :  STD_LOGIC;
                signal stage_23 :  STD_LOGIC;
                signal stage_24 :  STD_LOGIC;
                signal stage_25 :  STD_LOGIC;
                signal stage_26 :  STD_LOGIC;
                signal stage_27 :  STD_LOGIC;
                signal stage_28 :  STD_LOGIC;
                signal stage_29 :  STD_LOGIC;
                signal stage_3 :  STD_LOGIC;
                signal stage_30 :  STD_LOGIC;
                signal stage_31 :  STD_LOGIC;
                signal stage_32 :  STD_LOGIC;
                signal stage_33 :  STD_LOGIC;
                signal stage_34 :  STD_LOGIC;
                signal stage_35 :  STD_LOGIC;
                signal stage_36 :  STD_LOGIC;
                signal stage_37 :  STD_LOGIC;
                signal stage_38 :  STD_LOGIC;
                signal stage_39 :  STD_LOGIC;
                signal stage_4 :  STD_LOGIC;
                signal stage_40 :  STD_LOGIC;
                signal stage_41 :  STD_LOGIC;
                signal stage_42 :  STD_LOGIC;
                signal stage_43 :  STD_LOGIC;
                signal stage_44 :  STD_LOGIC;
                signal stage_45 :  STD_LOGIC;
                signal stage_46 :  STD_LOGIC;
                signal stage_47 :  STD_LOGIC;
                signal stage_48 :  STD_LOGIC;
                signal stage_49 :  STD_LOGIC;
                signal stage_5 :  STD_LOGIC;
                signal stage_50 :  STD_LOGIC;
                signal stage_51 :  STD_LOGIC;
                signal stage_52 :  STD_LOGIC;
                signal stage_53 :  STD_LOGIC;
                signal stage_54 :  STD_LOGIC;
                signal stage_55 :  STD_LOGIC;
                signal stage_56 :  STD_LOGIC;
                signal stage_57 :  STD_LOGIC;
                signal stage_58 :  STD_LOGIC;
                signal stage_59 :  STD_LOGIC;
                signal stage_6 :  STD_LOGIC;
                signal stage_60 :  STD_LOGIC;
                signal stage_61 :  STD_LOGIC;
                signal stage_62 :  STD_LOGIC;
                signal stage_63 :  STD_LOGIC;
                signal stage_64 :  STD_LOGIC;
                signal stage_65 :  STD_LOGIC;
                signal stage_66 :  STD_LOGIC;
                signal stage_67 :  STD_LOGIC;
                signal stage_68 :  STD_LOGIC;
                signal stage_69 :  STD_LOGIC;
                signal stage_7 :  STD_LOGIC;
                signal stage_70 :  STD_LOGIC;
                signal stage_71 :  STD_LOGIC;
                signal stage_72 :  STD_LOGIC;
                signal stage_8 :  STD_LOGIC;
                signal stage_9 :  STD_LOGIC;
                signal updated_one_count :  STD_LOGIC_VECTOR (7 DOWNTO 0);

begin

  data_out <= stage_0;
  full <= full_72;
  empty <= NOT(full_0);
  full_73 <= std_logic'('0');
  --data_72, which is an e_mux
  p72_stage_72 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_73 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, data_in);
  --data_reg_72, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_72 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_72))))) = '1' then 
        if std_logic'(((sync_reset AND full_72) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_73))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_72 <= std_logic'('0');
        else
          stage_72 <= p72_stage_72;
        end if;
      end if;
    end if;

  end process;

  --control_72, which is an e_mux
  p72_full_72 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_71))), std_logic_vector'("00000000000000000000000000000000")));
  --control_reg_72, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_72 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_72 <= std_logic'('0');
        else
          full_72 <= p72_full_72;
        end if;
      end if;
    end if;

  end process;

  --data_71, which is an e_mux
  p71_stage_71 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_72 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_72);
  --data_reg_71, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_71 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_71))))) = '1' then 
        if std_logic'(((sync_reset AND full_71) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_72))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_71 <= std_logic'('0');
        else
          stage_71 <= p71_stage_71;
        end if;
      end if;
    end if;

  end process;

  --control_71, which is an e_mux
  p71_full_71 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_70, full_72);
  --control_reg_71, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_71 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_71 <= std_logic'('0');
        else
          full_71 <= p71_full_71;
        end if;
      end if;
    end if;

  end process;

  --data_70, which is an e_mux
  p70_stage_70 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_71 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_71);
  --data_reg_70, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_70 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_70))))) = '1' then 
        if std_logic'(((sync_reset AND full_70) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_71))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_70 <= std_logic'('0');
        else
          stage_70 <= p70_stage_70;
        end if;
      end if;
    end if;

  end process;

  --control_70, which is an e_mux
  p70_full_70 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_69, full_71);
  --control_reg_70, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_70 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_70 <= std_logic'('0');
        else
          full_70 <= p70_full_70;
        end if;
      end if;
    end if;

  end process;

  --data_69, which is an e_mux
  p69_stage_69 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_70 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_70);
  --data_reg_69, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_69 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_69))))) = '1' then 
        if std_logic'(((sync_reset AND full_69) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_70))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_69 <= std_logic'('0');
        else
          stage_69 <= p69_stage_69;
        end if;
      end if;
    end if;

  end process;

  --control_69, which is an e_mux
  p69_full_69 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_68, full_70);
  --control_reg_69, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_69 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_69 <= std_logic'('0');
        else
          full_69 <= p69_full_69;
        end if;
      end if;
    end if;

  end process;

  --data_68, which is an e_mux
  p68_stage_68 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_69 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_69);
  --data_reg_68, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_68 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_68))))) = '1' then 
        if std_logic'(((sync_reset AND full_68) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_69))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_68 <= std_logic'('0');
        else
          stage_68 <= p68_stage_68;
        end if;
      end if;
    end if;

  end process;

  --control_68, which is an e_mux
  p68_full_68 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_67, full_69);
  --control_reg_68, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_68 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_68 <= std_logic'('0');
        else
          full_68 <= p68_full_68;
        end if;
      end if;
    end if;

  end process;

  --data_67, which is an e_mux
  p67_stage_67 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_68 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_68);
  --data_reg_67, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_67 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_67))))) = '1' then 
        if std_logic'(((sync_reset AND full_67) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_68))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_67 <= std_logic'('0');
        else
          stage_67 <= p67_stage_67;
        end if;
      end if;
    end if;

  end process;

  --control_67, which is an e_mux
  p67_full_67 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_66, full_68);
  --control_reg_67, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_67 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_67 <= std_logic'('0');
        else
          full_67 <= p67_full_67;
        end if;
      end if;
    end if;

  end process;

  --data_66, which is an e_mux
  p66_stage_66 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_67 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_67);
  --data_reg_66, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_66 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_66))))) = '1' then 
        if std_logic'(((sync_reset AND full_66) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_67))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_66 <= std_logic'('0');
        else
          stage_66 <= p66_stage_66;
        end if;
      end if;
    end if;

  end process;

  --control_66, which is an e_mux
  p66_full_66 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_65, full_67);
  --control_reg_66, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_66 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_66 <= std_logic'('0');
        else
          full_66 <= p66_full_66;
        end if;
      end if;
    end if;

  end process;

  --data_65, which is an e_mux
  p65_stage_65 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_66 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_66);
  --data_reg_65, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_65 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_65))))) = '1' then 
        if std_logic'(((sync_reset AND full_65) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_66))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_65 <= std_logic'('0');
        else
          stage_65 <= p65_stage_65;
        end if;
      end if;
    end if;

  end process;

  --control_65, which is an e_mux
  p65_full_65 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_64, full_66);
  --control_reg_65, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_65 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_65 <= std_logic'('0');
        else
          full_65 <= p65_full_65;
        end if;
      end if;
    end if;

  end process;

  --data_64, which is an e_mux
  p64_stage_64 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_65 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_65);
  --data_reg_64, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_64 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_64))))) = '1' then 
        if std_logic'(((sync_reset AND full_64) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_65))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_64 <= std_logic'('0');
        else
          stage_64 <= p64_stage_64;
        end if;
      end if;
    end if;

  end process;

  --control_64, which is an e_mux
  p64_full_64 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_63, full_65);
  --control_reg_64, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_64 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_64 <= std_logic'('0');
        else
          full_64 <= p64_full_64;
        end if;
      end if;
    end if;

  end process;

  --data_63, which is an e_mux
  p63_stage_63 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_64 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_64);
  --data_reg_63, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_63 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_63))))) = '1' then 
        if std_logic'(((sync_reset AND full_63) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_64))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_63 <= std_logic'('0');
        else
          stage_63 <= p63_stage_63;
        end if;
      end if;
    end if;

  end process;

  --control_63, which is an e_mux
  p63_full_63 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_62, full_64);
  --control_reg_63, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_63 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_63 <= std_logic'('0');
        else
          full_63 <= p63_full_63;
        end if;
      end if;
    end if;

  end process;

  --data_62, which is an e_mux
  p62_stage_62 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_63 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_63);
  --data_reg_62, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_62 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_62))))) = '1' then 
        if std_logic'(((sync_reset AND full_62) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_63))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_62 <= std_logic'('0');
        else
          stage_62 <= p62_stage_62;
        end if;
      end if;
    end if;

  end process;

  --control_62, which is an e_mux
  p62_full_62 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_61, full_63);
  --control_reg_62, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_62 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_62 <= std_logic'('0');
        else
          full_62 <= p62_full_62;
        end if;
      end if;
    end if;

  end process;

  --data_61, which is an e_mux
  p61_stage_61 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_62 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_62);
  --data_reg_61, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_61 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_61))))) = '1' then 
        if std_logic'(((sync_reset AND full_61) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_62))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_61 <= std_logic'('0');
        else
          stage_61 <= p61_stage_61;
        end if;
      end if;
    end if;

  end process;

  --control_61, which is an e_mux
  p61_full_61 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_60, full_62);
  --control_reg_61, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_61 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_61 <= std_logic'('0');
        else
          full_61 <= p61_full_61;
        end if;
      end if;
    end if;

  end process;

  --data_60, which is an e_mux
  p60_stage_60 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_61 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_61);
  --data_reg_60, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_60 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_60))))) = '1' then 
        if std_logic'(((sync_reset AND full_60) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_61))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_60 <= std_logic'('0');
        else
          stage_60 <= p60_stage_60;
        end if;
      end if;
    end if;

  end process;

  --control_60, which is an e_mux
  p60_full_60 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_59, full_61);
  --control_reg_60, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_60 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_60 <= std_logic'('0');
        else
          full_60 <= p60_full_60;
        end if;
      end if;
    end if;

  end process;

  --data_59, which is an e_mux
  p59_stage_59 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_60 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_60);
  --data_reg_59, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_59 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_59))))) = '1' then 
        if std_logic'(((sync_reset AND full_59) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_60))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_59 <= std_logic'('0');
        else
          stage_59 <= p59_stage_59;
        end if;
      end if;
    end if;

  end process;

  --control_59, which is an e_mux
  p59_full_59 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_58, full_60);
  --control_reg_59, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_59 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_59 <= std_logic'('0');
        else
          full_59 <= p59_full_59;
        end if;
      end if;
    end if;

  end process;

  --data_58, which is an e_mux
  p58_stage_58 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_59 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_59);
  --data_reg_58, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_58 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_58))))) = '1' then 
        if std_logic'(((sync_reset AND full_58) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_59))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_58 <= std_logic'('0');
        else
          stage_58 <= p58_stage_58;
        end if;
      end if;
    end if;

  end process;

  --control_58, which is an e_mux
  p58_full_58 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_57, full_59);
  --control_reg_58, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_58 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_58 <= std_logic'('0');
        else
          full_58 <= p58_full_58;
        end if;
      end if;
    end if;

  end process;

  --data_57, which is an e_mux
  p57_stage_57 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_58 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_58);
  --data_reg_57, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_57 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_57))))) = '1' then 
        if std_logic'(((sync_reset AND full_57) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_58))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_57 <= std_logic'('0');
        else
          stage_57 <= p57_stage_57;
        end if;
      end if;
    end if;

  end process;

  --control_57, which is an e_mux
  p57_full_57 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_56, full_58);
  --control_reg_57, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_57 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_57 <= std_logic'('0');
        else
          full_57 <= p57_full_57;
        end if;
      end if;
    end if;

  end process;

  --data_56, which is an e_mux
  p56_stage_56 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_57 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_57);
  --data_reg_56, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_56 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_56))))) = '1' then 
        if std_logic'(((sync_reset AND full_56) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_57))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_56 <= std_logic'('0');
        else
          stage_56 <= p56_stage_56;
        end if;
      end if;
    end if;

  end process;

  --control_56, which is an e_mux
  p56_full_56 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_55, full_57);
  --control_reg_56, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_56 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_56 <= std_logic'('0');
        else
          full_56 <= p56_full_56;
        end if;
      end if;
    end if;

  end process;

  --data_55, which is an e_mux
  p55_stage_55 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_56 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_56);
  --data_reg_55, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_55 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_55))))) = '1' then 
        if std_logic'(((sync_reset AND full_55) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_56))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_55 <= std_logic'('0');
        else
          stage_55 <= p55_stage_55;
        end if;
      end if;
    end if;

  end process;

  --control_55, which is an e_mux
  p55_full_55 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_54, full_56);
  --control_reg_55, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_55 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_55 <= std_logic'('0');
        else
          full_55 <= p55_full_55;
        end if;
      end if;
    end if;

  end process;

  --data_54, which is an e_mux
  p54_stage_54 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_55 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_55);
  --data_reg_54, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_54 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_54))))) = '1' then 
        if std_logic'(((sync_reset AND full_54) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_55))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_54 <= std_logic'('0');
        else
          stage_54 <= p54_stage_54;
        end if;
      end if;
    end if;

  end process;

  --control_54, which is an e_mux
  p54_full_54 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_53, full_55);
  --control_reg_54, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_54 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_54 <= std_logic'('0');
        else
          full_54 <= p54_full_54;
        end if;
      end if;
    end if;

  end process;

  --data_53, which is an e_mux
  p53_stage_53 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_54 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_54);
  --data_reg_53, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_53 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_53))))) = '1' then 
        if std_logic'(((sync_reset AND full_53) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_54))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_53 <= std_logic'('0');
        else
          stage_53 <= p53_stage_53;
        end if;
      end if;
    end if;

  end process;

  --control_53, which is an e_mux
  p53_full_53 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_52, full_54);
  --control_reg_53, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_53 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_53 <= std_logic'('0');
        else
          full_53 <= p53_full_53;
        end if;
      end if;
    end if;

  end process;

  --data_52, which is an e_mux
  p52_stage_52 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_53 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_53);
  --data_reg_52, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_52 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_52))))) = '1' then 
        if std_logic'(((sync_reset AND full_52) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_53))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_52 <= std_logic'('0');
        else
          stage_52 <= p52_stage_52;
        end if;
      end if;
    end if;

  end process;

  --control_52, which is an e_mux
  p52_full_52 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_51, full_53);
  --control_reg_52, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_52 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_52 <= std_logic'('0');
        else
          full_52 <= p52_full_52;
        end if;
      end if;
    end if;

  end process;

  --data_51, which is an e_mux
  p51_stage_51 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_52 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_52);
  --data_reg_51, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_51 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_51))))) = '1' then 
        if std_logic'(((sync_reset AND full_51) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_52))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_51 <= std_logic'('0');
        else
          stage_51 <= p51_stage_51;
        end if;
      end if;
    end if;

  end process;

  --control_51, which is an e_mux
  p51_full_51 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_50, full_52);
  --control_reg_51, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_51 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_51 <= std_logic'('0');
        else
          full_51 <= p51_full_51;
        end if;
      end if;
    end if;

  end process;

  --data_50, which is an e_mux
  p50_stage_50 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_51 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_51);
  --data_reg_50, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_50 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_50))))) = '1' then 
        if std_logic'(((sync_reset AND full_50) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_51))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_50 <= std_logic'('0');
        else
          stage_50 <= p50_stage_50;
        end if;
      end if;
    end if;

  end process;

  --control_50, which is an e_mux
  p50_full_50 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_49, full_51);
  --control_reg_50, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_50 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_50 <= std_logic'('0');
        else
          full_50 <= p50_full_50;
        end if;
      end if;
    end if;

  end process;

  --data_49, which is an e_mux
  p49_stage_49 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_50 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_50);
  --data_reg_49, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_49 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_49))))) = '1' then 
        if std_logic'(((sync_reset AND full_49) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_50))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_49 <= std_logic'('0');
        else
          stage_49 <= p49_stage_49;
        end if;
      end if;
    end if;

  end process;

  --control_49, which is an e_mux
  p49_full_49 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_48, full_50);
  --control_reg_49, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_49 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_49 <= std_logic'('0');
        else
          full_49 <= p49_full_49;
        end if;
      end if;
    end if;

  end process;

  --data_48, which is an e_mux
  p48_stage_48 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_49 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_49);
  --data_reg_48, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_48 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_48))))) = '1' then 
        if std_logic'(((sync_reset AND full_48) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_49))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_48 <= std_logic'('0');
        else
          stage_48 <= p48_stage_48;
        end if;
      end if;
    end if;

  end process;

  --control_48, which is an e_mux
  p48_full_48 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_47, full_49);
  --control_reg_48, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_48 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_48 <= std_logic'('0');
        else
          full_48 <= p48_full_48;
        end if;
      end if;
    end if;

  end process;

  --data_47, which is an e_mux
  p47_stage_47 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_48 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_48);
  --data_reg_47, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_47 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_47))))) = '1' then 
        if std_logic'(((sync_reset AND full_47) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_48))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_47 <= std_logic'('0');
        else
          stage_47 <= p47_stage_47;
        end if;
      end if;
    end if;

  end process;

  --control_47, which is an e_mux
  p47_full_47 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_46, full_48);
  --control_reg_47, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_47 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_47 <= std_logic'('0');
        else
          full_47 <= p47_full_47;
        end if;
      end if;
    end if;

  end process;

  --data_46, which is an e_mux
  p46_stage_46 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_47 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_47);
  --data_reg_46, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_46 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_46))))) = '1' then 
        if std_logic'(((sync_reset AND full_46) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_47))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_46 <= std_logic'('0');
        else
          stage_46 <= p46_stage_46;
        end if;
      end if;
    end if;

  end process;

  --control_46, which is an e_mux
  p46_full_46 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_45, full_47);
  --control_reg_46, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_46 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_46 <= std_logic'('0');
        else
          full_46 <= p46_full_46;
        end if;
      end if;
    end if;

  end process;

  --data_45, which is an e_mux
  p45_stage_45 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_46 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_46);
  --data_reg_45, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_45 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_45))))) = '1' then 
        if std_logic'(((sync_reset AND full_45) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_46))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_45 <= std_logic'('0');
        else
          stage_45 <= p45_stage_45;
        end if;
      end if;
    end if;

  end process;

  --control_45, which is an e_mux
  p45_full_45 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_44, full_46);
  --control_reg_45, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_45 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_45 <= std_logic'('0');
        else
          full_45 <= p45_full_45;
        end if;
      end if;
    end if;

  end process;

  --data_44, which is an e_mux
  p44_stage_44 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_45 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_45);
  --data_reg_44, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_44 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_44))))) = '1' then 
        if std_logic'(((sync_reset AND full_44) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_45))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_44 <= std_logic'('0');
        else
          stage_44 <= p44_stage_44;
        end if;
      end if;
    end if;

  end process;

  --control_44, which is an e_mux
  p44_full_44 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_43, full_45);
  --control_reg_44, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_44 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_44 <= std_logic'('0');
        else
          full_44 <= p44_full_44;
        end if;
      end if;
    end if;

  end process;

  --data_43, which is an e_mux
  p43_stage_43 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_44 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_44);
  --data_reg_43, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_43 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_43))))) = '1' then 
        if std_logic'(((sync_reset AND full_43) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_44))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_43 <= std_logic'('0');
        else
          stage_43 <= p43_stage_43;
        end if;
      end if;
    end if;

  end process;

  --control_43, which is an e_mux
  p43_full_43 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_42, full_44);
  --control_reg_43, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_43 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_43 <= std_logic'('0');
        else
          full_43 <= p43_full_43;
        end if;
      end if;
    end if;

  end process;

  --data_42, which is an e_mux
  p42_stage_42 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_43 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_43);
  --data_reg_42, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_42 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_42))))) = '1' then 
        if std_logic'(((sync_reset AND full_42) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_43))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_42 <= std_logic'('0');
        else
          stage_42 <= p42_stage_42;
        end if;
      end if;
    end if;

  end process;

  --control_42, which is an e_mux
  p42_full_42 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_41, full_43);
  --control_reg_42, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_42 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_42 <= std_logic'('0');
        else
          full_42 <= p42_full_42;
        end if;
      end if;
    end if;

  end process;

  --data_41, which is an e_mux
  p41_stage_41 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_42 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_42);
  --data_reg_41, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_41 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_41))))) = '1' then 
        if std_logic'(((sync_reset AND full_41) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_42))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_41 <= std_logic'('0');
        else
          stage_41 <= p41_stage_41;
        end if;
      end if;
    end if;

  end process;

  --control_41, which is an e_mux
  p41_full_41 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_40, full_42);
  --control_reg_41, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_41 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_41 <= std_logic'('0');
        else
          full_41 <= p41_full_41;
        end if;
      end if;
    end if;

  end process;

  --data_40, which is an e_mux
  p40_stage_40 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_41 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_41);
  --data_reg_40, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_40 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_40))))) = '1' then 
        if std_logic'(((sync_reset AND full_40) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_41))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_40 <= std_logic'('0');
        else
          stage_40 <= p40_stage_40;
        end if;
      end if;
    end if;

  end process;

  --control_40, which is an e_mux
  p40_full_40 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_39, full_41);
  --control_reg_40, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_40 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_40 <= std_logic'('0');
        else
          full_40 <= p40_full_40;
        end if;
      end if;
    end if;

  end process;

  --data_39, which is an e_mux
  p39_stage_39 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_40 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_40);
  --data_reg_39, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_39 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_39))))) = '1' then 
        if std_logic'(((sync_reset AND full_39) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_40))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_39 <= std_logic'('0');
        else
          stage_39 <= p39_stage_39;
        end if;
      end if;
    end if;

  end process;

  --control_39, which is an e_mux
  p39_full_39 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_38, full_40);
  --control_reg_39, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_39 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_39 <= std_logic'('0');
        else
          full_39 <= p39_full_39;
        end if;
      end if;
    end if;

  end process;

  --data_38, which is an e_mux
  p38_stage_38 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_39 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_39);
  --data_reg_38, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_38 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_38))))) = '1' then 
        if std_logic'(((sync_reset AND full_38) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_39))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_38 <= std_logic'('0');
        else
          stage_38 <= p38_stage_38;
        end if;
      end if;
    end if;

  end process;

  --control_38, which is an e_mux
  p38_full_38 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_37, full_39);
  --control_reg_38, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_38 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_38 <= std_logic'('0');
        else
          full_38 <= p38_full_38;
        end if;
      end if;
    end if;

  end process;

  --data_37, which is an e_mux
  p37_stage_37 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_38 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_38);
  --data_reg_37, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_37 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_37))))) = '1' then 
        if std_logic'(((sync_reset AND full_37) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_38))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_37 <= std_logic'('0');
        else
          stage_37 <= p37_stage_37;
        end if;
      end if;
    end if;

  end process;

  --control_37, which is an e_mux
  p37_full_37 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_36, full_38);
  --control_reg_37, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_37 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_37 <= std_logic'('0');
        else
          full_37 <= p37_full_37;
        end if;
      end if;
    end if;

  end process;

  --data_36, which is an e_mux
  p36_stage_36 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_37 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_37);
  --data_reg_36, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_36 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_36))))) = '1' then 
        if std_logic'(((sync_reset AND full_36) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_37))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_36 <= std_logic'('0');
        else
          stage_36 <= p36_stage_36;
        end if;
      end if;
    end if;

  end process;

  --control_36, which is an e_mux
  p36_full_36 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_35, full_37);
  --control_reg_36, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_36 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_36 <= std_logic'('0');
        else
          full_36 <= p36_full_36;
        end if;
      end if;
    end if;

  end process;

  --data_35, which is an e_mux
  p35_stage_35 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_36 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_36);
  --data_reg_35, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_35 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_35))))) = '1' then 
        if std_logic'(((sync_reset AND full_35) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_36))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_35 <= std_logic'('0');
        else
          stage_35 <= p35_stage_35;
        end if;
      end if;
    end if;

  end process;

  --control_35, which is an e_mux
  p35_full_35 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_34, full_36);
  --control_reg_35, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_35 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_35 <= std_logic'('0');
        else
          full_35 <= p35_full_35;
        end if;
      end if;
    end if;

  end process;

  --data_34, which is an e_mux
  p34_stage_34 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_35 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_35);
  --data_reg_34, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_34 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_34))))) = '1' then 
        if std_logic'(((sync_reset AND full_34) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_35))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_34 <= std_logic'('0');
        else
          stage_34 <= p34_stage_34;
        end if;
      end if;
    end if;

  end process;

  --control_34, which is an e_mux
  p34_full_34 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_33, full_35);
  --control_reg_34, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_34 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_34 <= std_logic'('0');
        else
          full_34 <= p34_full_34;
        end if;
      end if;
    end if;

  end process;

  --data_33, which is an e_mux
  p33_stage_33 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_34 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_34);
  --data_reg_33, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_33 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_33))))) = '1' then 
        if std_logic'(((sync_reset AND full_33) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_34))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_33 <= std_logic'('0');
        else
          stage_33 <= p33_stage_33;
        end if;
      end if;
    end if;

  end process;

  --control_33, which is an e_mux
  p33_full_33 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_32, full_34);
  --control_reg_33, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_33 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_33 <= std_logic'('0');
        else
          full_33 <= p33_full_33;
        end if;
      end if;
    end if;

  end process;

  --data_32, which is an e_mux
  p32_stage_32 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_33 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_33);
  --data_reg_32, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_32 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_32))))) = '1' then 
        if std_logic'(((sync_reset AND full_32) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_33))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_32 <= std_logic'('0');
        else
          stage_32 <= p32_stage_32;
        end if;
      end if;
    end if;

  end process;

  --control_32, which is an e_mux
  p32_full_32 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_31, full_33);
  --control_reg_32, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_32 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_32 <= std_logic'('0');
        else
          full_32 <= p32_full_32;
        end if;
      end if;
    end if;

  end process;

  --data_31, which is an e_mux
  p31_stage_31 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_32 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_32);
  --data_reg_31, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_31 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_31))))) = '1' then 
        if std_logic'(((sync_reset AND full_31) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_32))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_31 <= std_logic'('0');
        else
          stage_31 <= p31_stage_31;
        end if;
      end if;
    end if;

  end process;

  --control_31, which is an e_mux
  p31_full_31 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_30, full_32);
  --control_reg_31, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_31 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_31 <= std_logic'('0');
        else
          full_31 <= p31_full_31;
        end if;
      end if;
    end if;

  end process;

  --data_30, which is an e_mux
  p30_stage_30 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_31 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_31);
  --data_reg_30, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_30 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_30))))) = '1' then 
        if std_logic'(((sync_reset AND full_30) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_31))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_30 <= std_logic'('0');
        else
          stage_30 <= p30_stage_30;
        end if;
      end if;
    end if;

  end process;

  --control_30, which is an e_mux
  p30_full_30 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_29, full_31);
  --control_reg_30, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_30 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_30 <= std_logic'('0');
        else
          full_30 <= p30_full_30;
        end if;
      end if;
    end if;

  end process;

  --data_29, which is an e_mux
  p29_stage_29 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_30 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_30);
  --data_reg_29, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_29 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_29))))) = '1' then 
        if std_logic'(((sync_reset AND full_29) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_30))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_29 <= std_logic'('0');
        else
          stage_29 <= p29_stage_29;
        end if;
      end if;
    end if;

  end process;

  --control_29, which is an e_mux
  p29_full_29 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_28, full_30);
  --control_reg_29, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_29 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_29 <= std_logic'('0');
        else
          full_29 <= p29_full_29;
        end if;
      end if;
    end if;

  end process;

  --data_28, which is an e_mux
  p28_stage_28 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_29 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_29);
  --data_reg_28, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_28 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_28))))) = '1' then 
        if std_logic'(((sync_reset AND full_28) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_29))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_28 <= std_logic'('0');
        else
          stage_28 <= p28_stage_28;
        end if;
      end if;
    end if;

  end process;

  --control_28, which is an e_mux
  p28_full_28 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_27, full_29);
  --control_reg_28, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_28 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_28 <= std_logic'('0');
        else
          full_28 <= p28_full_28;
        end if;
      end if;
    end if;

  end process;

  --data_27, which is an e_mux
  p27_stage_27 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_28 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_28);
  --data_reg_27, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_27 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_27))))) = '1' then 
        if std_logic'(((sync_reset AND full_27) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_28))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_27 <= std_logic'('0');
        else
          stage_27 <= p27_stage_27;
        end if;
      end if;
    end if;

  end process;

  --control_27, which is an e_mux
  p27_full_27 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_26, full_28);
  --control_reg_27, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_27 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_27 <= std_logic'('0');
        else
          full_27 <= p27_full_27;
        end if;
      end if;
    end if;

  end process;

  --data_26, which is an e_mux
  p26_stage_26 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_27 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_27);
  --data_reg_26, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_26 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_26))))) = '1' then 
        if std_logic'(((sync_reset AND full_26) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_27))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_26 <= std_logic'('0');
        else
          stage_26 <= p26_stage_26;
        end if;
      end if;
    end if;

  end process;

  --control_26, which is an e_mux
  p26_full_26 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_25, full_27);
  --control_reg_26, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_26 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_26 <= std_logic'('0');
        else
          full_26 <= p26_full_26;
        end if;
      end if;
    end if;

  end process;

  --data_25, which is an e_mux
  p25_stage_25 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_26 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_26);
  --data_reg_25, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_25 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_25))))) = '1' then 
        if std_logic'(((sync_reset AND full_25) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_26))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_25 <= std_logic'('0');
        else
          stage_25 <= p25_stage_25;
        end if;
      end if;
    end if;

  end process;

  --control_25, which is an e_mux
  p25_full_25 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_24, full_26);
  --control_reg_25, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_25 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_25 <= std_logic'('0');
        else
          full_25 <= p25_full_25;
        end if;
      end if;
    end if;

  end process;

  --data_24, which is an e_mux
  p24_stage_24 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_25 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_25);
  --data_reg_24, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_24 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_24))))) = '1' then 
        if std_logic'(((sync_reset AND full_24) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_25))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_24 <= std_logic'('0');
        else
          stage_24 <= p24_stage_24;
        end if;
      end if;
    end if;

  end process;

  --control_24, which is an e_mux
  p24_full_24 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_23, full_25);
  --control_reg_24, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_24 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_24 <= std_logic'('0');
        else
          full_24 <= p24_full_24;
        end if;
      end if;
    end if;

  end process;

  --data_23, which is an e_mux
  p23_stage_23 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_24 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_24);
  --data_reg_23, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_23 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_23))))) = '1' then 
        if std_logic'(((sync_reset AND full_23) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_24))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_23 <= std_logic'('0');
        else
          stage_23 <= p23_stage_23;
        end if;
      end if;
    end if;

  end process;

  --control_23, which is an e_mux
  p23_full_23 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_22, full_24);
  --control_reg_23, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_23 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_23 <= std_logic'('0');
        else
          full_23 <= p23_full_23;
        end if;
      end if;
    end if;

  end process;

  --data_22, which is an e_mux
  p22_stage_22 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_23 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_23);
  --data_reg_22, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_22 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_22))))) = '1' then 
        if std_logic'(((sync_reset AND full_22) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_23))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_22 <= std_logic'('0');
        else
          stage_22 <= p22_stage_22;
        end if;
      end if;
    end if;

  end process;

  --control_22, which is an e_mux
  p22_full_22 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_21, full_23);
  --control_reg_22, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_22 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_22 <= std_logic'('0');
        else
          full_22 <= p22_full_22;
        end if;
      end if;
    end if;

  end process;

  --data_21, which is an e_mux
  p21_stage_21 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_22 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_22);
  --data_reg_21, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_21 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_21))))) = '1' then 
        if std_logic'(((sync_reset AND full_21) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_22))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_21 <= std_logic'('0');
        else
          stage_21 <= p21_stage_21;
        end if;
      end if;
    end if;

  end process;

  --control_21, which is an e_mux
  p21_full_21 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_20, full_22);
  --control_reg_21, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_21 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_21 <= std_logic'('0');
        else
          full_21 <= p21_full_21;
        end if;
      end if;
    end if;

  end process;

  --data_20, which is an e_mux
  p20_stage_20 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_21 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_21);
  --data_reg_20, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_20 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_20))))) = '1' then 
        if std_logic'(((sync_reset AND full_20) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_21))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_20 <= std_logic'('0');
        else
          stage_20 <= p20_stage_20;
        end if;
      end if;
    end if;

  end process;

  --control_20, which is an e_mux
  p20_full_20 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_19, full_21);
  --control_reg_20, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_20 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_20 <= std_logic'('0');
        else
          full_20 <= p20_full_20;
        end if;
      end if;
    end if;

  end process;

  --data_19, which is an e_mux
  p19_stage_19 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_20 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_20);
  --data_reg_19, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_19 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_19))))) = '1' then 
        if std_logic'(((sync_reset AND full_19) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_20))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_19 <= std_logic'('0');
        else
          stage_19 <= p19_stage_19;
        end if;
      end if;
    end if;

  end process;

  --control_19, which is an e_mux
  p19_full_19 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_18, full_20);
  --control_reg_19, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_19 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_19 <= std_logic'('0');
        else
          full_19 <= p19_full_19;
        end if;
      end if;
    end if;

  end process;

  --data_18, which is an e_mux
  p18_stage_18 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_19 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_19);
  --data_reg_18, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_18 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_18))))) = '1' then 
        if std_logic'(((sync_reset AND full_18) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_19))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_18 <= std_logic'('0');
        else
          stage_18 <= p18_stage_18;
        end if;
      end if;
    end if;

  end process;

  --control_18, which is an e_mux
  p18_full_18 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_17, full_19);
  --control_reg_18, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_18 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_18 <= std_logic'('0');
        else
          full_18 <= p18_full_18;
        end if;
      end if;
    end if;

  end process;

  --data_17, which is an e_mux
  p17_stage_17 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_18 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_18);
  --data_reg_17, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_17 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_17))))) = '1' then 
        if std_logic'(((sync_reset AND full_17) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_18))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_17 <= std_logic'('0');
        else
          stage_17 <= p17_stage_17;
        end if;
      end if;
    end if;

  end process;

  --control_17, which is an e_mux
  p17_full_17 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_16, full_18);
  --control_reg_17, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_17 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_17 <= std_logic'('0');
        else
          full_17 <= p17_full_17;
        end if;
      end if;
    end if;

  end process;

  --data_16, which is an e_mux
  p16_stage_16 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_17 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_17);
  --data_reg_16, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_16 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_16))))) = '1' then 
        if std_logic'(((sync_reset AND full_16) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_17))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_16 <= std_logic'('0');
        else
          stage_16 <= p16_stage_16;
        end if;
      end if;
    end if;

  end process;

  --control_16, which is an e_mux
  p16_full_16 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_15, full_17);
  --control_reg_16, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_16 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_16 <= std_logic'('0');
        else
          full_16 <= p16_full_16;
        end if;
      end if;
    end if;

  end process;

  --data_15, which is an e_mux
  p15_stage_15 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_16 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_16);
  --data_reg_15, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_15 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_15))))) = '1' then 
        if std_logic'(((sync_reset AND full_15) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_16))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_15 <= std_logic'('0');
        else
          stage_15 <= p15_stage_15;
        end if;
      end if;
    end if;

  end process;

  --control_15, which is an e_mux
  p15_full_15 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_14, full_16);
  --control_reg_15, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_15 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_15 <= std_logic'('0');
        else
          full_15 <= p15_full_15;
        end if;
      end if;
    end if;

  end process;

  --data_14, which is an e_mux
  p14_stage_14 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_15 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_15);
  --data_reg_14, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_14 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_14))))) = '1' then 
        if std_logic'(((sync_reset AND full_14) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_15))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_14 <= std_logic'('0');
        else
          stage_14 <= p14_stage_14;
        end if;
      end if;
    end if;

  end process;

  --control_14, which is an e_mux
  p14_full_14 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_13, full_15);
  --control_reg_14, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_14 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_14 <= std_logic'('0');
        else
          full_14 <= p14_full_14;
        end if;
      end if;
    end if;

  end process;

  --data_13, which is an e_mux
  p13_stage_13 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_14 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_14);
  --data_reg_13, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_13 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_13))))) = '1' then 
        if std_logic'(((sync_reset AND full_13) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_14))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_13 <= std_logic'('0');
        else
          stage_13 <= p13_stage_13;
        end if;
      end if;
    end if;

  end process;

  --control_13, which is an e_mux
  p13_full_13 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_12, full_14);
  --control_reg_13, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_13 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_13 <= std_logic'('0');
        else
          full_13 <= p13_full_13;
        end if;
      end if;
    end if;

  end process;

  --data_12, which is an e_mux
  p12_stage_12 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_13 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_13);
  --data_reg_12, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_12 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_12))))) = '1' then 
        if std_logic'(((sync_reset AND full_12) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_13))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_12 <= std_logic'('0');
        else
          stage_12 <= p12_stage_12;
        end if;
      end if;
    end if;

  end process;

  --control_12, which is an e_mux
  p12_full_12 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_11, full_13);
  --control_reg_12, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_12 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_12 <= std_logic'('0');
        else
          full_12 <= p12_full_12;
        end if;
      end if;
    end if;

  end process;

  --data_11, which is an e_mux
  p11_stage_11 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_12 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_12);
  --data_reg_11, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_11 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_11))))) = '1' then 
        if std_logic'(((sync_reset AND full_11) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_12))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_11 <= std_logic'('0');
        else
          stage_11 <= p11_stage_11;
        end if;
      end if;
    end if;

  end process;

  --control_11, which is an e_mux
  p11_full_11 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_10, full_12);
  --control_reg_11, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_11 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_11 <= std_logic'('0');
        else
          full_11 <= p11_full_11;
        end if;
      end if;
    end if;

  end process;

  --data_10, which is an e_mux
  p10_stage_10 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_11 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_11);
  --data_reg_10, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_10 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_10))))) = '1' then 
        if std_logic'(((sync_reset AND full_10) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_11))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_10 <= std_logic'('0');
        else
          stage_10 <= p10_stage_10;
        end if;
      end if;
    end if;

  end process;

  --control_10, which is an e_mux
  p10_full_10 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_9, full_11);
  --control_reg_10, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_10 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_10 <= std_logic'('0');
        else
          full_10 <= p10_full_10;
        end if;
      end if;
    end if;

  end process;

  --data_9, which is an e_mux
  p9_stage_9 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_10 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_10);
  --data_reg_9, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_9 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_9))))) = '1' then 
        if std_logic'(((sync_reset AND full_9) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_10))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_9 <= std_logic'('0');
        else
          stage_9 <= p9_stage_9;
        end if;
      end if;
    end if;

  end process;

  --control_9, which is an e_mux
  p9_full_9 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_8, full_10);
  --control_reg_9, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_9 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_9 <= std_logic'('0');
        else
          full_9 <= p9_full_9;
        end if;
      end if;
    end if;

  end process;

  --data_8, which is an e_mux
  p8_stage_8 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_9 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_9);
  --data_reg_8, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_8 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_8))))) = '1' then 
        if std_logic'(((sync_reset AND full_8) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_9))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_8 <= std_logic'('0');
        else
          stage_8 <= p8_stage_8;
        end if;
      end if;
    end if;

  end process;

  --control_8, which is an e_mux
  p8_full_8 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_7, full_9);
  --control_reg_8, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_8 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_8 <= std_logic'('0');
        else
          full_8 <= p8_full_8;
        end if;
      end if;
    end if;

  end process;

  --data_7, which is an e_mux
  p7_stage_7 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_8 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_8);
  --data_reg_7, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_7 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_7))))) = '1' then 
        if std_logic'(((sync_reset AND full_7) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_8))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_7 <= std_logic'('0');
        else
          stage_7 <= p7_stage_7;
        end if;
      end if;
    end if;

  end process;

  --control_7, which is an e_mux
  p7_full_7 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_6, full_8);
  --control_reg_7, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_7 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_7 <= std_logic'('0');
        else
          full_7 <= p7_full_7;
        end if;
      end if;
    end if;

  end process;

  --data_6, which is an e_mux
  p6_stage_6 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_7 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_7);
  --data_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_6))))) = '1' then 
        if std_logic'(((sync_reset AND full_6) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_7))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_6 <= std_logic'('0');
        else
          stage_6 <= p6_stage_6;
        end if;
      end if;
    end if;

  end process;

  --control_6, which is an e_mux
  p6_full_6 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_5, full_7);
  --control_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_6 <= std_logic'('0');
        else
          full_6 <= p6_full_6;
        end if;
      end if;
    end if;

  end process;

  --data_5, which is an e_mux
  p5_stage_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_6 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_6);
  --data_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_5))))) = '1' then 
        if std_logic'(((sync_reset AND full_5) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_6))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_5 <= std_logic'('0');
        else
          stage_5 <= p5_stage_5;
        end if;
      end if;
    end if;

  end process;

  --control_5, which is an e_mux
  p5_full_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_4, full_6);
  --control_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_5 <= std_logic'('0');
        else
          full_5 <= p5_full_5;
        end if;
      end if;
    end if;

  end process;

  --data_4, which is an e_mux
  p4_stage_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_5 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_5);
  --data_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_4))))) = '1' then 
        if std_logic'(((sync_reset AND full_4) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_5))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_4 <= std_logic'('0');
        else
          stage_4 <= p4_stage_4;
        end if;
      end if;
    end if;

  end process;

  --control_4, which is an e_mux
  p4_full_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_3, full_5);
  --control_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_4 <= std_logic'('0');
        else
          full_4 <= p4_full_4;
        end if;
      end if;
    end if;

  end process;

  --data_3, which is an e_mux
  p3_stage_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_4 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_4);
  --data_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_3))))) = '1' then 
        if std_logic'(((sync_reset AND full_3) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_4))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_3 <= std_logic'('0');
        else
          stage_3 <= p3_stage_3;
        end if;
      end if;
    end if;

  end process;

  --control_3, which is an e_mux
  p3_full_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_2, full_4);
  --control_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_3 <= std_logic'('0');
        else
          full_3 <= p3_full_3;
        end if;
      end if;
    end if;

  end process;

  --data_2, which is an e_mux
  p2_stage_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_3 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_3);
  --data_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_2))))) = '1' then 
        if std_logic'(((sync_reset AND full_2) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_3))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_2 <= std_logic'('0');
        else
          stage_2 <= p2_stage_2;
        end if;
      end if;
    end if;

  end process;

  --control_2, which is an e_mux
  p2_full_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_1, full_3);
  --control_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_2 <= std_logic'('0');
        else
          full_2 <= p2_full_2;
        end if;
      end if;
    end if;

  end process;

  --data_1, which is an e_mux
  p1_stage_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_2 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_2);
  --data_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_1))))) = '1' then 
        if std_logic'(((sync_reset AND full_1) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_2))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_1 <= std_logic'('0');
        else
          stage_1 <= p1_stage_1;
        end if;
      end if;
    end if;

  end process;

  --control_1, which is an e_mux
  p1_full_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_0, full_2);
  --control_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_1 <= std_logic'('0');
        else
          full_1 <= p1_full_1;
        end if;
      end if;
    end if;

  end process;

  --data_0, which is an e_mux
  p0_stage_0 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_1 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_1);
  --data_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(((sync_reset AND full_0) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_0 <= std_logic'('0');
        else
          stage_0 <= p0_stage_0;
        end if;
      end if;
    end if;

  end process;

  --control_0, which is an e_mux
  p0_full_0 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), std_logic_vector'("00000000000000000000000000000001"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1)))));
  --control_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'((clear_fifo AND NOT write)) = '1' then 
          full_0 <= std_logic'('0');
        else
          full_0 <= p0_full_0;
        end if;
      end if;
    end if;

  end process;

  one_count_plus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000") & (how_many_ones)) + std_logic_vector'("000000000000000000000000000000001")), 8);
  one_count_minus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000") & (how_many_ones)) - std_logic_vector'("000000000000000000000000000000001")), 8);
  --updated_one_count, which is an e_mux
  updated_one_count <= A_EXT (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND NOT(write)))) = '1'), std_logic_vector'("00000000000000000000000000000000"), (std_logic_vector'("000000000000000000000000") & (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND write))) = '1'), (std_logic_vector'("0000000") & (A_TOSTDLOGICVECTOR(data_in))), A_WE_StdLogicVector((std_logic'(((((read AND (data_in)) AND write) AND (stage_0)))) = '1'), how_many_ones, A_WE_StdLogicVector((std_logic'(((write AND (data_in)))) = '1'), one_count_plus_one, A_WE_StdLogicVector((std_logic'(((read AND (stage_0)))) = '1'), one_count_minus_one, how_many_ones))))))), 8);
  --counts how many ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      how_many_ones <= std_logic_vector'("00000000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        how_many_ones <= updated_one_count;
      end if;
    end if;

  end process;

  --this fifo contains ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      fifo_contains_ones_n <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        fifo_contains_ones_n <= NOT (or_reduce(updated_one_count));
      end if;
    end if;

  end process;


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library std;
use std.textio.all;

entity clock_crossing_read_s1_arbitrator is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal clock_crossing_read_s1_endofpacket : IN STD_LOGIC;
                 signal clock_crossing_read_s1_readdata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal clock_crossing_read_s1_readdatavalid : IN STD_LOGIC;
                 signal clock_crossing_read_s1_waitrequest : IN STD_LOGIC;
                 signal master_read_avalon_master_address_to_slave : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal master_read_avalon_master_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal master_read_avalon_master_read : IN STD_LOGIC;
                 signal master_read_latency_counter : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;

              -- outputs:
                 signal clock_crossing_read_s1_address : OUT STD_LOGIC_VECTOR (21 DOWNTO 0);
                 signal clock_crossing_read_s1_burstcount : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal clock_crossing_read_s1_byteenable : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal clock_crossing_read_s1_endofpacket_from_sa : OUT STD_LOGIC;
                 signal clock_crossing_read_s1_nativeaddress : OUT STD_LOGIC_VECTOR (21 DOWNTO 0);
                 signal clock_crossing_read_s1_read : OUT STD_LOGIC;
                 signal clock_crossing_read_s1_readdata_from_sa : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal clock_crossing_read_s1_reset_n : OUT STD_LOGIC;
                 signal clock_crossing_read_s1_waitrequest_from_sa : OUT STD_LOGIC;
                 signal clock_crossing_read_s1_write : OUT STD_LOGIC;
                 signal d1_clock_crossing_read_s1_end_xfer : OUT STD_LOGIC;
                 signal master_read_granted_clock_crossing_read_s1 : OUT STD_LOGIC;
                 signal master_read_qualified_request_clock_crossing_read_s1 : OUT STD_LOGIC;
                 signal master_read_read_data_valid_clock_crossing_read_s1 : OUT STD_LOGIC;
                 signal master_read_read_data_valid_clock_crossing_read_s1_shift_register : OUT STD_LOGIC;
                 signal master_read_requests_clock_crossing_read_s1 : OUT STD_LOGIC
              );
end entity clock_crossing_read_s1_arbitrator;


architecture europa of clock_crossing_read_s1_arbitrator is
component burstcount_fifo_for_clock_crossing_read_s1_module is 
           port (
                 -- inputs:
                    signal clear_fifo : IN STD_LOGIC;
                    signal clk : IN STD_LOGIC;
                    signal data_in : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal read : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal sync_reset : IN STD_LOGIC;
                    signal write : IN STD_LOGIC;

                 -- outputs:
                    signal data_out : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal empty : OUT STD_LOGIC;
                    signal fifo_contains_ones_n : OUT STD_LOGIC;
                    signal full : OUT STD_LOGIC
                 );
end component burstcount_fifo_for_clock_crossing_read_s1_module;

component rdv_fifo_for_master_read_avalon_master_to_clock_crossing_read_s1_module is 
           port (
                 -- inputs:
                    signal clear_fifo : IN STD_LOGIC;
                    signal clk : IN STD_LOGIC;
                    signal data_in : IN STD_LOGIC;
                    signal read : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal sync_reset : IN STD_LOGIC;
                    signal write : IN STD_LOGIC;

                 -- outputs:
                    signal data_out : OUT STD_LOGIC;
                    signal empty : OUT STD_LOGIC;
                    signal fifo_contains_ones_n : OUT STD_LOGIC;
                    signal full : OUT STD_LOGIC
                 );
end component rdv_fifo_for_master_read_avalon_master_to_clock_crossing_read_s1_module;

                signal clock_crossing_read_s1_allgrants :  STD_LOGIC;
                signal clock_crossing_read_s1_allow_new_arb_cycle :  STD_LOGIC;
                signal clock_crossing_read_s1_any_bursting_master_saved_grant :  STD_LOGIC;
                signal clock_crossing_read_s1_any_continuerequest :  STD_LOGIC;
                signal clock_crossing_read_s1_arb_counter_enable :  STD_LOGIC;
                signal clock_crossing_read_s1_arb_share_counter :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_s1_arb_share_counter_next_value :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_s1_arb_share_set_values :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_s1_bbt_burstcounter :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal clock_crossing_read_s1_beginbursttransfer_internal :  STD_LOGIC;
                signal clock_crossing_read_s1_begins_xfer :  STD_LOGIC;
                signal clock_crossing_read_s1_burstcount_fifo_empty :  STD_LOGIC;
                signal clock_crossing_read_s1_current_burst :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_s1_current_burst_minus_one :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_s1_end_xfer :  STD_LOGIC;
                signal clock_crossing_read_s1_firsttransfer :  STD_LOGIC;
                signal clock_crossing_read_s1_grant_vector :  STD_LOGIC;
                signal clock_crossing_read_s1_in_a_read_cycle :  STD_LOGIC;
                signal clock_crossing_read_s1_in_a_write_cycle :  STD_LOGIC;
                signal clock_crossing_read_s1_load_fifo :  STD_LOGIC;
                signal clock_crossing_read_s1_master_qreq_vector :  STD_LOGIC;
                signal clock_crossing_read_s1_move_on_to_next_transaction :  STD_LOGIC;
                signal clock_crossing_read_s1_next_bbt_burstcount :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal clock_crossing_read_s1_next_burst_count :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_s1_non_bursting_master_requests :  STD_LOGIC;
                signal clock_crossing_read_s1_readdatavalid_from_sa :  STD_LOGIC;
                signal clock_crossing_read_s1_reg_firsttransfer :  STD_LOGIC;
                signal clock_crossing_read_s1_selected_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_s1_slavearbiterlockenable :  STD_LOGIC;
                signal clock_crossing_read_s1_slavearbiterlockenable2 :  STD_LOGIC;
                signal clock_crossing_read_s1_this_cycle_is_the_last_burst :  STD_LOGIC;
                signal clock_crossing_read_s1_transaction_burst_count :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_s1_unreg_firsttransfer :  STD_LOGIC;
                signal clock_crossing_read_s1_waits_for_read :  STD_LOGIC;
                signal clock_crossing_read_s1_waits_for_write :  STD_LOGIC;
                signal d1_reasons_to_wait :  STD_LOGIC;
                signal enable_nonzero_assertions :  STD_LOGIC;
                signal end_xfer_arb_share_counter_term_clock_crossing_read_s1 :  STD_LOGIC;
                signal in_a_read_cycle :  STD_LOGIC;
                signal in_a_write_cycle :  STD_LOGIC;
                signal internal_clock_crossing_read_s1_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal internal_clock_crossing_read_s1_read :  STD_LOGIC;
                signal internal_clock_crossing_read_s1_waitrequest_from_sa :  STD_LOGIC;
                signal internal_clock_crossing_read_s1_write :  STD_LOGIC;
                signal internal_master_read_granted_clock_crossing_read_s1 :  STD_LOGIC;
                signal internal_master_read_qualified_request_clock_crossing_read_s1 :  STD_LOGIC;
                signal internal_master_read_requests_clock_crossing_read_s1 :  STD_LOGIC;
                signal master_read_avalon_master_arbiterlock :  STD_LOGIC;
                signal master_read_avalon_master_arbiterlock2 :  STD_LOGIC;
                signal master_read_avalon_master_continuerequest :  STD_LOGIC;
                signal master_read_rdv_fifo_empty_clock_crossing_read_s1 :  STD_LOGIC;
                signal master_read_rdv_fifo_output_from_clock_crossing_read_s1 :  STD_LOGIC;
                signal master_read_saved_grant_clock_crossing_read_s1 :  STD_LOGIC;
                signal module_input :  STD_LOGIC;
                signal module_input1 :  STD_LOGIC;
                signal module_input2 :  STD_LOGIC;
                signal module_input3 :  STD_LOGIC;
                signal module_input4 :  STD_LOGIC;
                signal module_input5 :  STD_LOGIC;
                signal p0_clock_crossing_read_s1_load_fifo :  STD_LOGIC;
                signal shifted_address_to_clock_crossing_read_s1_from_master_read_avalon_master :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal wait_for_clock_crossing_read_s1_counter :  STD_LOGIC;

begin

  process (clk, reset_n)
  begin
    if reset_n = '0' then
      d1_reasons_to_wait <= std_logic'('0');
    elsif clk'event and clk = '1' then
      d1_reasons_to_wait <= NOT clock_crossing_read_s1_end_xfer;
    end if;

  end process;

  clock_crossing_read_s1_begins_xfer <= NOT d1_reasons_to_wait AND (internal_master_read_qualified_request_clock_crossing_read_s1);
  --assign clock_crossing_read_s1_readdata_from_sa = clock_crossing_read_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  clock_crossing_read_s1_readdata_from_sa <= clock_crossing_read_s1_readdata;
  internal_master_read_requests_clock_crossing_read_s1 <= Vector_To_Std_Logic(((((std_logic_vector'("00000000000000000000000000000001")) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((master_read_avalon_master_read)))))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(master_read_avalon_master_read)))));
  --assign clock_crossing_read_s1_waitrequest_from_sa = clock_crossing_read_s1_waitrequest so that symbol knows where to group signals which may go to master only, which is an e_assign
  internal_clock_crossing_read_s1_waitrequest_from_sa <= clock_crossing_read_s1_waitrequest;
  --assign clock_crossing_read_s1_readdatavalid_from_sa = clock_crossing_read_s1_readdatavalid so that symbol knows where to group signals which may go to master only, which is an e_assign
  clock_crossing_read_s1_readdatavalid_from_sa <= clock_crossing_read_s1_readdatavalid;
  --clock_crossing_read_s1_arb_share_counter set values, which is an e_mux
  clock_crossing_read_s1_arb_share_set_values <= std_logic_vector'("0001");
  --clock_crossing_read_s1_non_bursting_master_requests mux, which is an e_mux
  clock_crossing_read_s1_non_bursting_master_requests <= std_logic'('0');
  --clock_crossing_read_s1_any_bursting_master_saved_grant mux, which is an e_mux
  clock_crossing_read_s1_any_bursting_master_saved_grant <= master_read_saved_grant_clock_crossing_read_s1;
  --clock_crossing_read_s1_arb_share_counter_next_value assignment, which is an e_assign
  clock_crossing_read_s1_arb_share_counter_next_value <= A_EXT (A_WE_StdLogicVector((std_logic'(clock_crossing_read_s1_firsttransfer) = '1'), (((std_logic_vector'("00000000000000000000000000000") & (clock_crossing_read_s1_arb_share_set_values)) - std_logic_vector'("000000000000000000000000000000001"))), A_WE_StdLogicVector((std_logic'(or_reduce(clock_crossing_read_s1_arb_share_counter)) = '1'), (((std_logic_vector'("00000000000000000000000000000") & (clock_crossing_read_s1_arb_share_counter)) - std_logic_vector'("000000000000000000000000000000001"))), std_logic_vector'("000000000000000000000000000000000"))), 4);
  --clock_crossing_read_s1_allgrants all slave grants, which is an e_mux
  clock_crossing_read_s1_allgrants <= clock_crossing_read_s1_grant_vector;
  --clock_crossing_read_s1_end_xfer assignment, which is an e_assign
  clock_crossing_read_s1_end_xfer <= NOT ((clock_crossing_read_s1_waits_for_read OR clock_crossing_read_s1_waits_for_write));
  --end_xfer_arb_share_counter_term_clock_crossing_read_s1 arb share counter enable term, which is an e_assign
  end_xfer_arb_share_counter_term_clock_crossing_read_s1 <= clock_crossing_read_s1_end_xfer AND (((NOT clock_crossing_read_s1_any_bursting_master_saved_grant OR in_a_read_cycle) OR in_a_write_cycle));
  --clock_crossing_read_s1_arb_share_counter arbitration counter enable, which is an e_assign
  clock_crossing_read_s1_arb_counter_enable <= ((end_xfer_arb_share_counter_term_clock_crossing_read_s1 AND clock_crossing_read_s1_allgrants)) OR ((end_xfer_arb_share_counter_term_clock_crossing_read_s1 AND NOT clock_crossing_read_s1_non_bursting_master_requests));
  --clock_crossing_read_s1_arb_share_counter counter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      clock_crossing_read_s1_arb_share_counter <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'(clock_crossing_read_s1_arb_counter_enable) = '1' then 
        clock_crossing_read_s1_arb_share_counter <= clock_crossing_read_s1_arb_share_counter_next_value;
      end if;
    end if;

  end process;

  --clock_crossing_read_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      clock_crossing_read_s1_slavearbiterlockenable <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clock_crossing_read_s1_master_qreq_vector AND end_xfer_arb_share_counter_term_clock_crossing_read_s1)) OR ((end_xfer_arb_share_counter_term_clock_crossing_read_s1 AND NOT clock_crossing_read_s1_non_bursting_master_requests)))) = '1' then 
        clock_crossing_read_s1_slavearbiterlockenable <= or_reduce(clock_crossing_read_s1_arb_share_counter_next_value);
      end if;
    end if;

  end process;

  --master_read/avalon_master clock_crossing_read/s1 arbiterlock, which is an e_assign
  master_read_avalon_master_arbiterlock <= clock_crossing_read_s1_slavearbiterlockenable AND master_read_avalon_master_continuerequest;
  --clock_crossing_read_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  clock_crossing_read_s1_slavearbiterlockenable2 <= or_reduce(clock_crossing_read_s1_arb_share_counter_next_value);
  --master_read/avalon_master clock_crossing_read/s1 arbiterlock2, which is an e_assign
  master_read_avalon_master_arbiterlock2 <= clock_crossing_read_s1_slavearbiterlockenable2 AND master_read_avalon_master_continuerequest;
  --clock_crossing_read_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  clock_crossing_read_s1_any_continuerequest <= std_logic'('1');
  --master_read_avalon_master_continuerequest continued request, which is an e_assign
  master_read_avalon_master_continuerequest <= std_logic'('1');
  internal_master_read_qualified_request_clock_crossing_read_s1 <= internal_master_read_requests_clock_crossing_read_s1 AND NOT ((master_read_avalon_master_read AND to_std_logic((((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(master_read_latency_counter))) /= std_logic_vector'("00000000000000000000000000000000"))) OR ((std_logic_vector'("00000000000000000000000000000001")<(std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(master_read_latency_counter))))))))));
  --unique name for clock_crossing_read_s1_move_on_to_next_transaction, which is an e_assign
  clock_crossing_read_s1_move_on_to_next_transaction <= clock_crossing_read_s1_this_cycle_is_the_last_burst AND clock_crossing_read_s1_load_fifo;
  --the currently selected burstcount for clock_crossing_read_s1, which is an e_mux
  clock_crossing_read_s1_selected_burstcount <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_master_read_granted_clock_crossing_read_s1)) = '1'), (std_logic_vector'("0000000000000000000000000000") & (master_read_avalon_master_burstcount)), std_logic_vector'("00000000000000000000000000000001")), 4);
  --burstcount_fifo_for_clock_crossing_read_s1, which is an e_fifo_with_registered_outputs
  burstcount_fifo_for_clock_crossing_read_s1 : burstcount_fifo_for_clock_crossing_read_s1_module
    port map(
      data_out => clock_crossing_read_s1_transaction_burst_count,
      empty => clock_crossing_read_s1_burstcount_fifo_empty,
      fifo_contains_ones_n => open,
      full => open,
      clear_fifo => module_input,
      clk => clk,
      data_in => clock_crossing_read_s1_selected_burstcount,
      read => clock_crossing_read_s1_this_cycle_is_the_last_burst,
      reset_n => reset_n,
      sync_reset => module_input1,
      write => module_input2
    );

  module_input <= std_logic'('0');
  module_input1 <= std_logic'('0');
  module_input2 <= ((in_a_read_cycle AND NOT clock_crossing_read_s1_waits_for_read) AND clock_crossing_read_s1_load_fifo) AND NOT ((clock_crossing_read_s1_this_cycle_is_the_last_burst AND clock_crossing_read_s1_burstcount_fifo_empty));

  --clock_crossing_read_s1 current burst minus one, which is an e_assign
  clock_crossing_read_s1_current_burst_minus_one <= A_EXT (((std_logic_vector'("00000000000000000000000000000") & (clock_crossing_read_s1_current_burst)) - std_logic_vector'("000000000000000000000000000000001")), 4);
  --what to load in current_burst, for clock_crossing_read_s1, which is an e_mux
  clock_crossing_read_s1_next_burst_count <= A_WE_StdLogicVector((std_logic'(((((in_a_read_cycle AND NOT clock_crossing_read_s1_waits_for_read)) AND NOT clock_crossing_read_s1_load_fifo))) = '1'), clock_crossing_read_s1_selected_burstcount, A_WE_StdLogicVector((std_logic'(((((in_a_read_cycle AND NOT clock_crossing_read_s1_waits_for_read) AND clock_crossing_read_s1_this_cycle_is_the_last_burst) AND clock_crossing_read_s1_burstcount_fifo_empty))) = '1'), clock_crossing_read_s1_selected_burstcount, A_WE_StdLogicVector((std_logic'((clock_crossing_read_s1_this_cycle_is_the_last_burst)) = '1'), clock_crossing_read_s1_transaction_burst_count, clock_crossing_read_s1_current_burst_minus_one)));
  --the current burst count for clock_crossing_read_s1, to be decremented, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      clock_crossing_read_s1_current_burst <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((clock_crossing_read_s1_readdatavalid_from_sa OR ((NOT clock_crossing_read_s1_load_fifo AND ((in_a_read_cycle AND NOT clock_crossing_read_s1_waits_for_read)))))) = '1' then 
        clock_crossing_read_s1_current_burst <= clock_crossing_read_s1_next_burst_count;
      end if;
    end if;

  end process;

  --a 1 or burstcount fifo empty, to initialize the counter, which is an e_mux
  p0_clock_crossing_read_s1_load_fifo <= Vector_To_Std_Logic(A_WE_StdLogicVector((std_logic'((NOT clock_crossing_read_s1_load_fifo)) = '1'), std_logic_vector'("00000000000000000000000000000001"), A_WE_StdLogicVector((std_logic'(((((in_a_read_cycle AND NOT clock_crossing_read_s1_waits_for_read)) AND clock_crossing_read_s1_load_fifo))) = '1'), std_logic_vector'("00000000000000000000000000000001"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT clock_crossing_read_s1_burstcount_fifo_empty))))));
  --whether to load directly to the counter or to the fifo, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      clock_crossing_read_s1_load_fifo <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((((in_a_read_cycle AND NOT clock_crossing_read_s1_waits_for_read)) AND NOT clock_crossing_read_s1_load_fifo) OR clock_crossing_read_s1_this_cycle_is_the_last_burst)) = '1' then 
        clock_crossing_read_s1_load_fifo <= p0_clock_crossing_read_s1_load_fifo;
      end if;
    end if;

  end process;

  --the last cycle in the burst for clock_crossing_read_s1, which is an e_assign
  clock_crossing_read_s1_this_cycle_is_the_last_burst <= NOT (or_reduce(clock_crossing_read_s1_current_burst_minus_one)) AND clock_crossing_read_s1_readdatavalid_from_sa;
  --rdv_fifo_for_master_read_avalon_master_to_clock_crossing_read_s1, which is an e_fifo_with_registered_outputs
  rdv_fifo_for_master_read_avalon_master_to_clock_crossing_read_s1 : rdv_fifo_for_master_read_avalon_master_to_clock_crossing_read_s1_module
    port map(
      data_out => master_read_rdv_fifo_output_from_clock_crossing_read_s1,
      empty => open,
      fifo_contains_ones_n => master_read_rdv_fifo_empty_clock_crossing_read_s1,
      full => open,
      clear_fifo => module_input3,
      clk => clk,
      data_in => internal_master_read_granted_clock_crossing_read_s1,
      read => clock_crossing_read_s1_move_on_to_next_transaction,
      reset_n => reset_n,
      sync_reset => module_input4,
      write => module_input5
    );

  module_input3 <= std_logic'('0');
  module_input4 <= std_logic'('0');
  module_input5 <= in_a_read_cycle AND NOT clock_crossing_read_s1_waits_for_read;

  master_read_read_data_valid_clock_crossing_read_s1_shift_register <= NOT master_read_rdv_fifo_empty_clock_crossing_read_s1;
  --local readdatavalid master_read_read_data_valid_clock_crossing_read_s1, which is an e_mux
  master_read_read_data_valid_clock_crossing_read_s1 <= clock_crossing_read_s1_readdatavalid_from_sa;
  --assign clock_crossing_read_s1_endofpacket_from_sa = clock_crossing_read_s1_endofpacket so that symbol knows where to group signals which may go to master only, which is an e_assign
  clock_crossing_read_s1_endofpacket_from_sa <= clock_crossing_read_s1_endofpacket;
  --master is always granted when requested
  internal_master_read_granted_clock_crossing_read_s1 <= internal_master_read_qualified_request_clock_crossing_read_s1;
  --master_read/avalon_master saved-grant clock_crossing_read/s1, which is an e_assign
  master_read_saved_grant_clock_crossing_read_s1 <= internal_master_read_requests_clock_crossing_read_s1;
  --allow new arb cycle for clock_crossing_read/s1, which is an e_assign
  clock_crossing_read_s1_allow_new_arb_cycle <= std_logic'('1');
  --placeholder chosen master
  clock_crossing_read_s1_grant_vector <= std_logic'('1');
  --placeholder vector of master qualified-requests
  clock_crossing_read_s1_master_qreq_vector <= std_logic'('1');
  --clock_crossing_read_s1_reset_n assignment, which is an e_assign
  clock_crossing_read_s1_reset_n <= reset_n;
  --clock_crossing_read_s1_firsttransfer first transaction, which is an e_assign
  clock_crossing_read_s1_firsttransfer <= A_WE_StdLogic((std_logic'(clock_crossing_read_s1_begins_xfer) = '1'), clock_crossing_read_s1_unreg_firsttransfer, clock_crossing_read_s1_reg_firsttransfer);
  --clock_crossing_read_s1_unreg_firsttransfer first transaction, which is an e_assign
  clock_crossing_read_s1_unreg_firsttransfer <= NOT ((clock_crossing_read_s1_slavearbiterlockenable AND clock_crossing_read_s1_any_continuerequest));
  --clock_crossing_read_s1_reg_firsttransfer first transaction, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      clock_crossing_read_s1_reg_firsttransfer <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'(clock_crossing_read_s1_begins_xfer) = '1' then 
        clock_crossing_read_s1_reg_firsttransfer <= clock_crossing_read_s1_unreg_firsttransfer;
      end if;
    end if;

  end process;

  --clock_crossing_read_s1_next_bbt_burstcount next_bbt_burstcount, which is an e_mux
  clock_crossing_read_s1_next_bbt_burstcount <= A_EXT (A_WE_StdLogicVector((std_logic'((((internal_clock_crossing_read_s1_write) AND to_std_logic((((std_logic_vector'("00000000000000000000000000000") & (clock_crossing_read_s1_bbt_burstcounter)) = std_logic_vector'("00000000000000000000000000000000"))))))) = '1'), (((std_logic_vector'("00000000000000000000000000000") & (internal_clock_crossing_read_s1_burstcount)) - std_logic_vector'("000000000000000000000000000000001"))), A_WE_StdLogicVector((std_logic'((((internal_clock_crossing_read_s1_read) AND to_std_logic((((std_logic_vector'("00000000000000000000000000000") & (clock_crossing_read_s1_bbt_burstcounter)) = std_logic_vector'("00000000000000000000000000000000"))))))) = '1'), std_logic_vector'("000000000000000000000000000000000"), (((std_logic_vector'("000000000000000000000000000000") & (clock_crossing_read_s1_bbt_burstcounter)) - std_logic_vector'("000000000000000000000000000000001"))))), 3);
  --clock_crossing_read_s1_bbt_burstcounter bbt_burstcounter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      clock_crossing_read_s1_bbt_burstcounter <= std_logic_vector'("000");
    elsif clk'event and clk = '1' then
      if std_logic'(clock_crossing_read_s1_begins_xfer) = '1' then 
        clock_crossing_read_s1_bbt_burstcounter <= clock_crossing_read_s1_next_bbt_burstcount;
      end if;
    end if;

  end process;

  --clock_crossing_read_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  clock_crossing_read_s1_beginbursttransfer_internal <= clock_crossing_read_s1_begins_xfer AND to_std_logic((((std_logic_vector'("00000000000000000000000000000") & (clock_crossing_read_s1_bbt_burstcounter)) = std_logic_vector'("00000000000000000000000000000000"))));
  --clock_crossing_read_s1_read assignment, which is an e_mux
  internal_clock_crossing_read_s1_read <= internal_master_read_granted_clock_crossing_read_s1 AND master_read_avalon_master_read;
  --clock_crossing_read_s1_write assignment, which is an e_mux
  internal_clock_crossing_read_s1_write <= std_logic'('0');
  shifted_address_to_clock_crossing_read_s1_from_master_read_avalon_master <= master_read_avalon_master_address_to_slave;
  --clock_crossing_read_s1_address mux, which is an e_mux
  clock_crossing_read_s1_address <= A_EXT (A_SRL(shifted_address_to_clock_crossing_read_s1_from_master_read_avalon_master,std_logic_vector'("00000000000000000000000000000010")), 22);
  --slaveid clock_crossing_read_s1_nativeaddress nativeaddress mux, which is an e_mux
  clock_crossing_read_s1_nativeaddress <= A_EXT (A_SRL(master_read_avalon_master_address_to_slave,std_logic_vector'("00000000000000000000000000000010")), 22);
  --d1_clock_crossing_read_s1_end_xfer register, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      d1_clock_crossing_read_s1_end_xfer <= std_logic'('1');
    elsif clk'event and clk = '1' then
      d1_clock_crossing_read_s1_end_xfer <= clock_crossing_read_s1_end_xfer;
    end if;

  end process;

  --clock_crossing_read_s1_waits_for_read in a cycle, which is an e_mux
  clock_crossing_read_s1_waits_for_read <= clock_crossing_read_s1_in_a_read_cycle AND internal_clock_crossing_read_s1_waitrequest_from_sa;
  --clock_crossing_read_s1_in_a_read_cycle assignment, which is an e_assign
  clock_crossing_read_s1_in_a_read_cycle <= internal_master_read_granted_clock_crossing_read_s1 AND master_read_avalon_master_read;
  --in_a_read_cycle assignment, which is an e_mux
  in_a_read_cycle <= clock_crossing_read_s1_in_a_read_cycle;
  --clock_crossing_read_s1_waits_for_write in a cycle, which is an e_mux
  clock_crossing_read_s1_waits_for_write <= clock_crossing_read_s1_in_a_write_cycle AND internal_clock_crossing_read_s1_waitrequest_from_sa;
  --clock_crossing_read_s1_in_a_write_cycle assignment, which is an e_assign
  clock_crossing_read_s1_in_a_write_cycle <= std_logic'('0');
  --in_a_write_cycle assignment, which is an e_mux
  in_a_write_cycle <= clock_crossing_read_s1_in_a_write_cycle;
  wait_for_clock_crossing_read_s1_counter <= std_logic'('0');
  --clock_crossing_read_s1_byteenable byte enable port mux, which is an e_mux
  clock_crossing_read_s1_byteenable <= A_EXT (-SIGNED(std_logic_vector'("00000000000000000000000000000001")), 4);
  --burstcount mux, which is an e_mux
  internal_clock_crossing_read_s1_burstcount <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_master_read_granted_clock_crossing_read_s1)) = '1'), (std_logic_vector'("0000000000000000000000000000") & (master_read_avalon_master_burstcount)), std_logic_vector'("00000000000000000000000000000001")), 4);
  --vhdl renameroo for output signals
  clock_crossing_read_s1_burstcount <= internal_clock_crossing_read_s1_burstcount;
  --vhdl renameroo for output signals
  clock_crossing_read_s1_read <= internal_clock_crossing_read_s1_read;
  --vhdl renameroo for output signals
  clock_crossing_read_s1_waitrequest_from_sa <= internal_clock_crossing_read_s1_waitrequest_from_sa;
  --vhdl renameroo for output signals
  clock_crossing_read_s1_write <= internal_clock_crossing_read_s1_write;
  --vhdl renameroo for output signals
  master_read_granted_clock_crossing_read_s1 <= internal_master_read_granted_clock_crossing_read_s1;
  --vhdl renameroo for output signals
  master_read_qualified_request_clock_crossing_read_s1 <= internal_master_read_qualified_request_clock_crossing_read_s1;
  --vhdl renameroo for output signals
  master_read_requests_clock_crossing_read_s1 <= internal_master_read_requests_clock_crossing_read_s1;
--synthesis translate_off
    --clock_crossing_read/s1 enable non-zero assertions, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        enable_nonzero_assertions <= std_logic'('0');
      elsif clk'event and clk = '1' then
        enable_nonzero_assertions <= std_logic'('1');
      end if;

    end process;

    --master_read/avalon_master non-zero burstcount assertion, which is an e_process
    process (clk)
    VARIABLE write_line : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((internal_master_read_requests_clock_crossing_read_s1 AND to_std_logic((((std_logic_vector'("0000000000000000000000000000") & (master_read_avalon_master_burstcount)) = std_logic_vector'("00000000000000000000000000000000"))))) AND enable_nonzero_assertions)) = '1' then 
          write(write_line, now);
          write(write_line, string'(": "));
          write(write_line, string'("master_read/avalon_master drove 0 on its 'burstcount' port while accessing slave clock_crossing_read/s1"));
          write(output, write_line.all);
          deallocate (write_line);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

--synthesis translate_on

end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library std;
use std.textio.all;

entity clock_crossing_read_m1_arbitrator is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal clock_crossing_read_m1_address : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal clock_crossing_read_m1_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal clock_crossing_read_m1_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal clock_crossing_read_m1_byteenable_system_burst_1_upstream : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal clock_crossing_read_m1_granted_system_burst_1_upstream : IN STD_LOGIC;
                 signal clock_crossing_read_m1_qualified_request_system_burst_1_upstream : IN STD_LOGIC;
                 signal clock_crossing_read_m1_read : IN STD_LOGIC;
                 signal clock_crossing_read_m1_read_data_valid_system_burst_1_upstream : IN STD_LOGIC;
                 signal clock_crossing_read_m1_read_data_valid_system_burst_1_upstream_shift_register : IN STD_LOGIC;
                 signal clock_crossing_read_m1_requests_system_burst_1_upstream : IN STD_LOGIC;
                 signal clock_crossing_read_m1_write : IN STD_LOGIC;
                 signal clock_crossing_read_m1_writedata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal d1_system_burst_1_upstream_end_xfer : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal system_burst_1_upstream_readdata_from_sa : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal system_burst_1_upstream_waitrequest_from_sa : IN STD_LOGIC;

              -- outputs:
                 signal clock_crossing_read_m1_address_to_slave : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal clock_crossing_read_m1_dbs_address : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal clock_crossing_read_m1_dbs_write_16 : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal clock_crossing_read_m1_latency_counter : OUT STD_LOGIC;
                 signal clock_crossing_read_m1_readdata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal clock_crossing_read_m1_readdatavalid : OUT STD_LOGIC;
                 signal clock_crossing_read_m1_reset_n : OUT STD_LOGIC;
                 signal clock_crossing_read_m1_waitrequest : OUT STD_LOGIC
              );
end entity clock_crossing_read_m1_arbitrator;


architecture europa of clock_crossing_read_m1_arbitrator is
                signal active_and_waiting_last_time :  STD_LOGIC;
                signal clock_crossing_read_m1_address_last_time :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal clock_crossing_read_m1_burstcount_last_time :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_m1_byteenable_last_time :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_m1_dbs_increment :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_read_m1_dbs_rdv_counter :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_read_m1_dbs_rdv_counter_inc :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_read_m1_next_dbs_rdv_counter :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_read_m1_read_last_time :  STD_LOGIC;
                signal clock_crossing_read_m1_run :  STD_LOGIC;
                signal clock_crossing_read_m1_write_last_time :  STD_LOGIC;
                signal clock_crossing_read_m1_writedata_last_time :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal dbs_count_enable :  STD_LOGIC;
                signal dbs_counter_overflow :  STD_LOGIC;
                signal dbs_latent_16_reg_segment_0 :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal dbs_rdv_count_enable :  STD_LOGIC;
                signal dbs_rdv_counter_overflow :  STD_LOGIC;
                signal internal_clock_crossing_read_m1_address_to_slave :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal internal_clock_crossing_read_m1_dbs_address :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal internal_clock_crossing_read_m1_waitrequest :  STD_LOGIC;
                signal next_dbs_address :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal p1_dbs_latent_16_reg_segment_0 :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal pre_dbs_count_enable :  STD_LOGIC;
                signal pre_flush_clock_crossing_read_m1_readdatavalid :  STD_LOGIC;
                signal r_0 :  STD_LOGIC;

begin

  --r_0 master_run cascaded wait assignment, which is an e_assign
  r_0 <= Vector_To_Std_Logic((((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((clock_crossing_read_m1_qualified_request_system_burst_1_upstream OR NOT clock_crossing_read_m1_requests_system_burst_1_upstream)))))) AND (((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((NOT clock_crossing_read_m1_qualified_request_system_burst_1_upstream OR NOT clock_crossing_read_m1_read)))) OR (((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT system_burst_1_upstream_waitrequest_from_sa)))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(clock_crossing_read_m1_read)))))))) AND (((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((NOT clock_crossing_read_m1_qualified_request_system_burst_1_upstream OR NOT clock_crossing_read_m1_write)))) OR ((((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT system_burst_1_upstream_waitrequest_from_sa)))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((internal_clock_crossing_read_m1_dbs_address(1)))))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(clock_crossing_read_m1_write)))))))));
  --cascaded wait assignment, which is an e_assign
  clock_crossing_read_m1_run <= r_0;
  --optimize select-logic by passing only those address bits which matter.
  internal_clock_crossing_read_m1_address_to_slave <= clock_crossing_read_m1_address(23 DOWNTO 0);
  --latent slave read data valids which may be flushed, which is an e_mux
  pre_flush_clock_crossing_read_m1_readdatavalid <= clock_crossing_read_m1_read_data_valid_system_burst_1_upstream AND dbs_rdv_counter_overflow;
  --latent slave read data valid which is not flushed, which is an e_mux
  clock_crossing_read_m1_readdatavalid <= Vector_To_Std_Logic((std_logic_vector'("00000000000000000000000000000000") OR (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(pre_flush_clock_crossing_read_m1_readdatavalid)))));
  --input to latent dbs-16 stored 0, which is an e_mux
  p1_dbs_latent_16_reg_segment_0 <= system_burst_1_upstream_readdata_from_sa;
  --dbs register for latent dbs-16 segment 0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      dbs_latent_16_reg_segment_0 <= std_logic_vector'("0000000000000000");
    elsif clk'event and clk = '1' then
      if std_logic'((dbs_rdv_count_enable AND to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((clock_crossing_read_m1_dbs_rdv_counter(1))))) = std_logic_vector'("00000000000000000000000000000000")))))) = '1' then 
        dbs_latent_16_reg_segment_0 <= p1_dbs_latent_16_reg_segment_0;
      end if;
    end if;

  end process;

  --clock_crossing_read/m1 readdata mux, which is an e_mux
  clock_crossing_read_m1_readdata <= Std_Logic_Vector'(system_burst_1_upstream_readdata_from_sa(15 DOWNTO 0) & dbs_latent_16_reg_segment_0);
  --mux write dbs 1, which is an e_mux
  clock_crossing_read_m1_dbs_write_16 <= A_WE_StdLogicVector((std_logic'((internal_clock_crossing_read_m1_dbs_address(1))) = '1'), clock_crossing_read_m1_writedata(31 DOWNTO 16), clock_crossing_read_m1_writedata(15 DOWNTO 0));
  --actual waitrequest port, which is an e_assign
  internal_clock_crossing_read_m1_waitrequest <= NOT clock_crossing_read_m1_run;
  --latent max counter, which is an e_assign
  clock_crossing_read_m1_latency_counter <= std_logic'('0');
  --clock_crossing_read_m1_reset_n assignment, which is an e_assign
  clock_crossing_read_m1_reset_n <= reset_n;
  --dbs count increment, which is an e_mux
  clock_crossing_read_m1_dbs_increment <= A_EXT (A_WE_StdLogicVector((std_logic'((clock_crossing_read_m1_requests_system_burst_1_upstream)) = '1'), std_logic_vector'("00000000000000000000000000000010"), std_logic_vector'("00000000000000000000000000000000")), 2);
  --dbs counter overflow, which is an e_assign
  dbs_counter_overflow <= internal_clock_crossing_read_m1_dbs_address(1) AND NOT((next_dbs_address(1)));
  --next master address, which is an e_assign
  next_dbs_address <= A_EXT (((std_logic_vector'("0") & (internal_clock_crossing_read_m1_dbs_address)) + (std_logic_vector'("0") & (clock_crossing_read_m1_dbs_increment))), 2);
  --dbs count enable, which is an e_mux
  dbs_count_enable <= pre_dbs_count_enable;
  --dbs counter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      internal_clock_crossing_read_m1_dbs_address <= std_logic_vector'("00");
    elsif clk'event and clk = '1' then
      if std_logic'(dbs_count_enable) = '1' then 
        internal_clock_crossing_read_m1_dbs_address <= next_dbs_address;
      end if;
    end if;

  end process;

  --p1 dbs rdv counter, which is an e_assign
  clock_crossing_read_m1_next_dbs_rdv_counter <= A_EXT (((std_logic_vector'("0") & (clock_crossing_read_m1_dbs_rdv_counter)) + (std_logic_vector'("0") & (clock_crossing_read_m1_dbs_rdv_counter_inc))), 2);
  --clock_crossing_read_m1_rdv_inc_mux, which is an e_mux
  clock_crossing_read_m1_dbs_rdv_counter_inc <= std_logic_vector'("10");
  --master any slave rdv, which is an e_mux
  dbs_rdv_count_enable <= clock_crossing_read_m1_read_data_valid_system_burst_1_upstream;
  --dbs rdv counter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      clock_crossing_read_m1_dbs_rdv_counter <= std_logic_vector'("00");
    elsif clk'event and clk = '1' then
      if std_logic'(dbs_rdv_count_enable) = '1' then 
        clock_crossing_read_m1_dbs_rdv_counter <= clock_crossing_read_m1_next_dbs_rdv_counter;
      end if;
    end if;

  end process;

  --dbs rdv counter overflow, which is an e_assign
  dbs_rdv_counter_overflow <= clock_crossing_read_m1_dbs_rdv_counter(1) AND NOT clock_crossing_read_m1_next_dbs_rdv_counter(1);
  --pre dbs count enable, which is an e_mux
  pre_dbs_count_enable <= Vector_To_Std_Logic(((((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((clock_crossing_read_m1_granted_system_burst_1_upstream AND clock_crossing_read_m1_read)))) AND std_logic_vector'("00000000000000000000000000000000")) AND std_logic_vector'("00000000000000000000000000000001")) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT system_burst_1_upstream_waitrequest_from_sa))))) OR (((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((clock_crossing_read_m1_granted_system_burst_1_upstream AND clock_crossing_read_m1_write)))) AND std_logic_vector'("00000000000000000000000000000001")) AND std_logic_vector'("00000000000000000000000000000001")) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT system_burst_1_upstream_waitrequest_from_sa)))))));
  --vhdl renameroo for output signals
  clock_crossing_read_m1_address_to_slave <= internal_clock_crossing_read_m1_address_to_slave;
  --vhdl renameroo for output signals
  clock_crossing_read_m1_dbs_address <= internal_clock_crossing_read_m1_dbs_address;
  --vhdl renameroo for output signals
  clock_crossing_read_m1_waitrequest <= internal_clock_crossing_read_m1_waitrequest;
--synthesis translate_off
    --clock_crossing_read_m1_address check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        clock_crossing_read_m1_address_last_time <= std_logic_vector'("000000000000000000000000");
      elsif clk'event and clk = '1' then
        clock_crossing_read_m1_address_last_time <= clock_crossing_read_m1_address;
      end if;

    end process;

    --clock_crossing_read/m1 waited last time, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        active_and_waiting_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        active_and_waiting_last_time <= internal_clock_crossing_read_m1_waitrequest AND ((clock_crossing_read_m1_read OR clock_crossing_read_m1_write));
      end if;

    end process;

    --clock_crossing_read_m1_address matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line1 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((clock_crossing_read_m1_address /= clock_crossing_read_m1_address_last_time))))) = '1' then 
          write(write_line1, now);
          write(write_line1, string'(": "));
          write(write_line1, string'("clock_crossing_read_m1_address did not heed wait!!!"));
          write(output, write_line1.all);
          deallocate (write_line1);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --clock_crossing_read_m1_burstcount check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        clock_crossing_read_m1_burstcount_last_time <= std_logic_vector'("0000");
      elsif clk'event and clk = '1' then
        clock_crossing_read_m1_burstcount_last_time <= clock_crossing_read_m1_burstcount;
      end if;

    end process;

    --clock_crossing_read_m1_burstcount matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line2 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((clock_crossing_read_m1_burstcount /= clock_crossing_read_m1_burstcount_last_time))))) = '1' then 
          write(write_line2, now);
          write(write_line2, string'(": "));
          write(write_line2, string'("clock_crossing_read_m1_burstcount did not heed wait!!!"));
          write(output, write_line2.all);
          deallocate (write_line2);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --clock_crossing_read_m1_byteenable check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        clock_crossing_read_m1_byteenable_last_time <= std_logic_vector'("0000");
      elsif clk'event and clk = '1' then
        clock_crossing_read_m1_byteenable_last_time <= clock_crossing_read_m1_byteenable;
      end if;

    end process;

    --clock_crossing_read_m1_byteenable matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line3 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((clock_crossing_read_m1_byteenable /= clock_crossing_read_m1_byteenable_last_time))))) = '1' then 
          write(write_line3, now);
          write(write_line3, string'(": "));
          write(write_line3, string'("clock_crossing_read_m1_byteenable did not heed wait!!!"));
          write(output, write_line3.all);
          deallocate (write_line3);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --clock_crossing_read_m1_read check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        clock_crossing_read_m1_read_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        clock_crossing_read_m1_read_last_time <= clock_crossing_read_m1_read;
      end if;

    end process;

    --clock_crossing_read_m1_read matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line4 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((std_logic'(clock_crossing_read_m1_read) /= std_logic'(clock_crossing_read_m1_read_last_time)))))) = '1' then 
          write(write_line4, now);
          write(write_line4, string'(": "));
          write(write_line4, string'("clock_crossing_read_m1_read did not heed wait!!!"));
          write(output, write_line4.all);
          deallocate (write_line4);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --clock_crossing_read_m1_write check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        clock_crossing_read_m1_write_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        clock_crossing_read_m1_write_last_time <= clock_crossing_read_m1_write;
      end if;

    end process;

    --clock_crossing_read_m1_write matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line5 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((std_logic'(clock_crossing_read_m1_write) /= std_logic'(clock_crossing_read_m1_write_last_time)))))) = '1' then 
          write(write_line5, now);
          write(write_line5, string'(": "));
          write(write_line5, string'("clock_crossing_read_m1_write did not heed wait!!!"));
          write(output, write_line5.all);
          deallocate (write_line5);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --clock_crossing_read_m1_writedata check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        clock_crossing_read_m1_writedata_last_time <= std_logic_vector'("00000000000000000000000000000000");
      elsif clk'event and clk = '1' then
        clock_crossing_read_m1_writedata_last_time <= clock_crossing_read_m1_writedata;
      end if;

    end process;

    --clock_crossing_read_m1_writedata matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line6 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((active_and_waiting_last_time AND to_std_logic(((clock_crossing_read_m1_writedata /= clock_crossing_read_m1_writedata_last_time)))) AND clock_crossing_read_m1_write)) = '1' then 
          write(write_line6, now);
          write(write_line6, string'(": "));
          write(write_line6, string'("clock_crossing_read_m1_writedata did not heed wait!!!"));
          write(output, write_line6.all);
          deallocate (write_line6);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

--synthesis translate_on

end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity clock_crossing_read_bridge_arbitrator is 
end entity clock_crossing_read_bridge_arbitrator;


architecture europa of clock_crossing_read_bridge_arbitrator is

begin


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library std;
use std.textio.all;

entity clock_crossing_write_s1_arbitrator is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal clock_crossing_write_s1_endofpacket : IN STD_LOGIC;
                 signal clock_crossing_write_s1_readdata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal clock_crossing_write_s1_readdatavalid : IN STD_LOGIC;
                 signal clock_crossing_write_s1_waitrequest : IN STD_LOGIC;
                 signal master_write_avalon_master_address_to_slave : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal master_write_avalon_master_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal master_write_avalon_master_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal master_write_avalon_master_write : IN STD_LOGIC;
                 signal master_write_avalon_master_writedata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal reset_n : IN STD_LOGIC;

              -- outputs:
                 signal clock_crossing_write_s1_address : OUT STD_LOGIC_VECTOR (21 DOWNTO 0);
                 signal clock_crossing_write_s1_burstcount : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal clock_crossing_write_s1_byteenable : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal clock_crossing_write_s1_endofpacket_from_sa : OUT STD_LOGIC;
                 signal clock_crossing_write_s1_nativeaddress : OUT STD_LOGIC_VECTOR (21 DOWNTO 0);
                 signal clock_crossing_write_s1_read : OUT STD_LOGIC;
                 signal clock_crossing_write_s1_readdata_from_sa : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal clock_crossing_write_s1_readdatavalid_from_sa : OUT STD_LOGIC;
                 signal clock_crossing_write_s1_reset_n : OUT STD_LOGIC;
                 signal clock_crossing_write_s1_waitrequest_from_sa : OUT STD_LOGIC;
                 signal clock_crossing_write_s1_write : OUT STD_LOGIC;
                 signal clock_crossing_write_s1_writedata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal d1_clock_crossing_write_s1_end_xfer : OUT STD_LOGIC;
                 signal master_write_granted_clock_crossing_write_s1 : OUT STD_LOGIC;
                 signal master_write_qualified_request_clock_crossing_write_s1 : OUT STD_LOGIC;
                 signal master_write_requests_clock_crossing_write_s1 : OUT STD_LOGIC
              );
end entity clock_crossing_write_s1_arbitrator;


architecture europa of clock_crossing_write_s1_arbitrator is
                signal clock_crossing_write_s1_allgrants :  STD_LOGIC;
                signal clock_crossing_write_s1_allow_new_arb_cycle :  STD_LOGIC;
                signal clock_crossing_write_s1_any_bursting_master_saved_grant :  STD_LOGIC;
                signal clock_crossing_write_s1_any_continuerequest :  STD_LOGIC;
                signal clock_crossing_write_s1_arb_counter_enable :  STD_LOGIC;
                signal clock_crossing_write_s1_arb_share_counter :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_write_s1_arb_share_counter_next_value :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_write_s1_arb_share_set_values :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_write_s1_bbt_burstcounter :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal clock_crossing_write_s1_beginbursttransfer_internal :  STD_LOGIC;
                signal clock_crossing_write_s1_begins_xfer :  STD_LOGIC;
                signal clock_crossing_write_s1_end_xfer :  STD_LOGIC;
                signal clock_crossing_write_s1_firsttransfer :  STD_LOGIC;
                signal clock_crossing_write_s1_grant_vector :  STD_LOGIC;
                signal clock_crossing_write_s1_in_a_read_cycle :  STD_LOGIC;
                signal clock_crossing_write_s1_in_a_write_cycle :  STD_LOGIC;
                signal clock_crossing_write_s1_master_qreq_vector :  STD_LOGIC;
                signal clock_crossing_write_s1_next_bbt_burstcount :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal clock_crossing_write_s1_non_bursting_master_requests :  STD_LOGIC;
                signal clock_crossing_write_s1_reg_firsttransfer :  STD_LOGIC;
                signal clock_crossing_write_s1_slavearbiterlockenable :  STD_LOGIC;
                signal clock_crossing_write_s1_slavearbiterlockenable2 :  STD_LOGIC;
                signal clock_crossing_write_s1_unreg_firsttransfer :  STD_LOGIC;
                signal clock_crossing_write_s1_waits_for_read :  STD_LOGIC;
                signal clock_crossing_write_s1_waits_for_write :  STD_LOGIC;
                signal d1_reasons_to_wait :  STD_LOGIC;
                signal enable_nonzero_assertions :  STD_LOGIC;
                signal end_xfer_arb_share_counter_term_clock_crossing_write_s1 :  STD_LOGIC;
                signal in_a_read_cycle :  STD_LOGIC;
                signal in_a_write_cycle :  STD_LOGIC;
                signal internal_clock_crossing_write_s1_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal internal_clock_crossing_write_s1_read :  STD_LOGIC;
                signal internal_clock_crossing_write_s1_waitrequest_from_sa :  STD_LOGIC;
                signal internal_clock_crossing_write_s1_write :  STD_LOGIC;
                signal internal_master_write_granted_clock_crossing_write_s1 :  STD_LOGIC;
                signal internal_master_write_qualified_request_clock_crossing_write_s1 :  STD_LOGIC;
                signal internal_master_write_requests_clock_crossing_write_s1 :  STD_LOGIC;
                signal master_write_avalon_master_arbiterlock :  STD_LOGIC;
                signal master_write_avalon_master_arbiterlock2 :  STD_LOGIC;
                signal master_write_avalon_master_continuerequest :  STD_LOGIC;
                signal master_write_saved_grant_clock_crossing_write_s1 :  STD_LOGIC;
                signal shifted_address_to_clock_crossing_write_s1_from_master_write_avalon_master :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal wait_for_clock_crossing_write_s1_counter :  STD_LOGIC;

begin

  process (clk, reset_n)
  begin
    if reset_n = '0' then
      d1_reasons_to_wait <= std_logic'('0');
    elsif clk'event and clk = '1' then
      d1_reasons_to_wait <= NOT clock_crossing_write_s1_end_xfer;
    end if;

  end process;

  clock_crossing_write_s1_begins_xfer <= NOT d1_reasons_to_wait AND (internal_master_write_qualified_request_clock_crossing_write_s1);
  --assign clock_crossing_write_s1_readdata_from_sa = clock_crossing_write_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  clock_crossing_write_s1_readdata_from_sa <= clock_crossing_write_s1_readdata;
  internal_master_write_requests_clock_crossing_write_s1 <= Vector_To_Std_Logic(((((std_logic_vector'("00000000000000000000000000000001")) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((master_write_avalon_master_write)))))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(master_write_avalon_master_write)))));
  --assign clock_crossing_write_s1_waitrequest_from_sa = clock_crossing_write_s1_waitrequest so that symbol knows where to group signals which may go to master only, which is an e_assign
  internal_clock_crossing_write_s1_waitrequest_from_sa <= clock_crossing_write_s1_waitrequest;
  --clock_crossing_write_s1_arb_share_counter set values, which is an e_mux
  clock_crossing_write_s1_arb_share_set_values <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_master_write_granted_clock_crossing_write_s1)) = '1'), (std_logic_vector'("0000000000000000000000000000") & (master_write_avalon_master_burstcount)), std_logic_vector'("00000000000000000000000000000001")), 4);
  --clock_crossing_write_s1_non_bursting_master_requests mux, which is an e_mux
  clock_crossing_write_s1_non_bursting_master_requests <= std_logic'('0');
  --clock_crossing_write_s1_any_bursting_master_saved_grant mux, which is an e_mux
  clock_crossing_write_s1_any_bursting_master_saved_grant <= master_write_saved_grant_clock_crossing_write_s1;
  --clock_crossing_write_s1_arb_share_counter_next_value assignment, which is an e_assign
  clock_crossing_write_s1_arb_share_counter_next_value <= A_EXT (A_WE_StdLogicVector((std_logic'(clock_crossing_write_s1_firsttransfer) = '1'), (((std_logic_vector'("00000000000000000000000000000") & (clock_crossing_write_s1_arb_share_set_values)) - std_logic_vector'("000000000000000000000000000000001"))), A_WE_StdLogicVector((std_logic'(or_reduce(clock_crossing_write_s1_arb_share_counter)) = '1'), (((std_logic_vector'("00000000000000000000000000000") & (clock_crossing_write_s1_arb_share_counter)) - std_logic_vector'("000000000000000000000000000000001"))), std_logic_vector'("000000000000000000000000000000000"))), 4);
  --clock_crossing_write_s1_allgrants all slave grants, which is an e_mux
  clock_crossing_write_s1_allgrants <= clock_crossing_write_s1_grant_vector;
  --clock_crossing_write_s1_end_xfer assignment, which is an e_assign
  clock_crossing_write_s1_end_xfer <= NOT ((clock_crossing_write_s1_waits_for_read OR clock_crossing_write_s1_waits_for_write));
  --end_xfer_arb_share_counter_term_clock_crossing_write_s1 arb share counter enable term, which is an e_assign
  end_xfer_arb_share_counter_term_clock_crossing_write_s1 <= clock_crossing_write_s1_end_xfer AND (((NOT clock_crossing_write_s1_any_bursting_master_saved_grant OR in_a_read_cycle) OR in_a_write_cycle));
  --clock_crossing_write_s1_arb_share_counter arbitration counter enable, which is an e_assign
  clock_crossing_write_s1_arb_counter_enable <= ((end_xfer_arb_share_counter_term_clock_crossing_write_s1 AND clock_crossing_write_s1_allgrants)) OR ((end_xfer_arb_share_counter_term_clock_crossing_write_s1 AND NOT clock_crossing_write_s1_non_bursting_master_requests));
  --clock_crossing_write_s1_arb_share_counter counter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      clock_crossing_write_s1_arb_share_counter <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'(clock_crossing_write_s1_arb_counter_enable) = '1' then 
        clock_crossing_write_s1_arb_share_counter <= clock_crossing_write_s1_arb_share_counter_next_value;
      end if;
    end if;

  end process;

  --clock_crossing_write_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      clock_crossing_write_s1_slavearbiterlockenable <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clock_crossing_write_s1_master_qreq_vector AND end_xfer_arb_share_counter_term_clock_crossing_write_s1)) OR ((end_xfer_arb_share_counter_term_clock_crossing_write_s1 AND NOT clock_crossing_write_s1_non_bursting_master_requests)))) = '1' then 
        clock_crossing_write_s1_slavearbiterlockenable <= or_reduce(clock_crossing_write_s1_arb_share_counter_next_value);
      end if;
    end if;

  end process;

  --master_write/avalon_master clock_crossing_write/s1 arbiterlock, which is an e_assign
  master_write_avalon_master_arbiterlock <= clock_crossing_write_s1_slavearbiterlockenable AND master_write_avalon_master_continuerequest;
  --clock_crossing_write_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  clock_crossing_write_s1_slavearbiterlockenable2 <= or_reduce(clock_crossing_write_s1_arb_share_counter_next_value);
  --master_write/avalon_master clock_crossing_write/s1 arbiterlock2, which is an e_assign
  master_write_avalon_master_arbiterlock2 <= clock_crossing_write_s1_slavearbiterlockenable2 AND master_write_avalon_master_continuerequest;
  --clock_crossing_write_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  clock_crossing_write_s1_any_continuerequest <= std_logic'('1');
  --master_write_avalon_master_continuerequest continued request, which is an e_assign
  master_write_avalon_master_continuerequest <= std_logic'('1');
  internal_master_write_qualified_request_clock_crossing_write_s1 <= internal_master_write_requests_clock_crossing_write_s1;
  --clock_crossing_write_s1_writedata mux, which is an e_mux
  clock_crossing_write_s1_writedata <= master_write_avalon_master_writedata;
  --assign clock_crossing_write_s1_endofpacket_from_sa = clock_crossing_write_s1_endofpacket so that symbol knows where to group signals which may go to master only, which is an e_assign
  clock_crossing_write_s1_endofpacket_from_sa <= clock_crossing_write_s1_endofpacket;
  --master is always granted when requested
  internal_master_write_granted_clock_crossing_write_s1 <= internal_master_write_qualified_request_clock_crossing_write_s1;
  --master_write/avalon_master saved-grant clock_crossing_write/s1, which is an e_assign
  master_write_saved_grant_clock_crossing_write_s1 <= internal_master_write_requests_clock_crossing_write_s1;
  --allow new arb cycle for clock_crossing_write/s1, which is an e_assign
  clock_crossing_write_s1_allow_new_arb_cycle <= std_logic'('1');
  --placeholder chosen master
  clock_crossing_write_s1_grant_vector <= std_logic'('1');
  --placeholder vector of master qualified-requests
  clock_crossing_write_s1_master_qreq_vector <= std_logic'('1');
  --clock_crossing_write_s1_reset_n assignment, which is an e_assign
  clock_crossing_write_s1_reset_n <= reset_n;
  --clock_crossing_write_s1_firsttransfer first transaction, which is an e_assign
  clock_crossing_write_s1_firsttransfer <= A_WE_StdLogic((std_logic'(clock_crossing_write_s1_begins_xfer) = '1'), clock_crossing_write_s1_unreg_firsttransfer, clock_crossing_write_s1_reg_firsttransfer);
  --clock_crossing_write_s1_unreg_firsttransfer first transaction, which is an e_assign
  clock_crossing_write_s1_unreg_firsttransfer <= NOT ((clock_crossing_write_s1_slavearbiterlockenable AND clock_crossing_write_s1_any_continuerequest));
  --clock_crossing_write_s1_reg_firsttransfer first transaction, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      clock_crossing_write_s1_reg_firsttransfer <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'(clock_crossing_write_s1_begins_xfer) = '1' then 
        clock_crossing_write_s1_reg_firsttransfer <= clock_crossing_write_s1_unreg_firsttransfer;
      end if;
    end if;

  end process;

  --clock_crossing_write_s1_next_bbt_burstcount next_bbt_burstcount, which is an e_mux
  clock_crossing_write_s1_next_bbt_burstcount <= A_EXT (A_WE_StdLogicVector((std_logic'((((internal_clock_crossing_write_s1_write) AND to_std_logic((((std_logic_vector'("00000000000000000000000000000") & (clock_crossing_write_s1_bbt_burstcounter)) = std_logic_vector'("00000000000000000000000000000000"))))))) = '1'), (((std_logic_vector'("00000000000000000000000000000") & (internal_clock_crossing_write_s1_burstcount)) - std_logic_vector'("000000000000000000000000000000001"))), A_WE_StdLogicVector((std_logic'((((internal_clock_crossing_write_s1_read) AND to_std_logic((((std_logic_vector'("00000000000000000000000000000") & (clock_crossing_write_s1_bbt_burstcounter)) = std_logic_vector'("00000000000000000000000000000000"))))))) = '1'), std_logic_vector'("000000000000000000000000000000000"), (((std_logic_vector'("000000000000000000000000000000") & (clock_crossing_write_s1_bbt_burstcounter)) - std_logic_vector'("000000000000000000000000000000001"))))), 3);
  --clock_crossing_write_s1_bbt_burstcounter bbt_burstcounter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      clock_crossing_write_s1_bbt_burstcounter <= std_logic_vector'("000");
    elsif clk'event and clk = '1' then
      if std_logic'(clock_crossing_write_s1_begins_xfer) = '1' then 
        clock_crossing_write_s1_bbt_burstcounter <= clock_crossing_write_s1_next_bbt_burstcount;
      end if;
    end if;

  end process;

  --clock_crossing_write_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  clock_crossing_write_s1_beginbursttransfer_internal <= clock_crossing_write_s1_begins_xfer AND to_std_logic((((std_logic_vector'("00000000000000000000000000000") & (clock_crossing_write_s1_bbt_burstcounter)) = std_logic_vector'("00000000000000000000000000000000"))));
  --clock_crossing_write_s1_read assignment, which is an e_mux
  internal_clock_crossing_write_s1_read <= std_logic'('0');
  --clock_crossing_write_s1_write assignment, which is an e_mux
  internal_clock_crossing_write_s1_write <= internal_master_write_granted_clock_crossing_write_s1 AND master_write_avalon_master_write;
  shifted_address_to_clock_crossing_write_s1_from_master_write_avalon_master <= master_write_avalon_master_address_to_slave;
  --clock_crossing_write_s1_address mux, which is an e_mux
  clock_crossing_write_s1_address <= A_EXT (A_SRL(shifted_address_to_clock_crossing_write_s1_from_master_write_avalon_master,std_logic_vector'("00000000000000000000000000000010")), 22);
  --slaveid clock_crossing_write_s1_nativeaddress nativeaddress mux, which is an e_mux
  clock_crossing_write_s1_nativeaddress <= A_EXT (A_SRL(master_write_avalon_master_address_to_slave,std_logic_vector'("00000000000000000000000000000010")), 22);
  --d1_clock_crossing_write_s1_end_xfer register, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      d1_clock_crossing_write_s1_end_xfer <= std_logic'('1');
    elsif clk'event and clk = '1' then
      d1_clock_crossing_write_s1_end_xfer <= clock_crossing_write_s1_end_xfer;
    end if;

  end process;

  --clock_crossing_write_s1_waits_for_read in a cycle, which is an e_mux
  clock_crossing_write_s1_waits_for_read <= clock_crossing_write_s1_in_a_read_cycle AND internal_clock_crossing_write_s1_waitrequest_from_sa;
  --clock_crossing_write_s1_in_a_read_cycle assignment, which is an e_assign
  clock_crossing_write_s1_in_a_read_cycle <= std_logic'('0');
  --in_a_read_cycle assignment, which is an e_mux
  in_a_read_cycle <= clock_crossing_write_s1_in_a_read_cycle;
  --clock_crossing_write_s1_waits_for_write in a cycle, which is an e_mux
  clock_crossing_write_s1_waits_for_write <= clock_crossing_write_s1_in_a_write_cycle AND internal_clock_crossing_write_s1_waitrequest_from_sa;
  --assign clock_crossing_write_s1_readdatavalid_from_sa = clock_crossing_write_s1_readdatavalid so that symbol knows where to group signals which may go to master only, which is an e_assign
  clock_crossing_write_s1_readdatavalid_from_sa <= clock_crossing_write_s1_readdatavalid;
  --clock_crossing_write_s1_in_a_write_cycle assignment, which is an e_assign
  clock_crossing_write_s1_in_a_write_cycle <= internal_master_write_granted_clock_crossing_write_s1 AND master_write_avalon_master_write;
  --in_a_write_cycle assignment, which is an e_mux
  in_a_write_cycle <= clock_crossing_write_s1_in_a_write_cycle;
  wait_for_clock_crossing_write_s1_counter <= std_logic'('0');
  --clock_crossing_write_s1_byteenable byte enable port mux, which is an e_mux
  clock_crossing_write_s1_byteenable <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_master_write_granted_clock_crossing_write_s1)) = '1'), (std_logic_vector'("0000000000000000000000000000") & (master_write_avalon_master_byteenable)), -SIGNED(std_logic_vector'("00000000000000000000000000000001"))), 4);
  --burstcount mux, which is an e_mux
  internal_clock_crossing_write_s1_burstcount <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_master_write_granted_clock_crossing_write_s1)) = '1'), (std_logic_vector'("0000000000000000000000000000") & (master_write_avalon_master_burstcount)), std_logic_vector'("00000000000000000000000000000001")), 4);
  --vhdl renameroo for output signals
  clock_crossing_write_s1_burstcount <= internal_clock_crossing_write_s1_burstcount;
  --vhdl renameroo for output signals
  clock_crossing_write_s1_read <= internal_clock_crossing_write_s1_read;
  --vhdl renameroo for output signals
  clock_crossing_write_s1_waitrequest_from_sa <= internal_clock_crossing_write_s1_waitrequest_from_sa;
  --vhdl renameroo for output signals
  clock_crossing_write_s1_write <= internal_clock_crossing_write_s1_write;
  --vhdl renameroo for output signals
  master_write_granted_clock_crossing_write_s1 <= internal_master_write_granted_clock_crossing_write_s1;
  --vhdl renameroo for output signals
  master_write_qualified_request_clock_crossing_write_s1 <= internal_master_write_qualified_request_clock_crossing_write_s1;
  --vhdl renameroo for output signals
  master_write_requests_clock_crossing_write_s1 <= internal_master_write_requests_clock_crossing_write_s1;
--synthesis translate_off
    --clock_crossing_write/s1 enable non-zero assertions, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        enable_nonzero_assertions <= std_logic'('0');
      elsif clk'event and clk = '1' then
        enable_nonzero_assertions <= std_logic'('1');
      end if;

    end process;

    --master_write/avalon_master non-zero burstcount assertion, which is an e_process
    process (clk)
    VARIABLE write_line7 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((internal_master_write_requests_clock_crossing_write_s1 AND to_std_logic((((std_logic_vector'("0000000000000000000000000000") & (master_write_avalon_master_burstcount)) = std_logic_vector'("00000000000000000000000000000000"))))) AND enable_nonzero_assertions)) = '1' then 
          write(write_line7, now);
          write(write_line7, string'(": "));
          write(write_line7, string'("master_write/avalon_master drove 0 on its 'burstcount' port while accessing slave clock_crossing_write/s1"));
          write(output, write_line7.all);
          deallocate (write_line7);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

--synthesis translate_on

end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library std;
use std.textio.all;

entity clock_crossing_write_m1_arbitrator is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal clock_crossing_write_m1_address : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal clock_crossing_write_m1_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal clock_crossing_write_m1_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal clock_crossing_write_m1_byteenable_system_burst_0_upstream : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal clock_crossing_write_m1_granted_system_burst_0_upstream : IN STD_LOGIC;
                 signal clock_crossing_write_m1_qualified_request_system_burst_0_upstream : IN STD_LOGIC;
                 signal clock_crossing_write_m1_read : IN STD_LOGIC;
                 signal clock_crossing_write_m1_read_data_valid_system_burst_0_upstream : IN STD_LOGIC;
                 signal clock_crossing_write_m1_read_data_valid_system_burst_0_upstream_shift_register : IN STD_LOGIC;
                 signal clock_crossing_write_m1_requests_system_burst_0_upstream : IN STD_LOGIC;
                 signal clock_crossing_write_m1_write : IN STD_LOGIC;
                 signal clock_crossing_write_m1_writedata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal d1_system_burst_0_upstream_end_xfer : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal system_burst_0_upstream_readdata_from_sa : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal system_burst_0_upstream_waitrequest_from_sa : IN STD_LOGIC;

              -- outputs:
                 signal clock_crossing_write_m1_address_to_slave : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal clock_crossing_write_m1_dbs_address : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal clock_crossing_write_m1_dbs_write_16 : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal clock_crossing_write_m1_latency_counter : OUT STD_LOGIC;
                 signal clock_crossing_write_m1_readdata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal clock_crossing_write_m1_readdatavalid : OUT STD_LOGIC;
                 signal clock_crossing_write_m1_reset_n : OUT STD_LOGIC;
                 signal clock_crossing_write_m1_waitrequest : OUT STD_LOGIC
              );
end entity clock_crossing_write_m1_arbitrator;


architecture europa of clock_crossing_write_m1_arbitrator is
                signal active_and_waiting_last_time :  STD_LOGIC;
                signal clock_crossing_write_m1_address_last_time :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal clock_crossing_write_m1_burstcount_last_time :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_write_m1_byteenable_last_time :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_write_m1_dbs_increment :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_write_m1_dbs_rdv_counter :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_write_m1_dbs_rdv_counter_inc :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_write_m1_next_dbs_rdv_counter :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_write_m1_read_last_time :  STD_LOGIC;
                signal clock_crossing_write_m1_run :  STD_LOGIC;
                signal clock_crossing_write_m1_write_last_time :  STD_LOGIC;
                signal clock_crossing_write_m1_writedata_last_time :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal dbs_count_enable :  STD_LOGIC;
                signal dbs_counter_overflow :  STD_LOGIC;
                signal dbs_latent_16_reg_segment_0 :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal dbs_rdv_count_enable :  STD_LOGIC;
                signal dbs_rdv_counter_overflow :  STD_LOGIC;
                signal internal_clock_crossing_write_m1_address_to_slave :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal internal_clock_crossing_write_m1_dbs_address :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal internal_clock_crossing_write_m1_waitrequest :  STD_LOGIC;
                signal next_dbs_address :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal p1_dbs_latent_16_reg_segment_0 :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal pre_dbs_count_enable :  STD_LOGIC;
                signal pre_flush_clock_crossing_write_m1_readdatavalid :  STD_LOGIC;
                signal r_0 :  STD_LOGIC;

begin

  --r_0 master_run cascaded wait assignment, which is an e_assign
  r_0 <= Vector_To_Std_Logic((((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((clock_crossing_write_m1_qualified_request_system_burst_0_upstream OR NOT clock_crossing_write_m1_requests_system_burst_0_upstream)))))) AND (((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((NOT clock_crossing_write_m1_qualified_request_system_burst_0_upstream OR NOT clock_crossing_write_m1_read)))) OR (((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT system_burst_0_upstream_waitrequest_from_sa)))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(clock_crossing_write_m1_read)))))))) AND (((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((NOT clock_crossing_write_m1_qualified_request_system_burst_0_upstream OR NOT clock_crossing_write_m1_write)))) OR ((((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT system_burst_0_upstream_waitrequest_from_sa)))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((internal_clock_crossing_write_m1_dbs_address(1)))))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(clock_crossing_write_m1_write)))))))));
  --cascaded wait assignment, which is an e_assign
  clock_crossing_write_m1_run <= r_0;
  --optimize select-logic by passing only those address bits which matter.
  internal_clock_crossing_write_m1_address_to_slave <= clock_crossing_write_m1_address(23 DOWNTO 0);
  --latent slave read data valids which may be flushed, which is an e_mux
  pre_flush_clock_crossing_write_m1_readdatavalid <= clock_crossing_write_m1_read_data_valid_system_burst_0_upstream AND dbs_rdv_counter_overflow;
  --latent slave read data valid which is not flushed, which is an e_mux
  clock_crossing_write_m1_readdatavalid <= Vector_To_Std_Logic((std_logic_vector'("00000000000000000000000000000000") OR (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(pre_flush_clock_crossing_write_m1_readdatavalid)))));
  --input to latent dbs-16 stored 0, which is an e_mux
  p1_dbs_latent_16_reg_segment_0 <= system_burst_0_upstream_readdata_from_sa;
  --dbs register for latent dbs-16 segment 0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      dbs_latent_16_reg_segment_0 <= std_logic_vector'("0000000000000000");
    elsif clk'event and clk = '1' then
      if std_logic'((dbs_rdv_count_enable AND to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((clock_crossing_write_m1_dbs_rdv_counter(1))))) = std_logic_vector'("00000000000000000000000000000000")))))) = '1' then 
        dbs_latent_16_reg_segment_0 <= p1_dbs_latent_16_reg_segment_0;
      end if;
    end if;

  end process;

  --clock_crossing_write/m1 readdata mux, which is an e_mux
  clock_crossing_write_m1_readdata <= Std_Logic_Vector'(system_burst_0_upstream_readdata_from_sa(15 DOWNTO 0) & dbs_latent_16_reg_segment_0);
  --mux write dbs 1, which is an e_mux
  clock_crossing_write_m1_dbs_write_16 <= A_WE_StdLogicVector((std_logic'((internal_clock_crossing_write_m1_dbs_address(1))) = '1'), clock_crossing_write_m1_writedata(31 DOWNTO 16), clock_crossing_write_m1_writedata(15 DOWNTO 0));
  --actual waitrequest port, which is an e_assign
  internal_clock_crossing_write_m1_waitrequest <= NOT clock_crossing_write_m1_run;
  --latent max counter, which is an e_assign
  clock_crossing_write_m1_latency_counter <= std_logic'('0');
  --clock_crossing_write_m1_reset_n assignment, which is an e_assign
  clock_crossing_write_m1_reset_n <= reset_n;
  --dbs count increment, which is an e_mux
  clock_crossing_write_m1_dbs_increment <= A_EXT (A_WE_StdLogicVector((std_logic'((clock_crossing_write_m1_requests_system_burst_0_upstream)) = '1'), std_logic_vector'("00000000000000000000000000000010"), std_logic_vector'("00000000000000000000000000000000")), 2);
  --dbs counter overflow, which is an e_assign
  dbs_counter_overflow <= internal_clock_crossing_write_m1_dbs_address(1) AND NOT((next_dbs_address(1)));
  --next master address, which is an e_assign
  next_dbs_address <= A_EXT (((std_logic_vector'("0") & (internal_clock_crossing_write_m1_dbs_address)) + (std_logic_vector'("0") & (clock_crossing_write_m1_dbs_increment))), 2);
  --dbs count enable, which is an e_mux
  dbs_count_enable <= pre_dbs_count_enable;
  --dbs counter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      internal_clock_crossing_write_m1_dbs_address <= std_logic_vector'("00");
    elsif clk'event and clk = '1' then
      if std_logic'(dbs_count_enable) = '1' then 
        internal_clock_crossing_write_m1_dbs_address <= next_dbs_address;
      end if;
    end if;

  end process;

  --p1 dbs rdv counter, which is an e_assign
  clock_crossing_write_m1_next_dbs_rdv_counter <= A_EXT (((std_logic_vector'("0") & (clock_crossing_write_m1_dbs_rdv_counter)) + (std_logic_vector'("0") & (clock_crossing_write_m1_dbs_rdv_counter_inc))), 2);
  --clock_crossing_write_m1_rdv_inc_mux, which is an e_mux
  clock_crossing_write_m1_dbs_rdv_counter_inc <= std_logic_vector'("10");
  --master any slave rdv, which is an e_mux
  dbs_rdv_count_enable <= clock_crossing_write_m1_read_data_valid_system_burst_0_upstream;
  --dbs rdv counter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      clock_crossing_write_m1_dbs_rdv_counter <= std_logic_vector'("00");
    elsif clk'event and clk = '1' then
      if std_logic'(dbs_rdv_count_enable) = '1' then 
        clock_crossing_write_m1_dbs_rdv_counter <= clock_crossing_write_m1_next_dbs_rdv_counter;
      end if;
    end if;

  end process;

  --dbs rdv counter overflow, which is an e_assign
  dbs_rdv_counter_overflow <= clock_crossing_write_m1_dbs_rdv_counter(1) AND NOT clock_crossing_write_m1_next_dbs_rdv_counter(1);
  --pre dbs count enable, which is an e_mux
  pre_dbs_count_enable <= Vector_To_Std_Logic(((((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((clock_crossing_write_m1_granted_system_burst_0_upstream AND clock_crossing_write_m1_read)))) AND std_logic_vector'("00000000000000000000000000000000")) AND std_logic_vector'("00000000000000000000000000000001")) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT system_burst_0_upstream_waitrequest_from_sa))))) OR (((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((clock_crossing_write_m1_granted_system_burst_0_upstream AND clock_crossing_write_m1_write)))) AND std_logic_vector'("00000000000000000000000000000001")) AND std_logic_vector'("00000000000000000000000000000001")) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT system_burst_0_upstream_waitrequest_from_sa)))))));
  --vhdl renameroo for output signals
  clock_crossing_write_m1_address_to_slave <= internal_clock_crossing_write_m1_address_to_slave;
  --vhdl renameroo for output signals
  clock_crossing_write_m1_dbs_address <= internal_clock_crossing_write_m1_dbs_address;
  --vhdl renameroo for output signals
  clock_crossing_write_m1_waitrequest <= internal_clock_crossing_write_m1_waitrequest;
--synthesis translate_off
    --clock_crossing_write_m1_address check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        clock_crossing_write_m1_address_last_time <= std_logic_vector'("000000000000000000000000");
      elsif clk'event and clk = '1' then
        clock_crossing_write_m1_address_last_time <= clock_crossing_write_m1_address;
      end if;

    end process;

    --clock_crossing_write/m1 waited last time, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        active_and_waiting_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        active_and_waiting_last_time <= internal_clock_crossing_write_m1_waitrequest AND ((clock_crossing_write_m1_read OR clock_crossing_write_m1_write));
      end if;

    end process;

    --clock_crossing_write_m1_address matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line8 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((clock_crossing_write_m1_address /= clock_crossing_write_m1_address_last_time))))) = '1' then 
          write(write_line8, now);
          write(write_line8, string'(": "));
          write(write_line8, string'("clock_crossing_write_m1_address did not heed wait!!!"));
          write(output, write_line8.all);
          deallocate (write_line8);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --clock_crossing_write_m1_burstcount check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        clock_crossing_write_m1_burstcount_last_time <= std_logic_vector'("0000");
      elsif clk'event and clk = '1' then
        clock_crossing_write_m1_burstcount_last_time <= clock_crossing_write_m1_burstcount;
      end if;

    end process;

    --clock_crossing_write_m1_burstcount matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line9 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((clock_crossing_write_m1_burstcount /= clock_crossing_write_m1_burstcount_last_time))))) = '1' then 
          write(write_line9, now);
          write(write_line9, string'(": "));
          write(write_line9, string'("clock_crossing_write_m1_burstcount did not heed wait!!!"));
          write(output, write_line9.all);
          deallocate (write_line9);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --clock_crossing_write_m1_byteenable check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        clock_crossing_write_m1_byteenable_last_time <= std_logic_vector'("0000");
      elsif clk'event and clk = '1' then
        clock_crossing_write_m1_byteenable_last_time <= clock_crossing_write_m1_byteenable;
      end if;

    end process;

    --clock_crossing_write_m1_byteenable matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line10 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((clock_crossing_write_m1_byteenable /= clock_crossing_write_m1_byteenable_last_time))))) = '1' then 
          write(write_line10, now);
          write(write_line10, string'(": "));
          write(write_line10, string'("clock_crossing_write_m1_byteenable did not heed wait!!!"));
          write(output, write_line10.all);
          deallocate (write_line10);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --clock_crossing_write_m1_read check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        clock_crossing_write_m1_read_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        clock_crossing_write_m1_read_last_time <= clock_crossing_write_m1_read;
      end if;

    end process;

    --clock_crossing_write_m1_read matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line11 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((std_logic'(clock_crossing_write_m1_read) /= std_logic'(clock_crossing_write_m1_read_last_time)))))) = '1' then 
          write(write_line11, now);
          write(write_line11, string'(": "));
          write(write_line11, string'("clock_crossing_write_m1_read did not heed wait!!!"));
          write(output, write_line11.all);
          deallocate (write_line11);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --clock_crossing_write_m1_write check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        clock_crossing_write_m1_write_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        clock_crossing_write_m1_write_last_time <= clock_crossing_write_m1_write;
      end if;

    end process;

    --clock_crossing_write_m1_write matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line12 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((std_logic'(clock_crossing_write_m1_write) /= std_logic'(clock_crossing_write_m1_write_last_time)))))) = '1' then 
          write(write_line12, now);
          write(write_line12, string'(": "));
          write(write_line12, string'("clock_crossing_write_m1_write did not heed wait!!!"));
          write(output, write_line12.all);
          deallocate (write_line12);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --clock_crossing_write_m1_writedata check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        clock_crossing_write_m1_writedata_last_time <= std_logic_vector'("00000000000000000000000000000000");
      elsif clk'event and clk = '1' then
        clock_crossing_write_m1_writedata_last_time <= clock_crossing_write_m1_writedata;
      end if;

    end process;

    --clock_crossing_write_m1_writedata matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line13 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((active_and_waiting_last_time AND to_std_logic(((clock_crossing_write_m1_writedata /= clock_crossing_write_m1_writedata_last_time)))) AND clock_crossing_write_m1_write)) = '1' then 
          write(write_line13, now);
          write(write_line13, string'(": "));
          write(write_line13, string'("clock_crossing_write_m1_writedata did not heed wait!!!"));
          write(output, write_line13.all);
          deallocate (write_line13);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

--synthesis translate_on

end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity clock_crossing_write_bridge_arbitrator is 
end entity clock_crossing_write_bridge_arbitrator;


architecture europa of clock_crossing_write_bridge_arbitrator is

begin


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library std;
use std.textio.all;

entity master_read_avalon_master_arbitrator is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal clock_crossing_read_s1_readdata_from_sa : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal clock_crossing_read_s1_waitrequest_from_sa : IN STD_LOGIC;
                 signal d1_clock_crossing_read_s1_end_xfer : IN STD_LOGIC;
                 signal master_read_avalon_master_address : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal master_read_avalon_master_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal master_read_avalon_master_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal master_read_avalon_master_read : IN STD_LOGIC;
                 signal master_read_granted_clock_crossing_read_s1 : IN STD_LOGIC;
                 signal master_read_qualified_request_clock_crossing_read_s1 : IN STD_LOGIC;
                 signal master_read_read_data_valid_clock_crossing_read_s1 : IN STD_LOGIC;
                 signal master_read_read_data_valid_clock_crossing_read_s1_shift_register : IN STD_LOGIC;
                 signal master_read_requests_clock_crossing_read_s1 : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;

              -- outputs:
                 signal master_read_avalon_master_address_to_slave : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal master_read_avalon_master_readdata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal master_read_avalon_master_readdatavalid : OUT STD_LOGIC;
                 signal master_read_avalon_master_reset : OUT STD_LOGIC;
                 signal master_read_avalon_master_waitrequest : OUT STD_LOGIC;
                 signal master_read_latency_counter : OUT STD_LOGIC
              );
end entity master_read_avalon_master_arbitrator;


architecture europa of master_read_avalon_master_arbitrator is
                signal active_and_waiting_last_time :  STD_LOGIC;
                signal internal_master_read_avalon_master_address_to_slave :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal internal_master_read_avalon_master_waitrequest :  STD_LOGIC;
                signal master_read_avalon_master_address_last_time :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal master_read_avalon_master_burstcount_last_time :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal master_read_avalon_master_byteenable_last_time :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal master_read_avalon_master_read_last_time :  STD_LOGIC;
                signal master_read_avalon_master_run :  STD_LOGIC;
                signal pre_flush_master_read_avalon_master_readdatavalid :  STD_LOGIC;
                signal r_0 :  STD_LOGIC;

begin

  --r_0 master_run cascaded wait assignment, which is an e_assign
  r_0 <= Vector_To_Std_Logic(((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((master_read_qualified_request_clock_crossing_read_s1 OR NOT master_read_requests_clock_crossing_read_s1)))))) AND (((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((NOT master_read_qualified_request_clock_crossing_read_s1 OR NOT (master_read_avalon_master_read))))) OR (((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT clock_crossing_read_s1_waitrequest_from_sa)))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((master_read_avalon_master_read))))))))));
  --cascaded wait assignment, which is an e_assign
  master_read_avalon_master_run <= r_0;
  --optimize select-logic by passing only those address bits which matter.
  internal_master_read_avalon_master_address_to_slave <= master_read_avalon_master_address(23 DOWNTO 0);
  --latent slave read data valids which may be flushed, which is an e_mux
  pre_flush_master_read_avalon_master_readdatavalid <= master_read_read_data_valid_clock_crossing_read_s1;
  --latent slave read data valid which is not flushed, which is an e_mux
  master_read_avalon_master_readdatavalid <= Vector_To_Std_Logic((std_logic_vector'("00000000000000000000000000000000") OR (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(pre_flush_master_read_avalon_master_readdatavalid)))));
  --master_read/avalon_master readdata mux, which is an e_mux
  master_read_avalon_master_readdata <= clock_crossing_read_s1_readdata_from_sa;
  --actual waitrequest port, which is an e_assign
  internal_master_read_avalon_master_waitrequest <= NOT master_read_avalon_master_run;
  --latent max counter, which is an e_assign
  master_read_latency_counter <= std_logic'('0');
  --~master_read_avalon_master_reset assignment, which is an e_assign
  master_read_avalon_master_reset <= NOT reset_n;
  --vhdl renameroo for output signals
  master_read_avalon_master_address_to_slave <= internal_master_read_avalon_master_address_to_slave;
  --vhdl renameroo for output signals
  master_read_avalon_master_waitrequest <= internal_master_read_avalon_master_waitrequest;
--synthesis translate_off
    --master_read_avalon_master_address check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        master_read_avalon_master_address_last_time <= std_logic_vector'("000000000000000000000000");
      elsif clk'event and clk = '1' then
        master_read_avalon_master_address_last_time <= master_read_avalon_master_address;
      end if;

    end process;

    --master_read/avalon_master waited last time, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        active_and_waiting_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        active_and_waiting_last_time <= internal_master_read_avalon_master_waitrequest AND (master_read_avalon_master_read);
      end if;

    end process;

    --master_read_avalon_master_address matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line14 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((master_read_avalon_master_address /= master_read_avalon_master_address_last_time))))) = '1' then 
          write(write_line14, now);
          write(write_line14, string'(": "));
          write(write_line14, string'("master_read_avalon_master_address did not heed wait!!!"));
          write(output, write_line14.all);
          deallocate (write_line14);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --master_read_avalon_master_burstcount check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        master_read_avalon_master_burstcount_last_time <= std_logic_vector'("0000");
      elsif clk'event and clk = '1' then
        master_read_avalon_master_burstcount_last_time <= master_read_avalon_master_burstcount;
      end if;

    end process;

    --master_read_avalon_master_burstcount matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line15 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((master_read_avalon_master_burstcount /= master_read_avalon_master_burstcount_last_time))))) = '1' then 
          write(write_line15, now);
          write(write_line15, string'(": "));
          write(write_line15, string'("master_read_avalon_master_burstcount did not heed wait!!!"));
          write(output, write_line15.all);
          deallocate (write_line15);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --master_read_avalon_master_byteenable check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        master_read_avalon_master_byteenable_last_time <= std_logic_vector'("0000");
      elsif clk'event and clk = '1' then
        master_read_avalon_master_byteenable_last_time <= master_read_avalon_master_byteenable;
      end if;

    end process;

    --master_read_avalon_master_byteenable matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line16 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((master_read_avalon_master_byteenable /= master_read_avalon_master_byteenable_last_time))))) = '1' then 
          write(write_line16, now);
          write(write_line16, string'(": "));
          write(write_line16, string'("master_read_avalon_master_byteenable did not heed wait!!!"));
          write(output, write_line16.all);
          deallocate (write_line16);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --master_read_avalon_master_read check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        master_read_avalon_master_read_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        master_read_avalon_master_read_last_time <= master_read_avalon_master_read;
      end if;

    end process;

    --master_read_avalon_master_read matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line17 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((std_logic'(master_read_avalon_master_read) /= std_logic'(master_read_avalon_master_read_last_time)))))) = '1' then 
          write(write_line17, now);
          write(write_line17, string'(": "));
          write(write_line17, string'("master_read_avalon_master_read did not heed wait!!!"));
          write(output, write_line17.all);
          deallocate (write_line17);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

--synthesis translate_on

end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library std;
use std.textio.all;

entity master_write_avalon_master_arbitrator is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal clock_crossing_write_s1_waitrequest_from_sa : IN STD_LOGIC;
                 signal d1_clock_crossing_write_s1_end_xfer : IN STD_LOGIC;
                 signal master_write_avalon_master_address : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal master_write_avalon_master_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal master_write_avalon_master_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal master_write_avalon_master_write : IN STD_LOGIC;
                 signal master_write_avalon_master_writedata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal master_write_granted_clock_crossing_write_s1 : IN STD_LOGIC;
                 signal master_write_qualified_request_clock_crossing_write_s1 : IN STD_LOGIC;
                 signal master_write_requests_clock_crossing_write_s1 : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;

              -- outputs:
                 signal master_write_avalon_master_address_to_slave : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal master_write_avalon_master_reset : OUT STD_LOGIC;
                 signal master_write_avalon_master_waitrequest : OUT STD_LOGIC
              );
end entity master_write_avalon_master_arbitrator;


architecture europa of master_write_avalon_master_arbitrator is
                signal active_and_waiting_last_time :  STD_LOGIC;
                signal internal_master_write_avalon_master_address_to_slave :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal internal_master_write_avalon_master_waitrequest :  STD_LOGIC;
                signal master_write_avalon_master_address_last_time :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal master_write_avalon_master_burstcount_last_time :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal master_write_avalon_master_byteenable_last_time :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal master_write_avalon_master_run :  STD_LOGIC;
                signal master_write_avalon_master_write_last_time :  STD_LOGIC;
                signal master_write_avalon_master_writedata_last_time :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal r_0 :  STD_LOGIC;

begin

  --r_0 master_run cascaded wait assignment, which is an e_assign
  r_0 <= Vector_To_Std_Logic((std_logic_vector'("00000000000000000000000000000001") AND (((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((NOT master_write_qualified_request_clock_crossing_write_s1 OR NOT (master_write_avalon_master_write))))) OR (((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT clock_crossing_write_s1_waitrequest_from_sa)))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((master_write_avalon_master_write))))))))));
  --cascaded wait assignment, which is an e_assign
  master_write_avalon_master_run <= r_0;
  --optimize select-logic by passing only those address bits which matter.
  internal_master_write_avalon_master_address_to_slave <= master_write_avalon_master_address(23 DOWNTO 0);
  --actual waitrequest port, which is an e_assign
  internal_master_write_avalon_master_waitrequest <= NOT master_write_avalon_master_run;
  --~master_write_avalon_master_reset assignment, which is an e_assign
  master_write_avalon_master_reset <= NOT reset_n;
  --vhdl renameroo for output signals
  master_write_avalon_master_address_to_slave <= internal_master_write_avalon_master_address_to_slave;
  --vhdl renameroo for output signals
  master_write_avalon_master_waitrequest <= internal_master_write_avalon_master_waitrequest;
--synthesis translate_off
    --master_write_avalon_master_address check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        master_write_avalon_master_address_last_time <= std_logic_vector'("000000000000000000000000");
      elsif clk'event and clk = '1' then
        master_write_avalon_master_address_last_time <= master_write_avalon_master_address;
      end if;

    end process;

    --master_write/avalon_master waited last time, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        active_and_waiting_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        active_and_waiting_last_time <= internal_master_write_avalon_master_waitrequest AND (master_write_avalon_master_write);
      end if;

    end process;

    --master_write_avalon_master_address matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line18 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((master_write_avalon_master_address /= master_write_avalon_master_address_last_time))))) = '1' then 
          write(write_line18, now);
          write(write_line18, string'(": "));
          write(write_line18, string'("master_write_avalon_master_address did not heed wait!!!"));
          write(output, write_line18.all);
          deallocate (write_line18);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --master_write_avalon_master_burstcount check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        master_write_avalon_master_burstcount_last_time <= std_logic_vector'("0000");
      elsif clk'event and clk = '1' then
        master_write_avalon_master_burstcount_last_time <= master_write_avalon_master_burstcount;
      end if;

    end process;

    --master_write_avalon_master_burstcount matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line19 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((master_write_avalon_master_burstcount /= master_write_avalon_master_burstcount_last_time))))) = '1' then 
          write(write_line19, now);
          write(write_line19, string'(": "));
          write(write_line19, string'("master_write_avalon_master_burstcount did not heed wait!!!"));
          write(output, write_line19.all);
          deallocate (write_line19);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --master_write_avalon_master_byteenable check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        master_write_avalon_master_byteenable_last_time <= std_logic_vector'("0000");
      elsif clk'event and clk = '1' then
        master_write_avalon_master_byteenable_last_time <= master_write_avalon_master_byteenable;
      end if;

    end process;

    --master_write_avalon_master_byteenable matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line20 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((master_write_avalon_master_byteenable /= master_write_avalon_master_byteenable_last_time))))) = '1' then 
          write(write_line20, now);
          write(write_line20, string'(": "));
          write(write_line20, string'("master_write_avalon_master_byteenable did not heed wait!!!"));
          write(output, write_line20.all);
          deallocate (write_line20);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --master_write_avalon_master_write check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        master_write_avalon_master_write_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        master_write_avalon_master_write_last_time <= master_write_avalon_master_write;
      end if;

    end process;

    --master_write_avalon_master_write matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line21 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((std_logic'(master_write_avalon_master_write) /= std_logic'(master_write_avalon_master_write_last_time)))))) = '1' then 
          write(write_line21, now);
          write(write_line21, string'(": "));
          write(write_line21, string'("master_write_avalon_master_write did not heed wait!!!"));
          write(output, write_line21.all);
          deallocate (write_line21);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --master_write_avalon_master_writedata check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        master_write_avalon_master_writedata_last_time <= std_logic_vector'("00000000000000000000000000000000");
      elsif clk'event and clk = '1' then
        master_write_avalon_master_writedata_last_time <= master_write_avalon_master_writedata;
      end if;

    end process;

    --master_write_avalon_master_writedata matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line22 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((active_and_waiting_last_time AND to_std_logic(((master_write_avalon_master_writedata /= master_write_avalon_master_writedata_last_time)))) AND master_write_avalon_master_write)) = '1' then 
          write(write_line22, now);
          write(write_line22, string'(": "));
          write(write_line22, string'("master_write_avalon_master_writedata did not heed wait!!!"));
          write(output, write_line22.all);
          deallocate (write_line22);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

--synthesis translate_on

end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity rdv_fifo_for_system_burst_0_downstream_to_sdram_s1_module is 
        port (
              -- inputs:
                 signal clear_fifo : IN STD_LOGIC;
                 signal clk : IN STD_LOGIC;
                 signal data_in : IN STD_LOGIC;
                 signal read : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal sync_reset : IN STD_LOGIC;
                 signal write : IN STD_LOGIC;

              -- outputs:
                 signal data_out : OUT STD_LOGIC;
                 signal empty : OUT STD_LOGIC;
                 signal fifo_contains_ones_n : OUT STD_LOGIC;
                 signal full : OUT STD_LOGIC
              );
end entity rdv_fifo_for_system_burst_0_downstream_to_sdram_s1_module;


architecture europa of rdv_fifo_for_system_burst_0_downstream_to_sdram_s1_module is
                signal full_0 :  STD_LOGIC;
                signal full_1 :  STD_LOGIC;
                signal full_2 :  STD_LOGIC;
                signal full_3 :  STD_LOGIC;
                signal full_4 :  STD_LOGIC;
                signal full_5 :  STD_LOGIC;
                signal full_6 :  STD_LOGIC;
                signal full_7 :  STD_LOGIC;
                signal how_many_ones :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal one_count_minus_one :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal one_count_plus_one :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p0_full_0 :  STD_LOGIC;
                signal p0_stage_0 :  STD_LOGIC;
                signal p1_full_1 :  STD_LOGIC;
                signal p1_stage_1 :  STD_LOGIC;
                signal p2_full_2 :  STD_LOGIC;
                signal p2_stage_2 :  STD_LOGIC;
                signal p3_full_3 :  STD_LOGIC;
                signal p3_stage_3 :  STD_LOGIC;
                signal p4_full_4 :  STD_LOGIC;
                signal p4_stage_4 :  STD_LOGIC;
                signal p5_full_5 :  STD_LOGIC;
                signal p5_stage_5 :  STD_LOGIC;
                signal p6_full_6 :  STD_LOGIC;
                signal p6_stage_6 :  STD_LOGIC;
                signal stage_0 :  STD_LOGIC;
                signal stage_1 :  STD_LOGIC;
                signal stage_2 :  STD_LOGIC;
                signal stage_3 :  STD_LOGIC;
                signal stage_4 :  STD_LOGIC;
                signal stage_5 :  STD_LOGIC;
                signal stage_6 :  STD_LOGIC;
                signal updated_one_count :  STD_LOGIC_VECTOR (3 DOWNTO 0);

begin

  data_out <= stage_0;
  full <= full_6;
  empty <= NOT(full_0);
  full_7 <= std_logic'('0');
  --data_6, which is an e_mux
  p6_stage_6 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_7 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, data_in);
  --data_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_6))))) = '1' then 
        if std_logic'(((sync_reset AND full_6) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_7))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_6 <= std_logic'('0');
        else
          stage_6 <= p6_stage_6;
        end if;
      end if;
    end if;

  end process;

  --control_6, which is an e_mux
  p6_full_6 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_5))), std_logic_vector'("00000000000000000000000000000000")));
  --control_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_6 <= std_logic'('0');
        else
          full_6 <= p6_full_6;
        end if;
      end if;
    end if;

  end process;

  --data_5, which is an e_mux
  p5_stage_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_6 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_6);
  --data_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_5))))) = '1' then 
        if std_logic'(((sync_reset AND full_5) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_6))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_5 <= std_logic'('0');
        else
          stage_5 <= p5_stage_5;
        end if;
      end if;
    end if;

  end process;

  --control_5, which is an e_mux
  p5_full_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_4, full_6);
  --control_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_5 <= std_logic'('0');
        else
          full_5 <= p5_full_5;
        end if;
      end if;
    end if;

  end process;

  --data_4, which is an e_mux
  p4_stage_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_5 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_5);
  --data_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_4))))) = '1' then 
        if std_logic'(((sync_reset AND full_4) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_5))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_4 <= std_logic'('0');
        else
          stage_4 <= p4_stage_4;
        end if;
      end if;
    end if;

  end process;

  --control_4, which is an e_mux
  p4_full_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_3, full_5);
  --control_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_4 <= std_logic'('0');
        else
          full_4 <= p4_full_4;
        end if;
      end if;
    end if;

  end process;

  --data_3, which is an e_mux
  p3_stage_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_4 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_4);
  --data_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_3))))) = '1' then 
        if std_logic'(((sync_reset AND full_3) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_4))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_3 <= std_logic'('0');
        else
          stage_3 <= p3_stage_3;
        end if;
      end if;
    end if;

  end process;

  --control_3, which is an e_mux
  p3_full_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_2, full_4);
  --control_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_3 <= std_logic'('0');
        else
          full_3 <= p3_full_3;
        end if;
      end if;
    end if;

  end process;

  --data_2, which is an e_mux
  p2_stage_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_3 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_3);
  --data_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_2))))) = '1' then 
        if std_logic'(((sync_reset AND full_2) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_3))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_2 <= std_logic'('0');
        else
          stage_2 <= p2_stage_2;
        end if;
      end if;
    end if;

  end process;

  --control_2, which is an e_mux
  p2_full_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_1, full_3);
  --control_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_2 <= std_logic'('0');
        else
          full_2 <= p2_full_2;
        end if;
      end if;
    end if;

  end process;

  --data_1, which is an e_mux
  p1_stage_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_2 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_2);
  --data_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_1))))) = '1' then 
        if std_logic'(((sync_reset AND full_1) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_2))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_1 <= std_logic'('0');
        else
          stage_1 <= p1_stage_1;
        end if;
      end if;
    end if;

  end process;

  --control_1, which is an e_mux
  p1_full_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_0, full_2);
  --control_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_1 <= std_logic'('0');
        else
          full_1 <= p1_full_1;
        end if;
      end if;
    end if;

  end process;

  --data_0, which is an e_mux
  p0_stage_0 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_1 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_1);
  --data_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(((sync_reset AND full_0) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_0 <= std_logic'('0');
        else
          stage_0 <= p0_stage_0;
        end if;
      end if;
    end if;

  end process;

  --control_0, which is an e_mux
  p0_full_0 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), std_logic_vector'("00000000000000000000000000000001"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1)))));
  --control_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'((clear_fifo AND NOT write)) = '1' then 
          full_0 <= std_logic'('0');
        else
          full_0 <= p0_full_0;
        end if;
      end if;
    end if;

  end process;

  one_count_plus_one <= A_EXT (((std_logic_vector'("00000000000000000000000000000") & (how_many_ones)) + std_logic_vector'("000000000000000000000000000000001")), 4);
  one_count_minus_one <= A_EXT (((std_logic_vector'("00000000000000000000000000000") & (how_many_ones)) - std_logic_vector'("000000000000000000000000000000001")), 4);
  --updated_one_count, which is an e_mux
  updated_one_count <= A_EXT (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND NOT(write)))) = '1'), std_logic_vector'("00000000000000000000000000000000"), (std_logic_vector'("0000000000000000000000000000") & (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND write))) = '1'), (std_logic_vector'("000") & (A_TOSTDLOGICVECTOR(data_in))), A_WE_StdLogicVector((std_logic'(((((read AND (data_in)) AND write) AND (stage_0)))) = '1'), how_many_ones, A_WE_StdLogicVector((std_logic'(((write AND (data_in)))) = '1'), one_count_plus_one, A_WE_StdLogicVector((std_logic'(((read AND (stage_0)))) = '1'), one_count_minus_one, how_many_ones))))))), 4);
  --counts how many ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      how_many_ones <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        how_many_ones <= updated_one_count;
      end if;
    end if;

  end process;

  --this fifo contains ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      fifo_contains_ones_n <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        fifo_contains_ones_n <= NOT (or_reduce(updated_one_count));
      end if;
    end if;

  end process;


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity rdv_fifo_for_system_burst_1_downstream_to_sdram_s1_module is 
        port (
              -- inputs:
                 signal clear_fifo : IN STD_LOGIC;
                 signal clk : IN STD_LOGIC;
                 signal data_in : IN STD_LOGIC;
                 signal read : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal sync_reset : IN STD_LOGIC;
                 signal write : IN STD_LOGIC;

              -- outputs:
                 signal data_out : OUT STD_LOGIC;
                 signal empty : OUT STD_LOGIC;
                 signal fifo_contains_ones_n : OUT STD_LOGIC;
                 signal full : OUT STD_LOGIC
              );
end entity rdv_fifo_for_system_burst_1_downstream_to_sdram_s1_module;


architecture europa of rdv_fifo_for_system_burst_1_downstream_to_sdram_s1_module is
                signal full_0 :  STD_LOGIC;
                signal full_1 :  STD_LOGIC;
                signal full_2 :  STD_LOGIC;
                signal full_3 :  STD_LOGIC;
                signal full_4 :  STD_LOGIC;
                signal full_5 :  STD_LOGIC;
                signal full_6 :  STD_LOGIC;
                signal full_7 :  STD_LOGIC;
                signal how_many_ones :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal one_count_minus_one :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal one_count_plus_one :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal p0_full_0 :  STD_LOGIC;
                signal p0_stage_0 :  STD_LOGIC;
                signal p1_full_1 :  STD_LOGIC;
                signal p1_stage_1 :  STD_LOGIC;
                signal p2_full_2 :  STD_LOGIC;
                signal p2_stage_2 :  STD_LOGIC;
                signal p3_full_3 :  STD_LOGIC;
                signal p3_stage_3 :  STD_LOGIC;
                signal p4_full_4 :  STD_LOGIC;
                signal p4_stage_4 :  STD_LOGIC;
                signal p5_full_5 :  STD_LOGIC;
                signal p5_stage_5 :  STD_LOGIC;
                signal p6_full_6 :  STD_LOGIC;
                signal p6_stage_6 :  STD_LOGIC;
                signal stage_0 :  STD_LOGIC;
                signal stage_1 :  STD_LOGIC;
                signal stage_2 :  STD_LOGIC;
                signal stage_3 :  STD_LOGIC;
                signal stage_4 :  STD_LOGIC;
                signal stage_5 :  STD_LOGIC;
                signal stage_6 :  STD_LOGIC;
                signal updated_one_count :  STD_LOGIC_VECTOR (3 DOWNTO 0);

begin

  data_out <= stage_0;
  full <= full_6;
  empty <= NOT(full_0);
  full_7 <= std_logic'('0');
  --data_6, which is an e_mux
  p6_stage_6 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_7 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, data_in);
  --data_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_6))))) = '1' then 
        if std_logic'(((sync_reset AND full_6) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_7))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_6 <= std_logic'('0');
        else
          stage_6 <= p6_stage_6;
        end if;
      end if;
    end if;

  end process;

  --control_6, which is an e_mux
  p6_full_6 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_5))), std_logic_vector'("00000000000000000000000000000000")));
  --control_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_6 <= std_logic'('0');
        else
          full_6 <= p6_full_6;
        end if;
      end if;
    end if;

  end process;

  --data_5, which is an e_mux
  p5_stage_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_6 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_6);
  --data_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_5))))) = '1' then 
        if std_logic'(((sync_reset AND full_5) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_6))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_5 <= std_logic'('0');
        else
          stage_5 <= p5_stage_5;
        end if;
      end if;
    end if;

  end process;

  --control_5, which is an e_mux
  p5_full_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_4, full_6);
  --control_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_5 <= std_logic'('0');
        else
          full_5 <= p5_full_5;
        end if;
      end if;
    end if;

  end process;

  --data_4, which is an e_mux
  p4_stage_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_5 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_5);
  --data_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_4))))) = '1' then 
        if std_logic'(((sync_reset AND full_4) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_5))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_4 <= std_logic'('0');
        else
          stage_4 <= p4_stage_4;
        end if;
      end if;
    end if;

  end process;

  --control_4, which is an e_mux
  p4_full_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_3, full_5);
  --control_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_4 <= std_logic'('0');
        else
          full_4 <= p4_full_4;
        end if;
      end if;
    end if;

  end process;

  --data_3, which is an e_mux
  p3_stage_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_4 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_4);
  --data_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_3))))) = '1' then 
        if std_logic'(((sync_reset AND full_3) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_4))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_3 <= std_logic'('0');
        else
          stage_3 <= p3_stage_3;
        end if;
      end if;
    end if;

  end process;

  --control_3, which is an e_mux
  p3_full_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_2, full_4);
  --control_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_3 <= std_logic'('0');
        else
          full_3 <= p3_full_3;
        end if;
      end if;
    end if;

  end process;

  --data_2, which is an e_mux
  p2_stage_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_3 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_3);
  --data_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_2))))) = '1' then 
        if std_logic'(((sync_reset AND full_2) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_3))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_2 <= std_logic'('0');
        else
          stage_2 <= p2_stage_2;
        end if;
      end if;
    end if;

  end process;

  --control_2, which is an e_mux
  p2_full_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_1, full_3);
  --control_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_2 <= std_logic'('0');
        else
          full_2 <= p2_full_2;
        end if;
      end if;
    end if;

  end process;

  --data_1, which is an e_mux
  p1_stage_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_2 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_2);
  --data_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_1))))) = '1' then 
        if std_logic'(((sync_reset AND full_1) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_2))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_1 <= std_logic'('0');
        else
          stage_1 <= p1_stage_1;
        end if;
      end if;
    end if;

  end process;

  --control_1, which is an e_mux
  p1_full_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_0, full_2);
  --control_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_1 <= std_logic'('0');
        else
          full_1 <= p1_full_1;
        end if;
      end if;
    end if;

  end process;

  --data_0, which is an e_mux
  p0_stage_0 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_1 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_1);
  --data_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(((sync_reset AND full_0) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_0 <= std_logic'('0');
        else
          stage_0 <= p0_stage_0;
        end if;
      end if;
    end if;

  end process;

  --control_0, which is an e_mux
  p0_full_0 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), std_logic_vector'("00000000000000000000000000000001"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1)))));
  --control_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'((clear_fifo AND NOT write)) = '1' then 
          full_0 <= std_logic'('0');
        else
          full_0 <= p0_full_0;
        end if;
      end if;
    end if;

  end process;

  one_count_plus_one <= A_EXT (((std_logic_vector'("00000000000000000000000000000") & (how_many_ones)) + std_logic_vector'("000000000000000000000000000000001")), 4);
  one_count_minus_one <= A_EXT (((std_logic_vector'("00000000000000000000000000000") & (how_many_ones)) - std_logic_vector'("000000000000000000000000000000001")), 4);
  --updated_one_count, which is an e_mux
  updated_one_count <= A_EXT (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND NOT(write)))) = '1'), std_logic_vector'("00000000000000000000000000000000"), (std_logic_vector'("0000000000000000000000000000") & (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND write))) = '1'), (std_logic_vector'("000") & (A_TOSTDLOGICVECTOR(data_in))), A_WE_StdLogicVector((std_logic'(((((read AND (data_in)) AND write) AND (stage_0)))) = '1'), how_many_ones, A_WE_StdLogicVector((std_logic'(((write AND (data_in)))) = '1'), one_count_plus_one, A_WE_StdLogicVector((std_logic'(((read AND (stage_0)))) = '1'), one_count_minus_one, how_many_ones))))))), 4);
  --counts how many ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      how_many_ones <= std_logic_vector'("0000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        how_many_ones <= updated_one_count;
      end if;
    end if;

  end process;

  --this fifo contains ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      fifo_contains_ones_n <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        fifo_contains_ones_n <= NOT (or_reduce(updated_one_count));
      end if;
    end if;

  end process;


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library std;
use std.textio.all;

entity sdram_s1_arbitrator is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal sdram_s1_readdata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal sdram_s1_readdatavalid : IN STD_LOGIC;
                 signal sdram_s1_waitrequest : IN STD_LOGIC;
                 signal system_burst_0_downstream_address_to_slave : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal system_burst_0_downstream_arbitrationshare : IN STD_LOGIC_VECTOR (4 DOWNTO 0);
                 signal system_burst_0_downstream_burstcount : IN STD_LOGIC;
                 signal system_burst_0_downstream_byteenable : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal system_burst_0_downstream_latency_counter : IN STD_LOGIC;
                 signal system_burst_0_downstream_read : IN STD_LOGIC;
                 signal system_burst_0_downstream_write : IN STD_LOGIC;
                 signal system_burst_0_downstream_writedata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal system_burst_1_downstream_address_to_slave : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal system_burst_1_downstream_arbitrationshare : IN STD_LOGIC_VECTOR (4 DOWNTO 0);
                 signal system_burst_1_downstream_burstcount : IN STD_LOGIC;
                 signal system_burst_1_downstream_byteenable : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal system_burst_1_downstream_latency_counter : IN STD_LOGIC;
                 signal system_burst_1_downstream_read : IN STD_LOGIC;
                 signal system_burst_1_downstream_write : IN STD_LOGIC;
                 signal system_burst_1_downstream_writedata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);

              -- outputs:
                 signal d1_sdram_s1_end_xfer : OUT STD_LOGIC;
                 signal sdram_s1_address : OUT STD_LOGIC_VECTOR (22 DOWNTO 0);
                 signal sdram_s1_byteenable_n : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal sdram_s1_chipselect : OUT STD_LOGIC;
                 signal sdram_s1_read_n : OUT STD_LOGIC;
                 signal sdram_s1_readdata_from_sa : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal sdram_s1_reset_n : OUT STD_LOGIC;
                 signal sdram_s1_waitrequest_from_sa : OUT STD_LOGIC;
                 signal sdram_s1_write_n : OUT STD_LOGIC;
                 signal sdram_s1_writedata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal system_burst_0_downstream_granted_sdram_s1 : OUT STD_LOGIC;
                 signal system_burst_0_downstream_qualified_request_sdram_s1 : OUT STD_LOGIC;
                 signal system_burst_0_downstream_read_data_valid_sdram_s1 : OUT STD_LOGIC;
                 signal system_burst_0_downstream_read_data_valid_sdram_s1_shift_register : OUT STD_LOGIC;
                 signal system_burst_0_downstream_requests_sdram_s1 : OUT STD_LOGIC;
                 signal system_burst_1_downstream_granted_sdram_s1 : OUT STD_LOGIC;
                 signal system_burst_1_downstream_qualified_request_sdram_s1 : OUT STD_LOGIC;
                 signal system_burst_1_downstream_read_data_valid_sdram_s1 : OUT STD_LOGIC;
                 signal system_burst_1_downstream_read_data_valid_sdram_s1_shift_register : OUT STD_LOGIC;
                 signal system_burst_1_downstream_requests_sdram_s1 : OUT STD_LOGIC
              );
end entity sdram_s1_arbitrator;


architecture europa of sdram_s1_arbitrator is
component rdv_fifo_for_system_burst_0_downstream_to_sdram_s1_module is 
           port (
                 -- inputs:
                    signal clear_fifo : IN STD_LOGIC;
                    signal clk : IN STD_LOGIC;
                    signal data_in : IN STD_LOGIC;
                    signal read : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal sync_reset : IN STD_LOGIC;
                    signal write : IN STD_LOGIC;

                 -- outputs:
                    signal data_out : OUT STD_LOGIC;
                    signal empty : OUT STD_LOGIC;
                    signal fifo_contains_ones_n : OUT STD_LOGIC;
                    signal full : OUT STD_LOGIC
                 );
end component rdv_fifo_for_system_burst_0_downstream_to_sdram_s1_module;

component rdv_fifo_for_system_burst_1_downstream_to_sdram_s1_module is 
           port (
                 -- inputs:
                    signal clear_fifo : IN STD_LOGIC;
                    signal clk : IN STD_LOGIC;
                    signal data_in : IN STD_LOGIC;
                    signal read : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal sync_reset : IN STD_LOGIC;
                    signal write : IN STD_LOGIC;

                 -- outputs:
                    signal data_out : OUT STD_LOGIC;
                    signal empty : OUT STD_LOGIC;
                    signal fifo_contains_ones_n : OUT STD_LOGIC;
                    signal full : OUT STD_LOGIC
                 );
end component rdv_fifo_for_system_burst_1_downstream_to_sdram_s1_module;

                signal d1_reasons_to_wait :  STD_LOGIC;
                signal enable_nonzero_assertions :  STD_LOGIC;
                signal end_xfer_arb_share_counter_term_sdram_s1 :  STD_LOGIC;
                signal in_a_read_cycle :  STD_LOGIC;
                signal in_a_write_cycle :  STD_LOGIC;
                signal internal_sdram_s1_waitrequest_from_sa :  STD_LOGIC;
                signal internal_system_burst_0_downstream_granted_sdram_s1 :  STD_LOGIC;
                signal internal_system_burst_0_downstream_qualified_request_sdram_s1 :  STD_LOGIC;
                signal internal_system_burst_0_downstream_requests_sdram_s1 :  STD_LOGIC;
                signal internal_system_burst_1_downstream_granted_sdram_s1 :  STD_LOGIC;
                signal internal_system_burst_1_downstream_qualified_request_sdram_s1 :  STD_LOGIC;
                signal internal_system_burst_1_downstream_requests_sdram_s1 :  STD_LOGIC;
                signal last_cycle_system_burst_0_downstream_granted_slave_sdram_s1 :  STD_LOGIC;
                signal last_cycle_system_burst_1_downstream_granted_slave_sdram_s1 :  STD_LOGIC;
                signal module_input10 :  STD_LOGIC;
                signal module_input11 :  STD_LOGIC;
                signal module_input6 :  STD_LOGIC;
                signal module_input7 :  STD_LOGIC;
                signal module_input8 :  STD_LOGIC;
                signal module_input9 :  STD_LOGIC;
                signal sdram_s1_allgrants :  STD_LOGIC;
                signal sdram_s1_allow_new_arb_cycle :  STD_LOGIC;
                signal sdram_s1_any_bursting_master_saved_grant :  STD_LOGIC;
                signal sdram_s1_any_continuerequest :  STD_LOGIC;
                signal sdram_s1_arb_addend :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal sdram_s1_arb_counter_enable :  STD_LOGIC;
                signal sdram_s1_arb_share_counter :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal sdram_s1_arb_share_counter_next_value :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal sdram_s1_arb_share_set_values :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal sdram_s1_arb_winner :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal sdram_s1_arbitration_holdoff_internal :  STD_LOGIC;
                signal sdram_s1_beginbursttransfer_internal :  STD_LOGIC;
                signal sdram_s1_begins_xfer :  STD_LOGIC;
                signal sdram_s1_chosen_master_double_vector :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal sdram_s1_chosen_master_rot_left :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal sdram_s1_end_xfer :  STD_LOGIC;
                signal sdram_s1_firsttransfer :  STD_LOGIC;
                signal sdram_s1_grant_vector :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal sdram_s1_in_a_read_cycle :  STD_LOGIC;
                signal sdram_s1_in_a_write_cycle :  STD_LOGIC;
                signal sdram_s1_master_qreq_vector :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal sdram_s1_move_on_to_next_transaction :  STD_LOGIC;
                signal sdram_s1_non_bursting_master_requests :  STD_LOGIC;
                signal sdram_s1_readdatavalid_from_sa :  STD_LOGIC;
                signal sdram_s1_reg_firsttransfer :  STD_LOGIC;
                signal sdram_s1_saved_chosen_master_vector :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal sdram_s1_slavearbiterlockenable :  STD_LOGIC;
                signal sdram_s1_slavearbiterlockenable2 :  STD_LOGIC;
                signal sdram_s1_unreg_firsttransfer :  STD_LOGIC;
                signal sdram_s1_waits_for_read :  STD_LOGIC;
                signal sdram_s1_waits_for_write :  STD_LOGIC;
                signal shifted_address_to_sdram_s1_from_system_burst_0_downstream :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal shifted_address_to_sdram_s1_from_system_burst_1_downstream :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal system_burst_0_downstream_arbiterlock :  STD_LOGIC;
                signal system_burst_0_downstream_arbiterlock2 :  STD_LOGIC;
                signal system_burst_0_downstream_continuerequest :  STD_LOGIC;
                signal system_burst_0_downstream_rdv_fifo_empty_sdram_s1 :  STD_LOGIC;
                signal system_burst_0_downstream_rdv_fifo_output_from_sdram_s1 :  STD_LOGIC;
                signal system_burst_0_downstream_saved_grant_sdram_s1 :  STD_LOGIC;
                signal system_burst_1_downstream_arbiterlock :  STD_LOGIC;
                signal system_burst_1_downstream_arbiterlock2 :  STD_LOGIC;
                signal system_burst_1_downstream_continuerequest :  STD_LOGIC;
                signal system_burst_1_downstream_rdv_fifo_empty_sdram_s1 :  STD_LOGIC;
                signal system_burst_1_downstream_rdv_fifo_output_from_sdram_s1 :  STD_LOGIC;
                signal system_burst_1_downstream_saved_grant_sdram_s1 :  STD_LOGIC;
                signal wait_for_sdram_s1_counter :  STD_LOGIC;

begin

  process (clk, reset_n)
  begin
    if reset_n = '0' then
      d1_reasons_to_wait <= std_logic'('0');
    elsif clk'event and clk = '1' then
      d1_reasons_to_wait <= NOT sdram_s1_end_xfer;
    end if;

  end process;

  sdram_s1_begins_xfer <= NOT d1_reasons_to_wait AND ((internal_system_burst_0_downstream_qualified_request_sdram_s1 OR internal_system_burst_1_downstream_qualified_request_sdram_s1));
  --assign sdram_s1_readdata_from_sa = sdram_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  sdram_s1_readdata_from_sa <= sdram_s1_readdata;
  internal_system_burst_0_downstream_requests_sdram_s1 <= Vector_To_Std_Logic(((std_logic_vector'("00000000000000000000000000000001")) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((system_burst_0_downstream_read OR system_burst_0_downstream_write)))))));
  --assign sdram_s1_waitrequest_from_sa = sdram_s1_waitrequest so that symbol knows where to group signals which may go to master only, which is an e_assign
  internal_sdram_s1_waitrequest_from_sa <= sdram_s1_waitrequest;
  --assign sdram_s1_readdatavalid_from_sa = sdram_s1_readdatavalid so that symbol knows where to group signals which may go to master only, which is an e_assign
  sdram_s1_readdatavalid_from_sa <= sdram_s1_readdatavalid;
  --sdram_s1_arb_share_counter set values, which is an e_mux
  sdram_s1_arb_share_set_values <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_system_burst_0_downstream_granted_sdram_s1)) = '1'), (std_logic_vector'("000000000000000000000000000") & (system_burst_0_downstream_arbitrationshare)), A_WE_StdLogicVector((std_logic'((internal_system_burst_1_downstream_granted_sdram_s1)) = '1'), (std_logic_vector'("000000000000000000000000000") & (system_burst_1_downstream_arbitrationshare)), A_WE_StdLogicVector((std_logic'((internal_system_burst_0_downstream_granted_sdram_s1)) = '1'), (std_logic_vector'("000000000000000000000000000") & (system_burst_0_downstream_arbitrationshare)), A_WE_StdLogicVector((std_logic'((internal_system_burst_1_downstream_granted_sdram_s1)) = '1'), (std_logic_vector'("000000000000000000000000000") & (system_burst_1_downstream_arbitrationshare)), std_logic_vector'("00000000000000000000000000000001"))))), 5);
  --sdram_s1_non_bursting_master_requests mux, which is an e_mux
  sdram_s1_non_bursting_master_requests <= std_logic'('0');
  --sdram_s1_any_bursting_master_saved_grant mux, which is an e_mux
  sdram_s1_any_bursting_master_saved_grant <= ((system_burst_0_downstream_saved_grant_sdram_s1 OR system_burst_1_downstream_saved_grant_sdram_s1) OR system_burst_0_downstream_saved_grant_sdram_s1) OR system_burst_1_downstream_saved_grant_sdram_s1;
  --sdram_s1_arb_share_counter_next_value assignment, which is an e_assign
  sdram_s1_arb_share_counter_next_value <= A_EXT (A_WE_StdLogicVector((std_logic'(sdram_s1_firsttransfer) = '1'), (((std_logic_vector'("0000000000000000000000000000") & (sdram_s1_arb_share_set_values)) - std_logic_vector'("000000000000000000000000000000001"))), A_WE_StdLogicVector((std_logic'(or_reduce(sdram_s1_arb_share_counter)) = '1'), (((std_logic_vector'("0000000000000000000000000000") & (sdram_s1_arb_share_counter)) - std_logic_vector'("000000000000000000000000000000001"))), std_logic_vector'("000000000000000000000000000000000"))), 5);
  --sdram_s1_allgrants all slave grants, which is an e_mux
  sdram_s1_allgrants <= (((or_reduce(sdram_s1_grant_vector)) OR (or_reduce(sdram_s1_grant_vector))) OR (or_reduce(sdram_s1_grant_vector))) OR (or_reduce(sdram_s1_grant_vector));
  --sdram_s1_end_xfer assignment, which is an e_assign
  sdram_s1_end_xfer <= NOT ((sdram_s1_waits_for_read OR sdram_s1_waits_for_write));
  --end_xfer_arb_share_counter_term_sdram_s1 arb share counter enable term, which is an e_assign
  end_xfer_arb_share_counter_term_sdram_s1 <= sdram_s1_end_xfer AND (((NOT sdram_s1_any_bursting_master_saved_grant OR in_a_read_cycle) OR in_a_write_cycle));
  --sdram_s1_arb_share_counter arbitration counter enable, which is an e_assign
  sdram_s1_arb_counter_enable <= ((end_xfer_arb_share_counter_term_sdram_s1 AND sdram_s1_allgrants)) OR ((end_xfer_arb_share_counter_term_sdram_s1 AND NOT sdram_s1_non_bursting_master_requests));
  --sdram_s1_arb_share_counter counter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      sdram_s1_arb_share_counter <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'(sdram_s1_arb_counter_enable) = '1' then 
        sdram_s1_arb_share_counter <= sdram_s1_arb_share_counter_next_value;
      end if;
    end if;

  end process;

  --sdram_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      sdram_s1_slavearbiterlockenable <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((or_reduce(sdram_s1_master_qreq_vector) AND end_xfer_arb_share_counter_term_sdram_s1)) OR ((end_xfer_arb_share_counter_term_sdram_s1 AND NOT sdram_s1_non_bursting_master_requests)))) = '1' then 
        sdram_s1_slavearbiterlockenable <= or_reduce(sdram_s1_arb_share_counter_next_value);
      end if;
    end if;

  end process;

  --system_burst_0/downstream sdram/s1 arbiterlock, which is an e_assign
  system_burst_0_downstream_arbiterlock <= sdram_s1_slavearbiterlockenable AND system_burst_0_downstream_continuerequest;
  --sdram_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  sdram_s1_slavearbiterlockenable2 <= or_reduce(sdram_s1_arb_share_counter_next_value);
  --system_burst_0/downstream sdram/s1 arbiterlock2, which is an e_assign
  system_burst_0_downstream_arbiterlock2 <= sdram_s1_slavearbiterlockenable2 AND system_burst_0_downstream_continuerequest;
  --system_burst_1/downstream sdram/s1 arbiterlock, which is an e_assign
  system_burst_1_downstream_arbiterlock <= sdram_s1_slavearbiterlockenable AND system_burst_1_downstream_continuerequest;
  --system_burst_1/downstream sdram/s1 arbiterlock2, which is an e_assign
  system_burst_1_downstream_arbiterlock2 <= sdram_s1_slavearbiterlockenable2 AND system_burst_1_downstream_continuerequest;
  --system_burst_1/downstream granted sdram/s1 last time, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      last_cycle_system_burst_1_downstream_granted_slave_sdram_s1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      last_cycle_system_burst_1_downstream_granted_slave_sdram_s1 <= Vector_To_Std_Logic(A_WE_StdLogicVector((std_logic'(system_burst_1_downstream_saved_grant_sdram_s1) = '1'), std_logic_vector'("00000000000000000000000000000001"), A_WE_StdLogicVector((((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(sdram_s1_arbitration_holdoff_internal))) OR std_logic_vector'("00000000000000000000000000000000")))) /= std_logic_vector'("00000000000000000000000000000000")), std_logic_vector'("00000000000000000000000000000000"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(last_cycle_system_burst_1_downstream_granted_slave_sdram_s1))))));
    end if;

  end process;

  --system_burst_1_downstream_continuerequest continued request, which is an e_mux
  system_burst_1_downstream_continuerequest <= Vector_To_Std_Logic(((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(last_cycle_system_burst_1_downstream_granted_slave_sdram_s1))) AND std_logic_vector'("00000000000000000000000000000001")));
  --sdram_s1_any_continuerequest at least one master continues requesting, which is an e_mux
  sdram_s1_any_continuerequest <= system_burst_1_downstream_continuerequest OR system_burst_0_downstream_continuerequest;
  internal_system_burst_0_downstream_qualified_request_sdram_s1 <= internal_system_burst_0_downstream_requests_sdram_s1 AND NOT ((((system_burst_0_downstream_read AND to_std_logic((((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(system_burst_0_downstream_latency_counter))) /= std_logic_vector'("00000000000000000000000000000000"))) OR ((std_logic_vector'("00000000000000000000000000000001")<(std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(system_burst_0_downstream_latency_counter)))))))))) OR system_burst_1_downstream_arbiterlock));
  --unique name for sdram_s1_move_on_to_next_transaction, which is an e_assign
  sdram_s1_move_on_to_next_transaction <= sdram_s1_readdatavalid_from_sa;
  --rdv_fifo_for_system_burst_0_downstream_to_sdram_s1, which is an e_fifo_with_registered_outputs
  rdv_fifo_for_system_burst_0_downstream_to_sdram_s1 : rdv_fifo_for_system_burst_0_downstream_to_sdram_s1_module
    port map(
      data_out => system_burst_0_downstream_rdv_fifo_output_from_sdram_s1,
      empty => open,
      fifo_contains_ones_n => system_burst_0_downstream_rdv_fifo_empty_sdram_s1,
      full => open,
      clear_fifo => module_input6,
      clk => clk,
      data_in => internal_system_burst_0_downstream_granted_sdram_s1,
      read => sdram_s1_move_on_to_next_transaction,
      reset_n => reset_n,
      sync_reset => module_input7,
      write => module_input8
    );

  module_input6 <= std_logic'('0');
  module_input7 <= std_logic'('0');
  module_input8 <= in_a_read_cycle AND NOT sdram_s1_waits_for_read;

  system_burst_0_downstream_read_data_valid_sdram_s1_shift_register <= NOT system_burst_0_downstream_rdv_fifo_empty_sdram_s1;
  --local readdatavalid system_burst_0_downstream_read_data_valid_sdram_s1, which is an e_mux
  system_burst_0_downstream_read_data_valid_sdram_s1 <= ((sdram_s1_readdatavalid_from_sa AND system_burst_0_downstream_rdv_fifo_output_from_sdram_s1)) AND NOT system_burst_0_downstream_rdv_fifo_empty_sdram_s1;
  --sdram_s1_writedata mux, which is an e_mux
  sdram_s1_writedata <= A_WE_StdLogicVector((std_logic'((internal_system_burst_0_downstream_granted_sdram_s1)) = '1'), system_burst_0_downstream_writedata, system_burst_1_downstream_writedata);
  internal_system_burst_1_downstream_requests_sdram_s1 <= Vector_To_Std_Logic(((std_logic_vector'("00000000000000000000000000000001")) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((system_burst_1_downstream_read OR system_burst_1_downstream_write)))))));
  --system_burst_0/downstream granted sdram/s1 last time, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      last_cycle_system_burst_0_downstream_granted_slave_sdram_s1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      last_cycle_system_burst_0_downstream_granted_slave_sdram_s1 <= Vector_To_Std_Logic(A_WE_StdLogicVector((std_logic'(system_burst_0_downstream_saved_grant_sdram_s1) = '1'), std_logic_vector'("00000000000000000000000000000001"), A_WE_StdLogicVector((((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(sdram_s1_arbitration_holdoff_internal))) OR std_logic_vector'("00000000000000000000000000000000")))) /= std_logic_vector'("00000000000000000000000000000000")), std_logic_vector'("00000000000000000000000000000000"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(last_cycle_system_burst_0_downstream_granted_slave_sdram_s1))))));
    end if;

  end process;

  --system_burst_0_downstream_continuerequest continued request, which is an e_mux
  system_burst_0_downstream_continuerequest <= Vector_To_Std_Logic(((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(last_cycle_system_burst_0_downstream_granted_slave_sdram_s1))) AND std_logic_vector'("00000000000000000000000000000001")));
  internal_system_burst_1_downstream_qualified_request_sdram_s1 <= internal_system_burst_1_downstream_requests_sdram_s1 AND NOT ((((system_burst_1_downstream_read AND to_std_logic((((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(system_burst_1_downstream_latency_counter))) /= std_logic_vector'("00000000000000000000000000000000"))) OR ((std_logic_vector'("00000000000000000000000000000001")<(std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(system_burst_1_downstream_latency_counter)))))))))) OR system_burst_0_downstream_arbiterlock));
  --rdv_fifo_for_system_burst_1_downstream_to_sdram_s1, which is an e_fifo_with_registered_outputs
  rdv_fifo_for_system_burst_1_downstream_to_sdram_s1 : rdv_fifo_for_system_burst_1_downstream_to_sdram_s1_module
    port map(
      data_out => system_burst_1_downstream_rdv_fifo_output_from_sdram_s1,
      empty => open,
      fifo_contains_ones_n => system_burst_1_downstream_rdv_fifo_empty_sdram_s1,
      full => open,
      clear_fifo => module_input9,
      clk => clk,
      data_in => internal_system_burst_1_downstream_granted_sdram_s1,
      read => sdram_s1_move_on_to_next_transaction,
      reset_n => reset_n,
      sync_reset => module_input10,
      write => module_input11
    );

  module_input9 <= std_logic'('0');
  module_input10 <= std_logic'('0');
  module_input11 <= in_a_read_cycle AND NOT sdram_s1_waits_for_read;

  system_burst_1_downstream_read_data_valid_sdram_s1_shift_register <= NOT system_burst_1_downstream_rdv_fifo_empty_sdram_s1;
  --local readdatavalid system_burst_1_downstream_read_data_valid_sdram_s1, which is an e_mux
  system_burst_1_downstream_read_data_valid_sdram_s1 <= ((sdram_s1_readdatavalid_from_sa AND system_burst_1_downstream_rdv_fifo_output_from_sdram_s1)) AND NOT system_burst_1_downstream_rdv_fifo_empty_sdram_s1;
  --allow new arb cycle for sdram/s1, which is an e_assign
  sdram_s1_allow_new_arb_cycle <= NOT system_burst_0_downstream_arbiterlock AND NOT system_burst_1_downstream_arbiterlock;
  --system_burst_1/downstream assignment into master qualified-requests vector for sdram/s1, which is an e_assign
  sdram_s1_master_qreq_vector(0) <= internal_system_burst_1_downstream_qualified_request_sdram_s1;
  --system_burst_1/downstream grant sdram/s1, which is an e_assign
  internal_system_burst_1_downstream_granted_sdram_s1 <= sdram_s1_grant_vector(0);
  --system_burst_1/downstream saved-grant sdram/s1, which is an e_assign
  system_burst_1_downstream_saved_grant_sdram_s1 <= sdram_s1_arb_winner(0);
  --system_burst_0/downstream assignment into master qualified-requests vector for sdram/s1, which is an e_assign
  sdram_s1_master_qreq_vector(1) <= internal_system_burst_0_downstream_qualified_request_sdram_s1;
  --system_burst_0/downstream grant sdram/s1, which is an e_assign
  internal_system_burst_0_downstream_granted_sdram_s1 <= sdram_s1_grant_vector(1);
  --system_burst_0/downstream saved-grant sdram/s1, which is an e_assign
  system_burst_0_downstream_saved_grant_sdram_s1 <= sdram_s1_arb_winner(1);
  --sdram/s1 chosen-master double-vector, which is an e_assign
  sdram_s1_chosen_master_double_vector <= A_EXT (((std_logic_vector'("0") & ((sdram_s1_master_qreq_vector & sdram_s1_master_qreq_vector))) AND (((std_logic_vector'("0") & (Std_Logic_Vector'(NOT sdram_s1_master_qreq_vector & NOT sdram_s1_master_qreq_vector))) + (std_logic_vector'("000") & (sdram_s1_arb_addend))))), 4);
  --stable onehot encoding of arb winner
  sdram_s1_arb_winner <= A_WE_StdLogicVector((std_logic'(((sdram_s1_allow_new_arb_cycle AND or_reduce(sdram_s1_grant_vector)))) = '1'), sdram_s1_grant_vector, sdram_s1_saved_chosen_master_vector);
  --saved sdram_s1_grant_vector, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      sdram_s1_saved_chosen_master_vector <= std_logic_vector'("00");
    elsif clk'event and clk = '1' then
      if std_logic'(sdram_s1_allow_new_arb_cycle) = '1' then 
        sdram_s1_saved_chosen_master_vector <= A_WE_StdLogicVector((std_logic'(or_reduce(sdram_s1_grant_vector)) = '1'), sdram_s1_grant_vector, sdram_s1_saved_chosen_master_vector);
      end if;
    end if;

  end process;

  --onehot encoding of chosen master
  sdram_s1_grant_vector <= Std_Logic_Vector'(A_ToStdLogicVector(((sdram_s1_chosen_master_double_vector(1) OR sdram_s1_chosen_master_double_vector(3)))) & A_ToStdLogicVector(((sdram_s1_chosen_master_double_vector(0) OR sdram_s1_chosen_master_double_vector(2)))));
  --sdram/s1 chosen master rotated left, which is an e_assign
  sdram_s1_chosen_master_rot_left <= A_EXT (A_WE_StdLogicVector((((A_SLL(sdram_s1_arb_winner,std_logic_vector'("00000000000000000000000000000001")))) /= std_logic_vector'("00")), (std_logic_vector'("000000000000000000000000000000") & ((A_SLL(sdram_s1_arb_winner,std_logic_vector'("00000000000000000000000000000001"))))), std_logic_vector'("00000000000000000000000000000001")), 2);
  --sdram/s1's addend for next-master-grant
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      sdram_s1_arb_addend <= std_logic_vector'("01");
    elsif clk'event and clk = '1' then
      if std_logic'(or_reduce(sdram_s1_grant_vector)) = '1' then 
        sdram_s1_arb_addend <= A_WE_StdLogicVector((std_logic'(sdram_s1_end_xfer) = '1'), sdram_s1_chosen_master_rot_left, sdram_s1_grant_vector);
      end if;
    end if;

  end process;

  --sdram_s1_reset_n assignment, which is an e_assign
  sdram_s1_reset_n <= reset_n;
  sdram_s1_chipselect <= internal_system_burst_0_downstream_granted_sdram_s1 OR internal_system_burst_1_downstream_granted_sdram_s1;
  --sdram_s1_firsttransfer first transaction, which is an e_assign
  sdram_s1_firsttransfer <= A_WE_StdLogic((std_logic'(sdram_s1_begins_xfer) = '1'), sdram_s1_unreg_firsttransfer, sdram_s1_reg_firsttransfer);
  --sdram_s1_unreg_firsttransfer first transaction, which is an e_assign
  sdram_s1_unreg_firsttransfer <= NOT ((sdram_s1_slavearbiterlockenable AND sdram_s1_any_continuerequest));
  --sdram_s1_reg_firsttransfer first transaction, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      sdram_s1_reg_firsttransfer <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'(sdram_s1_begins_xfer) = '1' then 
        sdram_s1_reg_firsttransfer <= sdram_s1_unreg_firsttransfer;
      end if;
    end if;

  end process;

  --sdram_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  sdram_s1_beginbursttransfer_internal <= sdram_s1_begins_xfer;
  --sdram_s1_arbitration_holdoff_internal arbitration_holdoff, which is an e_assign
  sdram_s1_arbitration_holdoff_internal <= sdram_s1_begins_xfer AND sdram_s1_firsttransfer;
  --~sdram_s1_read_n assignment, which is an e_mux
  sdram_s1_read_n <= NOT ((((internal_system_burst_0_downstream_granted_sdram_s1 AND system_burst_0_downstream_read)) OR ((internal_system_burst_1_downstream_granted_sdram_s1 AND system_burst_1_downstream_read))));
  --~sdram_s1_write_n assignment, which is an e_mux
  sdram_s1_write_n <= NOT ((((internal_system_burst_0_downstream_granted_sdram_s1 AND system_burst_0_downstream_write)) OR ((internal_system_burst_1_downstream_granted_sdram_s1 AND system_burst_1_downstream_write))));
  shifted_address_to_sdram_s1_from_system_burst_0_downstream <= system_burst_0_downstream_address_to_slave;
  --sdram_s1_address mux, which is an e_mux
  sdram_s1_address <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_system_burst_0_downstream_granted_sdram_s1)) = '1'), (A_SRL(shifted_address_to_sdram_s1_from_system_burst_0_downstream,std_logic_vector'("00000000000000000000000000000001"))), (A_SRL(shifted_address_to_sdram_s1_from_system_burst_1_downstream,std_logic_vector'("00000000000000000000000000000001")))), 23);
  shifted_address_to_sdram_s1_from_system_burst_1_downstream <= system_burst_1_downstream_address_to_slave;
  --d1_sdram_s1_end_xfer register, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      d1_sdram_s1_end_xfer <= std_logic'('1');
    elsif clk'event and clk = '1' then
      d1_sdram_s1_end_xfer <= sdram_s1_end_xfer;
    end if;

  end process;

  --sdram_s1_waits_for_read in a cycle, which is an e_mux
  sdram_s1_waits_for_read <= sdram_s1_in_a_read_cycle AND internal_sdram_s1_waitrequest_from_sa;
  --sdram_s1_in_a_read_cycle assignment, which is an e_assign
  sdram_s1_in_a_read_cycle <= ((internal_system_burst_0_downstream_granted_sdram_s1 AND system_burst_0_downstream_read)) OR ((internal_system_burst_1_downstream_granted_sdram_s1 AND system_burst_1_downstream_read));
  --in_a_read_cycle assignment, which is an e_mux
  in_a_read_cycle <= sdram_s1_in_a_read_cycle;
  --sdram_s1_waits_for_write in a cycle, which is an e_mux
  sdram_s1_waits_for_write <= sdram_s1_in_a_write_cycle AND internal_sdram_s1_waitrequest_from_sa;
  --sdram_s1_in_a_write_cycle assignment, which is an e_assign
  sdram_s1_in_a_write_cycle <= ((internal_system_burst_0_downstream_granted_sdram_s1 AND system_burst_0_downstream_write)) OR ((internal_system_burst_1_downstream_granted_sdram_s1 AND system_burst_1_downstream_write));
  --in_a_write_cycle assignment, which is an e_mux
  in_a_write_cycle <= sdram_s1_in_a_write_cycle;
  wait_for_sdram_s1_counter <= std_logic'('0');
  --~sdram_s1_byteenable_n byte enable port mux, which is an e_mux
  sdram_s1_byteenable_n <= A_EXT (NOT (A_WE_StdLogicVector((std_logic'((internal_system_burst_0_downstream_granted_sdram_s1)) = '1'), (std_logic_vector'("000000000000000000000000000000") & (system_burst_0_downstream_byteenable)), A_WE_StdLogicVector((std_logic'((internal_system_burst_1_downstream_granted_sdram_s1)) = '1'), (std_logic_vector'("000000000000000000000000000000") & (system_burst_1_downstream_byteenable)), -SIGNED(std_logic_vector'("00000000000000000000000000000001"))))), 2);
  --vhdl renameroo for output signals
  sdram_s1_waitrequest_from_sa <= internal_sdram_s1_waitrequest_from_sa;
  --vhdl renameroo for output signals
  system_burst_0_downstream_granted_sdram_s1 <= internal_system_burst_0_downstream_granted_sdram_s1;
  --vhdl renameroo for output signals
  system_burst_0_downstream_qualified_request_sdram_s1 <= internal_system_burst_0_downstream_qualified_request_sdram_s1;
  --vhdl renameroo for output signals
  system_burst_0_downstream_requests_sdram_s1 <= internal_system_burst_0_downstream_requests_sdram_s1;
  --vhdl renameroo for output signals
  system_burst_1_downstream_granted_sdram_s1 <= internal_system_burst_1_downstream_granted_sdram_s1;
  --vhdl renameroo for output signals
  system_burst_1_downstream_qualified_request_sdram_s1 <= internal_system_burst_1_downstream_qualified_request_sdram_s1;
  --vhdl renameroo for output signals
  system_burst_1_downstream_requests_sdram_s1 <= internal_system_burst_1_downstream_requests_sdram_s1;
--synthesis translate_off
    --sdram/s1 enable non-zero assertions, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        enable_nonzero_assertions <= std_logic'('0');
      elsif clk'event and clk = '1' then
        enable_nonzero_assertions <= std_logic'('1');
      end if;

    end process;

    --system_burst_0/downstream non-zero arbitrationshare assertion, which is an e_process
    process (clk)
    VARIABLE write_line23 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((internal_system_burst_0_downstream_requests_sdram_s1 AND to_std_logic((((std_logic_vector'("000000000000000000000000000") & (system_burst_0_downstream_arbitrationshare)) = std_logic_vector'("00000000000000000000000000000000"))))) AND enable_nonzero_assertions)) = '1' then 
          write(write_line23, now);
          write(write_line23, string'(": "));
          write(write_line23, string'("system_burst_0/downstream drove 0 on its 'arbitrationshare' port while accessing slave sdram/s1"));
          write(output, write_line23.all);
          deallocate (write_line23);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_0/downstream non-zero burstcount assertion, which is an e_process
    process (clk)
    VARIABLE write_line24 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((internal_system_burst_0_downstream_requests_sdram_s1 AND to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(system_burst_0_downstream_burstcount))) = std_logic_vector'("00000000000000000000000000000000"))))) AND enable_nonzero_assertions)) = '1' then 
          write(write_line24, now);
          write(write_line24, string'(": "));
          write(write_line24, string'("system_burst_0/downstream drove 0 on its 'burstcount' port while accessing slave sdram/s1"));
          write(output, write_line24.all);
          deallocate (write_line24);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_1/downstream non-zero arbitrationshare assertion, which is an e_process
    process (clk)
    VARIABLE write_line25 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((internal_system_burst_1_downstream_requests_sdram_s1 AND to_std_logic((((std_logic_vector'("000000000000000000000000000") & (system_burst_1_downstream_arbitrationshare)) = std_logic_vector'("00000000000000000000000000000000"))))) AND enable_nonzero_assertions)) = '1' then 
          write(write_line25, now);
          write(write_line25, string'(": "));
          write(write_line25, string'("system_burst_1/downstream drove 0 on its 'arbitrationshare' port while accessing slave sdram/s1"));
          write(output, write_line25.all);
          deallocate (write_line25);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_1/downstream non-zero burstcount assertion, which is an e_process
    process (clk)
    VARIABLE write_line26 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((internal_system_burst_1_downstream_requests_sdram_s1 AND to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(system_burst_1_downstream_burstcount))) = std_logic_vector'("00000000000000000000000000000000"))))) AND enable_nonzero_assertions)) = '1' then 
          write(write_line26, now);
          write(write_line26, string'(": "));
          write(write_line26, string'("system_burst_1/downstream drove 0 on its 'burstcount' port while accessing slave sdram/s1"));
          write(output, write_line26.all);
          deallocate (write_line26);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --grant signals are active simultaneously, which is an e_process
    process (clk)
    VARIABLE write_line27 : line;
    begin
      if clk'event and clk = '1' then
        if (std_logic_vector'("000000000000000000000000000000") & (((std_logic_vector'("0") & (A_TOSTDLOGICVECTOR(internal_system_burst_0_downstream_granted_sdram_s1))) + (std_logic_vector'("0") & (A_TOSTDLOGICVECTOR(internal_system_burst_1_downstream_granted_sdram_s1))))))>std_logic_vector'("00000000000000000000000000000001") then 
          write(write_line27, now);
          write(write_line27, string'(": "));
          write(write_line27, string'("> 1 of grant signals are active simultaneously"));
          write(output, write_line27.all);
          deallocate (write_line27);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --saved_grant signals are active simultaneously, which is an e_process
    process (clk)
    VARIABLE write_line28 : line;
    begin
      if clk'event and clk = '1' then
        if (std_logic_vector'("000000000000000000000000000000") & (((std_logic_vector'("0") & (A_TOSTDLOGICVECTOR(system_burst_0_downstream_saved_grant_sdram_s1))) + (std_logic_vector'("0") & (A_TOSTDLOGICVECTOR(system_burst_1_downstream_saved_grant_sdram_s1))))))>std_logic_vector'("00000000000000000000000000000001") then 
          write(write_line28, now);
          write(write_line28, string'(": "));
          write(write_line28, string'("> 1 of saved_grant signals are active simultaneously"));
          write(output, write_line28.all);
          deallocate (write_line28);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

--synthesis translate_on

end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity burstcount_fifo_for_system_burst_0_upstream_module is 
        port (
              -- inputs:
                 signal clear_fifo : IN STD_LOGIC;
                 signal clk : IN STD_LOGIC;
                 signal data_in : IN STD_LOGIC_VECTOR (4 DOWNTO 0);
                 signal read : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal sync_reset : IN STD_LOGIC;
                 signal write : IN STD_LOGIC;

              -- outputs:
                 signal data_out : OUT STD_LOGIC_VECTOR (4 DOWNTO 0);
                 signal empty : OUT STD_LOGIC;
                 signal fifo_contains_ones_n : OUT STD_LOGIC;
                 signal full : OUT STD_LOGIC
              );
end entity burstcount_fifo_for_system_burst_0_upstream_module;


architecture europa of burstcount_fifo_for_system_burst_0_upstream_module is
                signal full_0 :  STD_LOGIC;
                signal full_1 :  STD_LOGIC;
                signal full_2 :  STD_LOGIC;
                signal full_3 :  STD_LOGIC;
                signal full_4 :  STD_LOGIC;
                signal full_5 :  STD_LOGIC;
                signal full_6 :  STD_LOGIC;
                signal full_7 :  STD_LOGIC;
                signal full_8 :  STD_LOGIC;
                signal full_9 :  STD_LOGIC;
                signal how_many_ones :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal one_count_minus_one :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal one_count_plus_one :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p0_full_0 :  STD_LOGIC;
                signal p0_stage_0 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p1_full_1 :  STD_LOGIC;
                signal p1_stage_1 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p2_full_2 :  STD_LOGIC;
                signal p2_stage_2 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p3_full_3 :  STD_LOGIC;
                signal p3_stage_3 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p4_full_4 :  STD_LOGIC;
                signal p4_stage_4 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p5_full_5 :  STD_LOGIC;
                signal p5_stage_5 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p6_full_6 :  STD_LOGIC;
                signal p6_stage_6 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p7_full_7 :  STD_LOGIC;
                signal p7_stage_7 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p8_full_8 :  STD_LOGIC;
                signal p8_stage_8 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_0 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_1 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_2 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_3 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_4 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_5 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_6 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_7 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_8 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal updated_one_count :  STD_LOGIC_VECTOR (4 DOWNTO 0);

begin

  data_out <= stage_0;
  full <= full_8;
  empty <= NOT(full_0);
  full_9 <= std_logic'('0');
  --data_8, which is an e_mux
  p8_stage_8 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_9 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, data_in);
  --data_reg_8, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_8 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_8))))) = '1' then 
        if std_logic'(((sync_reset AND full_8) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_9))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_8 <= std_logic_vector'("00000");
        else
          stage_8 <= p8_stage_8;
        end if;
      end if;
    end if;

  end process;

  --control_8, which is an e_mux
  p8_full_8 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_7))), std_logic_vector'("00000000000000000000000000000000")));
  --control_reg_8, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_8 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_8 <= std_logic'('0');
        else
          full_8 <= p8_full_8;
        end if;
      end if;
    end if;

  end process;

  --data_7, which is an e_mux
  p7_stage_7 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_8 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_8);
  --data_reg_7, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_7 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_7))))) = '1' then 
        if std_logic'(((sync_reset AND full_7) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_8))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_7 <= std_logic_vector'("00000");
        else
          stage_7 <= p7_stage_7;
        end if;
      end if;
    end if;

  end process;

  --control_7, which is an e_mux
  p7_full_7 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_6, full_8);
  --control_reg_7, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_7 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_7 <= std_logic'('0');
        else
          full_7 <= p7_full_7;
        end if;
      end if;
    end if;

  end process;

  --data_6, which is an e_mux
  p6_stage_6 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_7 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_7);
  --data_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_6 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_6))))) = '1' then 
        if std_logic'(((sync_reset AND full_6) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_7))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_6 <= std_logic_vector'("00000");
        else
          stage_6 <= p6_stage_6;
        end if;
      end if;
    end if;

  end process;

  --control_6, which is an e_mux
  p6_full_6 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_5, full_7);
  --control_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_6 <= std_logic'('0');
        else
          full_6 <= p6_full_6;
        end if;
      end if;
    end if;

  end process;

  --data_5, which is an e_mux
  p5_stage_5 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_6 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_6);
  --data_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_5 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_5))))) = '1' then 
        if std_logic'(((sync_reset AND full_5) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_6))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_5 <= std_logic_vector'("00000");
        else
          stage_5 <= p5_stage_5;
        end if;
      end if;
    end if;

  end process;

  --control_5, which is an e_mux
  p5_full_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_4, full_6);
  --control_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_5 <= std_logic'('0');
        else
          full_5 <= p5_full_5;
        end if;
      end if;
    end if;

  end process;

  --data_4, which is an e_mux
  p4_stage_4 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_5 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_5);
  --data_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_4 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_4))))) = '1' then 
        if std_logic'(((sync_reset AND full_4) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_5))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_4 <= std_logic_vector'("00000");
        else
          stage_4 <= p4_stage_4;
        end if;
      end if;
    end if;

  end process;

  --control_4, which is an e_mux
  p4_full_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_3, full_5);
  --control_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_4 <= std_logic'('0');
        else
          full_4 <= p4_full_4;
        end if;
      end if;
    end if;

  end process;

  --data_3, which is an e_mux
  p3_stage_3 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_4 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_4);
  --data_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_3 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_3))))) = '1' then 
        if std_logic'(((sync_reset AND full_3) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_4))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_3 <= std_logic_vector'("00000");
        else
          stage_3 <= p3_stage_3;
        end if;
      end if;
    end if;

  end process;

  --control_3, which is an e_mux
  p3_full_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_2, full_4);
  --control_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_3 <= std_logic'('0');
        else
          full_3 <= p3_full_3;
        end if;
      end if;
    end if;

  end process;

  --data_2, which is an e_mux
  p2_stage_2 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_3 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_3);
  --data_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_2 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_2))))) = '1' then 
        if std_logic'(((sync_reset AND full_2) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_3))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_2 <= std_logic_vector'("00000");
        else
          stage_2 <= p2_stage_2;
        end if;
      end if;
    end if;

  end process;

  --control_2, which is an e_mux
  p2_full_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_1, full_3);
  --control_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_2 <= std_logic'('0');
        else
          full_2 <= p2_full_2;
        end if;
      end if;
    end if;

  end process;

  --data_1, which is an e_mux
  p1_stage_1 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_2 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_2);
  --data_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_1 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_1))))) = '1' then 
        if std_logic'(((sync_reset AND full_1) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_2))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_1 <= std_logic_vector'("00000");
        else
          stage_1 <= p1_stage_1;
        end if;
      end if;
    end if;

  end process;

  --control_1, which is an e_mux
  p1_full_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_0, full_2);
  --control_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_1 <= std_logic'('0');
        else
          full_1 <= p1_full_1;
        end if;
      end if;
    end if;

  end process;

  --data_0, which is an e_mux
  p0_stage_0 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_1 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_1);
  --data_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_0 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(((sync_reset AND full_0) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_0 <= std_logic_vector'("00000");
        else
          stage_0 <= p0_stage_0;
        end if;
      end if;
    end if;

  end process;

  --control_0, which is an e_mux
  p0_full_0 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), std_logic_vector'("00000000000000000000000000000001"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1)))));
  --control_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'((clear_fifo AND NOT write)) = '1' then 
          full_0 <= std_logic'('0');
        else
          full_0 <= p0_full_0;
        end if;
      end if;
    end if;

  end process;

  one_count_plus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000000") & (how_many_ones)) + std_logic_vector'("000000000000000000000000000000001")), 5);
  one_count_minus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000000") & (how_many_ones)) - std_logic_vector'("000000000000000000000000000000001")), 5);
  --updated_one_count, which is an e_mux
  updated_one_count <= A_EXT (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND NOT(write)))) = '1'), std_logic_vector'("00000000000000000000000000000000"), (std_logic_vector'("000000000000000000000000000") & (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND write))) = '1'), (std_logic_vector'("0000") & (A_TOSTDLOGICVECTOR(or_reduce(data_in)))), A_WE_StdLogicVector((std_logic'(((((read AND (or_reduce(data_in))) AND write) AND (or_reduce(stage_0))))) = '1'), how_many_ones, A_WE_StdLogicVector((std_logic'(((write AND (or_reduce(data_in))))) = '1'), one_count_plus_one, A_WE_StdLogicVector((std_logic'(((read AND (or_reduce(stage_0))))) = '1'), one_count_minus_one, how_many_ones))))))), 5);
  --counts how many ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      how_many_ones <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        how_many_ones <= updated_one_count;
      end if;
    end if;

  end process;

  --this fifo contains ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      fifo_contains_ones_n <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        fifo_contains_ones_n <= NOT (or_reduce(updated_one_count));
      end if;
    end if;

  end process;


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity rdv_fifo_for_clock_crossing_write_m1_to_system_burst_0_upstream_module is 
        port (
              -- inputs:
                 signal clear_fifo : IN STD_LOGIC;
                 signal clk : IN STD_LOGIC;
                 signal data_in : IN STD_LOGIC;
                 signal read : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal sync_reset : IN STD_LOGIC;
                 signal write : IN STD_LOGIC;

              -- outputs:
                 signal data_out : OUT STD_LOGIC;
                 signal empty : OUT STD_LOGIC;
                 signal fifo_contains_ones_n : OUT STD_LOGIC;
                 signal full : OUT STD_LOGIC
              );
end entity rdv_fifo_for_clock_crossing_write_m1_to_system_burst_0_upstream_module;


architecture europa of rdv_fifo_for_clock_crossing_write_m1_to_system_burst_0_upstream_module is
                signal full_0 :  STD_LOGIC;
                signal full_1 :  STD_LOGIC;
                signal full_2 :  STD_LOGIC;
                signal full_3 :  STD_LOGIC;
                signal full_4 :  STD_LOGIC;
                signal full_5 :  STD_LOGIC;
                signal full_6 :  STD_LOGIC;
                signal full_7 :  STD_LOGIC;
                signal full_8 :  STD_LOGIC;
                signal full_9 :  STD_LOGIC;
                signal how_many_ones :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal one_count_minus_one :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal one_count_plus_one :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p0_full_0 :  STD_LOGIC;
                signal p0_stage_0 :  STD_LOGIC;
                signal p1_full_1 :  STD_LOGIC;
                signal p1_stage_1 :  STD_LOGIC;
                signal p2_full_2 :  STD_LOGIC;
                signal p2_stage_2 :  STD_LOGIC;
                signal p3_full_3 :  STD_LOGIC;
                signal p3_stage_3 :  STD_LOGIC;
                signal p4_full_4 :  STD_LOGIC;
                signal p4_stage_4 :  STD_LOGIC;
                signal p5_full_5 :  STD_LOGIC;
                signal p5_stage_5 :  STD_LOGIC;
                signal p6_full_6 :  STD_LOGIC;
                signal p6_stage_6 :  STD_LOGIC;
                signal p7_full_7 :  STD_LOGIC;
                signal p7_stage_7 :  STD_LOGIC;
                signal p8_full_8 :  STD_LOGIC;
                signal p8_stage_8 :  STD_LOGIC;
                signal stage_0 :  STD_LOGIC;
                signal stage_1 :  STD_LOGIC;
                signal stage_2 :  STD_LOGIC;
                signal stage_3 :  STD_LOGIC;
                signal stage_4 :  STD_LOGIC;
                signal stage_5 :  STD_LOGIC;
                signal stage_6 :  STD_LOGIC;
                signal stage_7 :  STD_LOGIC;
                signal stage_8 :  STD_LOGIC;
                signal updated_one_count :  STD_LOGIC_VECTOR (4 DOWNTO 0);

begin

  data_out <= stage_0;
  full <= full_8;
  empty <= NOT(full_0);
  full_9 <= std_logic'('0');
  --data_8, which is an e_mux
  p8_stage_8 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_9 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, data_in);
  --data_reg_8, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_8 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_8))))) = '1' then 
        if std_logic'(((sync_reset AND full_8) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_9))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_8 <= std_logic'('0');
        else
          stage_8 <= p8_stage_8;
        end if;
      end if;
    end if;

  end process;

  --control_8, which is an e_mux
  p8_full_8 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_7))), std_logic_vector'("00000000000000000000000000000000")));
  --control_reg_8, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_8 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_8 <= std_logic'('0');
        else
          full_8 <= p8_full_8;
        end if;
      end if;
    end if;

  end process;

  --data_7, which is an e_mux
  p7_stage_7 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_8 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_8);
  --data_reg_7, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_7 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_7))))) = '1' then 
        if std_logic'(((sync_reset AND full_7) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_8))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_7 <= std_logic'('0');
        else
          stage_7 <= p7_stage_7;
        end if;
      end if;
    end if;

  end process;

  --control_7, which is an e_mux
  p7_full_7 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_6, full_8);
  --control_reg_7, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_7 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_7 <= std_logic'('0');
        else
          full_7 <= p7_full_7;
        end if;
      end if;
    end if;

  end process;

  --data_6, which is an e_mux
  p6_stage_6 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_7 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_7);
  --data_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_6))))) = '1' then 
        if std_logic'(((sync_reset AND full_6) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_7))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_6 <= std_logic'('0');
        else
          stage_6 <= p6_stage_6;
        end if;
      end if;
    end if;

  end process;

  --control_6, which is an e_mux
  p6_full_6 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_5, full_7);
  --control_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_6 <= std_logic'('0');
        else
          full_6 <= p6_full_6;
        end if;
      end if;
    end if;

  end process;

  --data_5, which is an e_mux
  p5_stage_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_6 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_6);
  --data_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_5))))) = '1' then 
        if std_logic'(((sync_reset AND full_5) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_6))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_5 <= std_logic'('0');
        else
          stage_5 <= p5_stage_5;
        end if;
      end if;
    end if;

  end process;

  --control_5, which is an e_mux
  p5_full_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_4, full_6);
  --control_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_5 <= std_logic'('0');
        else
          full_5 <= p5_full_5;
        end if;
      end if;
    end if;

  end process;

  --data_4, which is an e_mux
  p4_stage_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_5 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_5);
  --data_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_4))))) = '1' then 
        if std_logic'(((sync_reset AND full_4) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_5))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_4 <= std_logic'('0');
        else
          stage_4 <= p4_stage_4;
        end if;
      end if;
    end if;

  end process;

  --control_4, which is an e_mux
  p4_full_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_3, full_5);
  --control_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_4 <= std_logic'('0');
        else
          full_4 <= p4_full_4;
        end if;
      end if;
    end if;

  end process;

  --data_3, which is an e_mux
  p3_stage_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_4 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_4);
  --data_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_3))))) = '1' then 
        if std_logic'(((sync_reset AND full_3) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_4))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_3 <= std_logic'('0');
        else
          stage_3 <= p3_stage_3;
        end if;
      end if;
    end if;

  end process;

  --control_3, which is an e_mux
  p3_full_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_2, full_4);
  --control_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_3 <= std_logic'('0');
        else
          full_3 <= p3_full_3;
        end if;
      end if;
    end if;

  end process;

  --data_2, which is an e_mux
  p2_stage_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_3 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_3);
  --data_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_2))))) = '1' then 
        if std_logic'(((sync_reset AND full_2) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_3))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_2 <= std_logic'('0');
        else
          stage_2 <= p2_stage_2;
        end if;
      end if;
    end if;

  end process;

  --control_2, which is an e_mux
  p2_full_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_1, full_3);
  --control_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_2 <= std_logic'('0');
        else
          full_2 <= p2_full_2;
        end if;
      end if;
    end if;

  end process;

  --data_1, which is an e_mux
  p1_stage_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_2 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_2);
  --data_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_1))))) = '1' then 
        if std_logic'(((sync_reset AND full_1) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_2))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_1 <= std_logic'('0');
        else
          stage_1 <= p1_stage_1;
        end if;
      end if;
    end if;

  end process;

  --control_1, which is an e_mux
  p1_full_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_0, full_2);
  --control_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_1 <= std_logic'('0');
        else
          full_1 <= p1_full_1;
        end if;
      end if;
    end if;

  end process;

  --data_0, which is an e_mux
  p0_stage_0 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_1 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_1);
  --data_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(((sync_reset AND full_0) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_0 <= std_logic'('0');
        else
          stage_0 <= p0_stage_0;
        end if;
      end if;
    end if;

  end process;

  --control_0, which is an e_mux
  p0_full_0 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), std_logic_vector'("00000000000000000000000000000001"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1)))));
  --control_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'((clear_fifo AND NOT write)) = '1' then 
          full_0 <= std_logic'('0');
        else
          full_0 <= p0_full_0;
        end if;
      end if;
    end if;

  end process;

  one_count_plus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000000") & (how_many_ones)) + std_logic_vector'("000000000000000000000000000000001")), 5);
  one_count_minus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000000") & (how_many_ones)) - std_logic_vector'("000000000000000000000000000000001")), 5);
  --updated_one_count, which is an e_mux
  updated_one_count <= A_EXT (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND NOT(write)))) = '1'), std_logic_vector'("00000000000000000000000000000000"), (std_logic_vector'("000000000000000000000000000") & (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND write))) = '1'), (std_logic_vector'("0000") & (A_TOSTDLOGICVECTOR(data_in))), A_WE_StdLogicVector((std_logic'(((((read AND (data_in)) AND write) AND (stage_0)))) = '1'), how_many_ones, A_WE_StdLogicVector((std_logic'(((write AND (data_in)))) = '1'), one_count_plus_one, A_WE_StdLogicVector((std_logic'(((read AND (stage_0)))) = '1'), one_count_minus_one, how_many_ones))))))), 5);
  --counts how many ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      how_many_ones <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        how_many_ones <= updated_one_count;
      end if;
    end if;

  end process;

  --this fifo contains ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      fifo_contains_ones_n <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        fifo_contains_ones_n <= NOT (or_reduce(updated_one_count));
      end if;
    end if;

  end process;


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library std;
use std.textio.all;

entity system_burst_0_upstream_arbitrator is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal clock_crossing_write_m1_address_to_slave : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal clock_crossing_write_m1_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal clock_crossing_write_m1_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal clock_crossing_write_m1_dbs_address : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal clock_crossing_write_m1_dbs_write_16 : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal clock_crossing_write_m1_latency_counter : IN STD_LOGIC;
                 signal clock_crossing_write_m1_read : IN STD_LOGIC;
                 signal clock_crossing_write_m1_write : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal system_burst_0_upstream_readdata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal system_burst_0_upstream_readdatavalid : IN STD_LOGIC;
                 signal system_burst_0_upstream_waitrequest : IN STD_LOGIC;

              -- outputs:
                 signal clock_crossing_write_m1_byteenable_system_burst_0_upstream : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal clock_crossing_write_m1_granted_system_burst_0_upstream : OUT STD_LOGIC;
                 signal clock_crossing_write_m1_qualified_request_system_burst_0_upstream : OUT STD_LOGIC;
                 signal clock_crossing_write_m1_read_data_valid_system_burst_0_upstream : OUT STD_LOGIC;
                 signal clock_crossing_write_m1_read_data_valid_system_burst_0_upstream_shift_register : OUT STD_LOGIC;
                 signal clock_crossing_write_m1_requests_system_burst_0_upstream : OUT STD_LOGIC;
                 signal d1_system_burst_0_upstream_end_xfer : OUT STD_LOGIC;
                 signal system_burst_0_upstream_address : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal system_burst_0_upstream_burstcount : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal system_burst_0_upstream_byteaddress : OUT STD_LOGIC_VECTOR (24 DOWNTO 0);
                 signal system_burst_0_upstream_byteenable : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal system_burst_0_upstream_debugaccess : OUT STD_LOGIC;
                 signal system_burst_0_upstream_read : OUT STD_LOGIC;
                 signal system_burst_0_upstream_readdata_from_sa : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal system_burst_0_upstream_waitrequest_from_sa : OUT STD_LOGIC;
                 signal system_burst_0_upstream_write : OUT STD_LOGIC;
                 signal system_burst_0_upstream_writedata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
              );
end entity system_burst_0_upstream_arbitrator;


architecture europa of system_burst_0_upstream_arbitrator is
component burstcount_fifo_for_system_burst_0_upstream_module is 
           port (
                 -- inputs:
                    signal clear_fifo : IN STD_LOGIC;
                    signal clk : IN STD_LOGIC;
                    signal data_in : IN STD_LOGIC_VECTOR (4 DOWNTO 0);
                    signal read : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal sync_reset : IN STD_LOGIC;
                    signal write : IN STD_LOGIC;

                 -- outputs:
                    signal data_out : OUT STD_LOGIC_VECTOR (4 DOWNTO 0);
                    signal empty : OUT STD_LOGIC;
                    signal fifo_contains_ones_n : OUT STD_LOGIC;
                    signal full : OUT STD_LOGIC
                 );
end component burstcount_fifo_for_system_burst_0_upstream_module;

component rdv_fifo_for_clock_crossing_write_m1_to_system_burst_0_upstream_module is 
           port (
                 -- inputs:
                    signal clear_fifo : IN STD_LOGIC;
                    signal clk : IN STD_LOGIC;
                    signal data_in : IN STD_LOGIC;
                    signal read : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal sync_reset : IN STD_LOGIC;
                    signal write : IN STD_LOGIC;

                 -- outputs:
                    signal data_out : OUT STD_LOGIC;
                    signal empty : OUT STD_LOGIC;
                    signal fifo_contains_ones_n : OUT STD_LOGIC;
                    signal full : OUT STD_LOGIC
                 );
end component rdv_fifo_for_clock_crossing_write_m1_to_system_burst_0_upstream_module;

                signal clock_crossing_write_m1_arbiterlock :  STD_LOGIC;
                signal clock_crossing_write_m1_arbiterlock2 :  STD_LOGIC;
                signal clock_crossing_write_m1_byteenable_system_burst_0_upstream_segment_0 :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_write_m1_byteenable_system_burst_0_upstream_segment_1 :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_write_m1_continuerequest :  STD_LOGIC;
                signal clock_crossing_write_m1_rdv_fifo_empty_system_burst_0_upstream :  STD_LOGIC;
                signal clock_crossing_write_m1_rdv_fifo_output_from_system_burst_0_upstream :  STD_LOGIC;
                signal clock_crossing_write_m1_saved_grant_system_burst_0_upstream :  STD_LOGIC;
                signal d1_reasons_to_wait :  STD_LOGIC;
                signal enable_nonzero_assertions :  STD_LOGIC;
                signal end_xfer_arb_share_counter_term_system_burst_0_upstream :  STD_LOGIC;
                signal in_a_read_cycle :  STD_LOGIC;
                signal in_a_write_cycle :  STD_LOGIC;
                signal internal_clock_crossing_write_m1_byteenable_system_burst_0_upstream :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal internal_clock_crossing_write_m1_granted_system_burst_0_upstream :  STD_LOGIC;
                signal internal_clock_crossing_write_m1_qualified_request_system_burst_0_upstream :  STD_LOGIC;
                signal internal_clock_crossing_write_m1_requests_system_burst_0_upstream :  STD_LOGIC;
                signal internal_system_burst_0_upstream_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal internal_system_burst_0_upstream_read :  STD_LOGIC;
                signal internal_system_burst_0_upstream_waitrequest_from_sa :  STD_LOGIC;
                signal internal_system_burst_0_upstream_write :  STD_LOGIC;
                signal module_input12 :  STD_LOGIC;
                signal module_input13 :  STD_LOGIC;
                signal module_input14 :  STD_LOGIC;
                signal module_input15 :  STD_LOGIC;
                signal module_input16 :  STD_LOGIC;
                signal module_input17 :  STD_LOGIC;
                signal p0_system_burst_0_upstream_load_fifo :  STD_LOGIC;
                signal system_burst_0_upstream_allgrants :  STD_LOGIC;
                signal system_burst_0_upstream_allow_new_arb_cycle :  STD_LOGIC;
                signal system_burst_0_upstream_any_bursting_master_saved_grant :  STD_LOGIC;
                signal system_burst_0_upstream_any_continuerequest :  STD_LOGIC;
                signal system_burst_0_upstream_arb_counter_enable :  STD_LOGIC;
                signal system_burst_0_upstream_arb_share_counter :  STD_LOGIC_VECTOR (5 DOWNTO 0);
                signal system_burst_0_upstream_arb_share_counter_next_value :  STD_LOGIC_VECTOR (5 DOWNTO 0);
                signal system_burst_0_upstream_arb_share_set_values :  STD_LOGIC_VECTOR (5 DOWNTO 0);
                signal system_burst_0_upstream_bbt_burstcounter :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal system_burst_0_upstream_beginbursttransfer_internal :  STD_LOGIC;
                signal system_burst_0_upstream_begins_xfer :  STD_LOGIC;
                signal system_burst_0_upstream_burstcount_fifo_empty :  STD_LOGIC;
                signal system_burst_0_upstream_current_burst :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal system_burst_0_upstream_current_burst_minus_one :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal system_burst_0_upstream_end_xfer :  STD_LOGIC;
                signal system_burst_0_upstream_firsttransfer :  STD_LOGIC;
                signal system_burst_0_upstream_grant_vector :  STD_LOGIC;
                signal system_burst_0_upstream_in_a_read_cycle :  STD_LOGIC;
                signal system_burst_0_upstream_in_a_write_cycle :  STD_LOGIC;
                signal system_burst_0_upstream_load_fifo :  STD_LOGIC;
                signal system_burst_0_upstream_master_qreq_vector :  STD_LOGIC;
                signal system_burst_0_upstream_move_on_to_next_transaction :  STD_LOGIC;
                signal system_burst_0_upstream_next_bbt_burstcount :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal system_burst_0_upstream_next_burst_count :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal system_burst_0_upstream_non_bursting_master_requests :  STD_LOGIC;
                signal system_burst_0_upstream_readdatavalid_from_sa :  STD_LOGIC;
                signal system_burst_0_upstream_reg_firsttransfer :  STD_LOGIC;
                signal system_burst_0_upstream_selected_burstcount :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal system_burst_0_upstream_slavearbiterlockenable :  STD_LOGIC;
                signal system_burst_0_upstream_slavearbiterlockenable2 :  STD_LOGIC;
                signal system_burst_0_upstream_this_cycle_is_the_last_burst :  STD_LOGIC;
                signal system_burst_0_upstream_transaction_burst_count :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal system_burst_0_upstream_unreg_firsttransfer :  STD_LOGIC;
                signal system_burst_0_upstream_waits_for_read :  STD_LOGIC;
                signal system_burst_0_upstream_waits_for_write :  STD_LOGIC;
                signal wait_for_system_burst_0_upstream_counter :  STD_LOGIC;

begin

  process (clk, reset_n)
  begin
    if reset_n = '0' then
      d1_reasons_to_wait <= std_logic'('0');
    elsif clk'event and clk = '1' then
      d1_reasons_to_wait <= NOT system_burst_0_upstream_end_xfer;
    end if;

  end process;

  system_burst_0_upstream_begins_xfer <= NOT d1_reasons_to_wait AND (internal_clock_crossing_write_m1_qualified_request_system_burst_0_upstream);
  --assign system_burst_0_upstream_readdata_from_sa = system_burst_0_upstream_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  system_burst_0_upstream_readdata_from_sa <= system_burst_0_upstream_readdata;
  internal_clock_crossing_write_m1_requests_system_burst_0_upstream <= Vector_To_Std_Logic(((std_logic_vector'("00000000000000000000000000000001")) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((clock_crossing_write_m1_read OR clock_crossing_write_m1_write)))))));
  --assign system_burst_0_upstream_waitrequest_from_sa = system_burst_0_upstream_waitrequest so that symbol knows where to group signals which may go to master only, which is an e_assign
  internal_system_burst_0_upstream_waitrequest_from_sa <= system_burst_0_upstream_waitrequest;
  --assign system_burst_0_upstream_readdatavalid_from_sa = system_burst_0_upstream_readdatavalid so that symbol knows where to group signals which may go to master only, which is an e_assign
  system_burst_0_upstream_readdatavalid_from_sa <= system_burst_0_upstream_readdatavalid;
  --system_burst_0_upstream_arb_share_counter set values, which is an e_mux
  system_burst_0_upstream_arb_share_set_values <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_clock_crossing_write_m1_granted_system_burst_0_upstream)) = '1'), (A_WE_StdLogicVector((std_logic'((clock_crossing_write_m1_write)) = '1'), (std_logic_vector'("0000000000000000000000000000") & (A_SLL(clock_crossing_write_m1_burstcount,std_logic_vector'("00000000000000000000000000000001")))), std_logic_vector'("00000000000000000000000000000001"))), std_logic_vector'("00000000000000000000000000000001")), 6);
  --system_burst_0_upstream_non_bursting_master_requests mux, which is an e_mux
  system_burst_0_upstream_non_bursting_master_requests <= std_logic'('0');
  --system_burst_0_upstream_any_bursting_master_saved_grant mux, which is an e_mux
  system_burst_0_upstream_any_bursting_master_saved_grant <= clock_crossing_write_m1_saved_grant_system_burst_0_upstream;
  --system_burst_0_upstream_arb_share_counter_next_value assignment, which is an e_assign
  system_burst_0_upstream_arb_share_counter_next_value <= A_EXT (A_WE_StdLogicVector((std_logic'(system_burst_0_upstream_firsttransfer) = '1'), (((std_logic_vector'("000000000000000000000000000") & (system_burst_0_upstream_arb_share_set_values)) - std_logic_vector'("000000000000000000000000000000001"))), A_WE_StdLogicVector((std_logic'(or_reduce(system_burst_0_upstream_arb_share_counter)) = '1'), (((std_logic_vector'("000000000000000000000000000") & (system_burst_0_upstream_arb_share_counter)) - std_logic_vector'("000000000000000000000000000000001"))), std_logic_vector'("000000000000000000000000000000000"))), 6);
  --system_burst_0_upstream_allgrants all slave grants, which is an e_mux
  system_burst_0_upstream_allgrants <= system_burst_0_upstream_grant_vector;
  --system_burst_0_upstream_end_xfer assignment, which is an e_assign
  system_burst_0_upstream_end_xfer <= NOT ((system_burst_0_upstream_waits_for_read OR system_burst_0_upstream_waits_for_write));
  --end_xfer_arb_share_counter_term_system_burst_0_upstream arb share counter enable term, which is an e_assign
  end_xfer_arb_share_counter_term_system_burst_0_upstream <= system_burst_0_upstream_end_xfer AND (((NOT system_burst_0_upstream_any_bursting_master_saved_grant OR in_a_read_cycle) OR in_a_write_cycle));
  --system_burst_0_upstream_arb_share_counter arbitration counter enable, which is an e_assign
  system_burst_0_upstream_arb_counter_enable <= ((end_xfer_arb_share_counter_term_system_burst_0_upstream AND system_burst_0_upstream_allgrants)) OR ((end_xfer_arb_share_counter_term_system_burst_0_upstream AND NOT system_burst_0_upstream_non_bursting_master_requests));
  --system_burst_0_upstream_arb_share_counter counter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_0_upstream_arb_share_counter <= std_logic_vector'("000000");
    elsif clk'event and clk = '1' then
      if std_logic'(system_burst_0_upstream_arb_counter_enable) = '1' then 
        system_burst_0_upstream_arb_share_counter <= system_burst_0_upstream_arb_share_counter_next_value;
      end if;
    end if;

  end process;

  --system_burst_0_upstream_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_0_upstream_slavearbiterlockenable <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((system_burst_0_upstream_master_qreq_vector AND end_xfer_arb_share_counter_term_system_burst_0_upstream)) OR ((end_xfer_arb_share_counter_term_system_burst_0_upstream AND NOT system_burst_0_upstream_non_bursting_master_requests)))) = '1' then 
        system_burst_0_upstream_slavearbiterlockenable <= or_reduce(system_burst_0_upstream_arb_share_counter_next_value);
      end if;
    end if;

  end process;

  --clock_crossing_write/m1 system_burst_0/upstream arbiterlock, which is an e_assign
  clock_crossing_write_m1_arbiterlock <= system_burst_0_upstream_slavearbiterlockenable AND clock_crossing_write_m1_continuerequest;
  --system_burst_0_upstream_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  system_burst_0_upstream_slavearbiterlockenable2 <= or_reduce(system_burst_0_upstream_arb_share_counter_next_value);
  --clock_crossing_write/m1 system_burst_0/upstream arbiterlock2, which is an e_assign
  clock_crossing_write_m1_arbiterlock2 <= system_burst_0_upstream_slavearbiterlockenable2 AND clock_crossing_write_m1_continuerequest;
  --system_burst_0_upstream_any_continuerequest at least one master continues requesting, which is an e_assign
  system_burst_0_upstream_any_continuerequest <= std_logic'('1');
  --clock_crossing_write_m1_continuerequest continued request, which is an e_assign
  clock_crossing_write_m1_continuerequest <= std_logic'('1');
  internal_clock_crossing_write_m1_qualified_request_system_burst_0_upstream <= internal_clock_crossing_write_m1_requests_system_burst_0_upstream AND NOT ((clock_crossing_write_m1_read AND to_std_logic((((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(clock_crossing_write_m1_latency_counter))) /= std_logic_vector'("00000000000000000000000000000000"))) OR ((std_logic_vector'("00000000000000000000000000000001")<(std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(clock_crossing_write_m1_latency_counter))))))))));
  --unique name for system_burst_0_upstream_move_on_to_next_transaction, which is an e_assign
  system_burst_0_upstream_move_on_to_next_transaction <= system_burst_0_upstream_this_cycle_is_the_last_burst AND system_burst_0_upstream_load_fifo;
  --the currently selected burstcount for system_burst_0_upstream, which is an e_mux
  system_burst_0_upstream_selected_burstcount <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_clock_crossing_write_m1_granted_system_burst_0_upstream)) = '1'), (std_logic_vector'("0000000000000000000000000000") & (clock_crossing_write_m1_burstcount)), std_logic_vector'("00000000000000000000000000000001")), 5);
  --burstcount_fifo_for_system_burst_0_upstream, which is an e_fifo_with_registered_outputs
  burstcount_fifo_for_system_burst_0_upstream : burstcount_fifo_for_system_burst_0_upstream_module
    port map(
      data_out => system_burst_0_upstream_transaction_burst_count,
      empty => system_burst_0_upstream_burstcount_fifo_empty,
      fifo_contains_ones_n => open,
      full => open,
      clear_fifo => module_input12,
      clk => clk,
      data_in => system_burst_0_upstream_selected_burstcount,
      read => system_burst_0_upstream_this_cycle_is_the_last_burst,
      reset_n => reset_n,
      sync_reset => module_input13,
      write => module_input14
    );

  module_input12 <= std_logic'('0');
  module_input13 <= std_logic'('0');
  module_input14 <= ((in_a_read_cycle AND NOT system_burst_0_upstream_waits_for_read) AND system_burst_0_upstream_load_fifo) AND NOT ((system_burst_0_upstream_this_cycle_is_the_last_burst AND system_burst_0_upstream_burstcount_fifo_empty));

  --system_burst_0_upstream current burst minus one, which is an e_assign
  system_burst_0_upstream_current_burst_minus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000000") & (system_burst_0_upstream_current_burst)) - std_logic_vector'("000000000000000000000000000000001")), 5);
  --what to load in current_burst, for system_burst_0_upstream, which is an e_mux
  system_burst_0_upstream_next_burst_count <= A_EXT (A_WE_StdLogicVector((std_logic'(((((in_a_read_cycle AND NOT system_burst_0_upstream_waits_for_read)) AND NOT system_burst_0_upstream_load_fifo))) = '1'), (system_burst_0_upstream_selected_burstcount & A_ToStdLogicVector(std_logic'('0'))), A_WE_StdLogicVector((std_logic'(((((in_a_read_cycle AND NOT system_burst_0_upstream_waits_for_read) AND system_burst_0_upstream_this_cycle_is_the_last_burst) AND system_burst_0_upstream_burstcount_fifo_empty))) = '1'), (system_burst_0_upstream_selected_burstcount & A_ToStdLogicVector(std_logic'('0'))), A_WE_StdLogicVector((std_logic'((system_burst_0_upstream_this_cycle_is_the_last_burst)) = '1'), (system_burst_0_upstream_transaction_burst_count & A_ToStdLogicVector(std_logic'('0'))), (std_logic_vector'("0") & (system_burst_0_upstream_current_burst_minus_one))))), 5);
  --the current burst count for system_burst_0_upstream, to be decremented, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_0_upstream_current_burst <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((system_burst_0_upstream_readdatavalid_from_sa OR ((NOT system_burst_0_upstream_load_fifo AND ((in_a_read_cycle AND NOT system_burst_0_upstream_waits_for_read)))))) = '1' then 
        system_burst_0_upstream_current_burst <= system_burst_0_upstream_next_burst_count;
      end if;
    end if;

  end process;

  --a 1 or burstcount fifo empty, to initialize the counter, which is an e_mux
  p0_system_burst_0_upstream_load_fifo <= Vector_To_Std_Logic(A_WE_StdLogicVector((std_logic'((NOT system_burst_0_upstream_load_fifo)) = '1'), std_logic_vector'("00000000000000000000000000000001"), A_WE_StdLogicVector((std_logic'(((((in_a_read_cycle AND NOT system_burst_0_upstream_waits_for_read)) AND system_burst_0_upstream_load_fifo))) = '1'), std_logic_vector'("00000000000000000000000000000001"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT system_burst_0_upstream_burstcount_fifo_empty))))));
  --whether to load directly to the counter or to the fifo, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_0_upstream_load_fifo <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((((in_a_read_cycle AND NOT system_burst_0_upstream_waits_for_read)) AND NOT system_burst_0_upstream_load_fifo) OR system_burst_0_upstream_this_cycle_is_the_last_burst)) = '1' then 
        system_burst_0_upstream_load_fifo <= p0_system_burst_0_upstream_load_fifo;
      end if;
    end if;

  end process;

  --the last cycle in the burst for system_burst_0_upstream, which is an e_assign
  system_burst_0_upstream_this_cycle_is_the_last_burst <= NOT (or_reduce(system_burst_0_upstream_current_burst_minus_one)) AND system_burst_0_upstream_readdatavalid_from_sa;
  --rdv_fifo_for_clock_crossing_write_m1_to_system_burst_0_upstream, which is an e_fifo_with_registered_outputs
  rdv_fifo_for_clock_crossing_write_m1_to_system_burst_0_upstream : rdv_fifo_for_clock_crossing_write_m1_to_system_burst_0_upstream_module
    port map(
      data_out => clock_crossing_write_m1_rdv_fifo_output_from_system_burst_0_upstream,
      empty => open,
      fifo_contains_ones_n => clock_crossing_write_m1_rdv_fifo_empty_system_burst_0_upstream,
      full => open,
      clear_fifo => module_input15,
      clk => clk,
      data_in => internal_clock_crossing_write_m1_granted_system_burst_0_upstream,
      read => system_burst_0_upstream_move_on_to_next_transaction,
      reset_n => reset_n,
      sync_reset => module_input16,
      write => module_input17
    );

  module_input15 <= std_logic'('0');
  module_input16 <= std_logic'('0');
  module_input17 <= in_a_read_cycle AND NOT system_burst_0_upstream_waits_for_read;

  clock_crossing_write_m1_read_data_valid_system_burst_0_upstream_shift_register <= NOT clock_crossing_write_m1_rdv_fifo_empty_system_burst_0_upstream;
  --local readdatavalid clock_crossing_write_m1_read_data_valid_system_burst_0_upstream, which is an e_mux
  clock_crossing_write_m1_read_data_valid_system_burst_0_upstream <= system_burst_0_upstream_readdatavalid_from_sa;
  --system_burst_0_upstream_writedata mux, which is an e_mux
  system_burst_0_upstream_writedata <= clock_crossing_write_m1_dbs_write_16;
  --byteaddress mux for system_burst_0/upstream, which is an e_mux
  system_burst_0_upstream_byteaddress <= std_logic_vector'("0") & (clock_crossing_write_m1_address_to_slave);
  --master is always granted when requested
  internal_clock_crossing_write_m1_granted_system_burst_0_upstream <= internal_clock_crossing_write_m1_qualified_request_system_burst_0_upstream;
  --clock_crossing_write/m1 saved-grant system_burst_0/upstream, which is an e_assign
  clock_crossing_write_m1_saved_grant_system_burst_0_upstream <= internal_clock_crossing_write_m1_requests_system_burst_0_upstream;
  --allow new arb cycle for system_burst_0/upstream, which is an e_assign
  system_burst_0_upstream_allow_new_arb_cycle <= std_logic'('1');
  --placeholder chosen master
  system_burst_0_upstream_grant_vector <= std_logic'('1');
  --placeholder vector of master qualified-requests
  system_burst_0_upstream_master_qreq_vector <= std_logic'('1');
  --system_burst_0_upstream_firsttransfer first transaction, which is an e_assign
  system_burst_0_upstream_firsttransfer <= A_WE_StdLogic((std_logic'(system_burst_0_upstream_begins_xfer) = '1'), system_burst_0_upstream_unreg_firsttransfer, system_burst_0_upstream_reg_firsttransfer);
  --system_burst_0_upstream_unreg_firsttransfer first transaction, which is an e_assign
  system_burst_0_upstream_unreg_firsttransfer <= NOT ((system_burst_0_upstream_slavearbiterlockenable AND system_burst_0_upstream_any_continuerequest));
  --system_burst_0_upstream_reg_firsttransfer first transaction, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_0_upstream_reg_firsttransfer <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'(system_burst_0_upstream_begins_xfer) = '1' then 
        system_burst_0_upstream_reg_firsttransfer <= system_burst_0_upstream_unreg_firsttransfer;
      end if;
    end if;

  end process;

  --system_burst_0_upstream_next_bbt_burstcount next_bbt_burstcount, which is an e_mux
  system_burst_0_upstream_next_bbt_burstcount <= A_EXT (A_WE_StdLogicVector((std_logic'((((internal_system_burst_0_upstream_write) AND to_std_logic((((std_logic_vector'("00000000000000000000000000000") & (system_burst_0_upstream_bbt_burstcounter)) = std_logic_vector'("00000000000000000000000000000000"))))))) = '1'), (((std_logic_vector'("00000000000000000000000000000") & (internal_system_burst_0_upstream_burstcount)) - std_logic_vector'("000000000000000000000000000000001"))), A_WE_StdLogicVector((std_logic'((((internal_system_burst_0_upstream_read) AND to_std_logic((((std_logic_vector'("00000000000000000000000000000") & (system_burst_0_upstream_bbt_burstcounter)) = std_logic_vector'("00000000000000000000000000000000"))))))) = '1'), std_logic_vector'("000000000000000000000000000000000"), (((std_logic_vector'("000000000000000000000000000000") & (system_burst_0_upstream_bbt_burstcounter)) - std_logic_vector'("000000000000000000000000000000001"))))), 3);
  --system_burst_0_upstream_bbt_burstcounter bbt_burstcounter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_0_upstream_bbt_burstcounter <= std_logic_vector'("000");
    elsif clk'event and clk = '1' then
      if std_logic'(system_burst_0_upstream_begins_xfer) = '1' then 
        system_burst_0_upstream_bbt_burstcounter <= system_burst_0_upstream_next_bbt_burstcount;
      end if;
    end if;

  end process;

  --system_burst_0_upstream_beginbursttransfer_internal begin burst transfer, which is an e_assign
  system_burst_0_upstream_beginbursttransfer_internal <= system_burst_0_upstream_begins_xfer AND to_std_logic((((std_logic_vector'("00000000000000000000000000000") & (system_burst_0_upstream_bbt_burstcounter)) = std_logic_vector'("00000000000000000000000000000000"))));
  --system_burst_0_upstream_read assignment, which is an e_mux
  internal_system_burst_0_upstream_read <= internal_clock_crossing_write_m1_granted_system_burst_0_upstream AND clock_crossing_write_m1_read;
  --system_burst_0_upstream_write assignment, which is an e_mux
  internal_system_burst_0_upstream_write <= internal_clock_crossing_write_m1_granted_system_burst_0_upstream AND clock_crossing_write_m1_write;
  --system_burst_0_upstream_address mux, which is an e_mux
  system_burst_0_upstream_address <= A_EXT (Std_Logic_Vector'(A_SRL(clock_crossing_write_m1_address_to_slave,std_logic_vector'("00000000000000000000000000000010")) & A_ToStdLogicVector(clock_crossing_write_m1_dbs_address(1)) & A_ToStdLogicVector(std_logic'('0'))), 24);
  --d1_system_burst_0_upstream_end_xfer register, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      d1_system_burst_0_upstream_end_xfer <= std_logic'('1');
    elsif clk'event and clk = '1' then
      d1_system_burst_0_upstream_end_xfer <= system_burst_0_upstream_end_xfer;
    end if;

  end process;

  --system_burst_0_upstream_waits_for_read in a cycle, which is an e_mux
  system_burst_0_upstream_waits_for_read <= system_burst_0_upstream_in_a_read_cycle AND internal_system_burst_0_upstream_waitrequest_from_sa;
  --system_burst_0_upstream_in_a_read_cycle assignment, which is an e_assign
  system_burst_0_upstream_in_a_read_cycle <= internal_clock_crossing_write_m1_granted_system_burst_0_upstream AND clock_crossing_write_m1_read;
  --in_a_read_cycle assignment, which is an e_mux
  in_a_read_cycle <= system_burst_0_upstream_in_a_read_cycle;
  --system_burst_0_upstream_waits_for_write in a cycle, which is an e_mux
  system_burst_0_upstream_waits_for_write <= system_burst_0_upstream_in_a_write_cycle AND internal_system_burst_0_upstream_waitrequest_from_sa;
  --system_burst_0_upstream_in_a_write_cycle assignment, which is an e_assign
  system_burst_0_upstream_in_a_write_cycle <= internal_clock_crossing_write_m1_granted_system_burst_0_upstream AND clock_crossing_write_m1_write;
  --in_a_write_cycle assignment, which is an e_mux
  in_a_write_cycle <= system_burst_0_upstream_in_a_write_cycle;
  wait_for_system_burst_0_upstream_counter <= std_logic'('0');
  --system_burst_0_upstream_byteenable byte enable port mux, which is an e_mux
  system_burst_0_upstream_byteenable <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_clock_crossing_write_m1_granted_system_burst_0_upstream)) = '1'), (std_logic_vector'("000000000000000000000000000000") & (internal_clock_crossing_write_m1_byteenable_system_burst_0_upstream)), -SIGNED(std_logic_vector'("00000000000000000000000000000001"))), 2);
  (clock_crossing_write_m1_byteenable_system_burst_0_upstream_segment_1(1), clock_crossing_write_m1_byteenable_system_burst_0_upstream_segment_1(0), clock_crossing_write_m1_byteenable_system_burst_0_upstream_segment_0(1), clock_crossing_write_m1_byteenable_system_burst_0_upstream_segment_0(0)) <= clock_crossing_write_m1_byteenable;
  internal_clock_crossing_write_m1_byteenable_system_burst_0_upstream <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(clock_crossing_write_m1_dbs_address(1)))) = std_logic_vector'("00000000000000000000000000000000"))), clock_crossing_write_m1_byteenable_system_burst_0_upstream_segment_0, clock_crossing_write_m1_byteenable_system_burst_0_upstream_segment_1);
  --burstcount mux, which is an e_mux
  internal_system_burst_0_upstream_burstcount <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_clock_crossing_write_m1_granted_system_burst_0_upstream)) = '1'), (std_logic_vector'("0000000000000000000000000000") & (clock_crossing_write_m1_burstcount)), std_logic_vector'("00000000000000000000000000000001")), 4);
  --debugaccess mux, which is an e_mux
  system_burst_0_upstream_debugaccess <= std_logic'('0');
  --vhdl renameroo for output signals
  clock_crossing_write_m1_byteenable_system_burst_0_upstream <= internal_clock_crossing_write_m1_byteenable_system_burst_0_upstream;
  --vhdl renameroo for output signals
  clock_crossing_write_m1_granted_system_burst_0_upstream <= internal_clock_crossing_write_m1_granted_system_burst_0_upstream;
  --vhdl renameroo for output signals
  clock_crossing_write_m1_qualified_request_system_burst_0_upstream <= internal_clock_crossing_write_m1_qualified_request_system_burst_0_upstream;
  --vhdl renameroo for output signals
  clock_crossing_write_m1_requests_system_burst_0_upstream <= internal_clock_crossing_write_m1_requests_system_burst_0_upstream;
  --vhdl renameroo for output signals
  system_burst_0_upstream_burstcount <= internal_system_burst_0_upstream_burstcount;
  --vhdl renameroo for output signals
  system_burst_0_upstream_read <= internal_system_burst_0_upstream_read;
  --vhdl renameroo for output signals
  system_burst_0_upstream_waitrequest_from_sa <= internal_system_burst_0_upstream_waitrequest_from_sa;
  --vhdl renameroo for output signals
  system_burst_0_upstream_write <= internal_system_burst_0_upstream_write;
--synthesis translate_off
    --system_burst_0/upstream enable non-zero assertions, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        enable_nonzero_assertions <= std_logic'('0');
      elsif clk'event and clk = '1' then
        enable_nonzero_assertions <= std_logic'('1');
      end if;

    end process;

    --clock_crossing_write/m1 non-zero burstcount assertion, which is an e_process
    process (clk)
    VARIABLE write_line29 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((internal_clock_crossing_write_m1_requests_system_burst_0_upstream AND to_std_logic((((std_logic_vector'("0000000000000000000000000000") & (clock_crossing_write_m1_burstcount)) = std_logic_vector'("00000000000000000000000000000000"))))) AND enable_nonzero_assertions)) = '1' then 
          write(write_line29, now);
          write(write_line29, string'(": "));
          write(write_line29, string'("clock_crossing_write/m1 drove 0 on its 'burstcount' port while accessing slave system_burst_0/upstream"));
          write(output, write_line29.all);
          deallocate (write_line29);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

--synthesis translate_on

end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library std;
use std.textio.all;

entity system_burst_0_downstream_arbitrator is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal d1_sdram_s1_end_xfer : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal sdram_s1_readdata_from_sa : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal sdram_s1_waitrequest_from_sa : IN STD_LOGIC;
                 signal system_burst_0_downstream_address : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal system_burst_0_downstream_burstcount : IN STD_LOGIC;
                 signal system_burst_0_downstream_byteenable : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal system_burst_0_downstream_granted_sdram_s1 : IN STD_LOGIC;
                 signal system_burst_0_downstream_qualified_request_sdram_s1 : IN STD_LOGIC;
                 signal system_burst_0_downstream_read : IN STD_LOGIC;
                 signal system_burst_0_downstream_read_data_valid_sdram_s1 : IN STD_LOGIC;
                 signal system_burst_0_downstream_read_data_valid_sdram_s1_shift_register : IN STD_LOGIC;
                 signal system_burst_0_downstream_requests_sdram_s1 : IN STD_LOGIC;
                 signal system_burst_0_downstream_write : IN STD_LOGIC;
                 signal system_burst_0_downstream_writedata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);

              -- outputs:
                 signal system_burst_0_downstream_address_to_slave : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal system_burst_0_downstream_latency_counter : OUT STD_LOGIC;
                 signal system_burst_0_downstream_readdata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal system_burst_0_downstream_readdatavalid : OUT STD_LOGIC;
                 signal system_burst_0_downstream_reset_n : OUT STD_LOGIC;
                 signal system_burst_0_downstream_waitrequest : OUT STD_LOGIC
              );
end entity system_burst_0_downstream_arbitrator;


architecture europa of system_burst_0_downstream_arbitrator is
                signal active_and_waiting_last_time :  STD_LOGIC;
                signal internal_system_burst_0_downstream_address_to_slave :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal internal_system_burst_0_downstream_latency_counter :  STD_LOGIC;
                signal internal_system_burst_0_downstream_waitrequest :  STD_LOGIC;
                signal latency_load_value :  STD_LOGIC;
                signal p1_system_burst_0_downstream_latency_counter :  STD_LOGIC;
                signal pre_flush_system_burst_0_downstream_readdatavalid :  STD_LOGIC;
                signal r_0 :  STD_LOGIC;
                signal system_burst_0_downstream_address_last_time :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal system_burst_0_downstream_burstcount_last_time :  STD_LOGIC;
                signal system_burst_0_downstream_byteenable_last_time :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal system_burst_0_downstream_is_granted_some_slave :  STD_LOGIC;
                signal system_burst_0_downstream_read_but_no_slave_selected :  STD_LOGIC;
                signal system_burst_0_downstream_read_last_time :  STD_LOGIC;
                signal system_burst_0_downstream_run :  STD_LOGIC;
                signal system_burst_0_downstream_write_last_time :  STD_LOGIC;
                signal system_burst_0_downstream_writedata_last_time :  STD_LOGIC_VECTOR (15 DOWNTO 0);

begin

  --r_0 master_run cascaded wait assignment, which is an e_assign
  r_0 <= Vector_To_Std_Logic(((((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((system_burst_0_downstream_qualified_request_sdram_s1 OR NOT system_burst_0_downstream_requests_sdram_s1)))))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((system_burst_0_downstream_granted_sdram_s1 OR NOT system_burst_0_downstream_qualified_request_sdram_s1)))))) AND (((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((NOT system_burst_0_downstream_qualified_request_sdram_s1 OR NOT ((system_burst_0_downstream_read OR system_burst_0_downstream_write)))))) OR (((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT sdram_s1_waitrequest_from_sa)))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((system_burst_0_downstream_read OR system_burst_0_downstream_write)))))))))) AND (((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((NOT system_burst_0_downstream_qualified_request_sdram_s1 OR NOT ((system_burst_0_downstream_read OR system_burst_0_downstream_write)))))) OR (((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT sdram_s1_waitrequest_from_sa)))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((system_burst_0_downstream_read OR system_burst_0_downstream_write)))))))))));
  --cascaded wait assignment, which is an e_assign
  system_burst_0_downstream_run <= r_0;
  --optimize select-logic by passing only those address bits which matter.
  internal_system_burst_0_downstream_address_to_slave <= system_burst_0_downstream_address;
  --system_burst_0_downstream_read_but_no_slave_selected assignment, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_0_downstream_read_but_no_slave_selected <= std_logic'('0');
    elsif clk'event and clk = '1' then
      system_burst_0_downstream_read_but_no_slave_selected <= (system_burst_0_downstream_read AND system_burst_0_downstream_run) AND NOT system_burst_0_downstream_is_granted_some_slave;
    end if;

  end process;

  --some slave is getting selected, which is an e_mux
  system_burst_0_downstream_is_granted_some_slave <= system_burst_0_downstream_granted_sdram_s1;
  --latent slave read data valids which may be flushed, which is an e_mux
  pre_flush_system_burst_0_downstream_readdatavalid <= system_burst_0_downstream_read_data_valid_sdram_s1;
  --latent slave read data valid which is not flushed, which is an e_mux
  system_burst_0_downstream_readdatavalid <= system_burst_0_downstream_read_but_no_slave_selected OR pre_flush_system_burst_0_downstream_readdatavalid;
  --system_burst_0/downstream readdata mux, which is an e_mux
  system_burst_0_downstream_readdata <= sdram_s1_readdata_from_sa;
  --actual waitrequest port, which is an e_assign
  internal_system_burst_0_downstream_waitrequest <= NOT system_burst_0_downstream_run;
  --latent max counter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      internal_system_burst_0_downstream_latency_counter <= std_logic'('0');
    elsif clk'event and clk = '1' then
      internal_system_burst_0_downstream_latency_counter <= p1_system_burst_0_downstream_latency_counter;
    end if;

  end process;

  --latency counter load mux, which is an e_mux
  p1_system_burst_0_downstream_latency_counter <= Vector_To_Std_Logic(A_WE_StdLogicVector((std_logic'(((system_burst_0_downstream_run AND system_burst_0_downstream_read))) = '1'), (std_logic_vector'("00000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(latency_load_value))), A_WE_StdLogicVector((std_logic'((internal_system_burst_0_downstream_latency_counter)) = '1'), ((std_logic_vector'("00000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(internal_system_burst_0_downstream_latency_counter))) - std_logic_vector'("000000000000000000000000000000001")), std_logic_vector'("000000000000000000000000000000000"))));
  --read latency load values, which is an e_mux
  latency_load_value <= std_logic'('0');
  --system_burst_0_downstream_reset_n assignment, which is an e_assign
  system_burst_0_downstream_reset_n <= reset_n;
  --vhdl renameroo for output signals
  system_burst_0_downstream_address_to_slave <= internal_system_burst_0_downstream_address_to_slave;
  --vhdl renameroo for output signals
  system_burst_0_downstream_latency_counter <= internal_system_burst_0_downstream_latency_counter;
  --vhdl renameroo for output signals
  system_burst_0_downstream_waitrequest <= internal_system_burst_0_downstream_waitrequest;
--synthesis translate_off
    --system_burst_0_downstream_address check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        system_burst_0_downstream_address_last_time <= std_logic_vector'("000000000000000000000000");
      elsif clk'event and clk = '1' then
        system_burst_0_downstream_address_last_time <= system_burst_0_downstream_address;
      end if;

    end process;

    --system_burst_0/downstream waited last time, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        active_and_waiting_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        active_and_waiting_last_time <= internal_system_burst_0_downstream_waitrequest AND ((system_burst_0_downstream_read OR system_burst_0_downstream_write));
      end if;

    end process;

    --system_burst_0_downstream_address matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line30 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((system_burst_0_downstream_address /= system_burst_0_downstream_address_last_time))))) = '1' then 
          write(write_line30, now);
          write(write_line30, string'(": "));
          write(write_line30, string'("system_burst_0_downstream_address did not heed wait!!!"));
          write(output, write_line30.all);
          deallocate (write_line30);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_0_downstream_burstcount check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        system_burst_0_downstream_burstcount_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        system_burst_0_downstream_burstcount_last_time <= system_burst_0_downstream_burstcount;
      end if;

    end process;

    --system_burst_0_downstream_burstcount matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line31 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((std_logic'(system_burst_0_downstream_burstcount) /= std_logic'(system_burst_0_downstream_burstcount_last_time)))))) = '1' then 
          write(write_line31, now);
          write(write_line31, string'(": "));
          write(write_line31, string'("system_burst_0_downstream_burstcount did not heed wait!!!"));
          write(output, write_line31.all);
          deallocate (write_line31);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_0_downstream_byteenable check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        system_burst_0_downstream_byteenable_last_time <= std_logic_vector'("00");
      elsif clk'event and clk = '1' then
        system_burst_0_downstream_byteenable_last_time <= system_burst_0_downstream_byteenable;
      end if;

    end process;

    --system_burst_0_downstream_byteenable matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line32 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((system_burst_0_downstream_byteenable /= system_burst_0_downstream_byteenable_last_time))))) = '1' then 
          write(write_line32, now);
          write(write_line32, string'(": "));
          write(write_line32, string'("system_burst_0_downstream_byteenable did not heed wait!!!"));
          write(output, write_line32.all);
          deallocate (write_line32);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_0_downstream_read check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        system_burst_0_downstream_read_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        system_burst_0_downstream_read_last_time <= system_burst_0_downstream_read;
      end if;

    end process;

    --system_burst_0_downstream_read matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line33 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((std_logic'(system_burst_0_downstream_read) /= std_logic'(system_burst_0_downstream_read_last_time)))))) = '1' then 
          write(write_line33, now);
          write(write_line33, string'(": "));
          write(write_line33, string'("system_burst_0_downstream_read did not heed wait!!!"));
          write(output, write_line33.all);
          deallocate (write_line33);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_0_downstream_write check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        system_burst_0_downstream_write_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        system_burst_0_downstream_write_last_time <= system_burst_0_downstream_write;
      end if;

    end process;

    --system_burst_0_downstream_write matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line34 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((std_logic'(system_burst_0_downstream_write) /= std_logic'(system_burst_0_downstream_write_last_time)))))) = '1' then 
          write(write_line34, now);
          write(write_line34, string'(": "));
          write(write_line34, string'("system_burst_0_downstream_write did not heed wait!!!"));
          write(output, write_line34.all);
          deallocate (write_line34);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_0_downstream_writedata check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        system_burst_0_downstream_writedata_last_time <= std_logic_vector'("0000000000000000");
      elsif clk'event and clk = '1' then
        system_burst_0_downstream_writedata_last_time <= system_burst_0_downstream_writedata;
      end if;

    end process;

    --system_burst_0_downstream_writedata matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line35 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((active_and_waiting_last_time AND to_std_logic(((system_burst_0_downstream_writedata /= system_burst_0_downstream_writedata_last_time)))) AND system_burst_0_downstream_write)) = '1' then 
          write(write_line35, now);
          write(write_line35, string'(": "));
          write(write_line35, string'("system_burst_0_downstream_writedata did not heed wait!!!"));
          write(output, write_line35.all);
          deallocate (write_line35);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

--synthesis translate_on

end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity burstcount_fifo_for_system_burst_1_upstream_module is 
        port (
              -- inputs:
                 signal clear_fifo : IN STD_LOGIC;
                 signal clk : IN STD_LOGIC;
                 signal data_in : IN STD_LOGIC_VECTOR (4 DOWNTO 0);
                 signal read : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal sync_reset : IN STD_LOGIC;
                 signal write : IN STD_LOGIC;

              -- outputs:
                 signal data_out : OUT STD_LOGIC_VECTOR (4 DOWNTO 0);
                 signal empty : OUT STD_LOGIC;
                 signal fifo_contains_ones_n : OUT STD_LOGIC;
                 signal full : OUT STD_LOGIC
              );
end entity burstcount_fifo_for_system_burst_1_upstream_module;


architecture europa of burstcount_fifo_for_system_burst_1_upstream_module is
                signal full_0 :  STD_LOGIC;
                signal full_1 :  STD_LOGIC;
                signal full_2 :  STD_LOGIC;
                signal full_3 :  STD_LOGIC;
                signal full_4 :  STD_LOGIC;
                signal full_5 :  STD_LOGIC;
                signal full_6 :  STD_LOGIC;
                signal full_7 :  STD_LOGIC;
                signal full_8 :  STD_LOGIC;
                signal full_9 :  STD_LOGIC;
                signal how_many_ones :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal one_count_minus_one :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal one_count_plus_one :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p0_full_0 :  STD_LOGIC;
                signal p0_stage_0 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p1_full_1 :  STD_LOGIC;
                signal p1_stage_1 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p2_full_2 :  STD_LOGIC;
                signal p2_stage_2 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p3_full_3 :  STD_LOGIC;
                signal p3_stage_3 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p4_full_4 :  STD_LOGIC;
                signal p4_stage_4 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p5_full_5 :  STD_LOGIC;
                signal p5_stage_5 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p6_full_6 :  STD_LOGIC;
                signal p6_stage_6 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p7_full_7 :  STD_LOGIC;
                signal p7_stage_7 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p8_full_8 :  STD_LOGIC;
                signal p8_stage_8 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_0 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_1 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_2 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_3 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_4 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_5 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_6 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_7 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal stage_8 :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal updated_one_count :  STD_LOGIC_VECTOR (4 DOWNTO 0);

begin

  data_out <= stage_0;
  full <= full_8;
  empty <= NOT(full_0);
  full_9 <= std_logic'('0');
  --data_8, which is an e_mux
  p8_stage_8 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_9 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, data_in);
  --data_reg_8, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_8 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_8))))) = '1' then 
        if std_logic'(((sync_reset AND full_8) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_9))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_8 <= std_logic_vector'("00000");
        else
          stage_8 <= p8_stage_8;
        end if;
      end if;
    end if;

  end process;

  --control_8, which is an e_mux
  p8_full_8 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_7))), std_logic_vector'("00000000000000000000000000000000")));
  --control_reg_8, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_8 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_8 <= std_logic'('0');
        else
          full_8 <= p8_full_8;
        end if;
      end if;
    end if;

  end process;

  --data_7, which is an e_mux
  p7_stage_7 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_8 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_8);
  --data_reg_7, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_7 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_7))))) = '1' then 
        if std_logic'(((sync_reset AND full_7) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_8))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_7 <= std_logic_vector'("00000");
        else
          stage_7 <= p7_stage_7;
        end if;
      end if;
    end if;

  end process;

  --control_7, which is an e_mux
  p7_full_7 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_6, full_8);
  --control_reg_7, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_7 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_7 <= std_logic'('0');
        else
          full_7 <= p7_full_7;
        end if;
      end if;
    end if;

  end process;

  --data_6, which is an e_mux
  p6_stage_6 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_7 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_7);
  --data_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_6 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_6))))) = '1' then 
        if std_logic'(((sync_reset AND full_6) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_7))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_6 <= std_logic_vector'("00000");
        else
          stage_6 <= p6_stage_6;
        end if;
      end if;
    end if;

  end process;

  --control_6, which is an e_mux
  p6_full_6 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_5, full_7);
  --control_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_6 <= std_logic'('0');
        else
          full_6 <= p6_full_6;
        end if;
      end if;
    end if;

  end process;

  --data_5, which is an e_mux
  p5_stage_5 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_6 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_6);
  --data_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_5 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_5))))) = '1' then 
        if std_logic'(((sync_reset AND full_5) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_6))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_5 <= std_logic_vector'("00000");
        else
          stage_5 <= p5_stage_5;
        end if;
      end if;
    end if;

  end process;

  --control_5, which is an e_mux
  p5_full_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_4, full_6);
  --control_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_5 <= std_logic'('0');
        else
          full_5 <= p5_full_5;
        end if;
      end if;
    end if;

  end process;

  --data_4, which is an e_mux
  p4_stage_4 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_5 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_5);
  --data_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_4 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_4))))) = '1' then 
        if std_logic'(((sync_reset AND full_4) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_5))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_4 <= std_logic_vector'("00000");
        else
          stage_4 <= p4_stage_4;
        end if;
      end if;
    end if;

  end process;

  --control_4, which is an e_mux
  p4_full_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_3, full_5);
  --control_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_4 <= std_logic'('0');
        else
          full_4 <= p4_full_4;
        end if;
      end if;
    end if;

  end process;

  --data_3, which is an e_mux
  p3_stage_3 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_4 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_4);
  --data_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_3 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_3))))) = '1' then 
        if std_logic'(((sync_reset AND full_3) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_4))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_3 <= std_logic_vector'("00000");
        else
          stage_3 <= p3_stage_3;
        end if;
      end if;
    end if;

  end process;

  --control_3, which is an e_mux
  p3_full_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_2, full_4);
  --control_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_3 <= std_logic'('0');
        else
          full_3 <= p3_full_3;
        end if;
      end if;
    end if;

  end process;

  --data_2, which is an e_mux
  p2_stage_2 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_3 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_3);
  --data_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_2 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_2))))) = '1' then 
        if std_logic'(((sync_reset AND full_2) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_3))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_2 <= std_logic_vector'("00000");
        else
          stage_2 <= p2_stage_2;
        end if;
      end if;
    end if;

  end process;

  --control_2, which is an e_mux
  p2_full_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_1, full_3);
  --control_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_2 <= std_logic'('0');
        else
          full_2 <= p2_full_2;
        end if;
      end if;
    end if;

  end process;

  --data_1, which is an e_mux
  p1_stage_1 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_2 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_2);
  --data_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_1 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_1))))) = '1' then 
        if std_logic'(((sync_reset AND full_1) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_2))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_1 <= std_logic_vector'("00000");
        else
          stage_1 <= p1_stage_1;
        end if;
      end if;
    end if;

  end process;

  --control_1, which is an e_mux
  p1_full_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_0, full_2);
  --control_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_1 <= std_logic'('0');
        else
          full_1 <= p1_full_1;
        end if;
      end if;
    end if;

  end process;

  --data_0, which is an e_mux
  p0_stage_0 <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_1 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_1);
  --data_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_0 <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(((sync_reset AND full_0) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_0 <= std_logic_vector'("00000");
        else
          stage_0 <= p0_stage_0;
        end if;
      end if;
    end if;

  end process;

  --control_0, which is an e_mux
  p0_full_0 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), std_logic_vector'("00000000000000000000000000000001"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1)))));
  --control_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'((clear_fifo AND NOT write)) = '1' then 
          full_0 <= std_logic'('0');
        else
          full_0 <= p0_full_0;
        end if;
      end if;
    end if;

  end process;

  one_count_plus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000000") & (how_many_ones)) + std_logic_vector'("000000000000000000000000000000001")), 5);
  one_count_minus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000000") & (how_many_ones)) - std_logic_vector'("000000000000000000000000000000001")), 5);
  --updated_one_count, which is an e_mux
  updated_one_count <= A_EXT (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND NOT(write)))) = '1'), std_logic_vector'("00000000000000000000000000000000"), (std_logic_vector'("000000000000000000000000000") & (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND write))) = '1'), (std_logic_vector'("0000") & (A_TOSTDLOGICVECTOR(or_reduce(data_in)))), A_WE_StdLogicVector((std_logic'(((((read AND (or_reduce(data_in))) AND write) AND (or_reduce(stage_0))))) = '1'), how_many_ones, A_WE_StdLogicVector((std_logic'(((write AND (or_reduce(data_in))))) = '1'), one_count_plus_one, A_WE_StdLogicVector((std_logic'(((read AND (or_reduce(stage_0))))) = '1'), one_count_minus_one, how_many_ones))))))), 5);
  --counts how many ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      how_many_ones <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        how_many_ones <= updated_one_count;
      end if;
    end if;

  end process;

  --this fifo contains ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      fifo_contains_ones_n <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        fifo_contains_ones_n <= NOT (or_reduce(updated_one_count));
      end if;
    end if;

  end process;


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity rdv_fifo_for_clock_crossing_read_m1_to_system_burst_1_upstream_module is 
        port (
              -- inputs:
                 signal clear_fifo : IN STD_LOGIC;
                 signal clk : IN STD_LOGIC;
                 signal data_in : IN STD_LOGIC;
                 signal read : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal sync_reset : IN STD_LOGIC;
                 signal write : IN STD_LOGIC;

              -- outputs:
                 signal data_out : OUT STD_LOGIC;
                 signal empty : OUT STD_LOGIC;
                 signal fifo_contains_ones_n : OUT STD_LOGIC;
                 signal full : OUT STD_LOGIC
              );
end entity rdv_fifo_for_clock_crossing_read_m1_to_system_burst_1_upstream_module;


architecture europa of rdv_fifo_for_clock_crossing_read_m1_to_system_burst_1_upstream_module is
                signal full_0 :  STD_LOGIC;
                signal full_1 :  STD_LOGIC;
                signal full_2 :  STD_LOGIC;
                signal full_3 :  STD_LOGIC;
                signal full_4 :  STD_LOGIC;
                signal full_5 :  STD_LOGIC;
                signal full_6 :  STD_LOGIC;
                signal full_7 :  STD_LOGIC;
                signal full_8 :  STD_LOGIC;
                signal full_9 :  STD_LOGIC;
                signal how_many_ones :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal one_count_minus_one :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal one_count_plus_one :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal p0_full_0 :  STD_LOGIC;
                signal p0_stage_0 :  STD_LOGIC;
                signal p1_full_1 :  STD_LOGIC;
                signal p1_stage_1 :  STD_LOGIC;
                signal p2_full_2 :  STD_LOGIC;
                signal p2_stage_2 :  STD_LOGIC;
                signal p3_full_3 :  STD_LOGIC;
                signal p3_stage_3 :  STD_LOGIC;
                signal p4_full_4 :  STD_LOGIC;
                signal p4_stage_4 :  STD_LOGIC;
                signal p5_full_5 :  STD_LOGIC;
                signal p5_stage_5 :  STD_LOGIC;
                signal p6_full_6 :  STD_LOGIC;
                signal p6_stage_6 :  STD_LOGIC;
                signal p7_full_7 :  STD_LOGIC;
                signal p7_stage_7 :  STD_LOGIC;
                signal p8_full_8 :  STD_LOGIC;
                signal p8_stage_8 :  STD_LOGIC;
                signal stage_0 :  STD_LOGIC;
                signal stage_1 :  STD_LOGIC;
                signal stage_2 :  STD_LOGIC;
                signal stage_3 :  STD_LOGIC;
                signal stage_4 :  STD_LOGIC;
                signal stage_5 :  STD_LOGIC;
                signal stage_6 :  STD_LOGIC;
                signal stage_7 :  STD_LOGIC;
                signal stage_8 :  STD_LOGIC;
                signal updated_one_count :  STD_LOGIC_VECTOR (4 DOWNTO 0);

begin

  data_out <= stage_0;
  full <= full_8;
  empty <= NOT(full_0);
  full_9 <= std_logic'('0');
  --data_8, which is an e_mux
  p8_stage_8 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_9 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, data_in);
  --data_reg_8, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_8 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_8))))) = '1' then 
        if std_logic'(((sync_reset AND full_8) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_9))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_8 <= std_logic'('0');
        else
          stage_8 <= p8_stage_8;
        end if;
      end if;
    end if;

  end process;

  --control_8, which is an e_mux
  p8_full_8 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_7))), std_logic_vector'("00000000000000000000000000000000")));
  --control_reg_8, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_8 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_8 <= std_logic'('0');
        else
          full_8 <= p8_full_8;
        end if;
      end if;
    end if;

  end process;

  --data_7, which is an e_mux
  p7_stage_7 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_8 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_8);
  --data_reg_7, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_7 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_7))))) = '1' then 
        if std_logic'(((sync_reset AND full_7) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_8))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_7 <= std_logic'('0');
        else
          stage_7 <= p7_stage_7;
        end if;
      end if;
    end if;

  end process;

  --control_7, which is an e_mux
  p7_full_7 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_6, full_8);
  --control_reg_7, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_7 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_7 <= std_logic'('0');
        else
          full_7 <= p7_full_7;
        end if;
      end if;
    end if;

  end process;

  --data_6, which is an e_mux
  p6_stage_6 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_7 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_7);
  --data_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_6))))) = '1' then 
        if std_logic'(((sync_reset AND full_6) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_7))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_6 <= std_logic'('0');
        else
          stage_6 <= p6_stage_6;
        end if;
      end if;
    end if;

  end process;

  --control_6, which is an e_mux
  p6_full_6 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_5, full_7);
  --control_reg_6, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_6 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_6 <= std_logic'('0');
        else
          full_6 <= p6_full_6;
        end if;
      end if;
    end if;

  end process;

  --data_5, which is an e_mux
  p5_stage_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_6 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_6);
  --data_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_5))))) = '1' then 
        if std_logic'(((sync_reset AND full_5) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_6))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_5 <= std_logic'('0');
        else
          stage_5 <= p5_stage_5;
        end if;
      end if;
    end if;

  end process;

  --control_5, which is an e_mux
  p5_full_5 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_4, full_6);
  --control_reg_5, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_5 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_5 <= std_logic'('0');
        else
          full_5 <= p5_full_5;
        end if;
      end if;
    end if;

  end process;

  --data_4, which is an e_mux
  p4_stage_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_5 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_5);
  --data_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_4))))) = '1' then 
        if std_logic'(((sync_reset AND full_4) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_5))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_4 <= std_logic'('0');
        else
          stage_4 <= p4_stage_4;
        end if;
      end if;
    end if;

  end process;

  --control_4, which is an e_mux
  p4_full_4 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_3, full_5);
  --control_reg_4, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_4 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_4 <= std_logic'('0');
        else
          full_4 <= p4_full_4;
        end if;
      end if;
    end if;

  end process;

  --data_3, which is an e_mux
  p3_stage_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_4 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_4);
  --data_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_3))))) = '1' then 
        if std_logic'(((sync_reset AND full_3) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_4))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_3 <= std_logic'('0');
        else
          stage_3 <= p3_stage_3;
        end if;
      end if;
    end if;

  end process;

  --control_3, which is an e_mux
  p3_full_3 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_2, full_4);
  --control_reg_3, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_3 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_3 <= std_logic'('0');
        else
          full_3 <= p3_full_3;
        end if;
      end if;
    end if;

  end process;

  --data_2, which is an e_mux
  p2_stage_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_3 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_3);
  --data_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_2))))) = '1' then 
        if std_logic'(((sync_reset AND full_2) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_3))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_2 <= std_logic'('0');
        else
          stage_2 <= p2_stage_2;
        end if;
      end if;
    end if;

  end process;

  --control_2, which is an e_mux
  p2_full_2 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_1, full_3);
  --control_reg_2, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_2 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_2 <= std_logic'('0');
        else
          full_2 <= p2_full_2;
        end if;
      end if;
    end if;

  end process;

  --data_1, which is an e_mux
  p1_stage_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_2 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_2);
  --data_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_1))))) = '1' then 
        if std_logic'(((sync_reset AND full_1) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_2))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_1 <= std_logic'('0');
        else
          stage_1 <= p1_stage_1;
        end if;
      end if;
    end if;

  end process;

  --control_1, which is an e_mux
  p1_full_1 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), full_0, full_2);
  --control_reg_1, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(clear_fifo) = '1' then 
          full_1 <= std_logic'('0');
        else
          full_1 <= p1_full_1;
        end if;
      end if;
    end if;

  end process;

  --data_0, which is an e_mux
  p0_stage_0 <= A_WE_StdLogic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((full_1 AND NOT clear_fifo))))) = std_logic_vector'("00000000000000000000000000000000"))), data_in, stage_1);
  --data_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      stage_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'(((sync_reset AND full_0) AND NOT((((to_std_logic((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1))) = std_logic_vector'("00000000000000000000000000000000")))) AND read) AND write))))) = '1' then 
          stage_0 <= std_logic'('0');
        else
          stage_0 <= p0_stage_0;
        end if;
      end if;
    end if;

  end process;

  --control_0, which is an e_mux
  p0_full_0 <= Vector_To_Std_Logic(A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((read AND NOT(write)))))) = std_logic_vector'("00000000000000000000000000000000"))), std_logic_vector'("00000000000000000000000000000001"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(full_1)))));
  --control_reg_0, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      full_0 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((clear_fifo OR ((read XOR write))) OR ((write AND NOT(full_0))))) = '1' then 
        if std_logic'((clear_fifo AND NOT write)) = '1' then 
          full_0 <= std_logic'('0');
        else
          full_0 <= p0_full_0;
        end if;
      end if;
    end if;

  end process;

  one_count_plus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000000") & (how_many_ones)) + std_logic_vector'("000000000000000000000000000000001")), 5);
  one_count_minus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000000") & (how_many_ones)) - std_logic_vector'("000000000000000000000000000000001")), 5);
  --updated_one_count, which is an e_mux
  updated_one_count <= A_EXT (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND NOT(write)))) = '1'), std_logic_vector'("00000000000000000000000000000000"), (std_logic_vector'("000000000000000000000000000") & (A_WE_StdLogicVector((std_logic'(((((clear_fifo OR sync_reset)) AND write))) = '1'), (std_logic_vector'("0000") & (A_TOSTDLOGICVECTOR(data_in))), A_WE_StdLogicVector((std_logic'(((((read AND (data_in)) AND write) AND (stage_0)))) = '1'), how_many_ones, A_WE_StdLogicVector((std_logic'(((write AND (data_in)))) = '1'), one_count_plus_one, A_WE_StdLogicVector((std_logic'(((read AND (stage_0)))) = '1'), one_count_minus_one, how_many_ones))))))), 5);
  --counts how many ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      how_many_ones <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        how_many_ones <= updated_one_count;
      end if;
    end if;

  end process;

  --this fifo contains ones in the data pipeline, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      fifo_contains_ones_n <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'((((clear_fifo OR sync_reset) OR read) OR write)) = '1' then 
        fifo_contains_ones_n <= NOT (or_reduce(updated_one_count));
      end if;
    end if;

  end process;


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library std;
use std.textio.all;

entity system_burst_1_upstream_arbitrator is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal clock_crossing_read_m1_address_to_slave : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal clock_crossing_read_m1_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal clock_crossing_read_m1_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal clock_crossing_read_m1_dbs_address : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal clock_crossing_read_m1_dbs_write_16 : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal clock_crossing_read_m1_latency_counter : IN STD_LOGIC;
                 signal clock_crossing_read_m1_read : IN STD_LOGIC;
                 signal clock_crossing_read_m1_write : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal system_burst_1_upstream_readdata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal system_burst_1_upstream_readdatavalid : IN STD_LOGIC;
                 signal system_burst_1_upstream_waitrequest : IN STD_LOGIC;

              -- outputs:
                 signal clock_crossing_read_m1_byteenable_system_burst_1_upstream : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal clock_crossing_read_m1_granted_system_burst_1_upstream : OUT STD_LOGIC;
                 signal clock_crossing_read_m1_qualified_request_system_burst_1_upstream : OUT STD_LOGIC;
                 signal clock_crossing_read_m1_read_data_valid_system_burst_1_upstream : OUT STD_LOGIC;
                 signal clock_crossing_read_m1_read_data_valid_system_burst_1_upstream_shift_register : OUT STD_LOGIC;
                 signal clock_crossing_read_m1_requests_system_burst_1_upstream : OUT STD_LOGIC;
                 signal d1_system_burst_1_upstream_end_xfer : OUT STD_LOGIC;
                 signal system_burst_1_upstream_address : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal system_burst_1_upstream_burstcount : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                 signal system_burst_1_upstream_byteaddress : OUT STD_LOGIC_VECTOR (24 DOWNTO 0);
                 signal system_burst_1_upstream_byteenable : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal system_burst_1_upstream_debugaccess : OUT STD_LOGIC;
                 signal system_burst_1_upstream_read : OUT STD_LOGIC;
                 signal system_burst_1_upstream_readdata_from_sa : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal system_burst_1_upstream_waitrequest_from_sa : OUT STD_LOGIC;
                 signal system_burst_1_upstream_write : OUT STD_LOGIC;
                 signal system_burst_1_upstream_writedata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
              );
end entity system_burst_1_upstream_arbitrator;


architecture europa of system_burst_1_upstream_arbitrator is
component burstcount_fifo_for_system_burst_1_upstream_module is 
           port (
                 -- inputs:
                    signal clear_fifo : IN STD_LOGIC;
                    signal clk : IN STD_LOGIC;
                    signal data_in : IN STD_LOGIC_VECTOR (4 DOWNTO 0);
                    signal read : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal sync_reset : IN STD_LOGIC;
                    signal write : IN STD_LOGIC;

                 -- outputs:
                    signal data_out : OUT STD_LOGIC_VECTOR (4 DOWNTO 0);
                    signal empty : OUT STD_LOGIC;
                    signal fifo_contains_ones_n : OUT STD_LOGIC;
                    signal full : OUT STD_LOGIC
                 );
end component burstcount_fifo_for_system_burst_1_upstream_module;

component rdv_fifo_for_clock_crossing_read_m1_to_system_burst_1_upstream_module is 
           port (
                 -- inputs:
                    signal clear_fifo : IN STD_LOGIC;
                    signal clk : IN STD_LOGIC;
                    signal data_in : IN STD_LOGIC;
                    signal read : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal sync_reset : IN STD_LOGIC;
                    signal write : IN STD_LOGIC;

                 -- outputs:
                    signal data_out : OUT STD_LOGIC;
                    signal empty : OUT STD_LOGIC;
                    signal fifo_contains_ones_n : OUT STD_LOGIC;
                    signal full : OUT STD_LOGIC
                 );
end component rdv_fifo_for_clock_crossing_read_m1_to_system_burst_1_upstream_module;

                signal clock_crossing_read_m1_arbiterlock :  STD_LOGIC;
                signal clock_crossing_read_m1_arbiterlock2 :  STD_LOGIC;
                signal clock_crossing_read_m1_byteenable_system_burst_1_upstream_segment_0 :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_read_m1_byteenable_system_burst_1_upstream_segment_1 :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_read_m1_continuerequest :  STD_LOGIC;
                signal clock_crossing_read_m1_rdv_fifo_empty_system_burst_1_upstream :  STD_LOGIC;
                signal clock_crossing_read_m1_rdv_fifo_output_from_system_burst_1_upstream :  STD_LOGIC;
                signal clock_crossing_read_m1_saved_grant_system_burst_1_upstream :  STD_LOGIC;
                signal d1_reasons_to_wait :  STD_LOGIC;
                signal enable_nonzero_assertions :  STD_LOGIC;
                signal end_xfer_arb_share_counter_term_system_burst_1_upstream :  STD_LOGIC;
                signal in_a_read_cycle :  STD_LOGIC;
                signal in_a_write_cycle :  STD_LOGIC;
                signal internal_clock_crossing_read_m1_byteenable_system_burst_1_upstream :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal internal_clock_crossing_read_m1_granted_system_burst_1_upstream :  STD_LOGIC;
                signal internal_clock_crossing_read_m1_qualified_request_system_burst_1_upstream :  STD_LOGIC;
                signal internal_clock_crossing_read_m1_requests_system_burst_1_upstream :  STD_LOGIC;
                signal internal_system_burst_1_upstream_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal internal_system_burst_1_upstream_read :  STD_LOGIC;
                signal internal_system_burst_1_upstream_waitrequest_from_sa :  STD_LOGIC;
                signal internal_system_burst_1_upstream_write :  STD_LOGIC;
                signal module_input18 :  STD_LOGIC;
                signal module_input19 :  STD_LOGIC;
                signal module_input20 :  STD_LOGIC;
                signal module_input21 :  STD_LOGIC;
                signal module_input22 :  STD_LOGIC;
                signal module_input23 :  STD_LOGIC;
                signal p0_system_burst_1_upstream_load_fifo :  STD_LOGIC;
                signal system_burst_1_upstream_allgrants :  STD_LOGIC;
                signal system_burst_1_upstream_allow_new_arb_cycle :  STD_LOGIC;
                signal system_burst_1_upstream_any_bursting_master_saved_grant :  STD_LOGIC;
                signal system_burst_1_upstream_any_continuerequest :  STD_LOGIC;
                signal system_burst_1_upstream_arb_counter_enable :  STD_LOGIC;
                signal system_burst_1_upstream_arb_share_counter :  STD_LOGIC_VECTOR (5 DOWNTO 0);
                signal system_burst_1_upstream_arb_share_counter_next_value :  STD_LOGIC_VECTOR (5 DOWNTO 0);
                signal system_burst_1_upstream_arb_share_set_values :  STD_LOGIC_VECTOR (5 DOWNTO 0);
                signal system_burst_1_upstream_bbt_burstcounter :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal system_burst_1_upstream_beginbursttransfer_internal :  STD_LOGIC;
                signal system_burst_1_upstream_begins_xfer :  STD_LOGIC;
                signal system_burst_1_upstream_burstcount_fifo_empty :  STD_LOGIC;
                signal system_burst_1_upstream_current_burst :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal system_burst_1_upstream_current_burst_minus_one :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal system_burst_1_upstream_end_xfer :  STD_LOGIC;
                signal system_burst_1_upstream_firsttransfer :  STD_LOGIC;
                signal system_burst_1_upstream_grant_vector :  STD_LOGIC;
                signal system_burst_1_upstream_in_a_read_cycle :  STD_LOGIC;
                signal system_burst_1_upstream_in_a_write_cycle :  STD_LOGIC;
                signal system_burst_1_upstream_load_fifo :  STD_LOGIC;
                signal system_burst_1_upstream_master_qreq_vector :  STD_LOGIC;
                signal system_burst_1_upstream_move_on_to_next_transaction :  STD_LOGIC;
                signal system_burst_1_upstream_next_bbt_burstcount :  STD_LOGIC_VECTOR (2 DOWNTO 0);
                signal system_burst_1_upstream_next_burst_count :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal system_burst_1_upstream_non_bursting_master_requests :  STD_LOGIC;
                signal system_burst_1_upstream_readdatavalid_from_sa :  STD_LOGIC;
                signal system_burst_1_upstream_reg_firsttransfer :  STD_LOGIC;
                signal system_burst_1_upstream_selected_burstcount :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal system_burst_1_upstream_slavearbiterlockenable :  STD_LOGIC;
                signal system_burst_1_upstream_slavearbiterlockenable2 :  STD_LOGIC;
                signal system_burst_1_upstream_this_cycle_is_the_last_burst :  STD_LOGIC;
                signal system_burst_1_upstream_transaction_burst_count :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal system_burst_1_upstream_unreg_firsttransfer :  STD_LOGIC;
                signal system_burst_1_upstream_waits_for_read :  STD_LOGIC;
                signal system_burst_1_upstream_waits_for_write :  STD_LOGIC;
                signal wait_for_system_burst_1_upstream_counter :  STD_LOGIC;

begin

  process (clk, reset_n)
  begin
    if reset_n = '0' then
      d1_reasons_to_wait <= std_logic'('0');
    elsif clk'event and clk = '1' then
      d1_reasons_to_wait <= NOT system_burst_1_upstream_end_xfer;
    end if;

  end process;

  system_burst_1_upstream_begins_xfer <= NOT d1_reasons_to_wait AND (internal_clock_crossing_read_m1_qualified_request_system_burst_1_upstream);
  --assign system_burst_1_upstream_readdata_from_sa = system_burst_1_upstream_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  system_burst_1_upstream_readdata_from_sa <= system_burst_1_upstream_readdata;
  internal_clock_crossing_read_m1_requests_system_burst_1_upstream <= Vector_To_Std_Logic(((std_logic_vector'("00000000000000000000000000000001")) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((clock_crossing_read_m1_read OR clock_crossing_read_m1_write)))))));
  --assign system_burst_1_upstream_waitrequest_from_sa = system_burst_1_upstream_waitrequest so that symbol knows where to group signals which may go to master only, which is an e_assign
  internal_system_burst_1_upstream_waitrequest_from_sa <= system_burst_1_upstream_waitrequest;
  --assign system_burst_1_upstream_readdatavalid_from_sa = system_burst_1_upstream_readdatavalid so that symbol knows where to group signals which may go to master only, which is an e_assign
  system_burst_1_upstream_readdatavalid_from_sa <= system_burst_1_upstream_readdatavalid;
  --system_burst_1_upstream_arb_share_counter set values, which is an e_mux
  system_burst_1_upstream_arb_share_set_values <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_clock_crossing_read_m1_granted_system_burst_1_upstream)) = '1'), (A_WE_StdLogicVector((std_logic'((clock_crossing_read_m1_write)) = '1'), (std_logic_vector'("0000000000000000000000000000") & (A_SLL(clock_crossing_read_m1_burstcount,std_logic_vector'("00000000000000000000000000000001")))), std_logic_vector'("00000000000000000000000000000001"))), std_logic_vector'("00000000000000000000000000000001")), 6);
  --system_burst_1_upstream_non_bursting_master_requests mux, which is an e_mux
  system_burst_1_upstream_non_bursting_master_requests <= std_logic'('0');
  --system_burst_1_upstream_any_bursting_master_saved_grant mux, which is an e_mux
  system_burst_1_upstream_any_bursting_master_saved_grant <= clock_crossing_read_m1_saved_grant_system_burst_1_upstream;
  --system_burst_1_upstream_arb_share_counter_next_value assignment, which is an e_assign
  system_burst_1_upstream_arb_share_counter_next_value <= A_EXT (A_WE_StdLogicVector((std_logic'(system_burst_1_upstream_firsttransfer) = '1'), (((std_logic_vector'("000000000000000000000000000") & (system_burst_1_upstream_arb_share_set_values)) - std_logic_vector'("000000000000000000000000000000001"))), A_WE_StdLogicVector((std_logic'(or_reduce(system_burst_1_upstream_arb_share_counter)) = '1'), (((std_logic_vector'("000000000000000000000000000") & (system_burst_1_upstream_arb_share_counter)) - std_logic_vector'("000000000000000000000000000000001"))), std_logic_vector'("000000000000000000000000000000000"))), 6);
  --system_burst_1_upstream_allgrants all slave grants, which is an e_mux
  system_burst_1_upstream_allgrants <= system_burst_1_upstream_grant_vector;
  --system_burst_1_upstream_end_xfer assignment, which is an e_assign
  system_burst_1_upstream_end_xfer <= NOT ((system_burst_1_upstream_waits_for_read OR system_burst_1_upstream_waits_for_write));
  --end_xfer_arb_share_counter_term_system_burst_1_upstream arb share counter enable term, which is an e_assign
  end_xfer_arb_share_counter_term_system_burst_1_upstream <= system_burst_1_upstream_end_xfer AND (((NOT system_burst_1_upstream_any_bursting_master_saved_grant OR in_a_read_cycle) OR in_a_write_cycle));
  --system_burst_1_upstream_arb_share_counter arbitration counter enable, which is an e_assign
  system_burst_1_upstream_arb_counter_enable <= ((end_xfer_arb_share_counter_term_system_burst_1_upstream AND system_burst_1_upstream_allgrants)) OR ((end_xfer_arb_share_counter_term_system_burst_1_upstream AND NOT system_burst_1_upstream_non_bursting_master_requests));
  --system_burst_1_upstream_arb_share_counter counter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_1_upstream_arb_share_counter <= std_logic_vector'("000000");
    elsif clk'event and clk = '1' then
      if std_logic'(system_burst_1_upstream_arb_counter_enable) = '1' then 
        system_burst_1_upstream_arb_share_counter <= system_burst_1_upstream_arb_share_counter_next_value;
      end if;
    end if;

  end process;

  --system_burst_1_upstream_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_1_upstream_slavearbiterlockenable <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'((((system_burst_1_upstream_master_qreq_vector AND end_xfer_arb_share_counter_term_system_burst_1_upstream)) OR ((end_xfer_arb_share_counter_term_system_burst_1_upstream AND NOT system_burst_1_upstream_non_bursting_master_requests)))) = '1' then 
        system_burst_1_upstream_slavearbiterlockenable <= or_reduce(system_burst_1_upstream_arb_share_counter_next_value);
      end if;
    end if;

  end process;

  --clock_crossing_read/m1 system_burst_1/upstream arbiterlock, which is an e_assign
  clock_crossing_read_m1_arbiterlock <= system_burst_1_upstream_slavearbiterlockenable AND clock_crossing_read_m1_continuerequest;
  --system_burst_1_upstream_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  system_burst_1_upstream_slavearbiterlockenable2 <= or_reduce(system_burst_1_upstream_arb_share_counter_next_value);
  --clock_crossing_read/m1 system_burst_1/upstream arbiterlock2, which is an e_assign
  clock_crossing_read_m1_arbiterlock2 <= system_burst_1_upstream_slavearbiterlockenable2 AND clock_crossing_read_m1_continuerequest;
  --system_burst_1_upstream_any_continuerequest at least one master continues requesting, which is an e_assign
  system_burst_1_upstream_any_continuerequest <= std_logic'('1');
  --clock_crossing_read_m1_continuerequest continued request, which is an e_assign
  clock_crossing_read_m1_continuerequest <= std_logic'('1');
  internal_clock_crossing_read_m1_qualified_request_system_burst_1_upstream <= internal_clock_crossing_read_m1_requests_system_burst_1_upstream AND NOT ((clock_crossing_read_m1_read AND to_std_logic((((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(clock_crossing_read_m1_latency_counter))) /= std_logic_vector'("00000000000000000000000000000000"))) OR ((std_logic_vector'("00000000000000000000000000000001")<(std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(clock_crossing_read_m1_latency_counter))))))))));
  --unique name for system_burst_1_upstream_move_on_to_next_transaction, which is an e_assign
  system_burst_1_upstream_move_on_to_next_transaction <= system_burst_1_upstream_this_cycle_is_the_last_burst AND system_burst_1_upstream_load_fifo;
  --the currently selected burstcount for system_burst_1_upstream, which is an e_mux
  system_burst_1_upstream_selected_burstcount <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_clock_crossing_read_m1_granted_system_burst_1_upstream)) = '1'), (std_logic_vector'("0000000000000000000000000000") & (clock_crossing_read_m1_burstcount)), std_logic_vector'("00000000000000000000000000000001")), 5);
  --burstcount_fifo_for_system_burst_1_upstream, which is an e_fifo_with_registered_outputs
  burstcount_fifo_for_system_burst_1_upstream : burstcount_fifo_for_system_burst_1_upstream_module
    port map(
      data_out => system_burst_1_upstream_transaction_burst_count,
      empty => system_burst_1_upstream_burstcount_fifo_empty,
      fifo_contains_ones_n => open,
      full => open,
      clear_fifo => module_input18,
      clk => clk,
      data_in => system_burst_1_upstream_selected_burstcount,
      read => system_burst_1_upstream_this_cycle_is_the_last_burst,
      reset_n => reset_n,
      sync_reset => module_input19,
      write => module_input20
    );

  module_input18 <= std_logic'('0');
  module_input19 <= std_logic'('0');
  module_input20 <= ((in_a_read_cycle AND NOT system_burst_1_upstream_waits_for_read) AND system_burst_1_upstream_load_fifo) AND NOT ((system_burst_1_upstream_this_cycle_is_the_last_burst AND system_burst_1_upstream_burstcount_fifo_empty));

  --system_burst_1_upstream current burst minus one, which is an e_assign
  system_burst_1_upstream_current_burst_minus_one <= A_EXT (((std_logic_vector'("0000000000000000000000000000") & (system_burst_1_upstream_current_burst)) - std_logic_vector'("000000000000000000000000000000001")), 5);
  --what to load in current_burst, for system_burst_1_upstream, which is an e_mux
  system_burst_1_upstream_next_burst_count <= A_EXT (A_WE_StdLogicVector((std_logic'(((((in_a_read_cycle AND NOT system_burst_1_upstream_waits_for_read)) AND NOT system_burst_1_upstream_load_fifo))) = '1'), (system_burst_1_upstream_selected_burstcount & A_ToStdLogicVector(std_logic'('0'))), A_WE_StdLogicVector((std_logic'(((((in_a_read_cycle AND NOT system_burst_1_upstream_waits_for_read) AND system_burst_1_upstream_this_cycle_is_the_last_burst) AND system_burst_1_upstream_burstcount_fifo_empty))) = '1'), (system_burst_1_upstream_selected_burstcount & A_ToStdLogicVector(std_logic'('0'))), A_WE_StdLogicVector((std_logic'((system_burst_1_upstream_this_cycle_is_the_last_burst)) = '1'), (system_burst_1_upstream_transaction_burst_count & A_ToStdLogicVector(std_logic'('0'))), (std_logic_vector'("0") & (system_burst_1_upstream_current_burst_minus_one))))), 5);
  --the current burst count for system_burst_1_upstream, to be decremented, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_1_upstream_current_burst <= std_logic_vector'("00000");
    elsif clk'event and clk = '1' then
      if std_logic'((system_burst_1_upstream_readdatavalid_from_sa OR ((NOT system_burst_1_upstream_load_fifo AND ((in_a_read_cycle AND NOT system_burst_1_upstream_waits_for_read)))))) = '1' then 
        system_burst_1_upstream_current_burst <= system_burst_1_upstream_next_burst_count;
      end if;
    end if;

  end process;

  --a 1 or burstcount fifo empty, to initialize the counter, which is an e_mux
  p0_system_burst_1_upstream_load_fifo <= Vector_To_Std_Logic(A_WE_StdLogicVector((std_logic'((NOT system_burst_1_upstream_load_fifo)) = '1'), std_logic_vector'("00000000000000000000000000000001"), A_WE_StdLogicVector((std_logic'(((((in_a_read_cycle AND NOT system_burst_1_upstream_waits_for_read)) AND system_burst_1_upstream_load_fifo))) = '1'), std_logic_vector'("00000000000000000000000000000001"), (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT system_burst_1_upstream_burstcount_fifo_empty))))));
  --whether to load directly to the counter or to the fifo, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_1_upstream_load_fifo <= std_logic'('0');
    elsif clk'event and clk = '1' then
      if std_logic'(((((in_a_read_cycle AND NOT system_burst_1_upstream_waits_for_read)) AND NOT system_burst_1_upstream_load_fifo) OR system_burst_1_upstream_this_cycle_is_the_last_burst)) = '1' then 
        system_burst_1_upstream_load_fifo <= p0_system_burst_1_upstream_load_fifo;
      end if;
    end if;

  end process;

  --the last cycle in the burst for system_burst_1_upstream, which is an e_assign
  system_burst_1_upstream_this_cycle_is_the_last_burst <= NOT (or_reduce(system_burst_1_upstream_current_burst_minus_one)) AND system_burst_1_upstream_readdatavalid_from_sa;
  --rdv_fifo_for_clock_crossing_read_m1_to_system_burst_1_upstream, which is an e_fifo_with_registered_outputs
  rdv_fifo_for_clock_crossing_read_m1_to_system_burst_1_upstream : rdv_fifo_for_clock_crossing_read_m1_to_system_burst_1_upstream_module
    port map(
      data_out => clock_crossing_read_m1_rdv_fifo_output_from_system_burst_1_upstream,
      empty => open,
      fifo_contains_ones_n => clock_crossing_read_m1_rdv_fifo_empty_system_burst_1_upstream,
      full => open,
      clear_fifo => module_input21,
      clk => clk,
      data_in => internal_clock_crossing_read_m1_granted_system_burst_1_upstream,
      read => system_burst_1_upstream_move_on_to_next_transaction,
      reset_n => reset_n,
      sync_reset => module_input22,
      write => module_input23
    );

  module_input21 <= std_logic'('0');
  module_input22 <= std_logic'('0');
  module_input23 <= in_a_read_cycle AND NOT system_burst_1_upstream_waits_for_read;

  clock_crossing_read_m1_read_data_valid_system_burst_1_upstream_shift_register <= NOT clock_crossing_read_m1_rdv_fifo_empty_system_burst_1_upstream;
  --local readdatavalid clock_crossing_read_m1_read_data_valid_system_burst_1_upstream, which is an e_mux
  clock_crossing_read_m1_read_data_valid_system_burst_1_upstream <= system_burst_1_upstream_readdatavalid_from_sa;
  --system_burst_1_upstream_writedata mux, which is an e_mux
  system_burst_1_upstream_writedata <= clock_crossing_read_m1_dbs_write_16;
  --byteaddress mux for system_burst_1/upstream, which is an e_mux
  system_burst_1_upstream_byteaddress <= std_logic_vector'("0") & (clock_crossing_read_m1_address_to_slave);
  --master is always granted when requested
  internal_clock_crossing_read_m1_granted_system_burst_1_upstream <= internal_clock_crossing_read_m1_qualified_request_system_burst_1_upstream;
  --clock_crossing_read/m1 saved-grant system_burst_1/upstream, which is an e_assign
  clock_crossing_read_m1_saved_grant_system_burst_1_upstream <= internal_clock_crossing_read_m1_requests_system_burst_1_upstream;
  --allow new arb cycle for system_burst_1/upstream, which is an e_assign
  system_burst_1_upstream_allow_new_arb_cycle <= std_logic'('1');
  --placeholder chosen master
  system_burst_1_upstream_grant_vector <= std_logic'('1');
  --placeholder vector of master qualified-requests
  system_burst_1_upstream_master_qreq_vector <= std_logic'('1');
  --system_burst_1_upstream_firsttransfer first transaction, which is an e_assign
  system_burst_1_upstream_firsttransfer <= A_WE_StdLogic((std_logic'(system_burst_1_upstream_begins_xfer) = '1'), system_burst_1_upstream_unreg_firsttransfer, system_burst_1_upstream_reg_firsttransfer);
  --system_burst_1_upstream_unreg_firsttransfer first transaction, which is an e_assign
  system_burst_1_upstream_unreg_firsttransfer <= NOT ((system_burst_1_upstream_slavearbiterlockenable AND system_burst_1_upstream_any_continuerequest));
  --system_burst_1_upstream_reg_firsttransfer first transaction, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_1_upstream_reg_firsttransfer <= std_logic'('1');
    elsif clk'event and clk = '1' then
      if std_logic'(system_burst_1_upstream_begins_xfer) = '1' then 
        system_burst_1_upstream_reg_firsttransfer <= system_burst_1_upstream_unreg_firsttransfer;
      end if;
    end if;

  end process;

  --system_burst_1_upstream_next_bbt_burstcount next_bbt_burstcount, which is an e_mux
  system_burst_1_upstream_next_bbt_burstcount <= A_EXT (A_WE_StdLogicVector((std_logic'((((internal_system_burst_1_upstream_write) AND to_std_logic((((std_logic_vector'("00000000000000000000000000000") & (system_burst_1_upstream_bbt_burstcounter)) = std_logic_vector'("00000000000000000000000000000000"))))))) = '1'), (((std_logic_vector'("00000000000000000000000000000") & (internal_system_burst_1_upstream_burstcount)) - std_logic_vector'("000000000000000000000000000000001"))), A_WE_StdLogicVector((std_logic'((((internal_system_burst_1_upstream_read) AND to_std_logic((((std_logic_vector'("00000000000000000000000000000") & (system_burst_1_upstream_bbt_burstcounter)) = std_logic_vector'("00000000000000000000000000000000"))))))) = '1'), std_logic_vector'("000000000000000000000000000000000"), (((std_logic_vector'("000000000000000000000000000000") & (system_burst_1_upstream_bbt_burstcounter)) - std_logic_vector'("000000000000000000000000000000001"))))), 3);
  --system_burst_1_upstream_bbt_burstcounter bbt_burstcounter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_1_upstream_bbt_burstcounter <= std_logic_vector'("000");
    elsif clk'event and clk = '1' then
      if std_logic'(system_burst_1_upstream_begins_xfer) = '1' then 
        system_burst_1_upstream_bbt_burstcounter <= system_burst_1_upstream_next_bbt_burstcount;
      end if;
    end if;

  end process;

  --system_burst_1_upstream_beginbursttransfer_internal begin burst transfer, which is an e_assign
  system_burst_1_upstream_beginbursttransfer_internal <= system_burst_1_upstream_begins_xfer AND to_std_logic((((std_logic_vector'("00000000000000000000000000000") & (system_burst_1_upstream_bbt_burstcounter)) = std_logic_vector'("00000000000000000000000000000000"))));
  --system_burst_1_upstream_read assignment, which is an e_mux
  internal_system_burst_1_upstream_read <= internal_clock_crossing_read_m1_granted_system_burst_1_upstream AND clock_crossing_read_m1_read;
  --system_burst_1_upstream_write assignment, which is an e_mux
  internal_system_burst_1_upstream_write <= internal_clock_crossing_read_m1_granted_system_burst_1_upstream AND clock_crossing_read_m1_write;
  --system_burst_1_upstream_address mux, which is an e_mux
  system_burst_1_upstream_address <= A_EXT (Std_Logic_Vector'(A_SRL(clock_crossing_read_m1_address_to_slave,std_logic_vector'("00000000000000000000000000000010")) & A_ToStdLogicVector(clock_crossing_read_m1_dbs_address(1)) & A_ToStdLogicVector(std_logic'('0'))), 24);
  --d1_system_burst_1_upstream_end_xfer register, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      d1_system_burst_1_upstream_end_xfer <= std_logic'('1');
    elsif clk'event and clk = '1' then
      d1_system_burst_1_upstream_end_xfer <= system_burst_1_upstream_end_xfer;
    end if;

  end process;

  --system_burst_1_upstream_waits_for_read in a cycle, which is an e_mux
  system_burst_1_upstream_waits_for_read <= system_burst_1_upstream_in_a_read_cycle AND internal_system_burst_1_upstream_waitrequest_from_sa;
  --system_burst_1_upstream_in_a_read_cycle assignment, which is an e_assign
  system_burst_1_upstream_in_a_read_cycle <= internal_clock_crossing_read_m1_granted_system_burst_1_upstream AND clock_crossing_read_m1_read;
  --in_a_read_cycle assignment, which is an e_mux
  in_a_read_cycle <= system_burst_1_upstream_in_a_read_cycle;
  --system_burst_1_upstream_waits_for_write in a cycle, which is an e_mux
  system_burst_1_upstream_waits_for_write <= system_burst_1_upstream_in_a_write_cycle AND internal_system_burst_1_upstream_waitrequest_from_sa;
  --system_burst_1_upstream_in_a_write_cycle assignment, which is an e_assign
  system_burst_1_upstream_in_a_write_cycle <= internal_clock_crossing_read_m1_granted_system_burst_1_upstream AND clock_crossing_read_m1_write;
  --in_a_write_cycle assignment, which is an e_mux
  in_a_write_cycle <= system_burst_1_upstream_in_a_write_cycle;
  wait_for_system_burst_1_upstream_counter <= std_logic'('0');
  --system_burst_1_upstream_byteenable byte enable port mux, which is an e_mux
  system_burst_1_upstream_byteenable <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_clock_crossing_read_m1_granted_system_burst_1_upstream)) = '1'), (std_logic_vector'("000000000000000000000000000000") & (internal_clock_crossing_read_m1_byteenable_system_burst_1_upstream)), -SIGNED(std_logic_vector'("00000000000000000000000000000001"))), 2);
  (clock_crossing_read_m1_byteenable_system_burst_1_upstream_segment_1(1), clock_crossing_read_m1_byteenable_system_burst_1_upstream_segment_1(0), clock_crossing_read_m1_byteenable_system_burst_1_upstream_segment_0(1), clock_crossing_read_m1_byteenable_system_burst_1_upstream_segment_0(0)) <= clock_crossing_read_m1_byteenable;
  internal_clock_crossing_read_m1_byteenable_system_burst_1_upstream <= A_WE_StdLogicVector((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(clock_crossing_read_m1_dbs_address(1)))) = std_logic_vector'("00000000000000000000000000000000"))), clock_crossing_read_m1_byteenable_system_burst_1_upstream_segment_0, clock_crossing_read_m1_byteenable_system_burst_1_upstream_segment_1);
  --burstcount mux, which is an e_mux
  internal_system_burst_1_upstream_burstcount <= A_EXT (A_WE_StdLogicVector((std_logic'((internal_clock_crossing_read_m1_granted_system_burst_1_upstream)) = '1'), (std_logic_vector'("0000000000000000000000000000") & (clock_crossing_read_m1_burstcount)), std_logic_vector'("00000000000000000000000000000001")), 4);
  --debugaccess mux, which is an e_mux
  system_burst_1_upstream_debugaccess <= std_logic'('0');
  --vhdl renameroo for output signals
  clock_crossing_read_m1_byteenable_system_burst_1_upstream <= internal_clock_crossing_read_m1_byteenable_system_burst_1_upstream;
  --vhdl renameroo for output signals
  clock_crossing_read_m1_granted_system_burst_1_upstream <= internal_clock_crossing_read_m1_granted_system_burst_1_upstream;
  --vhdl renameroo for output signals
  clock_crossing_read_m1_qualified_request_system_burst_1_upstream <= internal_clock_crossing_read_m1_qualified_request_system_burst_1_upstream;
  --vhdl renameroo for output signals
  clock_crossing_read_m1_requests_system_burst_1_upstream <= internal_clock_crossing_read_m1_requests_system_burst_1_upstream;
  --vhdl renameroo for output signals
  system_burst_1_upstream_burstcount <= internal_system_burst_1_upstream_burstcount;
  --vhdl renameroo for output signals
  system_burst_1_upstream_read <= internal_system_burst_1_upstream_read;
  --vhdl renameroo for output signals
  system_burst_1_upstream_waitrequest_from_sa <= internal_system_burst_1_upstream_waitrequest_from_sa;
  --vhdl renameroo for output signals
  system_burst_1_upstream_write <= internal_system_burst_1_upstream_write;
--synthesis translate_off
    --system_burst_1/upstream enable non-zero assertions, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        enable_nonzero_assertions <= std_logic'('0');
      elsif clk'event and clk = '1' then
        enable_nonzero_assertions <= std_logic'('1');
      end if;

    end process;

    --clock_crossing_read/m1 non-zero burstcount assertion, which is an e_process
    process (clk)
    VARIABLE write_line36 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((internal_clock_crossing_read_m1_requests_system_burst_1_upstream AND to_std_logic((((std_logic_vector'("0000000000000000000000000000") & (clock_crossing_read_m1_burstcount)) = std_logic_vector'("00000000000000000000000000000000"))))) AND enable_nonzero_assertions)) = '1' then 
          write(write_line36, now);
          write(write_line36, string'(": "));
          write(write_line36, string'("clock_crossing_read/m1 drove 0 on its 'burstcount' port while accessing slave system_burst_1/upstream"));
          write(output, write_line36.all);
          deallocate (write_line36);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

--synthesis translate_on

end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library std;
use std.textio.all;

entity system_burst_1_downstream_arbitrator is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal d1_sdram_s1_end_xfer : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;
                 signal sdram_s1_readdata_from_sa : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal sdram_s1_waitrequest_from_sa : IN STD_LOGIC;
                 signal system_burst_1_downstream_address : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal system_burst_1_downstream_burstcount : IN STD_LOGIC;
                 signal system_burst_1_downstream_byteenable : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal system_burst_1_downstream_granted_sdram_s1 : IN STD_LOGIC;
                 signal system_burst_1_downstream_qualified_request_sdram_s1 : IN STD_LOGIC;
                 signal system_burst_1_downstream_read : IN STD_LOGIC;
                 signal system_burst_1_downstream_read_data_valid_sdram_s1 : IN STD_LOGIC;
                 signal system_burst_1_downstream_read_data_valid_sdram_s1_shift_register : IN STD_LOGIC;
                 signal system_burst_1_downstream_requests_sdram_s1 : IN STD_LOGIC;
                 signal system_burst_1_downstream_write : IN STD_LOGIC;
                 signal system_burst_1_downstream_writedata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);

              -- outputs:
                 signal system_burst_1_downstream_address_to_slave : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal system_burst_1_downstream_latency_counter : OUT STD_LOGIC;
                 signal system_burst_1_downstream_readdata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal system_burst_1_downstream_readdatavalid : OUT STD_LOGIC;
                 signal system_burst_1_downstream_reset_n : OUT STD_LOGIC;
                 signal system_burst_1_downstream_waitrequest : OUT STD_LOGIC
              );
end entity system_burst_1_downstream_arbitrator;


architecture europa of system_burst_1_downstream_arbitrator is
                signal active_and_waiting_last_time :  STD_LOGIC;
                signal internal_system_burst_1_downstream_address_to_slave :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal internal_system_burst_1_downstream_latency_counter :  STD_LOGIC;
                signal internal_system_burst_1_downstream_waitrequest :  STD_LOGIC;
                signal latency_load_value :  STD_LOGIC;
                signal p1_system_burst_1_downstream_latency_counter :  STD_LOGIC;
                signal pre_flush_system_burst_1_downstream_readdatavalid :  STD_LOGIC;
                signal r_0 :  STD_LOGIC;
                signal system_burst_1_downstream_address_last_time :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal system_burst_1_downstream_burstcount_last_time :  STD_LOGIC;
                signal system_burst_1_downstream_byteenable_last_time :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal system_burst_1_downstream_is_granted_some_slave :  STD_LOGIC;
                signal system_burst_1_downstream_read_but_no_slave_selected :  STD_LOGIC;
                signal system_burst_1_downstream_read_last_time :  STD_LOGIC;
                signal system_burst_1_downstream_run :  STD_LOGIC;
                signal system_burst_1_downstream_write_last_time :  STD_LOGIC;
                signal system_burst_1_downstream_writedata_last_time :  STD_LOGIC_VECTOR (15 DOWNTO 0);

begin

  --r_0 master_run cascaded wait assignment, which is an e_assign
  r_0 <= Vector_To_Std_Logic(((((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((system_burst_1_downstream_qualified_request_sdram_s1 OR NOT system_burst_1_downstream_requests_sdram_s1)))))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((system_burst_1_downstream_granted_sdram_s1 OR NOT system_burst_1_downstream_qualified_request_sdram_s1)))))) AND (((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((NOT system_burst_1_downstream_qualified_request_sdram_s1 OR NOT ((system_burst_1_downstream_read OR system_burst_1_downstream_write)))))) OR (((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT sdram_s1_waitrequest_from_sa)))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((system_burst_1_downstream_read OR system_burst_1_downstream_write)))))))))) AND (((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR((NOT system_burst_1_downstream_qualified_request_sdram_s1 OR NOT ((system_burst_1_downstream_read OR system_burst_1_downstream_write)))))) OR (((std_logic_vector'("00000000000000000000000000000001") AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT sdram_s1_waitrequest_from_sa)))) AND (std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(((system_burst_1_downstream_read OR system_burst_1_downstream_write)))))))))));
  --cascaded wait assignment, which is an e_assign
  system_burst_1_downstream_run <= r_0;
  --optimize select-logic by passing only those address bits which matter.
  internal_system_burst_1_downstream_address_to_slave <= system_burst_1_downstream_address;
  --system_burst_1_downstream_read_but_no_slave_selected assignment, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      system_burst_1_downstream_read_but_no_slave_selected <= std_logic'('0');
    elsif clk'event and clk = '1' then
      system_burst_1_downstream_read_but_no_slave_selected <= (system_burst_1_downstream_read AND system_burst_1_downstream_run) AND NOT system_burst_1_downstream_is_granted_some_slave;
    end if;

  end process;

  --some slave is getting selected, which is an e_mux
  system_burst_1_downstream_is_granted_some_slave <= system_burst_1_downstream_granted_sdram_s1;
  --latent slave read data valids which may be flushed, which is an e_mux
  pre_flush_system_burst_1_downstream_readdatavalid <= system_burst_1_downstream_read_data_valid_sdram_s1;
  --latent slave read data valid which is not flushed, which is an e_mux
  system_burst_1_downstream_readdatavalid <= system_burst_1_downstream_read_but_no_slave_selected OR pre_flush_system_burst_1_downstream_readdatavalid;
  --system_burst_1/downstream readdata mux, which is an e_mux
  system_burst_1_downstream_readdata <= sdram_s1_readdata_from_sa;
  --actual waitrequest port, which is an e_assign
  internal_system_burst_1_downstream_waitrequest <= NOT system_burst_1_downstream_run;
  --latent max counter, which is an e_register
  process (clk, reset_n)
  begin
    if reset_n = '0' then
      internal_system_burst_1_downstream_latency_counter <= std_logic'('0');
    elsif clk'event and clk = '1' then
      internal_system_burst_1_downstream_latency_counter <= p1_system_burst_1_downstream_latency_counter;
    end if;

  end process;

  --latency counter load mux, which is an e_mux
  p1_system_burst_1_downstream_latency_counter <= Vector_To_Std_Logic(A_WE_StdLogicVector((std_logic'(((system_burst_1_downstream_run AND system_burst_1_downstream_read))) = '1'), (std_logic_vector'("00000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(latency_load_value))), A_WE_StdLogicVector((std_logic'((internal_system_burst_1_downstream_latency_counter)) = '1'), ((std_logic_vector'("00000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(internal_system_burst_1_downstream_latency_counter))) - std_logic_vector'("000000000000000000000000000000001")), std_logic_vector'("000000000000000000000000000000000"))));
  --read latency load values, which is an e_mux
  latency_load_value <= std_logic'('0');
  --system_burst_1_downstream_reset_n assignment, which is an e_assign
  system_burst_1_downstream_reset_n <= reset_n;
  --vhdl renameroo for output signals
  system_burst_1_downstream_address_to_slave <= internal_system_burst_1_downstream_address_to_slave;
  --vhdl renameroo for output signals
  system_burst_1_downstream_latency_counter <= internal_system_burst_1_downstream_latency_counter;
  --vhdl renameroo for output signals
  system_burst_1_downstream_waitrequest <= internal_system_burst_1_downstream_waitrequest;
--synthesis translate_off
    --system_burst_1_downstream_address check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        system_burst_1_downstream_address_last_time <= std_logic_vector'("000000000000000000000000");
      elsif clk'event and clk = '1' then
        system_burst_1_downstream_address_last_time <= system_burst_1_downstream_address;
      end if;

    end process;

    --system_burst_1/downstream waited last time, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        active_and_waiting_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        active_and_waiting_last_time <= internal_system_burst_1_downstream_waitrequest AND ((system_burst_1_downstream_read OR system_burst_1_downstream_write));
      end if;

    end process;

    --system_burst_1_downstream_address matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line37 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((system_burst_1_downstream_address /= system_burst_1_downstream_address_last_time))))) = '1' then 
          write(write_line37, now);
          write(write_line37, string'(": "));
          write(write_line37, string'("system_burst_1_downstream_address did not heed wait!!!"));
          write(output, write_line37.all);
          deallocate (write_line37);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_1_downstream_burstcount check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        system_burst_1_downstream_burstcount_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        system_burst_1_downstream_burstcount_last_time <= system_burst_1_downstream_burstcount;
      end if;

    end process;

    --system_burst_1_downstream_burstcount matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line38 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((std_logic'(system_burst_1_downstream_burstcount) /= std_logic'(system_burst_1_downstream_burstcount_last_time)))))) = '1' then 
          write(write_line38, now);
          write(write_line38, string'(": "));
          write(write_line38, string'("system_burst_1_downstream_burstcount did not heed wait!!!"));
          write(output, write_line38.all);
          deallocate (write_line38);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_1_downstream_byteenable check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        system_burst_1_downstream_byteenable_last_time <= std_logic_vector'("00");
      elsif clk'event and clk = '1' then
        system_burst_1_downstream_byteenable_last_time <= system_burst_1_downstream_byteenable;
      end if;

    end process;

    --system_burst_1_downstream_byteenable matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line39 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((system_burst_1_downstream_byteenable /= system_burst_1_downstream_byteenable_last_time))))) = '1' then 
          write(write_line39, now);
          write(write_line39, string'(": "));
          write(write_line39, string'("system_burst_1_downstream_byteenable did not heed wait!!!"));
          write(output, write_line39.all);
          deallocate (write_line39);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_1_downstream_read check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        system_burst_1_downstream_read_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        system_burst_1_downstream_read_last_time <= system_burst_1_downstream_read;
      end if;

    end process;

    --system_burst_1_downstream_read matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line40 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((std_logic'(system_burst_1_downstream_read) /= std_logic'(system_burst_1_downstream_read_last_time)))))) = '1' then 
          write(write_line40, now);
          write(write_line40, string'(": "));
          write(write_line40, string'("system_burst_1_downstream_read did not heed wait!!!"));
          write(output, write_line40.all);
          deallocate (write_line40);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_1_downstream_write check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        system_burst_1_downstream_write_last_time <= std_logic'('0');
      elsif clk'event and clk = '1' then
        system_burst_1_downstream_write_last_time <= system_burst_1_downstream_write;
      end if;

    end process;

    --system_burst_1_downstream_write matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line41 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'((active_and_waiting_last_time AND to_std_logic(((std_logic'(system_burst_1_downstream_write) /= std_logic'(system_burst_1_downstream_write_last_time)))))) = '1' then 
          write(write_line41, now);
          write(write_line41, string'(": "));
          write(write_line41, string'("system_burst_1_downstream_write did not heed wait!!!"));
          write(output, write_line41.all);
          deallocate (write_line41);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

    --system_burst_1_downstream_writedata check against wait, which is an e_register
    process (clk, reset_n)
    begin
      if reset_n = '0' then
        system_burst_1_downstream_writedata_last_time <= std_logic_vector'("0000000000000000");
      elsif clk'event and clk = '1' then
        system_burst_1_downstream_writedata_last_time <= system_burst_1_downstream_writedata;
      end if;

    end process;

    --system_burst_1_downstream_writedata matches last port_name, which is an e_process
    process (clk)
    VARIABLE write_line42 : line;
    begin
      if clk'event and clk = '1' then
        if std_logic'(((active_and_waiting_last_time AND to_std_logic(((system_burst_1_downstream_writedata /= system_burst_1_downstream_writedata_last_time)))) AND system_burst_1_downstream_write)) = '1' then 
          write(write_line42, now);
          write(write_line42, string'(": "));
          write(write_line42, string'("system_burst_1_downstream_writedata did not heed wait!!!"));
          write(output, write_line42.all);
          deallocate (write_line42);
          assert false report "VHDL STOP" severity failure;
        end if;
      end if;

    end process;

--synthesis translate_on

end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity system_reset_clk_master_read_domain_synch_module is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal data_in : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;

              -- outputs:
                 signal data_out : OUT STD_LOGIC
              );
end entity system_reset_clk_master_read_domain_synch_module;


architecture europa of system_reset_clk_master_read_domain_synch_module is
                signal data_in_d1 :  STD_LOGIC;
attribute ALTERA_ATTRIBUTE : string;
attribute ALTERA_ATTRIBUTE of data_in_d1 : signal is "{-from ""*""} CUT=ON ; PRESERVE_REGISTER=ON ; SUPPRESS_DA_RULE_INTERNAL=R101";
attribute ALTERA_ATTRIBUTE of data_out : signal is "PRESERVE_REGISTER=ON ; SUPPRESS_DA_RULE_INTERNAL=R101";

begin

  process (clk, reset_n)
  begin
    if reset_n = '0' then
      data_in_d1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      data_in_d1 <= data_in;
    end if;

  end process;

  process (clk, reset_n)
  begin
    if reset_n = '0' then
      data_out <= std_logic'('0');
    elsif clk'event and clk = '1' then
      data_out <= data_in_d1;
    end if;

  end process;


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity system_reset_clk_master_write_domain_synch_module is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal data_in : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;

              -- outputs:
                 signal data_out : OUT STD_LOGIC
              );
end entity system_reset_clk_master_write_domain_synch_module;


architecture europa of system_reset_clk_master_write_domain_synch_module is
                signal data_in_d1 :  STD_LOGIC;
attribute ALTERA_ATTRIBUTE : string;
attribute ALTERA_ATTRIBUTE of data_in_d1 : signal is "{-from ""*""} CUT=ON ; PRESERVE_REGISTER=ON ; SUPPRESS_DA_RULE_INTERNAL=R101";
attribute ALTERA_ATTRIBUTE of data_out : signal is "PRESERVE_REGISTER=ON ; SUPPRESS_DA_RULE_INTERNAL=R101";

begin

  process (clk, reset_n)
  begin
    if reset_n = '0' then
      data_in_d1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      data_in_d1 <= data_in;
    end if;

  end process;

  process (clk, reset_n)
  begin
    if reset_n = '0' then
      data_out <= std_logic'('0');
    elsif clk'event and clk = '1' then
      data_out <= data_in_d1;
    end if;

  end process;


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity system_reset_clk_sdram_domain_synch_module is 
        port (
              -- inputs:
                 signal clk : IN STD_LOGIC;
                 signal data_in : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;

              -- outputs:
                 signal data_out : OUT STD_LOGIC
              );
end entity system_reset_clk_sdram_domain_synch_module;


architecture europa of system_reset_clk_sdram_domain_synch_module is
                signal data_in_d1 :  STD_LOGIC;
attribute ALTERA_ATTRIBUTE : string;
attribute ALTERA_ATTRIBUTE of data_in_d1 : signal is "{-from ""*""} CUT=ON ; PRESERVE_REGISTER=ON ; SUPPRESS_DA_RULE_INTERNAL=R101";
attribute ALTERA_ATTRIBUTE of data_out : signal is "PRESERVE_REGISTER=ON ; SUPPRESS_DA_RULE_INTERNAL=R101";

begin

  process (clk, reset_n)
  begin
    if reset_n = '0' then
      data_in_d1 <= std_logic'('0');
    elsif clk'event and clk = '1' then
      data_in_d1 <= data_in;
    end if;

  end process;

  process (clk, reset_n)
  begin
    if reset_n = '0' then
      data_out <= std_logic'('0');
    elsif clk'event and clk = '1' then
      data_out <= data_in_d1;
    end if;

  end process;


end europa;



-- turn off superfluous VHDL processor warnings 
-- altera message_level Level1 
-- altera message_off 10034 10035 10036 10037 10230 10240 10030 

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity system is 
        port (
              -- 1) global signals:
                 signal clk_master_read : IN STD_LOGIC;
                 signal clk_master_write : IN STD_LOGIC;
                 signal clk_sdram : IN STD_LOGIC;
                 signal reset_n : IN STD_LOGIC;

              -- the_master_read
                 signal control_done_from_the_master_read : OUT STD_LOGIC;
                 signal control_early_done_from_the_master_read : OUT STD_LOGIC;
                 signal control_fixed_location_to_the_master_read : IN STD_LOGIC;
                 signal control_go_to_the_master_read : IN STD_LOGIC;
                 signal control_read_base_to_the_master_read : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal control_read_length_to_the_master_read : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal user_buffer_output_data_from_the_master_read : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal user_data_available_from_the_master_read : OUT STD_LOGIC;
                 signal user_read_buffer_to_the_master_read : IN STD_LOGIC;

              -- the_master_write
                 signal control_done_from_the_master_write : OUT STD_LOGIC;
                 signal control_fixed_location_to_the_master_write : IN STD_LOGIC;
                 signal control_go_to_the_master_write : IN STD_LOGIC;
                 signal control_write_base_to_the_master_write : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal control_write_length_to_the_master_write : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                 signal user_buffer_full_from_the_master_write : OUT STD_LOGIC;
                 signal user_buffer_input_data_to_the_master_write : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                 signal user_write_buffer_to_the_master_write : IN STD_LOGIC;

              -- the_sdram
                 signal zs_addr_from_the_sdram : OUT STD_LOGIC_VECTOR (11 DOWNTO 0);
                 signal zs_ba_from_the_sdram : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal zs_cas_n_from_the_sdram : OUT STD_LOGIC;
                 signal zs_cke_from_the_sdram : OUT STD_LOGIC;
                 signal zs_cs_n_from_the_sdram : OUT STD_LOGIC;
                 signal zs_dq_to_and_from_the_sdram : INOUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                 signal zs_dqm_from_the_sdram : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                 signal zs_ras_n_from_the_sdram : OUT STD_LOGIC;
                 signal zs_we_n_from_the_sdram : OUT STD_LOGIC
              );
end entity system;


architecture europa of system is
component clock_crossing_read_s1_arbitrator is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal clock_crossing_read_s1_endofpacket : IN STD_LOGIC;
                    signal clock_crossing_read_s1_readdata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal clock_crossing_read_s1_readdatavalid : IN STD_LOGIC;
                    signal clock_crossing_read_s1_waitrequest : IN STD_LOGIC;
                    signal master_read_avalon_master_address_to_slave : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal master_read_avalon_master_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_read_avalon_master_read : IN STD_LOGIC;
                    signal master_read_latency_counter : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;

                 -- outputs:
                    signal clock_crossing_read_s1_address : OUT STD_LOGIC_VECTOR (21 DOWNTO 0);
                    signal clock_crossing_read_s1_burstcount : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal clock_crossing_read_s1_byteenable : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal clock_crossing_read_s1_endofpacket_from_sa : OUT STD_LOGIC;
                    signal clock_crossing_read_s1_nativeaddress : OUT STD_LOGIC_VECTOR (21 DOWNTO 0);
                    signal clock_crossing_read_s1_read : OUT STD_LOGIC;
                    signal clock_crossing_read_s1_readdata_from_sa : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal clock_crossing_read_s1_reset_n : OUT STD_LOGIC;
                    signal clock_crossing_read_s1_waitrequest_from_sa : OUT STD_LOGIC;
                    signal clock_crossing_read_s1_write : OUT STD_LOGIC;
                    signal d1_clock_crossing_read_s1_end_xfer : OUT STD_LOGIC;
                    signal master_read_granted_clock_crossing_read_s1 : OUT STD_LOGIC;
                    signal master_read_qualified_request_clock_crossing_read_s1 : OUT STD_LOGIC;
                    signal master_read_read_data_valid_clock_crossing_read_s1 : OUT STD_LOGIC;
                    signal master_read_read_data_valid_clock_crossing_read_s1_shift_register : OUT STD_LOGIC;
                    signal master_read_requests_clock_crossing_read_s1 : OUT STD_LOGIC
                 );
end component clock_crossing_read_s1_arbitrator;

component clock_crossing_read_m1_arbitrator is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal clock_crossing_read_m1_address : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal clock_crossing_read_m1_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal clock_crossing_read_m1_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal clock_crossing_read_m1_byteenable_system_burst_1_upstream : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal clock_crossing_read_m1_granted_system_burst_1_upstream : IN STD_LOGIC;
                    signal clock_crossing_read_m1_qualified_request_system_burst_1_upstream : IN STD_LOGIC;
                    signal clock_crossing_read_m1_read : IN STD_LOGIC;
                    signal clock_crossing_read_m1_read_data_valid_system_burst_1_upstream : IN STD_LOGIC;
                    signal clock_crossing_read_m1_read_data_valid_system_burst_1_upstream_shift_register : IN STD_LOGIC;
                    signal clock_crossing_read_m1_requests_system_burst_1_upstream : IN STD_LOGIC;
                    signal clock_crossing_read_m1_write : IN STD_LOGIC;
                    signal clock_crossing_read_m1_writedata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal d1_system_burst_1_upstream_end_xfer : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal system_burst_1_upstream_readdata_from_sa : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal system_burst_1_upstream_waitrequest_from_sa : IN STD_LOGIC;

                 -- outputs:
                    signal clock_crossing_read_m1_address_to_slave : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal clock_crossing_read_m1_dbs_address : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal clock_crossing_read_m1_dbs_write_16 : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal clock_crossing_read_m1_latency_counter : OUT STD_LOGIC;
                    signal clock_crossing_read_m1_readdata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal clock_crossing_read_m1_readdatavalid : OUT STD_LOGIC;
                    signal clock_crossing_read_m1_reset_n : OUT STD_LOGIC;
                    signal clock_crossing_read_m1_waitrequest : OUT STD_LOGIC
                 );
end component clock_crossing_read_m1_arbitrator;

component clock_crossing_read is 
           port (
                 -- inputs:
                    signal master_clk : IN STD_LOGIC;
                    signal master_endofpacket : IN STD_LOGIC;
                    signal master_readdata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal master_readdatavalid : IN STD_LOGIC;
                    signal master_reset_n : IN STD_LOGIC;
                    signal master_waitrequest : IN STD_LOGIC;
                    signal slave_address : IN STD_LOGIC_VECTOR (21 DOWNTO 0);
                    signal slave_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal slave_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal slave_clk : IN STD_LOGIC;
                    signal slave_nativeaddress : IN STD_LOGIC_VECTOR (21 DOWNTO 0);
                    signal slave_read : IN STD_LOGIC;
                    signal slave_reset_n : IN STD_LOGIC;
                    signal slave_write : IN STD_LOGIC;
                    signal slave_writedata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);

                 -- outputs:
                    signal master_address : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal master_burstcount : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_byteenable : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_nativeaddress : OUT STD_LOGIC_VECTOR (21 DOWNTO 0);
                    signal master_read : OUT STD_LOGIC;
                    signal master_write : OUT STD_LOGIC;
                    signal master_writedata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal slave_endofpacket : OUT STD_LOGIC;
                    signal slave_readdata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal slave_readdatavalid : OUT STD_LOGIC;
                    signal slave_waitrequest : OUT STD_LOGIC
                 );
end component clock_crossing_read;

component clock_crossing_read_bridge_arbitrator is 
end component clock_crossing_read_bridge_arbitrator;

component clock_crossing_write_s1_arbitrator is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal clock_crossing_write_s1_endofpacket : IN STD_LOGIC;
                    signal clock_crossing_write_s1_readdata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal clock_crossing_write_s1_readdatavalid : IN STD_LOGIC;
                    signal clock_crossing_write_s1_waitrequest : IN STD_LOGIC;
                    signal master_write_avalon_master_address_to_slave : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal master_write_avalon_master_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_write_avalon_master_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_write_avalon_master_write : IN STD_LOGIC;
                    signal master_write_avalon_master_writedata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal reset_n : IN STD_LOGIC;

                 -- outputs:
                    signal clock_crossing_write_s1_address : OUT STD_LOGIC_VECTOR (21 DOWNTO 0);
                    signal clock_crossing_write_s1_burstcount : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal clock_crossing_write_s1_byteenable : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal clock_crossing_write_s1_endofpacket_from_sa : OUT STD_LOGIC;
                    signal clock_crossing_write_s1_nativeaddress : OUT STD_LOGIC_VECTOR (21 DOWNTO 0);
                    signal clock_crossing_write_s1_read : OUT STD_LOGIC;
                    signal clock_crossing_write_s1_readdata_from_sa : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal clock_crossing_write_s1_readdatavalid_from_sa : OUT STD_LOGIC;
                    signal clock_crossing_write_s1_reset_n : OUT STD_LOGIC;
                    signal clock_crossing_write_s1_waitrequest_from_sa : OUT STD_LOGIC;
                    signal clock_crossing_write_s1_write : OUT STD_LOGIC;
                    signal clock_crossing_write_s1_writedata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal d1_clock_crossing_write_s1_end_xfer : OUT STD_LOGIC;
                    signal master_write_granted_clock_crossing_write_s1 : OUT STD_LOGIC;
                    signal master_write_qualified_request_clock_crossing_write_s1 : OUT STD_LOGIC;
                    signal master_write_requests_clock_crossing_write_s1 : OUT STD_LOGIC
                 );
end component clock_crossing_write_s1_arbitrator;

component clock_crossing_write_m1_arbitrator is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal clock_crossing_write_m1_address : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal clock_crossing_write_m1_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal clock_crossing_write_m1_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal clock_crossing_write_m1_byteenable_system_burst_0_upstream : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal clock_crossing_write_m1_granted_system_burst_0_upstream : IN STD_LOGIC;
                    signal clock_crossing_write_m1_qualified_request_system_burst_0_upstream : IN STD_LOGIC;
                    signal clock_crossing_write_m1_read : IN STD_LOGIC;
                    signal clock_crossing_write_m1_read_data_valid_system_burst_0_upstream : IN STD_LOGIC;
                    signal clock_crossing_write_m1_read_data_valid_system_burst_0_upstream_shift_register : IN STD_LOGIC;
                    signal clock_crossing_write_m1_requests_system_burst_0_upstream : IN STD_LOGIC;
                    signal clock_crossing_write_m1_write : IN STD_LOGIC;
                    signal clock_crossing_write_m1_writedata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal d1_system_burst_0_upstream_end_xfer : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal system_burst_0_upstream_readdata_from_sa : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal system_burst_0_upstream_waitrequest_from_sa : IN STD_LOGIC;

                 -- outputs:
                    signal clock_crossing_write_m1_address_to_slave : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal clock_crossing_write_m1_dbs_address : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal clock_crossing_write_m1_dbs_write_16 : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal clock_crossing_write_m1_latency_counter : OUT STD_LOGIC;
                    signal clock_crossing_write_m1_readdata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal clock_crossing_write_m1_readdatavalid : OUT STD_LOGIC;
                    signal clock_crossing_write_m1_reset_n : OUT STD_LOGIC;
                    signal clock_crossing_write_m1_waitrequest : OUT STD_LOGIC
                 );
end component clock_crossing_write_m1_arbitrator;

component clock_crossing_write is 
           port (
                 -- inputs:
                    signal master_clk : IN STD_LOGIC;
                    signal master_endofpacket : IN STD_LOGIC;
                    signal master_readdata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal master_readdatavalid : IN STD_LOGIC;
                    signal master_reset_n : IN STD_LOGIC;
                    signal master_waitrequest : IN STD_LOGIC;
                    signal slave_address : IN STD_LOGIC_VECTOR (21 DOWNTO 0);
                    signal slave_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal slave_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal slave_clk : IN STD_LOGIC;
                    signal slave_nativeaddress : IN STD_LOGIC_VECTOR (21 DOWNTO 0);
                    signal slave_read : IN STD_LOGIC;
                    signal slave_reset_n : IN STD_LOGIC;
                    signal slave_write : IN STD_LOGIC;
                    signal slave_writedata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);

                 -- outputs:
                    signal master_address : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal master_burstcount : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_byteenable : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_nativeaddress : OUT STD_LOGIC_VECTOR (21 DOWNTO 0);
                    signal master_read : OUT STD_LOGIC;
                    signal master_write : OUT STD_LOGIC;
                    signal master_writedata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal slave_endofpacket : OUT STD_LOGIC;
                    signal slave_readdata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal slave_readdatavalid : OUT STD_LOGIC;
                    signal slave_waitrequest : OUT STD_LOGIC
                 );
end component clock_crossing_write;

component clock_crossing_write_bridge_arbitrator is 
end component clock_crossing_write_bridge_arbitrator;

component master_read_avalon_master_arbitrator is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal clock_crossing_read_s1_readdata_from_sa : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal clock_crossing_read_s1_waitrequest_from_sa : IN STD_LOGIC;
                    signal d1_clock_crossing_read_s1_end_xfer : IN STD_LOGIC;
                    signal master_read_avalon_master_address : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal master_read_avalon_master_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_read_avalon_master_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_read_avalon_master_read : IN STD_LOGIC;
                    signal master_read_granted_clock_crossing_read_s1 : IN STD_LOGIC;
                    signal master_read_qualified_request_clock_crossing_read_s1 : IN STD_LOGIC;
                    signal master_read_read_data_valid_clock_crossing_read_s1 : IN STD_LOGIC;
                    signal master_read_read_data_valid_clock_crossing_read_s1_shift_register : IN STD_LOGIC;
                    signal master_read_requests_clock_crossing_read_s1 : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;

                 -- outputs:
                    signal master_read_avalon_master_address_to_slave : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal master_read_avalon_master_readdata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal master_read_avalon_master_readdatavalid : OUT STD_LOGIC;
                    signal master_read_avalon_master_reset : OUT STD_LOGIC;
                    signal master_read_avalon_master_waitrequest : OUT STD_LOGIC;
                    signal master_read_latency_counter : OUT STD_LOGIC
                 );
end component master_read_avalon_master_arbitrator;

component master_read is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal control_fixed_location : IN STD_LOGIC;
                    signal control_go : IN STD_LOGIC;
                    signal control_read_base : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal control_read_length : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal master_readdata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal master_readdatavalid : IN STD_LOGIC;
                    signal master_waitrequest : IN STD_LOGIC;
                    signal reset : IN STD_LOGIC;
                    signal user_read_buffer : IN STD_LOGIC;

                 -- outputs:
                    signal control_done : OUT STD_LOGIC;
                    signal control_early_done : OUT STD_LOGIC;
                    signal master_address : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal master_burstcount : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_byteenable : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_read : OUT STD_LOGIC;
                    signal user_buffer_output_data : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal user_data_available : OUT STD_LOGIC
                 );
end component master_read;

component master_write_avalon_master_arbitrator is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal clock_crossing_write_s1_waitrequest_from_sa : IN STD_LOGIC;
                    signal d1_clock_crossing_write_s1_end_xfer : IN STD_LOGIC;
                    signal master_write_avalon_master_address : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal master_write_avalon_master_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_write_avalon_master_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_write_avalon_master_write : IN STD_LOGIC;
                    signal master_write_avalon_master_writedata : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal master_write_granted_clock_crossing_write_s1 : IN STD_LOGIC;
                    signal master_write_qualified_request_clock_crossing_write_s1 : IN STD_LOGIC;
                    signal master_write_requests_clock_crossing_write_s1 : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;

                 -- outputs:
                    signal master_write_avalon_master_address_to_slave : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal master_write_avalon_master_reset : OUT STD_LOGIC;
                    signal master_write_avalon_master_waitrequest : OUT STD_LOGIC
                 );
end component master_write_avalon_master_arbitrator;

component master_write is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal control_fixed_location : IN STD_LOGIC;
                    signal control_go : IN STD_LOGIC;
                    signal control_write_base : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal control_write_length : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal master_waitrequest : IN STD_LOGIC;
                    signal reset : IN STD_LOGIC;
                    signal user_buffer_input_data : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal user_write_buffer : IN STD_LOGIC;

                 -- outputs:
                    signal control_done : OUT STD_LOGIC;
                    signal master_address : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal master_burstcount : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_byteenable : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal master_write : OUT STD_LOGIC;
                    signal master_writedata : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal user_buffer_full : OUT STD_LOGIC
                 );
end component master_write;

component sdram_s1_arbitrator is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal sdram_s1_readdata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal sdram_s1_readdatavalid : IN STD_LOGIC;
                    signal sdram_s1_waitrequest : IN STD_LOGIC;
                    signal system_burst_0_downstream_address_to_slave : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal system_burst_0_downstream_arbitrationshare : IN STD_LOGIC_VECTOR (4 DOWNTO 0);
                    signal system_burst_0_downstream_burstcount : IN STD_LOGIC;
                    signal system_burst_0_downstream_byteenable : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal system_burst_0_downstream_latency_counter : IN STD_LOGIC;
                    signal system_burst_0_downstream_read : IN STD_LOGIC;
                    signal system_burst_0_downstream_write : IN STD_LOGIC;
                    signal system_burst_0_downstream_writedata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal system_burst_1_downstream_address_to_slave : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal system_burst_1_downstream_arbitrationshare : IN STD_LOGIC_VECTOR (4 DOWNTO 0);
                    signal system_burst_1_downstream_burstcount : IN STD_LOGIC;
                    signal system_burst_1_downstream_byteenable : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal system_burst_1_downstream_latency_counter : IN STD_LOGIC;
                    signal system_burst_1_downstream_read : IN STD_LOGIC;
                    signal system_burst_1_downstream_write : IN STD_LOGIC;
                    signal system_burst_1_downstream_writedata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);

                 -- outputs:
                    signal d1_sdram_s1_end_xfer : OUT STD_LOGIC;
                    signal sdram_s1_address : OUT STD_LOGIC_VECTOR (22 DOWNTO 0);
                    signal sdram_s1_byteenable_n : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal sdram_s1_chipselect : OUT STD_LOGIC;
                    signal sdram_s1_read_n : OUT STD_LOGIC;
                    signal sdram_s1_readdata_from_sa : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal sdram_s1_reset_n : OUT STD_LOGIC;
                    signal sdram_s1_waitrequest_from_sa : OUT STD_LOGIC;
                    signal sdram_s1_write_n : OUT STD_LOGIC;
                    signal sdram_s1_writedata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal system_burst_0_downstream_granted_sdram_s1 : OUT STD_LOGIC;
                    signal system_burst_0_downstream_qualified_request_sdram_s1 : OUT STD_LOGIC;
                    signal system_burst_0_downstream_read_data_valid_sdram_s1 : OUT STD_LOGIC;
                    signal system_burst_0_downstream_read_data_valid_sdram_s1_shift_register : OUT STD_LOGIC;
                    signal system_burst_0_downstream_requests_sdram_s1 : OUT STD_LOGIC;
                    signal system_burst_1_downstream_granted_sdram_s1 : OUT STD_LOGIC;
                    signal system_burst_1_downstream_qualified_request_sdram_s1 : OUT STD_LOGIC;
                    signal system_burst_1_downstream_read_data_valid_sdram_s1 : OUT STD_LOGIC;
                    signal system_burst_1_downstream_read_data_valid_sdram_s1_shift_register : OUT STD_LOGIC;
                    signal system_burst_1_downstream_requests_sdram_s1 : OUT STD_LOGIC
                 );
end component sdram_s1_arbitrator;

component sdram is 
           port (
                 -- inputs:
                    signal az_addr : IN STD_LOGIC_VECTOR (22 DOWNTO 0);
                    signal az_be_n : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal az_cs : IN STD_LOGIC;
                    signal az_data : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal az_rd_n : IN STD_LOGIC;
                    signal az_wr_n : IN STD_LOGIC;
                    signal clk : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;

                 -- outputs:
                    signal za_data : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal za_valid : OUT STD_LOGIC;
                    signal za_waitrequest : OUT STD_LOGIC;
                    signal zs_addr : OUT STD_LOGIC_VECTOR (11 DOWNTO 0);
                    signal zs_ba : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal zs_cas_n : OUT STD_LOGIC;
                    signal zs_cke : OUT STD_LOGIC;
                    signal zs_cs_n : OUT STD_LOGIC;
                    signal zs_dq : INOUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal zs_dqm : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal zs_ras_n : OUT STD_LOGIC;
                    signal zs_we_n : OUT STD_LOGIC
                 );
end component sdram;

component system_burst_0_upstream_arbitrator is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal clock_crossing_write_m1_address_to_slave : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal clock_crossing_write_m1_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal clock_crossing_write_m1_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal clock_crossing_write_m1_dbs_address : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal clock_crossing_write_m1_dbs_write_16 : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal clock_crossing_write_m1_latency_counter : IN STD_LOGIC;
                    signal clock_crossing_write_m1_read : IN STD_LOGIC;
                    signal clock_crossing_write_m1_write : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal system_burst_0_upstream_readdata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal system_burst_0_upstream_readdatavalid : IN STD_LOGIC;
                    signal system_burst_0_upstream_waitrequest : IN STD_LOGIC;

                 -- outputs:
                    signal clock_crossing_write_m1_byteenable_system_burst_0_upstream : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal clock_crossing_write_m1_granted_system_burst_0_upstream : OUT STD_LOGIC;
                    signal clock_crossing_write_m1_qualified_request_system_burst_0_upstream : OUT STD_LOGIC;
                    signal clock_crossing_write_m1_read_data_valid_system_burst_0_upstream : OUT STD_LOGIC;
                    signal clock_crossing_write_m1_read_data_valid_system_burst_0_upstream_shift_register : OUT STD_LOGIC;
                    signal clock_crossing_write_m1_requests_system_burst_0_upstream : OUT STD_LOGIC;
                    signal d1_system_burst_0_upstream_end_xfer : OUT STD_LOGIC;
                    signal system_burst_0_upstream_address : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal system_burst_0_upstream_burstcount : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal system_burst_0_upstream_byteaddress : OUT STD_LOGIC_VECTOR (24 DOWNTO 0);
                    signal system_burst_0_upstream_byteenable : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal system_burst_0_upstream_debugaccess : OUT STD_LOGIC;
                    signal system_burst_0_upstream_read : OUT STD_LOGIC;
                    signal system_burst_0_upstream_readdata_from_sa : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal system_burst_0_upstream_waitrequest_from_sa : OUT STD_LOGIC;
                    signal system_burst_0_upstream_write : OUT STD_LOGIC;
                    signal system_burst_0_upstream_writedata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
                 );
end component system_burst_0_upstream_arbitrator;

component system_burst_0_downstream_arbitrator is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal d1_sdram_s1_end_xfer : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal sdram_s1_readdata_from_sa : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal sdram_s1_waitrequest_from_sa : IN STD_LOGIC;
                    signal system_burst_0_downstream_address : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal system_burst_0_downstream_burstcount : IN STD_LOGIC;
                    signal system_burst_0_downstream_byteenable : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal system_burst_0_downstream_granted_sdram_s1 : IN STD_LOGIC;
                    signal system_burst_0_downstream_qualified_request_sdram_s1 : IN STD_LOGIC;
                    signal system_burst_0_downstream_read : IN STD_LOGIC;
                    signal system_burst_0_downstream_read_data_valid_sdram_s1 : IN STD_LOGIC;
                    signal system_burst_0_downstream_read_data_valid_sdram_s1_shift_register : IN STD_LOGIC;
                    signal system_burst_0_downstream_requests_sdram_s1 : IN STD_LOGIC;
                    signal system_burst_0_downstream_write : IN STD_LOGIC;
                    signal system_burst_0_downstream_writedata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);

                 -- outputs:
                    signal system_burst_0_downstream_address_to_slave : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal system_burst_0_downstream_latency_counter : OUT STD_LOGIC;
                    signal system_burst_0_downstream_readdata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal system_burst_0_downstream_readdatavalid : OUT STD_LOGIC;
                    signal system_burst_0_downstream_reset_n : OUT STD_LOGIC;
                    signal system_burst_0_downstream_waitrequest : OUT STD_LOGIC
                 );
end component system_burst_0_downstream_arbitrator;

component system_burst_0 is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal downstream_readdata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal downstream_readdatavalid : IN STD_LOGIC;
                    signal downstream_waitrequest : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal upstream_address : IN STD_LOGIC_VECTOR (24 DOWNTO 0);
                    signal upstream_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal upstream_byteenable : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal upstream_debugaccess : IN STD_LOGIC;
                    signal upstream_nativeaddress : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal upstream_read : IN STD_LOGIC;
                    signal upstream_write : IN STD_LOGIC;
                    signal upstream_writedata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);

                 -- outputs:
                    signal downstream_address : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal downstream_arbitrationshare : OUT STD_LOGIC_VECTOR (4 DOWNTO 0);
                    signal downstream_burstcount : OUT STD_LOGIC;
                    signal downstream_byteenable : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal downstream_debugaccess : OUT STD_LOGIC;
                    signal downstream_nativeaddress : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal downstream_read : OUT STD_LOGIC;
                    signal downstream_write : OUT STD_LOGIC;
                    signal downstream_writedata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal upstream_readdata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal upstream_readdatavalid : OUT STD_LOGIC;
                    signal upstream_waitrequest : OUT STD_LOGIC
                 );
end component system_burst_0;

component system_burst_1_upstream_arbitrator is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal clock_crossing_read_m1_address_to_slave : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal clock_crossing_read_m1_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal clock_crossing_read_m1_byteenable : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal clock_crossing_read_m1_dbs_address : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal clock_crossing_read_m1_dbs_write_16 : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal clock_crossing_read_m1_latency_counter : IN STD_LOGIC;
                    signal clock_crossing_read_m1_read : IN STD_LOGIC;
                    signal clock_crossing_read_m1_write : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal system_burst_1_upstream_readdata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal system_burst_1_upstream_readdatavalid : IN STD_LOGIC;
                    signal system_burst_1_upstream_waitrequest : IN STD_LOGIC;

                 -- outputs:
                    signal clock_crossing_read_m1_byteenable_system_burst_1_upstream : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal clock_crossing_read_m1_granted_system_burst_1_upstream : OUT STD_LOGIC;
                    signal clock_crossing_read_m1_qualified_request_system_burst_1_upstream : OUT STD_LOGIC;
                    signal clock_crossing_read_m1_read_data_valid_system_burst_1_upstream : OUT STD_LOGIC;
                    signal clock_crossing_read_m1_read_data_valid_system_burst_1_upstream_shift_register : OUT STD_LOGIC;
                    signal clock_crossing_read_m1_requests_system_burst_1_upstream : OUT STD_LOGIC;
                    signal d1_system_burst_1_upstream_end_xfer : OUT STD_LOGIC;
                    signal system_burst_1_upstream_address : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal system_burst_1_upstream_burstcount : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal system_burst_1_upstream_byteaddress : OUT STD_LOGIC_VECTOR (24 DOWNTO 0);
                    signal system_burst_1_upstream_byteenable : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal system_burst_1_upstream_debugaccess : OUT STD_LOGIC;
                    signal system_burst_1_upstream_read : OUT STD_LOGIC;
                    signal system_burst_1_upstream_readdata_from_sa : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal system_burst_1_upstream_waitrequest_from_sa : OUT STD_LOGIC;
                    signal system_burst_1_upstream_write : OUT STD_LOGIC;
                    signal system_burst_1_upstream_writedata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
                 );
end component system_burst_1_upstream_arbitrator;

component system_burst_1_downstream_arbitrator is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal d1_sdram_s1_end_xfer : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal sdram_s1_readdata_from_sa : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal sdram_s1_waitrequest_from_sa : IN STD_LOGIC;
                    signal system_burst_1_downstream_address : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal system_burst_1_downstream_burstcount : IN STD_LOGIC;
                    signal system_burst_1_downstream_byteenable : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal system_burst_1_downstream_granted_sdram_s1 : IN STD_LOGIC;
                    signal system_burst_1_downstream_qualified_request_sdram_s1 : IN STD_LOGIC;
                    signal system_burst_1_downstream_read : IN STD_LOGIC;
                    signal system_burst_1_downstream_read_data_valid_sdram_s1 : IN STD_LOGIC;
                    signal system_burst_1_downstream_read_data_valid_sdram_s1_shift_register : IN STD_LOGIC;
                    signal system_burst_1_downstream_requests_sdram_s1 : IN STD_LOGIC;
                    signal system_burst_1_downstream_write : IN STD_LOGIC;
                    signal system_burst_1_downstream_writedata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);

                 -- outputs:
                    signal system_burst_1_downstream_address_to_slave : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal system_burst_1_downstream_latency_counter : OUT STD_LOGIC;
                    signal system_burst_1_downstream_readdata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal system_burst_1_downstream_readdatavalid : OUT STD_LOGIC;
                    signal system_burst_1_downstream_reset_n : OUT STD_LOGIC;
                    signal system_burst_1_downstream_waitrequest : OUT STD_LOGIC
                 );
end component system_burst_1_downstream_arbitrator;

component system_burst_1 is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal downstream_readdata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal downstream_readdatavalid : IN STD_LOGIC;
                    signal downstream_waitrequest : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;
                    signal upstream_address : IN STD_LOGIC_VECTOR (24 DOWNTO 0);
                    signal upstream_burstcount : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
                    signal upstream_byteenable : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal upstream_debugaccess : IN STD_LOGIC;
                    signal upstream_nativeaddress : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal upstream_read : IN STD_LOGIC;
                    signal upstream_write : IN STD_LOGIC;
                    signal upstream_writedata : IN STD_LOGIC_VECTOR (15 DOWNTO 0);

                 -- outputs:
                    signal downstream_address : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal downstream_arbitrationshare : OUT STD_LOGIC_VECTOR (4 DOWNTO 0);
                    signal downstream_burstcount : OUT STD_LOGIC;
                    signal downstream_byteenable : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal downstream_debugaccess : OUT STD_LOGIC;
                    signal downstream_nativeaddress : OUT STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal downstream_read : OUT STD_LOGIC;
                    signal downstream_write : OUT STD_LOGIC;
                    signal downstream_writedata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal upstream_readdata : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal upstream_readdatavalid : OUT STD_LOGIC;
                    signal upstream_waitrequest : OUT STD_LOGIC
                 );
end component system_burst_1;

component system_reset_clk_master_read_domain_synch_module is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal data_in : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;

                 -- outputs:
                    signal data_out : OUT STD_LOGIC
                 );
end component system_reset_clk_master_read_domain_synch_module;

component system_reset_clk_master_write_domain_synch_module is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal data_in : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;

                 -- outputs:
                    signal data_out : OUT STD_LOGIC
                 );
end component system_reset_clk_master_write_domain_synch_module;

component system_reset_clk_sdram_domain_synch_module is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal data_in : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;

                 -- outputs:
                    signal data_out : OUT STD_LOGIC
                 );
end component system_reset_clk_sdram_domain_synch_module;

                signal clk_master_read_reset_n :  STD_LOGIC;
                signal clk_master_write_reset_n :  STD_LOGIC;
                signal clk_sdram_reset_n :  STD_LOGIC;
                signal clock_crossing_read_m1_address :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal clock_crossing_read_m1_address_to_slave :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal clock_crossing_read_m1_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_m1_byteenable :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_m1_byteenable_system_burst_1_upstream :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_read_m1_dbs_address :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_read_m1_dbs_write_16 :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal clock_crossing_read_m1_endofpacket :  STD_LOGIC;
                signal clock_crossing_read_m1_granted_system_burst_1_upstream :  STD_LOGIC;
                signal clock_crossing_read_m1_latency_counter :  STD_LOGIC;
                signal clock_crossing_read_m1_nativeaddress :  STD_LOGIC_VECTOR (21 DOWNTO 0);
                signal clock_crossing_read_m1_qualified_request_system_burst_1_upstream :  STD_LOGIC;
                signal clock_crossing_read_m1_read :  STD_LOGIC;
                signal clock_crossing_read_m1_read_data_valid_system_burst_1_upstream :  STD_LOGIC;
                signal clock_crossing_read_m1_read_data_valid_system_burst_1_upstream_shift_register :  STD_LOGIC;
                signal clock_crossing_read_m1_readdata :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal clock_crossing_read_m1_readdatavalid :  STD_LOGIC;
                signal clock_crossing_read_m1_requests_system_burst_1_upstream :  STD_LOGIC;
                signal clock_crossing_read_m1_reset_n :  STD_LOGIC;
                signal clock_crossing_read_m1_waitrequest :  STD_LOGIC;
                signal clock_crossing_read_m1_write :  STD_LOGIC;
                signal clock_crossing_read_m1_writedata :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal clock_crossing_read_s1_address :  STD_LOGIC_VECTOR (21 DOWNTO 0);
                signal clock_crossing_read_s1_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_s1_byteenable :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_read_s1_endofpacket :  STD_LOGIC;
                signal clock_crossing_read_s1_endofpacket_from_sa :  STD_LOGIC;
                signal clock_crossing_read_s1_nativeaddress :  STD_LOGIC_VECTOR (21 DOWNTO 0);
                signal clock_crossing_read_s1_read :  STD_LOGIC;
                signal clock_crossing_read_s1_readdata :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal clock_crossing_read_s1_readdata_from_sa :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal clock_crossing_read_s1_readdatavalid :  STD_LOGIC;
                signal clock_crossing_read_s1_reset_n :  STD_LOGIC;
                signal clock_crossing_read_s1_waitrequest :  STD_LOGIC;
                signal clock_crossing_read_s1_waitrequest_from_sa :  STD_LOGIC;
                signal clock_crossing_read_s1_write :  STD_LOGIC;
                signal clock_crossing_read_s1_writedata :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal clock_crossing_write_m1_address :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal clock_crossing_write_m1_address_to_slave :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal clock_crossing_write_m1_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_write_m1_byteenable :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_write_m1_byteenable_system_burst_0_upstream :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_write_m1_dbs_address :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal clock_crossing_write_m1_dbs_write_16 :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal clock_crossing_write_m1_endofpacket :  STD_LOGIC;
                signal clock_crossing_write_m1_granted_system_burst_0_upstream :  STD_LOGIC;
                signal clock_crossing_write_m1_latency_counter :  STD_LOGIC;
                signal clock_crossing_write_m1_nativeaddress :  STD_LOGIC_VECTOR (21 DOWNTO 0);
                signal clock_crossing_write_m1_qualified_request_system_burst_0_upstream :  STD_LOGIC;
                signal clock_crossing_write_m1_read :  STD_LOGIC;
                signal clock_crossing_write_m1_read_data_valid_system_burst_0_upstream :  STD_LOGIC;
                signal clock_crossing_write_m1_read_data_valid_system_burst_0_upstream_shift_register :  STD_LOGIC;
                signal clock_crossing_write_m1_readdata :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal clock_crossing_write_m1_readdatavalid :  STD_LOGIC;
                signal clock_crossing_write_m1_requests_system_burst_0_upstream :  STD_LOGIC;
                signal clock_crossing_write_m1_reset_n :  STD_LOGIC;
                signal clock_crossing_write_m1_waitrequest :  STD_LOGIC;
                signal clock_crossing_write_m1_write :  STD_LOGIC;
                signal clock_crossing_write_m1_writedata :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal clock_crossing_write_s1_address :  STD_LOGIC_VECTOR (21 DOWNTO 0);
                signal clock_crossing_write_s1_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_write_s1_byteenable :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal clock_crossing_write_s1_endofpacket :  STD_LOGIC;
                signal clock_crossing_write_s1_endofpacket_from_sa :  STD_LOGIC;
                signal clock_crossing_write_s1_nativeaddress :  STD_LOGIC_VECTOR (21 DOWNTO 0);
                signal clock_crossing_write_s1_read :  STD_LOGIC;
                signal clock_crossing_write_s1_readdata :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal clock_crossing_write_s1_readdata_from_sa :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal clock_crossing_write_s1_readdatavalid :  STD_LOGIC;
                signal clock_crossing_write_s1_readdatavalid_from_sa :  STD_LOGIC;
                signal clock_crossing_write_s1_reset_n :  STD_LOGIC;
                signal clock_crossing_write_s1_waitrequest :  STD_LOGIC;
                signal clock_crossing_write_s1_waitrequest_from_sa :  STD_LOGIC;
                signal clock_crossing_write_s1_write :  STD_LOGIC;
                signal clock_crossing_write_s1_writedata :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal d1_clock_crossing_read_s1_end_xfer :  STD_LOGIC;
                signal d1_clock_crossing_write_s1_end_xfer :  STD_LOGIC;
                signal d1_sdram_s1_end_xfer :  STD_LOGIC;
                signal d1_system_burst_0_upstream_end_xfer :  STD_LOGIC;
                signal d1_system_burst_1_upstream_end_xfer :  STD_LOGIC;
                signal internal_control_done_from_the_master_read :  STD_LOGIC;
                signal internal_control_done_from_the_master_write :  STD_LOGIC;
                signal internal_control_early_done_from_the_master_read :  STD_LOGIC;
                signal internal_user_buffer_full_from_the_master_write :  STD_LOGIC;
                signal internal_user_buffer_output_data_from_the_master_read :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal internal_user_data_available_from_the_master_read :  STD_LOGIC;
                signal internal_zs_addr_from_the_sdram :  STD_LOGIC_VECTOR (11 DOWNTO 0);
                signal internal_zs_ba_from_the_sdram :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal internal_zs_cas_n_from_the_sdram :  STD_LOGIC;
                signal internal_zs_cke_from_the_sdram :  STD_LOGIC;
                signal internal_zs_cs_n_from_the_sdram :  STD_LOGIC;
                signal internal_zs_dqm_from_the_sdram :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal internal_zs_ras_n_from_the_sdram :  STD_LOGIC;
                signal internal_zs_we_n_from_the_sdram :  STD_LOGIC;
                signal master_read_avalon_master_address :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal master_read_avalon_master_address_to_slave :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal master_read_avalon_master_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal master_read_avalon_master_byteenable :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal master_read_avalon_master_read :  STD_LOGIC;
                signal master_read_avalon_master_readdata :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal master_read_avalon_master_readdatavalid :  STD_LOGIC;
                signal master_read_avalon_master_reset :  STD_LOGIC;
                signal master_read_avalon_master_waitrequest :  STD_LOGIC;
                signal master_read_granted_clock_crossing_read_s1 :  STD_LOGIC;
                signal master_read_latency_counter :  STD_LOGIC;
                signal master_read_qualified_request_clock_crossing_read_s1 :  STD_LOGIC;
                signal master_read_read_data_valid_clock_crossing_read_s1 :  STD_LOGIC;
                signal master_read_read_data_valid_clock_crossing_read_s1_shift_register :  STD_LOGIC;
                signal master_read_requests_clock_crossing_read_s1 :  STD_LOGIC;
                signal master_write_avalon_master_address :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal master_write_avalon_master_address_to_slave :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal master_write_avalon_master_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal master_write_avalon_master_byteenable :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal master_write_avalon_master_reset :  STD_LOGIC;
                signal master_write_avalon_master_waitrequest :  STD_LOGIC;
                signal master_write_avalon_master_write :  STD_LOGIC;
                signal master_write_avalon_master_writedata :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal master_write_granted_clock_crossing_write_s1 :  STD_LOGIC;
                signal master_write_qualified_request_clock_crossing_write_s1 :  STD_LOGIC;
                signal master_write_requests_clock_crossing_write_s1 :  STD_LOGIC;
                signal module_input24 :  STD_LOGIC;
                signal module_input25 :  STD_LOGIC;
                signal module_input26 :  STD_LOGIC;
                signal reset_n_sources :  STD_LOGIC;
                signal sdram_s1_address :  STD_LOGIC_VECTOR (22 DOWNTO 0);
                signal sdram_s1_byteenable_n :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal sdram_s1_chipselect :  STD_LOGIC;
                signal sdram_s1_read_n :  STD_LOGIC;
                signal sdram_s1_readdata :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal sdram_s1_readdata_from_sa :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal sdram_s1_readdatavalid :  STD_LOGIC;
                signal sdram_s1_reset_n :  STD_LOGIC;
                signal sdram_s1_waitrequest :  STD_LOGIC;
                signal sdram_s1_waitrequest_from_sa :  STD_LOGIC;
                signal sdram_s1_write_n :  STD_LOGIC;
                signal sdram_s1_writedata :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal system_burst_0_downstream_address :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal system_burst_0_downstream_address_to_slave :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal system_burst_0_downstream_arbitrationshare :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal system_burst_0_downstream_burstcount :  STD_LOGIC;
                signal system_burst_0_downstream_byteenable :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal system_burst_0_downstream_debugaccess :  STD_LOGIC;
                signal system_burst_0_downstream_granted_sdram_s1 :  STD_LOGIC;
                signal system_burst_0_downstream_latency_counter :  STD_LOGIC;
                signal system_burst_0_downstream_nativeaddress :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal system_burst_0_downstream_qualified_request_sdram_s1 :  STD_LOGIC;
                signal system_burst_0_downstream_read :  STD_LOGIC;
                signal system_burst_0_downstream_read_data_valid_sdram_s1 :  STD_LOGIC;
                signal system_burst_0_downstream_read_data_valid_sdram_s1_shift_register :  STD_LOGIC;
                signal system_burst_0_downstream_readdata :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal system_burst_0_downstream_readdatavalid :  STD_LOGIC;
                signal system_burst_0_downstream_requests_sdram_s1 :  STD_LOGIC;
                signal system_burst_0_downstream_reset_n :  STD_LOGIC;
                signal system_burst_0_downstream_waitrequest :  STD_LOGIC;
                signal system_burst_0_downstream_write :  STD_LOGIC;
                signal system_burst_0_downstream_writedata :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal system_burst_0_upstream_address :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal system_burst_0_upstream_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal system_burst_0_upstream_byteaddress :  STD_LOGIC_VECTOR (24 DOWNTO 0);
                signal system_burst_0_upstream_byteenable :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal system_burst_0_upstream_debugaccess :  STD_LOGIC;
                signal system_burst_0_upstream_read :  STD_LOGIC;
                signal system_burst_0_upstream_readdata :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal system_burst_0_upstream_readdata_from_sa :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal system_burst_0_upstream_readdatavalid :  STD_LOGIC;
                signal system_burst_0_upstream_waitrequest :  STD_LOGIC;
                signal system_burst_0_upstream_waitrequest_from_sa :  STD_LOGIC;
                signal system_burst_0_upstream_write :  STD_LOGIC;
                signal system_burst_0_upstream_writedata :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal system_burst_1_downstream_address :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal system_burst_1_downstream_address_to_slave :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal system_burst_1_downstream_arbitrationshare :  STD_LOGIC_VECTOR (4 DOWNTO 0);
                signal system_burst_1_downstream_burstcount :  STD_LOGIC;
                signal system_burst_1_downstream_byteenable :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal system_burst_1_downstream_debugaccess :  STD_LOGIC;
                signal system_burst_1_downstream_granted_sdram_s1 :  STD_LOGIC;
                signal system_burst_1_downstream_latency_counter :  STD_LOGIC;
                signal system_burst_1_downstream_nativeaddress :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal system_burst_1_downstream_qualified_request_sdram_s1 :  STD_LOGIC;
                signal system_burst_1_downstream_read :  STD_LOGIC;
                signal system_burst_1_downstream_read_data_valid_sdram_s1 :  STD_LOGIC;
                signal system_burst_1_downstream_read_data_valid_sdram_s1_shift_register :  STD_LOGIC;
                signal system_burst_1_downstream_readdata :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal system_burst_1_downstream_readdatavalid :  STD_LOGIC;
                signal system_burst_1_downstream_requests_sdram_s1 :  STD_LOGIC;
                signal system_burst_1_downstream_reset_n :  STD_LOGIC;
                signal system_burst_1_downstream_waitrequest :  STD_LOGIC;
                signal system_burst_1_downstream_write :  STD_LOGIC;
                signal system_burst_1_downstream_writedata :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal system_burst_1_upstream_address :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal system_burst_1_upstream_burstcount :  STD_LOGIC_VECTOR (3 DOWNTO 0);
                signal system_burst_1_upstream_byteaddress :  STD_LOGIC_VECTOR (24 DOWNTO 0);
                signal system_burst_1_upstream_byteenable :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal system_burst_1_upstream_debugaccess :  STD_LOGIC;
                signal system_burst_1_upstream_read :  STD_LOGIC;
                signal system_burst_1_upstream_readdata :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal system_burst_1_upstream_readdata_from_sa :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal system_burst_1_upstream_readdatavalid :  STD_LOGIC;
                signal system_burst_1_upstream_waitrequest :  STD_LOGIC;
                signal system_burst_1_upstream_waitrequest_from_sa :  STD_LOGIC;
                signal system_burst_1_upstream_write :  STD_LOGIC;
                signal system_burst_1_upstream_writedata :  STD_LOGIC_VECTOR (15 DOWNTO 0);

begin

  --the_clock_crossing_read_s1, which is an e_instance
  the_clock_crossing_read_s1 : clock_crossing_read_s1_arbitrator
    port map(
      clock_crossing_read_s1_address => clock_crossing_read_s1_address,
      clock_crossing_read_s1_burstcount => clock_crossing_read_s1_burstcount,
      clock_crossing_read_s1_byteenable => clock_crossing_read_s1_byteenable,
      clock_crossing_read_s1_endofpacket_from_sa => clock_crossing_read_s1_endofpacket_from_sa,
      clock_crossing_read_s1_nativeaddress => clock_crossing_read_s1_nativeaddress,
      clock_crossing_read_s1_read => clock_crossing_read_s1_read,
      clock_crossing_read_s1_readdata_from_sa => clock_crossing_read_s1_readdata_from_sa,
      clock_crossing_read_s1_reset_n => clock_crossing_read_s1_reset_n,
      clock_crossing_read_s1_waitrequest_from_sa => clock_crossing_read_s1_waitrequest_from_sa,
      clock_crossing_read_s1_write => clock_crossing_read_s1_write,
      d1_clock_crossing_read_s1_end_xfer => d1_clock_crossing_read_s1_end_xfer,
      master_read_granted_clock_crossing_read_s1 => master_read_granted_clock_crossing_read_s1,
      master_read_qualified_request_clock_crossing_read_s1 => master_read_qualified_request_clock_crossing_read_s1,
      master_read_read_data_valid_clock_crossing_read_s1 => master_read_read_data_valid_clock_crossing_read_s1,
      master_read_read_data_valid_clock_crossing_read_s1_shift_register => master_read_read_data_valid_clock_crossing_read_s1_shift_register,
      master_read_requests_clock_crossing_read_s1 => master_read_requests_clock_crossing_read_s1,
      clk => clk_master_read,
      clock_crossing_read_s1_endofpacket => clock_crossing_read_s1_endofpacket,
      clock_crossing_read_s1_readdata => clock_crossing_read_s1_readdata,
      clock_crossing_read_s1_readdatavalid => clock_crossing_read_s1_readdatavalid,
      clock_crossing_read_s1_waitrequest => clock_crossing_read_s1_waitrequest,
      master_read_avalon_master_address_to_slave => master_read_avalon_master_address_to_slave,
      master_read_avalon_master_burstcount => master_read_avalon_master_burstcount,
      master_read_avalon_master_read => master_read_avalon_master_read,
      master_read_latency_counter => master_read_latency_counter,
      reset_n => clk_master_read_reset_n
    );


  --the_clock_crossing_read_m1, which is an e_instance
  the_clock_crossing_read_m1 : clock_crossing_read_m1_arbitrator
    port map(
      clock_crossing_read_m1_address_to_slave => clock_crossing_read_m1_address_to_slave,
      clock_crossing_read_m1_dbs_address => clock_crossing_read_m1_dbs_address,
      clock_crossing_read_m1_dbs_write_16 => clock_crossing_read_m1_dbs_write_16,
      clock_crossing_read_m1_latency_counter => clock_crossing_read_m1_latency_counter,
      clock_crossing_read_m1_readdata => clock_crossing_read_m1_readdata,
      clock_crossing_read_m1_readdatavalid => clock_crossing_read_m1_readdatavalid,
      clock_crossing_read_m1_reset_n => clock_crossing_read_m1_reset_n,
      clock_crossing_read_m1_waitrequest => clock_crossing_read_m1_waitrequest,
      clk => clk_sdram,
      clock_crossing_read_m1_address => clock_crossing_read_m1_address,
      clock_crossing_read_m1_burstcount => clock_crossing_read_m1_burstcount,
      clock_crossing_read_m1_byteenable => clock_crossing_read_m1_byteenable,
      clock_crossing_read_m1_byteenable_system_burst_1_upstream => clock_crossing_read_m1_byteenable_system_burst_1_upstream,
      clock_crossing_read_m1_granted_system_burst_1_upstream => clock_crossing_read_m1_granted_system_burst_1_upstream,
      clock_crossing_read_m1_qualified_request_system_burst_1_upstream => clock_crossing_read_m1_qualified_request_system_burst_1_upstream,
      clock_crossing_read_m1_read => clock_crossing_read_m1_read,
      clock_crossing_read_m1_read_data_valid_system_burst_1_upstream => clock_crossing_read_m1_read_data_valid_system_burst_1_upstream,
      clock_crossing_read_m1_read_data_valid_system_burst_1_upstream_shift_register => clock_crossing_read_m1_read_data_valid_system_burst_1_upstream_shift_register,
      clock_crossing_read_m1_requests_system_burst_1_upstream => clock_crossing_read_m1_requests_system_burst_1_upstream,
      clock_crossing_read_m1_write => clock_crossing_read_m1_write,
      clock_crossing_read_m1_writedata => clock_crossing_read_m1_writedata,
      d1_system_burst_1_upstream_end_xfer => d1_system_burst_1_upstream_end_xfer,
      reset_n => clk_sdram_reset_n,
      system_burst_1_upstream_readdata_from_sa => system_burst_1_upstream_readdata_from_sa,
      system_burst_1_upstream_waitrequest_from_sa => system_burst_1_upstream_waitrequest_from_sa
    );


  --the_clock_crossing_read, which is an e_ptf_instance
  the_clock_crossing_read : clock_crossing_read
    port map(
      master_address => clock_crossing_read_m1_address,
      master_burstcount => clock_crossing_read_m1_burstcount,
      master_byteenable => clock_crossing_read_m1_byteenable,
      master_nativeaddress => clock_crossing_read_m1_nativeaddress,
      master_read => clock_crossing_read_m1_read,
      master_write => clock_crossing_read_m1_write,
      master_writedata => clock_crossing_read_m1_writedata,
      slave_endofpacket => clock_crossing_read_s1_endofpacket,
      slave_readdata => clock_crossing_read_s1_readdata,
      slave_readdatavalid => clock_crossing_read_s1_readdatavalid,
      slave_waitrequest => clock_crossing_read_s1_waitrequest,
      master_clk => clk_sdram,
      master_endofpacket => clock_crossing_read_m1_endofpacket,
      master_readdata => clock_crossing_read_m1_readdata,
      master_readdatavalid => clock_crossing_read_m1_readdatavalid,
      master_reset_n => clock_crossing_read_m1_reset_n,
      master_waitrequest => clock_crossing_read_m1_waitrequest,
      slave_address => clock_crossing_read_s1_address,
      slave_burstcount => clock_crossing_read_s1_burstcount,
      slave_byteenable => clock_crossing_read_s1_byteenable,
      slave_clk => clk_master_read,
      slave_nativeaddress => clock_crossing_read_s1_nativeaddress,
      slave_read => clock_crossing_read_s1_read,
      slave_reset_n => clock_crossing_read_s1_reset_n,
      slave_write => clock_crossing_read_s1_write,
      slave_writedata => clock_crossing_read_s1_writedata
    );


  --the_clock_crossing_write_s1, which is an e_instance
  the_clock_crossing_write_s1 : clock_crossing_write_s1_arbitrator
    port map(
      clock_crossing_write_s1_address => clock_crossing_write_s1_address,
      clock_crossing_write_s1_burstcount => clock_crossing_write_s1_burstcount,
      clock_crossing_write_s1_byteenable => clock_crossing_write_s1_byteenable,
      clock_crossing_write_s1_endofpacket_from_sa => clock_crossing_write_s1_endofpacket_from_sa,
      clock_crossing_write_s1_nativeaddress => clock_crossing_write_s1_nativeaddress,
      clock_crossing_write_s1_read => clock_crossing_write_s1_read,
      clock_crossing_write_s1_readdata_from_sa => clock_crossing_write_s1_readdata_from_sa,
      clock_crossing_write_s1_readdatavalid_from_sa => clock_crossing_write_s1_readdatavalid_from_sa,
      clock_crossing_write_s1_reset_n => clock_crossing_write_s1_reset_n,
      clock_crossing_write_s1_waitrequest_from_sa => clock_crossing_write_s1_waitrequest_from_sa,
      clock_crossing_write_s1_write => clock_crossing_write_s1_write,
      clock_crossing_write_s1_writedata => clock_crossing_write_s1_writedata,
      d1_clock_crossing_write_s1_end_xfer => d1_clock_crossing_write_s1_end_xfer,
      master_write_granted_clock_crossing_write_s1 => master_write_granted_clock_crossing_write_s1,
      master_write_qualified_request_clock_crossing_write_s1 => master_write_qualified_request_clock_crossing_write_s1,
      master_write_requests_clock_crossing_write_s1 => master_write_requests_clock_crossing_write_s1,
      clk => clk_master_write,
      clock_crossing_write_s1_endofpacket => clock_crossing_write_s1_endofpacket,
      clock_crossing_write_s1_readdata => clock_crossing_write_s1_readdata,
      clock_crossing_write_s1_readdatavalid => clock_crossing_write_s1_readdatavalid,
      clock_crossing_write_s1_waitrequest => clock_crossing_write_s1_waitrequest,
      master_write_avalon_master_address_to_slave => master_write_avalon_master_address_to_slave,
      master_write_avalon_master_burstcount => master_write_avalon_master_burstcount,
      master_write_avalon_master_byteenable => master_write_avalon_master_byteenable,
      master_write_avalon_master_write => master_write_avalon_master_write,
      master_write_avalon_master_writedata => master_write_avalon_master_writedata,
      reset_n => clk_master_write_reset_n
    );


  --the_clock_crossing_write_m1, which is an e_instance
  the_clock_crossing_write_m1 : clock_crossing_write_m1_arbitrator
    port map(
      clock_crossing_write_m1_address_to_slave => clock_crossing_write_m1_address_to_slave,
      clock_crossing_write_m1_dbs_address => clock_crossing_write_m1_dbs_address,
      clock_crossing_write_m1_dbs_write_16 => clock_crossing_write_m1_dbs_write_16,
      clock_crossing_write_m1_latency_counter => clock_crossing_write_m1_latency_counter,
      clock_crossing_write_m1_readdata => clock_crossing_write_m1_readdata,
      clock_crossing_write_m1_readdatavalid => clock_crossing_write_m1_readdatavalid,
      clock_crossing_write_m1_reset_n => clock_crossing_write_m1_reset_n,
      clock_crossing_write_m1_waitrequest => clock_crossing_write_m1_waitrequest,
      clk => clk_sdram,
      clock_crossing_write_m1_address => clock_crossing_write_m1_address,
      clock_crossing_write_m1_burstcount => clock_crossing_write_m1_burstcount,
      clock_crossing_write_m1_byteenable => clock_crossing_write_m1_byteenable,
      clock_crossing_write_m1_byteenable_system_burst_0_upstream => clock_crossing_write_m1_byteenable_system_burst_0_upstream,
      clock_crossing_write_m1_granted_system_burst_0_upstream => clock_crossing_write_m1_granted_system_burst_0_upstream,
      clock_crossing_write_m1_qualified_request_system_burst_0_upstream => clock_crossing_write_m1_qualified_request_system_burst_0_upstream,
      clock_crossing_write_m1_read => clock_crossing_write_m1_read,
      clock_crossing_write_m1_read_data_valid_system_burst_0_upstream => clock_crossing_write_m1_read_data_valid_system_burst_0_upstream,
      clock_crossing_write_m1_read_data_valid_system_burst_0_upstream_shift_register => clock_crossing_write_m1_read_data_valid_system_burst_0_upstream_shift_register,
      clock_crossing_write_m1_requests_system_burst_0_upstream => clock_crossing_write_m1_requests_system_burst_0_upstream,
      clock_crossing_write_m1_write => clock_crossing_write_m1_write,
      clock_crossing_write_m1_writedata => clock_crossing_write_m1_writedata,
      d1_system_burst_0_upstream_end_xfer => d1_system_burst_0_upstream_end_xfer,
      reset_n => clk_sdram_reset_n,
      system_burst_0_upstream_readdata_from_sa => system_burst_0_upstream_readdata_from_sa,
      system_burst_0_upstream_waitrequest_from_sa => system_burst_0_upstream_waitrequest_from_sa
    );


  --the_clock_crossing_write, which is an e_ptf_instance
  the_clock_crossing_write : clock_crossing_write
    port map(
      master_address => clock_crossing_write_m1_address,
      master_burstcount => clock_crossing_write_m1_burstcount,
      master_byteenable => clock_crossing_write_m1_byteenable,
      master_nativeaddress => clock_crossing_write_m1_nativeaddress,
      master_read => clock_crossing_write_m1_read,
      master_write => clock_crossing_write_m1_write,
      master_writedata => clock_crossing_write_m1_writedata,
      slave_endofpacket => clock_crossing_write_s1_endofpacket,
      slave_readdata => clock_crossing_write_s1_readdata,
      slave_readdatavalid => clock_crossing_write_s1_readdatavalid,
      slave_waitrequest => clock_crossing_write_s1_waitrequest,
      master_clk => clk_sdram,
      master_endofpacket => clock_crossing_write_m1_endofpacket,
      master_readdata => clock_crossing_write_m1_readdata,
      master_readdatavalid => clock_crossing_write_m1_readdatavalid,
      master_reset_n => clock_crossing_write_m1_reset_n,
      master_waitrequest => clock_crossing_write_m1_waitrequest,
      slave_address => clock_crossing_write_s1_address,
      slave_burstcount => clock_crossing_write_s1_burstcount,
      slave_byteenable => clock_crossing_write_s1_byteenable,
      slave_clk => clk_master_write,
      slave_nativeaddress => clock_crossing_write_s1_nativeaddress,
      slave_read => clock_crossing_write_s1_read,
      slave_reset_n => clock_crossing_write_s1_reset_n,
      slave_write => clock_crossing_write_s1_write,
      slave_writedata => clock_crossing_write_s1_writedata
    );


  --the_master_read_avalon_master, which is an e_instance
  the_master_read_avalon_master : master_read_avalon_master_arbitrator
    port map(
      master_read_avalon_master_address_to_slave => master_read_avalon_master_address_to_slave,
      master_read_avalon_master_readdata => master_read_avalon_master_readdata,
      master_read_avalon_master_readdatavalid => master_read_avalon_master_readdatavalid,
      master_read_avalon_master_reset => master_read_avalon_master_reset,
      master_read_avalon_master_waitrequest => master_read_avalon_master_waitrequest,
      master_read_latency_counter => master_read_latency_counter,
      clk => clk_master_read,
      clock_crossing_read_s1_readdata_from_sa => clock_crossing_read_s1_readdata_from_sa,
      clock_crossing_read_s1_waitrequest_from_sa => clock_crossing_read_s1_waitrequest_from_sa,
      d1_clock_crossing_read_s1_end_xfer => d1_clock_crossing_read_s1_end_xfer,
      master_read_avalon_master_address => master_read_avalon_master_address,
      master_read_avalon_master_burstcount => master_read_avalon_master_burstcount,
      master_read_avalon_master_byteenable => master_read_avalon_master_byteenable,
      master_read_avalon_master_read => master_read_avalon_master_read,
      master_read_granted_clock_crossing_read_s1 => master_read_granted_clock_crossing_read_s1,
      master_read_qualified_request_clock_crossing_read_s1 => master_read_qualified_request_clock_crossing_read_s1,
      master_read_read_data_valid_clock_crossing_read_s1 => master_read_read_data_valid_clock_crossing_read_s1,
      master_read_read_data_valid_clock_crossing_read_s1_shift_register => master_read_read_data_valid_clock_crossing_read_s1_shift_register,
      master_read_requests_clock_crossing_read_s1 => master_read_requests_clock_crossing_read_s1,
      reset_n => clk_master_read_reset_n
    );


  --the_master_read, which is an e_ptf_instance
  the_master_read : master_read
    port map(
      control_done => internal_control_done_from_the_master_read,
      control_early_done => internal_control_early_done_from_the_master_read,
      master_address => master_read_avalon_master_address,
      master_burstcount => master_read_avalon_master_burstcount,
      master_byteenable => master_read_avalon_master_byteenable,
      master_read => master_read_avalon_master_read,
      user_buffer_output_data => internal_user_buffer_output_data_from_the_master_read,
      user_data_available => internal_user_data_available_from_the_master_read,
      clk => clk_master_read,
      control_fixed_location => control_fixed_location_to_the_master_read,
      control_go => control_go_to_the_master_read,
      control_read_base => control_read_base_to_the_master_read,
      control_read_length => control_read_length_to_the_master_read,
      master_readdata => master_read_avalon_master_readdata,
      master_readdatavalid => master_read_avalon_master_readdatavalid,
      master_waitrequest => master_read_avalon_master_waitrequest,
      reset => master_read_avalon_master_reset,
      user_read_buffer => user_read_buffer_to_the_master_read
    );


  --the_master_write_avalon_master, which is an e_instance
  the_master_write_avalon_master : master_write_avalon_master_arbitrator
    port map(
      master_write_avalon_master_address_to_slave => master_write_avalon_master_address_to_slave,
      master_write_avalon_master_reset => master_write_avalon_master_reset,
      master_write_avalon_master_waitrequest => master_write_avalon_master_waitrequest,
      clk => clk_master_write,
      clock_crossing_write_s1_waitrequest_from_sa => clock_crossing_write_s1_waitrequest_from_sa,
      d1_clock_crossing_write_s1_end_xfer => d1_clock_crossing_write_s1_end_xfer,
      master_write_avalon_master_address => master_write_avalon_master_address,
      master_write_avalon_master_burstcount => master_write_avalon_master_burstcount,
      master_write_avalon_master_byteenable => master_write_avalon_master_byteenable,
      master_write_avalon_master_write => master_write_avalon_master_write,
      master_write_avalon_master_writedata => master_write_avalon_master_writedata,
      master_write_granted_clock_crossing_write_s1 => master_write_granted_clock_crossing_write_s1,
      master_write_qualified_request_clock_crossing_write_s1 => master_write_qualified_request_clock_crossing_write_s1,
      master_write_requests_clock_crossing_write_s1 => master_write_requests_clock_crossing_write_s1,
      reset_n => clk_master_write_reset_n
    );


  --the_master_write, which is an e_ptf_instance
  the_master_write : master_write
    port map(
      control_done => internal_control_done_from_the_master_write,
      master_address => master_write_avalon_master_address,
      master_burstcount => master_write_avalon_master_burstcount,
      master_byteenable => master_write_avalon_master_byteenable,
      master_write => master_write_avalon_master_write,
      master_writedata => master_write_avalon_master_writedata,
      user_buffer_full => internal_user_buffer_full_from_the_master_write,
      clk => clk_master_write,
      control_fixed_location => control_fixed_location_to_the_master_write,
      control_go => control_go_to_the_master_write,
      control_write_base => control_write_base_to_the_master_write,
      control_write_length => control_write_length_to_the_master_write,
      master_waitrequest => master_write_avalon_master_waitrequest,
      reset => master_write_avalon_master_reset,
      user_buffer_input_data => user_buffer_input_data_to_the_master_write,
      user_write_buffer => user_write_buffer_to_the_master_write
    );


  --the_sdram_s1, which is an e_instance
  the_sdram_s1 : sdram_s1_arbitrator
    port map(
      d1_sdram_s1_end_xfer => d1_sdram_s1_end_xfer,
      sdram_s1_address => sdram_s1_address,
      sdram_s1_byteenable_n => sdram_s1_byteenable_n,
      sdram_s1_chipselect => sdram_s1_chipselect,
      sdram_s1_read_n => sdram_s1_read_n,
      sdram_s1_readdata_from_sa => sdram_s1_readdata_from_sa,
      sdram_s1_reset_n => sdram_s1_reset_n,
      sdram_s1_waitrequest_from_sa => sdram_s1_waitrequest_from_sa,
      sdram_s1_write_n => sdram_s1_write_n,
      sdram_s1_writedata => sdram_s1_writedata,
      system_burst_0_downstream_granted_sdram_s1 => system_burst_0_downstream_granted_sdram_s1,
      system_burst_0_downstream_qualified_request_sdram_s1 => system_burst_0_downstream_qualified_request_sdram_s1,
      system_burst_0_downstream_read_data_valid_sdram_s1 => system_burst_0_downstream_read_data_valid_sdram_s1,
      system_burst_0_downstream_read_data_valid_sdram_s1_shift_register => system_burst_0_downstream_read_data_valid_sdram_s1_shift_register,
      system_burst_0_downstream_requests_sdram_s1 => system_burst_0_downstream_requests_sdram_s1,
      system_burst_1_downstream_granted_sdram_s1 => system_burst_1_downstream_granted_sdram_s1,
      system_burst_1_downstream_qualified_request_sdram_s1 => system_burst_1_downstream_qualified_request_sdram_s1,
      system_burst_1_downstream_read_data_valid_sdram_s1 => system_burst_1_downstream_read_data_valid_sdram_s1,
      system_burst_1_downstream_read_data_valid_sdram_s1_shift_register => system_burst_1_downstream_read_data_valid_sdram_s1_shift_register,
      system_burst_1_downstream_requests_sdram_s1 => system_burst_1_downstream_requests_sdram_s1,
      clk => clk_sdram,
      reset_n => clk_sdram_reset_n,
      sdram_s1_readdata => sdram_s1_readdata,
      sdram_s1_readdatavalid => sdram_s1_readdatavalid,
      sdram_s1_waitrequest => sdram_s1_waitrequest,
      system_burst_0_downstream_address_to_slave => system_burst_0_downstream_address_to_slave,
      system_burst_0_downstream_arbitrationshare => system_burst_0_downstream_arbitrationshare,
      system_burst_0_downstream_burstcount => system_burst_0_downstream_burstcount,
      system_burst_0_downstream_byteenable => system_burst_0_downstream_byteenable,
      system_burst_0_downstream_latency_counter => system_burst_0_downstream_latency_counter,
      system_burst_0_downstream_read => system_burst_0_downstream_read,
      system_burst_0_downstream_write => system_burst_0_downstream_write,
      system_burst_0_downstream_writedata => system_burst_0_downstream_writedata,
      system_burst_1_downstream_address_to_slave => system_burst_1_downstream_address_to_slave,
      system_burst_1_downstream_arbitrationshare => system_burst_1_downstream_arbitrationshare,
      system_burst_1_downstream_burstcount => system_burst_1_downstream_burstcount,
      system_burst_1_downstream_byteenable => system_burst_1_downstream_byteenable,
      system_burst_1_downstream_latency_counter => system_burst_1_downstream_latency_counter,
      system_burst_1_downstream_read => system_burst_1_downstream_read,
      system_burst_1_downstream_write => system_burst_1_downstream_write,
      system_burst_1_downstream_writedata => system_burst_1_downstream_writedata
    );


  --the_sdram, which is an e_ptf_instance
  the_sdram : sdram
    port map(
      za_data => sdram_s1_readdata,
      za_valid => sdram_s1_readdatavalid,
      za_waitrequest => sdram_s1_waitrequest,
      zs_addr => internal_zs_addr_from_the_sdram,
      zs_ba => internal_zs_ba_from_the_sdram,
      zs_cas_n => internal_zs_cas_n_from_the_sdram,
      zs_cke => internal_zs_cke_from_the_sdram,
      zs_cs_n => internal_zs_cs_n_from_the_sdram,
      zs_dq => zs_dq_to_and_from_the_sdram,
      zs_dqm => internal_zs_dqm_from_the_sdram,
      zs_ras_n => internal_zs_ras_n_from_the_sdram,
      zs_we_n => internal_zs_we_n_from_the_sdram,
      az_addr => sdram_s1_address,
      az_be_n => sdram_s1_byteenable_n,
      az_cs => sdram_s1_chipselect,
      az_data => sdram_s1_writedata,
      az_rd_n => sdram_s1_read_n,
      az_wr_n => sdram_s1_write_n,
      clk => clk_sdram,
      reset_n => sdram_s1_reset_n
    );


  --the_system_burst_0_upstream, which is an e_instance
  the_system_burst_0_upstream : system_burst_0_upstream_arbitrator
    port map(
      clock_crossing_write_m1_byteenable_system_burst_0_upstream => clock_crossing_write_m1_byteenable_system_burst_0_upstream,
      clock_crossing_write_m1_granted_system_burst_0_upstream => clock_crossing_write_m1_granted_system_burst_0_upstream,
      clock_crossing_write_m1_qualified_request_system_burst_0_upstream => clock_crossing_write_m1_qualified_request_system_burst_0_upstream,
      clock_crossing_write_m1_read_data_valid_system_burst_0_upstream => clock_crossing_write_m1_read_data_valid_system_burst_0_upstream,
      clock_crossing_write_m1_read_data_valid_system_burst_0_upstream_shift_register => clock_crossing_write_m1_read_data_valid_system_burst_0_upstream_shift_register,
      clock_crossing_write_m1_requests_system_burst_0_upstream => clock_crossing_write_m1_requests_system_burst_0_upstream,
      d1_system_burst_0_upstream_end_xfer => d1_system_burst_0_upstream_end_xfer,
      system_burst_0_upstream_address => system_burst_0_upstream_address,
      system_burst_0_upstream_burstcount => system_burst_0_upstream_burstcount,
      system_burst_0_upstream_byteaddress => system_burst_0_upstream_byteaddress,
      system_burst_0_upstream_byteenable => system_burst_0_upstream_byteenable,
      system_burst_0_upstream_debugaccess => system_burst_0_upstream_debugaccess,
      system_burst_0_upstream_read => system_burst_0_upstream_read,
      system_burst_0_upstream_readdata_from_sa => system_burst_0_upstream_readdata_from_sa,
      system_burst_0_upstream_waitrequest_from_sa => system_burst_0_upstream_waitrequest_from_sa,
      system_burst_0_upstream_write => system_burst_0_upstream_write,
      system_burst_0_upstream_writedata => system_burst_0_upstream_writedata,
      clk => clk_sdram,
      clock_crossing_write_m1_address_to_slave => clock_crossing_write_m1_address_to_slave,
      clock_crossing_write_m1_burstcount => clock_crossing_write_m1_burstcount,
      clock_crossing_write_m1_byteenable => clock_crossing_write_m1_byteenable,
      clock_crossing_write_m1_dbs_address => clock_crossing_write_m1_dbs_address,
      clock_crossing_write_m1_dbs_write_16 => clock_crossing_write_m1_dbs_write_16,
      clock_crossing_write_m1_latency_counter => clock_crossing_write_m1_latency_counter,
      clock_crossing_write_m1_read => clock_crossing_write_m1_read,
      clock_crossing_write_m1_write => clock_crossing_write_m1_write,
      reset_n => clk_sdram_reset_n,
      system_burst_0_upstream_readdata => system_burst_0_upstream_readdata,
      system_burst_0_upstream_readdatavalid => system_burst_0_upstream_readdatavalid,
      system_burst_0_upstream_waitrequest => system_burst_0_upstream_waitrequest
    );


  --the_system_burst_0_downstream, which is an e_instance
  the_system_burst_0_downstream : system_burst_0_downstream_arbitrator
    port map(
      system_burst_0_downstream_address_to_slave => system_burst_0_downstream_address_to_slave,
      system_burst_0_downstream_latency_counter => system_burst_0_downstream_latency_counter,
      system_burst_0_downstream_readdata => system_burst_0_downstream_readdata,
      system_burst_0_downstream_readdatavalid => system_burst_0_downstream_readdatavalid,
      system_burst_0_downstream_reset_n => system_burst_0_downstream_reset_n,
      system_burst_0_downstream_waitrequest => system_burst_0_downstream_waitrequest,
      clk => clk_sdram,
      d1_sdram_s1_end_xfer => d1_sdram_s1_end_xfer,
      reset_n => clk_sdram_reset_n,
      sdram_s1_readdata_from_sa => sdram_s1_readdata_from_sa,
      sdram_s1_waitrequest_from_sa => sdram_s1_waitrequest_from_sa,
      system_burst_0_downstream_address => system_burst_0_downstream_address,
      system_burst_0_downstream_burstcount => system_burst_0_downstream_burstcount,
      system_burst_0_downstream_byteenable => system_burst_0_downstream_byteenable,
      system_burst_0_downstream_granted_sdram_s1 => system_burst_0_downstream_granted_sdram_s1,
      system_burst_0_downstream_qualified_request_sdram_s1 => system_burst_0_downstream_qualified_request_sdram_s1,
      system_burst_0_downstream_read => system_burst_0_downstream_read,
      system_burst_0_downstream_read_data_valid_sdram_s1 => system_burst_0_downstream_read_data_valid_sdram_s1,
      system_burst_0_downstream_read_data_valid_sdram_s1_shift_register => system_burst_0_downstream_read_data_valid_sdram_s1_shift_register,
      system_burst_0_downstream_requests_sdram_s1 => system_burst_0_downstream_requests_sdram_s1,
      system_burst_0_downstream_write => system_burst_0_downstream_write,
      system_burst_0_downstream_writedata => system_burst_0_downstream_writedata
    );


  --the_system_burst_0, which is an e_ptf_instance
  the_system_burst_0 : system_burst_0
    port map(
      downstream_address => system_burst_0_downstream_address,
      downstream_arbitrationshare => system_burst_0_downstream_arbitrationshare,
      downstream_burstcount => system_burst_0_downstream_burstcount,
      downstream_byteenable => system_burst_0_downstream_byteenable,
      downstream_debugaccess => system_burst_0_downstream_debugaccess,
      downstream_nativeaddress => system_burst_0_downstream_nativeaddress,
      downstream_read => system_burst_0_downstream_read,
      downstream_write => system_burst_0_downstream_write,
      downstream_writedata => system_burst_0_downstream_writedata,
      upstream_readdata => system_burst_0_upstream_readdata,
      upstream_readdatavalid => system_burst_0_upstream_readdatavalid,
      upstream_waitrequest => system_burst_0_upstream_waitrequest,
      clk => clk_sdram,
      downstream_readdata => system_burst_0_downstream_readdata,
      downstream_readdatavalid => system_burst_0_downstream_readdatavalid,
      downstream_waitrequest => system_burst_0_downstream_waitrequest,
      reset_n => system_burst_0_downstream_reset_n,
      upstream_address => system_burst_0_upstream_byteaddress,
      upstream_burstcount => system_burst_0_upstream_burstcount,
      upstream_byteenable => system_burst_0_upstream_byteenable,
      upstream_debugaccess => system_burst_0_upstream_debugaccess,
      upstream_nativeaddress => system_burst_0_upstream_address,
      upstream_read => system_burst_0_upstream_read,
      upstream_write => system_burst_0_upstream_write,
      upstream_writedata => system_burst_0_upstream_writedata
    );


  --the_system_burst_1_upstream, which is an e_instance
  the_system_burst_1_upstream : system_burst_1_upstream_arbitrator
    port map(
      clock_crossing_read_m1_byteenable_system_burst_1_upstream => clock_crossing_read_m1_byteenable_system_burst_1_upstream,
      clock_crossing_read_m1_granted_system_burst_1_upstream => clock_crossing_read_m1_granted_system_burst_1_upstream,
      clock_crossing_read_m1_qualified_request_system_burst_1_upstream => clock_crossing_read_m1_qualified_request_system_burst_1_upstream,
      clock_crossing_read_m1_read_data_valid_system_burst_1_upstream => clock_crossing_read_m1_read_data_valid_system_burst_1_upstream,
      clock_crossing_read_m1_read_data_valid_system_burst_1_upstream_shift_register => clock_crossing_read_m1_read_data_valid_system_burst_1_upstream_shift_register,
      clock_crossing_read_m1_requests_system_burst_1_upstream => clock_crossing_read_m1_requests_system_burst_1_upstream,
      d1_system_burst_1_upstream_end_xfer => d1_system_burst_1_upstream_end_xfer,
      system_burst_1_upstream_address => system_burst_1_upstream_address,
      system_burst_1_upstream_burstcount => system_burst_1_upstream_burstcount,
      system_burst_1_upstream_byteaddress => system_burst_1_upstream_byteaddress,
      system_burst_1_upstream_byteenable => system_burst_1_upstream_byteenable,
      system_burst_1_upstream_debugaccess => system_burst_1_upstream_debugaccess,
      system_burst_1_upstream_read => system_burst_1_upstream_read,
      system_burst_1_upstream_readdata_from_sa => system_burst_1_upstream_readdata_from_sa,
      system_burst_1_upstream_waitrequest_from_sa => system_burst_1_upstream_waitrequest_from_sa,
      system_burst_1_upstream_write => system_burst_1_upstream_write,
      system_burst_1_upstream_writedata => system_burst_1_upstream_writedata,
      clk => clk_sdram,
      clock_crossing_read_m1_address_to_slave => clock_crossing_read_m1_address_to_slave,
      clock_crossing_read_m1_burstcount => clock_crossing_read_m1_burstcount,
      clock_crossing_read_m1_byteenable => clock_crossing_read_m1_byteenable,
      clock_crossing_read_m1_dbs_address => clock_crossing_read_m1_dbs_address,
      clock_crossing_read_m1_dbs_write_16 => clock_crossing_read_m1_dbs_write_16,
      clock_crossing_read_m1_latency_counter => clock_crossing_read_m1_latency_counter,
      clock_crossing_read_m1_read => clock_crossing_read_m1_read,
      clock_crossing_read_m1_write => clock_crossing_read_m1_write,
      reset_n => clk_sdram_reset_n,
      system_burst_1_upstream_readdata => system_burst_1_upstream_readdata,
      system_burst_1_upstream_readdatavalid => system_burst_1_upstream_readdatavalid,
      system_burst_1_upstream_waitrequest => system_burst_1_upstream_waitrequest
    );


  --the_system_burst_1_downstream, which is an e_instance
  the_system_burst_1_downstream : system_burst_1_downstream_arbitrator
    port map(
      system_burst_1_downstream_address_to_slave => system_burst_1_downstream_address_to_slave,
      system_burst_1_downstream_latency_counter => system_burst_1_downstream_latency_counter,
      system_burst_1_downstream_readdata => system_burst_1_downstream_readdata,
      system_burst_1_downstream_readdatavalid => system_burst_1_downstream_readdatavalid,
      system_burst_1_downstream_reset_n => system_burst_1_downstream_reset_n,
      system_burst_1_downstream_waitrequest => system_burst_1_downstream_waitrequest,
      clk => clk_sdram,
      d1_sdram_s1_end_xfer => d1_sdram_s1_end_xfer,
      reset_n => clk_sdram_reset_n,
      sdram_s1_readdata_from_sa => sdram_s1_readdata_from_sa,
      sdram_s1_waitrequest_from_sa => sdram_s1_waitrequest_from_sa,
      system_burst_1_downstream_address => system_burst_1_downstream_address,
      system_burst_1_downstream_burstcount => system_burst_1_downstream_burstcount,
      system_burst_1_downstream_byteenable => system_burst_1_downstream_byteenable,
      system_burst_1_downstream_granted_sdram_s1 => system_burst_1_downstream_granted_sdram_s1,
      system_burst_1_downstream_qualified_request_sdram_s1 => system_burst_1_downstream_qualified_request_sdram_s1,
      system_burst_1_downstream_read => system_burst_1_downstream_read,
      system_burst_1_downstream_read_data_valid_sdram_s1 => system_burst_1_downstream_read_data_valid_sdram_s1,
      system_burst_1_downstream_read_data_valid_sdram_s1_shift_register => system_burst_1_downstream_read_data_valid_sdram_s1_shift_register,
      system_burst_1_downstream_requests_sdram_s1 => system_burst_1_downstream_requests_sdram_s1,
      system_burst_1_downstream_write => system_burst_1_downstream_write,
      system_burst_1_downstream_writedata => system_burst_1_downstream_writedata
    );


  --the_system_burst_1, which is an e_ptf_instance
  the_system_burst_1 : system_burst_1
    port map(
      downstream_address => system_burst_1_downstream_address,
      downstream_arbitrationshare => system_burst_1_downstream_arbitrationshare,
      downstream_burstcount => system_burst_1_downstream_burstcount,
      downstream_byteenable => system_burst_1_downstream_byteenable,
      downstream_debugaccess => system_burst_1_downstream_debugaccess,
      downstream_nativeaddress => system_burst_1_downstream_nativeaddress,
      downstream_read => system_burst_1_downstream_read,
      downstream_write => system_burst_1_downstream_write,
      downstream_writedata => system_burst_1_downstream_writedata,
      upstream_readdata => system_burst_1_upstream_readdata,
      upstream_readdatavalid => system_burst_1_upstream_readdatavalid,
      upstream_waitrequest => system_burst_1_upstream_waitrequest,
      clk => clk_sdram,
      downstream_readdata => system_burst_1_downstream_readdata,
      downstream_readdatavalid => system_burst_1_downstream_readdatavalid,
      downstream_waitrequest => system_burst_1_downstream_waitrequest,
      reset_n => system_burst_1_downstream_reset_n,
      upstream_address => system_burst_1_upstream_byteaddress,
      upstream_burstcount => system_burst_1_upstream_burstcount,
      upstream_byteenable => system_burst_1_upstream_byteenable,
      upstream_debugaccess => system_burst_1_upstream_debugaccess,
      upstream_nativeaddress => system_burst_1_upstream_address,
      upstream_read => system_burst_1_upstream_read,
      upstream_write => system_burst_1_upstream_write,
      upstream_writedata => system_burst_1_upstream_writedata
    );


  --reset is asserted asynchronously and deasserted synchronously
  system_reset_clk_master_read_domain_synch : system_reset_clk_master_read_domain_synch_module
    port map(
      data_out => clk_master_read_reset_n,
      clk => clk_master_read,
      data_in => module_input24,
      reset_n => reset_n_sources
    );

  module_input24 <= std_logic'('1');

  --reset sources mux, which is an e_mux
  reset_n_sources <= Vector_To_Std_Logic(NOT (((((std_logic_vector'("0000000000000000000000000000000") & (A_TOSTDLOGICVECTOR(NOT reset_n))) OR std_logic_vector'("00000000000000000000000000000000")) OR std_logic_vector'("00000000000000000000000000000000")) OR std_logic_vector'("00000000000000000000000000000000"))));
  --reset is asserted asynchronously and deasserted synchronously
  system_reset_clk_master_write_domain_synch : system_reset_clk_master_write_domain_synch_module
    port map(
      data_out => clk_master_write_reset_n,
      clk => clk_master_write,
      data_in => module_input25,
      reset_n => reset_n_sources
    );

  module_input25 <= std_logic'('1');

  --reset is asserted asynchronously and deasserted synchronously
  system_reset_clk_sdram_domain_synch : system_reset_clk_sdram_domain_synch_module
    port map(
      data_out => clk_sdram_reset_n,
      clk => clk_sdram,
      data_in => module_input26,
      reset_n => reset_n_sources
    );

  module_input26 <= std_logic'('1');

  --clock_crossing_read_m1_endofpacket of type endofpacket does not connect to anything so wire it to default (0)
  clock_crossing_read_m1_endofpacket <= std_logic'('0');
  --clock_crossing_read_s1_writedata of type writedata does not connect to anything so wire it to default (0)
  clock_crossing_read_s1_writedata <= std_logic_vector'("00000000000000000000000000000000");
  --clock_crossing_write_m1_endofpacket of type endofpacket does not connect to anything so wire it to default (0)
  clock_crossing_write_m1_endofpacket <= std_logic'('0');
  --vhdl renameroo for output signals
  control_done_from_the_master_read <= internal_control_done_from_the_master_read;
  --vhdl renameroo for output signals
  control_done_from_the_master_write <= internal_control_done_from_the_master_write;
  --vhdl renameroo for output signals
  control_early_done_from_the_master_read <= internal_control_early_done_from_the_master_read;
  --vhdl renameroo for output signals
  user_buffer_full_from_the_master_write <= internal_user_buffer_full_from_the_master_write;
  --vhdl renameroo for output signals
  user_buffer_output_data_from_the_master_read <= internal_user_buffer_output_data_from_the_master_read;
  --vhdl renameroo for output signals
  user_data_available_from_the_master_read <= internal_user_data_available_from_the_master_read;
  --vhdl renameroo for output signals
  zs_addr_from_the_sdram <= internal_zs_addr_from_the_sdram;
  --vhdl renameroo for output signals
  zs_ba_from_the_sdram <= internal_zs_ba_from_the_sdram;
  --vhdl renameroo for output signals
  zs_cas_n_from_the_sdram <= internal_zs_cas_n_from_the_sdram;
  --vhdl renameroo for output signals
  zs_cke_from_the_sdram <= internal_zs_cke_from_the_sdram;
  --vhdl renameroo for output signals
  zs_cs_n_from_the_sdram <= internal_zs_cs_n_from_the_sdram;
  --vhdl renameroo for output signals
  zs_dqm_from_the_sdram <= internal_zs_dqm_from_the_sdram;
  --vhdl renameroo for output signals
  zs_ras_n_from_the_sdram <= internal_zs_ras_n_from_the_sdram;
  --vhdl renameroo for output signals
  zs_we_n_from_the_sdram <= internal_zs_we_n_from_the_sdram;

end europa;


--synthesis translate_off

library altera;
use altera.altera_europa_support_lib.all;

library altera_mf;
use altera_mf.altera_mf_components.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;



-- <ALTERA_NOTE> CODE INSERTED BETWEEN HERE
--add your libraries here
-- AND HERE WILL BE PRESERVED </ALTERA_NOTE>

entity test_bench is 
end entity test_bench;


architecture europa of test_bench is
component system is 
           port (
                 -- 1) global signals:
                    signal clk_master_read : IN STD_LOGIC;
                    signal clk_master_write : IN STD_LOGIC;
                    signal clk_sdram : IN STD_LOGIC;
                    signal reset_n : IN STD_LOGIC;

                 -- the_master_read
                    signal control_done_from_the_master_read : OUT STD_LOGIC;
                    signal control_early_done_from_the_master_read : OUT STD_LOGIC;
                    signal control_fixed_location_to_the_master_read : IN STD_LOGIC;
                    signal control_go_to_the_master_read : IN STD_LOGIC;
                    signal control_read_base_to_the_master_read : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal control_read_length_to_the_master_read : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal user_buffer_output_data_from_the_master_read : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal user_data_available_from_the_master_read : OUT STD_LOGIC;
                    signal user_read_buffer_to_the_master_read : IN STD_LOGIC;

                 -- the_master_write
                    signal control_done_from_the_master_write : OUT STD_LOGIC;
                    signal control_fixed_location_to_the_master_write : IN STD_LOGIC;
                    signal control_go_to_the_master_write : IN STD_LOGIC;
                    signal control_write_base_to_the_master_write : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal control_write_length_to_the_master_write : IN STD_LOGIC_VECTOR (23 DOWNTO 0);
                    signal user_buffer_full_from_the_master_write : OUT STD_LOGIC;
                    signal user_buffer_input_data_to_the_master_write : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
                    signal user_write_buffer_to_the_master_write : IN STD_LOGIC;

                 -- the_sdram
                    signal zs_addr_from_the_sdram : OUT STD_LOGIC_VECTOR (11 DOWNTO 0);
                    signal zs_ba_from_the_sdram : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal zs_cas_n_from_the_sdram : OUT STD_LOGIC;
                    signal zs_cke_from_the_sdram : OUT STD_LOGIC;
                    signal zs_cs_n_from_the_sdram : OUT STD_LOGIC;
                    signal zs_dq_to_and_from_the_sdram : INOUT STD_LOGIC_VECTOR (15 DOWNTO 0);
                    signal zs_dqm_from_the_sdram : OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal zs_ras_n_from_the_sdram : OUT STD_LOGIC;
                    signal zs_we_n_from_the_sdram : OUT STD_LOGIC
                 );
end component system;

component sdram_test_component is 
           port (
                 -- inputs:
                    signal clk : IN STD_LOGIC;
                    signal zs_addr : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
                    signal zs_ba : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal zs_cas_n : IN STD_LOGIC;
                    signal zs_cke : IN STD_LOGIC;
                    signal zs_cs_n : IN STD_LOGIC;
                    signal zs_dqm : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
                    signal zs_ras_n : IN STD_LOGIC;
                    signal zs_we_n : IN STD_LOGIC;

                 -- outputs:
                    signal zs_dq : INOUT STD_LOGIC_VECTOR (15 DOWNTO 0)
                 );
end component sdram_test_component;

                signal clk :  STD_LOGIC;
                signal clk_master_read :  STD_LOGIC;
                signal clk_master_write :  STD_LOGIC;
                signal clk_sdram :  STD_LOGIC;
                signal clock_crossing_read_m1_endofpacket :  STD_LOGIC;
                signal clock_crossing_read_m1_nativeaddress :  STD_LOGIC_VECTOR (21 DOWNTO 0);
                signal clock_crossing_read_s1_endofpacket_from_sa :  STD_LOGIC;
                signal clock_crossing_read_s1_writedata :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal clock_crossing_write_m1_endofpacket :  STD_LOGIC;
                signal clock_crossing_write_m1_nativeaddress :  STD_LOGIC_VECTOR (21 DOWNTO 0);
                signal clock_crossing_write_s1_endofpacket_from_sa :  STD_LOGIC;
                signal clock_crossing_write_s1_readdata_from_sa :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal clock_crossing_write_s1_readdatavalid_from_sa :  STD_LOGIC;
                signal control_done_from_the_master_read :  STD_LOGIC;
                signal control_done_from_the_master_write :  STD_LOGIC;
                signal control_early_done_from_the_master_read :  STD_LOGIC;
                signal control_fixed_location_to_the_master_read :  STD_LOGIC;
                signal control_fixed_location_to_the_master_write :  STD_LOGIC;
                signal control_go_to_the_master_read :  STD_LOGIC;
                signal control_go_to_the_master_write :  STD_LOGIC;
                signal control_read_base_to_the_master_read :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal control_read_length_to_the_master_read :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal control_write_base_to_the_master_write :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal control_write_length_to_the_master_write :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal reset_n :  STD_LOGIC;
                signal system_burst_0_downstream_debugaccess :  STD_LOGIC;
                signal system_burst_0_downstream_nativeaddress :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal system_burst_1_downstream_debugaccess :  STD_LOGIC;
                signal system_burst_1_downstream_nativeaddress :  STD_LOGIC_VECTOR (23 DOWNTO 0);
                signal user_buffer_full_from_the_master_write :  STD_LOGIC;
                signal user_buffer_input_data_to_the_master_write :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal user_buffer_output_data_from_the_master_read :  STD_LOGIC_VECTOR (31 DOWNTO 0);
                signal user_data_available_from_the_master_read :  STD_LOGIC;
                signal user_read_buffer_to_the_master_read :  STD_LOGIC;
                signal user_write_buffer_to_the_master_write :  STD_LOGIC;
                signal zs_addr_from_the_sdram :  STD_LOGIC_VECTOR (11 DOWNTO 0);
                signal zs_ba_from_the_sdram :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal zs_cas_n_from_the_sdram :  STD_LOGIC;
                signal zs_cke_from_the_sdram :  STD_LOGIC;
                signal zs_cs_n_from_the_sdram :  STD_LOGIC;
                signal zs_dq_to_and_from_the_sdram :  STD_LOGIC_VECTOR (15 DOWNTO 0);
                signal zs_dqm_from_the_sdram :  STD_LOGIC_VECTOR (1 DOWNTO 0);
                signal zs_ras_n_from_the_sdram :  STD_LOGIC;
                signal zs_we_n_from_the_sdram :  STD_LOGIC;


-- <ALTERA_NOTE> CODE INSERTED BETWEEN HERE
--add your component and signal declaration here
-- AND HERE WILL BE PRESERVED </ALTERA_NOTE>


begin

  --Set us up the Dut
  DUT : system
    port map(
      control_done_from_the_master_read => control_done_from_the_master_read,
      control_done_from_the_master_write => control_done_from_the_master_write,
      control_early_done_from_the_master_read => control_early_done_from_the_master_read,
      user_buffer_full_from_the_master_write => user_buffer_full_from_the_master_write,
      user_buffer_output_data_from_the_master_read => user_buffer_output_data_from_the_master_read,
      user_data_available_from_the_master_read => user_data_available_from_the_master_read,
      zs_addr_from_the_sdram => zs_addr_from_the_sdram,
      zs_ba_from_the_sdram => zs_ba_from_the_sdram,
      zs_cas_n_from_the_sdram => zs_cas_n_from_the_sdram,
      zs_cke_from_the_sdram => zs_cke_from_the_sdram,
      zs_cs_n_from_the_sdram => zs_cs_n_from_the_sdram,
      zs_dq_to_and_from_the_sdram => zs_dq_to_and_from_the_sdram,
      zs_dqm_from_the_sdram => zs_dqm_from_the_sdram,
      zs_ras_n_from_the_sdram => zs_ras_n_from_the_sdram,
      zs_we_n_from_the_sdram => zs_we_n_from_the_sdram,
      clk_master_read => clk_master_read,
      clk_master_write => clk_master_write,
      clk_sdram => clk_sdram,
      control_fixed_location_to_the_master_read => control_fixed_location_to_the_master_read,
      control_fixed_location_to_the_master_write => control_fixed_location_to_the_master_write,
      control_go_to_the_master_read => control_go_to_the_master_read,
      control_go_to_the_master_write => control_go_to_the_master_write,
      control_read_base_to_the_master_read => control_read_base_to_the_master_read,
      control_read_length_to_the_master_read => control_read_length_to_the_master_read,
      control_write_base_to_the_master_write => control_write_base_to_the_master_write,
      control_write_length_to_the_master_write => control_write_length_to_the_master_write,
      reset_n => reset_n,
      user_buffer_input_data_to_the_master_write => user_buffer_input_data_to_the_master_write,
      user_read_buffer_to_the_master_read => user_read_buffer_to_the_master_read,
      user_write_buffer_to_the_master_write => user_write_buffer_to_the_master_write
    );


  --the_sdram_test_component, which is an e_instance
  the_sdram_test_component : sdram_test_component
    port map(
      zs_dq => zs_dq_to_and_from_the_sdram,
      clk => clk_sdram,
      zs_addr => zs_addr_from_the_sdram,
      zs_ba => zs_ba_from_the_sdram,
      zs_cas_n => zs_cas_n_from_the_sdram,
      zs_cke => zs_cke_from_the_sdram,
      zs_cs_n => zs_cs_n_from_the_sdram,
      zs_dqm => zs_dqm_from_the_sdram,
      zs_ras_n => zs_ras_n_from_the_sdram,
      zs_we_n => zs_we_n_from_the_sdram
    );


  process
  begin
    clk_master_read <= '0';
    loop
       if (clk_master_read = '1') then
          wait for 10 ns;
          clk_master_read <= not clk_master_read;
       else
          wait for 11 ns;
          clk_master_read <= not clk_master_read;
       end if;
    end loop;
  end process;
  process
  begin
    clk_master_write <= '0';
    loop
       if (clk_master_write = '1') then
          wait for 10 ns;
          clk_master_write <= not clk_master_write;
       else
          wait for 11 ns;
          clk_master_write <= not clk_master_write;
       end if;
    end loop;
  end process;
  process
  begin
    clk_sdram <= '0';
    loop
       wait for 5 ns;
       clk_sdram <= not clk_sdram;
    end loop;
  end process;
  PROCESS
    BEGIN
       reset_n <= '0';
       wait for 210 ns;
       reset_n <= '1'; 
    WAIT;
  END PROCESS;


-- <ALTERA_NOTE> CODE INSERTED BETWEEN HERE
--add additional architecture here
-- AND HERE WILL BE PRESERVED </ALTERA_NOTE>


end europa;



--synthesis translate_on

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
--USE ieee.std_logic_arith._integer;
USE ieee.std_logic_arith.conv_std_logic_vector;
USE work.array_ADC8CHData.all;
--  Entity Declaration

ENTITY ADC8CH IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		RSTn : IN STD_LOGIC;
		CLK1M : IN STD_LOGIC;
		CLKAUX_P : IN STD_LOGIC;
		
		--Communication with the ADC
		SCK : BUFFER STD_LOGIC;
		SDO : BUFFER STD_LOGIC;
		SDI : IN STD_LOGIC;
		CONVST_RDn : BUFFER STD_LOGIC;
		
		ADC8CH_DATA : BUFFER array_ADC8CHData;
		data_valid : OUT STD_LOGIC;

		Transfer_Request : IN STD_LOGIC;
		
		LED : BUFFER STD_LOGIC_VECTOR(6 DOWNTO 0);
		
		ADC8CH_LASER_FEEDBACK1 : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		ADC8CH_LASER_FEEDBACK2 : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		ADC8CH_EXTRA_ADC1 : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		ADC8CH_EXTRA_ADC2 : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		ADC8CH_MOTOR_FEEDBACK1 : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		ADC8CH_MOTOR_FEEDBACK2 : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		ADC8CH_PAMON_OUT1 : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		ADC8CH_PAMON_OUT2 : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0)
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END ADC8CH;


--  Architecture Body

ARCHITECTURE ADC8CH_architecture OF ADC8CH IS

	CONSTANT INIT: STD_LOGIC_VECTOR(2 DOWNTO 0) := "000";
	CONSTANT PRE_CONV: STD_LOGIC_VECTOR(2 DOWNTO 0) := "101";
	CONSTANT CONV: STD_LOGIC_VECTOR(2 DOWNTO 0) := "001";
	CONSTANT READ_ADC: STD_LOGIC_VECTOR(2 DOWNTO 0) := "010";
	CONSTANT READ_DONE: STD_LOGIC_VECTOR(2 DOWNTO 0) := "011";
	CONSTANT TRANSFER: STD_LOGIC_VECTOR(2 DOWNTO 0) := "100";
    CONSTANT DONE: STD_LOGIC_VECTOR(2 DOWNTO 0) := "111";
	
	SIGNAL state : STD_LOGIC_VECTOR(2 DOWNTO 0) := "000";
	SIGNAL COM_ID : INTEGER RANGE 0 TO 8000;		
	SIGNAL SCK_ON : STD_LOGIC;					--Communication between FPGA and ADC in progress
	
	SIGNAL ADC8CH_DATA_SIG : array_ADC8CHData;
	

BEGIN

SCK <= CLK1M WHEN SCK_ON = '1' ELSE '0';
LED <= (OTHERS => '0');
PROCESS(RSTn, CLK1M)

	CONSTANT PERIOD : INTEGER := 1000;						--1000ns
	CONSTANT BIT_QUANTITY : INTEGER := 16;					--Number of BIT per ADC
	VARIABLE TIMER : INTEGER RANGE 1 TO 55000 := PERIOD;	--Used for conversion time of the ADC
	VARIABLE BIT_NUMBER : INTEGER RANGE 1 TO 16 := 16;		--To loop on all bits of an ADC
	VARIABLE SDO_BIT_NUMBER : INTEGER RANGE 1 TO 8 := 1;	--To loop on all bits to configure the ADC
BEGIN

	IF (RSTn = '0') THEN
		state <= INIT;
		SDO <= '0';
		CONVST_RDn <= '0';
		COM_ID <= 0;
		SCK_ON <= '0';
		BIT_NUMBER := BIT_QUANTITY;
		SDO_BIT_NUMBER := 1;
		
		ADC8CH_DATA_SIG <= (OTHERS => (OTHERS => '0'));
		ADC8CH_DATA <= (OTHERS => (OTHERS => '0'));
		
		ADC8CH_LASER_FEEDBACK1 <= (OTHERS => '0');
		ADC8CH_LASER_FEEDBACK2 <= (OTHERS => '0');
		ADC8CH_EXTRA_ADC1 <= (OTHERS => '0');
		ADC8CH_EXTRA_ADC2 <= (OTHERS => '0');
		ADC8CH_MOTOR_FEEDBACK1 <= (OTHERS => '0');
		ADC8CH_MOTOR_FEEDBACK2 <= (OTHERS => '0');
		ADC8CH_PAMON_OUT1 <= (OTHERS => '0');
		ADC8CH_PAMON_OUT2 <= (OTHERS => '0');	
		
	ELSIF (CLK1M'EVENT AND CLK1M = '1') THEN
		SDO <= SDO;
		CONVST_RDn <= '0';
		SCK_ON <= '0';
		state <= state;	
		COM_ID <= COM_ID;
		ADC8CH_DATA_SIG <= ADC8CH_DATA_SIG;
		ADC8CH_DATA <= ADC8CH_DATA;
		ADC8CH_LASER_FEEDBACK1 <= ADC8CH_LASER_FEEDBACK1;
		ADC8CH_LASER_FEEDBACK2 <= ADC8CH_LASER_FEEDBACK2;
		ADC8CH_EXTRA_ADC1 <= ADC8CH_EXTRA_ADC1;
		ADC8CH_EXTRA_ADC2 <= ADC8CH_EXTRA_ADC2;
		ADC8CH_MOTOR_FEEDBACK1 <= ADC8CH_MOTOR_FEEDBACK1;
		ADC8CH_MOTOR_FEEDBACK2 <= ADC8CH_MOTOR_FEEDBACK2;
		ADC8CH_PAMON_OUT1 <= ADC8CH_PAMON_OUT1;
		ADC8CH_PAMON_OUT2 <= ADC8CH_PAMON_OUT2;			
				
		CASE state IS
			WHEN INIT =>									--Sync with CLKAUX	
				COM_ID <= 0;
				IF CLKAUX_P = '1' THEN
					CONVST_RDn <= '0';
					TIMER := PERIOD;
					state <= PRE_CONV;	
					ADC8CH_DATA_SIG <= (OTHERS => (OTHERS => '0'));	
					data_valid <= '0';			
				ELSE				
					state <= INIT;
					data_valid <= '1';
				END IF;
								
			WHEN PRE_CONV =>
				IF TIMER > 5000 THEN	--Assert conversion time is at least 5�s
					TIMER := PERIOD;
					CONVST_RDn <= '0';
					state <= CONV;			
				ELSE
					TIMER := TIMER + PERIOD;
					CONVST_RDn <= '0';
					state  <= PRE_CONV;
				END IF;
				BIT_NUMBER := BIT_QUANTITY;	
				data_valid <= '0';	
					
			WHEN CONV => 									--Start conversion for 5�s		
				
				IF TIMER > 6000 THEN	--Assert conversion time is at least 5�s
					TIMER := PERIOD;
					CONVST_RDn <= '0';
					state <= READ_ADC;	
				ELSIF TIMER > 5000 THEN
					TIMER := TIMER + PERIOD;
					CONVST_RDn <= '0';
					state  <= CONV;	
				ELSE
					TIMER := TIMER + PERIOD;
					CONVST_RDn <= '1';
					state  <= CONV;
				END IF;
				BIT_NUMBER := BIT_QUANTITY;
				data_valid <= '0';
				
			WHEN READ_ADC => 									--Read data	(bit per bit)
				SCK_ON <= '1';
				CONVST_RDn <= '0';	
				
				IF COM_ID > 0 THEN		--The first read is not valid, it is only used to configure the second read
					ADC8CH_DATA_SIG(COM_ID-1)(BIT_NUMBER-1) <= SDI;
				END IF;
				
				IF BIT_NUMBER <= 1 THEN						--Group of 16 bits done reading
					BIT_NUMBER := BIT_QUANTITY;
					state <= READ_DONE;
				ELSE
					BIT_NUMBER := BIT_NUMBER - 1;
					state <= READ_ADC;
				END IF;		
				data_valid <= '0';	

			WHEN READ_DONE =>
				--Write to RAM
				CONVST_RDn <= '0';	
								 
				IF COM_ID < 8 THEN		--Not all CH have been read
					COM_ID <= COM_ID + 1;
					state <= PRE_CONV;
					data_valid <= '0';
				ELSE					--All 8 CH have been read
					--SAMPLE <= SAMPLE + 8;
					COM_ID <= 0;
					state <= DONE;
					ADC8CH_DATA <= ADC8CH_DATA_SIG;
					
					ADC8CH_LASER_FEEDBACK1 <= ADC8CH_DATA_SIG(0);
					ADC8CH_LASER_FEEDBACK2 <= ADC8CH_DATA_SIG(1);
					ADC8CH_EXTRA_ADC1 <= ADC8CH_DATA_SIG(2);
					ADC8CH_EXTRA_ADC2 <= ADC8CH_DATA_SIG(3);
					ADC8CH_MOTOR_FEEDBACK1 <= ADC8CH_DATA_SIG(4);
					ADC8CH_MOTOR_FEEDBACK2 <= ADC8CH_DATA_SIG(5);
					ADC8CH_PAMON_OUT1 <= ADC8CH_DATA_SIG(6);
					ADC8CH_PAMON_OUT2 <= ADC8CH_DATA_SIG(7);	
							
					data_valid <= '1';
				END IF;
            
            WHEN DONE =>
                data_valid <= '1';
                IF CLKAUX_P = '0' THEN
                    state <= INIT;
                END IF;
    
			WHEN OTHERS =>
				state <= INIT;
		END CASE;
	
	--Writing to the ADC has to be done during the reading but on the falling SCK edge
	--The information sent to the ADC will be used for the next conversion	
	ELSIF (CLK1M'EVENT AND CLK1M = '0') THEN	
		IF state = READ_ADC THEN --***********************************
			IF SDO_BIT_NUMBER = 1 THEN											--Single ended input
				SDO <= '1';
				SDO_BIT_NUMBER := SDO_BIT_NUMBER + 1;
			ELSIF SDO_BIT_NUMBER = 2 THEN										--Channel select
				IF COM_ID = 1 OR COM_ID = 3 OR COM_ID = 5 OR COM_ID = 7 THEN
					SDO <= '1';
				ELSE
					SDO <= '0';
				END IF;
				SDO_BIT_NUMBER := SDO_BIT_NUMBER + 1;
			ELSIF SDO_BIT_NUMBER = 3 THEN										--Channel select
				IF COM_ID = 4 OR COM_ID = 5 OR COM_ID = 6 OR COM_ID = 7 THEN
					SDO <= '1';
				ELSE
					SDO <= '0';
				END IF;
				SDO_BIT_NUMBER := SDO_BIT_NUMBER + 1;
			ELSIF SDO_BIT_NUMBER = 4 THEN										--Channel select
				IF COM_ID = 2 OR COM_ID = 3 OR COM_ID = 6 OR COM_ID = 7 THEN
					SDO <= '1';
				ELSE
					SDO <= '0';
				END IF;
				SDO_BIT_NUMBER := SDO_BIT_NUMBER + 1;
			ELSIF SDO_BIT_NUMBER = 5 THEN										--Input Range Selection (�5V)
				SDO <= '0';
				SDO_BIT_NUMBER := SDO_BIT_NUMBER + 1;
			ELSIF SDO_BIT_NUMBER = 6 THEN										--Input Range Selection (�5V)
				SDO <= '0';
				SDO_BIT_NUMBER := SDO_BIT_NUMBER + 1;												
			ELSIF SDO_BIT_NUMBER = 7 THEN										--Power Down Selection (Power On)
				SDO <= '0';
				SDO_BIT_NUMBER := SDO_BIT_NUMBER + 1;		
			ELSIF SDO_BIT_NUMBER = 8 THEN										--Power Down Selection (Power On)
				SDO <= '0';		
				SDO_BIT_NUMBER := SDO_BIT_NUMBER;			
			END IF;	
		ELSE	
			SDO <= '0';
			SDO_BIT_NUMBER := 1;
		END IF;
	END IF;
	
END PROCESS;
				
END ADC8CH_architecture;


LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY PAUSController IS
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;										

		PALaserTrigger : IN STD_LOGIC; -- Trigger from the photoacoustic laser
        PALaserTriggerOut : OUT STD_LOGIC; -- Trigger to the photoacoustic laser
	
		--Parameters received from the computer (see Matlab code for information)
        nSamples : IN INTEGER RANGE 0 TO 4095;
		delayBetweenUltrasoundsNCycles : IN UNSIGNED(15 DOWNTO 0);
		nAveragingUltrasound : IN UNSIGNED(15 DOWNTO 0);
		nAveragingUltrasoundFlush : IN UNSIGNED(15 DOWNTO 0);
		photoacousticOn : IN STD_LOGIC;
        DPSSPhotoacousticOn : IN STD_LOGIC;	
		motorSync : IN STD_LOGIC;
        skipPAAcq : IN STD_LOGIC; --To acquire US at the same time of the PA pulse, when there is an air bubble
		nSlices : IN UNSIGNED(15 DOWNTO 0);
		nTurnsPerSlice : IN UNSIGNED(15 DOWNTO 0);
		POSITION_PER_TURN : IN INTEGER RANGE 0 TO 1024;
		speedMotorTurnsPerSec : IN INTEGER RANGE 0 TO 63;
								
		--Communication avec la RAM interne au FPGA
		dualBuffer : BUFFER STD_LOGIC; --Switch buffer (acquisition is done in a RAM while we transfer the other)
		
		--Trigger signals
		pulseEnable : OUT STD_LOGIC; --Positive ultrasound pulser
        startPA : OUT STD_LOGIC;
        startUS : OUT STD_LOGIC;
        averageEnable : OUT STD_LOGIC;
        PAUSAcquisitionDone : IN STD_LOGIC;
		
		--Communications with the FIFO transfer block
		quantityToSend : BUFFER INTEGER RANGE 0 TO 2147483647;
		transferReady : BUFFER STD_LOGIC;
		scannedPosition : BUFFER UNSIGNED(31 DOWNTO 0); --Last scanned position
		transferring : IN STD_LOGIC; --Used to verify that the last transfer is finished and the current transfer starts before continuing demodulation
		
        --Duration of the acquisition at one position of a transfer vector
		durationAcquisition : BUFFER UNSIGNED(31 DOWNTO 0);
				
		--Communication with the motor controller
		position : IN UNSIGNED(11 DOWNTO 0);
        positionAdvanced : IN UNSIGNED(11 DOWNTO 0); --Equivalent to "position", but 150µs in advance. Used to trigger the laser 100µs before we wait for the trigger return.
		positionChanged : IN STD_LOGIC;
        positionAdvancedChanged : IN STD_LOGIC;
		
		--Communication with the pullback motor
		pullbackMotorDo : BUFFER STD_LOGIC; --The acquisition module will tell this module to change slice. It will wait for the done signal
		pullbackMotorReset : OUT STD_LOGIC; --Connected to startAcq, when '0' the motor slowly go to 0.
		pullbackMotorDone : IN STD_LOGIC; --Tells the acquisition module that the pullback is done
		
		demodReady : IN STD_LOGIC; --The demodulation process is read to demodulate a new position
		enableFluo : BUFFER STD_LOGIC; --Starting the acquisition of a new position (after the photoacoustic laser pulse)
						
		--Communication with the DAC controller	
		DAC_PA : BUFFER STD_LOGIC;
		DAC_US : BUFFER STD_LOGIC;
        DAC_CutUSPulse : BUFFER STD_LOGIC;
		
		LED : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);

		error : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
        debugOut : BUFFER UNSIGNED(11 DOWNTO 0);
		syncDurationPosition : BUFFER STD_LOGIC --Used to synchronize with the module that calculates the duration of a position ('1' before the first position)
	);
END PAUSController;

--  Architecture Body 
ARCHITECTURE PAUSController_architecture OF PAUSController IS

	TYPE stateMachine IS (WAIT_PULLBACK, WAIT_PULLBACK2, ACQ_DONE, INIT, MOTORS_INIT, MOTORS_INIT2, MOTORS, TRIG_LASER0, TRIG_LASER1, PA_START_INIT, PA_START, PA_DONE, PA_END, US_START_INIT,
	US_START, US_DONE, US_END, AVERAGING_ULTRASOUND_STATE, CHANGE_POSITION, TRANSFERT_FIFO_INIT, TRANSFERT_FIFO);
	SIGNAL state : stateMachine := WAIT_PULLBACK;
	
	SIGNAL QUANTITYTOSEND_CONSTANT : INTEGER RANGE 0 TO 4095; --1350*3 samples
	
	SIGNAL firstAcq : STD_LOGIC; --Used to check if we are out of sync with the motors

	SIGNAL currentPosition : UNSIGNED(11 DOWNTO 0); --Current position we are scanning	
    SIGNAL previousPositionAdvanced : UNSIGNED(11 DOWNTO 0); --Used to detect a change in positionAdvanced
    SIGNAL previousPositionAdvancedChanged : STD_LOGIC := '0';
	SIGNAL counterDurationAcquisition : INTEGER RANGE 0 TO 10000000;
	SIGNAL counterDelayBetweenUltrasoundsNCycles : INTEGER RANGE 0 TO 65535;
	
	SIGNAL idxAveragingUltrasound : INTEGER RANGE 0 TO 1010 := 0; --Curent ultrasound averaging
	SIGNAL idxSlice : INTEGER RANGE 0 TO 1010 := 1; --Current slice
	SIGNAL idxTurnPerSlice : INTEGER RANGE 0 TO 260 := 0; --Current turn in a given slice
	
	SIGNAL stateNum : INTEGER RANGE 0 TO 50 := 8;
    
    SIGNAL nextPAPulsePosition : INTEGER RANGE 0 TO 4095 := 0; --Represents the next position where a PA acquisition will be done.
    SIGNAL isPAPulsePosition : STD_LOGIC := '0'; --Indicates that the current position has a valid PA acquisition. This is sent to Matlab.
    --To do PA acquisition at all the positions (while keeping uniformity at 20 Hz), the process sends a trigger twice at position 0, so one of them must be ignored.
    SIGNAL isPAPulsePositionNext : STD_LOGIC := '1';
    --isPAPulsePosition is set with positionAdvanced. This signal will set the value with position (not advanced), so that Matlab gets the valid PA acquisition position.
    SIGNAL isPAPulsePositionSynced : STD_LOGIC := '0';
    SIGNAL isPAPulsePosition30TurnsSec : STD_LOGIC := '0'; --At 30turns/sec, it is the same has 10 turns/sec but we skip a turn
    SIGNAL previousRSTn : STD_LOGIC := '0'; --Used to detect the end of the reset
    

BEGIN
QUANTITYTOSEND_CONSTANT <= 3*nSamples;
quantityToSend <= QUANTITYTOSEND_CONSTANT;
--debugOut <= TO_UNSIGNED(stateNum,12);
LED <= STD_LOGIC_VECTOR(TO_UNSIGNED(idxSlice,5));
    
PROCESS(RSTn, clk48MHz)

BEGIN

IF (clk48MHz'EVENT AND clk48MHz = '1') THEN
    previousPositionAdvancedChanged <= positionAdvancedChanged;

    --Code to pulse the PA laser at the right position
    IF positionAdvancedChanged = '1' AND previousPositionAdvancedChanged = '0' AND photoacousticOn = '1' THEN --Execute only when there is a change in the position.
		--TODO: Buggy at 30 turns/sec, because it is reset before the end of the acquisition of the turn
        isPAPulsePosition <= '0'; --Reset, otherwise position 1 after a pullback is detected as a PA position.
		IF positionAdvanced = nextPAPulsePosition THEN --Check if we arrived at a PA position
			isPAPulsePositionNext <= '1'; --Only one case at 0.
			isPAPulsePosition <= '1'; --Only one case per speed at 0 and when 1/2 turns at 30 turns/sec.
			PALaserTriggerOut <= '1'; --Always send trigger, because it must be triggered at exactly 20 Hz. Except 1/2 turns at 30 turns/sec.
			--Compute the next pulse position
			IF speedMotorTurnsPerSec = 3 THEN --3 is actually 2.5 rounded
				--Pulse positions, in order, are: 0 31 ... 255 30 ... 254 29 ... 253 28 ... 252 ......... 3 ... 227 2 ... 226 1 ... 225 0 32 ... 224
				IF nextPAPulsePosition = 0 THEN
					--We pass two times at position 0, but only one is valid
					IF isPAPulsePositionNext = '1' THEN
						nextPAPulsePosition <= 31;
					ELSE
						nextPAPulsePosition <= 32;
						isPAPulsePosition <= '0';
					END IF;
				ELSIF nextPAPulsePosition < 224 THEN
					nextPAPulsePosition <= nextPAPulsePosition + 32;
				ELSIF nextPAPulsePosition = 224 THEN
					nextPAPulsePosition <= 0;       
				ELSIF nextPAPulsePosition = 225 THEN
					nextPAPulsePosition <= 0;  
					isPAPulsePositionNext <= '0';
				ELSIF nextPAPulsePosition > 225 THEN
					nextPAPulsePosition <= nextPAPulsePosition - 225;
				END IF;
			ELSIF speedMotorTurnsPerSec = 5 THEN
				--Pulse positions, in order, are: 0 63 ... 255 62 ... 254 61 ... 253 60 ... 252 ......... 3 ... 195 2 ... 194 1 ... 193 0 64 ... 192
				IF nextPAPulsePosition = 0 THEN
					--We pass two times at position 0, but only one is valid
					IF isPAPulsePositionNext = '1' THEN
						nextPAPulsePosition <= 63;
					ELSE
						nextPAPulsePosition <= 64;
						isPAPulsePosition <= '0';
					END IF;
				ELSIF nextPAPulsePosition < 192 THEN
					nextPAPulsePosition <= nextPAPulsePosition + 64;
				ELSIF nextPAPulsePosition = 192 THEN
					nextPAPulsePosition <= 0;       
				ELSIF nextPAPulsePosition = 193 THEN
					nextPAPulsePosition <= 0;  
					isPAPulsePositionNext <= '0';
				ELSIF nextPAPulsePosition > 193 THEN
					nextPAPulsePosition <= nextPAPulsePosition - 193;
				END IF;				
			ELSIF speedMotorTurnsPerSec = 10 THEN
				--Pulse positions, in order, are: 0 127 ... 255 126 ... 254 125 ... 253 124 ... 252 ......... 3 ... 131 2 ... 130 1 ... 129 0 64 ... 128
				IF nextPAPulsePosition = 0 THEN
					--We pass two times at position 0, but only one is valid
					IF isPAPulsePositionNext = '1' THEN
						nextPAPulsePosition <= 127;
					ELSE
						nextPAPulsePosition <= 128;
						isPAPulsePosition <= '0';
					END IF;
				ELSIF nextPAPulsePosition < 128 THEN
					nextPAPulsePosition <= nextPAPulsePosition + 128;
				ELSIF nextPAPulsePosition = 128 THEN
					nextPAPulsePosition <= 0;       
				ELSIF nextPAPulsePosition = 129 THEN
					nextPAPulsePosition <= 0;  
					isPAPulsePositionNext <= '0';
				ELSIF nextPAPulsePosition > 129 THEN
					nextPAPulsePosition <= nextPAPulsePosition - 129;
				END IF;					
			ELSIF speedMotorTurnsPerSec = 20 THEN
				--Pulse positions, in order, are: 0 255 254 253 3 2 1 0
				IF nextPAPulsePosition = 0 THEN
					--We pass two times at position 0, but only one is valid
					IF isPAPulsePositionNext = '1' THEN
						nextPAPulsePosition <= 255;
					ELSE
						nextPAPulsePosition <= 0;
						isPAPulsePosition <= '0';
					END IF;
				ELSIF nextPAPulsePosition < 256 THEN
					nextPAPulsePosition <= nextPAPulsePosition - 1;
				END IF;				
			ELSIF speedMotorTurnsPerSec = 30 THEN
                IF isPAPulsePosition30TurnsSec = '0' THEN --Skip one of two turns
                    isPAPulsePosition30TurnsSec <= '1';
                    --Pulse positions, in order, are: 0 127 ... 255 126 ... 254 125 ... 253 124 ... 252 ......... 3 ... 131 2 ... 130 1 ... 129 0 (not valid) 128
                    IF nextPAPulsePosition = 0 THEN
                        --We pass two times at position 0, but only one is valid
                        IF isPAPulsePositionNext = '1' THEN
                            nextPAPulsePosition <= 127;
                        ELSE
                            nextPAPulsePosition <= 128;
                            isPAPulsePosition <= '0';
                        END IF;
                    ELSIF nextPAPulsePosition < 128 THEN
                        nextPAPulsePosition <= nextPAPulsePosition + 128;
                    ELSIF nextPAPulsePosition = 128 THEN
                        nextPAPulsePosition <= 0;       
                    ELSIF nextPAPulsePosition = 129 THEN
                        nextPAPulsePosition <= 0;  
                        isPAPulsePositionNext <= '0';
                    ELSIF nextPAPulsePosition > 129 THEN
                        nextPAPulsePosition <= nextPAPulsePosition - 129;
                    END IF;
                ELSE
                    isPAPulsePosition30TurnsSec <= '0'; --Skipped
                    isPAPulsePosition <= '0'; --Only one case at 0.
                    PALaserTriggerOut <= '0'; --Always send trigger, because it must be triggered at exactly 20 Hz                    
                END IF;
            END IF;	            
		ELSE    
			PALaserTriggerOut <= '0';
		END IF;  
    ELSIF DPSSPhotoacousticOn = '1' THEN
        --For compatibility with the green DPSS laser, we still send a trigger when photoacoustic is off.
        IF state = TRIG_LASER0 THEN
            PALaserTriggerOut <= '1';
        ELSE
            PALaserTriggerOut <= '0';
        END IF;        
    END IF;
    
    --When the acquisition starts, we force the next PA pulse to be at position 0.
    previousRSTn <= RSTn;
    IF previousRSTn = '0' AND RSTn = '1' THEN
        nextPAPulsePosition <= 0;
        isPAPulsePosition <= '0';
        isPAPulsePositionNext <= '1';
    END IF;
        
    
IF (RSTn = '0') THEN
	 debugOut <= (OTHERS => '0');	
	state <= WAIT_PULLBACK2; --Allow the pullback to move to the initial position
	idxAveragingUltrasound <= 0;
	transferReady <= '0';
	DAC_PA <= '0'; DAC_US <= '0'; DAC_CutUSPulse <= '0';
	currentPosition <= (OTHERS => '0');	
	scannedPosition <= (OTHERS => '0');	

	enableFluo <= '0';
	dualBuffer <= '0';
	firstAcq <= '1';
	counterDurationAcquisition <= 0;
	durationAcquisition <= (OTHERS => '0');			
	syncDurationPosition <= '1';
	error <= (OTHERS => '0');
	pullbackMotorReset <= '1';
	pullbackMotorDo <= '0';
	idxSlice <= 1; --Set at WAIT_PULLBACK, but we start at WAIT_PULLBACK2
	idxTurnPerSlice <= 0;
    stateNum <= 8;
    
    startPA <= '0';
    startUS <= '0';
    averageEnable <= '0';
    pulseEnable <= '0';
    counterDelayBetweenUltrasoundsNCycles <= 0;
	--LED(6 DOWNTO 0) <= (OTHERS => '0');
ELSE    
	DAC_PA <= '0'; DAC_US <= '0'; DAC_CutUSPulse <= '0';
	transferReady <= '0';
	enableFluo <= '0';
	counterDurationAcquisition <= counterDurationAcquisition + 1;
	syncDurationPosition <= '0';
	error <= (OTHERS => '0');
	pullbackMotorReset <= '0';
	pullbackMotorDo <= '0';
    stateNum <= 31;
    
    startPA <= '0';
    startUS <= '0';
    averageEnable <= '0';
    pulseEnable <= '0';
    
    dualBuffer <= dualBuffer;
    scannedPosition <= scannedPosition;
    durationAcquisition <= durationAcquisition;
    state <= state;
    firstAcq <= firstAcq;
    currentPosition <= currentPosition;
    counterDelayBetweenUltrasoundsNCycles <= counterDelayBetweenUltrasoundsNCycles;
    idxAveragingUltrasound <= idxAveragingUltrasound;
    idxSlice <= idxSlice;
    idxTurnPerSlice <= idxTurnPerSlice;
    previousPositionAdvanced <= positionAdvanced;
        
	CASE state IS
	
		WHEN WAIT_PULLBACK => --Trigger pullback. Called the first time after startAcq and at the end of each turn
			stateNum <= 1;
			IF idxTurnPerSlice >= nTurnsPerSlice THEN
				IF idxSlice >= nSlices THEN
					state <= ACQ_DONE;
				ELSE
					pullbackMotorDo <= '1';
					IF pullbackMotorDo = '1' THEN
                        idxTurnPerSlice <= 0; --Reset just before changing state. Bug corrected, otherwise it would go to INIT not waiting for the pullback.
                        idxSlice <= idxSlice + 1;
						state <= WAIT_PULLBACK2; --2 cycles to make sure the pullback module detected the signal
					ELSE
						state <= WAIT_PULLBACK;
					END IF;
				END IF;
			ELSE
				state <= INIT;
                firstAcq <= '1';
			END IF;
		
		WHEN WAIT_PULLBACK2 => --Waiting for the end of the pullback
			stateNum <= 2;
			pullbackMotorDo <= '1';
			IF pullbackMotorDone = '1' THEN
				state <= INIT;
                firstAcq <= '1';
            ELSE
				state <= WAIT_PULLBACK2;
			END IF;
			
		WHEN ACQ_DONE =>
			stateNum <= 18;
			state <= ACQ_DONE;
			pullbackMotorReset <= '1';
			
		WHEN INIT => --Wait for a position change. Used because the position module is at a different frequency and we need to sync with it
			stateNum <= 4;

			state <= MOTORS_INIT;
				
			--Skip this state to not use the motors
			IF motorSync = '0' THEN
				state <= MOTORS_INIT;					
			END IF;
						
		WHEN MOTORS_INIT =>					--Synchronization with the motors (position before the one we are interested)
			stateNum <= 5;
			IF motorSync = '1' THEN
                --Position qui précède la position à analyser															
                IF (currentPosition > 0 AND position = currentPosition-1) OR (currentPosition = 0 AND position = POSITION_PER_TURN-1) THEN
                    IF demodReady = '1' THEN
                        state <= MOTORS_INIT2;
                    ELSE
						state <= MOTORS_INIT;
                    END IF;
                ELSIF firstAcq = '0' THEN --One turn with no acquisition, because the motor is too fast or the acquisition is too slow (except at the first acquisition after moving the pullback motors)
                    --BUG: Maybe go back to init if firstAcq???
                    IF demodReady = '1' THEN
                        error(0) <= '1';
                    ELSE
                        error(3) <= '1'; --Caused by the demodulation module
                    END IF;
                    state <= MOTORS_INIT;
                    isPAPulsePosition <= '0';     
                END IF;
			ELSE
				--Skip this state to not use the motors
				--We still have to wait for the demodulation process
				IF demodReady = '1' THEN
					state <= MOTORS_INIT2;
				ELSE
					state <= MOTORS_INIT;
				END IF;							
			END IF;
		
		WHEN MOTORS_INIT2 => --Wait for a position change. Used because the position module is at a different frequency and we need to sync to it
			stateNum <= 6;

			state <= MOTORS;
            
			--Skip this state to not use the motors
			IF motorSync = '0' THEN
				state <= MOTORS;				
			END IF;
						
		WHEN MOTORS => --Synchronization with the motor (at the position we are interested)	
			stateNum <= 7;	
			firstAcq <= '0';
            IF positionChanged = '1' THEN --Position is valid only when trigger positionChanged is received.
                IF position = currentPosition THEN
                    state <= TRIG_LASER0;
                ELSIF (currentPosition > 0 AND position = currentPosition-1) OR (currentPosition = 0 AND position = POSITION_PER_TURN-1) THEN
                    --This is the position before the one we want to analyze
                    state <= MOTORS;
                ELSE
                    --We are somewhere else, out of sync with the motors... very bad
                    --It can also mean that the motors are rotating in the wrong direction
                    error(4) <= '1';
                    state <= MOTORS_INIT;
                    --IF Laser_T_Delai = '0' THEN Laser_T_OUT <= '1'; END IF;
                END IF;
            ELSE
				state <= MOTORS;
            END IF;
			--Skip this state to not use the motors
			IF motorSync = '0' THEN
				state <= TRIG_LASER0;
				error(4) <= '0'; --Not an error		
			END IF;
			--Reinitialize the counter to calculer the duration of the acquisition at a given position
			counterDurationAcquisition <= 0;
									
		WHEN TRIG_LASER0 =>	--Waiting for the PA laser signal
			stateNum <= 8;
			--IF Laser_T_Delai = '1' THEN Laser_T_OUT <= '1'; END IF;
            IF DPSSPhotoacousticOn = '1' AND motorSync = '0' THEN --Mode for Meunier's laser without rotation, pulse at every position
                IF PALaserTrigger = '0' THEN --Pulse at rising edge
                    state <= TRIG_LASER1;
                END IF;
			ELSIF photoacousticOn = '1' THEN
                IF (PALaserTrigger = '1' AND isPAPulsePosition = '1') OR isPAPulsePosition = '0' THEN
                    IF isPAPulsePosition = '1' THEN
                        --counterDurationAcquisition <= counterDurationAcquisition - 1000;
                    END IF;
                    state <= TRIG_LASER1;
                ELSE
                    state <= TRIG_LASER0;
                END IF;
            ELSE
                state <= TRIG_LASER1;
            END IF;

		WHEN TRIG_LASER1 => --Waiting for the PA laser signal (falling edge)
            --Check if this is a valid PA acquisition position.
			stateNum <= 9;
			--IF Laser_T_Delai = '1' THEN Laser_T_OUT <= '1'; END IF;			
			IF photoacousticOn = '1' THEN
                IF (PALaserTrigger = '0' AND isPAPulsePosition = '1') OR isPAPulsePosition = '0' THEN
                    IF isPAPulsePosition = '1' THEN
                        --counterDurationAcquisition <= counterDurationAcquisition - 1000;
                    END IF;            
                    state <= PA_START_INIT;
                    isPAPulsePositionSynced <= isPAPulsePosition;
                    isPAPulsePosition <= '0';                
                ELSE
                    state <= TRIG_LASER1;
                END IF;	
            ELSIF DPSSPhotoacousticOn = '1' AND motorSync = '0' THEN --Mode for Meunier's laser without rotation, pulse at every position
                IF PALaserTrigger = '1' THEN --Pulse at rising edge
                    state <= PA_START_INIT;
                END IF;                
            ELSIF DPSSPhotoacousticOn = '1' THEN
                IF PALaserTrigger = '0' THEN --Inverted logic, 0 when the laser has pulsed
                    state <= PA_START_INIT;
                END IF;
            ELSE
                state <= PA_START_INIT;
            END IF;                
                
			counterDelayBetweenUltrasoundsNCycles <= 0;

		WHEN PA_START_INIT =>
            
            averageEnable <= '0';
			stateNum <= 29;
			enableFluo <= '1';
			DAC_PA <= '1';
			counterDelayBetweenUltrasoundsNCycles <= 0;
            IF PAUSAcquisitionDone = '1' THEN
                IF skipPAAcq = '0' THEN
                    startPA <= '1';
                    state <= PA_START;
                ELSE --To acquire US at the same time of the PA pulse, when there is an air bubble
                    startPA <= '0';
                    state <= PA_END;
                END IF;
            ELSE
				state <= PA_START_INIT;
            END IF;
			
		WHEN PA_START =>
            startPA <= '1';
            averageEnable <= '0';
            enableFluo <= '1';
			stateNum <= 10;
			DAC_PA <= '1';
			counterDelayBetweenUltrasoundsNCycles <= counterDelayBetweenUltrasoundsNCycles + 1;
            IF PAUSAcquisitionDone = '0' THEN
                state <= PA_DONE;
            ELSE
				state <= PA_START;
            END IF;
			
		WHEN PA_DONE => --Reading a group of samples
			stateNum <= 11;
			DAC_PA <= '1';
            enableFluo <= '1';
			counterDelayBetweenUltrasoundsNCycles <= counterDelayBetweenUltrasoundsNCycles + 1;
            IF PAUSAcquisitionDone = '1' THEN
                state <= PA_END;
            ELSE
				state <= PA_DONE;
            END IF; 
            --Reset TGC gain to attenuate the next US pulse (2.2 µs before)
            IF (counterDelayBetweenUltrasoundsNCycles >= delayBetweenUltrasoundsNCycles-105) THEN	
                DAC_CutUSPulse <= '1';
            END IF;
			
		WHEN PA_END => --End of PA acquisition with a delay
			stateNum <= 12;
            enableFluo <= '1';
			IF (counterDelayBetweenUltrasoundsNCycles >= delayBetweenUltrasoundsNCycles) THEN		
				state <= US_START_INIT;
				counterDelayBetweenUltrasoundsNCycles <= 0;
			ELSE
				counterDelayBetweenUltrasoundsNCycles <= counterDelayBetweenUltrasoundsNCycles + 1;
				state <= PA_END;
			END IF;       

     	WHEN US_START_INIT =>
            
			stateNum <= 30;
			counterDelayBetweenUltrasoundsNCycles <= 0;
            IF PAUSAcquisitionDone = '1' THEN
                startUS <= '1';
                pulseEnable <= '1';
                --Average only if it is not the first acquisition line
                IF idxAveragingUltrasound > 0 AND idxAveragingUltrasound > nAveragingUltrasoundFlush THEN
                    averageEnable <= '1';
                END IF;
                state <= US_START;
            ELSE
				state <= US_START_INIT;
            END IF;
            
		WHEN US_START => --Reading a group of samples
            startUS <= '1';
            pulseEnable <= '1';
            --Average only if it is not the first acquisition line
            IF idxAveragingUltrasound > 0 AND idxAveragingUltrasound > nAveragingUltrasoundFlush THEN
                averageEnable <= '1';
            END IF;
            
			stateNum <= 19;
			DAC_US <= '1';
			counterDelayBetweenUltrasoundsNCycles <= counterDelayBetweenUltrasoundsNCycles + 1;
            IF PAUSAcquisitionDone = '0' THEN
                state <= US_DONE;
            ELSE
				state <= US_START;
            END IF;                
            
		WHEN US_DONE => --Reading a group of samples
			stateNum <= 12;
			DAC_US <= '1';
			counterDelayBetweenUltrasoundsNCycles <= counterDelayBetweenUltrasoundsNCycles + 1;
            IF PAUSAcquisitionDone = '1' THEN
                state <= US_END;
            ELSE
				state <= US_DONE;
            END IF;  
            --Reset TGC gain to attenuate the next US pulse (2.2 µs before)
            IF (counterDelayBetweenUltrasoundsNCycles >= delayBetweenUltrasoundsNCycles-105) THEN	
                DAC_CutUSPulse <= '1';
            END IF;               
			
		WHEN US_END => --End of US acquisition with a delay	
			stateNum <= 20;				
			IF (counterDelayBetweenUltrasoundsNCycles >= delayBetweenUltrasoundsNCycles) THEN	
                counterDelayBetweenUltrasoundsNCycles <= 0;
                idxAveragingUltrasound <= idxAveragingUltrasound + 1;
				state <= AVERAGING_ULTRASOUND_STATE;
			ELSE
				counterDelayBetweenUltrasoundsNCycles <= counterDelayBetweenUltrasoundsNCycles + 1;
				state <= US_END;
			END IF;	           
			
		WHEN AVERAGING_ULTRASOUND_STATE => --Calculate the ultrasound averaging	
			stateNum <= 22;				
			IF (idxAveragingUltrasound >= nAveragingUltrasound) THEN --Averaging done
				state <= CHANGE_POSITION;
			ELSE
				state <= US_START_INIT;
                averageEnable <= '1';
			END IF;
						
		WHEN CHANGE_POSITION =>	--When an acquisition on a single position is done, we change position
			stateNum <= 25;
			idxAveragingUltrasound <= 0;

            state <= TRANSFERT_FIFO_INIT;
            error <= (OTHERS => '0');				
			
			--Acquisition is done at a given position. We store the number of cycles it took.
			counterDurationAcquisition <= counterDurationAcquisition;
			durationAcquisition <= TO_UNSIGNED(counterDurationAcquisition,32);
			
		WHEN TRANSFERT_FIFO_INIT =>  --Make sure the last transfer is done
			stateNum <= 26;
            
			IF transferring = '0' THEN	
                transferReady <= '1';
				syncDurationPosition <= '1';					
				state <= TRANSFERT_FIFO;
				dualBuffer <= NOT(dualBuffer); --Switch buffer
				scannedPosition <= "00000000000000000000" & currentPosition; --Position just finished and that will be transferred
                IF photoacousticOn = '1' THEN
                    scannedPosition(30) <= isPAPulsePositionSynced; --Informs Matlab that the scannedPosition includes a valid PA acquisition
                ELSE
                    scannedPosition(30) <= isPAPulsePositionSynced;
                END IF;
                isPAPulsePositionSynced <= '0';
				IF UNSIGNED(currentPosition) < POSITION_PER_TURN-1 THEN
					currentPosition <= currentPosition + 1;
				ELSE
					currentPosition <= currentPosition + 1 - POSITION_PER_TURN;
				END IF;							
			ELSE
				error(1) <= '1'; --Transfert précédent pas terminé
				state <= TRANSFERT_FIFO_INIT;
			END IF;
							
		WHEN TRANSFERT_FIFO =>							--Lecture d'une position terminée et envoi des résultats au FIFO	
			stateNum <= 27;	
			transferReady <= '1';					
			IF transferring = '1' THEN --attente que le transfert à l'ordinateur commence.
				syncDurationPosition <= '1';
				IF currentPosition = "000000000000" THEN --End of a turn
					idxTurnPerSlice <= idxTurnPerSlice + 1;                   
					state <= WAIT_PULLBACK;
					debugOut <= debugOut + 1;
				ELSE
					state <= MOTORS_INIT;--****************************************	
				END IF;
			ELSE
				state <= TRANSFERT_FIFO;
			END IF;	
														
		WHEN OTHERS =>
			stateNum <= 28;
			state <= WAIT_PULLBACK;					
	END CASE;	
END IF;
END IF;
END PROCESS;

END PAUSController_architecture;

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--USE ieee.std_logic_arith.conv_integer;
USE ieee.std_logic_arith.conv_std_logic_vector;

--  Entity Declaration

ENTITY LCD_Display IS

	PORT
	(	
		rst_n	  :in std_logic;
		CLK_SPI : IN STD_LOGIC;
		SDO : OUT STD_LOGIC;
		SS : OUT STD_LOGIC;
        CLK_OUT : OUT STD_LOGIC;
		GO 				: in  STD_LOGIC;
		DONE 			: out  STD_LOGIC;
		Done_init						: out  STD_LOGIC;
      ACK 			: out  STD_LOGIC;
		VAR1, VAR2, VAR3, VAR4 		: IN INTEGER RANGE 0 TO 999;
		Donnee : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
	
	
END LCD_Display;


ARCHITECTURE behavioral OF LCD_Display IS

type etat_t is (START, AFFICHE_0, AFFICHE_1, AFFICHE_2, AFFICHE_3, AFFICHE_4, AFFICHE_5, AFFICHE_6, AFFICHE_7,
					TRANSITION_0, TRANSITION_1, TRANSITION_2, TRANSITION_3, TRANSITION_4, TRANSITION_13, 
					TRANSITION_5, TRANSITION_6, TRANSITION_7, TRANSITION_8, TRANSITION_9, 
					TRANSITION_10, TRANSITION_11, TRANSITION_12,LCD_CLEAR_0, LCD_CLEAR_1, 
					LCD_CLEAR_2, LCD_CLEAR_3, LCD_CLEAR_4, LCD_CLEAR_5, LCD_CLEAR_6, 
					LCD_CLEAR_7, LCD_CLEAR_8, LCD_CLEAR_9, LCD_CLEAR_10, LCD_CLEAR_11, 
					LCD_CLEAR_12, LCD_CLEAR_13,LCD_CLEAR_14,LCD_CLEAR_15, REGISTRE1, REGISTRE2,
					SET_CONTRAST_0, SET_CONTRAST_1, SET_CONTRAST_2, SET_CONTRAST_3, SET_CONTRAST_4, 
					SET_CONTRAST_5, SET_CONTRAST_6, SET_CONTRAST_7, SET_CONTRAST_8, SET_CONTRAST_9,
					SET_CONTRAST_10,SET_CONTRAST_11,SET_CONTRAST_12,SET_CONTRAST_13,SET_CONTRAST_14,
					SET_CONTRAST_15,SET_CONTRAST_16,SET_CONTRAST_17,SET_CONTRAST_18,SET_CONTRAST_19, 
					SET_CONTRAST_20, SET_CONTRAST_21, SET_CONTRAST_22, SET_CONTRAST_23, 
					SET_BACKLIGHT_0, SET_BACKLIGHT_1, SET_BACKLIGHT_2, SET_BACKLIGHT_3, SET_BACKLIGHT_4,
					SET_BACKLIGHT_5, SET_BACKLIGHT_6, SET_BACKLIGHT_7, SET_BACKLIGHT_8, SET_BACKLIGHT_9,
					SET_BACKLIGHT_10, SET_BACKLIGHT_11, SET_BACKLIGHT_12, SET_BACKLIGHT_13, SET_BACKLIGHT_14,
					SET_BACKLIGHT_15, SET_BACKLIGHT_16,SET_BACKLIGHT_17, SET_BACKLIGHT_18, SET_BACKLIGHT_19, 
					SET_BACKLIGHT_20, SET_BACKLIGHT_21,SET_BACKLIGHT_22, SET_BACKLIGHT_23, RAFFRAICHISSEMENT,
	            POSITION_0, POSITION_1, POSITION_2, POSITION_3, POSITION_4, POSITION_5, 
					POSITION_6, POSITION_7,POSITION_01, POSITION_11, POSITION_21, POSITION_31, 
					POSITION_41, POSITION_51, POSITION_61, POSITION_71,MOT_UN_0, MOT_UN_1, 
					MOT_UN_2, MOT_UN_3, MOT_UN_4, MOT_UN_5, MOT_UN_6, MOT_UN_7, MOT_UN_8,
					VAR_UN_0, VAR_UN_1, VAR_UN_2, VAR_UN_3, VAR_UN_4, VAR_UN_5, VAR_UN_6, 
					VAR_UN_7, VAR_UN_8, FIN, FIN2, VERIF_RAFFRAICH, VERIF_RAFFRAICH2, COMPT_RAFFRAICH)
					;


signal etat 		: etat_t;          -- l'�tat pr�sent
signal etat_next 	: etat_t;  			 -- l'�tat futur

--D�CLARATION DES SIGNAUX INTERNES

signal SDO_in, SDO_in_next	  			:std_logic;
signal contrast					  		:std_logic_vector(7 downto 0);
signal clear_value						:std_logic_vector(7 downto 0);
signal light_value					   :std_logic_vector(7 downto 0);
signal SS_in,SS_in_next						:std_logic;
signal done_in ,done_in_next 					:std_logic;
signal init ,init_next 					:std_logic;
signal ack_in,ack_in_next 					:std_logic;
signal count,count_next 					:integer := 0;
signal count_raf,count_raf_next 		:integer := 0;
signal compt, compt_next 					:integer := 0;
signal position, position_next 					:integer;
signal num_nombre, num_nombre_next 						:integer;

type tableau3  is array (0 to 1) of std_logic_vector (7 downto 0);
constant	ligne0 : tableau3 := (
						X"FE",X"45"
						);

type tableau4  is array (0 to 3) of std_logic_vector (7 downto 0);
constant	ligne1 : tableau4 := (
						X"11",X"51",X"25",X"65"
						);

----------------- TABLEAUX POUR CONVERSION D'ENTIER EN ASCII ----------------
	
type tableau6  is array (0 to 2) of std_logic_vector (7 downto 0);		
signal nbr1 : tableau6; 
signal nbr2 : tableau6; 
signal nbr3 : tableau6; 
signal nbr4 : tableau6; 
		
signal donnee_reg, donnee_reg_next : tableau6;
type tableau9  is array (0 to 99) of tableau6;
signal dizaine : tableau9 ;


type tableau7  is array (0 to 3) of tableau6;	
signal nombre : tableau7 ;



begin

--ASSIGNATION DES SORTIES DE "ENTITY"

SDO <= SDO_in;
SS <= SS_in;
CLK_OUT <= CLK_SPI;
DONE <= done_in;
Done_init <= init;
ACK <= ack_in;


------ASSIGNATION DU TABLEAU POUR CONVERSION D'ENTIER EN ASCII -------

dizaine(0) <= (X"30",X"A0",X"A0");  dizaine(1) <= (X"31",X"A0",X"A0");
dizaine(2) <= (X"32",X"A0",X"A0");  dizaine(3) <= (X"33",X"A0",X"A0");
dizaine(4) <= (X"34",X"A0",X"A0");  dizaine(5) <= (X"35",X"A0",X"A0");
dizaine(6) <= (X"36",X"A0",X"A0");  dizaine(7) <= (X"37",X"A0",X"A0");
dizaine(8) <= (X"38",X"A0",X"A0");  dizaine(9) <= (X"39",X"A0",X"A0");
dizaine(10) <= (X"31",X"30",X"A0"); dizaine(11) <= (X"31",X"31",X"A0");
dizaine(12) <= (X"31",X"32",X"A0"); dizaine(13) <= (X"31",X"33",X"A0");
dizaine(14) <= (X"31",X"34",X"A0"); dizaine(15) <= (X"31",X"35",X"A0");
dizaine(16) <= (X"31",X"36",X"A0"); dizaine(17) <= (X"31",X"37",X"A0");
dizaine(18) <= (X"31",X"38",X"A0"); dizaine(19) <= (X"31",X"39",X"A0");
dizaine(20) <= (X"32",X"30",X"A0"); dizaine(21) <= (X"32",X"31",X"A0");
dizaine(22) <= (X"32",X"32",X"A0"); dizaine(23) <= (X"32",X"33",X"A0");
dizaine(24) <= (X"32",X"34",X"A0"); dizaine(25) <= (X"32",X"35",X"A0");
dizaine(26) <= (X"32",X"36",X"A0"); dizaine(27) <= (X"32",X"37",X"A0");
dizaine(28) <= (X"32",X"38",X"A0"); dizaine(29) <= (X"32",X"39",X"A0");
dizaine(30) <= (X"33",X"30",X"A0"); dizaine(31) <= (X"33",X"31",X"A0");
dizaine(32) <= (X"33",X"32",X"A0"); dizaine(33) <= (X"33",X"33",X"A0");
dizaine(34) <= (X"33",X"34",X"A0"); dizaine(35) <= (X"33",X"35",X"A0");
dizaine(36) <= (X"33",X"36",X"A0"); dizaine(37) <= (X"33",X"37",X"A0");
dizaine(38) <= (X"33",X"38",X"A0"); dizaine(39) <= (X"33",X"39",X"A0");
dizaine(40) <= (X"34",X"30",X"A0"); dizaine(41) <= (X"34",X"31",X"A0");
dizaine(42) <= (X"34",X"32",X"A0"); dizaine(43) <= (X"34",X"33",X"A0");
dizaine(44) <= (X"34",X"34",X"A0"); dizaine(45) <= (X"34",X"35",X"A0");
dizaine(46) <= (X"34",X"36",X"A0"); dizaine(47) <= (X"34",X"37",X"A0");
dizaine(48) <= (X"34",X"38",X"A0"); dizaine(49) <= (X"34",X"39",X"A0");
dizaine(50) <= (X"35",X"30",X"A0"); dizaine(51) <= (X"35",X"31",X"A0");
dizaine(52) <= (X"35",X"32",X"A0"); dizaine(53) <= (X"35",X"33",X"A0");
dizaine(54) <= (X"35",X"34",X"A0"); dizaine(55) <= (X"35",X"35",X"A0");
dizaine(56) <= (X"35",X"36",X"A0"); dizaine(57) <= (X"35",X"37",X"A0");
dizaine(58) <= (X"35",X"38",X"A0"); dizaine(59) <= (X"35",X"39",X"A0");
dizaine(60) <= (X"36",X"30",X"A0"); dizaine(61) <= (X"36",X"31",X"A0");
dizaine(62) <= (X"36",X"32",X"A0"); dizaine(63) <= (X"36",X"33",X"A0");
dizaine(64) <= (X"36",X"34",X"A0"); dizaine(65) <= (X"36",X"35",X"A0");
dizaine(66) <= (X"36",X"36",X"A0"); dizaine(67) <= (X"36",X"37",X"A0");
dizaine(68) <= (X"36",X"38",X"A0"); dizaine(69) <= (X"36",X"39",X"A0");
dizaine(70) <= (X"37",X"30",X"A0"); dizaine(71) <= (X"37",X"31",X"A0");
dizaine(72) <= (X"37",X"32",X"A0"); dizaine(73) <= (X"37",X"33",X"A0");
dizaine(74) <= (X"37",X"34",X"A0"); dizaine(75) <= (X"37",X"35",X"A0");
dizaine(76) <= (X"37",X"36",X"A0"); dizaine(77) <= (X"37",X"37",X"A0");
dizaine(78) <= (X"37",X"38",X"A0"); dizaine(79) <= (X"37",X"39",X"A0");
dizaine(80) <= (X"38",X"30",X"A0"); dizaine(81) <= (X"38",X"31",X"A0");
dizaine(82) <= (X"38",X"32",X"A0"); dizaine(83) <= (X"38",X"33",X"A0");
dizaine(84) <= (X"38",X"34",X"A0"); dizaine(85) <= (X"38",X"35",X"A0");
dizaine(86) <= (X"38",X"36",X"A0"); dizaine(87) <= (X"38",X"37",X"A0");
dizaine(88) <= (X"38",X"38",X"A0"); dizaine(89) <= (X"38",X"39",X"A0");
dizaine(90) <= (X"39",X"30",X"A0"); dizaine(91) <= (X"39",X"31",X"A0");
dizaine(92) <= (X"39",X"32",X"A0"); dizaine(93) <= (X"39",X"33",X"A0");
dizaine(94) <= (X"39",X"34",X"A0"); dizaine(95) <= (X"39",X"35",X"A0");
dizaine(96) <= (X"39",X"36",X"A0"); dizaine(97) <= (X"39",X"37",X"A0");
dizaine(98) <= (X"39",X"38",X"A0"); dizaine(99) <= (X"39",X"39",X"A0");


--***********************************************************
-- 					Conversion variable1 
--***********************************************************

VARIABLE1 :process(CLK_SPI, rst_n, VAR1, dizaine)
begin
if rst_n <= '0' then
	nbr1(2) <= (others =>'0');
	nbr1(1) <= (others =>'0');
	nbr1(0) <= (others =>'0');
elsif(CLK_SPI'EVENT and CLK_SPI = '1')then	
	if (VAR1 < 100) then
		nbr1(2) <= dizaine(VAR1)(2);
		nbr1(1) <= dizaine(VAR1)(1);
		nbr1(0) <= dizaine(VAR1)(0);
		
	elsif (VAR1 >= 100 and VAR1 < 200) then
		if (VAR1-100) <10 then
			nbr1(1) <= X"30";
			nbr1(2) <= dizaine(VAR1 - 100)(0);
		else
			nbr1(2) <= dizaine(VAR1 - 100)(1);
			nbr1(1) <= dizaine(VAR1 - 100)(0);
		end if;
		nbr1(0) <= X"31";
		
	elsif (VAR1 >= 200 and VAR1 < 300) then
 		if (VAR1-200) <10 then
			nbr1(1) <= X"30";
			nbr1(2) <= dizaine(VAR1 - 200)(0);
		else
			nbr1(2) <= dizaine(VAR1 - 200)(1);
			nbr1(1) <= dizaine(VAR1 - 200)(0);
		end if;
		nbr1(0) <= X"32";
		
	elsif (VAR1 >= 300 and VAR1 < 400) then
 		if (VAR1-300) <10 then
			nbr1(1) <= X"30";
			nbr1(2) <= dizaine(VAR1 - 300)(0);
		else
			nbr1(2) <= dizaine(VAR1 - 300)(1);
			nbr1(1) <= dizaine(VAR1 - 300)(0);
		end if;
		nbr1(0) <= X"33";
		
	elsif (VAR1 >= 400 and VAR1 < 500) then
 		if (VAR1-400) <10 then
			nbr1(1) <= X"30";
			nbr1(2) <= dizaine(VAR1 - 400)(0);
		else
			nbr1(2) <= dizaine(VAR1 - 400)(1);
			nbr1(1) <= dizaine(VAR1 - 400)(0);
		end if;
		nbr1(0) <= X"33";
		
	elsif (VAR1 >= 500 and VAR1 < 600) then
 		if (VAR1-500) <10 then
			nbr1(1) <= X"30";
			nbr1(2) <= dizaine(VAR1 - 500)(0);
		else
			nbr1(2) <= dizaine(VAR1 - 500)(1);
			nbr1(1) <= dizaine(VAR1 - 500)(0);
		end if;
		nbr1(0) <= X"34";
		
	elsif (VAR1 >= 600 and VAR1 < 700) then
 		if (VAR1-600) <10 then
			nbr1(1) <= X"30";
			nbr1(2) <= dizaine(VAR1 - 600)(0);
		else
			nbr1(2) <= dizaine(VAR1 - 600)(1);
			nbr1(1) <= dizaine(VAR1 - 600)(0);
		end if;
		nbr1(0) <= X"35";
		
	elsif (VAR1 >= 700 and VAR1 < 800) then
 		if (VAR1-700) <10 then
			nbr1(1) <= X"30";
			nbr1(2) <= dizaine(VAR1 - 700)(0);
		else
			nbr1(2) <= dizaine(VAR1 - 700)(1);
			nbr1(1) <= dizaine(VAR1 - 700)(0);
		end if;
		nbr1(0) <= X"36";
		
	elsif (VAR1 >= 800 and VAR1 < 900) then
 		if (VAR1-800) <10 then
			nbr1(1) <= X"30";
			nbr1(2) <= dizaine(VAR1 - 800)(0);
		else
			nbr1(2) <= dizaine(VAR1 - 800)(1);
			nbr1(1) <= dizaine(VAR1 - 800)(0);
		end if;
		nbr1(0) <= X"38";
		
	elsif (VAR1 >= 900 and VAR1 < 1000) then
 		if (VAR1-900) <10 then
			nbr1(1) <= X"30";
			nbr1(2) <= dizaine(VAR1 - 900)(0);
		else
			nbr1(2) <= dizaine(VAR1 - 900)(1);
			nbr1(1) <= dizaine(VAR1 - 900)(0);
		end if;
		nbr1(0) <= X"39";
	end if ;
end if;

end process VARIABLE1;

--***********************************************************
-- 					Conversion variable2 
--***********************************************************

VARIABLE2 :process(CLK_SPI, rst_n, VAR2, dizaine)
begin
if rst_n <= '0' then
	nbr2(2) <= (others =>'0');
	nbr2(1) <= (others =>'0');
	nbr2(0) <= (others =>'0');
elsif(CLK_SPI'EVENT and CLK_SPI = '1')then	
	if (VAR2 < 100) then
		nbr2(2) <= dizaine(VAR2)(2);
		nbr2(1) <= dizaine(VAR2)(1);
		nbr2(0) <= dizaine(VAR2)(0);
		
	elsif (VAR2 >= 100 and VAR2 < 200) then
		if (VAR2-100) <10 then
			nbr2(1) <= X"30";
			nbr2(2) <= dizaine(VAR2 - 100)(0);
		else
			nbr2(2) <= dizaine(VAR2 - 100)(1);
			nbr2(1) <= dizaine(VAR2 - 100)(0);
		end if;
		nbr2(0) <= X"31";
		
	elsif (VAR2 >= 200 and VAR2 < 300) then
 		if (VAR2-200) <10 then
			nbr2(1) <= X"30";
			nbr2(2) <= dizaine(VAR2 - 200)(0);
		else
			nbr2(2) <= dizaine(VAR2 - 200)(1);
			nbr2(1) <= dizaine(VAR2 - 200)(0);
		end if;
		nbr2(0) <= X"32";
		
	elsif (VAR2 >= 300 and VAR2 < 400) then
 		if (VAR2-300) <10 then
			nbr2(1) <= X"30";
			nbr2(2) <= dizaine(VAR2 - 300)(0);
		else
			nbr2(2) <= dizaine(VAR2 - 300)(1);
			nbr2(1) <= dizaine(VAR2 - 300)(0);
		end if;
		nbr2(0) <= X"33";
		
	elsif (VAR2 >= 400 and VAR2 < 500) then
 		if (VAR2-400) <10 then
			nbr2(1) <= X"30";
			nbr2(2) <= dizaine(VAR2 - 400)(0);
		else
			nbr2(2) <= dizaine(VAR2 - 400)(1);
			nbr2(1) <= dizaine(VAR2 - 400)(0);
		end if;
		nbr2(0) <= X"34";
		
	elsif (VAR2 >= 500 and VAR2 < 600) then
 		if (VAR2-500) <10 then
			nbr2(1) <= X"30";
			nbr2(2) <= dizaine(VAR2 - 500)(0);
		else
			nbr2(2) <= dizaine(VAR2 - 500)(1);
			nbr2(1) <= dizaine(VAR2 - 500)(0);
		end if;
		nbr2(0) <= X"35";
		
	elsif (VAR2 >= 600 and VAR2 < 700) then
 		if (VAR2-600) <10 then
			nbr2(1) <= X"30";
			nbr2(2) <= dizaine(VAR2 - 600)(0);
		else
			nbr2(2) <= dizaine(VAR2 - 600)(1);
			nbr2(1) <= dizaine(VAR2 - 600)(0);
		end if;
		nbr2(0) <= X"36";
		
	elsif (VAR2 >= 700 and VAR2 < 800) then
 		if (VAR2-700) <10 then
			nbr2(1) <= X"30";
			nbr2(2) <= dizaine(VAR2 - 700)(0);
		else
			nbr2(2) <= dizaine(VAR2 - 700)(1);
			nbr2(1) <= dizaine(VAR2 - 700)(0);
		end if;
		nbr2(0) <= X"37";
		
	elsif (VAR2 >= 800 and VAR2 < 900) then
 		if (VAR2-800) <10 then
			nbr2(1) <= X"30";
			nbr2(2) <= dizaine(VAR2 - 800)(0);
		else
			nbr2(2) <= dizaine(VAR2 - 800)(1);
			nbr2(1) <= dizaine(VAR2 - 800)(0);
		end if;
		nbr2(0) <= X"38";
		
	elsif (VAR2 >= 900 and VAR2 < 1000) then
 		if (VAR2-900) <10 then
			nbr2(1) <= X"30";
			nbr2(2) <= dizaine(VAR2 - 900)(0);
		else
			nbr2(2) <= dizaine(VAR2 - 900)(1);
			nbr2(1) <= dizaine(VAR2 - 900)(0);
		end if;
		nbr2(0) <= X"39";
	end if ;
end if;

end process VARIABLE2;

--***********************************************************
-- 					Conversion variable3 
--***********************************************************

VARIABLE3 :process(CLK_SPI, rst_n, VAR3, dizaine)
begin
if rst_n <= '0' then
	nbr3(2) <= (others =>'0');
	nbr3(1) <= (others =>'0');
	nbr3(0) <= (others =>'0');
elsif(CLK_SPI'EVENT and CLK_SPI = '1')then	
	if (VAR3 < 100) then
		nbr3(2) <= dizaine(VAR3)(2);
		nbr3(1) <= dizaine(VAR3)(1);
		nbr3(0) <= dizaine(VAR3)(0);
		
	elsif (VAR3 >= 100 and VAR3 < 200) then
		if (VAR3-100) <10 then
			nbr3(1) <= X"30";
			nbr3(2) <= dizaine(VAR3 - 100)(0);
		else
			nbr3(2) <= dizaine(VAR3 - 100)(1);
			nbr3(1) <= dizaine(VAR3 - 100)(0);
		end if;
		nbr3(0) <= X"31";
		
	elsif (VAR3 >= 200 and VAR3 < 300) then
 		if (VAR3-200) <10 then
			nbr3(1) <= X"30";
			nbr3(2) <= dizaine(VAR3 - 200)(0);
		else
			nbr3(2) <= dizaine(VAR3 - 200)(1);
			nbr3(1) <= dizaine(VAR3 - 200)(0);
		end if;
		nbr3(0) <= X"32";
		
	elsif (VAR3 >= 300 and VAR3 < 400) then
 		if (VAR3-300) <10 then
			nbr3(1) <= X"30";
			nbr3(2) <= dizaine(VAR3 - 300)(0);
		else
			nbr3(2) <= dizaine(VAR3 - 300)(1);
			nbr3(1) <= dizaine(VAR3 - 300)(0);
		end if;
		nbr3(0) <= X"33";
		
	elsif (VAR3 >= 400 and VAR3 < 500) then
 		if (VAR3-400) <10 then
			nbr3(1) <= X"30";
			nbr3(2) <= dizaine(VAR3 - 400)(0);
		else
			nbr3(2) <= dizaine(VAR3 - 400)(1);
			nbr3(1) <= dizaine(VAR3 - 400)(0);
		end if;
		nbr3(0) <= X"33";
		
	elsif (VAR3 >= 500 and VAR3 < 600) then
 		if (VAR3-500) <10 then
			nbr3(1) <= X"30";
			nbr3(2) <= dizaine(VAR3 - 500)(0);
		else
			nbr3(2) <= dizaine(VAR3 - 500)(1);
			nbr3(1) <= dizaine(VAR3 - 500)(0);
		end if;
		nbr3(0) <= X"34";
		
	elsif (VAR3 >= 600 and VAR3 < 700) then
 		if (VAR3-600) <10 then
			nbr3(1) <= X"30";
			nbr3(2) <= dizaine(VAR3 - 600)(0);
		else
			nbr3(2) <= dizaine(VAR3 - 600)(1);
			nbr3(1) <= dizaine(VAR3 - 600)(0);
		end if;
		nbr3(0) <= X"35";
		
	elsif (VAR3 >= 700 and VAR3 < 800) then
 		if (VAR3-700) <10 then
			nbr3(1) <= X"30";
			nbr3(2) <= dizaine(VAR3 - 700)(0);
		else
			nbr3(2) <= dizaine(VAR3 - 700)(1);
			nbr3(1) <= dizaine(VAR3 - 700)(0);
		end if;
		nbr3(0) <= X"36";
		
	elsif (VAR3 >= 800 and VAR3 < 900) then
 		if (VAR3-800) <10 then
			nbr3(1) <= X"30";
			nbr3(2) <= dizaine(VAR3 - 800)(0);
		else
			nbr3(2) <= dizaine(VAR3 - 800)(1);
			nbr3(1) <= dizaine(VAR3 - 800)(0);
		end if;
		nbr3(0) <= X"38";
		
	elsif (VAR3 >= 900 and VAR3 < 1000) then
 		if (VAR3-900) <10 then
			nbr3(1) <= X"30";
			nbr3(2) <= dizaine(VAR3 - 900)(0);
		else
			nbr3(2) <= dizaine(VAR3 - 900)(1);
			nbr3(1) <= dizaine(VAR3 - 900)(0);
		end if;
		nbr3(0) <= X"39";
	end if ;
end if;

end process VARIABLE3;

--***********************************************************
-- 					Conversion variable4 
--***********************************************************

VARIABLE4 :process(CLK_SPI, rst_n, VAR4, dizaine)
begin
if rst_n <= '0' then
	nbr4(2) <= (others =>'0');
	nbr4(1) <= (others =>'0');
	nbr4(0) <= (others =>'0');
elsif(CLK_SPI'EVENT and CLK_SPI = '1')then	
	if (VAR4 < 100) then
		nbr4(2) <= dizaine(VAR4)(2);
		nbr4(1) <= dizaine(VAR4)(1);
		nbr4(0) <= dizaine(VAR4)(0);
		
	elsif (VAR4 >= 100 and VAR4 < 200) then
		if (VAR4-100) <10 then
			nbr4(1) <= X"30";
			nbr4(2) <= dizaine(VAR4 - 100)(0);
		else
			nbr4(2) <= dizaine(VAR4 - 100)(1);
			nbr4(1) <= dizaine(VAR4 - 100)(0);
		end if;
		nbr4(0) <= X"31";
		
	elsif (VAR4 >= 200 and VAR4 < 300) then
 		if (VAR4-200) <10 then
			nbr4(1) <= X"30";
			nbr4(2) <= dizaine(VAR4 - 200)(0);
		else
			nbr4(2) <= dizaine(VAR4 - 200)(1);
			nbr4(1) <= dizaine(VAR4 - 200)(0);
		end if;
		nbr4(0) <= X"32";
		
	elsif (VAR4 >= 300 and VAR4 < 400) then
 		if (VAR4-300) <10 then
			nbr4(1) <= X"30";
			nbr4(2) <= dizaine(VAR4 - 300)(0);
		else
			nbr4(2) <= dizaine(VAR4 - 300)(1);
			nbr4(1) <= dizaine(VAR4 - 300)(0);
		end if;
		nbr4(0) <= X"33";
		
	elsif (VAR4 >= 400 and VAR4 < 500) then
 		if (VAR4-400) <10 then
			nbr4(1) <= X"30";
			nbr4(2) <= dizaine(VAR4 - 400)(0);
		else
			nbr4(2) <= dizaine(VAR4 - 400)(1);
			nbr4(1) <= dizaine(VAR4 - 400)(0);
		end if;
		nbr4(0) <= X"34";
		
	elsif (VAR4 >= 500 and VAR4 < 600) then
 		if (VAR4-500) <10 then
			nbr4(1) <= X"30";
			nbr4(2) <= dizaine(VAR4 - 500)(0);
		else
			nbr4(2) <= dizaine(VAR4 - 500)(1);
			nbr4(1) <= dizaine(VAR4 - 500)(0);
		end if;
		nbr4(0) <= X"35";
		
	elsif (VAR4 >= 600 and VAR4 < 700) then
 		if (VAR4-600) <10 then
			nbr4(1) <= X"30";
			nbr4(2) <= dizaine(VAR4 - 600)(0);
		else
			nbr4(2) <= dizaine(VAR4 - 600)(1);
			nbr4(1) <= dizaine(VAR4 - 600)(0);
		end if;
		nbr4(0) <= X"36";
		
	elsif (VAR4 >= 700 and VAR4 < 800) then
 		if (VAR4-700) <10 then
			nbr4(1) <= X"30";
			nbr4(2) <= dizaine(VAR4 - 700)(0);
		else
			nbr4(2) <= dizaine(VAR4 - 700)(1);
			nbr4(1) <= dizaine(VAR4 - 700)(0);
		end if;
		nbr4(0) <= X"37";
		
	elsif (VAR4 >= 800 and VAR4 < 900) then
 		if (VAR4-800) <10 then
			nbr4(1) <= X"30";
			nbr4(2) <= dizaine(VAR4 - 800)(0);
		else
			nbr4(2) <= dizaine(VAR4 - 800)(1);
			nbr4(1) <= dizaine(VAR4 - 800)(0);
		end if;
		nbr4(0) <= X"38";
		
	elsif (VAR4 >= 900 and VAR4 < 1000) then
 		if (VAR4-900) <10 then
			nbr4(1) <= X"30";
			nbr4(2) <= dizaine(VAR4 - 900)(0);
		else
			nbr4(2) <= dizaine(VAR4 - 900)(1);
			nbr4(1) <= dizaine(VAR4 - 900)(0);
		end if;
		nbr4(0) <= X"39";
	end if ;
end if;

end process VARIABLE4;


XREG:process(CLK_SPI, rst_n)
begin
	if(rst_n = '0')then   							-- reset asynchrone logique neg
	
-- VALEURS INITIALES AU RESET DOIT �TRE FAIT POUR TOUS LES REGISTRES

		etat <= START;
		SS_in <= '1';
   	SDO_in <= '1';	
		done_in <= '0';
		ack_in <= '0';
   	count <= 0;
		compt <= 0;
		count_raf <= 0;
		num_nombre <= 0;
		position <= 0;
		init <= '0';
		donnee_reg <=(others =>X"00");
	
	elsif(CLK_SPI'EVENT and CLK_SPI = '0')then		-- front descendant de l'horloge
		etat <= etat_next;
		SDO_in <= SDO_in_next;
		SS_in <= SS_in_next;
		done_in <= done_in_next;
		ack_in <= ack_in_next;
		count <= count_next;
		compt <= compt_next;
		init <= init_next;
		count_raf <= count_raf_next;
		num_nombre <= num_nombre_next;
		position <= position_next;
		donnee_reg <= donnee_reg_next;

	end if;
end process XREG;

LCD_Display: PROCESS(etat, contrast, clear_value,light_value, Donnee, GO, count, SDO_in_next, SS_in_next,count_raf,  nombre, 
					compt,num_nombre, position, donnee_reg_next,donnee_reg, nbr1, nbr2, nbr3, nbr4 )


BEGIN


nombre(0)<= nbr1;
nombre(1)<= nbr2;
nombre(2)<= nbr3;
nombre(3)<= nbr4;
done_in_next <= '0';
ack_in_next <= '0';
contrast <= (others =>'0');
light_value <= (others =>'0');
clear_value <= (others =>'0');
count_next <= 0;
count_raf_next <= 0;
compt_next <= 0;
init_next <= '0';
donnee_reg_next <= donnee_reg;
num_nombre_next <= num_nombre;
position_next <= position;


		CASE etat IS
		
			WHEN START =>
				SS_in_next <= '1';
				SDO_in_next <= clear_value(7);
				etat_next <= LCD_CLEAR_0;				
				clear_value <= X"FE";
							
----*******************************************
--				-- EFFACAGE DE L'ECRAN
----*******************************************
				
			WHEN LCD_CLEAR_0 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(7);
				etat_next <= LCD_CLEAR_1;					
				clear_value <= X"FE";
	
	
			WHEN LCD_CLEAR_1 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(6);
				etat_next <= LCD_CLEAR_2;	
				clear_value <= X"FE";

				
			WHEN LCD_CLEAR_2 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(5);
				etat_next <= LCD_CLEAR_3;
				clear_value <= X"FE";
	
	
			WHEN LCD_CLEAR_3 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(4);
				etat_next <= LCD_CLEAR_4;
				clear_value <= X"FE";

				
			WHEN LCD_CLEAR_4 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(3);
				etat_next <= LCD_CLEAR_5;
				clear_value <= X"FE";

				
			WHEN LCD_CLEAR_5 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(2);
				etat_next <= LCD_CLEAR_6;
				clear_value <= X"FE";

				
			WHEN LCD_CLEAR_6 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(1);
				etat_next <= LCD_CLEAR_7;
				clear_value <= X"FE";

				
			WHEN LCD_CLEAR_7 =>
				etat_next <= TRANSITION_0;
				SS_in_next <= '0';
				SDO_in_next <= clear_value(0);
				clear_value <= X"FE";

				
			WHEN TRANSITION_0 =>
				  SS_in_next <= '1';
				  SDO_in_next <= clear_value(7);
				  etat_next <= LCD_CLEAR_8;
			     clear_value <= X"FE";

			
			WHEN LCD_CLEAR_8 =>
				  SS_in_next <= '0';
				  SDO_in_next <= clear_value(7);
				  etat_next <= LCD_CLEAR_9;
				  clear_value <= X"51";

				
			WHEN LCD_CLEAR_9 =>
				  SS_in_next <= '0';	
				  SDO_in_next <= clear_value(6);
				  etat_next <= LCD_CLEAR_10;
				  clear_value <= X"51";

				
			WHEN LCD_CLEAR_10 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(5);
				etat_next <= LCD_CLEAR_11;					
				clear_value <= X"51";

				
			WHEN LCD_CLEAR_11 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(4);
				etat_next <= LCD_CLEAR_12;	
				clear_value <= X"51";

				
			WHEN LCD_CLEAR_12 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(3);
				etat_next <= LCD_CLEAR_13;
				clear_value <= X"51";

				
			WHEN LCD_CLEAR_13 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(2);
				etat_next <= LCD_CLEAR_14;
				clear_value <= X"51";
	
				
			WHEN LCD_CLEAR_14 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(1);
				etat_next <= LCD_CLEAR_15;
				clear_value <= X"51";

				
			WHEN LCD_CLEAR_15 =>
				SS_in_next <= '0';
				SDO_in_next <= clear_value(0);
				etat_next <= TRANSITION_1;
				clear_value <= X"51";
				
				
----*******************************************
--				-- REGLAGE DU CONTRAST 
----*******************************************
				
			WHEN TRANSITION_1 =>
			
			if(count < 105)then                		--TANT QUE LA VALEUR DU COMPTEUR "count" EST INF�RIEUR � 105
				count_next <= count +1;         		-- ON affecte a "count_next" LA VALEUR DE "count" +1
				etat_next <= TRANSITION_1;
			else
				count_next <= 0;	                  --REMISE � Z�RO DU COMPTEUR "count"
				etat_next <= SET_CONTRAST_0;
			end if;
			   SS_in_next <= '1';
				SDO_in_next <= contrast(7);
				contrast <= X"51";	
				
				
			WHEN SET_CONTRAST_0 =>        
				SS_in_next <= '0';
				SDO_in_next <= contrast(7);				
				etat_next <= SET_CONTRAST_1;
				contrast <= X"FE";	

				
			WHEN SET_CONTRAST_1 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(6);
				etat_next <= SET_CONTRAST_2;	
				contrast <= X"FE";	

				
			WHEN SET_CONTRAST_2 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(5);
				etat_next <= SET_CONTRAST_3;
				contrast <= X"FE";	

				
			WHEN SET_CONTRAST_3 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(4);
				etat_next <= SET_CONTRAST_4;
				contrast <= X"FE";
				
				
			WHEN SET_CONTRAST_4 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(3);
				etat_next <= SET_CONTRAST_5;
				contrast <= X"FE";
				
				
			WHEN SET_CONTRAST_5 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(2);
				etat_next <= SET_CONTRAST_6;
				contrast <= X"FE";
				
				
			WHEN SET_CONTRAST_6 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(1);
				etat_next <= SET_CONTRAST_7;
				contrast <= X"FE";
				
				
			WHEN SET_CONTRAST_7 =>
				etat_next <= TRANSITION_2;   
				SS_in_next <= '0';
				SDO_in_next <= contrast(0);
				contrast <= X"FE";
				
				
			WHEN TRANSITION_2 =>
				etat_next <= SET_CONTRAST_8;
			   SS_in_next <= '1';
				SDO_in_next <= contrast(7);
				contrast <= X"FE";
				
				
			WHEN SET_CONTRAST_8 =>        
				SS_in_next <= '0';
				SDO_in_next <= contrast(7);				
				etat_next <= SET_CONTRAST_9;
				contrast <= X"52";					
				
				
			WHEN SET_CONTRAST_9 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(6);
				etat_next <= SET_CONTRAST_10;	
				contrast <= X"52";
				
				
			WHEN SET_CONTRAST_10 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(5);
				etat_next <= SET_CONTRAST_11;
				contrast <= X"52";
				
				
			WHEN SET_CONTRAST_11 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(4);
				etat_next <= SET_CONTRAST_12;
				contrast <= X"52";
				
				
			WHEN SET_CONTRAST_12 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(3);
				etat_next <= SET_CONTRAST_13;
				contrast <= X"52";
				
				
			WHEN SET_CONTRAST_13 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(2);
				etat_next <= SET_CONTRAST_14;
				contrast <= X"52";
				
				
			WHEN SET_CONTRAST_14 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(1);
				etat_next <= SET_CONTRAST_15;
				contrast <= X"52";
				
				
			WHEN SET_CONTRAST_15 =>
				etat_next <= TRANSITION_3;   
				SS_in_next <= '0';
				SDO_in_next <= contrast(0);
				contrast <= X"52";
				
				
			WHEN TRANSITION_3 =>
				  SS_in_next <= '1';
				  SDO_in_next <= contrast(7);
				  etat_next <= SET_CONTRAST_16;
			     contrast <= X"20";
				
				
			WHEN SET_CONTRAST_16 =>
				  SS_in_next <= '0';
				  SDO_in_next <= contrast(7);
				  etat_next <= SET_CONTRAST_17;
			     contrast <= X"20";

				
			WHEN SET_CONTRAST_17 =>
				  etat_next <= SET_CONTRAST_18;
				  SS_in_next <= '0';
				  SDO_in_next <= contrast(6);
			     contrast <= X"20";
				
				
			WHEN SET_CONTRAST_18 =>
				  SS_in_next <= '0';
				  SDO_in_next <= contrast(5);
				  etat_next <= SET_CONTRAST_19;
			     contrast <= X"20";
				
			WHEN SET_CONTRAST_19 =>
				  SS_in_next <= '0';	
				  SDO_in_next <= contrast(4);
				  etat_next <= SET_CONTRAST_20;
			     contrast <= X"20";
											
										
			WHEN SET_CONTRAST_20 =>
				  SS_in_next <= '0';
				  SDO_in_next <= contrast(3);
				  etat_next <= SET_CONTRAST_21;
			     contrast <= X"20";
				  
				
			WHEN SET_CONTRAST_21 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(2);
				etat_next <= SET_CONTRAST_22;
			   contrast <= X"20";
	
				
			WHEN SET_CONTRAST_22 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(1);
				etat_next <= SET_CONTRAST_23;
			   contrast <= X"20";

				
			WHEN SET_CONTRAST_23 =>
				SS_in_next <= '0';
				SDO_in_next <= contrast(0);
				etat_next <= TRANSITION_4;
			   contrast <= X"20";
				
				
			WHEN TRANSITION_4 =>
				etat_next <= SET_BACKLIGHT_0;
			   SS_in_next <= '1';
				SDO_in_next <= light_value(7);
				light_value <= X"FE";
	
	
--*******************************************
--				-- REGLAGE DE l'ECLAIRAGE 
--*******************************************

										
			WHEN SET_BACKLIGHT_0 =>
				SS_in_next <= '0';	
				SDO_in_next <= light_value(7);
			   etat_next <= SET_BACKLIGHT_1;
				light_value <= X"FE";
			  	
				
			WHEN SET_BACKLIGHT_1 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(6);
				etat_next <= SET_BACKLIGHT_2;	
				light_value <= X"FE";
				
				
			WHEN SET_BACKLIGHT_2 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(5);
				etat_next <= SET_BACKLIGHT_3;
				light_value <= X"FE";
				
				
			WHEN SET_BACKLIGHT_3 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(4);
				etat_next <= SET_BACKLIGHT_4;
				light_value <= X"FE";
				
				
			WHEN SET_BACKLIGHT_4 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(3);
				etat_next <= SET_BACKLIGHT_5;
				light_value <= X"FE";
				
				
			WHEN SET_BACKLIGHT_5 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(2);
				etat_next <= SET_BACKLIGHT_6;
				light_value <= X"FE";
				
				
			WHEN SET_BACKLIGHT_6 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(1);
				etat_next <= SET_BACKLIGHT_7;
				light_value <= X"FE";
				
				
			WHEN SET_BACKLIGHT_7 =>
				etat_next <= TRANSITION_5;   
				SS_in_next <= '0';
				SDO_in_next <= light_value(0);
				light_value <= X"FE";
				
				
			WHEN TRANSITION_5 =>
				etat_next <= SET_BACKLIGHT_8;
			   SS_in_next <= '1';
				SDO_in_next <= light_value(7);
				light_value <= X"FE";
	
										
			WHEN SET_BACKLIGHT_8 =>
				SS_in_next <= '0';	
				SDO_in_next <= light_value(7);
			   etat_next <= SET_BACKLIGHT_9;
			   light_value <= X"53";
			  	
				
			WHEN SET_BACKLIGHT_9 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(6);
				etat_next <= SET_BACKLIGHT_10;	
			   light_value <= X"53";
				
				
			WHEN SET_BACKLIGHT_10 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(5);
				etat_next <= SET_BACKLIGHT_11;
			   light_value <= X"53";
				
				
			WHEN SET_BACKLIGHT_11 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(4);
				etat_next <= SET_BACKLIGHT_12;
			   light_value <= X"53";
				
				
			WHEN SET_BACKLIGHT_12 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(3);
				etat_next <= SET_BACKLIGHT_13;
			   light_value <= X"53";
				
				
			WHEN SET_BACKLIGHT_13 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(2);
				etat_next <= SET_BACKLIGHT_14;
			   light_value <= X"53";

				
			WHEN SET_BACKLIGHT_14 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(1);
				etat_next <= SET_BACKLIGHT_15;
			   light_value <= X"53";
				
				
			WHEN SET_BACKLIGHT_15 =>
				etat_next <= TRANSITION_6;   
				SS_in_next <= '0';
				SDO_in_next <= light_value(0);
			   light_value <= X"53";
				
			WHEN TRANSITION_6 =>
				etat_next <= SET_BACKLIGHT_16;
			   SS_in_next <= '1';
				SDO_in_next <= light_value(7);
			   light_value <= X"02";
	
										
			WHEN SET_BACKLIGHT_16 =>
				SS_in_next <= '0';	
				SDO_in_next <= light_value(7);
			   etat_next <= SET_BACKLIGHT_17;
			   light_value <= X"02";
			  	
				
			WHEN SET_BACKLIGHT_17 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(6);
				etat_next <= SET_BACKLIGHT_18;	
			   light_value <= X"02";
				
				
			WHEN SET_BACKLIGHT_18 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(5);
				etat_next <= SET_BACKLIGHT_19;
			   light_value <= X"02";
				
				
			WHEN SET_BACKLIGHT_19 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(4);
				etat_next <= SET_BACKLIGHT_20;
			   light_value <= X"02";
				
				
			WHEN SET_BACKLIGHT_20 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(3);
				etat_next <= SET_BACKLIGHT_21;
			   light_value <= X"02";
				
				
			WHEN SET_BACKLIGHT_21 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(2);
				etat_next <= SET_BACKLIGHT_22;
			   light_value <= X"02";
				
				
			WHEN SET_BACKLIGHT_22 =>
				SS_in_next <= '0';
				SDO_in_next <= light_value(1);
				etat_next <= SET_BACKLIGHT_23;
			   light_value <= X"02";
				
				
			WHEN SET_BACKLIGHT_23 =>
				etat_next <= TRANSITION_7;  
				SS_in_next <= '0';
				SDO_in_next <= light_value(0);
			   light_value <= X"02";				


			WHEN TRANSITION_7 =>
			
           if (GO = '0')then                    -- GO: signal indiquant au contr�leur LCD que donnee est valide
				 etat_next  <= TRANSITION_7;
				 done_in_next <= '1'; 
				 count_next <= count; 
			 elsif (count=3) then
				 etat_next  <= MOT_UN_1;
				 done_in_next <= '0'; 
				 count_next <= 0;		 
			 else
				etat_next  <= POSITION_0;
				done_in_next <= '1'; 
				count_next <= count; 
			 end if;			
			   SS_in_next <= '1';
				SDO_in_next <= Donnee(7);
				count_raf_next <= count_raf;                       -- done: Signal indiquant la fin d'une op�ration sur le LCDtion du LCD


----*********************************************************
--				-- POSITIONNEMENT DU CURSEUR POUR LES MOTS
----*********************************************************
--
--										
			WHEN POSITION_0 =>
				SS_in_next <= '0';	
				SDO_in_next <= Donnee(7);
			   etat_next <= POSITION_1;
				count_next <= count; 
				count_raf_next <= count_raf;
			  	
				
			WHEN POSITION_1 =>
				SS_in_next <= '0';
				SDO_in_next <= Donnee(6);
				etat_next <= POSITION_2;	
				count_next <= count;
				count_raf_next <= count_raf;

				
			WHEN POSITION_2 =>
				SS_in_next <= '0';
				SDO_in_next <= Donnee(5);
				etat_next <= POSITION_3;
				count_next <= count;
				count_raf_next <= count_raf;

				
			WHEN POSITION_3 =>
				SS_in_next <= '0';
				SDO_in_next <= Donnee(4);
				etat_next <= POSITION_4;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN POSITION_4 =>
				SS_in_next <= '0';
				SDO_in_next <= Donnee(3);
				etat_next <= POSITION_5;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN POSITION_5 =>
				SS_in_next <= '0';
				SDO_in_next <= Donnee(2);
				etat_next <= POSITION_6;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN POSITION_6 =>
				SS_in_next <= '0';
				SDO_in_next <= Donnee(1);
				etat_next <= POSITION_7;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN POSITION_7 =>
				etat_next <= TRANSITION_8;   
				SS_in_next <= '0';
				SDO_in_next <= Donnee(0);
				count_next <= count; 
				count_raf_next <= count_raf;
		
				
			WHEN TRANSITION_8 =>
				etat_next <= TRANSITION_7;
				count_next <= count +1; 
				count_raf_next <= count_raf;
			   SS_in_next <= '1';
				SDO_in_next <= '0';
				ack_in_next <= '1';                   -- ack: Signal indiquant que le signal GO a �t� bien re�u

--------------------------------------------------
--   AFFICHAGE DES MOTS						
--------------------------------------------------				
										

			WHEN MOT_UN_1 =>
           if (GO = '0')then 
				etat_next  <= MOT_UN_1;
				SS_in_next <= '1';
				done_in_next <= '0'; 
			 else
				 etat_next  <= MOT_UN_2;
				 SS_in_next <= '0';
				 done_in_next <= '1'; 
			 end if;	 
				SDO_in_next <= Donnee(7);
				count_next <= count; 
				count_raf_next <= count_raf;
			  	
				
			WHEN MOT_UN_2 =>
				SS_in_next <= '0';
				SDO_in_next <= Donnee(6);
				etat_next <= MOT_UN_3;	
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN MOT_UN_3 =>
				SS_in_next <= '0';
				SDO_in_next <= Donnee(5);
				etat_next <= MOT_UN_4;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN MOT_UN_4 =>
				SS_in_next <= '0';
				SDO_in_next <= Donnee(4);
				etat_next <= MOT_UN_5;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN MOT_UN_5 =>
				SS_in_next <= '0';
				SDO_in_next <= Donnee(3);
				etat_next <= MOT_UN_6;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN MOT_UN_6 =>
				SS_in_next <= '0';
				SDO_in_next <= Donnee(2);
				etat_next <= MOT_UN_7;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN MOT_UN_7 =>
				SS_in_next <= '0';
				SDO_in_next <= Donnee(1);
				etat_next <= MOT_UN_8;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN MOT_UN_8 =>
				etat_next <= TRANSITION_9;   
				SS_in_next <= '0';
				SDO_in_next <= Donnee(0);
				count_next <= count; 
				count_raf_next <= count_raf;
				

			WHEN TRANSITION_9 =>
		    if (count = 16) then
				 etat_next  <= REGISTRE1;
				 count_next <= 0;
			 else
				 etat_next  <= MOT_UN_1;
				 count_next <= count + 1;  
			 end if;	 
			   SS_in_next <= '1';
				SDO_in_next <= '0';
				ack_in_next <= '1';
				count_raf_next <= count_raf;
				
				
				
--==============================================
--      REGISTRAGE DE LA VALEUR  D'AFFICHAGE
--==============================================

				
				
			WHEN REGISTRE1 =>
--				donnee_reg_next <= nombre(position);
donnee_reg_next <= nombre(count_raf);
				etat_next <= AFFICHE_0;
				SS_in_next <= '1';
				SDO_in_next <= '0';
				count_next <= count; 
				count_raf_next <= count_raf;
				
				
--==============================================
--      DEBUT AFFICHAGE
--==============================================

			WHEN AFFICHE_0 =>
				SS_in_next <= '0';	
				SDO_in_next <= donnee_reg_next(count)(7);
			   etat_next <= AFFICHE_1;
			  	count_next <= count; 
				count_raf_next <= count_raf;
				
				
			WHEN AFFICHE_1 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(6);
				etat_next <= AFFICHE_2;	
				count_next <= count; 
				count_raf_next <= count_raf;
				
				
			WHEN AFFICHE_2 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(5);
				etat_next <= AFFICHE_3;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN AFFICHE_3 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(4);
				etat_next <= AFFICHE_4;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN AFFICHE_4 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(3);
				etat_next <= AFFICHE_5;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN AFFICHE_5 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(2);
				etat_next <= AFFICHE_6;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN AFFICHE_6 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(1);
				etat_next <= AFFICHE_7;
				count_next <= count; 
				count_raf_next <= count_raf;
				
				
				
			WHEN AFFICHE_7 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(0);
				etat_next <= TRANSITION_10;
				count_next <= count; 
				count_raf_next <= count_raf;
				
				
				
			WHEN TRANSITION_10 =>
			 if (count = 2) then
				 etat_next  <= VERIF_RAFFRAICH;
				 count_next <= 0;		 
			 else
				etat_next  <= AFFICHE_0;
				count_next <= count + 1; 
			 end if;			
			   SS_in_next <= '1';
				SDO_in_next <= donnee_reg_next(count)(0);
				count_raf_next <= count_raf;              
				
--==============================================
--      VERIFICATION DU RAFFRAICHISSEMENT
--==============================================

				
			WHEN VERIF_RAFFRAICH =>
			if (count_raf = 3) then
				count_raf_next <= 0;       					-- rafraichissement a 5Hz 
				etat_next  <= RAFFRAICHISSEMENT; 
				count_next <= 0;
			 else
				etat_next  <= TRANSITION_7; 
				count_next <= 0;	
				count_raf_next <= count_raf + 1;
			 end if;			
			   SS_in_next <= '1';
				SDO_in_next <= '0';
				done_in_next <= '0';
				
---------------------------------------------------------
--      DEBUT  DE RAFFRAICHISSEMENT
---------------------------------------------------------

			WHEN RAFFRAICHISSEMENT =>
			if (compt <= 1000) then
				etat_next  <= RAFFRAICHISSEMENT; 
				compt_next <= compt + 1;
			else
etat_next <= TRANSITION_7;
--				etat_next  <= POSITION_01;
				compt_next <= 0;
			end if;
				count_next <= 0;
				count_raf_next <= 0;	
				done_in_next <= '1';
				SS_in_next <= '1';
				SDO_in_next <= '0';
				ack_in_next <= '0';				


-------------------------------------------------------------------
-- POSITIONNEMENT DU CURSEUR POUR LA MISE A JOUR DES VARIABLES
-------------------------------------------------------------------

			WHEN POSITION_01 =>
				SS_in_next <= '0';	
				if count < 2 then
					SDO_in_next <= ligne0(count)(7);
				else 
					SDO_in_next <= ligne1(count_raf)(7);
				end if;			   
				etat_next <= POSITION_11;
			  	count_next <= count; 
				count_raf_next <= count_raf;
				
				
			WHEN POSITION_11 =>
				SS_in_next <= '0';
				if count < 2 then
					SDO_in_next <= ligne0(count)(6);
				else 
					SDO_in_next <= ligne1(count_raf)(6);
				end if;
				etat_next <= POSITION_21;	
				count_next <= count;
				count_raf_next <= count_raf;

				
			WHEN POSITION_21 =>
				SS_in_next <= '0';
				if count < 2 then
					SDO_in_next <= ligne0(count)(5);
				else 
					SDO_in_next <= ligne1(count_raf)(5);
				end if;
				etat_next <= POSITION_31;
				count_next <= count;
				count_raf_next <= count_raf;

				
			WHEN POSITION_31 =>
				SS_in_next <= '0';
				if count < 2 then
					SDO_in_next <= ligne0(count)(4);
				else 
					SDO_in_next <= ligne1(count_raf)(4);
				end if;
				etat_next <= POSITION_41;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN POSITION_41 =>
				SS_in_next <= '0';
				if count < 2 then
					SDO_in_next <= ligne0(count)(3);
				else 
					SDO_in_next <= ligne1(count_raf)(3);
				end if;
				etat_next <= POSITION_51;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN POSITION_51 =>
				SS_in_next <= '0';
				if count < 2 then
					SDO_in_next <= ligne0(count)(2);
				else 
					SDO_in_next <= ligne1(count_raf)(2);
				end if;
				etat_next <= POSITION_61;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN POSITION_61 =>
				SS_in_next <= '0';
				if count < 2 then
					SDO_in_next <= ligne0(count)(1);
				else 
					SDO_in_next <= ligne1(count_raf)(1);
				end if;
				etat_next <= POSITION_71;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN POSITION_71 =>
				etat_next <= TRANSITION_11;   
				SS_in_next <= '0';
				if count < 2 then
					SDO_in_next <= ligne0(count)(0);
				else 
					SDO_in_next <= ligne1(count_raf)(0);
				end if;
				count_next <= count; 
				count_raf_next <= count_raf;
				
				
			WHEN TRANSITION_11 =>
			 if (count = 2) then
				 etat_next  <= REGISTRE2;
				 count_next <= 0;	
				 SDO_in_next <= ligne1(count_raf)(7);
			 else
				etat_next  <= POSITION_01;
				count_next <= count + 1; 
				SDO_in_next <= ligne0(count)(7);
			 end if;			
			   SS_in_next <= '1';
				count_raf_next <= count_raf;      
		   
				
-------------------------------------------------------
--      REGISTRAGE DE LA VALEUR  D'AFFICHAGE
-------------------------------------------------------

			WHEN REGISTRE2 =>
				donnee_reg_next <= nombre(count_raf);
				etat_next <= VAR_UN_1;
				SS_in_next <= '1';
				SDO_in_next <= '0';
				count_next <= count; 
				count_raf_next <= count_raf;
				

---------------------------------------------------------
-----     MISE A JOUR DES VARIABLES
---------------------------------------------------------

					
			WHEN VAR_UN_1 =>
 				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(7);
				etat_next <= VAR_UN_2;	
				count_next <= count; 
				count_raf_next <= count_raf;
				

				
			WHEN VAR_UN_2 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(6);
				etat_next <= VAR_UN_3;	
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN VAR_UN_3 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(5);
				etat_next <= VAR_UN_4;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN VAR_UN_4 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(4);
				etat_next <= VAR_UN_5;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN VAR_UN_5 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(3);
				etat_next <= VAR_UN_6;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN VAR_UN_6 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(2);
				etat_next <= VAR_UN_7;
				count_next <= count;
				count_raf_next <= count_raf;	

				
			WHEN VAR_UN_7 =>
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(1);
				etat_next <= VAR_UN_8;
				count_next <= count; 
				count_raf_next <= count_raf;

				
			WHEN VAR_UN_8 =>
				etat_next <= TRANSITION_12;   
				SS_in_next <= '0';
				SDO_in_next <= donnee_reg_next(count)(0);
				count_next <= count; 
				count_raf_next <= count_raf;         
				
				
			WHEN TRANSITION_12 =>
			 if (count = 2) then
				 etat_next  <= VERIF_RAFFRAICH2;
				 count_next <= 0;		 
			 else
				etat_next  <= VAR_UN_1;
				count_next <= count + 1; 
			 end if;			
			   SS_in_next <= '1';
				SDO_in_next <= donnee_reg_next(count)(0);
				count_raf_next <= count_raf;           
				
				
			WHEN VERIF_RAFFRAICH2 =>
			if (count_raf = 3) then
				count_raf_next <= 0;       					
				count_next <= 0;
				etat_next  <= RAFFRAICHISSEMENT; 
			 else
				etat_next  <= POSITION_01; 
				count_next <= 0;	
				count_raf_next <= count_raf + 1;
			 end if;			
			   SS_in_next <= '1';
				SDO_in_next <= '0';
					

			  
			WHEN OTHERS =>
				SS_in_next <= '1';
				SDO_in_next <= '0';
				etat_next <= START;
				

		END CASE;
	
				
END PROCESS LCD_Display;					

END behavioral;

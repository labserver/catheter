LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration

ENTITY clkGenerator IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		clk48MHz : IN STD_LOGIC; --Master frequency to generate all the other frequencies and pulses
		RSTnFluo : IN STD_LOGIC; --Fluorescence frequencies and pulses needs to be reset for synchronization
        
		clk1Hz : BUFFER STD_LOGIC;
        clk2Hz : BUFFER STD_LOGIC;
		clk20Hz : BUFFER STD_LOGIC;
        clk1kHz : BUFFER STD_LOGIC;
        clk2kHz : BUFFER STD_LOGIC;
        clk50kHz : BUFFER STD_LOGIC;
        clk100kHz : BUFFER STD_LOGIC;
        clk1MHz : BUFFER STD_LOGIC;
        clk24MHz : BUFFER STD_LOGIC;
        
        clk5kHzFluo : BUFFER STD_LOGIC;
        clk10kHzFluo : BUFFER STD_LOGIC;
        clk100kHzFluo : BUFFER STD_LOGIC;
        clk500kHzFluo : BUFFER STD_LOGIC;
        clk5kHzFluoDelayed : BUFFER STD_LOGIC;
        clk10kHzFluoDelayed : BUFFER STD_LOGIC;      
		clk100kHzFluoDelayed : BUFFER STD_LOGIC
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END clkGenerator;


--  Architecture Body

ARCHITECTURE clkGenerator_architecture OF clkGenerator IS
	
	SIGNAL state : STD_LOGIC := '0';
	
BEGIN

PROCESS(clk48MHz)

	VARIABLE counter1Hz : INTEGER RANGE 0 TO 220000000 := 0;
    VARIABLE counter2Hz : INTEGER RANGE 0 TO 220000000 := 0;
	VARIABLE counter20Hz : INTEGER RANGE 0 TO 220000000 := 0;
    VARIABLE counter1kHz : INTEGER RANGE 0 TO 22000000 := 0;
	VARIABLE counter2kHz : INTEGER RANGE 0 TO 22000000 := 0;
	VARIABLE counter50kHz : INTEGER RANGE 0 TO 22000000 := 0;
	VARIABLE counter100kHz : INTEGER RANGE 0 TO 22000000 := 0;
	VARIABLE counter1MHz : INTEGER RANGE 0 TO 22000000 := 0;
	
	VARIABLE counter5kHzFluo : INTEGER RANGE 0 TO 22000000 := 0;
	VARIABLE counter10kHzFluo : INTEGER RANGE 0 TO 22000000 := 0;    
	VARIABLE counter100kHzFluo : INTEGER RANGE 0 TO 22000000 := 0;
    VARIABLE counter500kHzFluo : INTEGER RANGE 0 TO 22000000 := 0;
	VARIABLE counter5kHzFluoDelayed : INTEGER RANGE 0 TO 22000000 := 0;
	VARIABLE counter10kHzFluoDelayed : INTEGER RANGE 0 TO 22000000 := 0;   
	VARIABLE counter100kHzFluoDelayed : INTEGER RANGE 0 TO 22000000 := 0; 
BEGIN

	IF (clk48MHz'EVENT AND clk48MHz = '1') THEN

        clk24MHz <= NOT(clk24MHz);

		counter1Hz := counter1Hz + 1;
		IF counter1Hz >= 24000000 THEN
			clk1Hz <= NOT(clk1Hz);
			counter1Hz := 0;
		END IF;

		counter2Hz := counter2Hz + 1;
		IF counter2Hz >= 12000000 THEN
			clk2Hz <= NOT(clk2Hz);
			counter2Hz := 0;
		END IF;
		
		counter20Hz := counter20Hz + 1;
		IF counter20Hz >= 1200000 THEN
			clk20Hz <= NOT(clk20Hz);
			counter20Hz := 0;
		END IF;
        
		counter1kHz := counter1kHz + 1;
		IF counter1kHz >= 24000 THEN
			clk1kHz <= NOT(clk1kHz);
			counter1kHz := 0;
		END IF;	
		
 		counter2kHz := counter2kHz + 1;
		IF counter2kHz >= 12000 THEN
			clk2kHz <= NOT(clk2kHz);
			counter2kHz := 0;
		END IF;	
        
 		counter50kHz := counter50kHz + 1;
		IF counter50kHz >= 480 THEN
			clk50kHz <= NOT(clk50kHz);
			counter50kHz := 0;
		END IF;	
        
 		counter100kHz := counter100kHz + 1;
		IF counter100kHz >= 240 THEN
			clk100kHz <= NOT(clk100kHz);
			counter100kHz := 0;
		END IF;	
 
		counter1MHz := counter1MHz + 1;
		IF counter1MHz >= 24 THEN
			clk1MHz <= NOT(clk1MHz);
			counter1MHz := 0;
		END IF;	
   
   
 
 		IF RSTnFluo = '0' THEN
			counter5kHzFluo := 500; --Square wave is advanced of 9µs
			clk5kHzFluo <= '0';
		ELSE
			counter5kHzFluo := counter5kHzFluo + 1;
			IF counter5kHzFluo >= 4800 THEN
				clk5kHzFluo <= NOT(clk5kHzFluo);
				counter5kHzFluo := 0;
			END IF;	
		END IF;

 		IF RSTnFluo = '0' THEN
			counter10kHzFluo := 500; --Square wave is advanced of 9µs
			clk10kHzFluo <= '0';
		ELSE
			counter10kHzFluo := counter10kHzFluo + 1;
			IF counter10kHzFluo >= 2400 THEN
				clk10kHzFluo <= NOT(clk10kHzFluo);
				counter10kHzFluo := 0;
			END IF;	
		END IF;
        
		IF RSTnFluo = '0' THEN
			counter100kHzFluo := 100; --Writing to the DAC is advanced to maximize the demodulation process
			clk100kHzFluo <= '0';
		ELSE
			counter100kHzFluo := counter100kHzFluo + 1;
			IF counter100kHzFluo >= 240 THEN
				clk100kHzFluo <= NOT(clk100kHzFluo);
				counter100kHzFluo := 0;
			END IF;	
		END IF;

		IF RSTnFluo = '0' THEN
			counter500kHzFluo := 0;
			clk500kHzFluo <= '0';
		ELSE
			counter500kHzFluo := counter500kHzFluo + 1;
			IF counter500kHzFluo >= 48 THEN
				clk500kHzFluo <= NOT(clk500kHzFluo);
				counter500kHzFluo := 0;
			END IF;	
		END IF;
        
        
 		IF RSTnFluo = '0' THEN
			counter5kHzFluoDelayed := 0;
			clk5kHzFluoDelayed <= '0';
		ELSE
			counter5kHzFluoDelayed := counter5kHzFluoDelayed + 1;
			IF counter5kHzFluoDelayed >= 4800 THEN
				clk5kHzFluoDelayed <= NOT(clk5kHzFluoDelayed);
				counter5kHzFluoDelayed := 0;
			END IF;	
		END IF;        
 
 		IF RSTnFluo = '0' THEN
			counter10kHzFluoDelayed := 0;
			clk10kHzFluoDelayed <= '0';
		ELSE
			counter10kHzFluoDelayed := counter10kHzFluoDelayed + 1;
			IF counter10kHzFluoDelayed >= 2400 THEN
				clk10kHzFluoDelayed <= NOT(clk10kHzFluoDelayed);
				counter10kHzFluoDelayed := 0;
			END IF;	
		END IF;   

 		IF RSTnFluo = '0' THEN
			counter100kHzFluoDelayed := 0;
			clk100kHzFluoDelayed <= '0';
		ELSE
			counter100kHzFluoDelayed := counter100kHzFluoDelayed + 1;
			IF counter100kHzFluoDelayed >= 240 THEN
				clk100kHzFluoDelayed <= NOT(clk100kHzFluoDelayed);
				counter100kHzFluoDelayed := 0;
			END IF;	
		END IF;  
        
	END IF;	
		
END PROCESS;

END clkGenerator_architecture;

LIBRARY ieee;
USE ieee.std_logic_1164.all;


--  Entity Declaration

ENTITY Shutter IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		CLK20HZ : IN STD_LOGIC;
		RSTn : IN STD_LOGIC;
		SHUTTER_START : BUFFER STD_LOGIC;
		SHUTTER_RESET : BUFFER STD_LOGIC
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END Shutter;


--  Architecture Body

ARCHITECTURE Shutter_architecture OF Shutter IS
	
	SIGNAL state : STD_LOGIC_VECTOR(2 DOWNTO 0) := "000"; --bit2=sync bit1=reset bit0=start
	
BEGIN

PROCESS(CLK20HZ, RSTn)

BEGIN

	IF (RSTn = '0') THEN
		state <= "110";
	ELSIF (CLK20HZ'EVENT AND CLK20HZ = '1') THEN
		state <= state;
		IF state = "000" THEN
			state <= "001";
		ELSIF state = "001" THEN
			state <= "100";
		ELSIF state = "100" THEN
			state <= "110";
		ELSIF state = "110" THEN
			state <= "000";
		END IF;
	END IF;
		
END PROCESS;

SHUTTER_START <= state(0);
SHUTTER_RESET <= state(1);

END Shutter_architecture;

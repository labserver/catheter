LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY PAUSAcquisition IS
	PORT
	(
		RSTn : IN STD_LOGIC;
		clkPAUS : IN STD_LOGIC;										

		ADC : IN SIGNED(13 DOWNTO 0); --Bus from the ADC
        nSamples : IN INTEGER RANGE 0 TO 4095;
		
        --Trigger signals
        startPA : IN STD_LOGIC;
        startUS : IN STD_LOGIC;
        averageEnable : IN STD_LOGIC;
        PAUSAcquisitionDone : OUT STD_LOGIC;
        
		--Communication avec la RAM interne au FPGA
		RAM_data : OUT SIGNED(23 DOWNTO 0);
		RAM_wraddressOut : OUT UNSIGNED(11 DOWNTO 0);
		RAM_wren : OUT STD_LOGIC;
		RAM_rdaddressOut : OUT UNSIGNED(11 DOWNTO 0);
		RAM_q : IN SIGNED(23 DOWNTO 0)

	);
END PAUSAcquisition;

--  Architecture Body 
ARCHITECTURE PAUSAcquisition_architecture OF PAUSAcquisition IS

	TYPE stateMachine IS (WAIT_START, PA_DELAY0, PA_DELAY00, PA_DELAY, PA_ACQUISITION, US_DELAY0, US_DELAY00, US_DELAY, US_ACQUISITION);
	SIGNAL state : stateMachine;
	
	SIGNAL NSAMPLES_PA : UNSIGNED(11 DOWNTO 0); --1350 samples
	SIGNAL NSAMPLES_US : UNSIGNED(11 DOWNTO 0); --2700 samples

	SIGNAL idxSample : INTEGER RANGE 0 TO 2710 := 0; --Curent sample index
    
    SIGNAL averageEnableSignal : STD_LOGIC;

    SIGNAL RAM_wraddress : UNSIGNED(11 DOWNTO 0);
    SIGNAL RAM_rdaddress : UNSIGNED(11 DOWNTO 0);
        
BEGIN

RAM_wraddressOut <= RAM_wraddress;
RAM_rdaddressOut <= RAM_rdaddress;
    
PROCESS(RSTn, clkPAUS)

BEGIN
IF (RSTn = '0') THEN
	state <= WAIT_START;
	idxSample <= 0;
	RAM_wren <= '0';
	RAM_data <= (OTHERS => '0');
    RAM_rdaddress <= (OTHERS => '0');
    RAM_wraddress <= (OTHERS => '0');
    PAUSAcquisitionDone <= '0';
    averageEnableSignal <= '0';
    NSAMPLES_PA <= (OTHERS => '0');
    NSAMPLES_US <= (OTHERS => '0');

ELSIF (clkPAUS'EVENT AND clkPAUS = '1') THEN
    NSAMPLES_PA <= TO_UNSIGNED(nSamples,12);
    NSAMPLES_US <= TO_UNSIGNED(2*nSamples,12);
	CASE state IS
	
		WHEN WAIT_START => --Trigger pullback. Called the first time after startAcq and at the end of each turn
            RAM_data <= (OTHERS => '0');
            RAM_wren <= '0';
            idxSample <= 0;
            averageEnableSignal <= averageEnable;
            
            IF startPA = '1' THEN
                RAM_rdaddress <= (OTHERS => '0');
                RAM_wraddress <= (OTHERS => '0');
                PAUSAcquisitionDone <= '0';
                state <= PA_DELAY0;
            ELSIF startUS = '1' THEN
                RAM_rdaddress <= NSAMPLES_PA; --Offset after PA in the RAM
                RAM_wraddress <= NSAMPLES_PA; --Offset after PA in the RAM
                PAUSAcquisitionDone <= '0';
                state <= US_DELAY0;
            ELSE
                PAUSAcquisitionDone <= '1';
                RAM_wraddress <= (OTHERS => '0');
                RAM_rdaddress <= (OTHERS => '0');
                state <= WAIT_START;
            END IF;

        WHEN PA_DELAY0 => --Delay for the synchronization with the RAM
            RAM_data <= (OTHERS => '0');
            RAM_wren <= '0';
			idxSample <= 1;
			state <= PA_DELAY00;
			RAM_rdaddress <= RAM_rdaddress + 1;
            RAM_wraddress <= (OTHERS => '0');
            PAUSAcquisitionDone <= '0';
            averageEnableSignal <= averageEnableSignal;
 
        WHEN PA_DELAY00 => --Delay for the synchronization with the RAM
            RAM_data <= (OTHERS => '0');
            RAM_wren <= '0';
			state <= PA_DELAY;
			RAM_rdaddress <= RAM_rdaddress + 1;
            RAM_wraddress <= (OTHERS => '0');
            PAUSAcquisitionDone <= '0';
            averageEnableSignal <= averageEnableSignal;
            
		WHEN PA_DELAY => --Delay for the synchronization with the RAM
            RAM_data <= (OTHERS => '0');
            RAM_wren <= '0';
			state <= PA_ACQUISITION;
			RAM_rdaddress <= RAM_rdaddress + 1;
            RAM_wraddress <= (OTHERS => '0');
            PAUSAcquisitionDone <= '0';
            averageEnableSignal <= averageEnableSignal;
			
		WHEN PA_ACQUISITION => --Reading a group of samples
        
            IF averageEnable = '1' THEN
                RAM_data <= ADC + RAM_q;
				--RAM_data <= idxSample + RAM_q;
            ELSE
                RAM_data <= ADC + TO_SIGNED(0,24);
				--RAM_data <= idxSample + TO_SIGNED(0,24);
            END IF;    
            
			RAM_wren <= '1';	
             --Increment write address (except for the first sample)
			IF idxSample > 1 THEN																					
				RAM_wraddress <= RAM_wraddress + 1;
            ELSE
                RAM_wraddress <= RAM_wraddress;
			END IF;
            
            idxSample <= idxSample + 1;
            RAM_rdaddress <= RAM_rdaddress + 1;	
			IF (idxSample >= NSAMPLES_PA) THEN
				state <= WAIT_START;
                PAUSAcquisitionDone <= '1';
			ELSE
                PAUSAcquisitionDone <= '0';
                state <= PA_ACQUISITION;
			END IF;
            averageEnableSignal <= averageEnableSignal;
            
		WHEN US_DELAY0 => --Delay for the synchronization with the RAM
            RAM_data <= (OTHERS => '0');
            RAM_wren <= '0';
			idxSample <= 1;
			state <= US_DELAY00;
			RAM_rdaddress <= RAM_rdaddress + 1;
            RAM_wraddress <= RAM_wraddress;
            PAUSAcquisitionDone <= '0';
            averageEnableSignal <= averageEnableSignal;

		WHEN US_DELAY00 => --Delay for the synchronization with the RAM
            RAM_data <= (OTHERS => '0');
            RAM_wren <= '0';
			idxSample <= 1;
			state <= US_DELAY;
			RAM_rdaddress <= RAM_rdaddress + 1;
            RAM_wraddress <= RAM_wraddress;
            PAUSAcquisitionDone <= '0';
            averageEnableSignal <= averageEnableSignal;
            
		WHEN US_DELAY => --Delay for the synchronization with the RAM
            RAM_data <= (OTHERS => '0');
            RAM_wren <= '0';
			idxSample <= 1;
			state <= US_ACQUISITION;
			RAM_rdaddress <= RAM_rdaddress + 1;
            RAM_wraddress <= RAM_wraddress;
            PAUSAcquisitionDone <= '0';
            averageEnableSignal <= averageEnableSignal;
			
		WHEN US_ACQUISITION => --Reading a group of samples
            IF averageEnableSignal = '1' THEN
                RAM_data <= ADC + RAM_q;
				--RAM_data <= idxSample + RAM_q;
            ELSE
                RAM_data <= ADC + TO_SIGNED(0,24);
				--RAM_data <= idxSample + TO_SIGNED(0,24);
            END IF;    
            
			RAM_wren <= '1';	
             --Increment write address (except for the first sample)
			IF idxSample > 1 THEN																					
				RAM_wraddress <= RAM_wraddress + 1;
            ELSE
                RAM_wraddress <= RAM_wraddress;
			END IF;
            
            idxSample <= idxSample + 1;
            RAM_rdaddress <= RAM_rdaddress + 1;	
			IF (idxSample >= NSAMPLES_US) THEN
				state <= WAIT_START;
                PAUSAcquisitionDone <= '1';
			ELSE
                state <= US_ACQUISITION;
                PAUSAcquisitionDone <= '0';
			END IF;
            
            averageEnableSignal <= averageEnableSignal;
														
		WHEN OTHERS =>
			RAM_wren <= '0';
            RAM_data <= (OTHERS => '0');
            RAM_wraddress <= (OTHERS => '0');
            RAM_rdaddress <= (OTHERS => '0');
			state <= WAIT_START;	
            idxSample <= 0;
            PAUSAcquisitionDone <= '1';
            averageEnableSignal <= '0';
	END CASE;	
END IF;

END PROCESS;

END PAUSAcquisition_architecture;

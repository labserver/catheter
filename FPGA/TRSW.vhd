LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

--  Entity Declaration
ENTITY TRSW IS
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk1MHz : IN STD_LOGIC;
		enableAcq : IN STD_LOGIC;
	
		--Param�tre provenant de l'ordinateur
		TRSW_PARAMETER : IN UNSIGNED(11 DOWNTO 0);	
		
        TRSW_CLK : OUT STD_LOGIC;
        TRSW_LEn : OUT STD_LOGIC;
        TRSW_DIN : OUT STD_LOGIC; 
        TRSW_CLR : OUT STD_LOGIC
	);

END TRSW;

--  Architecture Body 
ARCHITECTURE TRSW_architecture OF TRSW IS

TYPE stateMachine IS (WAIT_START, SEND, SEND_DELAY, LATCH, DONE);
SIGNAL state : stateMachine := WAIT_START;

SIGNAL counterDataBit : INTEGER RANGE 0 TO 11 := 11;

BEGIN

PROCESS(RSTn, clk1MHz)
    VARIABLE nextData : UNSIGNED(11 DOWNTO 0);
BEGIN
	IF (RSTn = '0') THEN
		state <= WAIT_START;
        TRSW_LEn <= '1';
        TRSW_DIN <= '1';
        TRSW_CLK <= '0';
        counterDataBit <= 11;
        TRSW_CLR <= '1';
        
	ELSIF (clk1MHz'EVENT AND clk1MHz = '1') THEN   
        TRSW_LEn <= '1';
        TRSW_CLK <= '0';
        TRSW_CLR <= '0';
        
		CASE state IS

            WHEN WAIT_START =>
                IF enableAcq = '1' THEN
                    state <= SEND;
                    counterDataBit <= 11;
                END IF;

			WHEN SEND =>
                TRSW_DIN <= TRSW_PARAMETER(counterDataBit);
                state <= SEND_DELAY;
                

            WHEN SEND_DELAY =>
                TRSW_CLK <= '1';
                counterDataBit <= counterDataBit - 1;
                IF counterDataBit = 0 THEN
                    state <= LATCH;
                ELSE
                    state <= SEND;
                END IF;
                
            WHEN LATCH =>
                TRSW_LEn <= '0';
                state <= DONE;
                
            WHEN DONE =>
                state <= WAIT_START;

			WHEN OTHERS =>
				state <= WAIT_START;

		END CASE;
            
	END IF;
END PROCESS;

END TRSW_architecture;

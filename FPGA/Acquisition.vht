LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY TB_Acquisition IS
	PORT
	(
        --Communication avec la RAM interne au FPGA
        RAM_data : BUFFER SIGNED(23 downto 0);
        RAM_wraddress : BUFFER UNSIGNED(10 downto 0);
        RAM_wren : OUT STD_LOGIC;
        RAM_rdaddress : BUFFER UNSIGNED(10 downto 0);
        dualBuffer : BUFFER STD_LOGIC; --Switch buffer (acquisition is done in a RAM while we transfer the other)
        
        --Output of the system
        pulseUS1 : OUT STD_LOGIC; --Positive ultrasound pulser
        pulseUS2 : OUT STD_LOGIC; --Negative ultrasound pulser
        pulseUSRTZ : OUT STD_LOGIC; --Return-To-Zero pulser after pulsing
        
        --Communications with the FIFO transfer block
        quantityToSend : BUFFER INTEGER RANGE 0 TO 2147483647;
        transferReady : BUFFER STD_LOGIC;
        scannedPosition : BUFFER UNSIGNED(31 downto 0); --Last scanned position
        
        --Duration of the acquisition at one position of a transfer vector
        durationAcquisition : BUFFER UNSIGNED(31 DOWNTO 0);
                        
        --Communication with the pullback motor
        pullbackMotorDo : BUFFER STD_LOGIC; --The acquisition module will tell this module to change slice. It will wait for the done signal
        pullbackMotorReset : OUT STD_LOGIC; --Connected to startAcq, when '0' the motor slowly go to 0.
        
        enableFluo : BUFFER STD_LOGIC; --Starting the acquisition of a new position (after the photoacoustic laser pulse)
                        
        --Communication with the DAC controller	
        DAC_PA : BUFFER STD_LOGIC;
        DAC_US : BUFFER STD_LOGIC;
        
        LED : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);

        error : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
        debugOut : BUFFER UNSIGNED(11 DOWNTO 0);
        syncDurationPosition : BUFFER STD_LOGIC --Used to synchronize with the module that calculates the duration of a position ('1' before the first position)
    );
END TB_Acquisition;

ARCHITECTURE TB OF TB_Acquisition IS

    COMPONENT Acquisition
        PORT
        (
            RSTn : IN STD_LOGIC;
            clk125MHz : IN STD_LOGIC;										

            ADC : IN SIGNED(13 DOWNTO 0); --Bus from the ADC
            PALaserTrigger : IN STD_LOGIC; -- Trigger from the photoacoustic laser
        
            --Parameters received from the computer (see Matlab code for information)
            delayBetweenUltrasoundsNCycles : IN UNSIGNED(15 DOWNTO 0);
            nAveragingUltrasound : IN UNSIGNED(15 DOWNTO 0);
            nAveragingUltrasoundFlush : IN UNSIGNED(15 DOWNTO 0);
            photoacousticOn : IN STD_LOGIC;
            motorSync : IN STD_LOGIC;
            nSlices : IN UNSIGNED(15 DOWNTO 0);
            nTurnsPerSlice : IN UNSIGNED(15 DOWNTO 0);
            pulseShape : IN UNSIGNED(15 DOWNTO 0);
            StartRTZ : IN UNSIGNED(11 DOWNTO 0);
            StopRTZ : IN UNSIGNED(11 DOWNTO 0);
                                    
            --Communication avec la RAM interne au FPGA
            RAM_data : BUFFER SIGNED(23 downto 0);
            RAM_wraddress : BUFFER UNSIGNED(10 downto 0);
            RAM_wren : OUT STD_LOGIC;
            RAM_rdaddress : BUFFER UNSIGNED(10 downto 0);
            RAM_q : IN SIGNED(23 downto 0);
            dualBuffer : BUFFER STD_LOGIC; --Switch buffer (acquisition is done in a RAM while we transfer the other)
            
            --Output of the system
            pulseUS1 : OUT STD_LOGIC; --Positive ultrasound pulser
            pulseUS2 : OUT STD_LOGIC; --Negative ultrasound pulser
            pulseUSRTZ : OUT STD_LOGIC; --Return-To-Zero pulser after pulsing
            
            --Communications with the FIFO transfer block
            quantityToSend : BUFFER INTEGER RANGE 0 TO 2147483647;
            transferReady : BUFFER STD_LOGIC;
            scannedPosition : BUFFER UNSIGNED(31 downto 0); --Last scanned position
            transferring : IN STD_LOGIC; --Used to verify that the last transfer is finished and the current transfer starts before continuing demodulation
            
            --Duration of the acquisition at one position of a transfer vector
            durationAcquisition : BUFFER UNSIGNED(31 DOWNTO 0);
                    
            --Communication with the motor controller
            position : IN UNSIGNED(11 DOWNTO 0);
            positionChanged : IN STD_LOGIC;
            
            --Communication with the pullback motor
            pullbackMotorDo : BUFFER STD_LOGIC; --The acquisition module will tell this module to change slice. It will wait for the done signal
            pullbackMotorReset : OUT STD_LOGIC; --Connected to startAcq, when '0' the motor slowly go to 0.
            pullbackMotorDone : IN STD_LOGIC; --Tells the acquisition module that the pullback is done
            
            demodReady : IN STD_LOGIC; --The demodulation process is read to demodulate a new position
            enableFluo : BUFFER STD_LOGIC; --Starting the acquisition of a new position (after the photoacoustic laser pulse)
                            
            --Communication with the DAC controller	
            DAC_PA : BUFFER STD_LOGIC;
            DAC_US : BUFFER STD_LOGIC;
            
            LED : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);

            error : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
            debugOut : BUFFER UNSIGNED(11 DOWNTO 0);
            syncDurationPosition : BUFFER STD_LOGIC --Used to synchronize with the module that calculates the duration of a position ('1' before the first position)
        );
    END COMPONENT;

            SIGNAL RSTn : STD_LOGIC;
            SIGNAL clk125MHz : STD_LOGIC;										

            SIGNAL ADC : SIGNED(13 DOWNTO 0); --Bus from the ADC
            SIGNAL PALaserTrigger : STD_LOGIC; -- Trigger from the photoacoustic laser
        
            --Parameters received from the computer (see Matlab code for information)
            SIGNAL delayBetweenUltrasoundsNCycles : UNSIGNED(15 DOWNTO 0);
            SIGNAL nAveragingUltrasound : UNSIGNED(15 DOWNTO 0);
            SIGNAL nAveragingUltrasoundFlush : UNSIGNED(15 DOWNTO 0);
            SIGNAL photoacousticOn : STD_LOGIC;
            SIGNAL motorSync : STD_LOGIC;
            SIGNAL nSlices : UNSIGNED(15 DOWNTO 0);
            SIGNAL nTurnsPerSlice : UNSIGNED(15 DOWNTO 0);
            SIGNAL pulseShape : UNSIGNED(15 DOWNTO 0);
            SIGNAL StartRTZ : UNSIGNED(11 DOWNTO 0);
            SIGNAL StopRTZ : UNSIGNED(11 DOWNTO 0);

            SIGNAL RAM_q : SIGNED(23 downto 0);

            SIGNAL transferring : STD_LOGIC; --Used to verify that the last transfer is finished and the current transfer starts before continuing demodulation

            --Communication with the motor controller
            SIGNAL position : UNSIGNED(11 DOWNTO 0) := "000000000000";
            SIGNAL positionChanged : STD_LOGIC;

            SIGNAL pullbackMotorDone : STD_LOGIC; --Tells the acquisition module that the pullback is done
            
            SIGNAL demodReady : STD_LOGIC; --The demodulation process is read to demodulate a new position

            
BEGIN

    U_Acquisition: Acquisition PORT MAP (RSTn, clk125MHz, ADC, PALaserTrigger, delayBetweenUltrasoundsNCycles, nAveragingUltrasound, nAveragingUltrasoundFlush, photoacousticOn, motorSync, nSlices, 
        nTurnsPerSlice, pulseShape, StartRTZ, StopRTZ, RAM_data, RAM_wraddress, RAM_wren, RAM_rdaddress, RAM_q, dualBuffer, pulseUS1, pulseUS2, pulseUSRTZ, quantityToSend, transferReady, 
        scannedPosition, transferring, durationAcquisition, position, positionChanged, pullbackMotorDo, pullbackMotorReset, pullbackMotorDone, demodReady, enableFluo, DAC_PA, DAC_US, LED, 
        error, debugOut, syncDurationPosition);
        
    --Motors
    motorSync <= '1';
    position_gen : PROCESS
	BEGIN
		WAIT FOR 50 us;	
        IF position = 255 THEN
            position <= TO_UNSIGNED(0,12);
        ELSE
            position <= position + 1;
        END IF;
        positionChanged <= '1';
        WAIT FOR 8 ns;
        positionChanged <= '0';
	END PROCESS;	    
    
    pullbackMotor_gen : PROCESS
	BEGIN
		pullbackMotorDone <= '1' after 1900 us, '0' after 1950 us;
		WAIT FOR 1950 us;	
	END PROCESS;	    
 
    transferring_gen : PROCESS
	BEGIN
		transferring <= '1' after 4999 us, '0' after 5000 us;
		WAIT FOR 6000 us;	
	END PROCESS;	
    
    --Maybe important
    nSlices <= "0000000000000101";
    nTurnsPerSlice <= "0000000000000001";
    
    --Not important
    ADC <= "00000000000011";
    PALaserTrigger <= '0';
    delayBetweenUltrasoundsNCycles <= "0000000000000001";
    nAveragingUltrasound <= "0000000000000001";
    nAveragingUltrasoundFlush <= "0000000000000000";
    photoacousticOn <= '0';
    pulseShape <= TO_UNSIGNED(1,16);
    StartRTZ <= TO_UNSIGNED(1,12);
    StopRTZ <= TO_UNSIGNED(5,12);
    RAM_q <= TO_SIGNED(5,24);
    demodReady <= '1';
    
    
    RSTn_gen : PROCESS
	BEGIN
		RSTn <= '0';
		WAIT FOR 100 ns;
		RSTn <= '1';
		WAIT;
	END PROCESS;	
    
	clock_gen : PROCESS
	BEGIN
		clk125MHz <= '0' after 4 ns, '1' after 8 ns;
		WAIT FOR 8 ns;	
	END PROCESS;
END TB;
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

--  Entity Declaration

ENTITY RotatingMotor IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		CLK48M : IN STD_LOGIC;
		RSTn : IN STD_LOGIC;
		RotatingMotorValue : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0)
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END RotatingMotor;


--  Architecture Body

ARCHITECTURE RotatingMotor_architecture OF RotatingMotor IS

BEGIN

PROCESS(CLK48M)

	VARIABLE compteur1000HZ : INTEGER RANGE 0 TO 220000000 := 0;

BEGIN

	IF (CLK48M'EVENT AND CLK48M = '1') THEN
		compteur1000HZ := compteur1000HZ + 1;
		IF compteur1000HZ >= 48000 THEN
			compteur1000HZ := 0;
			IF RotatingMotorValue = "111111111111" THEN
				RotatingMotorValue <= (OTHERS => '0');
			ELSE
				RotatingMotorValue <= RotatingMotorValue + 1;
			END IF;
		END IF;
		
	END IF;	
		
END PROCESS;

END RotatingMotor_architecture;

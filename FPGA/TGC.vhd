LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY TGC IS
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;
		enableAcq : IN STD_LOGIC;
		
		--Communication avec le bloc Acquisition pour indiquer quelle acquisition est en cours
		DAC_PA : IN STD_LOGIC;
		DAC_US : IN STD_LOGIC;
        DAC_US_DONE : IN STD_LOGIC;
        DAC_CutUSPulse : IN STD_LOGIC;
        
		
		--Paramètre provenant de l'ordinateur pour indiquer la valeur du gain désiré
		AMPLI_GAIN_INIT_PA : IN UNSIGNED(11 DOWNTO 0);	
		AMPLI_GAIN_INIT_US : IN UNSIGNED(11 DOWNTO 0);	
        TGCPA : IN STD_LOGIC;
        TGCUS : IN STD_LOGIC;
        TGCCutPulse : IN STD_LOGIC;
		TGC1 : IN UNSIGNED(11 DOWNTO 0); --Additional gain wanted at 1.5 mm
        TGC2 : IN UNSIGNED(11 DOWNTO 0); --Additional gain wanted at 3 mm
        TGC3 : IN UNSIGNED(11 DOWNTO 0); --Additional gain wanted at 4.5 mm
        TGC4 : IN UNSIGNED(11 DOWNTO 0); --Additional gain wanted at 6 mm
		
		TGC_SDIN : OUT STD_LOGIC;
		TGC_SYNCn : OUT STD_LOGIC;
        TGC_SCLK : OUT STD_LOGIC;
		FAST_TGC : OUT STD_LOGIC
	);

END TGC;

--  Architecture Body 
ARCHITECTURE TGC_architecture OF TGC IS

TYPE stateMachine IS (WAIT_START, SEND, DUMMY);
SIGNAL state : stateMachine := SEND;

SIGNAL TGC_DATA : UNSIGNED(15 DOWNTO 0);
SIGNAL counterDataBit : INTEGER RANGE 0 TO 15 := 15;
SIGNAL CLKEN : STD_LOGIC := '0';

SIGNAL previousDAC_PA : STD_LOGIC;
SIGNAL previousDAC_US : STD_LOGIC;
SIGNAL previousDAC_US_DONE : STD_LOGIC;
SIGNAL previousDAC_CutUSPulse : STD_LOGIC;
SIGNAL previousEnableAcq : STD_LOGIC;

SIGNAL dummyStateCounter : INTEGER RANGE 0 TO 100 := 0;
SIGNAL rampIndex : INTEGER RANGE 0 TO 100 := 0; --Used to know where we are in the ramp. Every 6 rampIndex (3 in PA), we change TGC value.


BEGIN

TGC_SCLK <= clk48MHz WHEN CLKEN = '1' ELSE '1';

PROCESS(RSTn, clk48MHz)

BEGIN
	IF (RSTn = '0') THEN
		state <= SEND;
        FAST_TGC <= '1';
        TGC_SYNCn <= '1';
        TGC_DATA(11 DOWNTO 0) <= (OTHERS => '0');
        TGC_DATA(15 DOWNTO 12) <= "0000";
        counterDataBit <= 15;
        previousDAC_PA <= '0';
        previousDAC_US <= '0';
        previousDAC_US_DONE <= '0'; 
        previousDAC_CutUSPulse <= '0';
        dummyStateCounter <= 0;
        rampIndex <= 0;
        CLKEN <= '0';
        
	ELSIF (clk48MHz'EVENT AND clk48MHz = '1') THEN

        FAST_TGC <= '1';
        TGC_DATA(15 DOWNTO 12) <= "0000";    
        dummyStateCounter <= 0;
        CLKEN <= '0'; 

		CASE state IS
            
            WHEN WAIT_START =>
                --Verify what is the next data to send
                --Real output is delayed by 1 µs, plus 500 ns for the amplifier to respond (total of 1.5 µs). Slew rate is 1V/µs
                previousDAC_PA <= DAC_PA;
                previousDAC_US <= DAC_US;
                previousDAC_US_DONE <= DAC_US_DONE;
                previousDAC_CutUSPulse <= DAC_CutUSPulse;
                previousEnableAcq <= enableAcq;    
                
                IF previousEnableAcq = '0' AND enableAcq = '1' THEN --Starting of the acquisition
                    TGC_DATA(11 DOWNTO 0) <= AMPLI_GAIN_INIT_PA;
                    state <= SEND; CLKEN <= '1';   
                ELSIF DAC_CutUSPulse = '1' AND previousDAC_CutUSPulse = '0' AND TGCCutPulse = '1' THEN --PA or US acquisition is done in 1 µs. Reduce the gain to minimum.
                    TGC_DATA(11 DOWNTO 0) <= (OTHERS => '0');
                    state <= SEND; CLKEN <= '1';                       
                ELSIF DAC_PA = '1' AND previousDAC_PA = '0' THEN --Rising edge on DAC_PA
                    rampIndex <= 1;
                    IF TGCPA = '1' THEN
                        TGC_DATA(11 DOWNTO 0) <= AMPLI_GAIN_INIT_PA + TGC1;
                    END IF;
                    state <= SEND; CLKEN <= '1';               
                ELSIF DAC_PA = '1' AND previousDAC_PA = '1' THEN --Not the first DAC_PA, increment data.
                    IF TGCPA = '1' THEN
                        rampIndex <= rampIndex + 1;
                        IF rampIndex <= 1 THEN
                            TGC_DATA(11 DOWNTO 0) <= TGC_DATA(11 DOWNTO 0) + TO_INTEGER(TGC1(11 DOWNTO 1)); --Divided by 2
                        ELSIF rampIndex <= 3 THEN
                            TGC_DATA(11 DOWNTO 0) <= TGC_DATA(11 DOWNTO 0) + TO_INTEGER(TGC2(11 DOWNTO 1)); --Divided by 2
                        ELSIF rampIndex <= 5 THEN
                            TGC_DATA(11 DOWNTO 0) <= TGC_DATA(11 DOWNTO 0) + TO_INTEGER(TGC3(11 DOWNTO 1)); --Divided by 2
                        ELSIF rampIndex <= 7 THEN
                            TGC_DATA(11 DOWNTO 0) <= TGC_DATA(11 DOWNTO 0) + TO_INTEGER(TGC4(11 DOWNTO 1)); --Divided by 2   
                        END IF; 
                        state <= SEND; CLKEN <= '1';   
                    END IF;
                ELSIF DAC_US = '1' AND previousDAC_US = '0' THEN --Rising edge on DAC_US
                    rampIndex <= 1;
                    IF TGCUS = '1' THEN
                        TGC_DATA(11 DOWNTO 0) <= AMPLI_GAIN_INIT_US + TGC1;
                        state <= SEND; CLKEN <= '1';   
                    ELSIF TGCUS = '0' AND TGCCutPulse = '1' THEN
                        TGC_DATA(11 DOWNTO 0) <= AMPLI_GAIN_INIT_US;
                        state <= SEND; CLKEN <= '1';   
                    END IF;
                ELSIF DAC_US = '1' AND previousDAC_US = '1' THEN --Not the first DAC_US, increment data.
                    IF TGCUS = '1' THEN
                        rampIndex <= rampIndex + 1;
                        IF rampIndex <= 3 THEN
                            TGC_DATA(11 DOWNTO 0) <= TGC_DATA(11 DOWNTO 0) + TGC1;
                        ELSIF rampIndex <= 7 THEN
                            TGC_DATA(11 DOWNTO 0) <= TGC_DATA(11 DOWNTO 0) + TGC2;                        
                        ELSIF rampIndex <= 11 THEN
                            TGC_DATA(11 DOWNTO 0) <= TGC_DATA(11 DOWNTO 0) + TGC3;
                        ELSIF rampIndex <= 15 THEN
                            TGC_DATA(11 DOWNTO 0) <= TGC_DATA(11 DOWNTO 0) + TGC4;    
                        END IF;
                        state <= SEND; CLKEN <= '1';   
                    END IF;
                ELSIF DAC_PA = '0' AND previousDAC_PA = '1' THEN --Falling edge on DAC_PA, next will be US.
                    TGC_DATA(11 DOWNTO 0) <= AMPLI_GAIN_INIT_US;
                    state <= SEND; CLKEN <= '1';   
                ELSIF DAC_US = '0' AND previousDAC_US = '1' THEN --Falling edge on DAC_US, next will be US (unless followed by a rising edge on DAC_US_DONE).
                    TGC_DATA(11 DOWNTO 0) <= AMPLI_GAIN_INIT_US;
                    state <= SEND; CLKEN <= '1';   
                ELSIF DAC_US_DONE = '1' AND previousDAC_US_DONE = '0' THEN --Rising edge on DAC_US_DONE, next will be PA.
                    TGC_DATA(11 DOWNTO 0) <= AMPLI_GAIN_INIT_PA;
                    state <= SEND; CLKEN <= '1';   
                ELSIF previousEnableAcq = '1' AND enableAcq = '0' THEN --Ending of the acquisition
                    TGC_DATA(11 DOWNTO 0) <= AMPLI_GAIN_INIT_PA;
                    state <= SEND; CLKEN <= '1';              
                END IF;         

                counterDataBit <= 15;
                TGC_SYNCn <= '1';
                
			WHEN SEND =>	
                CLKEN <= '1';       
                TGC_SYNCn <= '0';
                TGC_SDIN <= TGC_DATA(counterDataBit);
                IF counterDataBit = 0 THEN
                    state <= DUMMY;
                ELSE
                    counterDataBit <= counterDataBit - 1;
                END IF;
             
            WHEN DUMMY => --State used to refresh TGC exactly every 0.5 µs.
                dummyStateCounter <= dummyStateCounter + 1;
                IF dummyStateCounter >= 6 THEN
                    state <= WAIT_START;
                END IF;
                TGC_SYNCn <= '1';
                
			WHEN OTHERS =>
				state <= WAIT_START;			
		END CASE;
            
	END IF;
END PROCESS;

END TGC_architecture;

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY Moteurs_Controller IS
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;
		motorIsReset : OUT STD_LOGIC;
		
		--Param�tre du parcours des moteurs (Voir Matlab pour les d�tails)
		constantPullback : IN STD_LOGIC; 
		distanceFirstSlice : IN UNSIGNED(15 DOWNTO 0); --In steps of 0.1mm
		nSlices : IN UNSIGNED(15 DOWNTO 0);	
		distanceBetweenSlices : IN UNSIGNED(15 DOWNTO 0); --In steps of 0.1mm
		linearMotorSpeed : IN UNSIGNED(15 DOWNTO 0); --In steps of 0.1mm/s
		linearMotorSpeedHome : IN UNSIGNED(15 DOWNTO 0); --In steps of 0.1mm/s
		
		--Signaux de communication avec le bloc VHDL Moteurs		
		motorInstruction : BUFFER STD_LOGIC_VECTOR (47 DOWNTO 0);
		motorWrite : OUT STD_LOGIC;
		motorDone : IN STD_LOGIC;
		
		--Signaux de communication avec le bloc VHDL Acquisition
		Deplacement_Enable : IN STD_LOGIC;
		Deplacement_Done : BUFFER STD_LOGIC;
		
		Start_Acq : IN STD_LOGIC;
		LED : BUFFER STD_LOGIC_VECTOR(5 DOWNTO 0)
	);
	
END Moteurs_Controller;

--  Architecture Body 
ARCHITECTURE Moteurs_Controller_architecture OF Moteurs_Controller IS

	TYPE stateMachine IS (RESTORE_SETTINGS, RENUMBER, HOME_SPEED, MOVE_SPEED, DISABLE_KNOB, HOME, WAIT_START, INIT1, INIT2, ABSOLUTE_MOVE, CHANGE_POSITION, DONE);
	SIGNAL state : stateMachine;

	--Variables indiquants la position actuelle des moteurs
	--Varient de min � max
	SIGNAL current_x : INTEGER RANGE 0 TO 2147483647;
	SIGNAL idxSlice : INTEGER RANGE 0 TO 1000;

	--Param�tres fix�s au d�but de l'acquisition
	SIGNAL distanceFirstSlice_steps : INTEGER RANGE 0 TO 2147483647;
	SIGNAL distanceBetweenSlices_steps : INTEGER RANGE 0 TO 2147483647;
	
	SIGNAL previousStart_Acq : STD_LOGIC;
	
	CONSTANT resolutionConvert : INTEGER := 52; --(10µm / resolution(=0.1905µm)
	CONSTANT speedConvert : INTEGER := 56; --(0.1/(9.375*0.1905e-3) (in 0.1mm/s), from spreadsheet of Zaber)


BEGIN

PROCESS(clk48MHz, RSTn)
	VARIABLE dataTemp : STD_LOGIC_VECTOR(31 DOWNTO 0);
BEGIN
	IF (RSTn = '0') THEN
		state <= RENUMBER;
		Deplacement_Done <= '0';
		motorWrite <= '0';
		motorInstruction <= (OTHERS => '0');
		motorIsReset <= '0'; 
		idxSlice <= 0;
		previousStart_Acq <= '0';
		--LED <= (OTHERS => '0');
		
	ELSIF (clk48MHz'EVENT AND clk48MHz = '1') THEN	
		motorWrite <= '0';
		Deplacement_Done <= '0';
		previousStart_Acq <= Start_Acq;
		
		CASE state IS

			--Donn�es � transmettre:
			--Vecteur 47 DOWNTO 40 : Num�ro du moteur (0 pour tous les moteurs)
			--Vecteur 39 DOWNTO 32 : Code de la commande (2 pour renumber, 1 pour home, 20 pour d�placement absolu)
			--Vecteur 31 DOWNTO 0 : Donn�es (Seulement pour le d�placement absolu)

			WHEN RESTORE_SETTINGS => --Skipped, does not work well with disable knob
				IF Start_Acq = '0' THEN
					motorIsReset <= '0'; --Don't allow the acquisition to start before motor is homed 
				END IF;
				motorInstruction(47 DOWNTO 40) <= "00000000";
				motorInstruction(39 DOWNTO 32) <= "00100100"; --Code 36
				motorInstruction(31 DOWNTO 24) <= "00000000";
				motorInstruction(23 DOWNTO 16) <= "00000000";
				motorInstruction(15 DOWNTO 8) <= "00000000";
				motorInstruction(7 DOWNTO 0) <= "00000000";
                state <= RENUMBER;
--				motorWrite <= '1';
--				IF motorDone = '1' THEN
--					motorWrite <= '0';
--					state <= RENUMBER;
--				END IF;	
				
			WHEN RENUMBER =>	
				motorInstruction(47 DOWNTO 40) <= "00000000";
				motorInstruction(39 DOWNTO 32) <= "00000010"; --Code 2
				motorInstruction(31 DOWNTO 24) <= "00000000";
				motorInstruction(23 DOWNTO 16) <= "00000000";
				motorInstruction(15 DOWNTO 8) <= "00000000";
				motorInstruction(7 DOWNTO 0) <= "00000000";
				motorWrite <= '1';
				IF motorDone = '1' THEN
					motorWrite <= '0';
					state <= HOME_SPEED;
				END IF;			
				
			WHEN HOME_SPEED =>	
				motorInstruction(47 DOWNTO 40) <= "00000000";
				motorInstruction(39 DOWNTO 32) <= "00101001"; --Code 41
				dataTemp := STD_LOGIC_VECTOR(TO_UNSIGNED(TO_INTEGER(linearMotorSpeedHome)*speedConvert,32));
				motorInstruction(31 DOWNTO 24) <= dataTemp(7 DOWNTO 0);
				motorInstruction(23 DOWNTO 16) <= dataTemp(15 DOWNTO 8);
				motorInstruction(15 DOWNTO 8) <= dataTemp(23 DOWNTO 16);
				motorInstruction(7 DOWNTO 0) <= dataTemp(31 DOWNTO 24);
				motorWrite <= '1';
				IF motorDone = '1' THEN
					motorWrite <= '0';
					state <= MOVE_SPEED;
				END IF;			
								
			WHEN MOVE_SPEED =>	
				motorInstruction(47 DOWNTO 40) <= "00000000";
				motorInstruction(39 DOWNTO 32) <= "00101010"; --Code 42
				dataTemp := STD_LOGIC_VECTOR(TO_UNSIGNED(TO_INTEGER(linearMotorSpeed)*speedConvert,32));
				motorInstruction(31 DOWNTO 24) <= dataTemp(7 DOWNTO 0);
				motorInstruction(23 DOWNTO 16) <= dataTemp(15 DOWNTO 8);
				motorInstruction(15 DOWNTO 8) <= dataTemp(23 DOWNTO 16);
				motorInstruction(7 DOWNTO 0) <= dataTemp(31 DOWNTO 24);
				motorWrite <= '1';
				IF motorDone = '1' THEN
					motorWrite <= '0';
					state <= DISABLE_KNOB;
				END IF;		
                
			WHEN DISABLE_KNOB =>	
				motorInstruction(47 DOWNTO 40) <= "00000000";
				motorInstruction(39 DOWNTO 32) <= "00101000"; --Code 40
				dataTemp := STD_LOGIC_VECTOR(TO_UNSIGNED(2048+8,32));
				motorInstruction(31 DOWNTO 24) <= dataTemp(7 DOWNTO 0);
				motorInstruction(23 DOWNTO 16) <= dataTemp(15 DOWNTO 8);
				motorInstruction(15 DOWNTO 8) <= dataTemp(23 DOWNTO 16);
				motorInstruction(7 DOWNTO 0) <= dataTemp(31 DOWNTO 24);
				motorWrite <= '1';
				IF motorDone = '1' THEN
					motorWrite <= '0';
					state <= HOME;
				END IF;		                

			WHEN HOME =>	
				motorInstruction(47 DOWNTO 40) <= "00000000";
				motorInstruction(39 DOWNTO 32) <= "00000001"; --Code 1
				motorInstruction(31 DOWNTO 24) <= "00000000";
				motorInstruction(23 DOWNTO 16) <= "00000000";
				motorInstruction(15 DOWNTO 8) <= "00000000";
				motorInstruction(7 DOWNTO 0) <= "00000000";
				motorWrite <= '1';
				IF motorDone = '1' THEN
					motorWrite <= '0';
					state <= WAIT_START;
					motorIsReset <= '1'; --Motor homed, acquisition can start
				END IF;	
			
			WHEN WAIT_START =>
				IF previousStart_Acq = '0' AND Start_Acq = '1' THEN
					--Reset everything at the start of the acquisition
					state <= RESTORE_SETTINGS;
				ELSIF Start_Acq = '1' THEN
					state <= INIT1;
				END IF;
												
			WHEN INIT1 =>					--Initialisation des variables
				IF Start_Acq = '1' THEN
					distanceFirstSlice_steps <= resolutionConvert*TO_INTEGER(distanceFirstSlice); --Approx
					distanceBetweenSlices_steps <= resolutionConvert*TO_INTEGER(distanceBetweenSlices); --Approx
					state <= INIT2;
				END IF;
			
			WHEN INIT2 =>					--�tat n�cessaire pour assigner current_x et current_y
				IF constantPullback = '0' THEN
					current_x <= distanceFirstSlice_steps;
				ELSE
					--First and only move has to be the total distance
					current_x <= distanceFirstSlice_steps + distanceBetweenSlices_steps;
				END IF;
				idxSlice <= 0;
				state <= ABSOLUTE_MOVE;
                IF Start_Acq = '0' THEN --Reset to home position when the acq is stopped or finished
                    state <= RESTORE_SETTINGS;
                END IF;
									
			WHEN ABSOLUTE_MOVE =>					--Envoi de la position aux moteurs	
				motorInstruction(47 DOWNTO 40) <= "00000001";	--Code pour le moteur 1 seulement
				motorInstruction(39 DOWNTO 32) <= "00010100";	--Code 20
				dataTemp := STD_LOGIC_VECTOR(TO_UNSIGNED(current_x,32));
				motorInstruction(31 DOWNTO 24) <= dataTemp(7 DOWNTO 0);
				motorInstruction(23 DOWNTO 16) <= dataTemp(15 DOWNTO 8);
				motorInstruction(15 DOWNTO 8) <= dataTemp(23 DOWNTO 16);
				motorInstruction(7 DOWNTO 0) <= dataTemp(31 DOWNTO 24);			
				IF Deplacement_Enable = '1' AND Start_Acq = '1' THEN	
					motorWrite <= '1';
					--Si Fin d'une position
					IF motorDone = '1' THEN
						motorWrite <= '0';
						Deplacement_Done <= '1';
                        IF constantPullback = '0' THEN
                            state <= CHANGE_POSITION;
                        ELSE
                            state <= DONE;
                        END IF;
					END IF;
				END IF;
                IF Start_Acq = '0' THEN --Reset to home position when the acq is stopped or finished
                    state <= RESTORE_SETTINGS;
                END IF;
                
			WHEN CHANGE_POSITION =>					--D�termination de la prochaine position
				IF Deplacement_Enable = '0' THEN --Make sure we detect rising edges
					idxSlice <= idxSlice + 1;
					IF idxSlice + 1 < nSlices THEN
						current_x <= current_x + distanceBetweenSlices_steps;	
						state <= ABSOLUTE_MOVE;
					ELSE
						state <= DONE;	--Fin du scan complet
					END IF;
                    IF distanceBetweenSlices_steps = 0 THEN
                        state <= DONE;	--Don't bother moving if the distance is 0.
                    END IF;
				ELSE
					Deplacement_Done <= '1';
					state <= CHANGE_POSITION;
				END IF;
                IF Start_Acq = '0' THEN --Reset to home position when the acq is stopped or finished
                    state <= RESTORE_SETTINGS;
                END IF;                
								
			WHEN DONE =>	--D�placement termin�. L'ordinateur cr�era un reset pour retourner � l'�tat initial 
                Deplacement_Done <= '1';
				IF Start_Acq = '0' THEN --Reset when acquisition is done
					state <= RESTORE_SETTINGS;
				END IF;
																		
			WHEN OTHERS =>
	
		END CASE;
	END IF;
END PROCESS;

LED(4 DOWNTO 0) <= "00000";
LED(5) <= '1';
	
END Moteurs_Controller_architecture;

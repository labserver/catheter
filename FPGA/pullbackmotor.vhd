LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;


--  Entity Declaration

ENTITY PullbackMotor IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		PORSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;
		pulse1kHz : IN STD_LOGIC; --Every ms, a new feedback value is available

		pullbackMotorDo : IN STD_LOGIC; --The acquisition module will tell this module to change slice. It will wait for the done signal
		pullbackMotorReset : IN STD_LOGIC; --Connected to startAcq, when '0' the motor slowly go to 0.
		pullbackMotorDone : OUT STD_LOGIC; --Tells the acquisition module that the pullback is done
		distanceBetweenSlices : IN UNSIGNED(11 DOWNTO 0); --Distance to move at eack pullback
		distanceFirstSlice : IN UNSIGNED(11 DOWNTO 0); --Distance to move at eack pullback
		
		pullbackMotorValue : BUFFER UNSIGNED(11 DOWNTO 0); --To DAC
		pullbackMotorFeedback : IN UNSIGNED(15 DOWNTO 0); --Feedback of the position of the motor
		
		pulserOnPullbackPos : OUT STD_LOGIC; --The pulser is <<<d at the end of the pullback to make sure the pulses will be uniform during the scan
		pulserOnPullbackNeg : OUT STD_LOGIC; --The pulser is activated at the end of the pullback to make sure the pulses will be uniform during the scan
        pulserOnPullbackRTZ : OUT STD_LOGIC; --The pulser is activated at the end of the pullback to make sure the pulses will be uniform during the scan
		
		test1 : BUFFER UNSIGNED(31 DOWNTO 0);
		test2 : BUFFER UNSIGNED(31 DOWNTO 0);
		error : BUFFER STD_LOGIC; --Error if the motor is turning in the wrong side
		LED : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END PullbackMotor;


--  Architecture Body 

ARCHITECTURE PullbackMotor_architecture OF PullbackMotor IS
	TYPE stateMachine IS (WAIT_DO, MOVE, MOVE_DELAY, FIRST_MOVE, FIRST_MOVE_DELAY, DONE_DELAY_INIT, DONE_DELAY, DONE, RESET, RESET_DELAY, ACTIVATE, ACTIVATE_DELAY);
	SIGNAL state : stateMachine;
	
	SIGNAL firstDo : STD_LOGIC; --First Do after a reset will go to the first distance, instead of the normal incrementation
	SIGNAL pulse1kHzPrevious : STD_LOGIC; --Used to detect a rising edge on pulse1kHz
	SIGNAL nextPullbackMotorValue : UNSIGNED(11 DOWNTO 0); --Position we are going to
	SIGNAL counterMoveDelay : INTEGER RANGE 0 TO 800000000; --Counter used to slowly move the motors
	SIGNAL counterDoneDelay : INTEGER RANGE 0 TO 200000000; --Counter used to verify if the motors have finished moving
	SIGNAL counterResetDelay : INTEGER RANGE 0 TO 50000000; --Counter used to reset very slowly the motors
	SIGNAL counterPulser : UNSIGNED(31 DOWNTO 0); --Counter used to pulse even when the motors are moving
	SIGNAL pullbackMotorFeedback1 : UNSIGNED(31 DOWNTO 0); --Averaged value of the feedback to verify if the motors are stable
	SIGNAL pullbackMotorFeedbackAverage : UNSIGNED(31 DOWNTO 0); --Incrementing averaged value of the feedback to verify if the motors are stable
		
BEGIN

--LED(0) <= '1' WHEN state = WAIT_DO ELSE '0';
--LED(1) <= '1' WHEN state = WAIT_DO ELSE '0';
--LED(2) <= '1' WHEN state = DONE ELSE '0';
--LED(3) <= '1' WHEN state = ACTIVATE ELSE '0';

PROCESS(PORSTn, clk48MHz)

BEGIN
			
IF PORSTn = '0' THEN
	state <= ACTIVATE;
	pullbackMotorValue <= "001000000000"; --We have to activate the motor first
	nextPullbackMotorValue <= (OTHERS => '0');
	counterMoveDelay <= 0;
	counterResetDelay <= 0;
	counterPulser <= (OTHERS => '0');
	pullbackMotorDone <= '0';
	error <= '0';
	firstDo <= '1';
	pulserOnPullbackPos <= '0';
	pulserOnPullbackNeg <= '0';
    pulserOnPullbackRTZ <= '0';
	pulse1kHzPrevious <= '0';
	test1 <= (OTHERS => '0');
	test2 <= (OTHERS => '0');
    
ELSIF (clk48MHz'EVENT AND clk48MHz = '1') THEN
	error <= '0';
	pullbackMotorDone <= '0';
	counterMoveDelay <= 0;
	counterResetDelay <= 0;
	counterPulser <= (OTHERS => '0');
	pulserOnPullbackPos <= '0';
	pulserOnPullbackNeg <= '0';
    pulserOnPullbackRTZ <= '0';
	--LED(0) <= '0';
	--LED(1) <= '0';
	pulse1kHzPrevious <= '0';
	CASE state IS
		WHEN WAIT_DO => --Waiting for a start signal
            LED <= "000";
			--LED(0) <= '1';
			IF pullbackMotorDo = '1' THEN
				state <= MOVE;
				IF (RESIZE(pullbackMotorValue,13) +  distanceBetweenSlices) > 4095 THEN 
					nextPullbackMotorValue <= (OTHERS => '1'); --Make sure we are in a valid range
					error <= '1';
				ELSE
					IF firstDo = '1' THEN
						nextPullbackMotorValue <= distanceFirstSlice;
                        state <= FIRST_MOVE;
						firstDo <= '0';
					ELSE
						nextPullbackMotorValue <= pullbackMotorValue + distanceBetweenSlices;
						IF distanceBetweenSlices = 0 THEN
                            --BUG: Don't go directly to done (no delay)...
							state <= DONE_DELAY; --Don't move if we don't have to
						END IF;
					END IF;
				END IF;
			END IF;
			IF pullbackMotorReset = '1' THEN
				state <= RESET;
			END IF;				
			
		WHEN MOVE => --Increment the DAC value to move a little
            LED <= "001";
			IF pullbackMotorValue < nextPullbackMotorValue THEN
				pullbackMotorValue <= pullbackMotorValue + 1;
				state <= MOVE_DELAY;
                counterMoveDelay <= 0;
			ELSE
				state <= DONE_DELAY_INIT;
			END IF;
			IF pullbackMotorReset = '1' THEN
				state <= RESET;
			END IF;				
		
		WHEN MOVE_DELAY => --Wait to move slowly
            LED <= "010";
			counterMoveDelay <= counterMoveDelay + 1;
			IF counterMoveDelay > 480000 THEN--********************************
				state <= MOVE;
                counterMoveDelay <= 0;
			END IF;
			IF pullbackMotorReset = '1' THEN
				state <= RESET;
			END IF;					

		WHEN DONE_DELAY_INIT => 
            LED <= "011";
			counterMoveDelay <= counterMoveDelay + 1;
			IF counterMoveDelay > 9600000 THEN --Wait at least 200ms
				state <= DONE_DELAY;
				counterMoveDelay <= 0;
			END IF;
			IF pullbackMotorReset = '1' THEN
				state <= RESET;
			END IF;	
			
		WHEN DONE_DELAY => --Wait until the motor finished moving (at least 0.1seconds where the feedback value is stable)
            LED <= "100";
			pulse1kHzPrevious <= pulse1kHz;
			IF pulse1kHz = '1' AND pulse1kHzPrevious = '0' THEN --Rising edge every ms
				counterMoveDelay <= counterMoveDelay + 1;
				IF counterMoveDelay = 199 THEN --200ms
					pullbackMotorFeedback1 <= pullbackMotorFeedbackAverage + pullbackMotorFeedback; --200 averaged feedback values
					pullbackMotorFeedbackAverage <= (OTHERS => '0'); --Reset averaging
				ELSIF counterMoveDelay >= 400 THEN --400ms
					--If the difference between the two sets of averaged feedback value is larger than 20000, the motors are not stable
					--Acceptable difference: 200=1mm
--					IF pullbackMotorFeedback1 > pullbackMotorFeedbackAverage + 400000 OR pullbackMotorFeedbackAverage > pullbackMotorFeedback1 + 400000 THEN
--						--We stay here...
--					ELSE
						test1 <= pullbackMotorFeedback1;
						test2 <= pullbackMotorFeedbackAverage;					
						state <= DONE;					
--					END IF;	
					pullbackMotorFeedback1 <= (OTHERS => '0');
					pullbackMotorFeedbackAverage <= (OTHERS => '0');						
					counterMoveDelay <= 0;
				ELSE
					pullbackMotorFeedbackAverage <= pullbackMotorFeedbackAverage + pullbackMotorFeedback; --Average the feedback value
				END IF;
			ELSE
				counterMoveDelay <= counterMoveDelay;
			END IF;
			--LED(3) <= '1';	
			counterPulser <= counterPulser + 1;
			IF counterMoveDelay < 400 THEN --Except at the last 1ms...
				IF counterPulser(9 DOWNTO 0) = "1111000000" THEN --Every 10�s, we pulse to keep the pulser stable during the acquisition
					pulserOnPullbackPos <= '1';
				ELSIF counterPulser(9 DOWNTO 0) = "1111000001" THEN --Every 10�s, we pulse to keep the pulser stable during the acquisition
					pulserOnPullbackNeg <= '1';		
				ELSIF counterPulser(9 DOWNTO 6) = "1111" THEN --Every 10�s, we pulse to keep the pulser stable during the acquisition
					pulserOnPullbackRTZ <= '1';
				END IF;		                
			END IF;
			IF pullbackMotorReset = '1' THEN
				state <= RESET;
			END IF;	
			
		WHEN DONE =>
            LED <= "101";
			pullbackMotorDone <= '1';
			--LED(0) <= '1';
			IF pullbackMotorDo <= '0' THEN --Make sure the acquisition module as received the Done signal
				state <= WAIT_DO;
			END IF;
			IF pullbackMotorReset = '1' THEN
				state <= RESET;
			END IF;			
		
		WHEN RESET =>
            LED <= "110";
			IF pullbackMotorValue > 0 THEN
				pullbackMotorValue <= pullbackMotorValue - 1;
				state <= RESET_DELAY;
			ELSE
				firstDo <= '1';			
				state <= WAIT_DO;
			END IF;	
			
		WHEN RESET_DELAY => --Wait to reset very slowly
            LED <= "111";
			counterResetDelay <= counterResetDelay + 1;
			IF counterResetDelay > 960000 THEN
				state <= RESET;
			END IF;

		WHEN FIRST_MOVE => --Increment the DAC value to move a little
            LED <= "001";
			IF pullbackMotorValue < nextPullbackMotorValue THEN
				pullbackMotorValue <= pullbackMotorValue + 1;
				state <= FIRST_MOVE_DELAY;
                counterMoveDelay <= 0;
			ELSE
				state <= DONE_DELAY_INIT;
			END IF;
			IF pullbackMotorReset = '1' THEN
				state <= RESET;
			END IF;				
		
		WHEN FIRST_MOVE_DELAY => --Wait to move slowly
            LED <= "010";
			counterMoveDelay <= counterMoveDelay + 1;
			IF counterMoveDelay > 480000 THEN--********************************
				state <= FIRST_MOVE;
                counterMoveDelay <= 0;
			END IF;
			IF pullbackMotorReset = '1' THEN
				state <= RESET;
			END IF;	
			
		WHEN ACTIVATE => --We have to send a certain voltage to the motor in order to activate it, because the motor is scanning all its input for a signal.
            LED <= "100";
			IF pullbackMotorValue > 0 THEN
				pullbackMotorValue <= pullbackMotorValue - 1;
				state <= ACTIVATE_DELAY;
			ELSE
				firstDo <= '1';			
				state <= WAIT_DO;
			END IF;				
			
		WHEN ACTIVATE_DELAY => --The activate voltage is rapidly put to 0.
            LED <= "001";
			counterResetDelay <= counterResetDelay + 1;
			IF counterResetDelay > 48000 THEN
				state <= ACTIVATE;
			END IF;
						
		WHEN OTHERS =>
			state <= WAIT_DO;
		
	END CASE;

END IF;
END PROCESS;

	
END PullbackMotor_architecture;				
				
				
				
				
## Generated SDC file "PAUSFluo2.out.sdc"

## Copyright (C) 1991-2012 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 12.1 Build 177 11/07/2012 SJ Web Edition"

## DATE    "Fri Feb 08 06:07:50 2013"

##
## DEVICE  "EP3C16Q240C8"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {IFCLK} -period 20.833 -waveform { 0.000 10.416 } [get_ports {IFCLK}]
create_clock -name {ADC_CLKOUT} -period 5.000 -waveform { 0.000 2.500 } [get_ports {ADC_CLKOUT}]
create_clock -name {clkGenerator:inst34|clk1MHz} -period 1000.000 -waveform { 0.000 500.000 } [get_registers {clkGenerator:inst34|clk1MHz}]
create_clock -name {clkGenerator:inst34|clk2KHz} -period 1000.000 -waveform { 0.000 500.000 } [get_registers {clkGenerator:inst34|clk2KHz}]
create_clock -name {clkGenerator:inst34|clk2Hz} -period 1000.000 -waveform { 0.000 500.000 } [get_registers {clkGenerator:inst34|clk2Hz}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {inst7|altpll_component|auto_generated|pll1|clk[0]} -source [get_pins {inst7|altpll_component|auto_generated|pll1|inclk[0]}] -duty_cycle 50.000 -multiply_by 1 -master_clock {IFCLK} [get_pins {inst7|altpll_component|auto_generated|pll1|clk[0]}] 
create_generated_clock -name {inst7|altpll_component|auto_generated|pll1|clk[1]} -source [get_pins {inst7|altpll_component|auto_generated|pll1|inclk[0]}] -duty_cycle 50.000 -multiply_by 2 -master_clock {IFCLK} [get_pins {inst7|altpll_component|auto_generated|pll1|clk[1]}] 
create_generated_clock -name {inst7|altpll_component|auto_generated|pll1|clk[2]} -source [get_pins {inst7|altpll_component|auto_generated|pll1|inclk[0]}] -duty_cycle 50.000 -multiply_by 2 -phase -105.000 -master_clock {IFCLK} [get_pins {inst7|altpll_component|auto_generated|pll1|clk[2]}] 
create_generated_clock -name {inst7|altpll_component|auto_generated|pll1|clk[3]} -source [get_pins {inst7|altpll_component|auto_generated|pll1|inclk[0]}] -duty_cycle 50.000 -multiply_by 1 -divide_by 2 -master_clock {IFCLK} [get_pins {inst7|altpll_component|auto_generated|pll1|clk[3]}] 
create_generated_clock -name {inst|altpll_component|auto_generated|pll1|clk[0]} -source [get_pins {inst|altpll_component|auto_generated|pll1|inclk[0]}] -duty_cycle 50.000 -multiply_by 1 -master_clock {ADC_CLKOUT} [get_pins {inst|altpll_component|auto_generated|pll1|clk[0]}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk2Hz}] -rise_to [get_clocks {clkGenerator:inst34|clk2Hz}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk2Hz}] -fall_to [get_clocks {clkGenerator:inst34|clk2Hz}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk2Hz}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.070  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk2Hz}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk2Hz}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.070  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk2Hz}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk2Hz}] -rise_to [get_clocks {clkGenerator:inst34|clk2Hz}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk2Hz}] -fall_to [get_clocks {clkGenerator:inst34|clk2Hz}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk2Hz}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.070  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk2Hz}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk2Hz}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.070  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk2Hz}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {clkGenerator:inst34|clk2Hz}] -setup 0.100  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {clkGenerator:inst34|clk2Hz}] -hold 0.070  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {clkGenerator:inst34|clk2Hz}] -setup 0.100  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {clkGenerator:inst34|clk2Hz}] -hold 0.070  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}]  0.150  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}]  0.150  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {clkGenerator:inst34|clk2KHz}] -setup 0.100  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {clkGenerator:inst34|clk2KHz}] -hold 0.070  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {clkGenerator:inst34|clk2KHz}] -setup 0.100  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {clkGenerator:inst34|clk2KHz}] -hold 0.070  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {clkGenerator:inst34|clk1MHz}] -setup 0.100  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {clkGenerator:inst34|clk1MHz}] -hold 0.070  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {clkGenerator:inst34|clk1MHz}] -setup 0.100  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {clkGenerator:inst34|clk1MHz}] -hold 0.070  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {clkGenerator:inst34|clk2Hz}] -setup 0.100  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {clkGenerator:inst34|clk2Hz}] -hold 0.070  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {clkGenerator:inst34|clk2Hz}] -setup 0.100  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {clkGenerator:inst34|clk2Hz}] -hold 0.070  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}]  0.150  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}]  0.150  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {clkGenerator:inst34|clk2KHz}] -setup 0.100  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {clkGenerator:inst34|clk2KHz}] -hold 0.070  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {clkGenerator:inst34|clk2KHz}] -setup 0.100  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {clkGenerator:inst34|clk2KHz}] -hold 0.070  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {clkGenerator:inst34|clk1MHz}] -setup 0.100  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {clkGenerator:inst34|clk1MHz}] -hold 0.070  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {clkGenerator:inst34|clk1MHz}] -setup 0.100  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {clkGenerator:inst34|clk1MHz}] -hold 0.070  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[1]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.150  
set_clock_uncertainty -rise_from [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.150  
set_clock_uncertainty -rise_from [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.150  
set_clock_uncertainty -fall_from [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  0.150  
set_clock_uncertainty -fall_from [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk2KHz}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.070  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk2KHz}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk2KHz}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.070  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk2KHz}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk2KHz}] -rise_to [get_clocks {clkGenerator:inst34|clk2KHz}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk2KHz}] -fall_to [get_clocks {clkGenerator:inst34|clk2KHz}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk2KHz}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.070  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk2KHz}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk2KHz}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.070  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk2KHz}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk2KHz}] -rise_to [get_clocks {clkGenerator:inst34|clk2KHz}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk2KHz}] -fall_to [get_clocks {clkGenerator:inst34|clk2KHz}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk1MHz}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.070  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk1MHz}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk1MHz}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.070  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk1MHz}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk1MHz}] -rise_to [get_clocks {clkGenerator:inst34|clk2KHz}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk1MHz}] -fall_to [get_clocks {clkGenerator:inst34|clk2KHz}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk1MHz}] -rise_to [get_clocks {clkGenerator:inst34|clk1MHz}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {clkGenerator:inst34|clk1MHz}] -fall_to [get_clocks {clkGenerator:inst34|clk1MHz}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk1MHz}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.070  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk1MHz}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk1MHz}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -setup 0.070  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk1MHz}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -hold 0.100  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk1MHz}] -rise_to [get_clocks {clkGenerator:inst34|clk2KHz}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk1MHz}] -fall_to [get_clocks {clkGenerator:inst34|clk2KHz}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk1MHz}] -rise_to [get_clocks {clkGenerator:inst34|clk1MHz}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clkGenerator:inst34|clk1MHz}] -fall_to [get_clocks {clkGenerator:inst34|clk1MHz}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -rise_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}] -fall_to [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[3]}]  0.020  


#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************

set_false_path  -from  [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]  -to  [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path  -from  [get_clocks {inst|altpll_component|auto_generated|pll1|clk[0]}]  -to  [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path  -from  [get_clocks {clkGenerator:inst34|clk1MHz}]  -to  [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path  -from  [get_clocks {clkGenerator:inst34|clk2KHz}]  -to  [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path  -from  [get_clocks {clkGenerator:inst34|clk2Hz}]  -to  [get_clocks {inst7|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path -from [get_keepers {*rdptr_g*}] -to [get_keepers {*ws_dgrp|dffpipe_qe9:dffpipe16|dffe17a*}]
set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*rs_dgwp|dffpipe_pe9:dffpipe12|dffe13a*}]
set_false_path -from [get_keepers {*rdptr_g*}] -to [get_keepers {*ws_dgrp|dffpipe_fd9:dffpipe15|dffe16a*}]
set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*rs_dgwp|dffpipe_ed9:dffpipe12|dffe13a*}]


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************


LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration

ENTITY PAUSDataDivide IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		PAUSDataDivisor : IN INTEGER RANGE 0 TO 7; --If '1', when need to concatenate two samples in one data
		
		dataIn : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		dataOut : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
		
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END PAUSDataDivide;

--  Architecture Body 

ARCHITECTURE PAUSDataDivide_architecture OF PAUSDataDivide IS

BEGIN

dataOut <= dataIn(31) & dataIn(31 DOWNTO 1) WHEN PAUSDataDivisor = 1 ELSE
	dataIn(31) & dataIn(31) & dataIn(31 DOWNTO 2) WHEN PAUSDataDivisor = 2 ELSE
	dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31 DOWNTO 3) WHEN PAUSDataDivisor = 3 ELSE
	dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31 DOWNTO 4) WHEN PAUSDataDivisor = 4 ELSE
	dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31 DOWNTO 5) WHEN PAUSDataDivisor = 5 ELSE
	dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31 DOWNTO 6) WHEN PAUSDataDivisor = 6 ELSE
	dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31) & dataIn(31 DOWNTO 7) WHEN PAUSDataDivisor = 7 ELSE
	dataIn;


END PAUSDataDivide_architecture;

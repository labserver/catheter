LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY MotorDoneDelay IS
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;
		enableAcq : IN STD_LOGIC;
		constantPullback : IN STD_LOGIC; 
		
		distanceFirstSlice : IN UNSIGNED(15 DOWNTO 0); --In steps of 0.1mm
		linearMotorSpeed : IN UNSIGNED(15 DOWNTO 0); --In steps of 0.1mm/s
		
		
		pullbackMotorDo : IN STD_LOGIC;
		pullbackMotorDoneIn : IN STD_LOGIC;
		pullbackMotorDoneOut : OUT STD_LOGIC;
		
		LED : BUFFER STD_LOGIC_VECTOR(5 DOWNTO 0)
	);
	
END MotorDoneDelay;

--  Architecture Body 
ARCHITECTURE MotorDoneDelay_architecture OF MotorDoneDelay IS
	CONSTANT CLK_FREQ : INTEGER := 48000000;
	
	TYPE stateMachine IS (WAIT_ENABLE_ACQ, DELAY_CALC, DELAY, DELAY_END);
	SIGNAL state : stateMachine;
	SIGNAL delayTime : INTEGER RANGE 0 TO 2000000000 := 50400000;
	SIGNAL counterDelayTime : INTEGER RANGE 0 TO 2000000000;
	
BEGIN

PROCESS(clk48MHz, RSTn, enableAcq)
BEGIN
IF (RSTn = '0' OR enableAcq = '0') THEN
	state <= WAIT_ENABLE_ACQ;
	pullbackMotorDoneOut <= '0';
	counterDelayTime <= 0;
	delayTime <= 50400000;
	LED <= (OTHERS => '0');
	
ELSIF (clk48MHz'EVENT AND clk48MHz = '1') THEN	
pullbackMotorDoneOut <= '0';
CASE state IS
	WHEN WAIT_ENABLE_ACQ =>	
		LED <= (OTHERS => '0');
		IF constantPullback = '0' THEN
			pullbackMotorDoneOut <= pullbackMotorDoneIn;
		ELSIF pullbackMotorDo = '1' THEN
			delayTime <= TO_INTEGER(distanceFirstSlice)/10*(CLK_FREQ/TO_INTEGER(linearMotorSpeed));
			counterDelayTime <= 0;
			state <= DELAY;
		END IF;
								
	WHEN DELAY_CALC => --Let some time to do the big calculation
		counterDelayTime <= counterDelayTime + 1;
		IF counterDelayTime > 100 THEN
			state <= DELAY;
		END IF;
		
	WHEN DELAY =>
		counterDelayTime <= counterDelayTime + 1;
		IF counterDelayTime > delayTime THEN
			LED <= (OTHERS => '1');
			pullbackMotorDoneOut <= '1';
			state <= DELAY_END;
		END IF;
		
	WHEN DELAY_END => --Make sure that we don't detect another motorDo in the same acquisition
		pullbackMotorDoneOut <= '1'; --Don't wait for the motor anymore
		
	WHEN OTHERS =>
END CASE;
END IF;
END PROCESS;


END MotorDoneDelay_architecture;

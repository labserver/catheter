---------------------------------------------------------------
-- Test Bench for Multiplexer (ESD figure 2.5)
-- by Weijun Zhang, 04/2001
--
-- four operations are tested in this example.
---------------------------------------------------------------

library IEEE;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

entity TB_Encoder is			-- empty entity
end TB_Encoder;

---------------------------------------------------------------

architecture TB of TB_Encoder is

    -- initialize the declared signals  
    signal T_RSTn : STD_LOGIC;
    signal T_clk48MHz : STD_LOGIC;
    signal T_CH1 : STD_LOGIC;	--Signal from the optical encoder
    signal T_CH2 : STD_LOGIC;	--Signal from the optical encoder
    signal T_motorPosition : UNSIGNED(19 downto 0); --Position of the motor
    signal T_motorPositionChanged : STD_LOGIC;
    signal T_error : STD_LOGIC; --Error if the motor is turning in the wrong side
    signal T_LED : STD_LOGIC;
	
    component Encoder
		PORT
		(
			RSTn : IN STD_LOGIC;
			clk48MHz : IN STD_LOGIC;
			CH1 : IN STD_LOGIC;	--Signal from the optical encoder
			CH2 : IN STD_LOGIC;	--Signal from the optical encoder
			motorPosition : BUFFER UNSIGNED(19 downto 0); --Position of the motor
			motorPositionChanged : OUT STD_LOGIC;
			error : BUFFER STD_LOGIC; --Error if the motor is turning in the wrong side
			LED : OUT STD_LOGIC
		);
    end component;

begin

    U_Encoder: Encoder port map (T_RSTn, T_clk48MHz, T_CH1, T_CH2, T_motorPosition, T_motorPositionChanged, T_error, T_LED);

	RSTn_gen : PROCESS IS
	BEGIN
		T_RSTn <= '0';
		WAIT FOR 100 ns;
		T_RSTn <= '1';
		WAIT;
	END PROCESS;	

	clock_gen : PROCESS IS
	BEGIN
		T_clk48MHz <= '0' after 10 ns, '1' after 20 ns;
		WAIT FOR 20 ns;	
	END PROCESS;

	PROCESS IS
	BEGIN
		T_CH1 <= '0';	
		FOR i in 0 TO 100000 LOOP
			WAIT FOR 5000 ns;
			T_CH1 <= '0';
			WAIT FOR 5000 ns;
			T_CH1 <= '1';			
		END LOOP;
	END PROCESS;
	
	PROCESS IS
	BEGIN
		T_CH2 <= '0';	
		WAIT FOR 2500 ns;
		FOR i in 0 TO 100000 LOOP
			WAIT FOR 5000 ns;
			T_CH2 <= '0';
			WAIT FOR 5000 ns;
			T_CH2 <= '1';			
		END LOOP;
	END PROCESS;

end TB;

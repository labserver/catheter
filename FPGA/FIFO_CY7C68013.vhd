--COMMENTS:
--
--This module communicates with the Cypress microcontroller CY7C68013 through a 16-bits bidirectional FIFO bus.
--
--Version: 1.1
--
--Codes of the data received from the computer
--A code is the LSW of the first data (32bits) received from the computer.
--Code 2: Receive a set of data. MSW represents the number of 32-bits data to receive.
--Code 4: Reset the VHDL code.
--Code 6: Change only one data in the set (usually during the acquisition). MSW represents the index of the data to change.
--
--Transfer from computer to FPGA: now 256Mbps (if necessary it could be accelerated a bit, but extremely difficult due to severe timing constraints)
--
--Transfer from FPGA to computer could be faster: adjustable up to 365 Mbps (the bottleneck is NOT the FPGA at this speed)

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY FIFO_CY7C68013 IS
	GENERIC(WriteNCyclesDelay : NATURAL := 1); --Used to add a delay when writing to the FX2 uC. Put 0 to achieve the fastest transfer. A delay of 2 is safer and allows a transfer of 250 Mbps.
	PORT
	(
		RSTn : IN STD_LOGIC;
		RSTnUser : OUT STD_LOGIC; --Signal sent from the computer (code 4) to reset the FPGA program
		clk48MHz : IN STD_LOGIC;
		
		--Communication with the computer via the FX2 uC
		FLAGS : IN STD_LOGIC_VECTOR(2 DOWNTO 0); --Prog-level(2)(Default = 1) - FULLn(1) - EMPTYn(0)
		SLOE : OUT STD_LOGIC; --Output enable of the FIFO bus (0 = uC in control, 1 = FPGA in control)	
		FIFOADR : OUT STD_LOGIC_VECTOR(1 DOWNTO 0); --EP6 = 10 (Read), EP8 = 11 (Write)
		PKTEND : OUT STD_LOGIC; --0 to end a packet
		SLCSn : OUT STD_LOGIC; --0 to activate
		SLWR : OUT STD_LOGIC; --0 to write
		SLRD : OUT STD_LOGIC; --0 to read
		FD : INOUT STD_LOGIC_VECTOR(15 DOWNTO 0); --Bidirectionnal FIFO bus
		
		--Communications with the other VHDL modules to write to the computer 
		--On-chip memory or VHDL logic will always have data available and can be accessed by address.
		--FIFO buffers and external RAM might not always have data available. Changes on the index of the data to send is monitored by the external module to prepare the next data, but the absolute value is not used.
		dataToFIFO : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --Data from the other VHDL modules to transfer to the computer
		idxSend : BUFFER INTEGER RANGE 0 TO 2147483647; --Index of the data to send.
		dataAvailable : IN STD_LOGIC; --Assert to '1' if data is always available. On the clock cycle following the rising edge, the data is sent to the FIFO and idxSend is incremented (the write delay is done before monitoring dataAvailable).
		enableTransfer : IN STD_LOGIC; --Signal that enables the transfer.
	
		--Data received from the computer
		dataFromFIFO : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --Data received from the computer
		idxReceive : OUT INTEGER RANGE 0 TO 65535; --Index of the current data being received from the computer. Starts at 1.

		--Debug
		error : OUT STD_LOGIC;	
		LED : BUFFER STD_LOGIC_VECTOR(3 DOWNTO 0)		
	);
END FIFO_CY7C68013;

--  Architecture Body
ARCHITECTURE FIFO_CY7C68013_architecture OF FIFO_CY7C68013 IS
	CONSTANT EP6 : STD_LOGIC_VECTOR(1 DOWNTO 0) := "10"; --Reading endpoint
	CONSTANT EP8 : STD_LOGIC_VECTOR(1 DOWNTO 0) := "11"; --Writing endpoint
	TYPE stateMachine IS (WAIT_FOR_REQUEST, READING, WRITING, WRITE_DELAY, WRITE_END);
	SIGNAL state : stateMachine;	
	SIGNAL writeMSW : STD_LOGIC := '0'; --Used to switch between MSW and LSW to send 32bits in 2 X 16bits.							                  
	SIGNAL readMSW : STD_LOGIC := '0'; --Used to switch between MSW and LSW to receive 32bits in 2 X 16bits.
	SIGNAL waiting : INTEGER RANGE -20 TO 200 := 0; --Used to wait a given number of cycles in a state
	SIGNAL modifOneParameter : STD_LOGIC; --Signal to indicate that we are not waiting for another parameter (reset idxReceive)
	SIGNAL idxReceiveNext :  INTEGER RANGE 0 TO 65535; --Index of the current parameter being transferred from the computer. Starts at 1.
	SIGNAL quantityToReceive : INTEGER RANGE 0 TO 65535; --Number of data to receive. This is set after a code is received
	SIGNAL receiveCode : INTEGER RANGE 0 TO 65535; --Receive code to determine which action to take
	SIGNAL dataFromFIFOLSW : STD_LOGIC_VECTOR(15 DOWNTO 0); --Used to store LSW and assign the complete the 32-bit data in a single clock cycle
BEGIN
PROCESS(RSTn, clk48MHz)
BEGIN
IF (RSTn = '0') THEN
	RSTnUser <= '1';
	SLOE <= '0';		
	FIFOADR <= EP6;
	PKTEND <= '1';
	SLWR <= '1';	
	SLRD <= '1';
	idxSend <= 0;		
	FD <= (OTHERS => 'Z');           
	state <= WAIT_FOR_REQUEST;		
	writeMSW <= '0';
	readMSW <= '0';
	idxReceive <= 0;
	idxReceiveNext <= 0;
	waiting <= 0;
	LED <= (OTHERS => '0');
	error <= '0';
	modifOneParameter <= '0';
	dataFromFIFO <= (OTHERS => '0');
	receiveCode <= 0;			
ELSIF (clk48MHz'EVENT AND clk48MHz = '1') THEN
	RSTnUser <= '1';
	PKTEND <= '1';	 			
	SLRD <= '1';
	SLWR <= '1';
	SLOE <= '1';			
	waiting <= 0;
	error <= '0';
	LED <= (OTHERS => '0');
	CASE state IS						
		WHEN WAIT_FOR_REQUEST => --Waiting for data from the computer or for a trigger to transfer data to the computer
			FD <= (OTHERS => 'Z');
			FIFOADR <= EP6;
			SLOE <= '0'; --Cypress is in control of the bus
			IF FLAGS(0) = '1' THEN --Data available from FX2	
				state <= READING;
			ELSIF enableTransfer = '1' THEN --Transfer request
				FIFOADR <= EP8; --Endpoint change (write)
				idxSend <= 0;
				writeMSW <= '0';
				state <= WRITING; --Start writing
			END IF;
				
		WHEN READING => --Reception of the codes and parameters	
			FD <= (OTHERS => 'Z');
			FIFOADR <= EP6;
			state <= WAIT_FOR_REQUEST;
			IF FLAGS(0) = '1' THEN
				SLOE <= '0'; --Cypress is in control of the bus
				SLRD <= '0'; --Change data for the next reading	
				readMSW <= NOT(readMSW); --Switch between LSW and MSW				
				IF readMSW <= '0' THEN
					dataFromFIFOLSW <= FD; --Keep LSW
				ELSE
					dataFromFIFO <= FD & dataFromFIFOLSW; --Assign output
					idxReceive <= idxReceiveNext; --Output index is updated at the same time as the data is changed.
					idxReceiveNext <= idxReceiveNext + 1;
				END IF;
				IF idxReceiveNext = 0 AND readMSW = '0' THEN --Reception of the code
					receiveCode <= TO_INTEGER(UNSIGNED(FD(15 DOWNTO 0)));
				ELSIF idxReceiveNext = 0 AND readMSW = '1' THEN --Reception following the code
					IF receiveCode = 2 THEN
						quantityToReceive <= TO_INTEGER(UNSIGNED(FD));				
					ELSIF receiveCode = 4 THEN		--Reset all (code 4)
						idxReceiveNext <= 0;
						idxReceive <= 0;
						RSTnUser <= '0';									
					ELSIF receiveCode = 6 THEN --Modification of one parameter only (code 6)
						idxReceiveNext <= TO_INTEGER(UNSIGNED(FD));
						modifOneParameter <= '1';		
					ELSE --Wrong code is detected (Should never happen)
						idxReceiveNext <= 0;
						idxReceive <= 0;
						receiveCode <= 0;
						error <= '1';															
					END IF;
				END IF;
                
                --Reset the receive index when all the data have been received					
				IF ((receiveCode = 2 AND idxReceiveNext >= quantityToReceive) OR modifOneParameter = '1') AND idxReceiveNext > 0 AND readMSW = '1' THEN
					idxReceiveNext <= 0;
					receiveCode <= 0; --Receive code is reset.
					modifOneParameter <= '0';
				END IF;
			END IF;

		WHEN WRITING => --Writing on the FIFO bus
			FD <= FD;
			FIFOADR <= EP8;
			IF FLAGS(1)= '1' AND (dataAvailable = '1' OR enableTransfer = '0') THEN --If the endpoint is not full and the data to transfer is available...
				writeMSW <= NOT(writeMSW); -- Switch between MSW and LSW
				IF writeMSW = '0' THEN			
					FD <= dataToFIFO(15 DOWNTO 0);	--LSW first				
				ELSE
					FD <= dataToFIFO(31 DOWNTO 16);	--Followed by MSW
				END IF;		
				SLWR <= '0'; -- Write.	
				IF enableTransfer = '0' AND writeMSW = '1' THEN --Transfer finished
					state <= WRITE_END;							
				ELSE
					IF writeMSW = '1' THEN --Both MSW and LSW have been written
						idxSend <= idxSend + 1; --Change data
						writeMSW <= '0';
					END IF;		
					IF WriteNCyclesDelay > 0 OR (TO_UNSIGNED(idxSend,16) mod 128 = TO_UNSIGNED(127,16) AND writeMSW = '1') THEN	
						state <= WRITE_DELAY; --A delay is also required after writing 512bytes, to be able to get the full flag before writing again
                        IF (TO_UNSIGNED(idxSend,16) mod 128 = TO_UNSIGNED(127,16) AND writeMSW = '1') THEN
                            waiting <= -10;
                        END IF;
					END IF;	
				END IF;
			END IF;				
										
		WHEN WRITE_DELAY => --Delay used to make sure that the data from other VHDL modules has enough time to be changed
			FD <= FD;
			FIFOADR <= EP8;
			waiting <= waiting + 1;
			--Waiting is adjustable, depending on the speed of the other modules
			IF waiting >= WriteNCyclesDelay-1 THEN --Enough wait
				state <= WRITING;
				waiting <= 0;
			END IF;					
				
		WHEN WRITE_END => --End of a writing. Delay for PKTEND and to make sure that the data is written
			FD <= FD;		
			FIFOADR <= EP8;
			waiting <= waiting + 1;
			--Waiting values are arbitrary (not optimal)
			IF waiting > 11 THEN --Enough wait
				FIFOADR <= EP6;
				FD <= (OTHERS => 'Z');
				state <= WAIT_FOR_REQUEST;
				waiting <= 0;
				idxSend <= 0;
			ELSIF waiting > 10 THEN
				FIFOADR <= EP6;
				FD <= (OTHERS => 'Z');
			ELSIF waiting = 6 THEN
				IF FLAGS(1)= '0' THEN
					waiting <= waiting; --Make sure that the endpoint is not full before commiting the zero-length packet
				END IF;
			ELSIF waiting = 7 OR waiting = 8 THEN --At least two cycles before a PKTEND as stated in the FX2 uC datasheet
				PKTEND <= '0'; --2 end of a packet to send a zero-length packet		
			END IF;				
				
		WHEN OTHERS =>
			FD <= (OTHERS => 'Z');
			FIFOADR <= EP6;
			state <= WAIT_FOR_REQUEST;
	END CASE;
END IF;
END PROCESS;

SLCSn <= '0'; -- Cypress is always activated
 
END FIFO_CY7C68013_architecture;
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration

ENTITY Demodulation IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;
		enableFluo : IN STD_LOGIC; --Starting the acquisition of a new position (after the photoacoustic laser pulse)
		demodReady : OUT STD_LOGIC; --The demodulation process is read to demodulate a new position
		
		--Parameters received from Matlab (see Matlab code for information)
		fluoSingleMeasureOn : IN STD_LOGIC; --Similar to disabling demodulation, but only one measure per angular position
        demodulationOn : IN STD_LOGIC;
		nCyclesDemod : IN UNSIGNED(15 DOWNTO 0);
		
		--Transfer datas and communication	
		transferring : IN STD_LOGIC; --Used to verify that the last transfer is finished and the current transfer starts before continuing demodulation
		transferReady : OUT STD_LOGIC; --Indicates to the transfer module that the demodulation for 4 positions is done
		RSTnFluo : OUT STD_LOGIC; --Reset the square waves and the ADC recption module to synchronize the demodulation process at the end of a transfer
		saturation : OUT STD_LOGIC_VECTOR(1 DOWNTO 0); --Saturation of the 2 detectors
		addressFluo : IN UNSIGNED(1 DOWNTO 0); --Address to select which output to transfer to the computer
		dataDemodulation : OUT SIGNED(31 DOWNTO 0);	--Output to the computer (1 of the 4 demodulation signals multiplexed)
        ADCReadingEnable : OUT STD_LOGIC; --Used to deactivate the ADC reading before receiving the trigger (enableFluo). It allows to have the exact same timing, especially if a reading would have started just before the trigger.
        demodulationDone : OUT STD_LOGIC; --Used to shut down the laser diode
		
        --Duration of the demodulation at each of the 4 positions of a transfer vector
		durationDemod : OUT UNSIGNED(31 DOWNTO 0);
		
		--Input signals to demodulate
		source1: IN STD_LOGIC; --Square wave 1
		source2: IN STD_LOGIC; --Square wave 2
		ADCData1 : IN SIGNED(31 DOWNTO 0); --18 bit ADC data	
		ADCData2 : IN SIGNED(31 DOWNTO 0); --18 bit ADC data
		ADCReadDone : IN STD_LOGIC; --ADCData are valid and available

		--Communication avec la RAM interne au FPGA
		RAM_data : OUT SIGNED(23 DOWNTO 0);
		RAM_wraddress : BUFFER UNSIGNED(11 DOWNTO 0);
		RAM_wren : OUT STD_LOGIC;
        
		error : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		LED : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END Demodulation;


--  Architecture Body

ARCHITECTURE Demodulation_architecture OF Demodulation IS

	TYPE stateMachine IS (WAIT_DATA_IN, DELAI, DEMOD, WAIT_MOTEUR_INIT, WAIT_MOTEUR, WAIT_TRANSFER, TRANSFER, CHECK_END_POSITION);
	SIGNAL state : stateMachine;
	
	SIGNAL Detector1 : SIGNED(31 DOWNTO 0); --Value of detector 1. This signal keeps ADCData from the acquisition during the whole demodulation process
	SIGNAL Detector2 : SIGNED(31 DOWNTO 0); --Value of detector 2. This signal keeps ADCData from the acquisition during the whole demodulation process

	SIGNAL FirstDemod : STD_LOGIC_VECTOR(1 DOWNTO 0); --Used to reset a vector after a lastAdd
	TYPE arr2 IS ARRAY(1 DOWNTO 0) OF INTEGER RANGE -2530 TO 20;
	SIGNAL AssertAdd : arr2 := (OTHERS => (0)); --Used to check that as much add than sub as been done
	
	SIGNAL sourceCurrent: STD_LOGIC_VECTOR(1 DOWNTO 0);	--Delayed square wave
	SIGNAL sourceFuture: STD_LOGIC_VECTOR(1 DOWNTO 0); --Square wave used "to predict" the future and determine if it is the last addition of the demodulation
	
	--Counts the number of cycles of demodulation (A cycle is finished at the falling edge of a square wave at the source)
	SIGNAL counterDurationNCycles : INTEGER RANGE 0 TO 10000;
	SIGNAL counterDurationNCycles2 : INTEGER RANGE 0 TO 10000;
	--Counts the number of cycle of a demodulation at a given position
	SIGNAL counterDurationDemod : INTEGER RANGE 0 TO 10000000;

	--Array used in the demodulation
	TYPE arr IS ARRAY(3 DOWNTO 0) OF SIGNED(31 DOWNTO 0);
	SIGNAL demodulationTemp :arr;
	SIGNAL demodulationComplete :arr;
	SIGNAL demodTransfer :arr;
	SIGNAL demodTransfer2 :arr;
	
	SIGNAL dualBuffer : STD_LOGIC; --Switch buffer (demodulation is done in a matrix while we transfer the other)

BEGIN

PROCESS(RSTn, clk48MHz)

	--Signals depend on the source value. They indicate how we should manipulate the detector value
	VARIABLE lastAdd : STD_LOGIC_VECTOR(1 DOWNTO 0);
	VARIABLE SubAdd : STD_LOGIC_VECTOR(1 DOWNTO 0);

BEGIN

	IF (RSTn = '0') THEN
		demodReady <= '0';
		transferReady <= '0';
		RSTnFluo <= '1';
		saturation <= (OTHERS => '0');
		durationDemod <= (OTHERS => '0');
		error <= (OTHERS => '0');

		state <= WAIT_MOTEUR_INIT;
		Detector1 <= (OTHERS => '0');
		Detector2 <= (OTHERS => '0');
		FirstDemod <= (OTHERS => '1');
		AssertAdd <= (OTHERS => (0));
		sourceCurrent <= (OTHERS => '0');
		sourceFuture <= (OTHERS => '0');
		counterDurationNCycles <= 0;
		counterDurationNCycles2 <= 0;
		counterDurationDemod <= 0;	
		demodulationTemp <= (OTHERS => (OTHERS => '0'));
		demodulationComplete <= (OTHERS => (OTHERS => '0'));
		demodTransfer <= (OTHERS => (OTHERS => '0'));
		demodTransfer2 <= (OTHERS => (OTHERS => '0'));
		dualBuffer <= '0';
        demodulationDone <= '0';
		
		lastAdd := (OTHERS => '0');
		SubAdd := (OTHERS => '0');
        ADCReadingEnable <= '0';
        LED <= (OTHERS => '0');
        RAM_wraddress <= (OTHERS => '0');
        RAM_wren <= '0';
		
	ELSIF (clk48MHz'EVENT AND clk48MHz = '1') THEN
		transferReady <= '0';
		RSTnFluo <= '1';
		error <= (OTHERS => '0');
		counterDurationDemod <= counterDurationDemod + 1;
		demodReady <= '0';
        ADCReadingEnable <= '1';
        LED <= (OTHERS => '0');
        demodulationDone <= '0';
        RAM_wren <= '0';
		
		CASE state IS
			WHEN WAIT_MOTEUR_INIT => --We make sure that we detect a rising edge on enableFluo
                ADCReadingEnable <= '0';
                demodulationDone <= '1';
				IF enableFluo = '0' THEN
					state <= WAIT_MOTEUR;
				ELSE
					error(4) <= '1';
				END IF;
				FirstDemod <= (OTHERS => '1');
				AssertAdd <= (OTHERS => (0));
								
			WHEN WAIT_MOTEUR =>	--Mesure at a scanningPosition done, waiting for next position. Signal sent by the acquisition process, because it needs to be after a PA laser pulse
                ADCReadingEnable <= '0';
                demodulationDone <= '1';
				demodReady <= '1';
				FirstDemod <= (OTHERS => '1');
				AssertAdd <= (OTHERS => (0));	
				IF enableFluo = '1' THEN		--Le moteur est � la bonne position pour acqu�rir � nouveau
					state <= WAIT_DATA_IN;
					counterDurationDemod <= 0;	
					RSTnFluo <= '0'; --Pulse to reset
                    RAM_wraddress <= (OTHERS => '0');
				END IF;
				
			WHEN WAIT_DATA_IN => --Read and store ADC data
				IF ADCReadDone = '1' THEN				
					sourceCurrent <= sourceFuture;
					sourceFuture(1 DOWNTO 0) <= source2 & source1;				
					
					--8 Detectors have been acquired. Initialization.																	
					state <= DELAI;
					Detector1(31 DOWNTO 0) <= ADCData1; 

					Detector2(31 DOWNTO 0) <= ADCData2;
							
				END IF;	
				
			WHEN DELAI => --If there is no timing problems, this is not useful
				state <= DEMOD;	
                LED <= "11";
				
			WHEN DEMOD => --Demodulation...
            
                RAM_data <= Detector2(23 DOWNTO 0);
                RAM_data(20) <= sourceCurrent(0);
                RAM_wren <= '1';
                RAM_wraddress <= RAM_wraddress + 1;
        
				--Verify if it is the last add of the demodulation
				IF (sourceCurrent(0) = '1' AND sourceFuture(0) = '0') THEN lastAdd(0) := '1'; ELSE lastAdd(0) := '0'; END IF;
				IF (sourceCurrent(1) = '1' AND sourceFuture(1) = '0') THEN lastAdd(1) := '1'; ELSE lastAdd(1) := '0'; END IF;
				--Verify if we have to add or sub the signal of the detector
				IF sourceCurrent(0) = '1' THEN SubAdd(0) := '1'; ELSE SubAdd(0) := '0'; END IF;
				IF sourceCurrent(1) = '1' THEN SubAdd(1) := '1'; ELSE SubAdd(1) := '0'; END IF;
				
				--Demodulation starts here
				IF lastAdd(0) = '1' THEN		--An add will be done. Check that as much add than sub as been done. Writing to a new vector
					counterDurationNCycles <= counterDurationNCycles + 1;
					AssertAdd(0) <= 0;
					IF Detector1 > 230000 THEN
						saturation(0) <= '1';
					ELSE
						saturation(0) <= '0';
					END IF;
					IF Detector2 > 230000 THEN
						saturation(1) <= '1';
					ELSE
						saturation(1) <= '0';
					END IF;
										
					--Normal mode (demodulation ON)
					IF fluoSingleMeasureOn = '0' THEN	
						--If it is the First demodulation after a transfer
						IF FirstDemod(0) = '1' THEN
							FirstDemod(0) <= '0';	
							--2 Add have to be done
							IF AssertAdd(0) = -2 THEN
								demodulationComplete(0) <= demodulationTemp(0) + Detector1 + Detector1;
								demodulationComplete(1) <= demodulationTemp(1) + Detector2 + Detector2;
							--No Add has to be done
							ELSIF AssertAdd(0) = 0 THEN
								demodulationComplete(0) <= demodulationTemp(0);
								demodulationComplete(1) <= demodulationTemp(1);
							--1 Add has to be done
							ELSIF AssertAdd(0) = -1 THEN
								demodulationComplete(0) <= demodulationTemp(0) + Detector1;
								demodulationComplete(1) <= demodulationTemp(1) + Detector2;
							ELSE
								--There is a problem. The first complete cycle is ignored.
								demodulationComplete(0) <= "00100000000000000000000000000000";
								demodulationComplete(1) <= "00100000000000000000000000000000";
							END IF;

						--If it is not the First demodulation after a transfer
						ELSE	
							--2 Add have to be done
							IF AssertAdd(0) = -2 THEN							
								demodulationComplete(0) <= demodulationComplete(0) + demodulationTemp(0) + Detector1 + Detector1;
								demodulationComplete(1) <= demodulationComplete(1) + demodulationTemp(1) + Detector2 + Detector2;
							--No Add has to be done
							ELSIF AssertAdd(0) = 0 THEN
								demodulationComplete(0) <= demodulationComplete(0) + demodulationTemp(0);
								demodulationComplete(1) <= demodulationComplete(1) + demodulationTemp(1);
							--1 Add has to be done
							ELSIF AssertAdd(0) = -1 THEN
								demodulationComplete(0) <= demodulationComplete(0) + demodulationTemp(0) + Detector1;
								demodulationComplete(1) <= demodulationComplete(1) + demodulationTemp(1) + Detector2;					
							ELSE
								--There is a problem. Do not write. Should never happen.
								demodulationComplete(0) <= "00100000000000000000000000000000";
								demodulationComplete(1) <= "00100000000000000000000000000000";
								--LED(2) <= '1';
							END IF;
						END IF;		
					--If fluoSingleMeasureOn = 1, the raw signal is saved (FOR TESTS ONLY)
					ELSE	
						demodulationComplete(0) <= Detector1;
						demodulationComplete(1) <= Detector2;	
					END IF;	
																																																		
				ELSIF SubAdd(0) = '1' THEN	--An add will be done		
					AssertAdd(0) <= AssertAdd(0) + 1;		
					demodulationTemp(0) <= demodulationTemp(0) + Detector1;
					demodulationTemp(1) <= demodulationTemp(1) + Detector2;	
				ELSE						--A sub will de done
					AssertAdd(0) <= AssertAdd(0) - 1;
					
                    IF demodulationOn = '1' THEN
                        --If it is the first sub after a transfer or a complete cycle, do not read the last value.
                        IF AssertAdd(0) < 0 THEN
                            demodulationTemp(0) <= demodulationTemp(0) - Detector1;
                            demodulationTemp(1) <= demodulationTemp(1) - Detector2;	
                        ELSE
                            demodulationTemp(0) <= 0 - Detector1;
                            demodulationTemp(1) <= 0 - Detector2;	
                        END IF;
                    ELSE --Same thing when no demodulation, except that we add.
                        --If it is the first sub after a transfer or a complete cycle, do not read the last value.
                        IF AssertAdd(0) < 0 THEN
                            demodulationTemp(0) <= demodulationTemp(0) + Detector1;
                            demodulationTemp(1) <= demodulationTemp(1) + Detector2;	
                        ELSE
                            demodulationTemp(0) <= 0 + Detector1;
                            demodulationTemp(1) <= 0 + Detector2;	
                        END IF;   
                    END IF;
				END IF;
				
				--Same for second source
				IF lastAdd(1) = '1' THEN		--An add will be done. Check that as much add than sub as been done. Writing to a new vector
					counterDurationNCycles2 <= counterDurationNCycles2 + 1;
					AssertAdd(1) <= 0;
										
					--Normal mode (demodulation ON)
					IF fluoSingleMeasureOn = '0' THEN	
						--If it is the First demodulation after a transfer
						IF FirstDemod(1) = '1' THEN
							FirstDemod(1) <= '0';	
							--2 Add have to be done
							IF AssertAdd(1) = -2 THEN
								demodulationComplete(2) <= demodulationTemp(2) + Detector1 + Detector1;
								demodulationComplete(3) <= demodulationTemp(3) + Detector2 + Detector2;
							--No Add has to be done
							ELSIF AssertAdd(1) = 0 THEN
								demodulationComplete(2) <= demodulationTemp(2);
								demodulationComplete(3) <= demodulationTemp(3);
							--1 Add has to be done
							ELSIF AssertAdd(1) = -1 THEN
								demodulationComplete(2) <= demodulationTemp(2) + Detector1;
								demodulationComplete(3) <= demodulationTemp(3) + Detector2;
							ELSE
								--There is a problem. The first complete cycle is ignored.
								demodulationComplete(2) <= "00100000000000000000000000000000";
								demodulationComplete(3) <= "00100000000000000000000000000000";
							END IF;

						--If it is not the First demodulation after a transfer
						ELSE	
							--2 Add have to be done
							IF AssertAdd(1) = -2 THEN							
								demodulationComplete(2) <= demodulationComplete(2) + demodulationTemp(2) + Detector1 + Detector1;
								demodulationComplete(3) <= demodulationComplete(3) + demodulationTemp(3) + Detector2 + Detector2;
							--No Add has to be done
							ELSIF AssertAdd(1) = 0 THEN
								demodulationComplete(2) <= demodulationComplete(2) + demodulationTemp(2);
								demodulationComplete(3) <= demodulationComplete(3) + demodulationTemp(3);
							--1 Add has to be done
							ELSIF AssertAdd(1) = -1 THEN
								demodulationComplete(2) <= demodulationComplete(2) + demodulationTemp(2) + Detector1;
								demodulationComplete(3) <= demodulationComplete(3) + demodulationTemp(3) + Detector2;					
							ELSE
								--There is a problem. Do not write. Should never happen.
								demodulationComplete(2) <= "00100000000000000000000000000000";
								demodulationComplete(3) <= "00100000000000000000000000000000";
								--LED(2) <= '1';
							END IF;
						END IF;		
					--If fluoSingleMeasureOn = 0, the raw signal is saved (FOR TESTS ONLY)
					ELSE	
						demodulationComplete(2) <= Detector1;
						demodulationComplete(3) <= Detector2;	
					END IF;	
																																																		
				ELSIF SubAdd(1) = '1' THEN	--An add will be done		
					AssertAdd(1) <= AssertAdd(1) + 1;		
					demodulationTemp(2) <= demodulationTemp(2) + Detector1;
					demodulationTemp(3) <= demodulationTemp(3) + Detector2;	
				ELSE						--A sub will de done
					AssertAdd(1) <= AssertAdd(1) - 1;
                    
					IF demodulationOn = '1' THEN
                        --If it is the first sub after a transfer or a complete cycle, do not read the last value.
                        IF AssertAdd(1) < 0 THEN
                            demodulationTemp(2) <= demodulationTemp(2) - Detector1;
                            demodulationTemp(3) <= demodulationTemp(3) - Detector2;	
                        ELSE
                            demodulationTemp(2) <= 0 - Detector1;
                            demodulationTemp(3) <= 0 - Detector2;	
                        END IF;
                    ELSE --Same thing when no demodulation, except that we add.
                         --If it is the first sub after a transfer or a complete cycle, do not read the last value.
                        IF AssertAdd(1) < 0 THEN
                            demodulationTemp(2) <= demodulationTemp(2) + Detector1;
                            demodulationTemp(3) <= demodulationTemp(3) + Detector2;	
                        ELSE
                            demodulationTemp(2) <= 0 + Detector1;
                            demodulationTemp(3) <= 0 + Detector2;	
                        END IF;
                    END IF;                   
				END IF;		
				state <= CHECK_END_POSITION;
				
			WHEN CHECK_END_POSITION => --Demodulation done, we transfer demodulationComplete in a transfer matrix
				IF counterDurationNCycles >= UNSIGNED(nCyclesDemod) THEN
					IF counterDurationNCycles > UNSIGNED(nCyclesDemod) OR counterDurationNCycles2 /= UNSIGNED(nCyclesDemod+nCyclesDemod) THEN
						error(1) <= '1';
					END IF;
					
					IF lastAdd(0) = '0' OR lastAdd(1) = '0' THEN
						error(2) <= '1';
					END IF;
					
					IF dualBuffer = '0' THEN
                        demodTransfer(0) <= demodulationComplete(0);
                        demodTransfer(1) <= demodulationComplete(1);
                        demodTransfer(2) <= demodulationComplete(2);
                        demodTransfer(3) <= demodulationComplete(3);
					ELSE
                        demodTransfer2(0) <= demodulationComplete(0);
                        demodTransfer2(1) <= demodulationComplete(1);
                        demodTransfer2(2) <= demodulationComplete(2);
                        demodTransfer2(3) <= demodulationComplete(3);
					END IF;					
					counterDurationNCycles <= 0;
					counterDurationNCycles2 <= 0;
					
					--Demodulation is done, we store the duration ot took
					counterDurationDemod <= counterDurationDemod;
					durationDemod <= TO_UNSIGNED(counterDurationDemod,32);
					
					state <= WAIT_TRANSFER;				
				ELSE
					state <= WAIT_DATA_IN;
				END IF;			
			
			WHEN WAIT_TRANSFER => --Make sure the last transfer is done	
                ADCReadingEnable <= '0';
                demodulationDone <= '1';
				IF transferring = '0' THEN	
                    transferReady <= '1';
					state <= TRANSFER;	
					dualBuffer <= NOT(dualBuffer);					
				ELSE
					error(3) <= '1';
				END IF;	
					
			WHEN TRANSFER => --Wait until the transfer module received transferReady signal		
                ADCReadingEnable <= '0';
				transferReady <= '1';
				IF transferring = '1' THEN
					state <= WAIT_MOTEUR_INIT;
				END IF;		
                demodulationDone <= '1';   
								
			WHEN OTHERS =>
                ADCReadingEnable <= '0';
				state <= WAIT_MOTEUR_INIT;
                demodulationDone <= '1';

		END CASE;
		

	END IF;					
END PROCESS;

dataDemodulation <= demodTransfer(TO_INTEGER(addressFluo)) WHEN dualBuffer = '1' ELSE demodTransfer2(TO_INTEGER(addressFluo));

END Demodulation_architecture;
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;


--  Entity Declaration
ENTITY BicolorLED IS
	PORT
	(
		RSTn : IN STD_LOGIC;
        clk48MHz : IN STD_LOGIC;
        clk2Hz : IN STD_LOGIC;
        inColorRed : IN STD_LOGIC;
        inColorGreen : IN STD_LOGIC;
        inColorYellow : IN STD_LOGIC;
        inColorOff : IN STD_LOGIC;
		BicolorLED : OUT STD_LOGIC
	);
END BicolorLED;

--  Architecture Body 
ARCHITECTURE BicolorLED_architecture OF BicolorLED IS
	
	TYPE stateMachine IS (INIT, RED, GREEN, YELLOW, OFF);
	SIGNAL state : stateMachine;
    
    SIGNAL counterYellow : INTEGER RANGE 1 TO 48000;
    SIGNAL YellowColor : STD_LOGIC;
		
BEGIN

PROCESS(clk48MHz, RSTn)
BEGIN
	IF RSTn = '0' THEN	
		state <= RED;	
		BicolorLED <= 'Z';
        
        counterYellow <= 1;
		YellowColor <= '0';        

	ELSIF (clk48MHz'EVENT AND clk48MHz = '1') THEN
		CASE state IS
            WHEN INIT =>
                IF inColorRed = '1' THEN
                    state <= RED;
                ELSIF inColorYellow = '1' THEN
                    state <= YELLOW;                    
                ELSIF inColorGreen = '1' THEN
                    state <= GREEN;
                ELSE
                    state <= OFF;
                END IF;
                
			WHEN RED =>
                BicolorLED <= '0';
                IF inColorYellow = '1' THEN
                    state <= YELLOW;                    
                ELSIF inColorGreen = '1' THEN
                    state <= GREEN;
                ELSIF inColorOff = '1' THEN
                    state <= OFF;
                END IF;                
				
			WHEN YELLOW =>
                BicolorLED <= YellowColor;                 
                IF inColorGreen = '1' THEN
                    state <= GREEN;
                ELSIF inColorOff = '1' THEN
                    state <= OFF;
                ELSIF inColorRed = '1' THEN
                    state <= RED;                   
                END IF; 
                
            WHEN GREEN =>
                BicolorLED <= '1';
                IF inColorOff = '1' THEN
                    state <= OFF;
                ELSIF inColorRed = '1' THEN
                    state <= RED;    
                ELSIF inColorYellow = '1' THEN
                    state <= YELLOW;      
                END IF;                      
                
			WHEN OFF =>
                BicolorLED <= 'Z';
                IF inColorRed = '1' THEN
                    state <= RED;
                ELSIF inColorYellow = '1' THEN
                    state <= YELLOW;                    
                ELSIF inColorGreen = '1' THEN
                    state <= GREEN;
                ELSIF inColorOff = '1' THEN
                    state <= OFF;
                END IF;                   
            
			WHEN OTHERS =>
				state <= INIT;
	
		END CASE;

        --Increment counterYellow. Duration of counter is 1ms.
        counterYellow <= counterYellow + 1;
        IF counterYellow = 48000 THEN
            counterYellow <= 1;
        END IF;
        --Set oscillation and duty cycle
        IF counterYellow < 15000 THEN
            YellowColor <= '0';
        ELSE
            YellowColor <= '1';
        END IF;
    
    END IF;
END PROCESS;
	
END BicolorLED_architecture;				
				
				
				
				
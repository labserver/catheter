LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;


--  Entity Declaration
ENTITY TestBicolorLED IS
	PORT
	(
		RSTn : IN STD_LOGIC;
        clk1Hz : IN STD_LOGIC;
        inColorRed : OUT STD_LOGIC;
        inColorGreen : OUT STD_LOGIC;
        inColorYellow : OUT STD_LOGIC;
        inColorOff : OUT STD_LOGIC
	);
END TestBicolorLED;

--  Architecture Body 
ARCHITECTURE TestBicolorLED_architecture OF TestBicolorLED IS
	
	TYPE stateMachine IS (S1, S2, S3, S4);
	SIGNAL state : stateMachine;

BEGIN

PROCESS(clk1Hz, RSTn)
BEGIN
	IF RSTn = '0' THEN	
		state <= S1;	
        inColorRed <= '0';
        inColorGreen <= '0';
        inColorYellow <= '0';
        inColorOff <= '0';

	ELSIF (clk1Hz'EVENT AND clk1Hz = '1') THEN
		CASE state IS
            WHEN S1 =>
                inColorRed <= '0';
                inColorGreen <= '0';
                inColorYellow <= '0';
                inColorOff <= '1';
                state <= S2;

            WHEN S2 =>
                inColorRed <= '1';
                inColorGreen <= '0';
                inColorYellow <= '0';
                inColorOff <= '0'; 
                state <= S3;
           
            WHEN S3 =>
                inColorRed <= '0';
                inColorGreen <= '0';
                inColorYellow <= '1';
                inColorOff <= '0';
                state <= S4;
           
            WHEN S4 =>
                inColorRed <= '0';
                inColorGreen <= '1';
                inColorYellow <= '0';
                inColorOff <= '0';  
                state <= S1; 
              
            WHEN OTHERS =>
				state <= S1;
	
		END CASE;
    
    END IF;
END PROCESS;
	
END TestBicolorLED_architecture;				
				
				
				
				
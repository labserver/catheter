LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY PulseShaper IS
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk200MHz : IN STD_LOGIC;										
	
		--Parameters received from the computer (see Matlab code for information)
		pulseShapeP : IN UNSIGNED(15 DOWNTO 0);
        pulseShapeN : IN UNSIGNED(15 DOWNTO 0);
		startRTZ : IN UNSIGNED(11 DOWNTO 0);
		stopRTZ : IN UNSIGNED(11 DOWNTO 0);
		
		--Output of the system
		pulseUSP : OUT STD_LOGIC; --Positive ultrasound pulser
		pulseUSN : OUT STD_LOGIC; --Negative ultrasound pulser
		pulseUSRTZ : OUT STD_LOGIC; --Return-To-Zero pulser after pulsing
					
		--Communication with the PAUS controller
		pulseEnable : IN STD_LOGIC

	);	
END PulseShaper;

--  Architecture Body 
ARCHITECTURE PulseShaper_architecture OF PulseShaper IS

	TYPE stateMachine IS (WAIT_ENABLE0, WAIT_ENABLE1, US_PULSE);
	SIGNAL state : stateMachine;
	
	SIGNAL counterPulse : INTEGER RANGE 0 TO 200; --For the ultrasound pulse

BEGIN
    
PROCESS(RSTn, clk200MHz)

BEGIN
IF (RSTn = '0') THEN
	state <= WAIT_ENABLE0;
	pulseUSP <= '0'; pulseUSN <= '0'; pulseUSRTZ <= '0';
    counterPulse <= 0;

ELSIF (clk200MHz'EVENT AND clk200MHz = '1') THEN
    pulseUSP <= '0'; pulseUSN <= '0'; pulseUSRTZ <= '0';
    
	CASE state IS
	
		WHEN WAIT_ENABLE0 =>
            IF pulseEnable = '0' THEN
                state <= WAIT_ENABLE1;
            ELSE
                state <= WAIT_ENABLE0;
            END IF;
            counterPulse <= 0;
            pulseUSP <= '0'; pulseUSN <= '0'; pulseUSRTZ <= '0';
 
		WHEN WAIT_ENABLE1 => --Wait for pulseEnable rising edge
            IF pulseEnable = '1' THEN
                state <= US_PULSE;
            ELSE
                state <= WAIT_ENABLE1;
            END IF;
            counterPulse <= 0;
            pulseUSP <= '0'; pulseUSN <= '0'; pulseUSRTZ <= '0';
            
		WHEN US_PULSE => 
        
            --16 states where the pulse is ajustable
            IF counterPulse <= 15 THEN
                pulseUSP <= pulseShapeP(counterPulse); --Pulse+
                pulseUSN <= pulseShapeN(counterPulse); --Pulse-
            ELSE
                pulseUSP <= '0'; --Pulse+
                pulseUSN <= '0'; --Pulse-            
            END IF;
            
			--Pulse RTZ for a given time
			IF counterPulse < StopRTZ AND counterPulse > StartRTZ THEN
				pulseUSRTZ <= '1';
            ELSE
                pulseUSRTZ <= '0';
            END IF;  
				
            IF counterPulse > StopRTZ AND counterPulse >= 15 THEN
                counterPulse <= 0;
                state <= WAIT_ENABLE0;
            ELSE
                counterPulse <= counterPulse + 1;
                state <= US_PULSE;
			END IF;
            
		WHEN OTHERS =>
			state <= WAIT_ENABLE0;	
            pulseUSP <= '0'; pulseUSN <= '0'; pulseUSRTZ <= '0';  
            counterPulse <= 0;
	END CASE;	
END IF;

END PROCESS;

END PulseShaper_architecture;

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE ieee.std_logic_arith.conv_std_logic_vector;

--  Entity Declaration

ENTITY LCD_Display_Controller IS

	PORT
	(	
		rst_n	  							: IN STD_LOGIC;
		CLK_PRINC 						: IN STD_LOGIC;
		GO 								: OUT  STD_LOGIC;
		DONE 								: IN  STD_LOGIC;
		Done_init						: IN  STD_LOGIC;
      ACK 								: IN  STD_LOGIC;
		Donnee	 						: OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
	
END LCD_Display_Controller;

ARCHITECTURE behavioral OF LCD_Display_Controller IS

type etat is (DEBUT, POS_0, POS_1, POS_2, POS_3, POS_4, POS_5, MOT_0, MOT_1, MOT_2,  
				  DEUXPOINT_0, DEUXPOINT_1, DEUXPOINT_2, NUM_0, NUM_1, NUM_2, NUM_3, 
				  RAFFRAICH, REGISTRE, VERIF_MOT, FIN);
					 
signal step , step_next : etat ;

type tableau1  is array (0 to 9) of std_logic_vector (7 downto 0);
constant	chiffre : tableau1 := (
						X"30",X"31",X"32",X"33",X"34",
						X"35",X"36",X"37",X"38",X"39"
						);
type tableau2  is array (0 to 7) of std_logic_vector (7 downto 0);
constant	valeur : tableau2 := (
						X"FE",X"45",X"3A",X"2E",         -- FE 45  :  . 
						X"28",X"29",X"2F",X"25"				-- (   )  /  %
						);	
type tableau3  is array (0 to 3) of std_logic_vector (7 downto 0);
constant	ligne : tableau3 := (
						X"00",X"40",X"14",X"54"
						);
						
						
--DÉCLARATION DES SIGNAUX INTERNES

signal go_out, go_out_next 	  							:std_logic;
signal position, position_next 							:integer := 0;
signal num_lettre, num_lettre_next 						:integer := 0;
signal donnee_out, donnee_out_next						:std_logic_vector(7 downto 0);

---------------- TABLEAU POUR L'ECRITURE DES MOTS -------------------

type tableau4  is array (0 to 15) of std_logic_vector (7 downto 0);
constant var10: tableau4 := (
		X"4D",X"6F",X"74",X"65",X"75",
		X"72",X"A0",X"72",X"6F",X"74",
		X"2E",X"A0",X"28",X"25",X"29",
		X"A0"); 
		
constant var20: tableau4:= (
		X"4D",X"6F",X"74",X"65",X"75",
		X"72",X"A0",X"6C",X"69",X"6E",
		X"2E",X"A0",X"28",X"6D",X"6D",
		X"29"); 
		
constant var30 : tableau4:= (
		X"4D",X"61",X"63",X"68",X"69",
		X"6E",X"65",X"A0",X"64",X"27",
		X"65",X"74",X"61",X"74",X"A0",
		X"A0");
		
constant var40: tableau4:= (
		X"43",X"6F",X"64",X"65",X"A0",
		X"64",X"27",X"65",X"72",X"72",
		X"65",X"75",X"72",X"A0",X"A0",
		X"A0");
 
type tableau5  is array (0 to 3) of  tableau4;
signal mot : tableau5 ;
				

		
begin


        
--ASSIGNATION DES SORTIES DE "ENTITY"
GO <= go_out;
Donnee <= donnee_out;






--***********************************************************
-- Machine a etats 
--***********************************************************

ETAPE:process(CLK_PRINC, rst_n)

begin

	if(rst_n = '0')then   							           -- reset asynchrone logique pos
		step <= DEBUT;
		donnee_out <= (others =>'0');
   	go_out <= '0';
		position <= 0;
		num_lettre <= 0;
	elsif(CLK_PRINC'EVENT and CLK_PRINC = '0')then		  -- front descendant de l'horloge
		step <= step_next;
		donnee_out <= donnee_out_next;
		go_out <= go_out_next;
		position <= position_next;
		num_lettre <= num_lettre_next;
	end if;
	
end process ETAPE;



--***********************************************************
-- Code pour envoi de caractere au LCD
--***********************************************************

LCD_Display_Controller: PROCESS(step, go_out_next, donnee_out,num_lettre, num_lettre_next,ack, 
						Done,position, position_next, done_init,
						mot)



BEGIN

mot(0)<= var10; 
mot(1)<= var20;
mot(2)<= var30;
mot(3)<= var40;
donnee_out_next <= donnee_out;
position_next <= position;
num_lettre_next <= num_lettre;


		CASE step IS
		

------*******************************************
----				-- POSITION DU PREMIER MOT
------*******************************************

--				envoie du premier paquet: FE 

		
			WHEN DEBUT =>
				if (done = '0') then
					step_next <= DEBUT;
					go_out_next <= '0';
				elsif (position < 4)  then				
					step_next <=  POS_0;
					go_out_next <= '1';
				else
					step_next <= FIN ;
					go_out_next <= '1';
				end if ;
				donnee_out_next <= valeur(0);



			WHEN POS_0 => 
			if (ack = '1') then
				step_next <= POS_1;
				go_out_next <= '0';
			else
				go_out_next <= '1';
				step_next <= POS_0;		
			end if;
			donnee_out_next <= valeur(0);
			

--				envoie du deuxieme paquet: 45 


			WHEN POS_1 =>
				if (done = '0') then
					step_next <= POS_1;
					go_out_next <= '0';
				else  				
					step_next <= POS_2 ;
					go_out_next <= '1';
				end if ;
				donnee_out_next <= valeur(1);
				
				
			WHEN POS_2 => 
			if (ack = '1') then
				go_out_next <= '0';
				step_next <= POS_3;
			else
				go_out_next <= '1';
				step_next <= POS_2;		
			end if;
			donnee_out_next <= valeur(1);
			
--		envoie du troisieme paquet: La position desire 

	
			WHEN POS_3 =>
				if (done = '0') then
					step_next <= POS_3;
				else  				
					step_next <= POS_4 ;
				end if ;
					donnee_out_next <= ligne(position);
					go_out_next <= '0';
					
				
			WHEN POS_4 =>
				go_out_next <= '1';
				donnee_out_next <= ligne(position);
				step_next <= POS_5;


			WHEN POS_5 => 
			if (ack = '1') then
				go_out_next <= '0';
				step_next <= MOT_0;
			else
				go_out_next <= '1';
				step_next <= POS_5;		
			end if;
			donnee_out_next <= ligne(position);
			

--		envoie des paquets des lettres constituant le mot 		
					
				
			WHEN MOT_0 =>
				if (done = '0') then
					step_next <= MOT_0;	
					go_out_next <= '1';
				else 				             							-- sinon
					step_next <= MOT_1 ; 								-- passer au caractere ":"			
					go_out_next <= '0';
				end if ;
				donnee_out_next <= mot(position)(num_lettre);
					
				
			WHEN MOT_1 =>
				go_out_next <= '1';
				donnee_out_next <= mot(position)(num_lettre);
				step_next <= MOT_2;


			WHEN MOT_2 => 
			if (ack = '1') then
				go_out_next <= '0';
				step_next <= VERIF_MOT;
			else
				go_out_next <= '1';
				step_next <= MOT_2;	
			end if;
			donnee_out_next <= mot(position)(num_lettre);
			
			
			when VERIF_MOT =>
				if (num_lettre = 15)  then				
					step_next <=  DEUXPOINT_0;
					num_lettre_next <= 0;
				else
					num_lettre_next <= num_lettre + 1;
					step_next <= MOT_0 ;
				end if ;
				donnee_out_next <= (others =>'0');
				go_out_next <= '0';

			
--		envoie de la consigne pour le caractere ":"


			WHEN DEUXPOINT_0 =>
				if (done = '0') then
					step_next <= DEUXPOINT_0;
					go_out_next <= '1';
				else  				
					step_next <= DEUXPOINT_1 ;
					go_out_next <= '0';
				end if ;
				donnee_out_next <= valeur(2);
					
				
			WHEN DEUXPOINT_1 =>
				go_out_next <= '1';
				donnee_out_next <= X"3A";
				step_next <= DEUXPOINT_2;


			WHEN DEUXPOINT_2 => 
			if (ack = '1') then
				go_out_next <= '0';
				step_next <= NUM_0;
			else
				go_out_next <= '1';
				step_next <= DEUXPOINT_2;		
			end if;
			donnee_out_next <= X"3A";
			
			
			WHEN NUM_0 =>
				step_next <= DEBUT;   
				go_out_next <= '1';
				donnee_out_next <= X"00";
				position_next <= position + 1;
				num_lettre_next <= 0;
				
				
			WHEN FIN =>
--				step_next <= FIN;
	step_next <= DEBUT;			
    			go_out_next <= '1';
				donnee_out_next <= (others =>'0');
				position_next <= 0;
				num_lettre_next <= 0;
				
				
			  
			WHEN OTHERS =>
				step_next <= DEBUT;
				go_out_next <= '0';
				donnee_out_next <= (others =>'0');
							

		END CASE;
	
				
END PROCESS LCD_Display_Controller;					

END behavioral;

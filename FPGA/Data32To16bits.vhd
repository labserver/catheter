LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--  Entity Declaration

ENTITY Data32To16bits IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		RSTn : IN STD_LOGIC;
		CLK48MHz : IN STD_LOGIC;
		clkPAUS : IN STD_LOGIC;
		
		transferPAUS16bits : IN STD_LOGIC; --If '1', when need to concatenate two samples in one data
		
		rdAddressToRAM : BUFFER UNSIGNED(11 DOWNTO 0);
		rdAddressFromSDRAM : IN UNSIGNED(11 DOWNTO 0);
		
		transferDataFromRAM : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		transferDataToSDRAM : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
		
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END Data32To16bits;

--  Architecture Body 

ARCHITECTURE Data32To16bits_architecture OF Data32To16bits IS
	SIGNAL transferDataTemp1 : STD_LOGIC_VECTOR(15 DOWNTO 0);
	SIGNAL counterInc : INTEGER RANGE 0 TO 31;	
	SIGNAL waitData : INTEGER RANGE 0 TO 31;
	SIGNAL waitData2 : INTEGER RANGE 0 TO 31;
	SIGNAL CLK48MHzPrevious : STD_LOGIC := '1';
BEGIN

PROCESS(RSTn, clkPAUS)
	
BEGIN
IF (RSTn = '0') THEN
	transferDataTemp1 <= (OTHERS => '0');
	rdAddressToRAM <= (OTHERS => '0');
	transferDataToSDRAM <= (OTHERS => '0');
	counterInc <= 6;
	waitData <= 6;
	waitData2 <= 6;
	CLK48MHzPrevious <= '1';

ELSIF (clkPAUS'EVENT AND clkPAUS = '1') THEN
	waitData <= waitData + 1;
	waitData2 <= waitData2 + 1;
	counterInc <= counterInc + 1;
	CLK48MHzPrevious <= CLK48MHz;
	IF CLK48MHz = '1' AND CLK48MHzPrevious = '0' THEN
		counterInc <= 1;
	END IF;
	
	IF counterInc = 3 THEN
		--rdAddressFromSDRAM is stable (just before rising edge on CLK48MHz)
		rdAddressToRAM <= rdAddressFromSDRAM;
		waitData <= 1;
	END IF;
	IF waitData = 1 THEN --Increment  read address 1 clock cycle later
		rdAddressToRAM <= rdAddressToRAM+1;
	ELSIF waitData = 3 THEN
		waitData2 <= 1;
	END IF;
	
	IF waitData2 = 2 THEN
		transferDataTemp1 <= transferDataFromRAM(15 DOWNTO 0); --Read first data
	ELSIF waitData2 = 3 THEN
		transferDataToSDRAM <= transferDataFromRAM(15 DOWNTO 0) & transferDataTemp1; --Read second data
	END IF;

END IF;
END PROCESS;

END Data32To16bits_architecture;

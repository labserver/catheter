--COMMENTS:
--
--This module acts as a demultiplexer. The received data from the computer are used to set the parameters of the acquisition and to enable it.

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--  Entity Declaration
ENTITY Parameters IS
	PORT
	(
		PORSTn : IN STD_LOGIC;
		RSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;
		
		--Data received from the computer
		dataFromFIFO : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		idxReceive : IN INTEGER RANGE 0 TO 65535; --Index of the current parameter being transferred from the computer. Starts at 1.
		
		--Output parameters
		nCycleDemod : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);		
		delayBetweenUltrasoundsNCycles : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		nAveragingUltrasound : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		nAveragingUltrasoundFlush : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		laserPower1 : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		laserPower2 : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		PMTGain1 : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		PMTGain2 : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		gainTransducerPA : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		gainTransducer : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		nSlices : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		nTurnsPerSlice : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		distanceBetweenTurns : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		distanceFirstSlice : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		pulserOn : BUFFER STD_LOGIC;
		photoacousticOn : BUFFER STD_LOGIC;
		motorSync : BUFFER STD_LOGIC;
		modulationOn : BUFFER STD_LOGIC;
		demodulationOn : BUFFER STD_LOGIC;
		fluoSingleMeasureOn : BUFFER STD_LOGIC;
		useLinearMotor : BUFFER STD_LOGIC;
		TGCPA : BUFFER STD_LOGIC;  
		TGCUS : BUFFER STD_LOGIC;  
		TGCCutPulse : BUFFER STD_LOGIC;  
		transferPAUS16bits : BUFFER STD_LOGIC;		
		constantPullback : BUFFER STD_LOGIC;	
        DPSSPhotoacousticOn : BUFFER STD_LOGIC;	
        skipPAAcq : BUFFER STD_LOGIC;	
		pulseShapeP : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		pulseShapeN : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		pulseShapeStartRTZ : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		pulseShapeStopRTZ : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		TGC1 : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		TGC2 : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		TGC3 : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		TGC4 : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		nSamples : BUFFER STD_LOGIC_VECTOR(11 DOWNTO 0);
		lineDuration : BUFFER STD_LOGIC_VECTOR(23 DOWNTO 0);
		lineDurationTol : BUFFER STD_LOGIC_VECTOR(23 DOWNTO 0);
		linearMotorSpeed : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		linearMotorSpeedHome : BUFFER STD_LOGIC_VECTOR(15 DOWNTO 0);
		PAUSDivisor : BUFFER STD_LOGIC_VECTOR(7 DOWNTO 0);
		linesPerTurnEncoder : BUFFER STD_LOGIC_VECTOR(14 DOWNTO 0);
		EncoderToPosition : BUFFER STD_LOGIC_VECTOR(7 DOWNTO 0);
		nPositionsPerTurn : BUFFER STD_LOGIC_VECTOR(10 DOWNTO 0);
		speedMotorTurnsPerSec : BUFFER STD_LOGIC_VECTOR(5 DOWNTO 0);

		enableAcq : BUFFER STD_LOGIC --Signal that enables the acquisition, after receiving all the parameters.		
	);	
END Parameters;

--  Architecture Body
ARCHITECTURE Parameters_architecture OF Parameters IS
	SIGNAL idxReceiveSignal : INTEGER RANGE 0 TO 65535; --Used to check if idxReceive has changed.
BEGIN
PROCESS(PORSTn, RSTn, clk48MHz)
BEGIN
IF (PORSTn = '0') THEN
	idxReceiveSignal <= 0;
	nCycleDemod <= (OTHERS => '0');	
	delayBetweenUltrasoundsNCycles <= (OTHERS => '0');
	nAveragingUltrasound <= (OTHERS => '0');
	nAveragingUltrasoundFlush <= (OTHERS => '0');
	laserPower1 <= "000000000000";
	laserPower2 <= "100000110100";
	PMTGain1 <= (OTHERS => '0');
	PMTGain2 <= (OTHERS => '0');
	gainTransducerPA <= (OTHERS => '0');
    gainTransducer <= (OTHERS => '0');
	pulserOn <= '0';
	photoacousticOn <= '0';
	motorSync <= '1';
	modulationOn <= '0';
	demodulationOn <= '0';
    fluoSingleMeasureOn <= '0';
    useLinearMotor <= '1';
    TGCPA <= '0';
    TGCUS <= '0';
    TGCCutPulse <= '0';
	transferPAUS16bits <= '0';
	constantPullback <= '0';
    DPSSPhotoacousticOn <= '0';
    skipPAAcq <= '0';
	nSlices <= (OTHERS => '0');
    nTurnsPerSlice <= (OTHERS => '0');
	distanceBetweenTurns <= (OTHERS => '0');
	distanceFirstSlice <= (OTHERS => '0');
	nTurnsPerSlice <= (OTHERS => '0');	
	pulseShapeP <= (OTHERS => '0');	
    pulseShapeN <= (OTHERS => '0');	
    pulseShapeStartRTZ <= (OTHERS => '0');	
    pulseShapeStopRTZ <= (OTHERS => '0');	
    TGC1 <= (OTHERS => '0');	
    TGC2 <= (OTHERS => '0');	
    TGC3 <= (OTHERS => '0');	
    TGC4 <= (OTHERS => '0');
    nSamples <= (OTHERS => '0');
    lineDuration <= "000000011110100001001000";
    lineDurationTol <= "000000000001100001101010";  
	linearMotorSpeed <= "0000000000010100";
	linearMotorSpeedHome <= "0000000000010100";
	PAUSDivisor <= (OTHERS => '0');
	linesPerTurnEncoder <= "000000000000010";
	EncoderToPosition <= "00000100";
	nPositionsPerTurn <= "00100000000";
	speedMotorTurnsPerSec <= "001010";
    
	enableAcq <= '0';	

ELSIF (RSTn = '0') THEN	
    --Some signals are not reset after an acquisition
    --PMTGains and gain for PAUS are kept in order to avoid the ramp at the beginning of a scan.
    --Lasers are turned off, for protection, even if it would allow to keep them stable at the beginning of a scan.
    --Pulser is also turned off, for protection, even if it would allow to keep them stable at the beginning of a scan.
	idxReceiveSignal <= 0;
	nCycleDemod <= (OTHERS => '0');	
	delayBetweenUltrasoundsNCycles <= (OTHERS => '0');
	nAveragingUltrasound <= (OTHERS => '0');
	nAveragingUltrasoundFlush <= (OTHERS => '0');
	laserPower1 <= "000000000000";
	--laserPower2 <= "000000000000"; --Don't reset laser 2 after an acquisition.
	pulserOn <= '0';
	photoacousticOn <= '0';
	motorSync <= '1';
	modulationOn <= '0';
	demodulationOn <= '0';
    fluoSingleMeasureOn <= '0';
    useLinearMotor <= '1';    
    TGCPA <= '0';
    TGCUS <= '0';
    TGCCutPulse <= '0';
	transferPAUS16bits <= '0';
	constantPullback <= '0';
    DPSSPhotoacousticOn <= '0';
    SkipPAAcq <= '0';
	nSlices <= (OTHERS => '0');
    nTurnsPerSlice <= (OTHERS => '0');
	distanceBetweenTurns <= (OTHERS => '0');
	distanceFirstSlice <= (OTHERS => '0');
	nTurnsPerSlice <= (OTHERS => '0');	
	pulseShapeP <= (OTHERS => '0');	
    pulseShapeN <= (OTHERS => '0');	
    pulseShapeStartRTZ <= (OTHERS => '0');	
    pulseShapeStopRTZ <= (OTHERS => '0');	
    nSamples <= (OTHERS => '0');
    --lineDuration <= "000000011110100001001000"; --Don't reset motor speed after an acquisition.
    --lineDurationTol <= "000000000001100001101010"; --Don't reset motor speed after an acquisition.
	linearMotorSpeed <= "0000000000010100";
	--linearMotorSpeedHome <= "0000000000010100";
	PAUSDivisor <= (OTHERS => '0');
	--linesPerTurnEncoder <= "000000000000010";
	--EncoderToPosition <= "00000100";
-- 	nPositionsPerTurn <= "00100000000";
--	speedMotorTurnsPerSec <= "001010";
	
	enableAcq <= '0';	
    
ELSIF (clk48MHz'EVENT AND clk48MHz = '1') THEN 
	
	idxReceiveSignal <= idxReceive;
	IF idxReceiveSignal /= idxReceive THEN
		IF (idxReceive = 1) THEN
			nCycleDemod <= dataFromFIFO(15 DOWNTO 0);			
		ELSIF (idxReceive = 2) THEN
			delayBetweenUltrasoundsNCycles <= dataFromFIFO(15 DOWNTO 0);	
		ELSIF (idxReceive = 3) THEN
			nAveragingUltrasound <= dataFromFIFO(15 DOWNTO 0);
		ELSIF (idxReceive = 4) THEN
			nAveragingUltrasoundFlush <= dataFromFIFO(15 DOWNTO 0);
		ELSIF (idxReceive = 5) THEN						
			laserPower1 <= dataFromFIFO(11 DOWNTO 0);
		ELSIF (idxReceive = 6) THEN					
			laserPower2 <= dataFromFIFO(11 DOWNTO 0);
		ELSIF (idxReceive = 7) THEN					
			PMTGain1 <= dataFromFIFO(11 DOWNTO 0);
		ELSIF (idxReceive = 8) THEN					
			PMTGain2 <= dataFromFIFO(11 DOWNTO 0);
		ELSIF (idxReceive = 9) THEN				
			gainTransducerPA <= dataFromFIFO(11 DOWNTO 0);
		ELSIF (idxReceive = 10) THEN				
			gainTransducer <= dataFromFIFO(11 DOWNTO 0);
        ELSIF (idxReceive = 11) THEN						
			pulserOn <= dataFromFIFO(0);
			photoacousticOn <= dataFromFIFO(1);
			motorSync <= dataFromFIFO(2);
			modulationOn <= dataFromFIFO(3);
			demodulationOn <= dataFromFIFO(4);
            fluoSingleMeasureOn <= dataFromFIFO(5);
            useLinearMotor <= dataFromFIFO(6);           
            TGCPA <= dataFromFIFO(7);  
            TGCUS <= dataFromFIFO(8);  
            TGCCutPulse <= dataFromFIFO(9);  
			transferPAUS16bits <= dataFromFIFO(10);  
			constantPullback <= dataFromFIFO(11);  
            DPSSPhotoacousticOn <= dataFromFIFO(12);  
            skipPAAcq <= dataFromFIFO(13);
		ELSIF (idxReceive = 12) THEN				
			nSlices <= dataFromFIFO(15 DOWNTO 0);
		ELSIF (idxReceive = 13) THEN				
			nTurnsPerSlice <= dataFromFIFO(15 DOWNTO 0);
        ELSIF (idxReceive = 14) THEN				
			distanceBetweenTurns <= dataFromFIFO(15 DOWNTO 0);
		ELSIF (idxReceive = 15) THEN				
			distanceFirstSlice <= dataFromFIFO(15 DOWNTO 0);
		ELSIF (idxReceive = 16) THEN				
			pulseShapeP <= dataFromFIFO(15 DOWNTO 0);						
		ELSIF (idxReceive = 17) THEN				
			pulseShapeN <= dataFromFIFO(15 DOWNTO 0);	
        ELSIF (idxReceive = 18) THEN				
			pulseShapeStartRTZ <= dataFromFIFO(11 DOWNTO 0);	
		ELSIF (idxReceive = 19) THEN				
			pulseShapeStopRTZ <= dataFromFIFO(11 DOWNTO 0);	
		ELSIF (idxReceive = 20) THEN				
			TGC1 <= dataFromFIFO(11 DOWNTO 0);	            
		ELSIF (idxReceive = 21) THEN				
			TGC2 <= dataFromFIFO(11 DOWNTO 0);	
		ELSIF (idxReceive = 22) THEN				
			TGC3 <= dataFromFIFO(11 DOWNTO 0);	
		ELSIF (idxReceive = 23) THEN				
			TGC4 <= dataFromFIFO(11 DOWNTO 0);	 
 		ELSIF (idxReceive = 24) THEN				
			nSamples <= dataFromFIFO(11 DOWNTO 0);	 
 		ELSIF (idxReceive = 25) THEN				
			lineDuration <= dataFromFIFO(23 DOWNTO 0);	 
 		ELSIF (idxReceive = 26) THEN				
			lineDurationTol <= dataFromFIFO(23 DOWNTO 0);
		ELSIF (idxReceive = 27) THEN
			linearMotorSpeed <= dataFromFIFO(15 DOWNTO 0);
		ELSIF (idxReceive = 28) THEN
			linearMotorSpeedHome <= dataFromFIFO(15 DOWNTO 0);
		ELSIF (idxReceive = 29) THEN
			PAUSDivisor <= dataFromFIFO(7 DOWNTO 0);	
		ELSIF (idxReceive = 30) THEN
			linesPerTurnEncoder <= dataFromFIFO(14 DOWNTO 0);		
		ELSIF (idxReceive = 31) THEN
			EncoderToPosition <= dataFromFIFO(7 DOWNTO 0);	
		ELSIF (idxReceive = 32) THEN
			nPositionsPerTurn <= dataFromFIFO(10 DOWNTO 0);	
		ELSIF (idxReceive = 33) THEN
			speedMotorTurnsPerSec <= dataFromFIFO(5 DOWNTO 0);				
		ELSIF (idxReceive = 34) THEN				
			enableAcq <= dataFromFIFO(0);
		END IF;
	END IF;
END IF;
END PROCESS;

END Parameters_architecture;
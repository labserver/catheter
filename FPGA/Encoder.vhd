LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;


--  Entity Declaration
ENTITY Encoder IS
	PORT
	(
		RSTn : IN STD_LOGIC;
		clk48MHz : IN STD_LOGIC;
		gearRatio : IN UNSIGNED(7 DOWNTO 0);
		POSITIONS_PER_TURN : IN INTEGER RANGE 0 TO 1024; --Nombre de positions de l'encodeur optique par tour		
		encoderIn1 : IN STD_LOGIC;	--Signal from the optical encoder
        encoderIn2 : IN STD_LOGIC;	--Signal from the optical encoder
		encoderLine : BUFFER UNSIGNED(19 DOWNTO 0); --Position of the motor
		encoderLineChanged : OUT STD_LOGIC;
		position : BUFFER UNSIGNED(11 DOWNTO 0); --Position of the motor
        positionAdvancedChanged : OUT STD_LOGIC;
		
		ENCODER_TO_POSITION : IN INTEGER RANGE 0 TO 255; --LINES_PER_TURN_MOTOR*GEAR_RATIO/nPositionPerTurns
		LINES_PER_TURN : IN INTEGER RANGE 0 TO 32000;--LINES_PER_TURN_MOTOR*GEAR_RATIO (=512*2). Number of lines of the optical encoder per turn on the catheter.
		
		error : BUFFER STD_LOGIC; --Error if the motor is turning in the wrong side
		LED : OUT STD_LOGIC
	);
END Encoder;

--  Architecture Body 
ARCHITECTURE Encoder_architecture OF Encoder IS

	TYPE stateMachine IS (ENCODER0, ENCODER1);
	SIGNAL state : stateMachine;
    
	SIGNAL counterDebounce : INTEGER RANGE 0 TO 1000; --A debouncer is integrated (10µs)
	SIGNAL encoderInDebounced : STD_LOGIC;
	SIGNAL encoderInPrevious : STD_LOGIC;
	SIGNAL positionCounter : INTEGER RANGE 0 TO 100; --Used to help calculate the position
		
BEGIN

PROCESS(clk48MHz, RSTn)
--A variable is used for nextState to detect the change at the same clock edge
VARIABLE nextState : stateMachine := ENCODER0;
BEGIN

	IF RSTn = '0' THEN	
		encoderInDebounced <= '0';

		encoderLine <= (OTHERS => '0');
		state <= ENCODER0;	
		nextState := ENCODER0;
		
		error <= '0';
		encoderLineChanged <= '0';
		positionCounter <= 0;
		LED <= '0';
        counterDebounce <= 0;
        positionAdvancedChanged <= '0';

	ELSIF (clk48MHz'EVENT AND clk48MHz = '1') THEN
		error <= '0';
		encoderLineChanged <= '0';
		LED <= '0';
	
		CASE state IS
			WHEN ENCODER0 =>
                --Incrementing the position
				IF encoderInDebounced = '1' THEN --AND encoderIn2 = '0' THEN
					nextState := ENCODER1;	
                    encoderLineChanged <= '1';
					positionCounter <= positionCounter + 1;
					IF positionCounter + 1 >= ENCODER_TO_POSITION THEN --LINES_PER_TURN/256--512lines*gearRatio/256pos
						positionCounter <= 0;
						IF position < POSITIONS_PER_TURN-1 THEN
							position <= position + 1;
						ELSE
							position <= (OTHERS => '0');
						END IF;
                        positionAdvancedChanged <= '1';
					END IF;
					IF UNSIGNED(encoderLine) < LINES_PER_TURN-1 THEN
						encoderLine <= encoderLine + 1;
					ELSE
						encoderLine <= (OTHERS => '0');
					END IF;	
				END IF;
				
			WHEN ENCODER1 =>
                
				--Wait for encoderIn back to 0
				IF encoderInDebounced = '0' THEN --AND encoderIn2 = '1' THEN
                    --Leave positionAdvancedChanged to '1' for at least 1 µs
                    positionAdvancedChanged <= '0';
                    --positionCounter <= 0;
					nextState := ENCODER0;				
				END IF;	
                
			WHEN OTHERS =>
				nextState := ENCODER0;
	
		END CASE;
		
        encoderInPrevious <= encoderIn2;
        IF counterDebounce = 0 THEN        
            IF encoderIn2 /= encoderInPrevious THEN --Edge detected
                counterDebounce <= 1;
            END IF;     
        ELSIF counterDebounce < 100 THEN
            IF encoderIn2 /= encoderInPrevious THEN --Edge detected
                counterDebounce <= 0; --No edge must be detected (otherwise it is a glitch)
            ELSE
                counterDebounce <= counterDebounce + 1;
            END IF;
        ELSE --100 cycles with the same value
            counterDebounce <= 0;
            IF encoderIn2 = encoderInPrevious THEN
                encoderInDebounced <= encoderIn2;
            END IF;
        END IF;
      
        state <= nextState;

	END IF;
END PROCESS;
	
END Encoder_architecture;				
				
				
				
				
---------------------------------------------------------------
-- Test Bench for Multiplexer (ESD figure 2.5)
-- by Weijun Zhang, 04/2001
--
-- four operations are tested in this example.
---------------------------------------------------------------

library IEEE;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

entity TB_PullbackMotor is			-- empty entity
end TB_PullbackMotor;

---------------------------------------------------------------

architecture TB of TB_PullbackMotor is

    -- initialize the declared signals  
	SIGNAL PORSTn : STD_LOGIC;
	SIGNAL clk125MHz : STD_LOGIC;

	SIGNAL pullbackMotorDo : STD_LOGIC; --The acquisition module will tell this module to change slice. It will wait for the done signal
	SIGNAL pullbackMotorReset : STD_LOGIC; --Connected to startAcq, when '0' the motor slowly go to 0.
	SIGNAL pullbackMotorDone : STD_LOGIC; --Tells the acquisition module that the pullback is done
	SIGNAL distanceBetweenSlices : UNSIGNED(11 DOWNTO 0); --Distance to move at eack pullback

	SIGNAL pullbackMotorValue : UNSIGNED(11 DOWNTO 0); --To DAC
	SIGNAL pullbackMotorFeedback : UNSIGNED(15 DOWNTO 0); --Feedback of the position of the motor
	SIGNAL error : STD_LOGIC; --Error if the motor is turning in the wrong side
	SIGNAL LED : STD_LOGIC;
	
    component PullbackMotor
		PORT
		(
			PORSTn : IN STD_LOGIC;
			clk125MHz : IN STD_LOGIC;

			pullbackMotorDo : IN STD_LOGIC; --The acquisition module will tell this module to change slice. It will wait for the done signal
			pullbackMotorReset : IN STD_LOGIC; --Connected to startAcq, when '0' the motor slowly go to 0.
			pullbackMotorDone : OUT STD_LOGIC; --Tells the acquisition module that the pullback is done
			distanceBetweenSlices : IN UNSIGNED(11 DOWNTO 0); --Distance to move at eack pullback

			pullbackMotorValue : BUFFER UNSIGNED(11 DOWNTO 0); --To DAC
			pullbackMotorFeedback : IN UNSIGNED(15 DOWNTO 0); --Feedback of the position of the motor
			error : BUFFER STD_LOGIC; --Error if the motor is turning in the wrong side
			LED : OUT STD_LOGIC
		);
    end component;

begin

    U_PullbackMotor: PullbackMotor port map (PORSTn, clk125MHz, pullbackMotorDo, pullbackMotorReset, pullbackMotorDone, distanceBetweenSlices, pullbackMotorValue, pullbackMotorFeedback, error, LED);

	--TIMESCALE 1ns/1ps;
	
	RSTn_gen : PROCESS IS
	BEGIN
		PORSTn <= '0';
		WAIT FOR 100 ns;
		PORSTn <= '1';
		WAIT;
	END PROCESS;	

	clock_gen : PROCESS IS
	BEGIN
		clk125MHz <= '0' after 8 ns, '1' after 16 ns;
		WAIT FOR 16 ns;	
	END PROCESS;

	distanceBetweenSlices <= "001000000000"; --A bit more than 1cm
	
	PROCESS IS
	BEGIN
		pullbackMotorDo <= '0';	
		pullbackMotorReset <= '0';
		WAIT FOR 1000 ns;
		FOR i in 0 TO 3 LOOP
			pullbackMotorDo <= '1', '0' after 20 ns;
			WAIT FOR 20000000 us;		
		END LOOP;
		pullbackMotorReset <= '1', '0' after 20 ns;
	END PROCESS;


end TB;

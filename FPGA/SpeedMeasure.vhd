LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;


--  Entity Declaration

ENTITY SpeedMeasure IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		clk1MHz : IN STD_LOGIC;
		RSTn : IN STD_LOGIC;
		POSITIONS_PER_TURN : IN INTEGER RANGE 0 TO 1024;
		position : IN UNSIGNED(11 DOWNTO 0);
		motorSync : IN STD_LOGIC;
		durationPosition : BUFFER UNSIGNED(31 DOWNTO 0);
        error : OUT STD_LOGIC
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END SpeedMeasure;


--  Architecture Body 

ARCHITECTURE SpeedMeasure_architecture OF SpeedMeasure IS
    
	SIGNAL counterPosition : INTEGER RANGE 0 TO 200000000;
	SIGNAL positionPrevious : UNSIGNED(11 DOWNTO 0);
    SIGNAL firstState : STD_LOGIC := '1'; --Signal to make sure a change in position is not detected at the very first state of an acquisition
			
BEGIN

PROCESS(clk1MHz, RSTn, motorSync)
	
BEGIN
		IF (RSTn = '0' OR motorSync = '0') THEN	--If we are not synchronized with the motors, the duration of a position is irrelevent		
			durationPosition <= (OTHERS => '1');
			counterPosition <= 0;
            error <= '0';  
            positionPrevious <= (OTHERS => '0');
            firstState <= '1';
							
		ELSIF (clk1MHz'EVENT AND clk1MHz = '1') THEN
			positionPrevious <= position;	
			durationPosition <= durationPosition;
            error <= '0';
            IF firstState = '1' THEN
                firstState <= '0';
            END IF;
			--Position changed		
			IF positionPrevious /= position AND firstState = '0' THEN
				IF (position > 0 AND positionPrevious = position-1) OR (position = 0 AND positionPrevious = POSITIONS_PER_TURN-1) THEN
                    --Good
                ELSE
                    error <= '1';
                END IF;    
				durationPosition <= UNSIGNED(CONV_STD_LOGIC_VECTOR(counterPosition, 32));
						
				counterPosition <= 1;
			ELSE
				counterPosition <= counterPosition + 48;
			END IF;		
				
		END IF;
END PROCESS;
	
END SpeedMeasure_architecture;			
# README #

### What is this repository for? ###

* Matlab code for acquisition, analysis, display and saving of the multimodal catheter system.
* FPGA code for the electronic circuit.
* USB microcontroller code used for data transfer and USB-Blaster emulator.

### How do I get set up? ###

* Install LibUSB and link it to the USB device of the catheter system (the computer might have to be in test mode)
* Install Matlab (a mex compiler might be required)
* Run PAUSFluo GUI in Matlab.

### Who do I talk to? ###

* Maxime Abran
* Frédéric Lesage
* Google
%
% Performs the signal processing calculations, such as angle shifts,
% removing the fluorescence baseline, hiding signals out of range, downsampling, band pass filtering, Hilbert transform
%   
% INPUTS:
%
% param: Structure containing all the parameters used in the acquisition.
%
% displayInfo: Structure containing the display information.
%
% acqInfo: Structure containing variables used when reading data from the FPGA to synchronize all the processes.
%
% PA, US, fluo: Data to process of the three modalities.
%
% fluoOnly: Used to skip PA and US processing to save time.
%
% OUTPUTS:
%
% PAF, USF, fluoF: Data processed of the three modalities.
%
% COMMENTS:
%
function [PAF,USF,fluoF] = AnalysisFilter(param, displayInfo, acqInfo, PA, US, fluo, fluoOnly)

if nargin < 7
    fluoOnly = 0;
end

%When the acquisition is running, we use the conventionnal filter. If not, the filter is longer but there is no phase shift.
% ultrafast = acqInfo.acqRunning;
ultrafast = 1; %TODO: Check difference

dS = displayInfo.downSampleValue;

%Filtered signals
PAF = single(zeros(dS*displayInfo.nSamplesToDisplay,param.nPositionsPerTurn));
USF = single(zeros(dS*2*displayInfo.nSamplesToDisplay,param.nPositionsPerTurn));
fluoF = single(zeros(4,param.nPositionsPerTurn));

%Calculate the information of the butterworth filter
numLPA = displayInfo.filterButter.numLPA; denomLPA = displayInfo.filterButter.denomLPA; 
numHPA = displayInfo.filterButter.numHPA; denomHPA = displayInfo.filterButter.denomHPA;
numLUS = displayInfo.filterButter.numLUS; denomLUS = displayInfo.filterButter.denomLUS; 
numHUS = displayInfo.filterButter.numHUS; denomHUS = displayInfo.filterButter.denomHUS;

%First, we filter the fluorescence signal
numL = 0; denomL = 0; numH = 0; denomH = 0;
if displayInfo.fluoLowPassF > 0
    [numL, denomL] = butter(displayInfo.fluoFilterButterOrder, displayInfo.fluoLowPassF/(param.nPositionsPerTurn/2),'low');
end
if displayInfo.fluoHighPassF > 0
    [numH, denomH] = butter(displayInfo.fluoFilterButterOrder, displayInfo.fluoHighPassF/(param.nPositionsPerTurn/2),'high');
end
for idxDemod = 1:4   
    if displayInfo.filterWrapFluo ~= 0 %Concatenate the signal 3 times
        fluoTemp = [fluo(1:end,idxDemod) ; fluo(1:end,idxDemod) ; fluo(1:end,idxDemod)];
    end    
    [fluoTemp] = filterButter(single(fluoTemp), displayInfo.fluoLowPassF, displayInfo.fluoHighPassF, numL, denomL, numH, denomH, ultrafast);
    if displayInfo.filterWrapFluo ~= 0 %Recover the signal in the middle
        fluoF(idxDemod,:)=fluoTemp(length(fluoTemp)/3+1:2*length(fluoTemp)/3);
    end       
end

%Change angle
nPos=param.nPositionsPerTurn;
aS=displayInfo.angleShiftFluo;
if aS > 0
    fluoFTemp = fluoF(:,nPos-aS+1:nPos);
    fluoF(:,aS+1:end) = fluoF(:,1:nPos-aS);
    fluoF(:,1:aS) = fluoFTemp;
elseif aS < 0
	aS = abs(aS);
    fluoFTemp = fluoF(:,1:aS);
    fluoF(:,1:nPos-aS) = fluoF(:,aS+1:nPos);
    fluoF(:,nPos-aS+1:nPos) = fluoFTemp;
end

if (displayInfo.fluoRemoveBaseline)
    %Remove fluorescence baseline (after filtering?)
    if displayInfo.fluoRemoveBaselineStatus == '1' %Baseline is valid
        fluoF = fluoF - displayInfo.fluoBaseline;
    else
        %AnalysisFilter is called during the calculation of the baseline.
        %Don't do anything here.        
    end
end

if fluoOnly == 1
    return
end

aS=displayInfo.angleShiftPA;
if aS > 0
    PATemp = PA(:,nPos-aS+1:nPos);
    PA(:,aS+1:end) = PA(:,1:nPos-aS);
    PA(:,1:aS) = PATemp;
elseif aS < 0
	aS = abs(aS);
    PATemp = PA(:,1:aS);
    PA(:,1:nPos-aS) = PA(:,aS+1:nPos);
    PA(:,nPos-aS+1:nPos) = PATemp;
end
aS=displayInfo.angleShiftUS;
if aS > 0
    USTemp = US(:,nPos-aS+1:nPos);
    US(:,aS+1:end) = US(:,1:nPos-aS);
    US(:,1:aS) = USTemp;
elseif aS < 0
	aS = abs(aS);
    USTemp = US(:,1:aS);
    US(:,1:nPos-aS) = US(:,aS+1:nPos);
    US(:,nPos-aS+1:nPos) = USTemp;
end

%We add samples to the vectors because the transducer is not exactly in the middle
if displayInfo.radiusTransducerNSamples > 0
    PAF(1:displayInfo.radiusTransducerNSamples,:) = repmat(PA(1,:),2,displayInfo.radiusTransducerNSamples);
    USF(1:2*displayInfo.radiusTransducerNSamples,:) = repmat(US(1,:),2,2*displayInfo.radiusTransducerNSamples);
end
PAF(dS*displayInfo.radiusTransducerNSamples+1:dS*displayInfo.nSamplesToDisplay,:) = single(PA(1:dS*(displayInfo.nSamplesToDisplay-displayInfo.radiusTransducerNSamples),:));
USF(dS*2*displayInfo.radiusTransducerNSamples+1:dS*2*displayInfo.nSamplesToDisplay,:) = single(US(1:dS*2*(displayInfo.nSamplesToDisplay-displayInfo.radiusTransducerNSamples),:));     

%Filters
PAF = filterButter(PAF, displayInfo.PALowPassF, displayInfo.PAHighPassF, numLPA, denomLPA, numHPA, denomHPA, ultrafast);
USF = filterButter(USF, displayInfo.USLowPassF, displayInfo.USHighPassF, numLUS, denomLUS, numHUS, denomHUS, ultrafast);

%Downsample after filtering
PAFtemp = PAF(1:dS:end,:);
clear PAF; PAF = PAFtemp;
USFtemp = USF(1:dS:end,:); 
clear USF; USF = USFtemp;

%We put a constant value for the first samples to represent the catheter and hide the pulse
if displayInfo.radiusCatheterFlushNSamples > 0
    PAF(1:displayInfo.radiusCatheterFlushNSamples,:) = 0;
    USF(1:2*displayInfo.radiusCatheterFlushNSamples,:) = 0;
end
 
%Enveloppe with hilbert transform
if displayInfo.PAHilbert == 1
    PAF=abs(hilbert(PAF));
end
if displayInfo.USHilbert == 1
    USF=abs(hilbert(USF));  
    %Additional high pass filter after the Hilbert transform (1 MHz)
%     USF = USF - repmat(mean(USF(:,end-2000:end),2),1,size(USF,2));
end

%Draw lines at 0.5mm to 5mm
% USF(round(1.2e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue):round(1.2e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue)+5,:) = 250;
% USF(round(1.7e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue):round(1.7e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue)+5,:) = 250;
% USF(round(2.2e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue):round(2.2e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue)+2,:) = 250;
% USF(round(2.7e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue):round(2.7e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue)+2,:) = 250;
% USF(round(3.2e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue):round(3.2e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue)+2,:) = 250;
% USF(round(3.7e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue):round(3.7e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue)+2,:) = 250;
% USF(round(4.2e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue):round(4.2e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue)+2,:) = 250;
% USF(round(4.7e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue):round(4.7e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue)+2,:) = 250;
% USF(round(5.2e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue):round(5.2e-3*2/param.speedUltrasound*param.transducerAcquisitionFreq/displayInfo.downSampleValue)+1,:) = 250;

% for idxAngle = 1:256
%     if max(USF(idxAngle,1:700)) > 0.5*max(max(USF(:,1:700)))
%         [~,distanceUS(idxAngle)] = max(USF(idxAngle,1:700));
%     else
%         distanceUS(idxAngle) = 1;
%     end
%     if distanceUS(idxAngle) > 1
%         fluoF(1,idxAngle) = fluoF(1,idxAngle).*exp(0.01*(distanceUS(idxAngle)-400));  
%     end
% end

% load('psfSiliconCentered3.mat')
% %Add padding
% USFtemp=[USF(end-99:end,:);USF;USF(1:100,:)];
% psf=[zeros(100,1500);psf;zeros(100,1500)];
% 
% % Maintenant faisons la deconvolution Wiener
% sigma=100;
% deconvim_denoised=fft2(USFtemp).*conj(fft2(psf))./(fft2(psf).*conj(fft2(psf))+sigma);
% USFtemp=abs(fftshift(ifft2(deconvim_denoised)));
% %Remove padding
% USF = USFtemp(101:end-100,:);


% fluoF = log(abs(fluoF)+1);


return
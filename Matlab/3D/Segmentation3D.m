%Function for manual segmentation of multiple images
%Loads a 3D matrix in a .mat file (multiple slices in PA, US or Fluo)
%Displays each slice and allows to segment the loction of the inclusion
%Save the segmented 3D matrix with the same dimensions
function Segmentation3D()

%Load file
[filename, pathname] = uigetfile('*.mat','Select a file containing results in a 3D matrix');
if filename == 0
    return
end
load([pathname filename]);

%Check filetype
if exist('PA','var')
    data = PA;
elseif exist('US','var')
    data = US;
elseif exist('fluo','var')
    data = fluo;
else
    warndlg('Invalid file.')
    return
end

nSlices = size(data,1);
segmentedData = zeros(size(data));

%Start segmentation for each slice
for idxSlice = 1:nSlices
    slice = squeeze(data(idxSlice,:,:));
    
    %Loop until segmentation is valid and user is happy
    isSegmented = 0;
    while isSegmented ~= 1
        %Display current slice
        hFig = figure(1); hAxes = subplot(1,1,1);
        hImage = imagesc(slice);
        axis off;
        set(hFig,'name',['Segmentation of slice ' num2str(idxSlice) '/' num2str(nSlices)])
        
        %Allow free hand selection
        fcn = makeConstrainToRectFcn('imfreehand', get(hAxes, 'XLim'), get(hAxes, 'YLim'));
        h = imfreehand(hAxes, 'PositionConstraintFcn', fcn);
        if isSegmented ~= 2
            mask = createMask(h, hImage);
        else
            mask = mask | createMask(h, hImage); %Add to last segmentation    
        end
        hold on
        hAlpha = imagesc(double(mask));
        hold off
        set(hAlpha,'alphadata',0.5*mask)
        
        if max(mask(:)) < 1
            warndlg('Invalid segmentation. Try again.')
        else
            ButtonName = questdlg('Is the segmentation correct?', ...
                         ['Validation of slice ' num2str(idxSlice) '/' num2str(nSlices)], ...
                         'Yes', 'No, restart', 'No, add', 'Yes');
            switch ButtonName,
                case 'Yes',
                    isSegmented = 1;
                case 'No, add',
                    isSegmented = 2; %Special code to add to last segmentation      
                case 'No, restart',
                    isSegmented = 0;                      
            end
        end
    end
    
    %Segmentation worked
    segmentedData(idxSlice,:,:) = mask;

end
 
%Save segmented data
[~,origFilename,ext] = fileparts(filename);
if exist('PA','var')
    PASeg = segmentedData;
    save([pathname origFilename '_Segmented' ext],'PASeg');
elseif exist('US','var')
    USSeg = segmentedData;
    save([pathname origFilename '_Segmented' ext],'USSeg');
elseif exist('fluo','var')
    fluoSeg2D = segmentedData;
    save([pathname origFilename '_Segmented2D' ext],'fluoSeg2D');
end

%Display segmented data
for idxSlice = 1:nSlices
    %Display current slice
    hFig = figure(2); hAxes = subplot(1,1,1);
    hImage = imagesc(slice);
    axis off;
    hold on;
    set(hFig,'name',['Segmentation of slice ' num2str(idxSlice) '/' num2str(nSlices)])

    hAlpha = imagesc(double(squeeze(segmentedData(idxSlice,:,:))));
    hold off
    set(hAlpha,'alphadata',0.5*squeeze(segmentedData(idxSlice,:,:)))
    pause(0.5)
end
%%
%Code to directly load 2D fluo segmentation, if only this cell is executed
if ~exist('US','var') && ~exist('PA','var') && ~exist('fluoSeg2D','var')
    [filename, pathname] = uigetfile('*.mat','Select a file containing results in a 3D matrix');
    load([pathname filename]);
    [~,origFilename,ext] = fileparts(filename);
end

%Finally, fluorescence segmentation needs US segmentation for 3D
if exist('fluoSeg2D','var')
    [filename, pathname] = uigetfile('*.mat','Select a file containing the ultrasound segmentated data (needed for 3D fluorescence)');
    if filename == 0
        return
    end
    load([pathname filename]);   
    if ~exist('USSeg','var')
        warndlg('Invalid file.')
        return
    end 
    
    %Transform fluo polar to cartesian view  
    nAngles = size(fluoSeg2D,3);
    depth = 100;
    angle=[0 2*pi-(2*pi/nAngles')];
    radius=[0 depth];    
    lin1 = linspace(radius(1), radius(2), depth); %returns the full vector of axis coordinates
    col1 = linspace(angle(1), angle(2), nAngles);    
    xyBounds = linspace(-depth,depth,size(USSeg,3));
    [x2 y2] = meshgrid(xyBounds, xyBounds);

    rho = sqrt(x2.^2 + y2.^2);  % I want values at this distance
    theta = atan2(y2,x2);       % I want values at this angle
    %atan retourne des angles entre -pi et pi et on veut des angles entre 0 et 2pi
    theta_change = find(theta < angle(1));
    theta(theta_change) = theta(theta_change) + 2*pi;
    %Les points du plan cart�sien qui ont un angle theta sup�rieur au dernier
    %angle acquis sont modifi�s afin de permettre l'interpolation
    outbounds = find(theta >= angle(2));
    if ~isempty(outbounds)
        theta(outbounds) = 2*pi-theta(outbounds);
    end
    
    fluoTemp = repmat(fluoSeg2D,1,100);
    segmentedData = zeros(size(USSeg));
    for idxSlice = 1:size(fluoSeg2D,1)
        fluo2D = interp2(col1,lin1,squeeze(fluoTemp(idxSlice,:,:)),theta,rho,'spline');
        fluoSeg3D(idxSlice,:,:) = squeeze(USSeg(idxSlice,:,:)).*round(fluo2D);
    end
    
    %Save segmented data
    fluoSeg = fluoSeg3D;
    save([pathname origFilename '_Segmented' ext],'fluoSeg');   
    
    %Display segmented data
    for idxSlice = 1:nSlices
        %Display current slice
        hFig = figure(2); hAxes = subplot(1,1,1);
        hImage = imagesc(slice);
        axis off;
        set(hFig,'name',['Segmentation of slice ' num2str(idxSlice) '/' num2str(nSlices)])

        hAlpha = imagesc(double(squeeze(fluoSeg(idxSlice,:,:))));
        hold off
        set(hAlpha,'alphadata',0.5*squeeze(fluoSeg(idxSlice,:,:)))
        pause(0.5)
    end    
end


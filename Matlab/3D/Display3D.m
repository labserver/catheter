%Function that displays segmented trimodal data in 3D
%Loads all 3D matrix containing segmented data
%Displays trimodal data in 3D
%Saves a screenshot (put a breakpoint before to rotate the view)
function Display3D()

%Load PA, US and fluo data
[filename, pathname] = uigetfile('*.mat','Select a file containing the photoacoustic segmentated data');
if filename == 0; return; end
load([pathname filename]);   
if ~exist('PASeg','var'); warndlg('Invalid file.'); return; end 

[filename, pathname] = uigetfile('*.mat','Select a file containing the ultrasound segmentated data');
if filename == 0; return; end
load([pathname filename]);   
if ~exist('USSeg','var'); warndlg('Invalid file.'); return; end 

[filename, pathname] = uigetfile('*.mat','Select a file containing the fluorescence segmentated data');
if filename == 0; return; end
load([pathname filename]);   
if ~exist('fluoSeg','var'); warndlg('Invalid file.'); return; end 

%Create catheter in 3D
%Circle with a radius of 35 centered at 250,250 in an 500x500image
[rr, cc] = meshgrid(1:500);
CatheterSeg1 = sqrt((rr-250).^2+(cc-250).^2)<=35;
CatheterSeg = zeros(size(USSeg));
for i = 1:size(USSeg,1)
    CatheterSeg(i,:,:) = CatheterSeg1;
end

%Rotate slices
for i = 1:size(USSeg,1)
    PASeg(i,:,:) = rot90(squeeze(PASeg(i,:,:)));
    USSeg(i,:,:) = rot90(squeeze(USSeg(i,:,:)));
    fluoSeg(i,:,:) = rot90(squeeze(fluoSeg(i,:,:)));
end

%Remove some slices
PASeg = PASeg(9:38,:,:);
USSeg = USSeg(9:38,:,:);
fluoSeg = fluoSeg(9:38,:,:);
CatheterSeg = CatheterSeg(9:38,:,:);

%3D display US+Catheter
close all
figure(3)
p1 = patch(isosurface(CatheterSeg,0.5));
isonormals(CatheterSeg,p1)
set(p1,'facecolor','black','edgecolor','none','FaceAlpha',0.6);
view(44,44); %axis tight; grid on;
camlight; lighting gouraud;
camproj perspective
daspect([1 0.02 1]);
axis([0 500 0 size(USSeg,1) 0 500])
axis off

hold on
p2 = patch(isosurface(USSeg,0.5));
isonormals(USSeg,p2)
set(p2,'facecolor','blue','edgecolor','none','FaceAlpha',1);

%3D display PA+Fluo+Catheter
figure(4)
p1 = patch(isosurface(CatheterSeg,0.5));
isonormals(CatheterSeg,p1)
set(p1,'facecolor','black','edgecolor','none','FaceAlpha',0.6);
view(44,44); %axis tight; grid on;
camlight; lighting gouraud;
camproj perspective
daspect([1 0.02 1]);
axis([0 500 0 size(USSeg,1) 0 500])
axis off

hold on
p2 = patch(isosurface(PASeg,0.5));
isonormals(PASeg,p2)
set(p2,'facecolor','red','edgecolor','none','FaceAlpha',1);

hold on
p2 = patch(isosurface(fluoSeg,0.5));
isonormals(fluoSeg,p2)
set(p2,'facecolor',[0 0.5 0],'edgecolor','none','FaceAlpha',1);

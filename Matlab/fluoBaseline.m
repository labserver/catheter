%
% Calculates the baseline of the fluorescence signal when there is no
% fluorophore. This is used to remove the baseline of the signal, usually
% caused by the imperfection of the rotary joint.
%   
% INPUTS:
%
% param: Structure containing all the parameters used in the acquisition.
%
% displayInfo: Structure containing the display information.
%
% acqInfo: Structure containing variables used when reading data from the FPGA to synchronize all the processes.
%
% OUTPUTS:
%
% fluoBaselineOut: The baseline that was calculated. Must be subtracted from the original fluorescence signal.
%
% sliceBaseline: Slices selected by the user to be the baseline. It represents the location where there is no fluorescence signal.
%
% COMMENTS:
%
function [fluoBaselineOut, sliceBaseline] = fluoBaseline(param, displayInfo, acqInfo)
%Open a dialog box to ask for baseline slices.
if ~isfield(displayInfo,'sliceBaseline')
    options.WindowStyle='normal'; %Used to see dialogs that are under the undocked figure
    sliceBaseline = str2num(char(inputdlg('Enter the slices of the baseline where there is no fluorescence.','Baseline selection',1,{'[1:8]'},options)));
else
    sliceBaseline = displayInfo.sliceBaseline;
end
fluoBaselineOut = zeros(acqInfo.lengthFluo,param.nPositionsPerTurn);
fluoBaselineQuantity = 0;

if acqInfo.oldSavedFiles == 0
    %Retrieve the slice in the .dat files   
    fileFluo = fopen([acqInfo.outputFullFileName '_Fluo.dat'],'r');    
end

%Check how many complete slices there are.
if acqInfo.acqInterrupted == 0
    nSlices = param.nSlices;
else
    nSlices = floor(floor(acqInfo.idxPositionTotal/param.nPositionsPerTurn)/param.nTurnsPerSlice);
end

for idxSliceFluo = 1:nSlices
    %Check if the loaded file is from an older version
    if acqInfo.oldSavedFiles == 0
        %Go to the correct slice.
        type = param.PAUSDataType;
        typeFluo = type; typeFluo(typeFluo < 4) = 4; %There is no fluo data at 16 bits
        fseek(fileFluo, (idxSliceFluo-1)*param.nTurnsPerSlice*param.nPositionsPerTurn*acqInfo.lengthFluo*typeFluo, 'bof');
    end    
    fluoFTemp = zeros(4,param.nPositionsPerTurn);

    for idxTurn = 1:param.nTurnsPerSlice
        %Different with the old version of the results files
        if acqInfo.oldSavedFiles == 1
            %Will not work on old saved files. We would have to transfer
            %results
%             idxTurnTotalFluo = param.nTurnsPerSlice*(idxSliceFluo-1) + idxTurn;
%             fluo = squeeze(results.fluo(idxTurnTotalFluo,:,:));            
        else
            if param.PAUSDataType == 8 %Saved data is in double and must be transposed
                %Old data, in double format
                fluo = reshape(fread(fileFluo, param.nPositionsPerTurn*acqInfo.lengthFluo, 'double'),param.nPositionsPerTurn,acqInfo.lengthFluo);            
                fluo = int32(fluo);
            elseif param.PAUSDataType == 4                  
                fluo = reshape(fread(fileFluo, param.nPositionsPerTurn*acqInfo.lengthFluo, 'int32'),param.nPositionsPerTurn,acqInfo.lengthFluo);                
            else %2
                fluo = reshape(fread(fileFluo, param.nPositionsPerTurn*acqInfo.lengthFluo, 'int32'),param.nPositionsPerTurn,acqInfo.lengthFluo);
            end       
        end        

        [dummy1,dummy2,fluoF] = AnalysisFilter(param, displayInfo, acqInfo, zeros(param.nPositionsPerTurn,param.nSamples), zeros(param.nPositionsPerTurn,2*param.nSamples), fluo, 1);

        %Average all the turns of a slice
        if (mod(idxTurn,param.nTurnsPerSlice) ~= 1 && param.nTurnsPerSlice ~= 1)
            %Not the first turn of a slice
            fluoFTemp = fluoFTemp + fluoF;
        else
            fluoFTemp = fluoF;  
        end 
    end
    %toc
    fluoF = fluoFTemp; 
    
    if find(sliceBaseline == idxSliceFluo) %Current slice is part of the baseline
        fluoBaselineOut = fluoBaselineOut + fluoF;
        fluoBaselineQuantity = fluoBaselineQuantity + param.nTurnsPerSlice;
    end

end
fclose('all');

fluoBaselineOut = fluoBaselineOut ./ fluoBaselineQuantity;
    
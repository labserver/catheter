%
% Calculates the baseline of the fluorescence signal when there is no
% fluorophore. This is used to remove the baseline of the signal, usually
% caused by the imperfection of the rotary joint
%   
% INPUTS:
%
% param: Structure containing all the parameters used in the acquisition.
%
% displayInfo: Structure containing the display information.
%
% acqInfo: Structure containing variables used when reading data from the FPGA to synchronize all the processes.
%
% OUTPUTS:
%
% USLineTimeOut: The US Line-Time image that was calculated.
%
% COMMENTS:
%
function USLineTimeOut = USLineTime(param, displayInfo, acqInfo)

if acqInfo.acqInterrupted == 0
    nSlices = param.nSlices;
else
    nSlices = floor(floor(acqInfo.idxPositionTotal/param.nPositionsPerTurn)/param.nTurnsPerSlice);
end

%Uncomment to display only a few slices. Use an offset if required.
%     nSlices = 90;
    offset = 0;

USLineTimeOut = single(zeros(nSlices,4*displayInfo.nSamplesToDisplay));

if acqInfo.oldSavedFiles == 0
    %Retrieve the slice in the .dat files   
    fileUS = fopen([acqInfo.outputFullFileName '_US.dat'],'r');    
end

for idxSliceUS = 1:nSlices
    %Check if the loaded file is from an older version
    if acqInfo.oldSavedFiles == 0
        %Go to the correct slice.
        type = param.PAUSDataType;
        fseek(fileUS, (offset+idxSliceUS-1)*param.nTurnsPerSlice*param.nPositionsPerTurn*2*param.nSamples*type, 'bof'); 
    end    
    
    USFTemp = zeros(1,2*param.nSamples);

    for idxTurn = 1:param.nTurnsPerSlice
        %Different with the old version of the results files
        if acqInfo.oldSavedFiles == 1
            %Will not work on old saved files. We would have to transfer
            %results            
%             idxTurnTotalUS = param.nTurnsPerSlice*(idxSliceUS-1) + idxTurn;
%             US = squeeze(results.US(idxTurnTotalUS,:,:));            
        else
            if param.PAUSDataType == 8 %Saved data is in double and must be transposed
                %Old data, in double format
                US = reshape(fread(fileUS, param.nPositionsPerTurn*2*param.nSamples, 'double'),param.nPositionsPerTurn,2*param.nSamples);            
                US = int32(US);
            elseif param.PAUSDataType == 4
                US = reshape(fread(fileUS, param.nPositionsPerTurn*2*param.nSamples, 'int32'),2*param.nSamples,param.nPositionsPerTurn);
            else %2
                US = reshape(fread(fileUS, param.nPositionsPerTurn*2*param.nSamples, 'int16'),2*param.nSamples,param.nPositionsPerTurn);                
            end                 
        end        

        [bidon1,USF,bidon2] = AnalysisFilter(param, displayInfo, acqInfo, zeros(param.nSamples,param.nPositionsPerTurn), US, zeros(param.nPositionsPerTurn,acqInfo.lengthFluo), 0);

        %Average all the turns of a slice
        if mod(idxTurn,param.nTurnsPerSlice) ~= 1 && param.nTurnsPerSlice ~= 1 && strcmp(displayInfo.displayMultipleTurns,'Sum')
            %Not the first turn of a slice
            USFTemp = USFTemp + USF;
        else
            USFTemp = USF;  
        end 
    end
    %toc  
    angle = mod(displayInfo.USAngle-1,128)+1;
    angle2 = angle + 128;
%     angle2 = angle + 105; %Uncomment to use a different second angle.
    USLineTimeOut(idxSliceUS,:) = [USFTemp(end:-1:1,angle2); USFTemp(:,angle)];

end

fclose('all');

%
% Read data sent by the FPGA. This data has been stored by a second thread running in the background. 
% The buffer common to both thread is emptied by calling this function.
%   
% INPUTS:
%
% handles: Handles to the GUI, which allows reading of the parameters of the acquisition, the display parameters, the results and the acquisition information set by the user.
%
% hObject: Link to the GUI. It is used to save the handles at the end of the function
%
% OUTPUTS:
%
% none.
%
% COMMENTS:
%
% There is no outputs. However, user data of the handles is updated at the end of the function using the command guidata.
%
function ReadUSB(handles,hObject)

results = getappdata(handles.figure1,'results');

reception = [];
autoStop=0; %Synchronization warning occurs rarely. If they occur sequentially, the program will never recover, so we stop it. Same thing if we don't receive any data.

%Loop until we recover at least one full turn
while (isempty(reception))
    [reception, turnsInBuffer] = ThreadUSB('Read',handles.acqInfo.lengthNormalReception*handles.param.nPositionsPerTurn); %Read one full turn
%     reception = int16(reception);
    if (isempty(reception)); 
        autoStop=autoStop+1;
%         pause(0.01); 
        if 0%autoStop>20000
            stop(handles.timer) %Stop the timer
            set(handles.pbStop, 'Enable', 'off') %Disable the Stop button
            handles.acqInfo.acqInterrupted = 1; %Premature stop (possible corrupted data)
            End(handles, hObject); %Save data and end acquisition
            handles = guidata(hObject); % Retrieve the updated handle after calling a function
            set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), 'Acquisition automatically stopped.'))  
            set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))
            return
        elseif autoStop>600000
            autoStop = 550000;
            disp('Warning: No data');
            pause(0.01);
        end
    else
        autoStop=0;
    end
end

reception = reshape(reception,handles.acqInfo.lengthNormalReception,handles.param.nPositionsPerTurn);
%See Parameters.m for indexes informations.
handles.acqInfo.idxPositionTotal = handles.acqInfo.idxPositionTotal + handles.param.nPositionsPerTurn;
handles.acqInfo.idxTurnTotal = floor((handles.acqInfo.idxPositionTotal-1)/handles.param.nPositionsPerTurn)+1;

if handles.param.PAUSDataType == 4
    US(:,:) = reception(handles.param.nSamples+1:3*handles.param.nSamples,:);
    PA(:,:) = reception(1:handles.param.nSamples,:);    
    fluo(:,:) = reception(handles.acqInfo.lengthPAUS+(1:handles.acqInfo.lengthFluo),:)';
else
    tempUS = reception(handles.param.nSamples/2+1:3*handles.param.nSamples/2,:);
    US(:,:) = reshape(typecast(tempUS(:),'int16'),2*handles.param.nSamples,handles.param.nPositionsPerTurn);
    tempPA = reception(1:handles.param.nSamples/2,:);
    PA(:,:) = reshape(typecast(tempPA(:),'int16'),handles.param.nSamples,handles.param.nPositionsPerTurn);
    fluo(:,:) = int32(reception(handles.acqInfo.lengthPAUS+(1:handles.acqInfo.lengthFluo),:))';
end
%     %Change position for Meunier's laser
%     newUS = results.US;
%     newUS(:,1:2:end) = results.US(:,1:128);
%     newUS(:,2:2:end) = results.US(:,129:256);
%     results.US = newUS;
%     newPA = results.PA;
%     newPA(:,1:2:end) = results.PA(:,1:128);
%     newPA(:,2:2:end) = results.PA(:,129:256);
%     results.PA = newPA;

results.fluoPullback{handles.acqInfo.idxTurnTotal} = mean(reception(handles.acqInfo.lengthPAUS+(1:handles.acqInfo.lengthFluo),:),2);
% results.fluoFull2D{handles.acqInfo.idxTurnTotal} = reception(handles.acqInfo.lengthPAUS+(1:handles.acqInfo.lengthFluo),:);

results.ADC8CH{handles.acqInfo.idxTurnTotal} = double(reception(handles.acqInfo.lengthPAUS+handles.acqInfo.lengthFluo+1:handles.acqInfo.lengthPAUS+handles.acqInfo.lengthFluo+handles.acqInfo.lengthADC8CH,:));

%Additionnal information is received (scanPos, errors/warnings, multiple durations & voltage for motor)
results.scanPos{handles.acqInfo.idxTurnTotal} = mod(reception(handles.acqInfo.lengthPAUS+handles.acqInfo.lengthFluo+handles.acqInfo.lengthADC8CH+1,:),handles.param.nPositionsPerTurn);
results.scanPosIsPAPulse{handles.acqInfo.idxTurnTotal} = (reception(handles.acqInfo.lengthPAUS+handles.acqInfo.lengthFluo+handles.acqInfo.lengthADC8CH+1,:) > handles.param.nPositionsPerTurn);
results.error{handles.acqInfo.idxTurnTotal} = reception(handles.acqInfo.lengthPAUS+handles.acqInfo.lengthFluo+handles.acqInfo.lengthADC8CH+2,:);
results.durationAcquisition{handles.acqInfo.idxTurnTotal} = double(reception(handles.acqInfo.lengthPAUS+handles.acqInfo.lengthFluo+handles.acqInfo.lengthADC8CH+3,:))/handles.param.freqFPGA;
results.durationDemod{handles.acqInfo.idxTurnTotal} = double(reception(handles.acqInfo.lengthPAUS+handles.acqInfo.lengthFluo+handles.acqInfo.lengthADC8CH+4,:))/handles.param.freqFPGA;
results.durationPosition{handles.acqInfo.idxTurnTotal} = double(reception(handles.acqInfo.lengthPAUS+handles.acqInfo.lengthFluo+handles.acqInfo.lengthADC8CH+5,:))/handles.param.freqFPGA;
results.durationTransfer{handles.acqInfo.idxTurnTotal} = double(reception(handles.acqInfo.lengthPAUS+handles.acqInfo.lengthFluo+handles.acqInfo.lengthADC8CH+6,:))/handles.param.freqFPGA;
results.voltageMotor{handles.acqInfo.idxTurnTotal} = double(reception(handles.acqInfo.lengthPAUS+handles.acqInfo.lengthFluo+handles.acqInfo.lengthADC8CH+7,:))/2^12*12; %12bits et 12Volts
results.durationBetweenTransfers{handles.acqInfo.idxTurnTotal} = double(reception(handles.acqInfo.lengthPAUS+handles.acqInfo.lengthFluo+handles.acqInfo.lengthADC8CH+8,:))/handles.param.freqFPGA;
results.durationSDRAMWrite{handles.acqInfo.idxTurnTotal} = double(reception(handles.acqInfo.lengthPAUS+handles.acqInfo.lengthFluo+handles.acqInfo.lengthADC8CH+9,:))/handles.param.freqFPGA;

 %Test gain
% if (~isfield(handles,'gainTest')); handles.gainTest = 0; end 
% handles.gainTest = handles.gainTest+4095/50;
% if (floor(handles.gainTest) > 4095); handles.gainTest = 0; end
% gainTest = floor(handles.gainTest);
% clear send_data
% send_data(1) = 6+65536*9;
% send_data(2) = gainTest;
% ThreadUSB('Write',int32(send_data))        
% clear send_data
% send_data(1) = 6+65536*10;
% send_data(2) = gainTest;
% ThreadUSB('Write',int32(send_data))  
% gainTest
% 
%         %Checking scanPos. scanPos is the angular position in the reception vector. It should match with the Matlab index.
%         if ~isequal(mod(results.scanPos(handles.acqInfo.idxPositionTotal+[-handles.param.nPositionsPerTurn+1:0],1),handles.param.nPositionsPerTurn)',0:handles.param.nPositionsPerTurn-1)
%             set(handles.lbProblems, 'String', strvcat(get(handles.lbProblems, 'String'), 'Error: Wrong angular position read.'))
%             set(handles.lbProblems,'Value',size(get(handles.lbProblems, 'String'),1))
%             disp(['Error. Wrong angular position read. Read:' num2str(results.scanPos(handles.acqInfo.idxPositionTotal,1)) 'Should be:' num2str(handles.acqInfo.idxPosition - 1)])
%             stop(handles.timer) %Stop the timer
%             set(handles.pbStop, 'Enable', 'off') %Disable the Stop button
%             handles.acqInfo.acqInterrupted = 1; %Premature stop (possible corrupted data)
%             End(handles, hObject); %Save data and end acquisition
%             handles = guidata(hObject); % Retrieve the updated handle after calling a function
%             set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), 'Acquisition automatically stopped.'))    
%             set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))
%         end
  
%Skip display if the buffer contains at least one more turn (except if it is the last one)
if turnsInBuffer >= 30 && handles.acqInfo.idxTurnTotal ~= handles.param.nTurnsPerSlice
    %Skip 1/2 the data if more than 10 turns/second, if there is data accumulating in the buffer
    if handles.param.speedMotorTurnsPerSec >= 20 && mod(handles.acqInfo.idxTurnTotal,2) == 0
%         set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), ['Skipped display1/2. ' num2str(turnsInBuffer) ' turns in the buffer'])) 
%         set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))            
        handles.displayInfo.skipDisplay = 1;
    %Skip only if the buffer is expected to have more than 150 turns at the end of the acquisition        
    elseif double(turnsInBuffer*handles.param.nTurns/handles.acqInfo.idxTurnTotal) > 150 && turnsInBuffer >= 40;
    %Check if it is the last turn of a slice, if it is not, no need to skip display BUG: NOT TRUE
%             if mod(handles.acqInfo.idxTurnTotal,handles.param.nTurnsPerSlice) == 0
        set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), ['Skipped display. ' num2str(turnsInBuffer) ' turns in the buffer'])) 
        set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))
        handles.displayInfo.skipDisplay = 1;
%             end
    end
end

if handles.param.saveSingleImagePerSlice == 0
    if handles.param.PAUSDataType == 4
        type = 'int32';
    else
        type = 'int16';            
    end
    fwrite(handles.acqInfo.filePA, PA, type);
    fwrite(handles.acqInfo.fileUS, US, type);
    fwrite(handles.acqInfo.fileFluo, fluo, 'int32');
end
setappdata(handles.figure1,'results',results); %Save results before calling a function
ProcessReadData(handles, hObject, PA, US, fluo, results.scanPosIsPAPulse)
handles = guidata(hObject); % Retrieve the updated handle after calling a function
% results = getappdata(handles.figure1,'results'); %Retrieve results after calling a function

%Check if user wants to change TGC
if get(handles.cbChangeTGC,'Value') == 1
    clear send_data
    send_data(1) = 6+65536*20; 
    send_data(2) = uint32(handles.param.TGC1/50*4095/4);
    send_data(3) = 6+65536*21; 
    send_data(4) = uint32(handles.param.TGC2/50*4095/4);
    send_data(5) = 6+65536*22; 
    send_data(6) = uint32(handles.param.TGC3/50*4095/4);
    send_data(7) = 6+65536*23; 
    send_data(8) = uint32(handles.param.TGC4/50*4095/4);            
    ThreadUSB('Write',int32(send_data))
    disp('Changed TGC')
    handles.acqInfo.TGCChangeDuringAcquisition = 1;
end

% clear send_data;
% send_data(1) = 6; %Code 6 starts the reception of one parameter
% send_data(2) = 5; %Parameter 5 to change
% send_data(3) = mod(handles.acqInfo.idxTurnTotal,256);		
% send_data(4) = floor(handles.acqInfo.idxTurnTotal/256);
% send_data = uint8(send_data);
% %Send data
% ThreadUSB('Write',send_data)

guidata(hObject, handles); % Update handles at the end of a function

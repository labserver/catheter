%
% Initializes the structures needed to perform an acquisition.
%   
% INPUTS:
%
% handles: handles to the GUI, which allows reading of the parameters set
% by the user.
%
% loadParam: Optional parameter. If it is set, the structure will be initialized using the content of loadParam
%   loadParam should be a structure with these fields: param, results & acqInfo
%   loadParam is usually set when the user loaded previous results.
%
% OUTPUTS:
%
% param: Structure containing all the parameters used in the acquisition.
%
% results: Structure containing all the preallocated results matrix, except PA, US and fluo results.
%
% acqInfo: Structure contains variables used when reading data from the FPGA to synchronize all the processes.
%
% COMMENTS:
%
% Unless otherwise noted, all parameters are set using SI units (meters, seconds, Hz)
% Parameters without tabs are the ones that can be edited from this file
% Parameters with one tab are edited in the GUI
% Parameters with two tabs should never be edited
%
function [param, results, acqInfo] = Parameters(handles,loadParam)
%We use the information from the GUI to set the parameters
if nargin >= 2
    %Load a file that contains everything
    param = [];
    results = [];
    acqInfo = [];
    return
else
%     %Debug: Keep PANoise
%     if (isfield(handles,'results'));
%         if (isfield(handles.results,'PANoise'));
% %             results.PANoise = handles.results.PANoise;
%         end
%     end

    %We use the information from the GUI to set the parameters

        %Fixed parameters. NEVER MODIFY, for information only
    param.speedMotorTurnsPerSec = 10;%7.45;%If CCW, must slow down. %Speed of the rotating motor in turns per second (at the catheter)
    param.gearRatio = 2; %Gear ratio of the rotating motor
    
    if param.speedMotorTurnsPerSec < 10
        %At low speed, operate at 32 bits without dividing
        param.PAUSDataType = 4; %Saved data is either in 16 or 32 bits (2 or 4 bytes)
        param.PAUSDataDivisor = 0; %Power of 2, up to 128. (0=no divide, 1=divide by 2, 2=divide by 4, etc.)
    elseif param.speedMotorTurnsPerSec >= 10
        %At higher speed, operate at 16 bits and dividing data by 4
        param.PAUSDataType = 2; %Saved data is either in 16 or 32 bits (2 or 4 bytes)
        param.PAUSDataDivisor = 2; %Power of 2, up to 128. (0=no divide, 1=divide by 2, 2=divide by 4, etc.)
    end
    param.linearMotorSpeed = 5; %Speed of the linear motor in steps of 0.1mm/s
    param.linearMotorSpeedHome = 20; %Speed of the linear motor in steps of 0.1mm/s
        param.speedMotorTurnsPerMin = param.speedMotorTurnsPerSec*60; %Speed of the rotating motor in turns per minute (at the catheter)
        param.nPositionsPerTurn = 256; %Number of angular positions per turn (at the catheter)
        param.PANTurnsFull = param.nPositionsPerTurn*param.speedMotorTurnsPerSec/20; %Number of turns to acquire a full PA slice. 20 Hz is the fequency of the PA pulsed laser.
        param.durationPosition = 1/param.speedMotorTurnsPerSec/param.nPositionsPerTurn; %Duration that the catheter is aligned at a given position in seconds (�3% variation)
    param.nSamples = 1350; %Number of samples acquired by the transducer for each reading (500 represents ~6mm). Twice the number of samples will be acquired for ultrasound due to the round trip
    if param.nSamples*3*8*param.PAUSDataType*param.nPositionsPerTurn*param.speedMotorTurnsPerSec > 250e6
        param.nSamples = round(250e6/(3*8*param.PAUSDataType*param.nPositionsPerTurn*param.speedMotorTurnsPerSec));
%         warndlg(['The number of samples was invalid. It has been automatically corrected to ' num2str(param.nSamples) '.'],'Problems with parameters')            
    end     
        param.transducerAcquisitionFreq = 200e6; %Acquisition frequency of the ADC reading the analog signal from the transducer in Hz
        param.freqFPGA = 48e6; %Frequency of the main clock of the FPGA
        param.speedUltrasound = 1540; %Speed of ultrasound waves in water in m/s
        param.depthUltrasound = param.nSamples*param.speedUltrasound/param.transducerAcquisitionFreq; %Calculated depth acquired
        param.transducerCentralFreq = 45e6;%Central frequency of the transducer used
        param.freqModulation1 = 100e3; %Modulation frequency for the laser diode 1
        param.freqModulation2 = 100e3; %Modulation frequency for the laser diode 2       
        param.nCyclesDemod = floor(0.6*param.durationPosition*param.freqModulation1); %Number of cycles of demodulation (based on the frequency of diode 1), for each angular position (10% tolerance implemented)

        
    %Make sure number of samples is an even number (To be compatible with 16 bits transfers)
    param.nSamples = param.nSamples-mod(param.nSamples,2);    
    %Variable parameters of the acquisition (Lines with an additionnal tab should not be modified)
    param.constantPullback = get(handles.cbConstantPullback,'Value');
    param.nTurnsPerSlice = str2double(get(handles.etTurnsSlice,'String')); %Number of turns per slice of acquisition. The signal is averaged for all turns
    if (isreal(param.nTurnsPerSlice) == 0 || param.nTurnsPerSlice<=0 || param.nTurnsPerSlice>1000 || mod(param.nTurnsPerSlice,1) ~= 0)
        param.nTurnsPerSlice=double(uint32(param.nTurnsPerSlice-1))+1;
        param.nTurnsPerSlice(param.nTurnsPerSlice>1000)=1000;
        set(handles.etTurnsSlice,'String',num2str(param.nTurnsPerSlice));
        warndlg(['The number of turns per slice was invalid. It has been automatically corrected to ' num2str(param.nTurnsPerSlice) '.'],'Problems with parameters')            
    end    
    param.distanceFirstSlice = 0.001; %Distance of the first slice (in meters)
    if (isreal(param.distanceFirstSlice) == 0 || param.distanceFirstSlice<0.0001 || param.distanceFirstSlice>0.1 || mod(param.distanceFirstSlice,0.00001) ~= 0)
        if param.distanceFirstSlice>0.1
            param.distanceFirstSlice = 0.1;
        else
            param.distanceFirstSlice = 0.001;
        end
        warndlg(['The distance of the first slice was invalid. It has been automatically corrected to ' num2str(param.distanceFirstSlice) '.'],'Problems with parameters')            
    end
        param.distanceFirstSliceDAC = double(uint32(param.distanceFirstSlice*100000)); %Distance to send to the FPGA.
        param.distanceFirstSliceDAC(param.distanceFirstSliceDAC>65535) = 65535;    
    %if constantPullback, update nSlices, because distanceFirstSlice might have changed
    if get(handles.cbConstantPullback,'Value');
        set(handles.etNSlices,'Enable','on')
        totalDistance = str2num(get(handles.etDistanceSlice,'String'))/1000 - param.distanceFirstSlice;
        set(handles.etNSlices,'String',num2str(floor(param.speedMotorTurnsPerSec/(param.linearMotorSpeed/10)*totalDistance*1000)))
        set(handles.etNSlices,'Enable','off')
    end
    param.nSlices = str2double(get(handles.etNSlices,'String')); %Number of slices in an acquisition
    if (isreal(param.nSlices) == 0 || param.nSlices<0 || param.nSlices>10000 || mod(param.nSlices,1) ~= 0)
        param.nSlices=double(uint32(param.nSlices-1))+1;
        param.nSlices(param.nSlices>10000)=10000;
        set(handles.etNSlices,'String',num2str(param.nSlices));
        warndlg(['The number of slices was invalid. It has been automatically corrected to ' num2str(param.nSlices) '.'],'Problems with parameters')            
    end  
    %Get distance between slices. Depends if it is a constant pullback or not
    if ~param.constantPullback
        param.distanceBetweenSlices = str2double(get(handles.etDistanceSlice,'String'))/1000; %Distance between two slices (in meters)
    else
        param.distanceBetweenSlices = (str2double(get(handles.etDistanceSlice,'String'))/1000-param.distanceFirstSlice)/param.nSlices;
    end
    if (isreal(param.distanceBetweenSlices) == 0 || param.distanceBetweenSlices<0 || param.distanceBetweenSlices>0.1)
        if param.distanceBetweenSlices>0.1
            param.distanceBetweenSlices = 0.1;
        else
            param.distanceBetweenSlices = 0;
        end
        set(handles.etDistanceSlice,'String',num2str(param.distanceBetweenSlices*1000));
        warndlg(['The distance between slices was invalid. It has been automatically corrected to ' num2str(param.distanceBetweenSlices) '.'],'Problems with parameters')            
    end 
    %Check if total distance is not too large
    if (param.distanceBetweenSlices*(param.nSlices-1)+param.distanceFirstSlice > 0.1)
        param.distanceBetweenSlices = floor(0.1-param.distanceFirstSlice/(param.nSlices-1)/0.0001)*0.0001;
        param.distanceBetweenSlices(param.distanceBetweenSlices < 0) = 0;
        set(handles.etDistanceSlice,'String',num2str( param.distanceBetweenSlices*1000));
        warndlg(['The distance between slices was invalid. It has been automatically corrected to ' num2str(param.distanceBetweenSlices) '.'],'Problems with parameters')            
    end    
        if (param.constantPullback == 0)
            param.distanceBetweenSlicesDAC = double(uint32(param.distanceBetweenSlices*100000)); %Distance to send to the FPGA.
        else
            param.distanceBetweenSlicesDAC = double(uint32(param.distanceBetweenSlices*param.nSlices*100000)); %Total distance when constant pullback
        end
        param.distanceBetweenSlicesDAC(param.distanceBetweenSlicesDAC>65535) = 65535;
        param.nTurns = param.nTurnsPerSlice*param.nSlices; %Total number of turns in an acquisition
    param.delayBetweenUltrasounds = 2*param.depthUltrasound/param.speedUltrasound + 1.2e-6 + 8e-6; %Minimum delay between two ultrasound readings, to avoid detecting a far echo in the next reading (in seconds) (max 500�s). It is set to the depth to acquire plus 1 �s to let the gain go down to attenuate the pulse.
        param.delayBetweenUltrasoundsNCycles = param.delayBetweenUltrasounds*param.freqFPGA; %Conversion in number of cycles, so that the FPGA can measure it
%This parameter might be ajusted:
        param.nAveragingUltrasoundMax = floor(0.8*param.durationPosition/max(param.delayBetweenUltrasounds,2*param.nSamples/param.transducerAcquisitionFreq))-1; %Maximum number of ultrasound averaging (10% tolerance implemented)
        param.nAveragingUltrasoundMax(param.nAveragingUltrasoundMax>1000) = 1000;
        %If 16 bits data, only 4 averaging are allowed (multiplied by divisor)
        if param.PAUSDataType == 2
            param.nAveragingUltrasoundMax(param.nAveragingUltrasoundMax > 4*2^param.PAUSDataDivisor) = 4*2^param.PAUSDataDivisor;
        end
param.nAveragingUltrasound = floor(param.nAveragingUltrasoundMax); %Number of ultrasound averaging (below the maximum)
param.nAveragingUltrasoundFlush = 0; %The first ultrasound averaging can be flushed, because the high voltage is too powerful
% param.nAveragingUltrasound = 1;

%2100 is the maximum for HL7301MG (45 mW output), 2800 is the maximum for
%L785P090 (90 mW output, maximum is 100 mW). 2800 for HL6545MG (more does not work)
param.laserPower1 = 2100; %Power to the laser diode 1 from 0 to 4095 (not linear) 
param.laserPower2 = 2800; %Power to the laser diode 2 from 0 to 4095 (not linear)
    
param.modulationOn = boolean(0); %NOT WORKING ANYMORE. Activates the modulation of the laser diodes
param.demodulationOn = boolean(0); %NOT WORKING ANYMORE. Activates the demodulation. If not activated, the raw signal is acquired.
param.fluoSingleMeasureOn = boolean(0); %Allows to take only a single measure per angular position on the fluorescence ADC. In other words, it disables the averaging.

param.PMTGain1 = 3500; %Gain of the PMT 1 from 0 to 4095.
param.PMTGain2 = 3200; %Gain of the PMT 2 from 0 to 4095.
    
param.pulserOn = boolean(1); %Activates the ultrasound pulser    
param.photoacousticOn = boolean(0); %Activates the photoacoustic reading 
param.DPSSPhotoacousticOn = boolean(0); %Acitvates photoacoustic with the DPSS laser. A laser pulse is sent at every angular position. Do not activate photoacousticOn at the same time.
param.skipPAAcq = boolean(0); %To acquire US at the same time of the PA pulse, when there is an air bubble
param.saveSingleImagePerSlice = boolean(0); %Allows to save only one PAUSFluo image per slice, and not per turn. Extremely useful for phantoms.
%For now, there is no seperate signal to activate the linear motors, because it allows to keep the same Matlab code for both PCB versions.
param.motorSync = boolean(1); %Synchronization of the acquisition with the rotating motor. When deactivated, the motor still rotates.
param.useLinearMotor = boolean(1); %Activates the movement of the linear motor between each slice. When deactivated, the acquisition is faster.

%Pattern of the ultrasound pulse.
%PulseShaper is at 200 MHz. 16 states are controlled using 16 bits for each + and - pulses.
param.pulseShapeP = bin2dec('0000000000000011');
param.pulseShapeN = bin2dec('0000000000011000');

%Index of the clock cycle when return to zero (RTZ) is activated.
%Clock is at 200 MHz.
param.pulseShapeStartRTZ = 5;
param.pulseShapeStopRTZ = 10;

    param.PAUSInitialGain = str2double(get(handles.etPAUSInitialGain,'String')); %Gain of the PAUS acquisition chain
    param.PAUSPreampGain = 20;
    param.PAUSMinGain = param.PAUSPreampGain+5;
    param.PAUSMaxGain = param.PAUSMinGain+50;
    if (isreal(param.PAUSInitialGain) == 0 || param.PAUSInitialGain<param.PAUSMinGain || param.PAUSInitialGain>param.PAUSMaxGain)
        if param.PAUSInitialGain>param.PAUSMaxGain
            param.PAUSInitialGain = param.PAUSMaxGain;
        else
            param.PAUSInitialGain = param.PAUSMinGain;
        end
        set(handles.etPAUSInitialGain,'String',num2str(param.PAUSInitialGain));
        warndlg(['The initial gain was invalid. It has been automatically corrected to ' num2str(param.PAUSInitialGain) '.'],'Problems with parameters')            
    end
    
    param.transducerGain = double(uint32((param.PAUSInitialGain-param.PAUSMinGain)/50*4095)); %Gain of the signal of the transducer from 0 to 4095 (35 to 85dB).
        param.transducerGainIncrementSample = 0;%1 + 256*1 + 512*1 + 1024*0 + 2048*0; %Not implemented yet. The gain will increment of 10 after every group of samples.
        param.transducerGainIncrementValue = 0;
    param.TGCPA = 0; %TGC on PA can be activated. The delays are a bit different for PA.
    param.TGCUS = 1; %TGC on US can be activated. The delays are a bit different for PA.
    param.TGCCutPulse = 1; %Gain can be set to cut the pulse only.
    param.transducerGainPA = param.transducerGain; %double(uint32((param.PAUSInitialGain-35)/50*4095)); %Gain of photoacoustic can be changed.
    %TGC: Gain is refreshed every 0.5 �s (1/4 mm in US and 1/2 mm in PA)
    param.TGC1 = 0.2*round(5*get(handles.slTGC1,'Value'));
    param.TGC2 = 0.2*round(5*get(handles.slTGC2,'Value'));
    param.TGC3 = 0.2*round(5*get(handles.slTGC3,'Value'));
    param.TGC4 = 0.2*round(5*get(handles.slTGC4,'Value'));
    if param.PAUSInitialGain + 1.5*param.TGC1 > param.PAUSMaxGain
        param.TGC1 = 0.2*floor(5*(param.PAUSMaxGain - param.PAUSInitialGain)/1.5);
        param.TGC2 = 0;
        param.TGC3 = 0;
        param.TGC4 = 0;
        warndlg(['The TGC 1 was invalid. It has been automatically corrected to ' num2str(param.TGC1) '.'],'Problems with parameters')            
    end
    if param.PAUSInitialGain + 1.5*param.TGC1 + 1.5*param.TGC2 > param.PAUSMaxGain
        param.TGC2 = 0.2*floor(5*(param.PAUSMaxGain - param.PAUSInitialGain - 1.5*param.TGC1)/1.5);
        param.TGC3 = 0;
        param.TGC4 = 0;
        warndlg(['The TGC 2 was invalid. It has been automatically corrected to ' num2str(param.TGC2) '.'],'Problems with parameters')            
    end
    if param.PAUSInitialGain + 1.5*param.TGC1 + 1.5*param.TGC2 + 1.5*param.TGC3 > param.PAUSMaxGain
        param.TGC3 = 0.2*floor(5*(param.PAUSMaxGain - param.PAUSInitialGain - 1.5*param.TGC1 - 1.5*param.TGC2)/1.5);
        param.TGC4 = 0;
        warndlg(['The TGC 3 was invalid. It has been automatically corrected to ' num2str(param.TGC3) '.'],'Problems with parameters')            
    end
    if param.PAUSInitialGain + 1.5*param.TGC1 + 1.5*param.TGC2 + 1.5*param.TGC3 + 1.5*param.TGC4 > param.PAUSMaxGain
        param.TGC4 = 0.2*floor(5*(param.PAUSMaxGain - param.PAUSInitialGain - 1.5*param.TGC1 - 1.5*param.TGC2 - 1.5*param.TGC3)/1.5);
        warndlg(['The TGC 4 was invalid. It has been automatically corrected to ' num2str(param.TGC4) '.'],'Problems with parameters')            
    end  
    %Change TGC values (even if nothing has changed, simpler to code)
    set(handles.slTGC1,'Value',param.TGC1);
    set(handles.slTGC2,'Value',param.TGC2);
    set(handles.slTGC3,'Value',param.TGC3);
    set(handles.slTGC4,'Value',param.TGC4);
    set(handles.etTGC1,'Enable','on'); set(handles.etTGC1,'String',param.TGC1); set(handles.etTGC1,'Enable','off');
    set(handles.etTGC2,'Enable','on'); set(handles.etTGC2,'String',param.TGC2); set(handles.etTGC2,'Enable','off');
    set(handles.etTGC3,'Enable','on'); set(handles.etTGC3,'String',param.TGC3); set(handles.etTGC3,'Enable','off');
    set(handles.etTGC4,'Enable','on'); set(handles.etTGC4,'String',param.TGC4); set(handles.etTGC4,'Enable','off');
    
        if (param.delayBetweenUltrasounds<0 || param.delayBetweenUltrasounds>100)
            param.delayBetweenUltrasounds(param.delayBetweenUltrasounds<0) = 0;
            param.delayBetweenUltrasounds(param.delayBetweenUltrasounds>100) = 100;
            warndlg(['The delay between two ultrasound acquisition was invalid. It has been automatically corrected to ' num2str(param.delayBetweenUltrasounds) '.'],'Problems with parameters')            
        end
        if mod(param.nAveragingUltrasound,1) ~= 0 || param.nAveragingUltrasound < 1
            param.nAveragingUltrasound = double(uint32(floor(param.nAveragingUltrasound-1)))+1;
            warndlg(['The number of ultrasound averaging was invalid. It has been automatically corrected to ' num2str(param.nAveragingUltrasound) '.'],'Problems with parameters')            
        end  
        if param.nAveragingUltrasound > param.nAveragingUltrasoundMax
            param.nAveragingUltrasound = param.nAveragingUltrasoundMax;
            warndlg('The number of ultrasound averaging was higher than the maximum accepted. It has been automatically corrected.','Problems with parameters')
        end
        if mod(param.nAveragingUltrasoundFlush,1) ~= 0 || param.nAveragingUltrasoundFlush < 0
            param.nAveragingUltrasoundFlush = double(uint32(param.nAveragingUltrasoundFlush));
            warndlg(['The number of ultrasound averaging was invalid. It has been automatically corrected to ' num2str(param.nAveragingUltrasound) '.'],'Problems with parameters')            
        end            
        if param.nAveragingUltrasoundFlush >= param.nAveragingUltrasound
            param.nAveragingUltrasoundFlush = param.nAveragingUltrasound - 1;
            warndlg('The number of ultrasound averaging was higher than the maximum accepted. It has been automatically corrected.','Problems with parameters')
        end
        if (param.laserPower1<0 || param.laserPower1>4095 || mod(param.laserPower1,1) ~= 0)
            param.laserPower1=double(uint32(param.laserPower1));
            param.laserPower1(param.laserPower1>4095)=4095;
            warndlg(['The laser power of diode 1 was invalid. It has been automatically corrected to ' num2str(param.laserPower1) '.'],'Problems with parameters')            
        end
        if (param.laserPower2<0 || param.laserPower2>4095 || mod(param.laserPower2,1) ~= 0)
            param.laserPower2=double(uint32(param.laserPower2));
            param.laserPower2(param.laserPower2>4095)=4095;
            warndlg(['The laser power of diode 2 was invalid. It has been automatically corrected to ' num2str(param.laserPower2) '.'],'Problems with parameters')            
        end    
        if (param.modulationOn == 1 && param.demodulationOn == 0); 
            param.modulationOn = 0; %if demodulation is off, it makes no sense to activate the modulation
            warndlg('Modulation has been deactivated, because demodulation is deactivated.','Problems with parameters')
        end
        if (param.fluoSingleMeasureOn == 1 && (param.demodulationOn == 1 || param.modulationOn == 1)); 
            param.modulationOn = 0; %if fluoSingleMeasure is on, it makes no sense to activate the modulation
            param.demodulationOn = 0;
            warndlg('Modulation/demodulation has been deactivated, because single measure is activated.','Problems with parameters')
        end
        if (param.PMTGain1<0 || param.PMTGain1>4095 || mod(param.PMTGain1,1) ~= 0)
            param.PMTGain1=double(uint32(param.PMTGain1));
            param.PMTGain1(param.PMTGain1>4095)=4095;
            warndlg(['The gain of the PMT 1 was invalid. It has been automatically corrected to ' num2str(param.PMTGain1) '.'],'Problems with parameters')            
        end
        if (param.PMTGain2<0 || param.PMTGain2>4095 || mod(param.PMTGain2,1) ~= 0)
            param.PMTGain2=double(uint32(param.PMTGain2));
            param.PMTGain2(param.PMTGain2>4095)=4095;
            warndlg(['The gain of the PMT 2 was invalid. It has been automatically corrected to ' num2str(param.PMTGain2) '.'],'Problems with parameters')            
        end   
        if (param.transducerGain<0 || param.transducerGain>4095 || mod(param.transducerGain,1) ~= 0)
            param.transducerGain=double(uint32(param.transducerGain));
            param.transducerGain(param.transducerGain>4095)=4095;
            warndlg(['The gain of the transducer was invalid. It has been automatically corrected to ' num2str(param.transducerGain) '.'],'Problems with parameters')            
        end 
        if (param.photoacousticOn==boolean(1) && mod(param.nTurnsPerSlice,param.PANTurnsFull) ~= 0)
            param.photoacousticOn=boolean(0);
%             warndlg(['The number of turns per slice is invalid. It should be a multiple of ' num2str(param.PANTurnsFull) '. The photoacoustic acquisition has been disabled.'],'Problems with parameters')            
        end 
        if (param.photoacousticOn==boolean(1) && param.constantPullback == 1)
            param.photoacousticOn=boolean(0);
            warndlg('Constant pullback cannot be activated. The photoacoustic acquisition has been disabled.','Problems with parameters')            
        end 
        if (param.photoacousticOn==boolean(1) && ~ismember(param.speedMotorTurnsPerSec,[2.5 5 10 20 30]))
            param.photoacousticOn=boolean(0);
            warndlg('Catheter must be turning at 2.5, 5, 10, 20 or 30 turns/second for now. The photoacoustic acquisition has been disabled.','Problems with parameters')            
        end   
        %Informations on the acquisition
        %Use the one stored in handles at the opening of the function taht is tested for directory
        acqInfo.currentPath = pwd; %Current path when the GUI was opened. This path contains a directory name "Results". Do not use in the code, it is only to be written in the .mat at the end
        acqInfo.lengthPAUS = param.nSamples*3; %4 positions angulaires par vecteur de r�ception, photoacoustique et ultrasons en all�-retour
        %If we are acquiring 16 bits data, length of PAUS is actually 2 times less
        if param.PAUSDataType == 2
            acqInfo.lengthPAUS = acqInfo.lengthPAUS / 2;
        end
        acqInfo.lengthFluo = 2*2; %4 angular positions per reception vectors (2 sources & 2 detectors)
        acqInfo.lengthADC8CH = 8; %8 ADC channels
        acqInfo.lengthInfo = 9; %Length of the additionnal information
        acqInfo.lengthNormalReception = acqInfo.lengthPAUS+acqInfo.lengthFluo+acqInfo.lengthADC8CH+acqInfo.lengthInfo; %Length of a normal reception. Used to make sure that we receive the correct number of data
        acqInfo.idxTurnTotal = 1; %Index of the current turn
        acqInfo.idxPositionTotal = 0; %Index of the current position. It does not roll-back (i. e. "257" will follow "256")
        acqInfo.acqRunning = 0; %Signal indicating that an acquisition is running
        acqInfo.refreshPeriod = 0.02; %Refresh period for the timer
        acqInfo.acqInterrupted = 0; %Indicates that the set of data is incomplete and possibly corrupted
       
        results.ADC8CH = cell(1,param.nTurns);
        results.scanPos = cell(1,param.nTurns);
        results.scanPosIsPAPulse = cell(1,param.nTurns);
        results.error = cell(1,param.nTurns);
        results.durationAcquisition = cell(1,param.nTurns);
        results.durationDemod = cell(1,param.nTurns);
        results.durationPosition = cell(1,param.nTurns);
        results.durationTransfer = cell(1,param.nTurns);
        results.voltageMotor = cell(1,param.nTurns);
        results.durationBetweenTransfers = cell(1,param.nTurns);
        
        results.durationSDRAMWrite = cell(1,param.nTurns);
        results.fluoPullback = cell(1,param.nTurns);   
%         results.fluoFull2D = cell(1,param.nTurns);   
        %Don't initialize, it is slower
%         for idxTurn = 1:param.nTurns
%             results.ADC8CH{idxTurn} = int32(zeros(acqInfo.lengthADC8CH,param.nPositionsPerTurn)); %8CH ADCs signals
%             results.scanPos{idxTurn} = zeros(param.nPositionsPerTurn,1); %List of scanned position
%             results.scanPosIsPAPulse{idxTurn} = zeros(param.nPositionsPerTurn,1); %List of scanned position
%             results.error{idxTurn} = zeros(param.nPositionsPerTurn,1); %List of errors/warnings
%             results.durationAcquisition{idxTurn} = zeros(param.nPositionsPerTurn,1); %List of durations of the ultrasound acquisition
%             results.durationDemod{idxTurn} = zeros(param.nPositionsPerTurn,1); %List of durations of the demodulation
%             results.durationPosition{idxTurn} = zeros(param.nPositionsPerTurn,1); %List of durations at each angular position
%             results.durationTransfer{idxTurn} = zeros(param.nPositionsPerTurn,1); %List of duration of all the USB transfers
%             results.voltageMotor{idxTurn} = zeros(param.nPositionsPerTurn,1); %List of voltage values sent to the motors
%             results.durationBetweenTransfers{idxTurn} = zeros(param.nPositionsPerTurn,1); %List of duration between two transfers
%             results.durationSDRAMWrite{idxTurn} = zeros(param.nPositionsPerTurn,1); %List of duration of all the SDRAM write transfers
%             results.fluoPullback{idxTurn} = zeros(acqInfo.lengthFluo,1); %Fluorescence signal, ignoring the rotation.
%         end
        results.durationTotalAcquisition = 0;
        
        setappdata(handles.figure1,'results',results);

%         clear handles.resultsTemp
        set(handles.cbRemoveFluoBase,'Value',0);
        set(handles.cbRemoveFluoBase,'UserData','0');
        
            %Restore slider
        set(handles.slSlice,'Enable','off'); %Disable slider to make sure the change of the slider value doesn't call a display
        if param.nSlices > 1
            set(handles.slSlice,'Min',1);set(handles.slSlice,'Max',param.nSlices);set(handles.slSlice,'Value',1);
            set(handles.slSlice,'SliderStep',[1/(param.nSlices-1) 1/(param.nSlices-1)]);
        else
            set(handles.slSlice,'Min',0.9);set(handles.slSlice,'Max',param.nSlices);set(handles.slSlice,'Value',1);
            set(handles.slSlice,'SliderStep',[1 1]);
        end
        set(handles.slSlice,'Enable','on'); %Re-enable the slider
  
        %Restore slider
        acqInfo.startTime=datestr(now,'yyyy.mm.dd_HH.MM.SS');
        %Open the data files for writing
        acqInfo.filePA = fopen([acqInfo.currentPath '\Temp\DataPA_StartTime_' acqInfo.startTime '.dat'],'w'); 
        acqInfo.fileUS = fopen([acqInfo.currentPath '\Temp\DataUS_StartTime_' acqInfo.startTime '.dat'],'w'); 
        acqInfo.fileFluo = fopen([acqInfo.currentPath '\Temp\DataFluo_StartTime_' acqInfo.startTime '.dat'],'w'); 
        acqInfo.outputFullFileName = ''; %Will be set in End function.
        
        acqInfo.TGCChangeDuringAcquisition = 0;        
  
end
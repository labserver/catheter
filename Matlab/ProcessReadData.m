%
% Processes the data at the end of the acquisition of a turn.
% Since displaying 2D images (when selected) can take a few seconds, this
% function is not always called at the end of each turn.
%   
% INPUTS:
%
% handles: Handles to the GUI, which allows reading of the parameters of the acquisition, the display parameters, 
% the results and the acquisition information set by the user.
%
% hObject: Link to the GUI. It is used to save the handles at the end of
% the function.
%
% PA, US, fluo: Data to process of the three modalities.
%
% scanPosIsPAPulse: Used to determine if the current angular position
% contains a valid photoacoustic signal. It is part of the results
% structure, but we don't want to pass it with every call to this function.
%
% OUTPUTS:
%
% none.
%
% COMMENTS:
%
% There is no outputs. However, user data of the handles is update at the end of the function using the command guidata
%
function ProcessReadData(handles, hObject, PA, US, fluo, scanPosIsPAPulse)
   
if handles.displayInfo.skipDisplay == 0
    DisplaySettings(handles,hObject)
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
end

%Don't filter if we skip the only display of a slice
if ~(handles.displayInfo.skipDisplay == 1 && handles.param.nTurnsPerSlice == 1)
    %Filter all turns of a slice
    [PAF,USF,fluoF] = AnalysisFilter(handles.param, handles.displayInfo, handles.acqInfo, PA, US, fluo);
end
%Average all the turns of a slice (only if there is more than 1 turn)
if handles.param.nTurnsPerSlice > 1
    %Make sure resultsTemp exists and it is the right size
    if ~isfield(handles,'resultsTemp'); handles.resultsTemp = []; end
    if ~isfield(handles.resultsTemp,'PAF'); handles.resultsTemp.PAF = zeros(handles.displayInfo.nSamplesToDisplay,handles.param.nPositionsPerTurn); end
    if size(handles.resultsTemp.PAF) ~= [handles.displayInfo.nSamplesToDisplay handles.param.nPositionsPerTurn]; handles.resultsTemp.PAF = zeros(handles.displayInfo.nSamplesToDisplay,handles.param.nPositionsPerTurn); end

    if (mod(handles.acqInfo.idxTurnTotal,handles.param.nTurnsPerSlice) ~= 1 && handles.param.nTurnsPerSlice ~= 1)
        %Not the first turn of a slice
        %handles.resultsTemp.USF = handles.resultsTemp.USF + USF;
        handles.resultsTemp.fluoF = handles.resultsTemp.fluoF + fluoF;
        if handles.param.photoacousticOn == 0
            handles.resultsTemp.PAF = handles.resultsTemp.PAF + PAF;  
        end            
        if handles.param.saveSingleImagePerSlice == 1; handles.resultsTemp.fluo = handles.resultsTemp.fluo + fluo; end %Also keep fluo (not filtered), to save its sum
    else
        %First turn of a slice
        handles.resultsTemp.PAF = zeros(handles.displayInfo.nSamplesToDisplay,handles.param.nPositionsPerTurn);
        if handles.param.saveSingleImagePerSlice == 1; handles.resultsTemp.PA = int32(zeros(handles.param.nSamples,handles.param.nPositionsPerTurn)); if handles.param.PAUSDataType == 2; handles.resultsTemp.PA = int16(handles.resultsTemp.PA); end; end %Also keep PA (not filtered), to save its sum
        %handles.resultsTemp.USF = USF;
        handles.resultsTemp.fluoF = fluoF; 
        if handles.param.photoacousticOn == 0
            handles.resultsTemp.PAF = PAF;  
        end
        if handles.param.saveSingleImagePerSlice == 1; handles.resultsTemp.fluo = fluo; end %Also keep fluo (not filtered), to save its sum
    end   
end

if handles.param.photoacousticOn == 1
    scanPosIsPAPulse = scanPosIsPAPulse{handles.acqInfo.idxTurnTotal};
    for ind = find(scanPosIsPAPulse)                    
        %Need 32 slices to get a complete PA acquisition
        if (mod(handles.acqInfo.idxTurnTotal,handles.param.PANTurnsFull) ~= 1 && handles.param.nTurnsPerSlice ~= handles.param.PANTurnsFull) 
            %Not the first 32 turns of a slice
            handles.resultsTemp.PAF(:,ind) = handles.resultsTemp.PAF(:,ind) + PAF(:,ind);
            if handles.param.saveSingleImagePerSlice == 1; handles.resultsTemp.PA(:,ind) = handles.resultsTemp.PA(:,ind) + PA(:,ind); end %Also keep PA (not filtered), to save its sum
        else
            %First 32 turns of a slice
            handles.resultsTemp.PAF(:,ind) = PAF(:,ind);
            if handles.param.saveSingleImagePerSlice == 1; handles.resultsTemp.PA(:,ind) = PA(:,ind); end %Also keep PA (not filtered), to save its sum
        end
%             USF(100:280,ind) = 100000; %Display a white bar where there
%             is a PA pulse
    end
end

if handles.param.saveSingleImagePerSlice == 1 && mod(handles.acqInfo.idxTurnTotal,handles.param.nTurnsPerSlice) == 0
    if handles.param.PAUSDataType == 4
        type = 'int32';
    else
        type = 'int16';            
    end
    fwrite(handles.acqInfo.filePA, handles.resultsTemp.PA, type);
    fwrite(handles.acqInfo.fileUS, US, type);
    fwrite(handles.acqInfo.fileFluo, handles.resultsTemp.fluo, 'int32');
end   

%If the USB read function asks to skip a display, don't display
if (handles.displayInfo.skipDisplay == 1)
    handles.displayInfo.skipDisplay = 0;
    guidata(hObject, handles); % Update handles at the end of a function 
    return
%If there is only one turn per slice, we display it no matter what is the
%display stype
elseif handles.param.nTurnsPerSlice == 1
    %Do nothing
%If the user wants to display only one after acquiring all the turns, don't display if it is not the past turn
elseif strcmp(handles.displayInfo.displayMultipleTurns,'Once') && mod(handles.acqInfo.idxTurnTotal,handles.param.nTurnsPerSlice) ~= 0
    handles.displayInfo.skipDisplay = 0;
    guidata(hObject, handles); % Update handles at the end of a function 
    return
%Display if it is the last turn at "Once".
elseif strcmp(handles.displayInfo.displayMultipleTurns,'Once')
    PAF = handles.resultsTemp.PAF;
    %USF = handles.resultsTemp.USF;
    fluoF = handles.resultsTemp.fluoF;        
%If the user wants to display all the turns, display them (before summing)
elseif strcmp(handles.displayInfo.displayMultipleTurns,'All')
    %Nothing to do
%If the user wants to display the sum of the turns, display them
elseif strcmp(handles.displayInfo.displayMultipleTurns,'Sum')
    PAF = handles.resultsTemp.PAF;
    %USF = handles.resultsTemp.USF;
    fluoF = handles.resultsTemp.fluoF;         
end

%     USF(:,mod((1:20)+(64*handles.acqInfo.idxTurnTotal),256))=0;    
%We calculate the 2D image.
[PA2D,US2D,fluo2D] = Analysis2D(handles.param, handles.displayInfo, PAF, USF, fluoF);
Display(handles, hObject, PAF, USF, fluoF, PA2D, US2D, fluo2D);
handles = guidata(hObject); % Retrieve the updated handle after calling a function   

end
// The commands available in ThreadUSB are:
// ThreadUSB('Init') 					%Initialization of the USB connexion
// ThreadUSB('Write',int32(send_data))  %Sending the informations to the FPGA (send_data is a vector)
// data=ThreadUSB('Single_Read'); 		%Read a set of data from the FPGA. (Use only the flush the data after a "Init" or before a "Close").
// ThreadUSB('Start');                  %Start the dedicated thread that will continuously read the incoming data from the USB connexion.
// data = ThreadUSB('Read');			%Read a set of data from the FPGA. Dedicated thread must have be started. Much faster than Single_Read.
// ThreadUSB('Stop');					%Stop the dedicated thread.
// ThreadUSB('Close');					%Close the USB connexion.

#include "mex.h"
#include "libusb.h"
#include <windows.h>
#include <stdio.h>
#include <process.h>
#include <string.h>
#include "matrix.h"

#define CALL_CHECK(fcall) do { r=fcall; if (r < 0) ERR_EXIT(r); } while (0);
#define ERR_EXIT(errcode) do { printf("   %s\n", libusb_strerror((enum libusb_error)errcode)); return -1; } while (0)

/* Definition des enpoints et donnees pour le transfer */
#define EP6         0x06      // endpoint for writing
#define EP8         0x88      // endpoint for reading
#define TIMEOUT     1000   // in ms
#define TIMEOUT_SINGLE 10   // in ms, faster because Single_Read is used only to flush the data
#define VENDOR_TYPE 0x40   // USB vendor request, RequestType field
#define VEND_SPEED  0x91   // vendor request to change the USB speed
#define VEND_JTAG   0x92   // vendor request to change source of JTAG
#define VEND_SYNC   0x93   // vendor request to change between synchronous/asynchrous FIFO
#define VEND_ZLP   0x95   // vendor request to change zero-length buffer allowance
#define JTAG_FX2    0      // index of the Vendor Req for high speed
#define JTAG_EXT    1      // index of the Vendor Req for full speed
#define HIGH_SPEED  0      // index of the Vendor Req for high speed
#define FULL_SPEED  1      // index of the Vendor Req for full speed
#define SHARED_BUFFER_SIZE 1000000000     // size of the accumulation buffer
#define READ_BUFFER_SIZE   10000000     // size of the USB buffer that we receive
#define ALTERA_VID  0x09FB // USB vendor ID of Altera USBBlaster
#define ALTERA_PID  0x6001 // USB product ID of Altera USBBlaster
#define USB_TYPE_VENDOR			(0x02 << 5)
#define USB_ENDPOINT_IN			0x80

/*These global variables will be accessible to both threads and will exist
 *in the MEX function through multiple calls to the MEX function */
static unsigned Counter;
static HANDLE hThread=NULL;
static HANDLE hThreadMain=NULL;
static HANDLE hMutex=NULL;

static struct libusb_device* device;           // pointer on the USB device
static struct libusb_device_handle* h_device;     // handle on the usb device	  
static char data[SHARED_BUFFER_SIZE];                  // 1GByte of data possible
static int data_index=0;
static int doneDataIndex=0;
static bool started;
static int nb_ifaces = 0;
static struct libusb_config_descriptor *conf_desc;
static libusb_context *ctx;
static const struct libusb_version* version;   
static struct libusb_device_descriptor desc;

/*This thread runs in parallel and reads data. */
 unsigned __stdcall AcquisitionThreadFunc( void* pArguments ) {    
 	  int rv;
      int r;
      char buffer[READ_BUFFER_SIZE];     // buffer for receiving
 
     // For indexing the large data
     //data_index=0;
     
     // Read continuously until stopped
     while ( Counter == 0 ) 
     {
         //A packet could be lost when a lot of data are transfered (with a duration superior to the timeout)
         //In that case, data received will be smaller than BUFFER_SIZE and the driver will lose the last packet, 
         //because it is waiting for another one. While it is waiting, it doesn't commit the previous packet.
         //For this reason, the timeout should be high and BUFFER_SIZE too, to allow varying the length of the transfer.     
         r = libusb_bulk_transfer(h_device, EP8, buffer, READ_BUFFER_SIZE, &rv, TIMEOUT);      
		 if( rv > 0 )
		 { 
			if(WaitForSingleObject(hMutex, 10000)==WAIT_TIMEOUT) {
                printf("USB Interface: Cannot lock mutex..."); 
				mexErrMsgTxt("USB Interface: Cannot lock mutex...");
			}
			if( data_index+rv < SHARED_BUFFER_SIZE )
			{
				// Copy in the larger data place-holder
				memcpy(&data[data_index],buffer,rv);
				// update the pointer
				data_index=data_index+rv;             
			}
			else
			{
                printf("USB Interface: Read overflow...");
				mexErrMsgTxt("USB Interface: Read overflow...");
			}
			ReleaseMutex(hMutex);
		 }
         //Sleep( 0.1L );
     }    
     _endthreadex( 0 );
     return 0;
 }
 
/* La fonction mex controle l'acces au board usb. Elle prend un argument
 *de commande et aussi un argument optionnel qui permet de lire et d'envoyer
 *une commande au board. Tant que nous n'appelons pas close, le fichier mex
 *est "locked" ce qui veut dire que les threads restent actives.
 */
void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray*prhs[]) {
    unsigned threadID, threadID2;
    char* cmd;
    char* datap;
    char* datatosend; 
	int flag, rv, rv1, r;
    mwSize N, M;    
    char buffer[READ_BUFFER_SIZE];     // buffer for receiving in single_read mode   
    unsigned short* shortbuff;
    int* errorReturn;
    int* returnInfo;
    int len;
    int iface; 
    int cfg0, cfg;

    /*  check for proper number of arguments */
    if(nrhs>2)
        mexErrMsgTxt("USB Interface: One or two inputs required.");
    
    /* check to make sure the first input argument is a string */
    if (!mxIsChar(prhs[0]))
        mexErrMsgTxt("USB Interface: First argument must be a string.");
    
    /* get command from first input */
    cmd = mxArrayToString(prhs[0]);

    /* if command is "Init", initialize the usb device */
    if (!strcmp(cmd,"Init")) {  
        //Return fail or success
        plhs[0] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
        returnInfo = (int*) mxGetPr(plhs[0]);     
        returnInfo[0] = 1; //Success by default
        if (mexIsLocked())
        {
            printf("USB Interface Init: USB already initialized.\n");
            returnInfo[0] = 0;
            return;
        }

        version = libusb_get_version();
        //printf("Using libusb v%d.%d.%d.%d\n\n", version->major, version->minor, version->micro, version->nano);
        r = libusb_init(&ctx);
        if (r < 0) {
            printf("Error initializing libusb\n");
            return;
        }

        h_device = libusb_open_device_with_vid_pid(ctx, ALTERA_VID, ALTERA_PID);
        if (h_device == NULL) {
            printf("USB Interface Init: Cannot initialize USB bus.\n");
            returnInfo[0] = 0;
            return;
        }    
        device = libusb_get_device(h_device);
        
        r = libusb_get_device_descriptor(device, &desc);
        if (r < 0) {
            printf("Error getting device descriptor\n");
            return;
        }  
        
        r = libusb_get_config_descriptor(device, 0, &conf_desc);
        if (r < 0) {
            printf("Error getting config descriptor\n");
            return;
        }    
        cfg0 = conf_desc->bConfigurationValue;
        libusb_free_config_descriptor(conf_desc);   
                
        r = libusb_get_configuration(h_device, &cfg);
        if (r < 0) {
            printf("Error getting configuration\n");
            return;
        }        
        
        // set configuration (needed especially for windows)
        // tolerate EBUSY: one device with one configuration, but two interfaces
        //    and libftdi sessions to both interfaces (e.g. FT2232)
        if (desc.bNumConfigurations > 0 && cfg != cfg0) {
            printf("Here\n");
            r = libusb_set_configuration(h_device, cfg0);
            if (r < 0) {
                libusb_close (h_device);
                h_device = NULL;
                printf("Error setting configuration\n");
                return;
            }
        }
        
        nb_ifaces = conf_desc->bNumInterfaces;
        printf("n interfaces %i.\n", nb_ifaces); 
        libusb_set_auto_detach_kernel_driver(h_device, 1);         
        for (iface = 0; iface < nb_ifaces; iface++)
        {
            printf("\nClaiming interface %d...\n", iface);
            //libusb_detach_kernel_driver(h_device, iface);
            r = libusb_claim_interface(h_device, iface);
            if (r < 0) {
                printf("Claim failed %i.\n",r);
                return;
            }
        }        
                
        /* lock thread so that no-one accidentally clears function, 
           from here we always have to unlock mex upon errors */
        mexLock();
        started=false;
        // Open the device
        //h_device = usb_open(device);
        if (h_device == NULL)
        {
            mexUnlock();
            printf("USB Interface Init: Could not open usb device.\n"); 
            returnInfo[0] = 0;
            return;
        }     
        
        //Set the FIFO to synchronous mode to transfer faster
         libusb_control_transfer(h_device,
         USB_TYPE_VENDOR | USB_ENDPOINT_IN, 
         0x93,  // The vendor command
         0,     // value
         0,     // index (0 for synchronous and 1 for asynchronous)
         (char*)data,  // returned data
         2,     // size of data
         TIMEOUT); // timeout in ms   
         printf("Here");
         //Allow zero-length packet to receive data length that are multiples of 512 bytes
         libusb_control_transfer(h_device,
         USB_TYPE_VENDOR | USB_ENDPOINT_IN, 
         VEND_ZLP,  // The vendor command
         0,     // value
         1,     // index (0 for zero-length packet disabled and 1 for zero-length packet enabled)
         (char*)data,  // returned data
         2,     // size of data
         TIMEOUT); // timeout in ms   
         //printf("ZLP: %i",rv1);  
          
         
        /*Check if kenel driver attached*/
        /*
           r = libusb_claim_interface(h_device, 0);
              if (r < 0) {
                  printf("Problem claiming %i\n",r);
                return;
              }
         */
        
    }
    else if (!strcmp(cmd,"Single_Read") )
    {
         /* make sure that the system was initialized*/
        if (nlhs > 1)
        	mexErrMsgTxt("Too many output arguments.");
        if (!mexIsLocked())
        {
            printf("USB Interface: USB device not initialized, call Init first.\n");
            plhs[0] = mxCreateNumericMatrix(1, 0, mxINT32_CLASS, mxREAL);
            memcpy(mxGetPr(plhs[0]), (int*) shortbuff, 0);             
            return;
        }
        //mexPrintf("USB Interface: Reading a single vector of data.\n");               
                
        shortbuff = (unsigned short*) buffer;
        
        r = libusb_bulk_transfer(h_device, EP8, buffer, READ_BUFFER_SIZE, &rv, TIMEOUT_SINGLE);
        if (rv > 0)
        {   
            plhs[0] = mxCreateNumericMatrix(1, rv/4, mxINT32_CLASS, mxREAL);
            memcpy(mxGetPr(plhs[0]), (int*) shortbuff, rv);      
        }
        else
        {
            plhs[0] = mxCreateNumericMatrix(1, 0, mxINT32_CLASS, mxREAL);
            memcpy(mxGetPr(plhs[0]), (int*) shortbuff, 0); 
        }
    }
     else if (!strcmp(cmd,"Start") )
     {
        data_index = 0;
        doneDataIndex = 0;
          /* make sure that the system was initialized*/
        if (!mexIsLocked())
            mexErrMsgTxt("USB Interface Start: USB device not initialized, call Init first."); 
        if (started)
            mexErrMsgTxt("USB Interface: USB device already started.");   
         /* Create second process for acquisition in parallel. */
		 hMutex = CreateMutex(NULL,false,"AcqMutex");
         Counter = 0;
         hThread = (HANDLE)_beginthreadex( NULL, 0, &AcquisitionThreadFunc, NULL, 0, &threadID );
         
         //Upgrade priority of the thread that reads the USB data
         SetThreadPriority(hThread, 15);
         SetThreadPriority(GetCurrentThread(), -15);
         started = true;
     }
     else if (!strcmp(cmd,"Stop") )
     {
          /* make sure that the system was initialized*/
         if (!mexIsLocked())
         {
             printf("USB Interface: USB device not initialized.\n"); 
             return;
         }
          if (!started)
          {
             printf("USB Interface: USB device not started.\n"); 
             return;
          }
         
         /* wait for thread to finish */
         Counter = 1;
         WaitForSingleObject( hThread, INFINITE );
         /* Release and close opened interface */        
         
         started = false;
         /* Destroy the thread object, free memory, and unlock mex. */
         CloseHandle( hThread );
     }
     else if (!strcmp(cmd,"Read") )
     {
        int NdataToRead;
        if(nrhs!=2) //Read all if no quantity requested
            NdataToRead = (data_index-doneDataIndex)/4;
        else
            NdataToRead = *mxGetPr(prhs[1]);  
        //printf("DataIndex: %i", data_index); 
        //printf("DoneDataIndex: %i \n", doneDataIndex); 
        //printf("NdataToRead: %i \n", NdataToRead); 
          /* make sure that the system was initialized*/
         if (!mexIsLocked())
         {
             printf("USB Interface: USB device not initialized, call Init first.\n"); 
             plhs[0] = mxCreateNumericMatrix(1, 0/4, mxINT32_CLASS, mxREAL);
             plhs[1] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
             return;
         }
          /* To read data, the system must have been started */        
         if (!started)
         {
             printf("USB Interface: Reading while acquisition not started, call Start first.\n"); 
             plhs[0] = mxCreateNumericMatrix(1, 0/4, mxINT32_CLASS, mxREAL);
             plhs[1] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
             return;
         }

		if(WaitForSingleObject(hMutex, 10000)==WAIT_TIMEOUT) {
			mexErrMsgTxt("USB Interface: Cannot lock mutex...");
		}
        shortbuff = (unsigned short*) data;
        if (data_index-doneDataIndex >= NdataToRead*4 & NdataToRead > 0) //Check if there is at least one slice to read
        {  
            plhs[0] = mxCreateNumericMatrix(1, NdataToRead, mxINT32_CLASS, mxREAL);
            memcpy(mxGetPr(plhs[0]), (int*) &data[doneDataIndex], NdataToRead*4); //Recover first slice of the buffer
            doneDataIndex=doneDataIndex+NdataToRead*4; //Done one slice
            //memcpy(&data[0], &data[256*4071*4], data_index); //Remove first slice of the buffer
            //memcpy(&data[data_index],buffer,rv);
        }
        else
        { 
            plhs[0] = mxCreateNumericMatrix(1, 0/4, mxINT32_CLASS, mxREAL);
            memcpy(mxGetPr(plhs[0]), (int*) shortbuff, 0);  
            //data_index=data_index;             
        }  
           
        //Also return the number of turns left in the buffer
        plhs[1] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
        returnInfo = (int*) mxGetPr(plhs[1]);
        if (NdataToRead > 0)
            returnInfo[0] = (data_index-doneDataIndex)/4/NdataToRead;
        else
            returnInfo[0] = 0;
       
        //Check if we are getting to the end of the buffer
        if (data_index > (SHARED_BUFFER_SIZE-NdataToRead*4*10)) //Around 10 turns left in the buffer (5%)
        {
            printf("Almost full...\n"); 
            if (data_index > doneDataIndex) //The buffer contains some data, move it to the beginning of the buffer
            {
                int dataLeft = data_index-doneDataIndex;
                memcpy(&data[0], &data[doneDataIndex], dataLeft); //Remove first slice of the buffer
                data_index = dataLeft;
                doneDataIndex = 0;
            }
            else //The buffer is empty, we can start from index 0
            {
                data_index = 0;
                doneDataIndex = 0;
            }
        }
        
        ReleaseMutex(hMutex);

     }
    else if (!strcmp(cmd,"Write") )
    {      
        if(nrhs!=2)
            mexErrMsgTxt("USB Interface Write: Two inputs required to write."); 
         /* make sure that the system was initialized*/
        if (!mexIsLocked())
        {
            printf("USB Interface Write: USB device not initialized, call Init first.\n"); 
            return;
        }
         /* To write data, the system need to be initialized but not necessarily started */  
         /* get data from second input */
        N=mxGetN(prhs[1]); 
        datatosend = mxGetPr(prhs[1]);      
        
        len = 4*N;
		r = libusb_bulk_transfer(h_device, EP6, datatosend, 4*N, &len, 1000); // envoi du message
		if (len != 4*N){
            printf("Data not written correctly %i  %i", len, r); 
            // Stop reading if started
            if(started)
            {
                /* wait for thread to finish */
                Counter = 1;
                WaitForSingleObject( hThread, INFINITE );
                /* Release and close opened interface */        
                started = false;
                /* Destroy the thread object, free memory, and unlock mex. */
                CloseHandle( hThread );
            }
            libusb_release_interface(h_device, iface);
            /*
            if (usb_release_interface(h_device,device->config->interface->altsetting->bInterfaceNumber))
            {
               usb_close(h_device);
               mexUnlock();
               printf("USB Interface Stop: Could not release the interface.\n");
               return;
            } 
            */            
            // close device
            //usb_close(h_device);     
            mexUnlock();
            mexErrMsgTxt("USB Interface Write: Failed to send data. Closing.");            
		}        
    }    
    else if (!strcmp(cmd,"Close") )
    {
         /* make sure that the system was initialized*/
        if (!mexIsLocked())
        {
            printf("USB Interface: USB device not initialized.\n"); 
            return;
        }
        for (iface = 0; iface<nb_ifaces; iface++) {
            printf("Releasing interface %d... total %i\n", iface, nb_ifaces);
            libusb_release_interface(h_device, iface);
        }     
        /*
        if (usb_release_interface(h_device,device->config->interface->altsetting->bInterfaceNumber))
        {
           usb_close(h_device);
           printf("USB Interface Stop: Could not release the interface.\n"); 
           return;
        }
        */
        // close the device
        if(started)
        {
            /* wait for thread to finish */
            Counter = 1;
            WaitForSingleObject( hThread, INFINITE );
            /* Release and close opened interface */        
            started = false;
            /* Destroy the thread object, free memory, and unlock mex. */
            CloseHandle( hThread );
        }
        
        libusb_close(h_device);        
        mexUnlock();
    }
    else 
    {
        mexErrMsgTxt("USB Interface: Wrong command.");
    }

    return;
}
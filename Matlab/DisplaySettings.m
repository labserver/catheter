%
% Updates the structure containing the display information.
%   
% INPUTS:
%
% handles: handles to the GUI, which allows reading of the parameters set
% by the user.
%
% hObject: Link to the GUI. It is used to save the handles at the end of the function
%
% OUTPUTS:
%
% none.
%
% COMMENTS:
%
% Unless otherwise noted, all parameters are set using SI units (meters, seconds, Hz)
% There is no outputs. However, display information of the handles is updated at the end of the function using the command guidata.
%
function DisplaySettings(handles,hObject)

%TODO: Check if values not in the GUI were different in the loaded data. But not very useful

%Set values not based on the GUI
handles.displayInfo.radiusTransducer = 0; %Radius of the transducer from the rotating axe. It is used to add samples in the results to have the correct scale when reconstructing 2D images
handles.displayInfo.PAFilterButterOrder = 4; %Order of the Butterworth filter
handles.displayInfo.USFilterButterOrder = 4; %Order of the Butterworth filter
handles.displayInfo.fluoFilterButterOrder = 4; %Order of the Butterworth filter
handles.displayInfo.angleShiftPA = 0; %Angle shift of PA results
handles.displayInfo.angleShiftUS = 0; %Angle shift of PA results
handles.displayInfo.angleShiftFluo = 0; %Angle shift of PA results

if ~isfield(handles.displayInfo,'enableDownSample'); handles.displayInfo.enableDownSample = 0; end
if handles.displayInfo.enableDownSample ~= handles.acqInfo.acqRunning
    handles.displayInfo.enableDownSample = handles.acqInfo.acqRunning;
    handles.displayInfo.updateAxisInformation = 1;
end
% handles.displayInfo.enableDownSample = 0;

handles.displayInfo.displayGainPA = str2double(get(handles.etDisplayGainPA,'String'));
handles.displayInfo.displayDynamicRangePA = str2double(get(handles.etDisplayDynamicRangePA,'String'));
handles.displayInfo.displayGainUS = str2double(get(handles.etDisplayGainUS,'String'));
handles.displayInfo.displayDynamicRangeUS = str2double(get(handles.etDisplayDynamicRangeUS,'String'));
handles.displayInfo.displayGainFluo = str2double(get(handles.etDisplayGainFluo,'String'));
handles.displayInfo.displayDynamicRangeFluo = str2double(get(handles.etDisplayDynamicRangeFluo,'String'));
%Get display stype
listDisplayMultipleTurns = get(handles.pmDisplayMultipleTurns,'String'); handles.displayInfo.displayMultipleTurns = listDisplayMultipleTurns(get(handles.pmDisplayMultipleTurns,'Value'));    

if ~isfield(handles.displayInfo,'updateAxisInformation'); handles.displayInfo.updateAxisInformation = 1; end

%Check if we need to update filter informations (if settings changed or if it
%is the first display)
if ~isfield(handles.displayInfo,'PALowPassF'); handles.displayInfo.PALowPassF = 0; handles.displayInfo.PAHighPassF = 0; handles.displayInfo.USLowPassF = 0; handles.displayInfo.USHighPassF = 0; end %Make sure old data exists
newPALowPassF = str2double(get(handles.etPALPF,'String')); %Load new data
newPAHighPassF = str2double(get(handles.etPAHPF,'String')); %Load new data
newUSLowPassF = str2double(get(handles.etUSLPF,'String')); %Load new data
newUSHighPassF = str2double(get(handles.etUSHPF,'String')); %Load new data
if handles.displayInfo.updateAxisInformation == 1 || ~isfield(handles.displayInfo,'filterButter') ...
        || newPALowPassF ~= handles.displayInfo.PALowPassF || newPAHighPassF ~= handles.displayInfo.PAHighPassF ...
        || newUSLowPassF ~= handles.displayInfo.USLowPassF || newUSHighPassF ~= handles.displayInfo.USHighPassF || handles.acqInfo.idxTurnTotal == 1 %Check change or if info exists or if start of the acquisition
    updateFilterInfo = 1;    
else
    updateFilterInfo = 0;
end

%Check if we need to update 2D informations (if settings changed or if it
%is the first display)
if ~isfield(handles.displayInfo,'nPixels'); handles.displayInfo.nPixels = 1; end %Make sure old data exists
if ~isfield(handles.displayInfo,'depthToDisplay'); handles.displayInfo.depthToDisplay = 1; end %Make sure old data exists
newNPixels = str2double(get(handles.etNPixels,'String')); %Load new data
newDepthToDisplay = str2double(get(handles.etDepthToDisplay,'String')); %Load new data
if handles.displayInfo.updateAxisInformation == 1 || newNPixels ~= handles.displayInfo.nPixels || newDepthToDisplay ~= handles.displayInfo.depthToDisplay ...
        || ~isfield(handles.displayInfo,'rho') || handles.displayInfo.radiusCatheterFlush ~= str2double(get(handles.etHideRadius,'String')) || handles.acqInfo.idxTurnTotal == 1 %Check change or if info exists or if start of the acquisition
    update2DInfo = 1; %Updated at the end of the function
else
    update2DInfo = 0;
end

%Display parameters
    handles.displayInfo.PALowPassF = newPALowPassF; %Cutoff frequency of the low-pass filter in Hz
        if (isreal(handles.displayInfo.PALowPassF) == 0)
            handles.displayInfo.PALowPassF=0;
            set(handles.etPALPF,'String',num2str(handles.displayInfo.PALowPassF));
            warndlg(['The photoacoustic low-pass filter cutoff frequency was invalid. It has been automatically corrected to ' num2str(handles.displayInfo.PALowPassF) '.'],'Problems with handles.displayInfo')            
        end      
    handles.displayInfo.PAHighPassF = newPAHighPassF; %Cutoff frequency of the high-pass filter in Hz
        if (isreal(handles.displayInfo.PAHighPassF) == 0)
            handles.displayInfo.PAHighPassF=0;
            set(handles.etPAHPF,'String',num2str(handles.displayInfo.PAHighPassF));
            warndlg(['The photoacoustic high-pass filter cutoff frequency was invalid. It has been automatically corrected to ' num2str(handles.displayInfo.PAHighPassF) '.'],'Problems with handles.displayInfo')            
        end           
    handles.displayInfo.USLowPassF = newUSLowPassF; %Cutoff frequency of the low-pass filter in Hz
        if (isreal(handles.displayInfo.USLowPassF) == 0)
            handles.displayInfo.USLowPassF=0;
            set(handles.etUSLPF,'String',num2str(handles.displayInfo.USLowPassF));
            warndlg(['The photoacoustic low-pass filter cutoff frequency was invalid. It has been automatically corrected to ' num2str(handles.displayInfo.USLowPassF) '.'],'Problems with handles.displayInfo')            
        end      
    handles.displayInfo.USHighPassF = newUSHighPassF; %Cutoff frequency of the high-pass filter in Hz
        if (isreal(handles.displayInfo.USHighPassF) == 0)
            handles.displayInfo.USHighPassF=0;
            set(handles.etUSHPF,'String',num2str(handles.displayInfo.USHighPassF));
            warndlg(['The photoacoustic high-pass filter cutoff frequency was invalid. It has been automatically corrected to ' num2str(handles.displayInfo.USHighPassF) '.'],'Problems with handles.displayInfo')            
        end           
    handles.displayInfo.fluoLowPassF = str2double(get(handles.etFluoLPF,'String')); %Cutoff frequency of the low-pass filter in Hz
        if (isreal(handles.displayInfo.fluoLowPassF) == 0)
            handles.displayInfo.fluoLowPassF=0;
            set(handles.etFluoLPF,'String',num2str(handles.displayInfo.fluoLowPassF));
            warndlg(['The fluorescence low-pass filter cutoff frequency was invalid. It has been automatically corrected to ' num2str(handles.displayInfo.fluoLowPassF) '.'],'Problems with handles.displayInfo')            
        end      
    handles.displayInfo.fluoHighPassF = str2double(get(handles.etFluoHPF,'String')); %Cutoff frequency of the high-pass filter in Hz
        if (isreal(handles.displayInfo.fluoHighPassF) == 0)
            handles.displayInfo.fluoHighPassF=0;
            set(handles.etFluoHPF,'String',num2str(handles.displayInfo.fluoHighPassF));
            warndlg(['The fluorescence high-pass filter cutoff frequency was invalid. It has been automatically corrected to ' num2str(handles.displayInfo.fluoHighPassF) '.'],'Problems with handles.displayInfo')            
        end           
    handles.displayInfo.PAHilbert = get(handles.cbPAHilbert,'Value'); %Apply Hilbert transform to PA data
    handles.displayInfo.USHilbert = get(handles.cbUSHilbert,'Value'); %Apply Hilbert transform to US data
    
    if (isreal(handles.displayInfo.radiusTransducer) == 0 || handles.displayInfo.radiusTransducer<0 || handles.displayInfo.radiusTransducer>0.01 || mod(real(handles.displayInfo.radiusTransducer),0.0001) ~= 0)
        handles.displayInfo.radiusTransducer=double(real(uint32(handles.displayInfo.radiusTransducer*10000)))/10000;
        handles.displayInfo.radiusTransducer(handles.displayInfo.radiusTransducer<0)=0;
        handles.displayInfo.radiusTransducer(handles.displayInfo.radiusTransducer>0.01)=0.01;
        warndlg(['The radius of the transducer was invalid. It has been automatically corrected to ' num2str(handles.displayInfo.radiusTransducer) '.'],'Problems with handles.displayInfo')            
    end
        handles.displayInfo.radiusTransducerNSamples = double(uint32(handles.displayInfo.radiusTransducer/handles.param.speedUltrasound*handles.param.transducerAcquisitionFreq)); %Conversion to a number of samples    

    %Get depthToDisplay
    %Update axis information if depthToDisplay changed
    if isfield(handles.displayInfo,'depthToDisplay'); if handles.displayInfo.depthToDisplay ~= str2double(get(handles.etDepthToDisplay,'String')) handles.displayInfo.updateAxisInformation = 1; end; end
    handles.displayInfo.depthToDisplay = newDepthToDisplay; %Depth to display in meters
    if handles.displayInfo.depthToDisplay <= 0 || handles.displayInfo.depthToDisplay > handles.param.depthUltrasound+handles.displayInfo.radiusTransducer
        handles.displayInfo.depthToDisplay(handles.displayInfo.depthToDisplay <= 0) = 0.001;
        handles.displayInfo.depthToDisplay(handles.displayInfo.depthToDisplay > handles.param.depthUltrasound+handles.displayInfo.radiusTransducer) = handles.param.depthUltrasound+handles.displayInfo.radiusTransducer;
        set(handles.etDepthToDisplay,'String',num2str(handles.displayInfo.depthToDisplay));
%             warndlg(['The depth to display was invalid. It has been automatically corrected to ' num2str(handles.displayInfo.depthToDisplay) '.'],'Problems with handles.displayInfo')            
        update2DInfo = 1; 
    end  
    handles.displayInfo.nSamplesToDisplay = round(handles.displayInfo.depthToDisplay/handles.param.speedUltrasound*handles.param.transducerAcquisitionFreq);
    %Make sure number of samples is an even number (To be compatible with 16 bits transfers)
    handles.displayInfo.nSamplesToDisplay = handles.displayInfo.nSamplesToDisplay-mod(handles.displayInfo.nSamplesToDisplay,2); 
    
   handles.displayInfo.nPixels = newNPixels; %Number of pixels in each direction for the 2D display
        if (isreal(handles.displayInfo.nPixels) == 0 || handles.displayInfo.nPixels<1 || handles.displayInfo.nPixels>2*handles.displayInfo.nSamplesToDisplay || mod(handles.displayInfo.nPixels,1) ~= 0)
            handles.displayInfo.nPixels=double(real(uint32(handles.displayInfo.nPixels-1)))+1;
            handles.displayInfo.nPixels(handles.displayInfo.nPixels>2*handles.displayInfo.nSamplesToDisplay)=2*handles.displayInfo.nSamplesToDisplay;
            set(handles.etNPixels,'String',num2str(handles.displayInfo.nPixels));
            warndlg(['The number of pixels was invalid. It has been automatically corrected to ' num2str(handles.displayInfo.nPixels) '.'],'Problems with handles.displayInfo')            
        end   
        
    handles.displayInfo.radiusCatheterFlush = str2double(get(handles.etHideRadius,'String')); %Radius of the catheter or radius to erase data. It is used to hide the ultrasound initial pulse and it allows the user to see better where is the catheter
    if (isreal(handles.displayInfo.radiusCatheterFlush) == 0 || handles.displayInfo.radiusCatheterFlush<0 || handles.displayInfo.radiusCatheterFlush>handles.displayInfo.depthToDisplay || mod(real(handles.displayInfo.radiusCatheterFlush),0.0001) ~= 0)
        handles.displayInfo.radiusCatheterFlush=double(real(uint32(handles.displayInfo.radiusCatheterFlush*10000)))/10000;
        handles.displayInfo.radiusCatheterFlush(handles.displayInfo.radiusCatheterFlush<0)=0;
        handles.displayInfo.radiusCatheterFlush(handles.displayInfo.radiusCatheterFlush>handles.displayInfo.depthToDisplay)=floor(handles.displayInfo.depthToDisplay*10000)/10000;
        set(handles.etHideRadius,'String',num2str(handles.displayInfo.radiusCatheterFlush));
        warndlg(['The radius to hide was invalid. It has been automatically corrected to ' num2str(handles.displayInfo.radiusCatheterFlush) '.'],'Problems with handles.displayInfo')            
    end        
        handles.displayInfo.radiusCatheterFlushNSamples = double(uint32(handles.displayInfo.radiusCatheterFlush/handles.param.speedUltrasound*handles.param.transducerAcquisitionFreq)); %Conversion to a number of samples

    %Downsample
    if handles.displayInfo.enableDownSample == 1
        handles.displayInfo.downSampleValue = round(2*handles.param.nSamples/handles.displayInfo.nPixels);
        handles.displayInfo.downSampleValue(handles.displayInfo.downSampleValue<1)=1;
        handles.displayInfo.nSamplesToDisplay = floor(handles.displayInfo.nSamplesToDisplay/handles.displayInfo.downSampleValue);
        handles.displayInfo.radiusCatheterFlushNSamples = floor(handles.displayInfo.radiusCatheterFlushNSamples/handles.displayInfo.downSampleValue);
        handles.displayInfo.radiusTransducerNSamples = floor(handles.displayInfo.radiusTransducerNSamples/handles.displayInfo.downSampleValue);
    else
        handles.displayInfo.downSampleValue = 1;
    end
    
    %Filter wrap (On/Off)
    handles.displayInfo.filterWrapFluo = 1; %get(handles.cbFilterWrapFluo,'Value');   

    %Check which display type the user wants (Log/Normal)
    if ~isfield(handles.displayInfo,'displayTypePA');
        handles.displayInfo.displayTypePA = ' ';
        handles.displayInfo.displayTypeUS = ' ';
        handles.displayInfo.displayTypeFluo = ' ';    
    end 
    listDisplayType = get(handles.pmDisplayTypePA,'String'); newDisplayTypePA = listDisplayType(get(handles.pmDisplayTypePA,'Value'));    
    listDisplayType = get(handles.pmDisplayTypeUS,'String'); newDisplayTypeUS = listDisplayType(get(handles.pmDisplayTypeUS,'Value'));    
    listDisplayType = get(handles.pmDisplayTypeFluo,'String'); newDisplayTypeFluo = listDisplayType(get(handles.pmDisplayTypeFluo,'Value'));    
    if (~strcmp(handles.displayInfo.displayTypePA,newDisplayTypePA) || ~strcmp(handles.displayInfo.displayTypeUS,newDisplayTypeUS) || ~strcmp(handles.displayInfo.displayTypeFluo,newDisplayTypeFluo))
        %Uldate axis if display type changed (log/linear) because it
        %changes the scale of the display
        handles.displayInfo.updateAxisInformation = 1;
    end
    handles.displayInfo.displayTypePA = newDisplayTypePA;
    handles.displayInfo.displayTypeUS = newDisplayTypeUS;
    handles.displayInfo.displayTypeFluo = newDisplayTypeFluo;
    
    %Check if we need to update axis information (title, labels)
    if isfield(handles.displayInfo,'PADisplaySignal')
        if handles.displayInfo.PADisplaySignal ~= get(handles.rbPARF,'Value') || handles.displayInfo.USDisplaySignal ~= get(handles.rbUSRF,'Value') || ...
                handles.displayInfo.fluoDisplaySignal ~= get(handles.rbFluoSignal,'Value') || handles.displayInfo.fluoDisplayFull2D ~= get(handles.rbFluoFull2D,'Value') || ...
                handles.displayInfo.USDisplayLineTime ~= get(handles.rbUSLineTime,'Value') || ...
                handles.displayInfo.PADisplay2DCart ~= get(handles.rbPA2DCart,'Value') || handles.displayInfo.USDisplay2DCart ~= get(handles.rbUS2DCart,'Value')
            handles.displayInfo.updateAxisInformation = 1;
        end
    else %First display
        handles.displayInfo.updateAxisInformation = 1;
    end
    
    %Check which signal type to display (Signal/2D)
    handles.displayInfo.PADisplaySignal = get(handles.rbPARF,'Value');
    handles.displayInfo.PADisplay2DCart = get(handles.rbPA2DCart,'Value');  
    handles.displayInfo.USDisplaySignal = get(handles.rbUSRF,'Value');
    handles.displayInfo.USDisplayLineTime = get(handles.rbUSLineTime,'Value');  
    handles.displayInfo.USDisplay2DCart = get(handles.rbUS2DCart,'Value');  
    handles.displayInfo.fluoDisplaySignal = get(handles.rbFluoSignal,'Value');  
    handles.displayInfo.fluoDisplayFull2D = get(handles.rbFluoFull2D,'Value');  
    
    %Check which pullback position the user wants to display
    handles.displayInfo.PAPullback = get(handles.pmPAPullback,'Value');
    handles.displayInfo.USPullback = get(handles.pmUSPullback,'Value');
    handles.displayInfo.fluoPullback = get(handles.pmFluoPullback,'Value');
    
    %Display fluo of 1 mm before so it fits with the IVUS image
    diff = 4;
    handles.displayInfo.fluoPullback  = handles.displayInfo.fluoPullback - diff;
    handles.displayInfo.fluoPullback(handles.displayInfo.fluoPullback<0) = 0;
    
    %Check which RF signal angular position the user wants to display (in PA and US)
    handles.displayInfo.PAAngle = get(handles.pmPAAngle,'Value');
    handles.displayInfo.USAngle = get(handles.pmUSAngle,'Value');
    %SD pair the user wants to display
    handles.displayInfo.SDPair = get(handles.pmFluoSDPair,'Value');
    %Check which information the user wants to display
    listInfo = get(handles.pmInfo,'String'); handles.displayInfo.infoToDisplay = listInfo(get(handles.pmInfo,'Value'));

    %Check if the user wants to suppress a display (faster display)
    newInfoDisplayOn = get(handles.cbInfoDisplayOn,'Value');
    newPADisplayOn = get(handles.cbPADisplayOn,'Value');
    newFluoDisplayOn = get(handles.cbFluoDisplayOn,'Value');
    if ~isfield(handles.displayInfo,'infoDisplayOn'); handles.displayInfo.infoDisplayOn = -1; handles.displayInfo.PADisplayOn = -1; handles.displayInfo.fluoDisplayOn = -1; end %Make sure old data exists
    if handles.displayInfo.infoDisplayOn ~= newInfoDisplayOn || handles.displayInfo.PADisplayOn ~= newPADisplayOn || handles.displayInfo.fluoDisplayOn ~= newFluoDisplayOn
        handles.displayInfo.updateAxisInformation = 1;
    end
    handles.displayInfo.infoDisplayOn = newInfoDisplayOn;
    handles.displayInfo.PADisplayOn = newPADisplayOn;
    handles.displayInfo.fluoDisplayOn = newFluoDisplayOn;
    
    %Check options for US display
    handles.displayInfo.USFluo = get(handles.cbUSFluo,'Value');
    
    %Dock settings
        if ~isfield(handles.displayInfo,'PAUndock'); handles.displayInfo.PAUndock = -1; end
        PAUndock = get(handles.cbPAUndock,'Value');
        if PAUndock ~= handles.displayInfo.PAUndock
            %Undock changed, delete handle to force a new one
            if isfield(handles,'PAImage'); handles = rmfield(handles,'PAImage'); end
        end
        %Check if the user asked to close the external figure (x button or unchecked PA Undock
        if get(handles.cbPAUndock,'UserData') >= 1 || (PAUndock == 0 && PAUndock ~= handles.displayInfo.PAUndock)
            set(handles.cbPAUndock,'UserData',0);
            if isfield(handles,'PAImage'); handles = rmfield(handles,'PAImage'); end
            delete(findobj('type','figure','name','PA'));
            set(handles.cbPAUndock,'Value',0);
            PAUndock = 0;
        end
        handles.displayInfo.PAUndock = PAUndock;
        if ~isfield(handles.displayInfo,'USUndock'); handles.displayInfo.USUndock = -1; end
        USUndock = get(handles.cbUSUndock,'Value');
        if USUndock ~= handles.displayInfo.USUndock
            %Undock changed, delete handle to force a new one
            if isfield(handles,'USImage'); handles = rmfield(handles,'USImage'); end
        end
        %Check if the user asked to close the external figure (x button or unchecked US Undock
        if get(handles.cbUSUndock,'UserData') >= 1 || (USUndock == 0 && USUndock ~= handles.displayInfo.USUndock)
            set(handles.cbUSUndock,'UserData',0);
            if isfield(handles,'USImage'); handles = rmfield(handles,'USImage'); end
            delete(findobj('type','figure','name','US'));
            set(handles.cbUSUndock,'Value',0);
            USUndock = 0;
        end
        handles.displayInfo.USUndock = USUndock;
        if ~isfield(handles.displayInfo,'fluoUndock'); handles.displayInfo.fluoUndock = -1; end
        fluoUndock = get(handles.cbFluoUndock,'Value');
        if fluoUndock ~= handles.displayInfo.fluoUndock
            %Undock changed, delete handle to force a new one
            if isfield(handles,'fluoImage'); handles = rmfield(handles,'fluoImage'); end
        end
        %Check if the user asked to close the external figure (x button or unchecked Fluo Undock
        if get(handles.cbFluoUndock,'UserData') >= 1 || (fluoUndock == 0 && fluoUndock ~= handles.displayInfo.fluoUndock)
            set(handles.cbFluoUndock,'UserData',0);
            if isfield(handles,'fluoImage'); handles = rmfield(handles,'fluoImage'); end
            delete(findobj('type','figure','name','Fluo'));
            set(handles.cbFluoUndock,'Value',0);
            fluoUndock = 0;
        end
        handles.displayInfo.fluoUndock = fluoUndock;
        
    if ~(isfield(handles.displayInfo,'skipDisplay'))
        handles.displayInfo.skipDisplay = 0;
    end
    
    %Remove fluo baseline
    handles.displayInfo.fluoRemoveBaseline = get(handles.cbRemoveFluoBase,'Value');  
    handles.displayInfo.fluoRemoveBaselineStatus = get(handles.cbRemoveFluoBase,'UserData');
   
%Update filter informations if something changed
if updateFilterInfo == 1
    if newPALowPassF > 0
        [handles.displayInfo.filterButter.numLPA, handles.displayInfo.filterButter.denomLPA] = butter(handles.displayInfo.PAFilterButterOrder, newPALowPassF/(handles.param.transducerAcquisitionFreq/2),'low');
    else
        handles.displayInfo.filterButter.numLPA = 0; handles.displayInfo.filterButter.denomLPA = 0;
    end 
    if newPAHighPassF > 0
        [handles.displayInfo.filterButter.numHPA, handles.displayInfo.filterButter.denomHPA] = butter(handles.displayInfo.PAFilterButterOrder, newPAHighPassF/(handles.param.transducerAcquisitionFreq/2),'high');
    else
        handles.displayInfo.filterButter.numHPA = 0; handles.displayInfo.filterButter.denomHPA = 0;        
    end    
    if newUSLowPassF > 0
        [handles.displayInfo.filterButter.numLUS, handles.displayInfo.filterButter.denomLUS] = butter(handles.displayInfo.USFilterButterOrder, newUSLowPassF/(handles.param.transducerAcquisitionFreq/2),'low');
    else
        handles.displayInfo.filterButter.numLUS = 0; handles.displayInfo.filterButter.denomLUS = 0;
    end 
    if newUSHighPassF > 0
        [handles.displayInfo.filterButter.numHUS, handles.displayInfo.filterButter.denomHUS] = butter(handles.displayInfo.USFilterButterOrder, newUSHighPassF/(handles.param.transducerAcquisitionFreq/2),'high');
    else
        handles.displayInfo.filterButter.numHUS = 0; handles.displayInfo.filterButter.denomHUS = 0;        
    end    
end   

%Update 2D informations if something changed
if update2DInfo == 1    
    nAngles = handles.param.nPositionsPerTurn;
    nRadiusPA = handles.displayInfo.nSamplesToDisplay;
    nRadiusUS = 2*handles.displayInfo.nSamplesToDisplay;


    %Le premier angle doit toujours �tre 0 et le dernier l�g�rement inf�rieur � 2pi
    angle=[0 2*pi-(2*pi/nAngles')];
    radius=[0 handles.displayInfo.depthToDisplay];

    xyBounds = linspace(-handles.displayInfo.depthToDisplay,handles.displayInfo.depthToDisplay,handles.displayInfo.nPixels);
    [x2 y2] = meshgrid(xyBounds, xyBounds);

    rho = sqrt(x2.^2 + y2.^2);  % I want values at this distance
    theta = atan2(y2,x2);       % I want values at this angle

    %atan retourne des angles entre -pi et pi et on veut des angles entre 0 et 2pi
    theta_change = find(theta < angle(1));
    theta(theta_change) = theta(theta_change) + 2*pi;

    %Les points du plan cart�sien final qui ne sont pas dans le rayon d'analyse
    %sont mis � la valeur NaN (ou valeur max pour rapidit� de l'affichage sans spline)
    outbounds = find(rho > radius(2));
    if ~isempty(outbounds)
        rho(outbounds) = 1;%radius(2);
    end
    outbounds = find(rho <= handles.displayInfo.radiusCatheterFlush);
    if ~isempty(outbounds)
        rho(outbounds) = 1;%radius(2);
    end

    %Les points du plan cart�sien qui ont un angle theta sup�rieur au dernier
    %angle acquis sont modifi�s afin de permettre l'interpolation
    outbounds = find(theta >= angle(2));
    if ~isempty(outbounds)
        theta(outbounds) = 2*pi-theta(outbounds);
    end  
    
    %Interpolation de env(Polaire) sur carto(Cart�sien)
    handles.displayInfo.lin1PA = single(linspace(radius(1), radius(2), nRadiusPA)); %returns the full vector of axis coordinates
    handles.displayInfo.lin1US = single(linspace(radius(1), radius(2), nRadiusUS)); %returns the full vector of axis coordinates
    handles.displayInfo.col1 = single(linspace(angle(1), angle(2), nAngles));    
    handles.displayInfo.rho = single(rho);
    handles.displayInfo.theta = single(theta);
    
    %Create mask for USFluo on the same image
    handles.displayInfo.maskUSFluo = false(handles.displayInfo.nPixels,handles.displayInfo.nPixels,3);  
    cx=handles.displayInfo.nPixels/2; cy=handles.displayInfo.nPixels/2; %Define center
    ix=handles.displayInfo.nPixels;iy=handles.displayInfo.nPixels; %Define size
    rOut=handles.displayInfo.nPixels/2; %Define mask radius
    rIn=0.95*handles.displayInfo.nPixels/2; %Define mask radius

    [x,y]=meshgrid(-(cx-1):(ix-cx),-(cy-1):(iy-cy));
    c_mask = ((x.^2+y.^2) <= rOut^2 & (x.^2+y.^2) > rIn^2);
    handles.displayInfo.maskUSFluo(:,:,1) = c_mask; handles.displayInfo.maskUSFluo(:,:,2) = c_mask; handles.displayInfo.maskUSFluo(:,:,3) = c_mask;
        
end   
guidata(hObject, handles); % Update handles at the end of a function 

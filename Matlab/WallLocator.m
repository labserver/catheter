% This script is used to identify the location of the artery wall in the IVUS images.
% Since handles is required, it is recommended to execute this script after
% a break point of the refresh button.
% IVUS image must be undocked in Figure 31.
% The code will loop on all IVUS images and ask the user to select 3 points
% were the artery wall is too close to the catheter (first boundary, inside
% the region and second boundary). 
% It is important that the second point clicked is between the first and
% the third ones.
% If the whole image is to be rejected, click 3 times inside the
% catheter.
% If the whole image is good, click 3 times outside the IVUS image.
% In the display function, add a circle at the radius to identify (usually
% 0.3mm + 0.7mm)
% function WallLocator(handles, hObject)

%Parameters
WL.skip = 20; %Will take 1 slice out of 20.
outputFile = handles.acqInfo.outputFullFileName;

idx = 0;
for idxSlice = 1:WL.skip:handles.param.nSlices 
    idx = idx + 1
    set(handles.pmUSPullback,'Value',idxSlice); %Change slice of US display
    ProcessData(handles, hObject) %Refresh display
    handles = guidata(hObject); % Retrieve the updated handle after calling a function

    set(0, 'currentfigure', handles.parentFigUS);  %Set current figure to IVUS image.
%     click(idx,:,:) = ginput(3);
    
    fcn = makeConstrainToRectFcn('impoly', get(gca, 'XLim'), get(gca, 'YLim'));
    h = impoly(gca);
    wallMask(idx,:,:) = createMask(h, handles.USImage);
    pause(0.1)
    h.delete;
end

%Then, we calculate the angle and the radius of the wall
radiusTotal=4;
nAngles=256;
nRadius=250;

angle=[0 2*pi-(2*pi/nAngles')];
radius=[0 radiusTotal];
xdim = size(wallMask,3);
ydim = size(wallMask,2);
x0 = xdim/2;
y0 = ydim/2;
%Coordinates of Polar image
R = repmat([1:xdim], xdim, 1);
Th = repmat([1:xdim]', 1, ydim);
%Coordinates we will need to interpolate at, with axis at the center
Xi = R.*cos((2*pi*Th)/ydim) + x0;
Yi = -R.*sin((2*pi*Th)/ydim) + y0;

for idxWall = 1:size(wallMask,1)           

    wallOut=interp2(squeeze(double(wallMask(idxWall,:,:))),Xi,Yi);
    
    for idxZ = 1:size(wallOut,1)
        val = find(wallOut(idxZ,:)>0,1,'last');
        if ~isempty(val)
            wallCart(idxZ) = find(wallOut(idxZ,:)>0,1,'last');
        else
            wallCart(idxZ) = 0;
        end
    end
    wallCart = wallCart/(size(wallMask,2)/2)*radiusTotal;
    
    %Get distance for 256 angles
    wallCartNew(idxWall,:) = interp1(1:size(wallCart,2),wallCart,linspace(1,size(wallCart,2),nAngles),'spline');
    
    wall(idxWall,:) = wallCartNew(idxWall,end:-1:1);
end

%Interpolate for all the skipped slices
nSlicesTotal = floor(floor(handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn)/handles.param.nTurnsPerSlice);
nSlicesWall = size(wall,1);
for idxAngle = 1:nAngles
    wallFull(:,idxAngle)=interp1(linspace(1,nSlicesTotal,nSlicesWall),wall(:,idxAngle),1:nSlicesTotal);
end
wall = wallFull;
save([outputFile '_Wall.mat'],'WL','wall') %Save wall distance in the same directory
%
% Stops the acquisition and the second thread. The USB connexion is closed and results are saved.
%   
% INPUTS:
%
% handles: Handles to the GUI.
%
% hObject: Link to the GUI. It is used to save the handles at the end of
% the function.
%
% OUTPUTS:
%
% none.
%
% COMMENTS:
%
function End(handles, hObject)

%------------                        ------------%
%------------ End of the acquisition ------------%
%------------                        ------------%      
if nargin == 0
    % Retrieve handle from GUI
    hObject = findall(0,'Name','PAUSFluo');
    handles = guidata(hObject);
    % Stop acquisition
    set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), 'Stopping acquisition...')) 
    set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))
    set(handles.pbStop, 'Enable', 'off') 
    handles.acqInfo.acqInterrupted = 1;    
    profile off
end
results = getappdata(handles.figure1,'results');
trash0=ThreadUSB('Read'); dummyLength = length(trash0);

%Sending the command to the system to stop the acquisition
clear send_data;
send_data(1) = 4;		
%Send data
ThreadUSB('Write',int32(send_data))

pause(0.25)
trash0000=ThreadUSB('Read'); length(trash0000);
%Closing the USB connexion
ThreadUSB('Stop') %Test
pause(0.25)
trash1=ThreadUSB('Single_Read'); length(trash1);
ThreadUSB('Close')

handles.acqInfo.userComments = get(handles.etComments,'String');
handles.acqInfo.outputFileName = get(handles.etFileName,'String');
scanDate=datestr(now,'yyyy.mm.dd_HH.MM.SS');
if handles.acqInfo.acqInterrupted == 0
    handles.acqInfo.outputFullFileName = [handles.acqInfo.currentPath '\Results\' handles.acqInfo.outputFileName '_' scanDate];
else
    handles.acqInfo.outputFullFileName = [handles.acqInfo.currentPath '\Results\' handles.acqInfo.outputFileName '_' scanDate '_INTERRUPTED'];
end

results.durationTotalAcquisition = toc(handles.durationTotalAcquisition);

%Before saving the results, we have to reconstruct the 3D matrix from all the slices (handles is also updated)
if handles.acqInfo.acqInterrupted == 0
    nTurns = handles.param.nTurns;
    nSlices = handles.param.nSlices;
else
    nTurns = floor(handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn); %If acq was interrupted, we save only the valid slices
    nSlices = floor(floor(handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn)/handles.param.nTurnsPerSlice);
    handles.param.nSlices = nSlices;
    handles.param.nTurns = nTurns;
end
%Make sure we change the size of the cells if interrupted
if handles.acqInfo.acqInterrupted == 1
    results.ADC8CH = results.ADC8CH(1:nTurns);
    results.scanPos = results.scanPos(1:nTurns);
    results.scanPosIsPAPulse = results.scanPosIsPAPulse(1:nTurns);
    results.error = results.error(1:nTurns);
    results.durationAcquisition = results.durationAcquisition(1:nTurns);
    results.durationDemod = results.durationDemod(1:nTurns);
    results.durationPosition = results.durationPosition(1:nTurns);
    results.durationTransfer = results.durationTransfer(1:nTurns);
    results.voltageMotor = results.voltageMotor(1:nTurns);
    results.durationBetweenTransfers = results.durationBetweenTransfers(1:nTurns);
    results.durationTransfer = results.durationTransfer(1:nTurns);
    results.durationSDRAMWrite = results.durationSDRAMWrite(1:nTurns);
    results.fluoPullback = results.fluoPullback(1:nTurns);
end

%Restore slider
if nSlices >= 1
    set(handles.slSlice,'Enable','off'); %Disable slider to make sure the change of the slider value doesn't call a display
    if nSlices > 1
        set(handles.slSlice,'Min',1);set(handles.slSlice,'Max',nSlices);set(handles.slSlice,'Value',1);
        set(handles.slSlice,'SliderStep',[1/(nSlices-1) 1/(nSlices-1)]);
    else
        set(handles.slSlice,'Min',0.9);set(handles.slSlice,'Max',nSlices);set(handles.slSlice,'Value',1);
        set(handles.slSlice,'SliderStep',[1 1]);
    end
    set(handles.slSlice,'Enable','on'); %Re-enable the slider
end

%Close and move data files in the Results folder
fclose('all');
tic
if handles.acqInfo.idxPositionTotal > 0 %Only if something has been acquired
    %movefile is very slow...
    handles.acqInfo.filePAF = -1; handles.acqInfo.fileUSF = -1; handles.acqInfo.fileFluoF = -1; handles.acqInfo.filePA2D = -1; handles.acqInfo.fileUS2D = -1; handles.acqInfo.fileFluo2D = -1;
%     movefile([handles.acqInfo.currentPath '\Temp\DataPA_StartTime_' handles.acqInfo.startTime '.dat'],[outputName '_PA.dat'])
%     movefile([handles.acqInfo.currentPath '\Temp\DataUS_StartTime_' handles.acqInfo.startTime '.dat'],[outputName '_US.dat'])
%     movefile([handles.acqInfo.currentPath '\Temp\DataFluo_StartTime_' handles.acqInfo.startTime '.dat'],[outputName '_Fluo.dat'])
    dos(['move ' handles.acqInfo.currentPath '\Temp\DataPA_StartTime_' handles.acqInfo.startTime '.dat ' handles.acqInfo.outputFullFileName '_PA.dat']);
    dos(['move ' handles.acqInfo.currentPath '\Temp\DataUS_StartTime_' handles.acqInfo.startTime '.dat ' handles.acqInfo.outputFullFileName '_US.dat']);
    dos(['move ' handles.acqInfo.currentPath '\Temp\DataFluo_StartTime_' handles.acqInfo.startTime '.dat ' handles.acqInfo.outputFullFileName '_Fluo.dat']);
end

% %Saving data
handles.results = results;
handles.scanDate = scanDate;
save([handles.acqInfo.outputFullFileName '.mat'],'-struct','handles','scanDate','param','results','acqInfo','displayInfo','-v7.3')
handles = rmfield(handles,'results');
clear handles.scanDate;

toc
%Set the popup menu of the pullback
if handles.acqInfo.acqInterrupted == 1 && nSlices > 0
    for idxSlice = 1:nSlices
        distance = handles.param.distanceFirstSlice+(idxSlice-1)*handles.param.distanceBetweenSlices;
        p = [char(num2str(distance)) 'm'];
        pullbackPosition(idxSlice) = {p};
    end
    set(handles.pmPAPullback,'String',pullbackPosition); 
    set(handles.pmUSPullback,'String',pullbackPosition); 
    set(handles.pmFluoPullback,'String',pullbackPosition); 
    set(handles.pmPAPullback,'Value',nSlices); 
    set(handles.pmUSPullback,'Value',nSlices); 
    set(handles.pmFluoPullback,'Value',nSlices);     
end

handles.acqInfo.oldSavedFiles = boolean(0); %Not an old saved file if the acquisition just finished.

%Restore GUI
set(handles.pmPAPullback,'Enable','on');
set(handles.pmUSPullback,'Enable','on');
set(handles.pmFluoPullback,'Enable','on');
set(handles.rbFluoFull2D,'Enable','on');
set(handles.cbRemoveFluoBase, 'Enable', 'on')
set(handles.cbRemoveFluoBase, 'Value', 0)
set(handles.pbStart, 'Enable', 'on') %Enable the Start Button to start a new acquisition
set(handles.pbLoad, 'Enable', 'on') %Allow loading previous results
set(handles.pbSavePA, 'Enable', 'on')
set(handles.pbSaveUS, 'Enable', 'on')
set(handles.pbSaveUSVideo, 'Enable', 'on')
set(handles.pbSaveFluo, 'Enable', 'on')    
set(handles.etTurnsSlice,'Enable','on');
if handles.param.constantPullback == 0
    set(handles.etNSlices,'Enable','on');
else
    set(handles.etNSlices,'Enable','off');
end
set(handles.etDistanceSlice,'Enable','on');
set(handles.pbRefreshDisplay, 'Enable', 'on') 
handles.acqInfo.acqRunning = 0; %Acquisition is not running anymore

disp('Ended acquisition successfully.')
set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), ['Total duration: ' num2str(results.durationTotalAcquisition) 'seconds.'])) 
set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))
setappdata(handles.figure1,'results',results);
guidata(hObject, handles); % Update handles at the end of a function
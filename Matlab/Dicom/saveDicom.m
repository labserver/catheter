acqInfo = handles.acqInfo;
displayInfo = handles.displayInfo;
filename = handles.acqInfo.outputFileName;
filename = 'PAUSFluo3D';
US2D = handles.resultsTemp.US2D;
PA2D = handles.resultsTemp.PA2D;
fluo2D = handles.resultsTemp.fluo2D;
clear handles
clear hObject


%Save US
US2Dall = zeros(displayInfo.nPixels,displayInfo.nPixels,size(US2D,1));
for indPullback = 1:size(US2D,1)
%     PA = squeeze(handles.results.PA(indPullback,:,:));
%     US = squeeze(handles.results.US(indPullback,:,:));
%     fluo = squeeze(handles.results.fluo(indPullback,:,:));    
%     [PAF,USF,fluoF,PA2D,US2D,fluo2D] = Analysis2D(handles.param, handles.displayInfo, PA, US, fluo);
    
    US2Dall(:,:,indPullback) = US2D(indPullback,:,:);
%     US2Dall(:,:,4*indPullback-2) = log(abs(resultsTemp.US2D(indPullback,:,:))+1);
%     US2Dall(:,:,4*indPullback-1) = log(abs(resultsTemp.US2D(indPullback,:,:))+1);
%     US2Dall(:,:,4*indPullback) = log(abs(resultsTemp.US2D(indPullback,:,:))+1);
    
end

volume=uint16((US2Dall-min(US2Dall(:)))/(max(US2Dall(:))-min(US2Dall(:)))*(60000) + 5000);

for indX = 1:500
    for indY = 1:500
        radius = sqrt((indX-250)^2 + (indY-250)^2);
        if radius <= 50
            volume(indX,indY,:) = radius;
        end
    end
end

imageType = 'US';
scanDate=datestr(now,'yyyy.mm.dd_HH.MM.SS');
filenameAll = ['Dicom' imageType '_' filename '_' scanDate];
mkdir('Dicom',filenameAll);
dirname = ['Dicom\' filenameAll '\' filenameAll '_'];
dicom_write_volume(volume,dirname);

%Save PAFluo
PA2Dall = zeros(displayInfo.nPixels,displayInfo.nPixels,size(PA2D,1));
fluo2Dall = zeros(displayInfo.nPixels,displayInfo.nPixels,size(PA2D,1));
for indPullback = 1:size(PA2D,1)
%     PA = squeeze(handles.results.PA(indPullback,:,:));
%     US = squeeze(handles.results.US(indPullback,:,:));
%     fluo = squeeze(handles.results.fluo(indPullback,:,:));    
%     [PAF,USF,fluoF,PA2D,US2D,fluo2D] = Analysis2D(handles.param, handles.displayInfo, PA, US, fluo);
    
    PA2Dall(:,:,indPullback) = log(abs(PA2D(indPullback,:,:))+1);
    fluo2Dall(:,:,indPullback) = fluo2D(indPullback,1,:,:);
    
%     US2Dall(:,:,4*indPullback-2) = log(abs(resultsTemp.US2D(indPullback,:,:))+1);
%     US2Dall(:,:,4*indPullback-1) = log(abs(resultsTemp.US2D(indPullback,:,:))+1);
%     US2Dall(:,:,4*indPullback) = log(abs(resultsTemp.US2D(indPullback,:,:))+1);
    
end
PA2Dall = (PA2Dall-min(PA2Dall(:)))/(max(PA2Dall(:))-min(PA2Dall(:)))*60000;
fluo2Dall = (fluo2Dall-min(fluo2Dall(:)))/(max(fluo2Dall(:))-min(fluo2Dall(:)));
fluo2Dall = (1-fluo2Dall);
volume=uint16(PA2Dall.*fluo2Dall + 5000);

for indX = 1:500
    for indY = 1:500
        radius = sqrt((indX-250)^2 + (indY-250)^2);
        if radius <= 50
            volume(indX,indY,:) = radius;
        end
    end
end

imageType = 'PAFluo';
scanDate=datestr(now,'yyyy.mm.dd_HH.MM.SS');
filenameAll = ['Dicom' imageType '_' filename '_' scanDate];
mkdir('Dicom',filenameAll);
dirname = ['Dicom\' filenameAll '\' filenameAll '_'];
dicom_write_volume(volume,dirname);

%
% Processes the data when there is a modification on a parameter or a refresh
% is called. This function is never called during an acquisition.
%   
% INPUTS:
%
% handles: Handles to the GUI, which allows reading of the parameters of the acquisition, the display parameters, 
% the results and the acquisition information set by the user.
%
% hObject: Link to the GUI. It is used to save the handles at the end of
% the function.
%
% doSave: Used to save the data from one of the three modalities shown. The
% value is sent to the Display function for processing.
%
% OUTPUTS:
%
% none.
%
% COMMENTS:
%
% There is no outputs. However, user data of the handles is update at the end of the function using the command guidata
%
function ProcessData(handles, hObject, doSave)

%Save is an optional parameter to save one of the three images
if nargin < 3
    doSave = 0;
end

results = getappdata(handles.figure1,'results');
DisplaySettings(handles,hObject)
handles = guidata(hObject); % Retrieve the updated handle after calling a function
    
if handles.acqInfo.acqRunning == 1
    guidata(hObject, handles); % Update handles at the end of a function 
    return %Should never happen
end
%Display at the opening of the GUI or if there is less than a slice acquired
if floor(handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn) < 1   
    if handles.param.PAUSDataType == 4
        PA = int32(zeros(handles.param.nSamples,handles.param.nPositionsPerTurn)); %Photoacoustic signal
        US = int32(zeros(2*handles.param.nSamples,handles.param.nPositionsPerTurn)); %Ultrasound signal
    else
        PA = int16(zeros(handles.param.nSamples,handles.param.nPositionsPerTurn)); %Photoacoustic signal
        US = int16(zeros(2*handles.param.nSamples,handles.param.nPositionsPerTurn)); %Ultrasound signal           
    end   
    fluo = int32(zeros(handles.param.nPositionsPerTurn,handles.acqInfo.lengthFluo)); %Fluorescence signal
    [PAF,USF,fluoF] = AnalysisFilter(handles.param, handles.displayInfo, handles.acqInfo, PA, US, fluo);
    
    %We calculate the 2D image.
    [PA2D,US2D,fluo2D] = Analysis2D(handles.param, handles.displayInfo, PAF, USF, fluoF); 
    Display(handles, hObject, PAF, USF, fluoF, PA2D, US2D, fluo2D, doSave);
    handles = guidata(hObject); % Retrieve the updated handle after calling a function   
    guidata(hObject, handles); % Update handles at the end of a function 
    return
end

handles.displaying = 1; %Prevent changes of some buttons

if size(fopen('all'))~=0
    fclose all;
%     disp('Files already opened')
%     return
end    
idxSlicePA = handles.displayInfo.PAPullback;
idxSliceUS = handles.displayInfo.USPullback;
idxSliceFluo = handles.displayInfo.fluoPullback;   

%Check if the user wants to calculate the baseline (function called
%only the first time)
if handles.displayInfo.fluoRemoveBaselineStatus == '0' && handles.displayInfo.fluoRemoveBaseline == 1
    set(handles.cbRemoveFluoBase,'UserData','2'); %Calculating fluoBaseline
    [handles.displayInfo.fluoBaseline, handles.displayInfo.sliceBaseline] = fluoBaseline(handles.param, handles.displayInfo, handles.acqInfo);
    set(handles.cbRemoveFluoBase,'UserData','1'); %fluoBaseline calculated
    handles.displayInfo.fluoRemoveBaselineStatus = '1';
end
%Check if the user wants to calculate the full 2D image (function called
%only the first time)    
if get(handles.rbFluoFull2D,'UserData') == '0' && handles.displayInfo.fluoDisplayFull2D == 1
    set(handles.rbFluoFull2D,'UserData','2'); %Calculating fluoFull2D
    results.fluoFull2D = fluoFull2D(handles.param, handles.displayInfo, handles.acqInfo);
    setappdata(handles.figure1,'results',results);
    set(handles.rbFluoFull2D,'UserData','1'); %fluoFull2D calculated
end
%Check if the user wants to calculate the US LineTime (function called
%only the first time)    
if get(handles.rbUSLineTime,'UserData') == '0' && handles.displayInfo.USDisplayLineTime == 1
    set(handles.rbUSLineTime,'UserData','2'); %Calculating USLineTime
    results.USLineTime = USLineTime(handles.param, handles.displayInfo, handles.acqInfo);
    setappdata(handles.figure1,'results',results);        
    set(handles.rbUSLineTime,'UserData','1'); %USLineTime calculated
end    
nTurnsPerSlice = handles.param.nTurnsPerSlice;
if handles.param.saveSingleImagePerSlice == 1; nTurnsPerSlice = 1; end    
%Check if the loaded file is from an older version
if handles.acqInfo.oldSavedFiles == 0
    %Retrieve the slice in the .dat files
    handles.acqInfo.filePA = fopen([handles.acqInfo.outputFullFileName '_PA.dat'],'r');
    handles.acqInfo.fileUS = fopen([handles.acqInfo.outputFullFileName '_US.dat'],'r'); 
    handles.acqInfo.fileFluo = fopen([handles.acqInfo.outputFullFileName '_Fluo.dat'],'r');
    %Go to the correct slice.
    type = handles.param.PAUSDataType;
    typeFluo = type; typeFluo(typeFluo < 4) = 4; %There is no fluo data at 16 bits
    fseek(handles.acqInfo.filePA, (idxSlicePA-1)*nTurnsPerSlice*handles.param.nPositionsPerTurn*handles.param.nSamples*type, 'bof');
    fseek(handles.acqInfo.fileUS, (idxSliceUS-1)*nTurnsPerSlice*handles.param.nPositionsPerTurn*2*handles.param.nSamples*type, 'bof');
    fseek(handles.acqInfo.fileFluo, (idxSliceFluo-1)*nTurnsPerSlice*handles.param.nPositionsPerTurn*handles.acqInfo.lengthFluo*typeFluo, 'bof');
end
%Loop for all turns of a slice
for idxTurn = 1:nTurnsPerSlice
    handles.idxTurn = idxTurn;
    %Different with the old version of the results files
    if handles.acqInfo.oldSavedFiles == 1
        %Not working anymore
            idxTurnTotalPA = handles.param.nTurnsPerSlice*(idxSlicePA-1) + idxTurn;
            idxTurnTotalUS = handles.param.nTurnsPerSlice*(idxSliceUS-1) + idxTurn;
            idxTurnTotalFluo = handles.param.nTurnsPerSlice*(idxSliceFluo-1) + idxTurn;
            PA = squeeze(results.PA(idxTurnTotalPA,:,:))';
            US = squeeze(results.US(idxTurnTotalUS,:,:))';
            fluo = squeeze(results.fluo(idxTurnTotalFluo,:,:));            
    else
        if handles.param.PAUSDataType == 8 %Saved data is in double and must be transposed
            %Must transpose old data for faster calculations
            PA = reshape(fread(handles.acqInfo.filePA, handles.param.nPositionsPerTurn*handles.param.nSamples, 'double'),handles.param.nPositionsPerTurn,handles.param.nSamples);
            US = reshape(fread(handles.acqInfo.fileUS, handles.param.nPositionsPerTurn*2*handles.param.nSamples, 'double'),handles.param.nPositionsPerTurn,2*handles.param.nSamples);
            fluo = reshape(fread(handles.acqInfo.fileFluo, handles.param.nPositionsPerTurn*handles.acqInfo.lengthFluo, 'double'),handles.param.nPositionsPerTurn,handles.acqInfo.lengthFluo);            
            PA = PA'; US = US'; %Must transpose old data for faster calculations
            PA = int32(PA); US = int32(US); fluo = int32(fluo);
        elseif handles.param.PAUSDataType == 4
            PA = reshape(fread(handles.acqInfo.filePA, handles.param.nPositionsPerTurn*handles.param.nSamples, 'int32'),handles.param.nSamples,handles.param.nPositionsPerTurn);
            US = reshape(fread(handles.acqInfo.fileUS, handles.param.nPositionsPerTurn*2*handles.param.nSamples, 'int32'),2*handles.param.nSamples,handles.param.nPositionsPerTurn);
            fluo = reshape(fread(handles.acqInfo.fileFluo, handles.param.nPositionsPerTurn*handles.acqInfo.lengthFluo, 'int32'),handles.param.nPositionsPerTurn,handles.acqInfo.lengthFluo);                     
        else %2
            PA = reshape(fread(handles.acqInfo.filePA, handles.param.nPositionsPerTurn*handles.param.nSamples, 'int16'),handles.param.nSamples,handles.param.nPositionsPerTurn);
            US = reshape(fread(handles.acqInfo.fileUS, handles.param.nPositionsPerTurn*2*handles.param.nSamples, 'int16'),2*handles.param.nSamples,handles.param.nPositionsPerTurn);
            fluo = reshape(fread(handles.acqInfo.fileFluo, handles.param.nPositionsPerTurn*handles.acqInfo.lengthFluo, 'int32'),handles.param.nPositionsPerTurn,handles.acqInfo.lengthFluo);                 
        end
    end
        %Code to display a given fluo turn instead of the remaining turns.
        %Useful in PA acquisition to choose the right fluo turn to save.
        %In PA acquisition, there is a glitch on the fluo result, so we
        %need to replace some angles with a previous turn.
%         if idxTurn == 190
%             handles.fluoTest2 = fluo;
%         elseif idxTurn == 222
%             handles.fluoTest = fluo;
%             %Glitch at 25:40 of turn 222 replaced by turn 190.
%             handles.fluoTest(25:40,2) = handles.fluoTest2(25:40,2);
%         elseif idxTurn > 222
%             fluo = handles.fluoTest;
%         end

        %Code to display a given US turn instead of the remaining turns.
        %Useful in PA acquisition to choose the right US turn to save.
%         if idxTurn == 222
%             handles.USTest = US;
%         elseif idxTurn >= 222
%             US = handles.USTest;
%         end

    [PAF,USF,fluoF] = AnalysisFilter(handles.param, handles.displayInfo, handles.acqInfo, PA, US, fluo);

    %Amplitude correction with wall distance
    %First, remove angle shift
    nPos=handles.param.nPositionsPerTurn; aS=-handles.displayInfo.angleShiftFluo;
    if aS > 0
        fluoFTemp = fluoF(:,nPos-aS+1:nPos); fluoF(:,aS+1:end) = fluoF(:,1:nPos-aS); fluoF(:,1:aS) = fluoFTemp;
    elseif aS < 0
        aS = abs(aS); fluoFTemp = fluoF(:,1:aS); fluoF(:,1:nPos-aS) = fluoF(:,aS+1:nPos); fluoF(:,nPos-aS+1:nPos) = fluoFTemp;
    end    
    outputFile = [handles.acqInfo.outputFullFileName '_Wall.mat'];
    if exist(outputFile,'file')
        load(outputFile)
        wall = wall-0.5; %Distance from catheter;
        wall(wall>0.4) = 0.8-wall(wall>0.4);
        wall(wall<0.1) = 0.1;
        ueff = 2;
        for ind = 1:4
            avg = min(min(fluoF(ind,:)));
            if (handles.displayInfo.fluoPullback > 0)
                fluoF(ind,:) = avg +(squeeze(fluoF(ind,:))-avg+1) .*  exp(ueff.*2.*wall(handles.displayInfo.fluoPullback,:));
            end
        end
    end
    %Put angle shift back
    nPos=handles.param.nPositionsPerTurn; aS=handles.displayInfo.angleShiftFluo;
    if aS > 0
        fluoFTemp = fluoF(:,nPos-aS+1:nPos); fluoF(:,aS+1:end) = fluoF(:,1:nPos-aS); fluoF(:,1:aS) = fluoFTemp;
    elseif aS < 0
        aS = abs(aS); fluoFTemp = fluoF(:,1:aS); fluoF(:,1:nPos-aS) = fluoF(:,aS+1:nPos); fluoF(:,nPos-aS+1:nPos) = fluoFTemp;
    end     

    %Make sure resultsTemp exists and it is the right size
    if ~isfield(handles,'resultsTemp'); handles.resultsTemp = []; end
    if ~isfield(handles.resultsTemp,'PAF'); handles.resultsTemp.PAF = zeros(handles.displayInfo.nSamplesToDisplay,handles.param.nPositionsPerTurn); end
    if ~isequal(size(handles.resultsTemp.PAF),[handles.displayInfo.nSamplesToDisplay handles.param.nPositionsPerTurn]); handles.resultsTemp.PAF = zeros(handles.displayInfo.nSamplesToDisplay,handles.param.nPositionsPerTurn); end

    %Average all the turns of a slice
    if (mod(idxTurn,handles.param.nTurnsPerSlice) ~= 1 && handles.param.nTurnsPerSlice ~= 1)
        %Not the first turn of a slice
        handles.resultsTemp.fluoF = handles.resultsTemp.fluoF + fluoF;
        if handles.param.photoacousticOn == 0
            handles.resultsTemp.PAF = handles.resultsTemp.PAF + PAF; 
        end
    elseif handles.param.nTurnsPerSlice == 1
        %First and only turn of a slice
        handles.resultsTemp.PAF = PAF; 
        handles.resultsTemp.fluoF = fluoF;  
    else
        %First turn of a slice
        handles.resultsTemp.PAF = zeros(handles.displayInfo.nSamplesToDisplay,handles.param.nPositionsPerTurn);;
        handles.resultsTemp.fluoF = fluoF;  
    end  
    if handles.param.photoacousticOn == 1 && handles.acqInfo.oldSavedFiles == 0
        %TODO: 20 should be a parameter
        scanPosIsPAPulse = results.scanPosIsPAPulse{(idxSlicePA-1)*handles.param.nPositionsPerTurn/20*handles.param.speedMotorTurnsPerSec+idxTurn};

        if handles.param.saveSingleImagePerSlice
            scanPosIsPAPulse = logical(scanPosIsPAPulse*0+1);
        end
        for ind = find(scanPosIsPAPulse)                    
            %Need 32 slices to get a complete PA acquisition
            if (mod(handles.acqInfo.idxTurnTotal,handles.param.PANTurnsFull) ~= 1 && handles.param.nTurnsPerSlice ~= handles.param.PANTurnsFull) 
                %Not the first 32 turns of a slice
                handles.resultsTemp.PAF(:,ind) = handles.resultsTemp.PAF(:,ind) + PAF(:,ind);
            else
                %First 32 turns of a slice
                handles.resultsTemp.PAF(:,ind) = PAF(:,ind);
            end
        end
    end  

%         %Code to load and save a single PA turn when there were 256 saved (used for faster processing) 
%         load('PAF3D.mat')
%         handles.resultsTemp.PAF = squeeze(PA(get(handles.pmPAPullback,'Value'),:,:));
%         load('PATrimodalGood');
%         PAF = PATrimodalGood;
%         handles.resultsTemp.PAF = PATrimodalGood;
%         if idxTurn == 256
%             PATrimodalGood=handles.resultsTemp.PAF;
%             save('PATrimodalGood.mat','PATrimodalGood')
%         end        
    %Display if it is the last turn at "Once" or if the user wants to display the sum of the turns, display them
    if (strcmp(handles.displayInfo.displayMultipleTurns,'Once') && idxTurn == handles.param.nTurnsPerSlice) || strcmp(handles.displayInfo.displayMultipleTurns,'Sum')
         %Calculate the 2D image.
        [PA2D,US2D,fluo2D] = Analysis2D(handles.param, handles.displayInfo, handles.resultsTemp.PAF, USF, handles.resultsTemp.fluoF); 
        Display(handles, hObject, handles.resultsTemp.PAF, USF, handles.resultsTemp.fluoF, PA2D, US2D, fluo2D, doSave);
        handles = guidata(hObject); % Retrieve the updated handle after calling a function         
    %If the user wants to display all the turns, display them (before summing)
    elseif strcmp(handles.displayInfo.displayMultipleTurns,'All')
        %Calculate the 2D image.

%             USF(2*round((0.3+0.7)*1e-3*200e6/1540)+(1:5),:) = max(USF(:)); %This will add a circle at a given distance from the center
%             if idxTurn == 5 %Display only a given turn per slice
            [PA2D,US2D,fluo2D] = Analysis2D(handles.param, handles.displayInfo, PAF, USF, fluoF); 
%                 if idxTurn==180 %DEBUG: Display only one turn
                Display(handles, hObject, PAF, USF, fluoF, PA2D, US2D, fluo2D, doSave);
                handles = guidata(hObject); % Retrieve the updated handle after calling a function  
%                 end
%             end
    end         

end

fclose('all');
handles.acqInfo.filePAF = -1; handles.acqInfo.fileUSF = -1; handles.acqInfo.fileFluoF = -1; handles.acqInfo.filePA2D = -1; handles.acqInfo.fileUS2D = -1; handles.acqInfo.fileFluo2D = -1; 
handles.displaying=0;

%Chec if slice slider moved
if ~isfield(handles,'displaying'); handles.displaying=0; end
slider = round(get(handles.slSlice,'Value'));
%Check if the slider changed position
if slider ~= get(handles.pmPAPullback,'Value') && get(handles.pmPAPullback,'Value') == get(handles.pmUSPullback,'Value') && get(handles.pmPAPullback,'Value') == get(handles.pmFluoPullback,'Value')
    set(handles.pmPAPullback,'Value',slider);
    set(handles.pmUSPullback,'Value',slider);
    set(handles.pmFluoPullback,'Value',slider);            
    ProcessData(handles, hObject)
    handles = guidata(hObject); % Retrieve the updated handle after calling a function 
end

%Check display queue
if ~isfield(handles,'savingVideo'); handles.savingVideo = 0; end
queue = get(handles.pbRefreshDisplay, 'UserData');
if isempty(queue); queue = -1; end;
if (handles.acqInfo.acqRunning == 0) && (queue >= 1) && (handles.savingVideo == 0)
    if get(handles.pbRefreshDisplay, 'UserData') == 2 %Slider moved
        slider = round(get(handles.slSlice,'Value'));
        %Check if the slider changed position
        if slider ~= get(handles.pmPAPullback,'Value') || slider ~= get(handles.pmUSPullback,'Value') || slider ~= get(handles.pmFluoPullback,'Value')
            %disp('Update display')
            set(handles.pmPAPullback,'Value',slider);
            set(handles.pmUSPullback,'Value',slider);
            set(handles.pmFluoPullback,'Value',slider);            
        end   
    elseif get(handles.pbRefreshDisplay, 'UserData') == 3 %Do fluo full 2D
        set(handles.rbFluoSignal,'Value',0)
        set(handles.rbFluo2D,'Value',0)
        set(handles.rbFluoFull2D,'Value',1)

        set(handles.rbFluoFull2D,'UserData','0'); %This values is at 1 when the fluo full 2D data is valid
        if ~get(handles.rbFluoFull2D,'Value')
            %Remove full 2D 
            if isfield(results,'fluoFull2D')
                results = rmfield(results,'fluoFull2D');
            end
        end        
    end
    set(handles.pbRefreshDisplay, 'UserData', 0);
    handles.displayInfo.updateAxisInformation = 1; set(handles.pmInfo, 'UserData', '1');
    ProcessData(handles, hObject) %Refresh display
    handles = guidata(hObject); % Retrieve the updated handle after calling a function        
end
guidata(hObject, handles); % Update handles at the end of a function 
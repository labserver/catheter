function varargout = PAUSFluo(varargin)
% PAUSFLUO M-file for PAUSFluo.fig
%      PAUSFLUO, by itself, creates a new PAUSFLUO or raises the existing
%      singleton*.
%
%      H = PAUSFLUO returns the handle to a new PAUSFLUO or the handle to
%      the existing singleton*.
%
%      PAUSFLUO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PAUSFLUO.M with the given input arguments.
%
%      PAUSFLUO('Property','Value',...) creates a new PAUSFLUO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PAUSFluo_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PAUSFluo_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PAUSFluo

% Last Modified by GUIDE v2.5 18-Feb-2014 17:27:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PAUSFluo_OpeningFcn, ...
                   'gui_OutputFcn',  @PAUSFluo_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PAUSFluo is made visible.
function PAUSFluo_OpeningFcn(hObject, eventdata, handles, varargin)
clc
% Choose default command line output for PAUSFluo
handles.output = hObject;
guidata(hObject, handles); % Update handles

%Check if we are in the right path
if exist([pwd '\Results'],'dir') ~= 7
    errordlg('The GUI can''t start because the folder Results doesn''exist. Please create this folder or choose the right directory.','Wrong directory')
    return
end

addpath('Tools')

handles.displaying = 0;

% Create the parameters and results structures.
[handles.param, results, handles.acqInfo] = Parameters(handles);

% Add the current path to the Matlab path, assuming that all scripts are in the same folder.
handles.acqInfo.currentPath = pwd;

fclose('all');

% Initialize the UI components.
set(handles.pbStop, 'Enable', 'off') 
set(handles.rbPARF,'Value', 1);
set(handles.rbPA2D,'Value', 0);
set(handles.rbPA2DCart,'Value', 0);
set(handles.rbUSRF,'Value', 0);
set(handles.rbUS2D,'Value', 1);
set(handles.rbUS2DCart,'Value', 0);
set(handles.rbUSLineTime,'Value', 0);
set(handles.rbFluoSignal,'Value', 1);
set(handles.rbFluo2D,'Value', 0);
set(handles.rbFluoFull2D,'Value', 0);
set(handles.cbRemoveFluoBase, 'Enable', 'off') 

set(handles.pbSavePA, 'Enable', 'off')
set(handles.pbSaveUS, 'Enable', 'off')
set(handles.pbSaveFluo, 'Enable', 'off')
set(handles.pbSavePAVideo, 'Enable', 'off')
set(handles.pbSaveUSVideo, 'Enable', 'off')
set(handles.pbSaveFluoVideo, 'Enable', 'off')

%Set the popup menu of the angular positions
for idx1 = 1:handles.param.nPositionsPerTurn
    angle(idx1) = idx1;
end
set(handles.pmPAAngle,'String',num2str(angle')); 
set(handles.pmUSAngle,'String',num2str(angle')); 

%Set the popup menu of the pullback
for idxSlice = 1:handles.param.nSlices
    distance = handles.param.distanceFirstSlice+(idxSlice-1)*handles.param.distanceBetweenSlices;
    p = [char(num2str(distance)) 'm'];
    pullbackPosition(idx1) = {p};
end
set(handles.pmPAPullback,'String',pullbackPosition); 
set(handles.pmUSPullback,'String',pullbackPosition); 
set(handles.pmFluoPullback,'String',pullbackPosition); 

set(handles.pmFluoSDPair,'String',{'S:1 D:1'; 'S:1 D:2'; 'S:2 D:1'; 'S:2 D:2'});
set(handles.pmInfo,'String',{'Angular synchronization'; 'Transfer synchronization'; 'Motor voltage'; 'Fluorescence'; 'Photoacoustic Monitor'; 'TGC Ramp'});

set(handles.cbRemoveFluoBase,'UserData', '0');
set(handles.rbFluoFull2D,'UserData', '0');
set(handles.rbUSLineTime,'UserData', '0');

%Detect changes on the slider to change pull-back positions of all the results
if handles.param.nSlices >= 1
    set(handles.slSlice,'Enable','off'); %Disable slider to make sure the change of the slider value doesn't call a display
    if handles.param.nSlices > 1
        set(handles.slSlice,'Min',1);set(handles.slSlice,'Max',handles.param.nSlices);set(handles.slSlice,'Value',1);
        set(handles.slSlice,'SliderStep',[1/(handles.param.nSlices-1) 1/(handles.param.nSlices-1)]);
    else
        set(handles.slSlice,'Min',0.9);set(handles.slSlice,'Max',handles.param.nSlices);set(handles.slSlice,'Value',1);
        set(handles.slSlice,'SliderStep',[1 1]);
    end
    set(handles.slSlice,'Enable','on'); %Re-enable the slider
end
hListener = addlistener(handles.slSlice,'Value','PostSet',@(src,event)slSliceMove(hObject, src, event));

%Detect changes on the TGC slider
hListenerTGC1 = addlistener(handles.slTGC1,'Value','PostSet',@(src,event)slTGCMove(hObject, src, event));
hListenerTGC2 = addlistener(handles.slTGC2,'Value','PostSet',@(src,event)slTGCMove(hObject, src, event));
hListenerTGC3 = addlistener(handles.slTGC3,'Value','PostSet',@(src,event)slTGCMove(hObject, src, event));
hListenerTGC4 = addlistener(handles.slTGC4,'Value','PostSet',@(src,event)slTGCMove(hObject, src, event));

%Detect changes on the display sliders
hListenerDisplayGainPA = addlistener(handles.slDisplayGainPA,'Value','PostSet',@(src,event)slDisplayMove(hObject, src, event));
hListenerDisplayDynamicRangePA = addlistener(handles.slDisplayDynamicRangePA,'Value','PostSet',@(src,event)slDisplayMove(hObject, src, event));
hListenerDisplayGainUS = addlistener(handles.slDisplayGainUS,'Value','PostSet',@(src,event)slDisplayMove(hObject, src, event));
hListenerDisplayDynamicRangeUS = addlistener(handles.slDisplayDynamicRangeUS,'Value','PostSet',@(src,event)slDisplayMove(hObject, src, event));
hListenerDisplayGainFluo = addlistener(handles.slDisplayGainFluo,'Value','PostSet',@(src,event)slDisplayMove(hObject, src, event));
hListenerDisplayDynamicRangeFluo = addlistener(handles.slDisplayDynamicRangeFluo,'Value','PostSet',@(src,event)slDisplayMove(hObject, src, event));

%Generate ToolTips
% set(handles.lbInfo, 'String', 'Hover the pointer over any box for more informations.')
% set(uicontrol(handles.pbStop, 'style','edit'),'ToolTipString','Stop the acquisition. You have to press twice, for more protection.');
% set(uicontrol(handles.etPALPF, 'style','edit'),'ToolTipString','Cutoff frequency of the low-pass filter (in Hz) (-1 to deactivate).');
% set(uicontrol(handles.etPAHPF, 'style','edit'),'ToolTipString','Cutoff frequency of the high-pass filter (in Hz) (-1 to deactivate).');
% set(uicontrol(handles.etUSLPF, 'style','edit'),'ToolTipString','Cutoff frequency of the low-pass filter (in Hz) (-1 to deactivate).');
% set(uicontrol(handles.etUSHPF, 'style','edit'),'ToolTipString','Cutoff frequency of the high-pass filter (in Hz) (-1 to deactivate).');
% set(uicontrol(handles.etFluoLPF, 'style','edit'),'ToolTipString','Cutoff frequency of the low-pass filter (in 1/position) (-1 to deactivate).');
% set(uicontrol(handles.etFluoHPF, 'style','edit'),'ToolTipString','Cutoff frequency of the high-pass filter (in 1/position) (-1 to deactivate).');
% set(uicontrol(handles.cbPAHilbert, 'style','edit'),'ToolTipString','Perform or not the Hilbert transform.');
% set(uicontrol(handles.cbUSHilbert, 'style','edit'),'ToolTipString','Perform or not the Hilbert transform.');
% set(uicontrol(handles.etHideRadius, 'style','edit'),'ToolTipString','Radius to hide (in meters). Used to show the catheter in the middle. Also useful to hide the ultrasound pulse.');
% set(uicontrol(handles.etNPixels, 'style','edit'),'ToolTipString','Proportionnal to the quality of the image, but slower to calculate. Increase before saving an image.');
% set(uicontrol(handles.etTurnsSlice, 'style','edit'),'ToolTipString','Multiple turns can be done on a single slice and all of them are stored independently.');
% set(uicontrol(handles.etNSlices, 'style','edit'),'ToolTipString','After each slice, the linear motor performs a pull-back.');
% set(uicontrol(handles.etDistanceSlice, 'style','edit'),'ToolTipString','Distance between each slice (in millimeters).');
% set(uicontrol(handles.etFileName, 'style','edit'),'ToolTipString','The results will be saved in a .mat starting with this text.');
% set(uicontrol(handles.etComments, 'style','edit'),'ToolTipString','These comments will be saved with the results.');
% set(uicontrol(handles.pmInfo, 'style','edit'),'ToolTipString','Choose which additionnal information will be displayed.');
% set(uicontrol(handles.lbInfo, 'style','edit'),'ToolTipString','Information on the current acquisition.');
% set(uicontrol(handles.lbProblems, 'style','edit'),'ToolTipString','Problems originating from the system, usually a synchronization warning.');
% set(uicontrol(handles.pbRefreshDisplay, 'style','edit'),'ToolTipString','Press this button after changing display settings, when the acquisition is not running.');
% set(uicontrol(handles.cbFilterWrapFluo, 'style','edit'),'ToolTipString','Activate when the signal is periodic (useful in fluorescence).');
% set(uicontrol(handles.pmDisplayTypePA, 'style','edit'),'ToolTipString','Display the log of the value or the normal value.');
% set(uicontrol(handles.pmDisplayTypeUS, 'style','edit'),'ToolTipString','Display the log of the value or the normal value.');
% set(uicontrol(handles.pmDisplayTypeFluo, 'style','edit'),'ToolTipString','Display the log of the value or the normal value.');
% set(uicontrol(handles.cbInfoDisplayOn, 'style','edit'),'ToolTipString','Suppress this display to speed up the rest of the display.');
% set(uicontrol(handles.cbPADisplayOn, 'style','edit'),'ToolTipString','Suppress this display to speed up the rest of the display.');
% set(uicontrol(handles.cbFluoDisplayOn, 'style','edit'),'ToolTipString','Suppress this display to speed up the rest of the display.');
% set(uicontrol(handles.cbUSFluo, 'style','edit'),'ToolTipString','Combine US and fluo images on the same display.');
% set(uicontrol(handles.cbPAUndock, 'style','edit'),'ToolTipString','Show PA on an external figure to speed up display.');
% set(uicontrol(handles.cbUSUndock, 'style','edit'),'ToolTipString','Show US on an external figure to speed up display.');
% set(uicontrol(handles.cbFluoUndock, 'style','edit'),'ToolTipString','Show Fluo on an external figure to speed up display.');

handles.displayInfo = [];
handles.displayInfo.updateAxisInformation = 1;
set(handles.pmInfo, 'UserData', '1'); %Used to inform the display function to update axis information
ProcessData(handles, hObject)
handles = guidata(hObject); % Retrieve the updated handle after calling a function
guidata(hObject, handles); % Update handles at the end of a function


function varargout = PAUSFluo_OutputFcn(hObject, eventdata, handles) 
%Check if we are in the right path
if exist([pwd '\Results'],'dir') ~= 7
    close(handles.figure1)
    return
end

%Depends on the screen (multiple monitors)
%It will maximize the GUI on the left monitor
set(gcf, 'Position', [-200, 10, 300, 60]);
maximize(gcf)

% Get default command line output from handles structure
%if exist([pwd '\Results'],'dir') == 7 %Workaround
    varargout{1} = handles.output;
%end


function pbStop_Callback(hObject, eventdata, handles)
if get(handles.pbStop, 'UserData') == '1'
    set(handles.pbStop, 'Enable', 'off') 
else
    set(handles.pbStop, 'UserData', '1')
    set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), 'Press Stop again to stop acquisition.')) 
    set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))
end


function pbStart_Callback(hObject, eventdata, handles)
if ~isvarname(get(handles.etFileName,'String'))
    errordlg('Please do not put spaces or special characters in the filename. Also, the filename must be limited to 63 characters.','Wrong parameter')
    handles.acqInfo.acqRunning = 0;
    guidata(hObject, handles);
    return
end

%Parameters are loaded again
%Not written on the real handle, in case InitUSB fails
[paramTemp, results, acqInfoTemp] = Parameters(handles);

%Initialize USB connexion
if InitUSB(paramTemp) == 0
    return
end

handles.param = paramTemp;
setappdata(handles.figure1,'results',results);
handles.acqInfo = acqInfoTemp;

%Prepare GUI
set(handles.pbStop, 'UserData', '0')
set(handles.pbRefreshDisplay, 'Enable', 'off')
set(handles.pbSavePA, 'Enable', 'off')
set(handles.pbSaveUS, 'Enable', 'off')
set(handles.pbSaveFluo, 'Enable', 'off')
set(handles.pbSavePAVideo, 'Enable', 'off')
set(handles.pbSaveUSVideo, 'Enable', 'off')
set(handles.pbSaveFluoVideo, 'Enable', 'off')
set(handles.slSlice,'Enable','off');
set(handles.cbChangeTGC,'Value',0);
set(handles.slTGC1,'Enable','off');
set(handles.slTGC2,'Enable','off');
set(handles.slTGC3,'Enable','off');
set(handles.slTGC4,'Enable','off');
set(handles.pbStart, 'Enable', 'off')
set(handles.pbLoad, 'Enable', 'off')
set(handles.cbRemoveFluoBase, 'Enable', 'off')
set(handles.cbRemoveFluoBase, 'Value', 0)
%The parameters can't be modified once the acquisition is started
set(handles.etTurnsSlice,'Enable','off');
set(handles.etNSlices,'Enable','off');
set(handles.etDistanceSlice,'Enable','off');

handles.gainTest = []; handles = rmfield(handles,'gainTest'); %Debug
%Make sure to clear the baseline from the previous scan
if isfield(handles.displayInfo,'fluoBaseline')
    handles.displayInfo = rmfield(handles.displayInfo,'fluoBaseline');
    handles.displayInfo = rmfield(handles.displayInfo,'sliceBaseline');
end   
%Clear errors displayed
handles.lastErrorDisplayed = 0;
handles.errorDisplayStop = 0;
handles.lastErrorDisplayed = 0;

%Change the popup menu of the pullback
    %The first pullback position must be selected before
    set(handles.pmPAPullback,'Value',1); 
    set(handles.pmUSPullback,'Value',1); 
    set(handles.pmFluoPullback,'Value',1); 
for idxSlice = 1:handles.param.nSlices
    distance = handles.param.distanceFirstSlice+(idxSlice-1)*handles.param.distanceBetweenSlices;
    p = [char(num2str(distance)) 'm'];
    pullbackPosition(idxSlice) = {p};
end
set(handles.pmPAPullback,'String',pullbackPosition); 
set(handles.pmUSPullback,'String',pullbackPosition); 
set(handles.pmFluoPullback,'String',pullbackPosition); 

%FluoFull2D cannot be done while acquiring
if get(handles.rbFluoFull2D,'Value') == 1
    set(handles.rbFluoSignal,'Value',1)
    set(handles.rbFluo2D,'Value',0)
    set(handles.rbFluoFull2D,'Value',0)
end

set(handles.pmPAPullback,'Enable','off');
set(handles.pmUSPullback,'Enable','off');
set(handles.pmFluoPullback,'Enable','off');
set(handles.rbFluoFull2D,'Enable','off');

handles.displayInfo.updateAxisInformation = 1;
set(handles.pmInfo, 'UserData','1');

pause(0.01)

handles.durationTotalAcquisition = tic;
% profile on
handles.acqInfo.acqRunning = 1;
% Start a timer to empty the USB buffer and to update the display.
handles.timer = timer('TimerFcn',...
                      {@TimerFunction, handles, hObject},... 
                      'BusyMode',... 
                      'drop',...
                      'ExecutionMode',... 
                      'FixedRate',... 
                      'Period',... 
                      handles.acqInfo.refreshPeriod);
 
set(handles.pbStop, 'Enable', 'on')
set(handles.lbInfo,'Value',1)
set(handles.lbInfo, 'String', 'Acquisition started.')
set(handles.lbProblems,'Value',1)
set(handles.lbProblems,'String', 'No problems yet.');

setappdata(handles.figure1,'results',results);

guidata(hObject, handles); % Update handles at the end of a function       
drawnow
% Start the timer.
start(handles.timer);


function TimerFunction(inSource, inEvent, handles, hObject) 
handles = guidata(hObject); % Retrieve the updated handle in a timer function

%Check is stop button has been pressed
if strcmp(get(handles.pbStop,'Enable'),'off')
    % Stop acquisition
    set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), 'Stopping acquisition...')) 
    set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))
    set(handles.pbStop, 'Enable', 'off') 
    isRunning = 0;
    handles.acqInfo.acqInterrupted = 1;        
else
    isRunning = 1;
    handles.acqInfo.acqInterrupted = 0;    
end
handles.acqInfo.acqRunning = isRunning;

%Check if we have acquired enough data or if the acquisition has been stopped
if handles.acqInfo.idxPositionTotal >= handles.param.nTurns*handles.param.nPositionsPerTurn || isRunning == 0  
    stop(handles.timer) %Stop the timer
%     profile viewer
%     p = profile('info');
%     save('Profiler2000_1t_app.mat','p')      
    set(handles.pbStop, 'Enable', 'off') %Disable the Stop button
    %handles.acqInfo.acqInterrupted = 1; %Premature stop (possible corrupted data)
    End(handles, hObject); %Save data and end acquisition
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
    if handles.acqInfo.acqInterrupted == 1
        set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), 'Acquisition stopped by user before the end.'))  
    else
        set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), 'Acquisition done.'))  
    end
    set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))
else
    % The acquisition is still running. Read available data
    ReadUSB(handles, hObject);
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
end  
guidata(hObject, handles); % Update handles at the end of a function


function pbLoad_Callback(hObject, eventdata, handles)
fclose('all');
%Import data from a file
handles.lastErrorDisplayed = 0;
[filename,pathname] = uigetfile('-mat','Choose a results file to load',[handles.acqInfo.currentPath '\Results']);
if filename == 0
    return;
end
handles.displaying = 1; %Prevent changes of some buttons
    guidata(hObject, handles); % Update handles at the end of a function 
    handles = guidata(hObject); % Retrieve the updated handle after calling a function  

file=open([pathname filename]);
handles.param = file.param;
handles.acqInfo = file.acqInfo;
handles.acqInfo.acqRunning = 0;
handles.displayInfo = file.displayInfo;
results = file.results;
handles.scanDate = file.scanDate;
%Check if results are saved in the .mat (Old version)
[trash,filenameNoExtension] = fileparts(filename);
if exist([pathname filenameNoExtension '_PA.dat'],'file') 
    handles.acqInfo.oldSavedFiles = boolean(0); 
else
    handles.acqInfo.oldSavedFiles = boolean(1);
end

%If there are filtered images, load them
if isfield(file,'resultsTemp'); handles.resultsTemp = file.resultsTemp; end

%When loading a file, the current path stored in the file must be changed to the current one.
handles.acqInfo.currentPath = pwd;

set(handles.lbInfo,'Value',1)
set(handles.lbInfo, 'String', 'Previous results loaded.')
set(handles.lbProblems,'Value', 1)
set(handles.lbProblems, 'String', '')
        
%Check if data is valid (usually for compatibility with previous versions

%Convert auxiliary results from matrix to cell
if ~iscell(results.ADC8CH)
    results.ADC8CH=mat2cell(results.ADC8CH(1:handles.param.nPositionsPerTurn*handles.param.nTurns,:)',handles.acqInfo.lengthADC8CH,handles.param.nPositionsPerTurn*ones(handles.param.nTurns,1));
    if ~isfield(results,'scanPosIsPAPulse'); 
        results.scanPosIsPAPulse = (results.scanPos > handles.param.nPositionsPerTurn);
    end
    results.scanPosIsPAPulse=mat2cell(results.scanPosIsPAPulse(1:handles.param.nPositionsPerTurn*handles.param.nTurns,:)',1,handles.param.nPositionsPerTurn*ones(handles.param.nTurns,1));
    results.scanPos=mat2cell(results.scanPos(1:handles.param.nPositionsPerTurn*handles.param.nTurns,:)',1,handles.param.nPositionsPerTurn*ones(handles.param.nTurns,1));
    results.error=mat2cell(results.error(1:handles.param.nPositionsPerTurn*handles.param.nTurns,:)',1,handles.param.nPositionsPerTurn*ones(handles.param.nTurns,1));
    results.durationAcquisition=mat2cell(results.durationAcquisition(1:handles.param.nPositionsPerTurn*handles.param.nTurns,:)',1,handles.param.nPositionsPerTurn*ones(handles.param.nTurns,1));
    results.durationDemod=mat2cell(results.durationDemod(1:handles.param.nPositionsPerTurn*handles.param.nTurns,:)',1,handles.param.nPositionsPerTurn*ones(handles.param.nTurns,1));
    results.durationPosition=mat2cell(results.durationPosition(1:handles.param.nPositionsPerTurn*handles.param.nTurns,:)',1,handles.param.nPositionsPerTurn*ones(handles.param.nTurns,1));
    results.durationTransfer=mat2cell(results.durationTransfer(1:handles.param.nPositionsPerTurn*handles.param.nTurns,:)',1,handles.param.nPositionsPerTurn*ones(handles.param.nTurns,1));
    results.voltageMotor=mat2cell(results.voltageMotor(1:handles.param.nPositionsPerTurn*handles.param.nTurns,:)',1,handles.param.nPositionsPerTurn*ones(handles.param.nTurns,1));
    results.durationBetweenTransfers=mat2cell(results.durationBetweenTransfers(1:handles.param.nPositionsPerTurn*handles.param.nTurns,:)',1,handles.param.nPositionsPerTurn*ones(handles.param.nTurns,1));
    results.durationSDRAMWrite=mat2cell(results.durationSDRAMWrite(1:handles.param.nPositionsPerTurn*handles.param.nTurns,:)',1,handles.param.nPositionsPerTurn*ones(handles.param.nTurns,1));
    if isfield(results,'fluoPullback')
        results.fluoPullback=mat2cell(results.fluoPullback(1:handles.param.nTurns,:)',handles.acqInfo.lengthFluo,1*ones(handles.param.nTurns,1));     
    else
        disp('Fluo pullback doesn''t exist...')
        results.fluoPullback = cell(1,param.nTurns);
        for idxTurn = 1:handles.param.nTurns
            results.fluoPullback{idxTurn} = zeros(handles.acqInfo.lengthFluo,1); %Fluorescence signal, ignoring the rotation.
        end
    end
end

if isfield(handles.acqInfo,'idxSlice'); handles.acqInfo = rmfield(handles.acqInfo,'idxSlice'); end %Not used anymore
if ~isfield(handles.param,'saveSingleImagePerSlice'); handles.param.saveSingleImagePerSlice = boolean(0); end
if ~isfield(handles.param,'linearMotorSpeed'); handles.param.linearMotorSpeed = 5; end
if ~isfield(handles.param,'linearMotorSpeedHome'); handles.param.linearMotorSpeedHome = 20; end
if ~isfield(handles.param,'PAUSDataType'); handles.param.PAUSDataType = 8; end %Old data is saved in double (8 bytes). Old PAUS data must also be transposed
if ~isfield(handles.param,'PAUSDataDivisor'); handles.param.PAUSDataDivisor = 0; end %Old data is not divided
if ~isfield(handles.param,'gearRatio'); handles.param.gearRatio = 2; end

if ~isfield(handles.acqInfo,'acqInterrupted'); handles.acqInfo.acqInterrupted = 0; end %Make sure "acqInterrupted" exists
if handles.acqInfo.acqInterrupted == 1
    set(handles.lbProblems, 'String', strvcat(get(handles.lbProblems, 'String'), 'Results are not from a complete acquisition.'))
    set(handles.lbProblems,'Value',size(get(handles.lbProblems, 'String'),1)) %Go to the last value in the list
end
if ~isfield(handles.acqInfo,'idxTurnTotal'); handles.acqInfo.idxTurnTotal = handles.acqInfo.idxTurn; handles.acqInfo = rmfield(handles.acqInfo,'idxTurn'); end
if ~isfield(handles.displayInfo,'infoDisplayOn'); handles.displayInfo.infoDisplayOn = 1; end
if ~isfield(handles.displayInfo,'depthToDisplay'); handles.displayInfo.depthToDisplay = handles.param.depthUltrasound; end
if ~isfield(handles.displayInfo,'samplesToDisplay'); handles.displayInfo.nSamplesToDisplay = round(handles.displayInfo.depthToDisplay/handles.param.speedUltrasound*handles.param.transducerAcquisitionFreq); end
if ~isfield(handles.displayInfo,'fluoDisplayFull2D'); handles.displayInfo.fluoDisplayFull2D = 0; end
if ~isfield(handles.displayInfo,'USDisplayLineTime'); handles.displayInfo.USDisplayLineTime = 0; end

if ~isfield(handles.param,'fluoSingleMeasureOn'); handles.param.fluoSingleMeasureOn = boolean(0); end
if ~isfield(handles.param,'useLinearMotor'); handles.param.useLinearMotor = boolean(1); end
if isfield(handles.param,'PAPositionsPerTurn'); handles.param.PAPositionsPerTurn = handles.param.PAPositionsPerTurn; handles.param = rmfield(handles.param,'PAPositionsPerTurn'); end %Renamed
if ~isfield(handles.param,'PAPositionsPerTurn'); handles.param.PAPositionsPerTurn = 32; end

if isfield(handles.displayInfo,'realDepthUltrasound'); handles.displayInfo = rmfield(handles.displayInfo,'realDepthUltrasound'); end
if isfield(handles.displayInfo,'realViewDistance'); handles.displayInfo = rmfield(handles.displayInfo,'realViewDistance'); end

if ~isfield(handles.displayInfo,'displayGain') && ~isfield(handles.displayInfo,'displayGainUS'); handles.displayInfo.displayGain = -20; end
if ~isfield(handles.displayInfo,'displayDynamicRange') && ~isfield(handles.displayInfo,'displayDynamicRangeUS'); handles.displayInfo.displayDynamicRange = 25; end
if isfield(handles.displayInfo,'displayGain'); 
    handles.displayInfo.displayGainPA = handles.displayInfo.displayGain;
    handles.displayInfo.displayGainUS = handles.displayInfo.displayGain;
    handles.displayInfo.displayGainFluo = handles.displayInfo.displayGain;
    handles.displayInfo = rmfield(handles.displayInfo,'displayGain'); 
end
if isfield(handles.displayInfo,'displayDynamicRange'); 
    handles.displayInfo.displayDynamicRangePA = handles.displayInfo.displayDynamicRange; 
    handles.displayInfo.displayDynamicRangeUS = handles.displayInfo.displayDynamicRange; 
    handles.displayInfo.displayDynamicRangeFluo = handles.displayInfo.displayDynamicRange; 
    handles.displayInfo = rmfield(handles.displayInfo,'displayDynamicRange'); 
end

if ~isfield(handles.displayInfo,'displayMultipleTurns'); 
    handles.displayInfo.displayMultipleTurns = 'All'; 
end

if ~isfield(handles.displayInfo,'PADisplayOn'); handles.displayInfo.PADisplayOn = 1; end
if ~isfield(handles.displayInfo,'fluoDisplayOn'); handles.displayInfo.fluoDisplayOn = 1; end
if ~isfield(handles.displayInfo,'USFluo'); handles.displayInfo.USFluo = 0; end
if ~isfield(handles.displayInfo,'PAUndock'); handles.displayInfo.PAUndock = 0; end
if ~isfield(handles.displayInfo,'USUndock'); handles.displayInfo.USUndock = 0; end
if ~isfield(handles.displayInfo,'fluoUndock'); handles.displayInfo.fluoUndock = 0; end

if ~isfield(handles.param,'PAUSInitialGain')
    handles.param.PAUSInitialGain = round(handles.param.transducerGain/4095*50+25);
end
if ~isfield(handles.param,'TGC1')
    handles.param.TGC1 = 0;
    handles.param.TGC2 = 0;
    handles.param.TGC3 = 0;
    handles.param.TGC4 = 0;
end

if ~isfield(handles.displayInfo,'fluoRemoveBaseline'); 
    handles.displayInfo.fluoRemoveBaseline = 0;
end
set(handles.cbRemoveFluoBase,'UserData', '0');
set(handles.rbFluoFull2D,'UserData', '0');
set(handles.rbUSLineTime,'UserData', '0');

if ~isfield(handles.acqInfo,'TGCChangeDuringAcquisition'); 
    handles.acqInfo.TGCChangeDuringAcquisition = 0;
end

if ~isfield(handles.param,'constantPullback'); handles.param.constantPullback = 0; end

if ~isfield(handles.displayInfo,'PADisplay2DCart'); handles.displayInfo.PADisplay2DCart = 0; end
if ~isfield(handles.displayInfo,'USDisplay2DCart'); handles.displayInfo.USDisplay2DCart = 0; end

if ~isfield(handles.displayInfo,'enableDownSample'); handles.displayInfo.enableDownSample = 0; end

if ~isfield(handles.param,'PAUSPreampGain'); param.PAUSPreampGain = 30; param.PAUSMinGain = param.PAUSPreampGain+5; param.PAUSMaxGain = param.PAUSMinGain+50; end

%Don't use the filename written in the file, because it might not be the right path
handles.acqInfo.outputFullFileName = [pathname filename];
handles.acqInfo.outputFullFileName = strrep(handles.acqInfo.outputFullFileName, '.mat', '');

%Check if errors were found. If not, tell the user
if max(cell2mat(results.error)) == 0
    set(handles.lbProblems, 'String', strvcat(get(handles.lbProblems, 'String'), 'No problems found.'))
    set(handles.lbProblems,'Value',size(get(handles.lbProblems, 'String'),1))
end
    
%Restore the parameters on the GUI
    set(handles.etPALPF,'String',handles.displayInfo.PALowPassF); %Cutoff frequency of the low-pass filter in Hz
    set(handles.etPAHPF,'String',handles.displayInfo.PAHighPassF); %Cutoff frequency of the high-pass filter in Hz
    set(handles.etUSLPF,'String',handles.displayInfo.USLowPassF); %Cutoff frequency of the low-pass filter in Hz
    set(handles.etUSHPF,'String',handles.displayInfo.USHighPassF); %Cutoff frequency of the high-pass filter in Hz
    set(handles.etFluoLPF,'String',handles.displayInfo.fluoLowPassF); %Cutoff frequency of the low-pass filter in Hz
    set(handles.etFluoHPF,'String',handles.displayInfo.fluoHighPassF); %Cutoff frequency of the high-pass filter in Hz
    set(handles.etNPixels,'String',handles.displayInfo.nPixels); %Number of pixels in each direction for the 2D display
    set(handles.cbPAHilbert,'Value',handles.displayInfo.PAHilbert); %Apply Hilbert transform to PA data
    set(handles.cbUSHilbert,'Value',handles.displayInfo.USHilbert); %Apply Hilbert transform to US data
    set(handles.etHideRadius,'String',handles.displayInfo.radiusCatheterFlush); %Radius of the transducer from the rotating axe. It is used to add samples in the results to have the correct scale when reconstructing 2D images
    set(handles.etTurnsSlice,'String',handles.param.nTurnsPerSlice); %Number of turns per slice of acquisition. The signal is averaged for all turns
    set(handles.etNSlices,'String',handles.param.nSlices); %Number of slices in an acquisition
    set(handles.etDistanceSlice,'String',1000*handles.param.distanceBetweenSlices); %Distance between two slices (in meters)
    set(handles.cbFilterWrapFluo,'Value',handles.displayInfo.filterWrapFluo); %Filter wrap (On/Off)
    set(handles.cbInfoDisplayOn,'Value',handles.displayInfo.infoDisplayOn); %Info Display (On/Off)
    set(handles.cbPADisplayOn,'Value',handles.displayInfo.PADisplayOn); %Info Display (On/Off)
    set(handles.cbFluoDisplayOn,'Value',handles.displayInfo.fluoDisplayOn); %Info Display (On/Off)
    set(handles.cbUSFluo,'Value',handles.displayInfo.USFluo); %Info Display (On/Off)
    set(handles.cbPAUndock,'Value',handles.displayInfo.PAUndock); %Dock Display (On/Off)
%     set(handles.cbUSUndock,'Value',handles.displayInfo.USUndock); %Dock Display (On/Off)
    set(handles.cbFluoUndock,'Value',handles.displayInfo.fluoUndock); %Dock Display (On/Off)
    
    set(handles.etPAUSInitialGain,'String',handles.param.PAUSInitialGain); %PAUS initial gain

    set(handles.slDisplayGainPA,'Value',handles.displayInfo.displayGainPA);
    set(handles.slDisplayDynamicRangePA,'Value',handles.displayInfo.displayDynamicRangePA);
    set(handles.etDisplayGainPA,'Enable','on'); set(handles.etDisplayGainPA,'String',handles.displayInfo.displayGainPA); set(handles.etDisplayGainPA,'Enable','off');
    set(handles.etDisplayDynamicRangePA,'Enable','on'); set(handles.etDisplayDynamicRangePA,'String',handles.displayInfo.displayDynamicRangePA); set(handles.etDisplayDynamicRangePA,'Enable','off');
    set(handles.slDisplayGainUS,'Value',handles.displayInfo.displayGainUS);
    set(handles.slDisplayDynamicRangeUS,'Value',handles.displayInfo.displayDynamicRangeUS);
    set(handles.etDisplayGainUS,'Enable','on'); set(handles.etDisplayGainUS,'String',handles.displayInfo.displayGainUS); set(handles.etDisplayGainUS,'Enable','off');
    set(handles.etDisplayDynamicRangeUS,'Enable','on'); set(handles.etDisplayDynamicRangeUS,'String',handles.displayInfo.displayDynamicRangeUS); set(handles.etDisplayDynamicRangeUS,'Enable','off');    
    set(handles.slDisplayGainFluo,'Value',handles.displayInfo.displayGainFluo);
    set(handles.slDisplayDynamicRangeFluo,'Value',handles.displayInfo.displayDynamicRangeFluo);
    set(handles.etDisplayGainFluo,'Enable','on'); set(handles.etDisplayGainFluo,'String',handles.displayInfo.displayGainFluo); set(handles.etDisplayGainFluo,'Enable','off');
    set(handles.etDisplayDynamicRangeFluo,'Enable','on'); set(handles.etDisplayDynamicRangeFluo,'String',handles.displayInfo.displayDynamicRangeFluo); set(handles.etDisplayDynamicRangeFluo,'Enable','off');
    if (strcmp(handles.displayInfo.displayMultipleTurns,'All'))
        set(handles.pmDisplayMultipleTurns,'Value',1);
    elseif (strcmp(handles.displayInfo.displayMultipleTurns,'Once'))
        set(handles.pmDisplayMultipleTurns,'Value',2);        
    else
        set(handles.pmDisplayMultipleTurns,'Value',3);
    end    
    
    set(handles.cbRemoveFluoBase,'Enable','on');
    if isfield(handles.displayInfo,'fluoBaseline') %Check if there is a baseline
        set(handles.cbRemoveFluoBase,'Value',1);
        set(handles.cbRemoveFluoBase,'UserData','1');
    else
        set(handles.cbRemoveFluoBase,'Value',0);
        set(handles.cbRemoveFluoBase,'UserData','0');        
    end
    
    %Restore TGC
    set(handles.slTGC1,'Value',handles.param.TGC1);
    set(handles.slTGC2,'Value',handles.param.TGC2);
    set(handles.slTGC3,'Value',handles.param.TGC3);
    set(handles.slTGC4,'Value',handles.param.TGC4);
    set(handles.etTGC1,'Enable','on'); set(handles.etTGC1,'String',handles.param.TGC1); set(handles.etTGC1,'Enable','off');
    set(handles.etTGC2,'Enable','on'); set(handles.etTGC2,'String',handles.param.TGC2); set(handles.etTGC2,'Enable','off');
    set(handles.etTGC3,'Enable','on'); set(handles.etTGC3,'String',handles.param.TGC3); set(handles.etTGC3,'Enable','off');
    set(handles.etTGC4,'Enable','on'); set(handles.etTGC4,'String',handles.param.TGC4); set(handles.etTGC4,'Enable','off');
    
    if (strcmp(handles.displayInfo.displayTypePA,'Linear'))
        set(handles.pmDisplayTypePA,'Value',1); %Check which display type the user wants (Log/Normal)  
    else
        set(handles.pmDisplayTypePA,'Value',2); %Check which display type the user wants (Log/Normal)  
    end
    if (strcmp(handles.displayInfo.displayTypeUS,'Linear'))
        set(handles.pmDisplayTypeUS,'Value',1); %Check which display type the user wants (Log/Normal)  
    else
        set(handles.pmDisplayTypeUS,'Value',2); %Check which display type the user wants (Log/Normal)  
    end
    if (strcmp(handles.displayInfo.displayTypeFluo,'Linear'))
        set(handles.pmDisplayTypeFluo,'Value',1); %Check which display type the user wants (Log/Normal)  
    else
        set(handles.pmDisplayTypeFluo,'Value',2); %Check which display type the user wants (Log/Normal)  
    end    
    set(handles.rbPARF,'Value',handles.displayInfo.PADisplaySignal); %Check which signal type to display (Signal/2D)
    set(handles.rbUSRF,'Value',handles.displayInfo.USDisplaySignal); %Check which signal type to display (Signal/2D)
    set(handles.rbPA2DCart,'Value',handles.displayInfo.PADisplay2DCart); %Check which signal type to display (Signal/2D)
    set(handles.rbUS2DCart,'Value',handles.displayInfo.USDisplay2DCart); %Check which signal type to display (Signal/2D)
    set(handles.rbFluoSignal,'Value',handles.displayInfo.fluoDisplaySignal); %Check which signal type to display (Signal/2D)
    set(handles.rbPA2D,'Value',~(handles.displayInfo.PADisplaySignal||handles.displayInfo.PADisplay2DCart)); %Check which signal type to display (Signal/2D)
    set(handles.rbUS2D,'Value',~(handles.displayInfo.USDisplaySignal||handles.displayInfo.USDisplayLineTime||handles.displayInfo.USDisplay2DCart)); %Check which signal type to display (Signal/2D)
    set(handles.rbUSLineTime,'Value',handles.displayInfo.USDisplayLineTime); %Check which signal type to display (Signal/2D)
    set(handles.rbFluo2D,'Value',~(handles.displayInfo.fluoDisplaySignal||handles.displayInfo.fluoDisplayFull2D)); %Check which signal type to display (Signal/2D)
    set(handles.rbFluoFull2D,'Value',handles.displayInfo.fluoDisplayFull2D); %Check which signal type to display (Signal/2D)
    
    %Set the popup menu of the pullback
    if handles.acqInfo.acqInterrupted == 0
        nSlices = handles.param.nSlices;
    else
        nSlices = floor(floor(handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn)/handles.param.nTurnsPerSlice);
    end
    for idxSlice = 1:nSlices
        distance = handles.param.distanceFirstSlice+(idxSlice-1)*handles.param.distanceBetweenSlices;
        p = [char(num2str(distance)) 'm'];
        pullbackPosition(idxSlice) = {p};
    end
    if nSlices > 0
        set(handles.pmPAPullback,'String',pullbackPosition); 
        set(handles.pmUSPullback,'String',pullbackPosition); 
        set(handles.pmFluoPullback,'String',pullbackPosition); 
    end
    
    %Restore slider
    if nSlices >= 1
        set(handles.slSlice,'Enable','off'); %Disable slider to make sure the change of the slider value doesn't call a display
        if nSlices > 1
            set(handles.slSlice,'Min',1);set(handles.slSlice,'Max',nSlices);set(handles.slSlice,'Value',1);
            set(handles.slSlice,'SliderStep',[1/(nSlices-1) 1/(nSlices-1)]);
        else
            set(handles.slSlice,'Min',0.9);set(handles.slSlice,'Max',nSlices);set(handles.slSlice,'Value',1);
            set(handles.slSlice,'SliderStep',[1 1]);
        end
        set(handles.slSlice,'Enable','on'); %Re-enable the slider
    end
    
    %Check which pullback position the user wants to display
    set(handles.pmPAPullback,'Value',handles.displayInfo.PAPullback);
    set(handles.pmUSPullback,'Value',handles.displayInfo.USPullback);
    set(handles.pmFluoPullback,'Value',handles.displayInfo.fluoPullback);
    
    %Check which pullback position the user wants to display
    set(handles.pmPAPullback,'Value',handles.displayInfo.PAPullback);
    set(handles.pmUSPullback,'Value',handles.displayInfo.USPullback);
    set(handles.pmFluoPullback,'Value',handles.displayInfo.fluoPullback);
    
    %Check which RF signal angular position the user wants to display (in PA and US)
    set(handles.pmPAAngle,'Value',handles.displayInfo.PAAngle);
    set(handles.pmUSAngle,'Value',handles.displayInfo.USAngle);
    %SD pair the user wants to display
    set(handles.pmFluoSDPair,'Value',handles.displayInfo.SDPair);
    %Check which information the user wants to display
    if strcmp(handles.displayInfo.infoToDisplay,'Angular synchronization')
        set(handles.pmInfo,'Value',1);    
    elseif strcmp(handles.displayInfo.infoToDisplay,'Transfer synchronization')
        set(handles.pmInfo,'Value',2); 
    elseif strcmp(handles.displayInfo.infoToDisplay,'Motor voltage')
        set(handles.pmInfo,'Value',3); 
    elseif strcmp(handles.displayInfo.infoToDisplay,'Fluorescence')
        set(handles.pmInfo,'Value',4);    
    elseif strcmp(handles.displayInfo.infoToDisplay,'Photoacoustic Monitor')
        set(handles.pmInfo,'Value',5);               
    else
        set(handles.pmInfo,'Value',6);
    end

    if isfield(file,'userComments') %Old structure
        handles.acqInfo.userComments = file.userComments;
        set(handles.etComments,'String',file.userComments);        
    elseif isfield(handles.acqInfo,'userComments') %New structure, August 5, 2011
        set(handles.etComments,'String',handles.acqInfo.userComments);
    end
    set(handles.etFileName,'String',handles.acqInfo.outputFileName); 
    
    %Set to the inverse of the value, will be inverted again
    set(handles.cbConstantPullback,'Value',~handles.param.constantPullback);
    cbConstantPullback_Callback(hObject, eventdata, handles);    
    
%Done restoring the GUI
    
handles.displayInfo.updateAxisInformation = 1;
handles.displaying = 0; %Prevent changes of some buttons (done)
set(handles.pbRefreshDisplay, 'UserData', 0); %Clear display queue
setappdata(handles.figure1,'results',results);
ProcessData(handles, hObject)
set(handles.pbSavePA, 'Enable', 'on')
set(handles.pbSaveUS, 'Enable', 'on')
set(handles.pbSaveFluo, 'Enable', 'on')
set(handles.pbSavePAVideo, 'Enable', 'on')
set(handles.pbSaveUSVideo, 'Enable', 'on')
set(handles.pbSaveFluoVideo, 'Enable', 'on')
guidata(hObject, handles); % Update handles at the end of a function 


function rbPARF_Callback(hObject, eventdata, handles)
set(handles.rbPARF,'Value',1)
set(handles.rbPA2D,'Value',0)
set(handles.rbPA2DCart,'Value',0)
set(handles.pmPAAngle,'Enable','on')

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function rbPA2D_Callback(hObject, eventdata, handles)
set(handles.rbPARF,'Value',0)
set(handles.rbPA2D,'Value',1)
set(handles.rbPA2DCart,'Value',0)
set(handles.pmPAAngle,'Enable','off')

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function pmPAPullback_Callback(hObject, eventdata, handles)
if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function pmPAAngle_Callback(hObject, eventdata, handles)
if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function rbUSRF_Callback(hObject, eventdata, handles)
set(handles.rbUSRF,'Value',1)
set(handles.rbUS2D,'Value',0)
set(handles.rbUS2DCart,'Value',0)
set(handles.rbUSLineTime,'Value',0)
set(handles.pmUSAngle,'Enable','on')

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function rbUS2D_Callback(hObject, eventdata, handles)
set(handles.rbUSRF,'Value',0)
set(handles.rbUS2D,'Value',1)
set(handles.rbUS2DCart,'Value',0)
set(handles.rbUSLineTime,'Value',0)
set(handles.pmUSAngle,'Enable','off')

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function pmUSPullback_Callback(hObject, eventdata, handles)
if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function pmUSAngle_Callback(hObject, eventdata, handles)
if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function rbFluoSignal_Callback(hObject, eventdata, handles)
set(handles.rbFluoSignal,'Value',1)
set(handles.rbFluo2D,'Value',0)
set(handles.rbFluoFull2D,'Value',0)

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function rbFluo2D_Callback(hObject, eventdata, handles)
set(handles.rbFluoSignal,'Value',0)
set(handles.rbFluo2D,'Value',1)
set(handles.rbFluoFull2D,'Value',0)

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function pmFluoPullback_Callback(hObject, eventdata, handles)
if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function pmInfo_Callback(hObject, eventdata, handles)
set(handles.pmInfo, 'UserData', '1'); %Used to inform the display function to update axis information

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function   


function pmFluoSDPair_Callback(hObject, eventdata, handles)
if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end


function pbSavePA_Callback(hObject, eventdata, handles)
if handles.acqInfo.acqRunning == 0 && handles.displaying == 0
    saveImageGetInfo(handles, hObject);
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
    handles.saveInfo.signalType = 'PA';    
    ProcessData(handles, hObject,1) %Refresh display and save PA
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
    guidata(hObject, handles); % Update handles at the end of a function
end

function pbSaveUS_Callback(hObject, eventdata, handles)
if handles.acqInfo.acqRunning == 0 && handles.displaying == 0
    saveImageGetInfo(handles, hObject);
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
    handles.saveInfo.signalType = 'US';
    ProcessData(handles, hObject,1) %Refresh display and save US
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
    guidata(hObject, handles); % Update handles at the end of a function
end


function pbSaveFluo_Callback(hObject, eventdata, handles)
if handles.acqInfo.acqRunning == 0 && handles.displaying == 0
    saveImageGetInfo(handles, hObject);
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
    handles.saveInfo.signalType = 'Fluo';
    ProcessData(handles, hObject,1) %Refresh display and save fluo
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
    guidata(hObject, handles); % Update handles at the end of a function
end


function saveImageGetInfo(handles, hObject)
handles.saveInfo.saveType = 'Image'; %We are saving an image, not a video
handles.saveInfo.overwrite = 0;
%Ask the user which turn to save, except if there is only one display
if ~(strcmp(handles.displayInfo.displayMultipleTurns,'Once') || handles.param.nTurnsPerSlice == 1)
    options.WindowStyle='normal'; %Used to see dialogs that are under the undocked figure
    handles.saveInfo.turnToSave = str2num(char(inputdlg('Enter which turns to save.','Turn selection',1,{'[1:1]'},options)));
else
    handles.saveInfo.turnToSave = handles.idxTurn;
end
handles.saveInfo.saveCorrespondingData = questdlgNonModal('Do you want to save all corresponding data? (If not, only the image file will be saved)','Save data','Yes','No','Yes');

if max(ismember(handles.saveInfo.turnToSave,1:handles.param.nTurnsPerSlice)) == 0
    warndlg('Turn selection doesn''t contain existing turns.','Invalid turn selection','non-modal')            
end 
guidata(hObject, handles); % Update handles at the end of a function


function etDistanceSlice_Callback(hObject, eventdata, handles)
cP = get(handles.cbConstantPullback,'Value');

if handles.acqInfo.acqRunning == 0 && handles.displaying == 0 && cP
    set(handles.etNSlices,'Enable','on')
    totalDistance = str2num(get(handles.etDistanceSlice,'String'))/1000 - handles.param.distanceFirstSlice;
    set(handles.etNSlices,'String',num2str(floor(handles.param.speedMotorTurnsPerSec/(handles.param.linearMotorSpeed/10)*totalDistance*1000)))
    set(handles.etNSlices,'Enable','off')
end


function pbRefreshDisplay_Callback(hObject, eventdata, handles)
if handles.acqInfo.acqRunning == 0
    %Reset
    handles.displaying = 0;
    %Recalculate fluorescence baseline and fluo full 2D data
    set(handles.cbRemoveFluoBase,'UserData','0');
    set(handles.rbFluoFull2D,'UserData','0');
    set(handles.rbUSLineTime,'UserData', '0');
    
    handles.displayInfo.updateAxisInformation = 1;
    ProcessData(handles, hObject) %Refresh display
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
    guidata(hObject, handles); % Update handles at the end of a function
end


function cbInfoDisplayOn_Callback(hObject, eventdata, handles)
%Don't remove this line:
handles = guidata(hObject); % Retrieve the updated handle after calling a function  

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function 


function pbPause_Callback(hObject, eventdata, handles)
%Put a breakpoint here to pause
pause(0.1)
set(handles.pbPause, 'UserData', '1')


function slSliceMove(hObject, eventdata, handles)
%Don't remove this line:
handles = guidata(hObject); % Retrieve the updated handle after calling a function  

if strcmp(get(handles.slSlice,'Enable'),'on')
    if ~isfield(handles,'displaying'); handles.displaying=0; end
    if handles.acqInfo.acqRunning == 0
        if handles.displaying == 0
            slider = round(get(handles.slSlice,'Value'));
            %Check if the slider changed position
            if slider ~= get(handles.pmPAPullback,'Value') || slider ~= get(handles.pmUSPullback,'Value') || slider ~= get(handles.pmFluoPullback,'Value')
                %disp('Update display')
                set(handles.pmPAPullback,'Value',slider);
                set(handles.pmUSPullback,'Value',slider);
                set(handles.pmFluoPullback,'Value',slider);            
                ProcessData(handles, hObject)
                handles = guidata(hObject); % Retrieve the updated handle after calling a function  
            end
        else
            %Currently displaying, queue display
            set(handles.pbRefreshDisplay, 'UserData', 2); %2 is a code to look at the slider
        end
    end
end

guidata(hObject, handles); % Update handles at the end of a function 


function rbFluoFull2D_Callback(hObject, eventdata, handles)
if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0 
    if handles.displaying == 0
        set(handles.rbFluoSignal,'Value',0)
        set(handles.rbFluo2D,'Value',0)
        set(handles.rbFluoFull2D,'Value',1)

        set(handles.rbFluoFull2D,'UserData','0'); %This values is at 1 when the fluo full 2D data is valid
        if ~get(handles.rbFluoFull2D,'Value')
            %Remove full 2D 
            results = getappdata(handles.figure1,'results');
            if isfield(results,'fluoFull2D')
                results = rmfield(results,'fluoFull2D');
            end
            setappdata(handles.figure1,'results',results);
        end
        ProcessData(handles,hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function     
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 3); %3 is a code to do full 2D        
    end
else
    %FluoFull2D cannot be done while acquiring
    oldValue = ~(boolean(get(handles.rbFluoSignal,'Value')) || boolean(get(handles.rbFluo2D,'Value')));
    set(handles.rbFluoFull2D,'Value',oldValue); %Reset checkbox
end
guidata(hObject, handles); % Update handles at the end of a function  


function cbRemoveFluoBase_Callback(hObject, eventdata, handles)
if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0 && handles.displaying == 0
    %Recalculate fluorescence baseline and fluo full 2D data
    set(handles.cbRemoveFluoBase,'UserData','0');
    set(handles.rbFluoFull2D,'UserData','0');
    if ~(get(handles.cbRemoveFluoBase,'Value'))     
        %Remove baseline 
        if isfield(handles.displayInfo,'fluoBaseline')
            handles.displayInfo = rmfield(handles.displayInfo,'fluoBaseline');
            handles.displayInfo = rmfield(handles.displayInfo,'sliceBaseline');
        end 
    end
    ProcessData(handles,hObject)
    handles = guidata(hObject); % Retrieve the updated handle after calling a function      
    guidata(hObject, handles); % Update handles at the end of a function  
else
    set(handles.cbRemoveFluoBase,'Value',~get(handles.cbRemoveFluoBase,'Value')) %Reset checkbox
end


function etPAUSInitialGain_Callback(hObject, eventdata, handles)
handles.param.PAUSInitialGain = str2num(get(handles.etPAUSInitialGain,'String'));
guidata(hObject, handles); % Update handles at the end of a function 


function cbChangeTGC_Callback(hObject, eventdata, handles)
if get(handles.cbChangeTGC,'Value') == 1
    set(handles.slTGC1,'Enable','on');
    set(handles.slTGC2,'Enable','on');
    set(handles.slTGC3,'Enable','on');
    set(handles.slTGC4,'Enable','on');
else
    set(handles.slTGC1,'Enable','off');
    set(handles.slTGC2,'Enable','off');
    set(handles.slTGC3,'Enable','off');
    set(handles.slTGC4,'Enable','off');    
end


function slTGCMove(hObject, eventdata, handles)
%Don't remove this line:
handles = guidata(hObject); % Retrieve the updated handle after calling a function  

handles.param.TGC1 = 0.2*round(5*get(handles.slTGC1,'Value'));
handles.param.TGC2 = 0.2*round(5*get(handles.slTGC2,'Value'));
handles.param.TGC3 = 0.2*round(5*get(handles.slTGC3,'Value'));
handles.param.TGC4 = 0.2*round(5*get(handles.slTGC4,'Value'));

set(handles.etTGC1,'Enable','on'); set(handles.etTGC1,'String',handles.param.TGC1); set(handles.etTGC1,'Enable','off');
set(handles.etTGC2,'Enable','on'); set(handles.etTGC2,'String',handles.param.TGC2); set(handles.etTGC2,'Enable','off');
set(handles.etTGC3,'Enable','on'); set(handles.etTGC3,'String',handles.param.TGC3); set(handles.etTGC3,'Enable','off');
set(handles.etTGC4,'Enable','on'); set(handles.etTGC4,'String',handles.param.TGC4); set(handles.etTGC4,'Enable','off');

%Write new TGC values to the FPGA

guidata(hObject, handles); % Update handles at the end of a function 


function slDisplayMove(hObject, eventdata, handles)
%Don't remove this line:
handles = guidata(hObject); % Retrieve the updated handle after calling a function  
displayGainPA = round(get(handles.slDisplayGainPA,'Value'));
displayDynamicRangePA = round(get(handles.slDisplayDynamicRangePA,'Value'));
displayGainUS = round(get(handles.slDisplayGainUS,'Value'));
displayDynamicRangeUS = round(get(handles.slDisplayDynamicRangeUS,'Value'));
displayGainFluo = round(get(handles.slDisplayGainFluo,'Value'));
displayDynamicRangeFluo = round(get(handles.slDisplayDynamicRangeFluo,'Value'));

set(handles.etDisplayGainPA,'Enable','on'); set(handles.etDisplayGainPA,'String',displayGainPA); set(handles.etDisplayGainPA,'Enable','off');
set(handles.etDisplayDynamicRangePA,'Enable','on'); set(handles.etDisplayDynamicRangePA,'String',displayDynamicRangePA); set(handles.etDisplayDynamicRangePA,'Enable','off');
set(handles.etDisplayGainUS,'Enable','on'); set(handles.etDisplayGainUS,'String',displayGainUS); set(handles.etDisplayGainUS,'Enable','off');
set(handles.etDisplayDynamicRangeUS,'Enable','on'); set(handles.etDisplayDynamicRangeUS,'String',displayDynamicRangeUS); set(handles.etDisplayDynamicRangeUS,'Enable','off');
set(handles.etDisplayGainFluo,'Enable','on'); set(handles.etDisplayGainFluo,'String',displayGainFluo); set(handles.etDisplayGainFluo,'Enable','off');
set(handles.etDisplayDynamicRangeFluo,'Enable','on'); set(handles.etDisplayDynamicRangeFluo,'String',displayDynamicRangeFluo); set(handles.etDisplayDynamicRangeFluo,'Enable','off');

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function 


function cbPADisplayOn_Callback(hObject, eventdata, handles)
%Don't remove this line:
handles = guidata(hObject); % Retrieve the updated handle after calling a function  

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function 


function cbFluoDisplayOn_Callback(hObject, eventdata, handles)
%Don't remove this line:
handles = guidata(hObject); % Retrieve the updated handle after calling a function  

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function 


function pbSaveUSVideo_Callback(hObject, eventdata, handles)
if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0 && handles.displaying == 0
    handles.savingVideo = 1;
    saveVideoGetInfo(handles, hObject);
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
    handles.saveInfo.signalType = 'US';
    for idxSlice = handles.saveInfo.sliceToSave
        if ismember(idxSlice,1:handles.param.nSlices)
            set(handles.pmPAPullback,'Value',idxSlice); %Change slice of PA display
            set(handles.pmUSPullback,'Value',idxSlice); %Change slice of US display
            set(handles.pmFluoPullback,'Value',idxSlice); %Change slice of Fluo display
            set(handles.slSlice,'Value',idxSlice);
            ProcessData(handles, hObject,1) %Refresh display and save US
            handles = guidata(hObject); % Retrieve the updated handle after calling a function
        end
    end
    handles.savingVideo = 0;
    guidata(hObject, handles); % Update handles at the end of a function
end


function cbUSFluo_Callback(hObject, eventdata, handles)
%Don't remove this line:
handles = guidata(hObject); % Retrieve the updated handle after calling a function  

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end

guidata(hObject, handles); % Update handles at the end of a function 


function cbUSUndock_Callback(hObject, eventdata, handles)
%Don't remove this line:
handles = guidata(hObject); % Retrieve the updated handle after calling a function  

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end    
end
guidata(hObject, handles); % Update handles at the end of a function 


function cbPAUndock_Callback(hObject, eventdata, handles)
%Don't remove this line:
handles = guidata(hObject); % Retrieve the updated handle after calling a function  

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end    
end
guidata(hObject, handles); % Update handles at the end of a function 


function cbFluoUndock_Callback(hObject, eventdata, handles)
%Don't remove this line:
handles = guidata(hObject); % Retrieve the updated handle after calling a function  

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end    
end
guidata(hObject, handles); % Update handles at the end of a function 


function rbUSLineTime_Callback(hObject, eventdata, handles)
set(handles.rbUSRF,'Value',0)
set(handles.rbUS2D,'Value',0)
set(handles.rbUS2DCart,'Value',0)
set(handles.rbUSLineTime,'Value',1)
set(handles.pmUSAngle,'Enable','on')

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function pbSavePAVideo_Callback(hObject, eventdata, handles)
if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0 && handles.displaying == 0
    handles.savingVideo = 1;
    saveVideoGetInfo(handles, hObject);
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
    handles.saveInfo.signalType = 'PA';
    for idxSlice = handles.saveInfo.sliceToSave
        if ismember(idxSlice,1:handles.param.nSlices)
            set(handles.pmPAPullback,'Value',idxSlice); %Change slice of PA display
            set(handles.pmUSPullback,'Value',idxSlice); %Change slice of US display
            set(handles.pmFluoPullback,'Value',idxSlice); %Change slice of Fluo display
            set(handles.slSlice,'Value',idxSlice);
            ProcessData(handles, hObject,1) %Refresh display and save PA
            handles = guidata(hObject); % Retrieve the updated handle after calling a function
        end
    end
    handles.savingVideo = 0;
    guidata(hObject, handles); % Update handles at the end of a function
end


function pbSaveFluoVideo_Callback(hObject, eventdata, handles)
if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0 && handles.displaying == 0
    if get(handles.rbFluoFull2D,'Value') == 1
        warndlg('Fluo full 2D cannot be saved as a video.','Cannot save fluo video','non-modal') 
        guidata(hObject, handles); % Update handles at the end of a function
        return
    end
    handles.savingVideo = 1;
    saveVideoGetInfo(handles, hObject);
    handles = guidata(hObject); % Retrieve the updated handle after calling a function
    handles.saveInfo.signalType = 'Fluo';
    for idxSlice = handles.saveInfo.sliceToSave
        if ismember(idxSlice,1:handles.param.nSlices)
            set(handles.pmPAPullback,'Value',idxSlice); %Change slice of PA display
            set(handles.pmUSPullback,'Value',idxSlice); %Change slice of US display
            set(handles.pmFluoPullback,'Value',idxSlice); %Change slice of Fluo display
            set(handles.slSlice,'Value',idxSlice);
            ProcessData(handles, hObject,1) %Refresh display and save fluo
            handles = guidata(hObject); % Retrieve the updated handle after calling a function
        end
    end
    handles.savingVideo = 0;
    guidata(hObject, handles); % Update handles at the end of a function
end


function saveVideoGetInfo(handles, hObject)
handles.saveInfo.VideoDataTemp = []; %Clear video data
handles.saveInfo.saveType = 'Video'; %We are saving an image, not a video
handles.saveInfo.overwrite = 0;
%Ask the user which turn to save, except if there is only one display
if ~(strcmp(handles.displayInfo.displayMultipleTurns,'Once') || handles.param.nTurnsPerSlice == 1)
    options.WindowStyle='normal'; %Used to see dialogs that are under the undocked figure
    handles.saveInfo.turnToSave = str2num(char(inputdlg('Enter which turns to save.','Turn selection',1,{'[1:1]'},options)));
else
    handles.saveInfo.turnToSave = handles.param.nTurnsPerSlice;
end
%Ask the user which slice to save
if ~(handles.param.nSlices == 1)
    options.WindowStyle='normal'; %Used to see dialogs that are under the undocked figure
    handles.saveInfo.sliceToSave = str2num(char(inputdlg('Enter which slice to save.','Slice selection',1,{'[1:1]'},options)));
else
    handles.saveInfo.sliceToSave = 1;
end

%Ask the user which FPS is wanted
handles.saveInfo.videoFps = (str2num(char(inputdlg('Enter the frame per second of the video (Default is real time, divide by 2 to slow down the video).','FPS selection',1,{num2str(handles.param.speedMotorTurnsPerSec)},options))));

handles.saveInfo.saveCorrespondingData = questdlgNonModal('Do you want to save all corresponding data? (If not, only the image file will be saved)','Save data','Yes','No','Yes');

[memberTurn memberTurnIdx] = ismember(handles.saveInfo.turnToSave,1:handles.param.nTurnsPerSlice);
[memberSlice memberSliceIdx] = ismember(handles.saveInfo.sliceToSave,1:handles.param.nSlices);
if max(memberTurn) == 0
    warndlg('Turn selection doesn''t contain existing turns.','Invalid turn selection','non-modal')            
end 
if max(memberSlice) == 0
    warndlg('Slice selection doesn''t contain existing slices.','Invalid slice selection','non-modal')            
end 

%We have to find the last slice and last turn, because the video will be
%saved at this moment
handles.saveInfo.lastTurnToSave = max(memberTurnIdx);
handles.saveInfo.lastSliceToSave = max(memberSliceIdx);

guidata(hObject, handles); % Update handles at the end of a function


function cbConstantPullback_Callback(hObject, eventdata, handles)
cP = get(handles.cbConstantPullback,'Value');

if handles.acqInfo.acqRunning == 0 && handles.displaying == 0
    if cP
        set(handles.etTurnsSlice,'String',num2str(1))
        set(handles.etTurnsSlice,'Enable','off')
        set(handles.etNSlices,'Enable','off')
        set(handles.txDistanceSlice,'String','Distance to scan (in mm)')
%         set(handles.etDistanceSlice,'String',num2str(0.1))
        etDistanceSlice_Callback(hObject, eventdata, handles);
    else
%         set(handles.etTurnsSlice,'String',num2str(5))
        set(handles.etTurnsSlice,'Enable','on')
        set(handles.etNSlices,'Enable','on')
        set(handles.txDistanceSlice,'String','Distance between slices (in mm)')
%         set(handles.etDistanceSlice,'String',num2str(0.001))        
    end 
else
    %Don't accept the change if we are acquiring or displaying
    set(handles.cbConstantPullback,'Value',~cP)
end


function rbUS2DCart_Callback(hObject, eventdata, handles)
set(handles.rbUSRF,'Value',0)
set(handles.rbUS2D,'Value',0)
set(handles.rbUS2DCart,'Value',1)
set(handles.rbUSLineTime,'Value',0)
set(handles.pmUSAngle,'Enable','off')

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


function rbPA2DCart_Callback(hObject, eventdata, handles)
set(handles.rbPARF,'Value',0)
set(handles.rbPA2D,'Value',0)
set(handles.rbPA2DCart,'Value',1)
set(handles.pmPAAngle,'Enable','off')

if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function


%Data cursor displays radius
function txt = updatefcn1(empt,event_obj)
if strcmp(get(get(event_obj,'Target'),'Type'), 'image')
    pos = get(event_obj,'Position');
    txt = {['X: ',num2str(pos(1)), 'Y: ', num2str(pos(2))], ['Distance: ', num2str(sqrt(pos(1)^2+pos(2)^2))]};
else
    pos = get(event_obj,'Position');
    txt = {['X: ',num2str(pos(1)), 'Y: ', num2str(pos(2), '%10.2e')]};
    
end


function uitoggletool4_OnCallback(hObject, eventdata, handles)
if strcmpi(get(handles.uitoggletool4,'State'),'on')
    dcm_obj = datacursormode(handles.figure1);
    set(dcm_obj,'UpdateFcn',@updatefcn1)
end

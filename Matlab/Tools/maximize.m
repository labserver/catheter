%MAXIMIZE  Maximize a figure window to fill the entire screen
%
% Examples:
%   maximize
%   maximize(hFig)
%
% Maximizes the current or input figure so that it fills the whole of the
% screen that the figure is currently on. This function is platform
% independent.
%
%IN:
%   hFig - Handle of figure to maximize. Default: gcf.

function maximize(hFig)
if nargin < 1
    hFig = gcf;
end
drawnow % Required to avoid Java errors
warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
jFig = get(handle(hFig), 'JavaFrame'); 
jFig.setMaximized(true);

%Alternative wehn JavaFrame becomes obsolete. N for french, X for english,
%found using get(0, 'Language')
% robot = java.awt.Robot; 
% robot.keyPress(java.awt.event.KeyEvent.VK_ALT);      %// send ALT
% robot.keyPress(java.awt.event.KeyEvent.VK_SPACE);    %// send SPACE
% robot.keyRelease(java.awt.event.KeyEvent.VK_SPACE);  %// release SPACE
% robot.keyRelease(java.awt.event.KeyEvent.VK_ALT);    %// release ALT
% robot.keyPress(java.awt.event.KeyEvent.VK_N);        %// send X
% robot.keyRelease(java.awt.event.KeyEvent.VK_N);      %// release X
%Load a file, modify something and save it back.

clear all; clc; close all;

%Load file
[filename,pathname] = uigetfile('-mat','Choose a results file to load',[pwd '\Results']);
if filename == 0
    return;
end
file=open([pathname filename]);


param=file.param;
results=file.results;
acqInfo=file.acqInfo;
displayInfo=file.displayInfo;
scanDate = file.scanDate;

%Modify something...
acqInfo.outputFileName = 'FloatingInputADC';
%         results.ADC8CH = zeros(param.nTurns*param.nPositionsPerTurn,acqInfo.lengthADC8CH); %8CH ADCs signals
%         results.scanPos = zeros(param.nTurns*param.nPositionsPerTurn,1); %List of scanned position
%         results.error = zeros(param.nTurns*param.nPositionsPerTurn,1); %List of errors/warnings
%         results.durationAcquisition = zeros(param.nTurns*param.nPositionsPerTurn,1); %List of durations of the ultrasound acquisition
%         results.durationDemod = zeros(param.nTurns*param.nPositionsPerTurn,1); %List of durations of the demodulation
%         results.durationPosition = zeros(param.nTurns*param.nPositionsPerTurn,1); %List of durations at each angular position
%         results.durationTransfer = zeros(param.nTurns*param.nPositionsPerTurn,1); %List of duration of all the USB transfers
%         results.voltageMotor = zeros(param.nTurns*param.nPositionsPerTurn,1); %List of voltage values sent to the motors
%         results.durationBetweenTransfers = zeros(param.nTurns*param.nPositionsPerTurn,1); %List of duration between two transfers
%         results.durationTotalAcquisition = 0; %Total duration of the acquisition, from the time the user press Start, until the data is saved
%         results.durationSDRAMWrite = zeros(param.nTurns*param.nPositionsPerTurn,1); %List of duration of all the SDRAM write transfers

%userComments = 'Tube de plastique de 2.4mm situ� � environ 3.7mm du transducteur.';


%Save file
if acqInfo.acqInterrupted == 0
    outputName = [acqInfo.currentPath '\Results\' acqInfo.outputFileName scanDate '_UPDATED.mat'];
else
    outputName = [acqInfo.currentPath '\Results\' acqInfo.outputFileName scanDate '_INTERRUPTED_UPDATED.mat'];
end

%Saving data
save(outputName,'scanDate','param','results','acqInfo','displayInfo')
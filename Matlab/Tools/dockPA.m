function dockPA(a,b)
% Retrieve handle from GUI
hObject = findall(0,'Name','PAUSFluo');

if isempty(hObject); delete(figure(30)); end

handles = guidata(hObject);
set(handles.cbPAUndock,'UserData',get(handles.cbPAUndock,'UserData')+1); %1 increment each X press

if get(handles.cbPAUndock,'UserData') >= 3
    delete(figure(30)); %Close figure after 3 X press
end

if ~isfield(handles,'displaying'); handles.displaying=0; end
if ~isfield(handles,'displaying'); handles.displaying=0; end
if handles.acqInfo.acqRunning == 0
    if handles.displaying == 0
        ProcessData(handles, hObject)
        handles = guidata(hObject); % Retrieve the updated handle after calling a function
    else
        %Currently displaying, queue display
        set(handles.pbRefreshDisplay, 'UserData', 1);
    end      
end
guidata(hObject, handles); % Update handles at the end of a function

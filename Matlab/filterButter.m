function [dataOut]=filterButterFast(dataIn, fCutLow, fCutHigh, numL, denomL, numH, denomH, ultraFast)
%filtfilt is slower, but there is no delay/shift. 
if fCutLow <= 0
    dataTemp = dataIn;
else
    if ultraFast == 1
        dataTemp = filter(numL, denomL, dataIn);  
    else
        dataTemp = filtfilt(numL, denomL, dataIn);  
    end
    
end

if fCutHigh <= 0
    dataOut = dataTemp;
else
    if ultraFast == 1
        dataOut = filter(numH, denomH, dataTemp);  
    else
        dataOut = filtfilt(numH, denomH, dataTemp);
    end     
end

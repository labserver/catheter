%
% Initializes the USB connexions and sends the parameters to start an acquisition.
% The second thread is also started to receive USB packets very fast
%   
% INPUTS:
%
% param: structure containing all the parameters used in the acquisition
%
% OUTPUTS:
%
% success: Returns 1 if the initialization of the USB connection was
% successful.
%
% COMMENTS:
%
function [success] = InitUSB(param)

% C files to compile only once, or when they are modified
%To path to libusb.lib might need to be modified
%mex ThreadUSB.c 'C:\Program Files (x86)\LibUSB-Win32\lib\msvc_x64\libusb.lib'

%Initialization of the USB connexion
success = ThreadUSB('Init');
if success == 0
    return
end

if param.PAUSDataType == 4; PAUSTransfer16bits = 0; else PAUSTransfer16bits = 1; end
nParam = 33;

%Preparation of the vector to send
%All the datas fits in 16 bits word. They are divided in 8bits values.
send_data(1) = 2+65536*nParam;		%Code 2 to modify all the parameters, MSB is the number of parameters (27)
send_data(2) = param.nCyclesDemod;
send_data(3) = param.delayBetweenUltrasoundsNCycles;	
send_data(4) = param.nAveragingUltrasound;
send_data(5) = param.nAveragingUltrasoundFlush;
send_data(6) = param.laserPower1;
send_data(7) = param.laserPower2;
send_data(8) = param.PMTGain1;
send_data(9) = param.PMTGain2;
send_data(10) = param.transducerGainPA;
send_data(11) = param.transducerGain;
send_data(12) = param.pulserOn + param.photoacousticOn*2 + param.motorSync*4 + param.modulationOn*8 + param.demodulationOn*16 + param.fluoSingleMeasureOn*32 + ...
    param.useLinearMotor*64 + param.TGCPA*128 + param.TGCUS*256 + param.TGCCutPulse*512 + PAUSTransfer16bits*1024 + param.constantPullback*2048 + param.DPSSPhotoacousticOn*4096 + param.skipPAAcq*8192;
send_data(13) = param.nSlices+1; %Do one more slice due to the last slice not always acquired
send_data(14) = param.nTurnsPerSlice;
send_data(15) = param.distanceBetweenSlicesDAC;
send_data(16) = param.distanceFirstSliceDAC;
send_data(17) = param.pulseShapeP;
send_data(18) = param.pulseShapeN;
send_data(19) = param.pulseShapeStartRTZ;
send_data(20) = param.pulseShapeStopRTZ;
send_data(21) = uint32(param.TGC1/50*4095/4);
send_data(22) = uint32(param.TGC2/50*4095/4);
send_data(23) = uint32(param.TGC3/50*4095/4);
send_data(24) = uint32(param.TGC4/50*4095/4);
send_data(25) = uint32(param.nSamples);
send_data(26) = uint32(param.freqFPGA/(512*param.gearRatio*param.speedMotorTurnsPerSec/20));
send_data(27) = uint32(param.freqFPGA/(512*param.gearRatio*param.speedMotorTurnsPerSec/20)/20);
send_data(28) = uint32(param.linearMotorSpeed);
send_data(29) = uint32(param.linearMotorSpeedHome);
send_data(30) = uint32(param.PAUSDataDivisor);
send_data(31) = uint32(512*param.gearRatio);
send_data(32) = uint32(512*param.gearRatio/param.nPositionsPerTurn);
send_data(33) = uint32(param.nPositionsPerTurn);
send_data(34) = uint32(round(param.speedMotorTurnsPerSec));

% send_data(30) = 1; %Don't start acquisition now, wait 2 second

%D�but de la r�ception des r�sultats
trash0=ThreadUSB('Single_Read'); %We first flush the remaining data.
%Sending the informations to the FPGA
ThreadUSB('Write',int32(send_data))

%After waiting 1 second, we start the acquisition. Allows to preheat everything.
pause(1)
clear send_data
send_data(1) = 6+65536*(nParam+1);		%Code 6 to modify all the parameters, MSB is the parameter index (27)
send_data(2) = 1;	
ThreadUSB('Write',int32(send_data))
% disp('Started')
%Starting the dedicated thread that will continuously read the incoming data from the USB connexion
ThreadUSB('Start')
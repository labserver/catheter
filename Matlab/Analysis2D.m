%
% Performs the 2D calculations, such as removing the PA baseline and
% passing from polar to cartesian coordinates.
%   
% INPUTS:
%
% param: Structure containing all the parameters used in the acquisition.
%
% displayInfo: Structure containing the display information.
%
% acqInfo: Structure containing variables used when reading data from the FPGA to synchronize all the processes.
%
% PAF, USF, fluoF: Data to analyse of the three modalities.
%
% OUTPUTS:
%
% PA2D, US2D, fluo2D: Data converted in cartesian coordinates of the three modalities.
%
% COMMENTS:
%
function [PA2D,US2D,fluo2D] = Analysis2D(param, displayInfo, PAF, USF, fluoF)

%First, remove baseline on every angle of photoacoustic and ultrasound signals
% baselinePA = sum(PAF,1)/256;
% PAF = bsxfun(@minus,PAF,baselinePA);
% baselineUS = sum(USF,1)/256;
% USF = abs(bsxfun(@minus,USF,baselineUS));

%Better display for PA saving when a PA pulse was missed
% [a,b] = find(PAF(100,:) == 0);
% for i = 1:length(b)
%     try
%         PAF(:,b(i)) = (PAF(:,b(i)-1) + PAF(:,b(i)+1)) / 2;
%     end
% end

%Generate PA baseline
% PAFBaseline = PAF; save('baselinePA.mat','PAFBaseline')
%Remove PA baseline from results
% load('baselinePA.mat')
% PAFBaselineTemp = PAFBaseline(1:1:end,:); clear PAFBaseline; PAFBaseline = PAFBaselineTemp(1:size(PAF,1),:);
% PAF = PAF./PAFBaseline;

%Load 2D calculations
rho = displayInfo.rho;
theta = displayInfo.theta;
lin1PA = displayInfo.lin1PA;
lin1US = displayInfo.lin1US;
col1 = displayInfo.col1;

%Interpolation
if displayInfo.PADisplaySignal==0
    PAF = single(PAF);
    PA2D = interp2(col1,lin1PA,PAF,theta,rho); %'spline'
else
    PA2D = 0;
end
if displayInfo.USDisplaySignal==0 && displayInfo.USDisplayLineTime==0
    USF = single(USF);
%     US2D = single(interp2(col1,lin1US,USF,theta,rho,'nearest')); %'spline'

    %Downsample USF
    dS = round(2*size(USF,1)/displayInfo.nPixels);
    dS(dS<1)=1;
    USFTemp = USF(1:dS:end,:)';    
    lin1US = lin1US(1:dS:end);
    col1New = repmat(col1,length(lin1US),1)'; lin1USNew = repmat(lin1US,length(col1),1); 
    g=griddedInterpolant(col1New,lin1USNew,USFTemp,'nearest');
    try %Works only on newer version of Matlab (mandatory)
        g.ExtrapolationMethod = 'none';
    end
    US2D=g(theta,rho);
%     US2D = single(qinterp2(col1New,lin1USNew,USF,theta,rho,0)); %'spline' 
else
    US2D = 0;
end
            
if (displayInfo.fluoDisplaySignal==0 && displayInfo.fluoDisplayFull2D==0) || displayInfo.USFluo == 1
    %Create 2D data from the fluorescence signal
    idFluo = displayInfo.SDPair;
    fluo2D = zeros(displayInfo.nPixels,displayInfo.nPixels);

    fluoTemp = repmat(fluoF(idFluo,:),displayInfo.nSamplesToDisplay,1);

    %We put a constant value for the first samples to represent the catheter and hide the pulse
    if displayInfo.radiusCatheterFlushNSamples > 0
        fluoTemp(1:displayInfo.radiusCatheterFlushNSamples,:) = NaN;
    end 
    fluoTemp = single(fluoTemp);
    
    %Downsample USF
    dS = round(2*size(fluoTemp,1)/displayInfo.nPixels);
    dS(dS<1)=1;
    fluoTemp = fluoTemp(1:dS:end,:)';    
    lin1PA = lin1PA(1:dS:end);
    col1New = repmat(col1,length(lin1PA),1)'; lin1PANew = repmat(lin1PA,length(col1),1); 
    g=griddedInterpolant(col1New,lin1PANew,fluoTemp,'nearest');
    fluo2D=g(theta,rho);    
    
%     fluo2D(:,:) = interp2(col1,lin1PA,squeeze(fluoTemp(:,:))',theta,rho);
else
    fluo2D = 0;
end      

%     %Code for PA 2D filtering
%     h = fspecial('disk', 20);
%     PA2D = imfilter(PA2D,h,'replicate');

return
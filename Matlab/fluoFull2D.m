%
% Calculates the baseline of the fluorescence signal when there is no
% fluorophore. This is used to remove the baseline of the signal, usually
% caused by the imperfection of the rotary joint
%   
% INPUTS:
%
% param: Structure containing all the parameters used in the acquisition.
%
% displayInfo: Structure containing the display information.
%
% acqInfo: Structure containing variables used when reading data from the FPGA to synchronize all the processes.
%
% OUTPUTS:
%
% fluoFull2DOut: The fluorescence 2D image that was calculated.
%
% COMMENTS:
%
function fluoFull2DOut = fluoFull2D(param, displayInfo, acqInfo)

if acqInfo.acqInterrupted == 0
    nSlices = param.nSlices;
else
    nSlices = floor(floor(acqInfo.idxPositionTotal/param.nPositionsPerTurn)/param.nTurnsPerSlice);
end

fluoFull2DOut = zeros(acqInfo.lengthFluo,nSlices,param.nPositionsPerTurn);

if acqInfo.oldSavedFiles == 0
    %Retrieve the slice in the .dat files   
    fileFluo = fopen([acqInfo.outputFullFileName '_Fluo.dat'],'r');    
end
   
for idxSliceFluo = 1:nSlices
    %Check if the loaded file is from an older version
    if acqInfo.oldSavedFiles == 0
        %Go to the correct slice.
        type = param.PAUSDataType;
        typeFluo = type; typeFluo(typeFluo < 4) = 4; %There is no fluo data at 16 bits
        fseek(fileFluo, (idxSliceFluo-1)*param.nTurnsPerSlice*param.nPositionsPerTurn*acqInfo.lengthFluo*typeFluo, 'bof');
    end    
    
    fluoFTemp = zeros(acqInfo.lengthFluo,param.nPositionsPerTurn);

    for idxTurn = 1:param.nTurnsPerSlice
        %Different with the old version of the results files
        if acqInfo.oldSavedFiles == 1
            %Will not work on old saved files. We would have to transfer
            %results            
%             idxTurnTotalFluo = param.nTurnsPerSlice*(idxSliceFluo-1) + idxTurn;
%             fluo = squeeze(results.fluo(idxTurnTotalFluo,:,:));            
        else
            if param.PAUSDataType == 8 %Saved data is in double and must be transposed
                %Old data, in double format
                fluo = reshape(fread(fileFluo, param.nPositionsPerTurn*acqInfo.lengthFluo, 'double'),param.nPositionsPerTurn,acqInfo.lengthFluo);            
                fluo = int32(fluo);
            elseif param.PAUSDataType == 4                  
                fluo = reshape(fread(fileFluo, param.nPositionsPerTurn*acqInfo.lengthFluo, 'int32'),param.nPositionsPerTurn,acqInfo.lengthFluo);
            else %2                
                fluo = reshape(fread(fileFluo, param.nPositionsPerTurn*acqInfo.lengthFluo, 'int32'),param.nPositionsPerTurn,acqInfo.lengthFluo);                
            end                 
        end        

        [bidon1,bidon2,fluoF] = AnalysisFilter(param, displayInfo, acqInfo, zeros(param.nSamples,param.nPositionsPerTurn), zeros(2*param.nSamples,param.nPositionsPerTurn), fluo, 1);

        %Average all the turns of a slice
        if (mod(idxTurn,param.nTurnsPerSlice) ~= 1 && param.nTurnsPerSlice ~= 1)
            %Not the first turn of a slice
            fluoFTemp = fluoFTemp + fluoF;
        else
            fluoFTemp = fluoF;  
        end 
    end
    %toc  
    fluoFull2DOut(:,idxSliceFluo,:) = fluoFTemp;

end

%Uncomment to change the scale of the image
%     fluoFull2DOut(fluoFull2DOut < max(fluoFull2DOut(1,1500,:))) = max(fluoFull2DOut(1,1500,:));
fluo2D = squeeze(fluoFull2DOut(1,:,:));
sorted = sort(fluo2D(:,:));
sorted = sorted(:); sorted = sort(sorted);
mean999 = mean(sorted(int32(length(sorted)*0.999):end));
minFluo = mean(sorted(1:int32(length(sorted)*0.3)));
fluo2DSignalBefore = mean999 - minFluo

%Amplitude correction with wall distance
outputFile = [acqInfo.outputFullFileName '_Wall.mat'];
if ~exist(outputFile,'file')
    fclose('all');
    return
end
% return
load(outputFile)
wall = wall-0.5; %Distance from catheter;
wall(wall>0.4) = 0.8-wall(wall>0.4);
wall(wall<0.1) = 0.1;
ueff = 2;
for ind = 1:1
    avg = min(min(squeeze(fluoFull2DOut(ind,:,:))));
    for idxSlice = 1:size(fluoFull2DOut,2)
        avg = min(min(squeeze(fluoFull2DOut(ind,idxSlice,:))));
        fluoFull2DOut(ind,idxSlice,:) = avg + (squeeze(fluoFull2DOut(ind,idxSlice,:))-avg+1) .*  exp(ueff.*2.*wall(idxSlice,:)');
    end
end

fclose('all');
fluo2D = squeeze(fluoFull2DOut(1,:,:));
% nSlices= 20*35;
% fluo2D  = fluo2D(1:nSlices,:);
sorted = sort(fluo2D(:,:));
sorted = sorted(:); sorted = sort(sorted);
mean999 = mean(sorted(int32(length(sorted)*0.999):end));
minFluo = mean(sorted(1:int32(length(sorted)*0.3)));
fluo2DSignalAfter = mean999 - minFluo
    
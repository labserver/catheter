%
% Displays the results of the acquisition on the GUI axes. The function is
% called at the end of the acquisition of a turn and when the user wants to
% refresh the display, once the acquisition is completed.
% Since displaying 2D images (when selected) can be long, this
% function is sometimes skipped during an acquisition.
%   
% INPUTS:
%
% handles: handles to the GUI, which allows reading of the parameters of the acquisition, the display parameters, 
% the results and the acquisition information set by the user.
%
% hObject: Link to the GUI. It is used to save the handles at the end of the function
%
% PAF, USF, fluoF, PA2D, US2D, fluo2D: Data to display of the three
% modalities.
%
% doSave: Used to inform the function to save the data from one of the
% three modalities shown. The data that is displayed is saved in a .mat, as
% well as the image in .tiff. Saving a video of multiple images is also
% possible.
%
% OUTPUTS:
%
% none.
%
% COMMENTS:
%
% There is no outputs. However, user data of the handles is update at the end of the function using the command guidata
%
function Display(handles, hObject, PAF, USF, fluoF, PA2D, US2D, fluo2D, doSave)
% std(PAF(100:200,100))
% std(fluoF(1,:))
results = getappdata(handles.figure1,'results'); %Load the rest of the results to display

%Save is an optional parameter to save one of the three images
if nargin < 9
    doSave = 0;
end

%Update current slice text
    if handles.acqInfo.acqRunning == 1
        position = floor((handles.acqInfo.idxTurnTotal-1)/handles.param.nTurnsPerSlice)+1;
    else
        position = handles.displayInfo.USPullback;
    end
    
    %Check how many complete slices there are.
    if handles.param.distanceBetweenSlices > 0
        set(handles.txIdxSlice,'String',[num2str(1000*(handles.param.distanceFirstSlice+(position-1)*handles.param.distanceBetweenSlices)) ' mm'])
    else
        set(handles.txIdxSlice,'String',['S #: ' num2str(position)])
    end

% Check for displayed errors since the last display (time consuming)
% if ~isfield(handles,'lastErrorDisplayed'); handles.lastErrorDisplayed = 0; end
% if ~isfield(handles,'errorDisplayStop'); handles.errorDisplayStop = 0; end
% if length(get(handles.lbProblems, 'String')) < 200 && handles.errorDisplayStop == 0 %Less than 200 errors displayed
%     if handles.lastErrorDisplayed < handles.acqInfo.idxPositionTotal % && handles.acqInfo.idxTurnTotal <= handles.param.nTurns %Not all errors have been displayed
%         if sum(results.error(handles.lastErrorDisplayed+1:handles.acqInfo.idxPositionTotal)) ~= 0
%             disp('Oups. There is an error.')
%             %Display only 200 errors
%             %handles.lastErrorDisplayed(handles.lastErrorDisplayed < handles.acqInfo.idxPositionTotal-200) = double(uint32(handles.acqInfo.idxPositionTotal-200));
%             for idx1 = handles.lastErrorDisplayed+1:handles.acqInfo.idxPositionTotal
%                 if results.error(idx1) > 0 && length(get(handles.lbProblems, 'String')) < 200
%                     set(handles.lbProblems, 'String', strvcat(get(handles.lbProblems, 'String'), ['Warning/Error detected. Code: ' num2str(results.error(idx1))]))              
%                     set(handles.lbProblems,'Value',size(get(handles.lbProblems, 'String'),1))
%                 end
%             end
%         end 
%         handles.lastErrorDisplayed = handles.acqInfo.idxPositionTotal;
%     end
% elseif handles.errorDisplayStop == 0
%     %Too much error displayed
%     set(handles.lbProblems, 'String', strvcat(get(handles.lbProblems, 'String'), ['Too much errors to display.']))
%     set(handles.lbProblems,'Value',size(get(handles.lbProblems, 'String'),1))
%     handles.errorDisplayStop = 1;
% end

%Display debugging information
% disp(['PA Noise (std): ' num2str(std(squeeze(PAF(end-100:end,handles.displayInfo.PAAngle))))])
% disp(['PA Noise (std): ' num2str(std(squeeze(PAF(:))))])
% disp(['PA Max: ' num2str(max(squeeze(PAF(:))))])
% disp(['US Noise (std): ' num2str(std(squeeze(USF(end-100:end,handles.displayInfo.USAngle))))])
% maxUS = max(squeeze(USF(:)));
% if maxUS > 2000
%     disp(['US Max: ' num2str(maxUS) ' Slice: ' num2str(handles.acqInfo.idxTurnTotal)])
% end
% disp(['Fluo Noise (std): ' num2str(std(squeeze(fluoF(2,1:80))))])
% disp(['Fluo Mean: ' num2str(mean(squeeze(fluoF(2,:))))])
% disp(['Fluo Max: ' num2str(max(squeeze(fluoF(2,:))))])
% [~,maxPos] = max(squeeze(fluoF(2,:))); maxPos(maxPos < 6) = 6; maxPos(maxPos > 251) = 251;
% disp(['Fluo MeanSig-MeanNoise: ' num2str(mean(squeeze(fluoF(2,maxPos-5:maxPos+5))) - mean(squeeze(fluoF(2,100:150))))])
% [val, idx] = max(max(PAF(:,7:30)));
% [idxY, idxX] = ind2sub(size(PAF),idx);
% set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), ['Max value in PA: ' num2str(val,'%6.3g') ' at X=' num2str(idxX) ', Y=' num2str(1000*idxY/size(PAF,1)*handles.displayInfo.depthToDisplay,'%6.3g') '.'])); set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))
% disp(['PA Noise (std): ' num2str(std(squeeze(PAF(round(size(PAF,1)/2),1:end))))])

%Values for log compression
minIndex=0;
maxIndex=256;
SmallNonZeroNumber=10^(-20); %To avoid log of zero

%PA display
if handles.displayInfo.PADisplayOn ~= 1
    if handles.displayInfo.updateAxisInformation == 1
        cla(handles.axPA,'reset');
        tic;close(findobj('type','figure','name','PA'));toc
    end
elseif mod(handles.acqInfo.idxTurnTotal-1,handles.param.nTurnsPerSlice) < 2*handles.param.speedMotorTurnsPerSec && handles.acqInfo.acqRunning == 1 && strcmp(handles.displayInfo.displayMultipleTurns,'Sum')
    %If in sum mode, skip during 2 seconds to allow displaying the complete
    %image longer (only when acq is running)    
else
%     handles.displayInfo.displayGainPA = round(get(handles.slDisplayGainPA,'Value'));
%     handles.displayInfo.displayDynamicRangePA = round(get(handles.slDisplayDynamicRangePA,'Value'));
    gainPA = handles.displayInfo.displayGainPA;
    dynPA = handles.displayInfo.displayDynamicRangePA;
    set(handles.etDisplayGainPA,'Enable','on'); set(handles.etDisplayGainPA,'String',gainPA); set(handles.etDisplayGainPA,'Enable','off');
    set(handles.etDisplayDynamicRangePA,'Enable','on'); set(handles.etDisplayDynamicRangePA,'String',dynPA); set(handles.etDisplayDynamicRangePA,'Enable','off');      
    if handles.displayInfo.PADisplaySignal==1 %RF signal display
    %     baselinePA = sum(PAF,2)/256;
    %     PAF = bsxfun(@minus,PAF,baselinePA);
%         PAF(:,1) = mean(PAF,2);
        PAF=PAF(:,handles.displayInfo.PAAngle);
        if strcmp(handles.displayInfo.displayTypePA,'Log')
            temp = single(PAF);
            temp2 = max(SmallNonZeroNumber,temp);
            temp3 = 10*log10(temp2);
            PAF=maxIndex*(gainPA+temp3)/dynPA;
            PAF=min(maxIndex, max(minIndex,PAF));    
        end
%         %Remove the first samples for FFT calculations
%         PAFtemp = double(USF(round(5.8*length(USF)/10.395):round(6.8*length(USF)/10.395)));
%         %FFT of PAF
%         Fs = 200e6;
%         T = 1/Fs;
%         L = length(PAFtemp);
%         t = (0:L-1)*T;
%         NFFT = 2^nextpow2(L);
%         PAFFT = 20*log10(abs(fft(PAFtemp,NFFT)/L));
%         PAFFT = PAFFT(1:NFFT/2+1);
%         PAFFT = PAFFT-max(PAFFT);
%         f = Fs/2*linspace(0,1,NFFT/2+1);
%         hF = figure(100); hA = subplot(1,1,1); plot(hA, f,PAFFT);
% %         ylim(hA,[0 0.3])
%         xlabel(hA,'Frequency (Hz)')
%         ylabel(hA,'Power spectrum (dB)')
       
        if (~isfield(handles,'PAImage')); handles.PAImage = -1; end
        if handles.displayInfo.updateAxisInformation == 1 || ~ishandle(handles.PAImage)
            if handles.displayInfo.PAUndock == 1
                cla(handles.axPA,'reset');
                handles.parentFigPA = figure(30);
                set(handles.axPA,'Units','Pixels'); origPos = get(handles.axPA,'Position'); set(handles.axPA,'Units','Normalized'); %Determine position on top of docked figure. Must put units back to normalized.
                set(handles.figure1,'Units','Pixels'); GUIPos = get(handles.figure1,'Position'); %Get position of the GUI window.
                set(handles.parentFigPA, 'Position', [origPos(1)+GUIPos(1)-30 origPos(2)+GUIPos(2)-10 handles.displayInfo.nPixels handles.displayInfo.nPixels])  
                set(handles.parentFigPA, 'MenuBar','none','ToolBar','figure')
                drawnow
                winontop(handles.parentFigPA);
                set(handles.parentFigPA,'name','PA');
                set(handles.parentFigPA,'CloseRequestFcn',@dockPA)
                handles.parentAxesPA = subplot(1,1,1);
            else
                handles.parentAxesPA = handles.axPA;
            end            
            
            handles.PAImage = plot(handles.parentAxesPA,1e3*linspace(0,handles.displayInfo.depthToDisplay,handles.displayInfo.nSamplesToDisplay),PAF);             
            axis(handles.parentAxesPA,'normal'); axis(handles.parentAxesPA,'xy'); axis(handles.parentAxesPA,'tight');
            set(handles.parentAxesPA,'NextPlot','replacechildren');
            if strcmp(handles.displayInfo.displayTypePA,'Log')
                ylim(handles.parentAxesPA,[minIndex maxIndex])
            else
                ylim(handles.parentAxesPA,'auto')
            end  
            xlabel(handles.parentAxesPA,'Depth (in millimeters)','FontName','Arial','FontSize',12,'HandleVisibility','off')
            ylabel(handles.parentAxesPA,'Signal (A. U.)','FontName','Arial','FontSize',12,'HandleVisibility','off')  
            set(handles.parentAxesPA,'YTickMode','auto') 
        else
            handles.PAImage = plot(handles.parentAxesPA,1e3*linspace(0,handles.displayInfo.depthToDisplay,handles.displayInfo.nSamplesToDisplay),PAF);  
        end   
        
    else %2D display 
        if handles.displayInfo.PADisplay2DCart == 1
            PA2D = PAF;
        end
        PA2D = single(PA2D);
        if strcmp(handles.displayInfo.displayTypePA,'Log')
            PA2D = max(SmallNonZeroNumber,PA2D);
            PA2D = 10*log10(PA2D);
        else
            gainPA = 10^(gainPA/10);
            dynPA = 10^(dynPA/10);
        end
        PA2D=maxIndex*(gainPA+PA2D)/dynPA;
        PA2D=min(maxIndex, max(minIndex,PA2D));  
        if (~isfield(handles,'PAImage')); handles.PAImage = -1; end
        if handles.displayInfo.updateAxisInformation == 1 || ~ishandle(handles.PAImage)
            if handles.displayInfo.PAUndock == 1 %First undock display
                cla(handles.axPA,'reset'); %Reset docked axes to save time
                handles.parentFigPA = figure(30); %Create figure
                handles.parentAxesPA = subplot(1,1,1); %Parent is either docked or undocked axes
                set(handles.axPA,'Units','Pixels'); origPos = get(handles.axPA,'Position'); set(handles.axPA,'Units','Normalized'); %Determine position on top of docked figure. Must put units back to normalized.
                set(handles.figure1,'Units','Pixels'); GUIPos = get(handles.figure1,'Position'); %Get position of the GUI window.
                set(handles.parentFigPA, 'Position', [origPos(1)+GUIPos(1)-30 origPos(2)+GUIPos(2)-10 handles.displayInfo.nPixels handles.displayInfo.nPixels])  
                set(handles.parentFigPA, 'MenuBar','none','ToolBar','figure') %Remove menu bar
                drawnow
                winontop(handles.parentFigPA); %Figure will always be on top
                set(handles.parentFigPA,'name','PA');
                set(handles.parentFigPA,'CloseRequestFcn',@dockPA) %Closing the figure will be managed by a special function
                colormap(handles.parentFigPA,'hot'); drawnow
            else
                handles.parentAxesPA = handles.axPA; %Parent is either docked or undocked axes
            end            

            if handles.displayInfo.PADisplay2DCart == 0
                boundX = 1e3*[-handles.displayInfo.depthToDisplay handles.displayInfo.depthToDisplay];
                boundY = 1e3*[-handles.displayInfo.depthToDisplay handles.displayInfo.depthToDisplay];
            else
                boundX = 1:256;
                boundY = 1e3*[0 handles.displayInfo.depthToDisplay];
            end            
            handles.PAImage = imagesc(boundX,boundY,PA2D,'Parent',handles.parentAxesPA);
            if strcmp(handles.displayInfo.displayTypePA,'Log')
                set(handles.parentAxesPA,'CLim',[minIndex maxIndex]);
            else
                set(handles.parentAxesPA,'CLimMode','auto');
            end

            %Display 1mm scale bar
                        %Display 1mm scale bar
            depth = handles.displayInfo.depthToDisplay*1000;
            if handles.displayInfo.PADisplay2DCart == 0
                rectangle('position',[depth-1.1 depth-0.35 1 0.25], 'facecolor','w','Parent',handles.parentAxesPA)
    %             text(depth-0.6,depth-0.6,'1mm','Parent',handles.parentAxesUS,'FontName','Arial','FontSize',12,'HorizontalAlignment','center','Color','w')  
            else
                rectangle('position',[245 depth-1.1 5 1], 'facecolor','w','Parent',handles.parentAxesPA)    
            end

            axis(handles.parentAxesPA,'square'); axis(handles.parentAxesPA,'ij'); axis(handles.parentAxesPA,'tight');
            set(handles.parentAxesPA,'NextPlot','replacechildren');
            if handles.displayInfo.PAUndock == 0
                xlabel(handles.parentAxesPA,'Position X (in millimeters)','FontName','Arial','FontSize',12,'HandleVisibility','off')
                ylabel(handles.parentAxesPA,'Position Y (in millimeters)','FontName','Arial','FontSize',12,'HandleVisibility','off')      
                set(handles.parentAxesPA,'XTickMode','auto');set(handles.parentAxesPA,'YTickMode','auto');
                if handles.displayInfo.PADisplay2DCart == 1
                    set(handles.parentAxesPA,'XTick',[1 64:64:256]) 
                end
                    
            else
                axis(handles.parentAxesPA,'off'); %Remove axis
                set(handles.parentAxesPA, 'Units', 'normalized', 'Position', [0 0 1 1]) %Fill the figure with the axes
            end
        else
            set(handles.PAImage,'CData',PA2D);
        end  
    end
end

%US display
% handles.displayInfo.displayGainUS = round(get(handles.slDisplayGainUS,'Value'));
% handles.displayInfo.displayDynamicRangeUS = round(get(handles.slDisplayDynamicRangeUS,'Value'));
gainUS = handles.displayInfo.displayGainUS;
dynUS = handles.displayInfo.displayDynamicRangeUS;
set(handles.etDisplayGainUS,'Enable','on'); set(handles.etDisplayGainUS,'String',gainUS); set(handles.etDisplayGainUS,'Enable','off');
set(handles.etDisplayDynamicRangeUS,'Enable','on'); set(handles.etDisplayDynamicRangeUS,'String',dynUS); set(handles.etDisplayDynamicRangeUS,'Enable','off');  
if handles.displayInfo.USDisplaySignal==1 %RF signal display
    USF=USF(:,handles.displayInfo.USAngle);
    if strcmp(handles.displayInfo.displayTypeUS,'Log')
        temp = single(USF);
        temp2 = max(SmallNonZeroNumber,temp);
        temp3 = 10*log10(temp2);
        USF=maxIndex*(gainUS+temp3)/dynUS;
        USF=min(maxIndex, max(minIndex,USF));   
    end
    if (~isfield(handles,'USImage')); handles.USImage = -1; end
    if handles.displayInfo.updateAxisInformation == 1 || ~ishandle(handles.USImage) 
        if handles.displayInfo.USUndock == 1
            cla(handles.axUS,'reset');
            handles.parentFigUS = figure(31); %Create figure
            set(handles.axUS,'Units','Pixels'); origPos = get(handles.axUS,'Position'); set(handles.axUS,'Units','Normalized'); %Determine position on top of docked figure. Must put units back to normalized.
            set(handles.figure1,'Units','Pixels'); GUIPos = get(handles.figure1,'Position'); %Get position of the GUI window.
            set(handles.parentFigUS, 'Position', [origPos(1)+GUIPos(1)-30 origPos(2)+GUIPos(2)-10 handles.displayInfo.nPixels handles.displayInfo.nPixels])  
            set(handles.parentFigUS, 'MenuBar','none','ToolBar','figure')
            drawnow
            winontop(handles.parentFigUS);  
            set(handles.parentFigUS,'name','US');
            set(handles.parentFigUS,'CloseRequestFcn',@dockUS)
            handles.parentAxesUS = subplot(1,1,1);
        else
            handles.parentAxesUS = handles.axUS;
        end
        
        handles.USImage = plot(handles.parentAxesUS,1e3*linspace(0,handles.displayInfo.depthToDisplay,2*handles.displayInfo.nSamplesToDisplay),USF); 
%         handles.USImage = plot(handles.parentAxesUS,1e3*linspace(0,handles.displayInfo.depthToDisplay,length(USF)),USF); 
        axis(handles.parentAxesUS,'normal'); axis(handles.parentAxesUS,'xy'); axis(handles.parentAxesUS,'tight');
        set(handles.parentAxesUS,'NextPlot','replacechildren');
        if strcmp(handles.displayInfo.displayTypeUS,'Log')
            ylim(handles.parentAxesUS,[minIndex maxIndex])
        else
            ylim(handles.parentAxesUS,'auto')
        end        
        xlabel(handles.parentAxesUS,'Depth (in millimeters)','FontName','Arial','FontSize',12,'HandleVisibility','off')
        ylabel(handles.parentAxesUS,'Signal (A. U.)','FontName','Arial','FontSize',12,'HandleVisibility','off')  
        set(handles.parentAxesUS,'YTickMode','auto') 
    else
%         handles.USImage = plot(handles.parentAxesUS,1e3*linspace(0,handles.displayInfo.depthToDisplay,2*handles.displayInfo.nSamplesToDisplay),USF(1:end)); 
        handles.USImage = plot(handles.parentAxesUS,1e3*linspace(0,handles.displayInfo.depthToDisplay,length(USF)),USF(1:end)); 
    end     
    
else %2D display 
    if handles.displayInfo.USDisplay2DCart == 1
        US2D = USF;
    end
    if handles.displayInfo.USDisplayLineTime == 0
        if strcmp(handles.displayInfo.displayTypeUS,'Log')
            %From http://folk.ntnu.no/hergum/medt8012/image_formation_signal_chain.pdf
            US2D = single(US2D);
            US2D = max(SmallNonZeroNumber,US2D);
            US2D = 10*log10(US2D);
            US2D=maxIndex*(gainUS+US2D)/dynUS;
            US2D=min(maxIndex, max(minIndex,US2D));  
        end
        %USFluo in a same image
        if handles.displayInfo.USFluo == 1 && handles.displayInfo.USDisplay2DCart == 0
%             load('fluo2DBackup.mat')           
            US2Dtemp = ind2rgb(uint8(US2D),gray(256));
            fluo2Dtemp = single(fluo2D(:,:));
            minFluo = min(fluoF(handles.displayInfo.SDPair,:));
            maxFluo = max(fluoF(handles.displayInfo.SDPair,:)); 
            %Uncomment to rescale all IVFL signals on the same scale
%                 minFluo = 3.17e7; %ICG14: 1.5e7 before correction, 1.75e7 after. ICG16: -0.3e7 before, 1.75e7 after. ICG12: 1.9e7 before, same after. ICG11: 3e7 after. ICG5: 2e7 after.
%                 maxFluo = 3.21e7; %ICG14: 2.75e7 before correction, 3.25e7 after. ICG16: -0.3e7 before, 3e7 after. ICG12: 2.3e7 before, same after. ICG11: 4.25e7 after. ICG5: 3.25e7 after.
                  
                  %6372629 for VP2, 9057499 for VP6, 6756517 for VP4,
                  %7092202 VP11 coll, 8424287 VP11 ICAM, 4309731 VP10 coll,
                  %8543498 VP10 ICAM
                  %6951889 for VP14 coll, 7898506 VP12 ICAM
%                   minFluo = 4309731; 
%                   maxFluo = minFluo + 2.25e6; %1.25e6 for Collagene, 0.25e6 for ICAM
%                 fluoF(fluoF < minFluo) = minFluo;
%                 fluoF(fluoF > maxFluo) = maxFluo;   
%                   minFluo = 8376386
%                   maxFluo = 8.7177e+06
            fluo2Dtemp = (fluo2Dtemp-minFluo) / (maxFluo-minFluo) * maxIndex; 
            mask = squeeze(handles.displayInfo.maskUSFluo(:,:,1));
            fluo2Dtemp2 = zeros(size(fluo2Dtemp,1),size(fluo2Dtemp,2),3);
            fluo2Dtemp2(handles.displayInfo.maskUSFluo) = ind2rgb(uint8(fluo2Dtemp(mask)),jet(256));
            US2Dtemp(handles.displayInfo.maskUSFluo) = fluo2Dtemp2(handles.displayInfo.maskUSFluo);
            US2D = US2Dtemp;      
        end
        
        %Add elasto
%         load('ElastoData.mat')
%         MinVal= min(Elasto(:)); MaxVal= max(Elasto(:));
%         Elasto = imresize(Elasto,[size(US2D,1) size(US2D,2)],'nearest');
% %         NormalizeVal= max(abs([MinVal MaxVal]));
% %         Elasto= 0.5 + Elasto./(2*NormalizeVal);
%         Elasto = (Elasto-MinVal)./(MaxVal-MinVal);
%         Elasto= 1+round(255*Elasto);   
%         Elasto= ind2rgb(Elasto,jet(256));
% %         Elasto = imresize(Elasto,[size(US2D,1) size(US2D,2)]); Elasto(Elasto>1)=1; Elasto(Elasto<0)=0;
%         ElastoMask = imresize(ElastoMask,[size(US2D,1) size(US2D,2)],'nearest');
%         ElastoMask = repmat(ElastoMask,[1 1 3]);  
%         if size(US2D,3) == 1
%             US2D = ind2rgb(uint8(US2D),gray(256));
%         end
%         Alpha = 0.4;
%         mask1= Alpha*logical(single(round(ElastoMask)));
%         mask2= 1-mask1;
%         US2D = US2D.*mask2 + Elasto.*mask1;             

        if (~isfield(handles,'USImage')); handles.USImage = -1; end
        if handles.displayInfo.updateAxisInformation == 1 || ~ishandle(handles.USImage) 
            if handles.displayInfo.USUndock == 1 %First undock display
                if ~ishandle(handles.USImage); keepPos = 0; else keepPos = 1; end
                cla(handles.axUS,'reset'); %Reset docked axes to save time
                handles.parentFigUS = figure(31); %Create figure
                handles.parentAxesUS = subplot(1,1,1); %Parent is either docked or undocked axes
                set(handles.axUS,'Units','Pixels'); origPos = get(handles.axUS,'Position'); set(handles.axUS,'Units','Normalized'); %Determine position on top of docked figure. Must put units back to normalized.
                set(handles.figure1,'Units','Pixels'); GUIPos = get(handles.figure1,'Position'); %Get position of the GUI window.
                if keepPos == 0 %Don't mess with the size of the figure, unless when it is created.
                    set(handles.parentFigUS, 'Position', [origPos(1)+GUIPos(1)-30 origPos(2)+GUIPos(2)-10 handles.displayInfo.nPixels handles.displayInfo.nPixels])                
                end
                set(handles.parentFigUS, 'MenuBar','none','ToolBar','figure') %Remove menu bar
                drawnow
                winontop(handles.parentFigUS); %Figure will always be on top
                set(handles.parentFigUS,'name','US');
                set(handles.parentFigUS,'CloseRequestFcn',@dockUS) %Closing the figure will be managed by a special function
                colormap(handles.parentFigUS,'gray'); drawnow
            else
                handles.parentAxesUS = handles.axUS; %Parent is either docked or undocked axes
            end

    %             handles.USImage = imshow(US2D,'Parent',handles.axUS);%handles.axUS);%,[mean(mean(US2D(200:300,50:75))) max(max(US2D))])   
            if handles.displayInfo.USDisplay2DCart == 0
                boundX = 1e3*[-handles.displayInfo.depthToDisplay handles.displayInfo.depthToDisplay];
                boundY = 1e3*[-handles.displayInfo.depthToDisplay handles.displayInfo.depthToDisplay];
            else
                boundX = 1:256;
                boundY = 1e3*[0 handles.displayInfo.depthToDisplay];
            end
            handles.USImage = imagesc(boundX,boundY,US2D,'Parent',handles.parentAxesUS);
            if strcmp(handles.displayInfo.displayTypeUS,'Log')
                set(handles.parentAxesUS,'CLim',[minIndex maxIndex]);
            else
                set(handles.parentAxesUS,'CLimMode','auto');
            end

            %Display 1mm scale bar
            depth = handles.displayInfo.depthToDisplay*1000;
            if handles.displayInfo.USDisplay2DCart == 0
                rectangle('position',[depth-1.1 depth-0.35 1 0.25], 'facecolor','w','Parent',handles.parentAxesUS)
%                 text(depth-0.6,depth-0.6,'1mm','Parent',handles.parentAxesUS,'FontName','Arial','FontSize',12,'HorizontalAlignment','center','Color','w')    
            else
                rectangle('position',[245 depth-1.1 5 1], 'facecolor','w','Parent',handles.parentAxesUS)
            end

            axis(handles.parentAxesUS,'square'); axis(handles.parentAxesUS,'ij'); axis(handles.parentAxesUS,'tight');  %We want a square display at the proper orientation
            set(handles.parentAxesUS,'NextPlot','replacechildren'); %Faster display
            if handles.displayInfo.USUndock == 0
                xlabel(handles.parentAxesUS,'Position X (in millimeters)','FontName','Arial','FontSize',12,'HandleVisibility','off')
                ylabel(handles.parentAxesUS,'Position Y (in millimeters)','FontName','Arial','FontSize',12,'HandleVisibility','off')      
                set(handles.parentAxesUS,'XTickMode','auto');set(handles.parentAxesUS,'YTickMode','auto');
                if handles.displayInfo.USDisplay2DCart == 1
                    set(handles.parentAxesUS,'XTick',[1 64:64:256]) 
                end
            else
                axis(handles.parentAxesUS,'off'); %Remove axis
                set(handles.parentAxesUS, 'Units', 'normalized', 'Position', [0 0 1 1]) %Fill the figure with the axes
            end
        else
    %             set(handles.USImage,'visible','off');
            set(handles.USImage,'CData',US2D);
    %             set(handles.USImage,'visible','on');
        end  
    else
        if strcmp(handles.displayInfo.displayTypeUS,'Log')
            %From http://folk.ntnu.no/hergum/medt8012/image_formation_signal_chain.pdf
            US2D = single(results.USLineTime);
            US2D = max(SmallNonZeroNumber,US2D);
            US2D = 10*log10(US2D);
            US2D=maxIndex*(gainUS+US2D)/dynUS;
            US2D=min(maxIndex, max(minIndex,US2D));  
        end

        %Check how many complete slices there are.
        if handles.acqInfo.acqInterrupted == 0
            nSlices = handles.param.nSlices;
        else
            nSlices = floor(floor(handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn)/handles.param.nTurnsPerSlice);
        end 
        
        %Uncomment to display only a few slices in USLineTime. Use an offset if required.
%             nSlices = 600; 
%             offset = 1200;
%             US2D = US2D(offset:offset+nSlices-1,:);
        %Uncomment to display only one angle, instead of both sides.
%             US2D = US2D(:,1:int32(size(US2D,2)/2));
        if (~isfield(handles,'USImage')); handles.USImage = -1; end
        if handles.displayInfo.updateAxisInformation == 1 || ~ishandle(handles.USImage) 
            if handles.displayInfo.USUndock == 1 %First undock display
                cla(handles.axUS,'reset'); %Reset docked axes to save time
                handles.parentFigUS = figure(31); %Create figure
                handles.parentAxesUS = subplot(1,1,1); %Parent is either docked or undocked axes
                set(handles.axUS,'Units','Pixels'); origPos = get(handles.axUS,'Position'); set(handles.axUS,'Units','Normalized'); %Determine position on top of docked figure
                set(handles.parentFigUS, 'Position', [origPos(1)+30 origPos(2)+30 handles.displayInfo.nPixels handles.displayInfo.nPixels]) %Set position
                set(handles.parentFigUS, 'MenuBar','none','ToolBar','figure') %Remove menu bar
                drawnow
                winontop(handles.parentFigUS); %Figure will always be on top
                set(handles.parentFigUS,'name','US');
                set(handles.parentFigUS,'CloseRequestFcn',@dockUS) %Closing the figure will be managed by a special function
                colormap(handles.parentFigUS,'gray'); drawnow
            else
                handles.parentAxesUS = handles.axUS; %Parent is either docked or undocked axes
            end

            distanceBetweenSlices = handles.param.distanceBetweenSlices;
            if distanceBetweenSlices == 0; distanceBetweenSlices = 1e-3; end
            handles.USImage = imagesc(1e3*[-handles.displayInfo.depthToDisplay handles.displayInfo.depthToDisplay],1e3*[handles.param.distanceFirstSlice handles.param.distanceFirstSlice+1e-6+(nSlices-1)*distanceBetweenSlices],US2D,'Parent',handles.parentAxesUS);
            if strcmp(handles.displayInfo.displayTypeUS,'Log')
                set(handles.parentAxesUS,'CLim',[minIndex maxIndex]);
            else
                set(handles.parentAxesUS,'CLimMode','auto');
            end
            
            %Uncomment to display the scale bar in USLineTime.
%                 depth = handles.displayInfo.depthToDisplay*1000;
%                 %TODO: Put the rectangle at the correct place.
%                 rectangle('position',[-depth+0.25 2.4 1 0.05], 'facecolor','w','Parent',handles.parentAxesUS)           

            axis(handles.parentAxesUS,'normal'); axis(handles.parentAxesUS,'ij'); axis(handles.parentAxesUS,'tight');  %We want a square display at the proper orientation
            set(handles.parentAxesUS,'NextPlot','replacechildren'); %Faster display
            if handles.displayInfo.USUndock == 0
                xlabel(handles.parentAxesUS,'Depth (in millimeters)','FontName','Arial','FontSize',12,'HandleVisibility','off')
                ylabel(handles.parentAxesUS,'Pullback position (in millimeters)','FontName','Arial','FontSize',12,'HandleVisibility','off')      
                set(handles.parentAxesUS,'YTick',1000*(handles.param.distanceFirstSlice:handles.param.distanceBetweenSlices:handles.param.distanceFirstSlice+(nSlices-1)*handles.param.distanceBetweenSlices))                  
            else
                axis(handles.parentAxesUS,'off'); %Remove axis
                set(handles.parentAxesUS, 'Units', 'normalized', 'Position', [0 0 1 1]) %Fill the figure with the axes
            end
        else
            set(handles.USImage,'CData',US2D);
        end          

    end
end

%Fluo display
if handles.displayInfo.fluoDisplayOn ~= 1
    if handles.displayInfo.updateAxisInformation == 1
        cla(handles.axFluo,'reset');
        close(findobj('type','figure','name','Fluo'))
    end
else
%     handles.displayInfo.displayGainFluo = round(get(handles.slDisplayGainFluo,'Value'));
%     handles.displayInfo.displayDynamicRangeFluo = round(get(handles.slDisplayDynamicRangeFluo,'Value'));
    gainFluo = handles.displayInfo.displayGainFluo;
    dynFluo = handles.displayInfo.displayDynamicRangeFluo;
    set(handles.etDisplayGainFluo,'Enable','on'); set(handles.etDisplayGainFluo,'String',gainFluo); set(handles.etDisplayGainFluo,'Enable','off');
    set(handles.etDisplayDynamicRangeFluo,'Enable','on'); set(handles.etDisplayDynamicRangeFluo,'String',dynFluo); set(handles.etDisplayDynamicRangeFluo,'Enable','off');         
    if handles.displayInfo.fluoDisplaySignal==1 %Signal display
        fluoF=fluoF(handles.displayInfo.SDPair,:);
%         fluoF = fluoF + 1.39e6; %Recover baseline.
%         fluoF = fluoF/110; %110 measures taken per angular position.
%         fluoF = fluoF / 2^17 * 12; %Convert to voltage (0 to 12V).

%         fluoF = fluoF - min(fluoF(:));
%         fluoF = fluoF / max(fluoF(:));

        if strcmp(handles.displayInfo.displayTypeFluo,'Log')
            temp = single(fluoF);
            temp2 = max(SmallNonZeroNumber,temp);
            temp3 = 10*log10(temp2);
            fluoF=maxIndex*(gainFluo+temp3)/dynFluo;
            fluoF=min(maxIndex, max(minIndex,fluoF));    
        end
        if (~isfield(handles,'fluoImage')); handles.fluoImage = -1; end        
        if handles.displayInfo.updateAxisInformation == 1 || ~ishandle(handles.fluoImage)
            if handles.displayInfo.fluoUndock == 1
                cla(handles.axFluo,'reset');
                handles.parentFigFluo = figure(32); 
                set(handles.axFluo,'Units','Pixels'); origPos = get(handles.axFluo,'Position'); set(handles.axFluo,'Units','Normalized'); %Determine position on top of docked figure. Must put units back to normalized.
                set(handles.figure1,'Units','Pixels'); GUIPos = get(handles.figure1,'Position'); %Get position of the GUI window.
                set(handles.parentFigFluo, 'Position', [origPos(1)+GUIPos(1)-30 origPos(2)+GUIPos(2)-10 handles.displayInfo.nPixels handles.displayInfo.nPixels])  
                set(handles.parentFigFluo, 'MenuBar','none','ToolBar','figure')
                drawnow
                winontop(handles.parentFigFluo);   
                set(handles.parentFigFluo,'name','Fluo');
                set(handles.parentFigFluo,'CloseRequestFcn',@dockFluo)
                handles.parentAxesFluo = subplot(1,1,1);
            else
                handles.parentAxesFluo = handles.axFluo;
            end                
            
%             handles.fluoImage = plot(handles.parentAxesFluo,linspace(0,360,handles.param.nPositionsPerTurn),fluoF,'LineWidth',2); axis tight;
            handles.fluoImage = plot(handles.parentAxesFluo,linspace(0,360,handles.param.nPositionsPerTurn),fluoF);
            axis(handles.parentAxesFluo,'normal'); axis(handles.parentAxesFluo,'xy'); axis(handles.parentAxesFluo,'tight');
            set(handles.parentAxesFluo,'NextPlot','replacechildren');  
            if strcmp(handles.displayInfo.displayTypeFluo,'Log')
                ylim(handles.parentAxesFluo,[minIndex maxIndex])
            else
                ylim(handles.parentAxesFluo,'auto')
            end            
            xlabel(handles.parentAxesFluo,'Angle (in degrees)','FontName','Arial','FontSize',12,'HandleVisibility','off')
            ylabel(handles.parentAxesFluo,'Signal (A. U.)','FontName','Arial','FontSize',12,'HandleVisibility','off')
%             ylabel(handles.parentAxesFluo,'Voltage (V)','FontName','Arial','FontSize',12,'HandleVisibility','off')
            set(handles.parentAxesFluo,'YTickMode','auto')   
            set(handles.parentAxesFluo,'XTick',0:90:360)
            set(handles.parentAxesFluo,'FontName','Arial')
            set(handles.parentAxesFluo,'FontSize',12)
                
        else
%             handles.fluoImage = plot(handles.parentAxesFluo,linspace(0,360,handles.param.nPositionsPerTurn),fluoF,'LineWidth',2); axis tight;
            handles.fluoImage = plot(handles.parentAxesFluo,linspace(0,360,handles.param.nPositionsPerTurn),fluoF);
%             ylim(handles.parentAxesFluo,[0 1])
        end   
        
    else %2D display
        if handles.displayInfo.fluoDisplayFull2D == 0    
            if strcmp(handles.displayInfo.displayTypeFluo,'Log')
                fluo2D = single(fluo2D);
                fluo2D = max(SmallNonZeroNumber,fluo2D);
                fluo2D = 10*log10(fluo2D);
                fluo2D=maxIndex*(gainFluo+fluo2D)/dynFluo;
                fluo2D=min(maxIndex, max(minIndex,fluo2D));  
            end
            if (~isfield(handles,'fluoImage')); handles.fluoImage = -1; end
            if handles.displayInfo.updateAxisInformation == 1 || ~ishandle(handles.fluoImage) 
                if handles.displayInfo.fluoUndock == 1 %First undock display
                    cla(handles.axFluo,'reset'); %Reset docked axes to save time
                    handles.parentFigFluo = figure(32); %Create figure
                    handles.parentAxesFluo = subplot(1,1,1); %Parent is either docked or undocked axes
                    set(handles.axFluo,'Units','Pixels'); origPos = get(handles.axFluo,'Position'); set(handles.axFluo,'Units','Normalized'); %Determine position on top of docked figure. Must put units back to normalized.
                    set(handles.figure1,'Units','Pixels'); GUIPos = get(handles.figure1,'Position'); %Get position of the GUI window.
                    set(handles.parentFigFluo, 'Position', [origPos(1)+GUIPos(1)-30 origPos(2)+GUIPos(2)-10 handles.displayInfo.nPixels handles.displayInfo.nPixels])  
                    set(handles.parentFigFluo, 'MenuBar','none','ToolBar','figure') %Remove menu bar
                    drawnow
                    winontop(handles.parentFigFluo); %Figure will always be on top
                    set(handles.parentFigFluo,'name','Fluo');
                    set(handles.parentFigFluo,'CloseRequestFcn',@dockFluo) %Closing the figure will be managed by a special function
                    colormap(handles.parentFigFluo,'jet'); drawnow
                else
                    handles.parentAxesFluo = handles.axFluo; %Parent is either docked or undocked axes
                end                  
                handles.fluoImage = imagesc(1e3*[-handles.displayInfo.depthToDisplay handles.displayInfo.depthToDisplay],1e3*[-handles.displayInfo.depthToDisplay handles.displayInfo.depthToDisplay],fluo2D,'Parent',handles.parentAxesFluo);
                if strcmp(handles.displayInfo.displayTypeFluo,'Log')
                    set(handles.parentAxesFluo,'CLim',[minIndex maxIndex]);
                else
                    set(handles.parentAxesFluo,'CLimMode','auto');
                end

                %Display 1mm scale bar
                depth = handles.displayInfo.depthToDisplay*1000;
                rectangle('position',[depth-1.1 depth-0.35 1 0.25], 'facecolor','w','Parent',handles.parentAxesFluo)
%                 text(depth-0.6,depth-0.6,'1mm','Parent',handles.parentAxesUS,'FontName','Arial','FontSize',12,'HorizontalAlignment','center','Color','w')  

                axis(handles.parentAxesFluo,'square'); axis(handles.parentAxesFluo,'ij'); axis(handles.parentAxesFluo,'tight');
                set(handles.parentAxesFluo,'NextPlot','replacechildren');
                if handles.displayInfo.PAUndock == 0
                    xlabel(handles.parentAxesFluo,'Position X (in millimeters)','FontName','Arial','FontSize',12,'HandleVisibility','off')
                    ylabel(handles.parentAxesFluo,'Position Y (in millimeters)','FontName','Arial','FontSize',12,'HandleVisibility','off')                  
                    set(handles.parentAxesFluo,'XTickMode','auto')
                    set(handles.parentAxesFluo,'YTickMode','auto')                       
                else
                    axis(handles.parentAxesFluo,'off'); %Remove axis
                    set(handles.parentAxesFluo, 'Units', 'normalized', 'Position', [0 0 1 1]) %Fill the figure with the axes
                end                            
            else
                set(handles.fluoImage,'CData',fluo2D);
            end              
    
        else
            if size(results.fluoFull2D,2) > 1
                fluo2D = squeeze(results.fluoFull2D(handles.displayInfo.SDPair,:,:));
            else
                fluo2D = squeeze(results.fluoFull2D(handles.displayInfo.SDPair,:,:))';
            end
            meanFluo = mean(fluo2D(:));
            %Remove drift when the laser is unstable
            for idx = 1:size(fluo2D,1)
%                 fluo2D(idx,:) = fluo2D(idx,:) - prctile(fluo2D(idx,:),5);
                sorted = sort(fluo2D(idx,:));
                sorted = sorted(:); sorted = sort(sorted);
                fifthPercentile = sorted(int32(length(sorted)*0.05));
                fluo2D(idx,:) = fluo2D(idx,:) - fifthPercentile;
            end
            %Calculation for SNR
%             region=[:];
            sorted = sort(fluo2D(:,:));
            sorted = sorted(:); sorted = sort(sorted);
            mean999 = mean(sorted(int32(length(sorted)*0.99):end));
            minFluo = mean(sorted(1:int32(length(sorted)*0.3)));
            fluo2DSignal = mean999 - minFluo;
            fluo2DSignal2 = (mean999 - minFluo) / minFluo %Pour porc
            %Uncomment to mirror fluo2D image and/or shift the image. Used to fit with ex vivo fluorescence images.
%                 fluo2D = fluo2D(:,end:-1:1);
%                 nPos=handles.param.nPositionsPerTurn;
%                 aS=100; %100 for ICG-16, 125 for ICG-14
%                 if aS > 0
%                     fluo2DTemp2 = fluo2D(:,nPos-aS+1:nPos);
%                     fluo2D(:,aS+1:end) = fluo2D(:,1:nPos-aS);
%                     fluo2D(:,1:aS) = fluo2DTemp2;
%                 elseif aS < 0
%                     aS = abs(aS);
%                     fluo2DTemp2 = fluo2D(:,1:aS);
%                     fluo2D(:,1:nPos-aS) = fluo2D(:,aS+1:nPos);
%                     fluo2D(:,nPos-aS+1:nPos) = fluo2DTemp2;
%                 end                
                
            if strcmp(handles.displayInfo.displayTypeFluo,'Log')
                fluo2D = single(fluo2D);
                fluo2D = max(SmallNonZeroNumber,fluo2D);
                fluo2D = 10*log10(fluo2D);
                fluo2D=maxIndex*(gainFluo+fluo2D)/dynFluo;
                fluo2D=min(maxIndex, max(minIndex,fluo2D));  
            end
            %Check how many complete slices there are.
            if handles.acqInfo.acqInterrupted == 0
                nSlices = handles.param.nSlices;
            else
                nSlices = floor(floor(handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn)/handles.param.nTurnsPerSlice);
            end     
%             nSlices= 20*21;
%             fluo2D  = fluo2D(1:nSlices,:);
            if (~isfield(handles,'fluoImage')); handles.fluoImage = -1; end
            if handles.displayInfo.updateAxisInformation == 1 || ~ishandle(handles.fluoImage) 
                if handles.displayInfo.fluoUndock == 1 %First undock display
                    cla(handles.axFluo,'reset'); %Reset docked axes to save time
                    handles.parentFigFluo = figure(32); %Create figure
                    handles.parentAxesFluo = subplot(1,1,1); %Parent is either docked or undocked axes
                    %Uncomment to make image full screen, with no axes.
%                         handles.parentAxesFluo = subplot('position', [0 0 1 1]);
                    set(handles.axFluo,'Units','Pixels'); origPos = get(handles.axFluo,'Position'); set(handles.axFluo,'Units','Normalized'); %Determine position on top of docked figure
                    set(handles.parentFigFluo, 'Position', [origPos(1)+30 origPos(2)+30 handles.displayInfo.nPixels handles.displayInfo.nPixels]) %Set position
                    set(handles.parentFigFluo, 'MenuBar','none','ToolBar','figure') %Remove menu bar
                    drawnow
                    winontop(handles.parentFigFluo); %Figure will always be on top
                    set(handles.parentFigFluo,'name','Fluo');
                    set(handles.parentFigFluo,'CloseRequestFcn',@dockFluo) %Closing the figure will be managed by a special function
                    colormap(handles.parentFigFluo,'hot'); drawnow
                else
                    handles.parentAxesFluo = handles.axFluo; %Parent is either docked or undocked axes
                end  
                
                distanceBetweenSlices = handles.param.distanceBetweenSlices;
                if distanceBetweenSlices == 0; distanceBetweenSlices = 1e-2; end
                handles.fluoImage = imagesc([0 360],1e3*[handles.param.distanceFirstSlice handles.param.distanceFirstSlice+1e-6+(nSlices-1)*distanceBetweenSlices],fluo2D,'Parent',handles.parentAxesFluo);  
                if strcmp(handles.displayInfo.displayTypeFluo,'Log')
                    set(handles.parentAxesFluo,'CLim',[minIndex maxIndex]);
                else
                    set(handles.parentAxesFluo,'CLimMode','auto');
                end
                %Uncomment to rescale all IVFL signals on the same scale
%                     minFluo = 8.85e6; %1.5e7 before correction, 1.75e7 after ICG16: same but -0.3e7 before
%                     maxFluo = 9.45e6; %3e7 before correction, 4e7 after ICG16: same but -0.3e7 before
                    sorted = sort(fluo2D(:));
                    noise = std(sorted(1:int32(length(sorted)*0.05)));
                    minFluo = sorted(int32(length(sorted)*0.3))
                    meanFluo;
                    range = minFluo*50;
%                     range = range * (meanFluo/6.5e6); %Same range for different catheters
                    maxFluo = minFluo + range;
%                     maxFluo = minFluo + 20*noise
                    
                    
                    set(handles.parentAxesFluo,'CLim',[minFluo maxFluo]);
                axis(handles.parentAxesFluo,'normal'); axis(handles.parentAxesFluo,'xy'); axis(handles.parentAxesFluo,'tight');
                set(handles.parentAxesFluo,'NextPlot','replacechildren');

                xlabel(handles.parentAxesFluo,'Angle (in degrees)','FontSize',12,'HandleVisibility','off')
                ylabel(handles.parentAxesFluo,'Pullback position (in millimeters)','FontSize',12,'HandleVisibility','off') 
                set(handles.parentAxesFluo,'XTickMode','auto')                
                set(handles.parentAxesFluo,'XTick',0:90:360)
                set(handles.parentAxesFluo,'YTick',round(1000*(0:0.005:(nSlices-1)*handles.param.distanceBetweenSlices)))        
                %Uncomment to hide axis
%                     axis(handles.parentAxesFluo,'off');
            else
                set(handles.fluoImage,'CData',fluo2D);
            end                 
        end
    end      
end

if handles.displayInfo.infoDisplayOn ~= 1
    if handles.displayInfo.updateAxisInformation == 1
        cla(handles.axInfo,'reset');
    end
else
    if strcmp(handles.displayInfo.infoToDisplay,'Angular synchronization')
        durationAcquisition = cell2mat(results.durationAcquisition); durationAcquisition = reshape(durationAcquisition,size(durationAcquisition,1)*size(durationAcquisition,2),1);
        durationDemod = cell2mat(results.durationDemod); durationDemod = reshape(durationDemod,size(durationDemod,1)*size(durationDemod,2),1);
        durationPosition = cell2mat(results.durationPosition); durationPosition = reshape(durationPosition,size(durationPosition,1)*size(durationPosition,2),1);

        %Display angular synchronization
        if handles.acqInfo.acqRunning == 0
            plot(handles.axInfo,linspace(0,handles.param.nTurns,length(durationAcquisition)),durationAcquisition, ...
                linspace(0,handles.param.nTurns,length(durationDemod)),durationDemod, ...
                linspace(0,handles.param.nTurns,length(durationPosition)),durationPosition); 
        else
            %If acquisition is running, we display only the acquired data
            plot(handles.axInfo,linspace(0,handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn,length(durationAcquisition)),durationAcquisition, ...
                linspace(0,handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn,length(durationDemod)),durationDemod, ...
                linspace(0,handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn,length(durationPosition)),durationPosition);
        end
        %xlim(handles.axInfo,[0 handles.param.nTurns]) %Too slow
        %Axis information are updated only if the user changes the info to display
        if get(handles.pmInfo, 'UserData') == '1'
            set(handles.pmInfo, 'UserData', '0'); %Reset
            h=title(handles.axInfo,'Angular synchronization','FontSize',12,'HandleVisibility','off'); set(h,'FontSize',12); set(h,'FontName','Arial');
            h=xlabel(handles.axInfo,'Number of turns','FontSize',12,'HandleVisibility','off'); set(h,'FontSize',12); set(h,'FontName','Arial');
            h=ylabel(handles.axInfo,'Duration (seconds)','FontSize',12,'HandleVisibility','off'); set(h,'FontSize',12); set(h,'FontName','Arial');
            h=legend(handles.axInfo,'PAUS Acquisition','Demodulation','Angular position'); set(h,'FontSize',12); set(h,'FontName','Arial');
            axis(handles.axInfo,'auto')
%             xlim(handles.axInfo,[0 handles.param.nTurns]) %Too slow
            set(handles.axInfo,'NextPlot','replacechildren');
        end
    elseif strcmp(handles.displayInfo.infoToDisplay,'Transfer synchronization')
        durationTransfer = cell2mat(results.durationTransfer); durationTransfer = reshape(durationTransfer,size(durationTransfer,1)*size(durationTransfer,2),1);
        durationSDRAMWrite = cell2mat(results.durationSDRAMWrite); durationSDRAMWrite = reshape(durationSDRAMWrite,size(durationSDRAMWrite,1)*size(durationSDRAMWrite,2),1);
        
        %Display transfer synchronization
        if handles.acqInfo.acqRunning == 0
            plot(handles.axInfo,linspace(0,handles.param.nTurns,length(durationTransfer)-1),durationTransfer(2:end), ...
                linspace(0,handles.param.nTurns,length(durationSDRAMWrite)-1),durationSDRAMWrite(2:end));
        else
            plot(handles.axInfo,linspace(0,handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn,length(durationTransfer)-1),durationTransfer(2:end), ...
                linspace(0,handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn,length(durationSDRAMWrite)-1),durationSDRAMWrite(2:end));
        end            
%         xlim(handles.axInfo,[0 handles.param.nTurns]) %Too slow
        %Axis information are updated only if the user changed the info to display
        if get(handles.pmInfo, 'UserData') == '1'
            set(handles.pmInfo, 'UserData', '0'); %Reset        
            title(handles.axInfo,'Transfer synchronization','FontSize',12,'HandleVisibility','off'); 
            xlabel(handles.axInfo,'Number of turns','FontSize',12,'HandleVisibility','off')
            ylabel(handles.axInfo,'Duration (seconds)','FontSize',12,'HandleVisibility','off')  
            legend(handles.axInfo,'Transfer','SDRAM write')
            axis(handles.axInfo,'auto')
%             xlim(handles.axInfo,[0 handles.param.nTurns]) %Too slow
            set(handles.axInfo,'NextPlot','replacechildren');
        end            
    elseif strcmp(handles.displayInfo.infoToDisplay,'Motor voltage')
        voltageMotor = cell2mat(results.voltageMotor); 
        
        %Display motor voltage
        if handles.acqInfo.acqRunning == 0
            plot(handles.axInfo,linspace(0,handles.param.nTurns,length(voltageMotor)),voltageMotor);
        else
            plot(handles.axInfo,linspace(0,handles.acqInfo.idxPositionTotal/handles.param.nPositionsPerTurn,length(voltageMotor)),voltageMotor)
        end  
%         xlim(handles.axInfo,[0 handles.param.nTurns]) %Too slow
        %Axis information are updated only if the user changed the info to display
        if get(handles.pmInfo, 'UserData') == '1'
            set(handles.pmInfo, 'UserData', '0'); %Reset        
            title(handles.axInfo,'Motor voltage','FontSize',12,'HandleVisibility','off'); 
            xlabel(handles.axInfo,'Number of turns','FontSize',12,'HandleVisibility','off')
            ylabel(handles.axInfo,'Voltage (Volts)','FontSize',12,'HandleVisibility','off')
            legend(handles.axInfo,'off')
            axis(handles.axInfo,'auto')
%             xlim(handles.axInfo,[0 handles.param.nTurns]) %Too slow
            set(handles.axInfo,'NextPlot','replacechildren');
        end            
    elseif strcmp(handles.displayInfo.infoToDisplay,'Fluorescence')
        fluoPullback = cell2mat(results.fluoPullback)'; 
        %Display fluorescence when there is no rotation
        fluoPullback = squeeze(fluoPullback(:,handles.displayInfo.SDPair));
        if handles.acqInfo.acqRunning == 0
            plot(handles.axInfo,linspace(0,handles.param.nTurns,length(fluoPullback)-1), fluoPullback(2:end));
        else
            plot(handles.axInfo,linspace(0,handles.acqInfo.idxTurnTotal,handles.acqInfo.idxTurnTotal-1), fluoPullback(2:handles.acqInfo.idxTurnTotal))
        end  
%         xlim(handles.axInfo,[0 handles.param.nTurns]) %Too slow
        %Axis information are updated only if the user changed the info to display
        if get(handles.pmInfo, 'UserData') == '1'
            set(handles.pmInfo, 'UserData', '0'); %Reset        
            title(handles.axInfo,'Fluorescence signal','FontSize',12,'HandleVisibility','off'); 
            xlabel(handles.axInfo,'Number of turns','FontSize',12,'HandleVisibility','off')
            ylabel(handles.axInfo,'Signal (A. U.)','FontSize',12,'HandleVisibility','off') 
            legend(handles.axInfo,'off')
            axis(handles.axInfo,'auto')
%             xlim(handles.axInfo,[0 handles.param.nTurns]) %Too slow
            set(handles.axInfo,'NextPlot','replacechildren');
        end            
    elseif strcmp(handles.displayInfo.infoToDisplay,'Photoacoustic Monitor')
        PAMONTemp = cell2mat(results.ADC8CH)'; 
        PAMON = squeeze(PAMONTemp(1:end,8)); %8 is input 2, 7 is input 1  

        if handles.acqInfo.acqRunning == 0
            plot(handles.axInfo,linspace(0,handles.param.nTurns,length(PAMON)),PAMON);
            axis(handles.axInfo,'auto')
        else
            %If acquisition is running, we display only the last 2 seconds (30 turns)
            range1 = size(PAMONTemp,1)-2*handles.param.speedMotorTurnsPerSec*handles.param.nPositionsPerTurn; range1(range1<1) = 1; %Display only the last 7680 samples (30 turns)
            PAMON = PAMON(range1:max(1,round((length(PAMON)-range1)/500)):end); %Display around 500 values
%             plot(handles.axInfo,linspace(max(0,handles.acqInfo.idxTurnTotal-30),handles.acqInfo.idxTurnTotal,length(PAMON)),PAMON);
            plot(handles.axInfo,PAMON);
%             xlim(handles.axInfo,[0,length(PAMON)]); ylim(handles.axInfo,[-1,100000]);  % static limits
            if ~isfield(handles,'counterAxInfoRefresh'); handles.counterAxInfoRefresh = 0; end
            handles.counterAxInfoRefresh = handles.counterAxInfoRefresh + 1;
            if handles.counterAxInfoRefresh > handles.param.speedMotorTurnsPerSec %Update limits every second
                ylim(handles.axInfo,[min(PAMON(:)),max(PAMON(:))+1]);  % static limits
                handles.counterAxInfoRefresh = 0;
            end
        end
%         axis(handles.axInfo,'tight')
%         xlim(handles.axInfo,[0 handles.param.nTurns]) %Too slow
        %Axis information are updated only if the user changed the info to display
        if get(handles.pmInfo, 'UserData') == '1'
            set(handles.pmInfo, 'UserData', '0'); %Reset        
            title(handles.axInfo,'Photoacoustic monitor','FontSize',12,'HandleVisibility','off'); 
            xlabel(handles.axInfo,'Number of turns','FontSize',12,'HandleVisibility','off')
            ylabel(handles.axInfo,'Signal (A. U.)','FontSize',12,'HandleVisibility','off')     
            legend(handles.axInfo,'off')
            axis(handles.axInfo,'tight')
            set(handles.axInfo,'NextPlot','replacechildren');
            xlim(handles.axInfo,[0,500]); ylim(handles.axInfo,[0,100000]);  % static limits         
        end 
    elseif strcmp(handles.displayInfo.infoToDisplay,'TGC Ramp')
        %Display TGC Ramp
        %Resolution is 1.5 mm in PA and 0.75 mm in US
        x = linspace(1,8.5,6);
        if handles.param.TGCUS == 0
            ramp(1) = str2num(get(handles.etPAUSInitialGain,'String'));
            ramp(2) = ramp(1)+1.5*str2num(get(handles.etTGC1,'String'));
            ramp(3) = ramp(2)+1.5*str2num(get(handles.etTGC2,'String'));
            ramp(4) = ramp(3)+1.5*str2num(get(handles.etTGC3,'String'));
            ramp(5) = ramp(4)+1.5*str2num(get(handles.etTGC4,'String'));
            ramp(6) = ramp(5);
        else
            ramp(1:6) = [str2num(get(handles.etPAUSInitialGain,'String'))];
        end
        plot(handles.axInfo,x,ramp)
        xlim(handles.axInfo,[0 8.5])
        %Axis information are updated only if the user changed the info to display
        if get(handles.pmInfo, 'UserData') == '1'
            set(handles.pmInfo, 'UserData', '0'); %Reset        
            title(handles.axInfo,'TGC Ramp','FontSize',12,'HandleVisibility','off'); 
            xlabel(handles.axInfo,'Depth (in millimeters)','FontSize',12,'HandleVisibility','off')
            ylabel(handles.axInfo,'Gain (in dB)','FontSize',12,'HandleVisibility','off')     
            legend(handles.axInfo,'off','HandleVisibility','off')
            axis(handles.axInfo,'auto')
            set(handles.axInfo,'NextPlot','replacechildren');
        end         
    end
end
if handles.displayInfo.updateAxisInformation == 1
    set(handles.figure1,'HandleVisibility','on'); %For colormap
    % set(handles.axPA,'HandleVisibility','on'); %For colormap
    % set(handles.axUS,'HandleVisibility','on'); %For colormap
    % set(handles.axFluo,'HandleVisibility','on'); %For colormap
    % set(handles.parentAxesUS,'HandleVisibility','on'); %For colormap

    colormap(handles.figure1,'gray')
    % colormap hot
    handles.displayInfo.updateAxisInformation = 0;
end

drawnow %Wait for plot and imagesc to draw 

%If a save button was pressed, we save the data from the image
if doSave > 0
    %Retreive data to save
    scanDate=handles.scanDate;
    handles.acqInfo.userComments = get(handles.etComments,'String');
    param = handles.param;
    acqInfo = handles.acqInfo;
    displayInfo = handles.displayInfo;
    results = results;
    acqInfo.outputFileName = get(handles.etFileName,'String');
    %Clear large result variables, because they are saved in the .dat files
    if isfield(results,'PA') 
        results = rmfield(results,'PA'); 
        results = rmfield(results,'US');
        results = rmfield(results,'fluo');          
    end
  
    %Check if the current turn must be saved. If not, return.
    if handles.idxTurn ~= handles.saveInfo.turnToSave
        guidata(hObject, handles); % Update handles at the end of a function 
        return
    end      

    signalType = handles.saveInfo.signalType;
    if strcmp(signalType,'PA') == 1 %Save PA
        %Check if the data is displayed correctly
        if ~isfield(handles,'parentFigPA'); handles.parentFigPA = -1; end
        if ~ishandle(handles.parentFigPA)
            if isfield(handles,'warningUndock')
                if ishandle(handles.warningUndock)
                    guidata(hObject, handles); % Update handles at the end of a function 
                    return
                end
            end            
            handles.warningUndock = warndlg('Make sure Undock is activated and an external figure is opened.','Cannot save PA image','non-modal');            
            guidata(hObject, handles); % Update handles at the end of a function 
            return
        end
        %Adapt filename
        if handles.displayInfo.PADisplaySignal==1
            imageType = 'Signal';
            outputName = [handles.acqInfo.currentPath '\Images\PA_' acqInfo.outputFileName '_' scanDate];
        else %2D display
            imageType = '2D';
            outputName = [handles.acqInfo.currentPath '\Images\PA2D_' acqInfo.outputFileName '_' scanDate];  
        end 
        if length(handles.saveInfo.turnToSave) ~= 1
            outputNameCorrected = [outputName '_' num2str(handles.idxTurn)];
        else
            outputNameCorrected = outputName;
        end
%         Save PA, PAF or PA2D in .mat (could be int16, int32 or double)
        if ~isfield(handles.saveInfo,'sliceToSave'); handles.saveInfo.sliceToSave = 1; end
        nImages = length(handles.saveInfo.turnToSave)*length(handles.saveInfo.sliceToSave);
        if ~isfield(handles,'PATemp'); handles.PATemp = double(zeros(nImages,size(PAF,1),size(PAF,2))); handles.PATempIdx = 1; end
        handles.PATemp(handles.PATempIdx,:,:) = PAF; %PA, PAF or PA2D
        if handles.PATempIdx == nImages %Last image: Save
            PA = double(handles.PATemp);
            save([outputName '.mat'],'PA','-v7.3'); %Saving data
            handles.PATempIdx = 1;
            handles = rmfield(handles,'PATemp');
        else
            handles.PATempIdx = handles.PATempIdx+1;
        end           
        %Check if image or video file exists and ask to continue (ask only once)
        if (exist([outputNameCorrected '.tiff'],'file') == 2 && strcmp(handles.saveInfo.saveType,'Image') == 1 ) || (exist([outputName '.avi'],'file') == 2 && strcmp(handles.saveInfo.saveType,'Image') == 0)
            if handles.saveInfo.overwrite == 0
                answer = questdlgNonModal('The file already exists. Do you want to continue?','File exists','Yes','No','Yes');
                if ~strcmp(answer,'Yes')
                    handles.saveInfo.overwrite = 2; %Don't continue
                    guidata(hObject, handles); % Update handles at the end of a function 
                    return    
                else
                    handles.saveInfo.overwrite = 1; %Continue
                end
            elseif handles.saveInfo.overwrite == 2
                guidata(hObject, handles); % Update handles at the end of a function 
                return                  
            end
        end
        %Adapt image dimension
        if handles.displayInfo.PADisplaySignal==1 %RF signal display
            origPos = get(handles.parentFigPA, 'Position');
            set(handles.parentFigPA, 'Position', [origPos(1) origPos(2) 450 450]) %Make sure to have 150 pixels/inch (3 inches)
            set(handles.parentFigPA, 'PaperPosition', [1 1 3 3]) 
            drawnow;
        else
            set(handles.parentFigPA, 'PaperPosition', [1 1 handles.displayInfo.nPixels/150 handles.displayInfo.nPixels/150])     
        end
        oneLineComment = reshape(handles.acqInfo.userComments',1,size(handles.acqInfo.userComments,1)*size(handles.acqInfo.userComments,2));
        [p,f,e] = fileparts(outputNameCorrected);
        shortOutputName = [f e];
        if strcmp(handles.saveInfo.saveType,'Image') == 1
            print(handles.parentFigPA,[outputNameCorrected '.tiff'],'-dtiff','-r150'); %Image of 150 pixels/inch
            %Add important information in the tilte of the tiff image
            t = Tiff([outputNameCorrected '.tiff'], 'r+');
            listPAPullback = get(handles.pmPAPullback,'String'); sliceName = char(listPAPullback(get(handles.pmPAPullback,'Value'))); 
            t.setTag('ImageDescription',['Original filename: ' shortOutputName ' Turn: ' num2str(handles.idxTurn) ' Slice: ' sliceName ' User comments: ' oneLineComment]); 
            t.close();
        else
            %If signal, frame is figure. If 2D, frame is axes.
            if handles.displayInfo.PADisplaySignal==1
                frame = handles.parentFigPA; frameSize = 450;%handles.displayInfo.nPixels;
            else %2D display
                frame = handles.parentAxesPA; frameSize = handles.displayInfo.nPixels;
            end
            if isempty(handles.saveInfo.VideoDataTemp)
                handles.saveInfo.VideoDataTemp = getframe(frame, [1 1 frameSize frameSize]);
            else
                handles.saveInfo.VideoDataTemp(end+1) = getframe(frame, [1 1 frameSize frameSize]);
            end
            %Check if it is the last turn and last slice to save
            if handles.idxTurn == handles.saveInfo.lastTurnToSave && handles.displayInfo.PAPullback == handles.saveInfo.lastSliceToSave
                videoName = ['F: ' shortOutputName ' T: ' num2str(handles.saveInfo.turnToSave) ' S: ' num2str(handles.saveInfo.sliceToSave) ' C: ' oneLineComment]; 
                if length(videoName) > 64; videoName = videoName(1:64); end;
                movie2avi(handles.saveInfo.VideoDataTemp,[outputName '.avi'], 'compression', 'none', 'fps', handles.saveInfo.videoFps,'videoname',videoName);
            end
        end
        set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), 'Content of PA image saved.')); set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))

    elseif strcmp(signalType,'US') == 1 %Save US
        %Check if the data is displayed correctly
        if ~isfield(handles,'parentFigUS'); handles.parentFigUS = -1; end
        if ~ishandle(handles.parentFigUS)
            if isfield(handles,'warningUndock')
                if ishandle(handles.warningUndock)
                    guidata(hObject, handles); % Update handles at the end of a function 
                    return
                end
            end            
            handles.warningUndock = warndlg('Make sure Undock is activated and an external figure is opened.','Cannot save US image','non-modal');            
            guidata(hObject, handles); % Update handles at the end of a function 
            return
        end
        %Adapt filename
        if handles.displayInfo.USDisplaySignal==1
            imageType = 'Signal';
            outputName = [handles.acqInfo.currentPath '\Images\US_' acqInfo.outputFileName '_' scanDate];
        else %2D display
            if handles.displayInfo.USDisplayLineTime == 0
                imageType = '2D';
                outputName = [handles.acqInfo.currentPath '\Images\US2D_' acqInfo.outputFileName '_' scanDate];  
            else
                imageType = 'LineTime';
                outputName = [handles.acqInfo.currentPath '\Images\USLineTime_' acqInfo.outputFileName '_' scanDate];                  
            end
        end 
        if length(handles.saveInfo.turnToSave) ~= 1
            outputNameCorrected = [outputName '_' num2str(handles.idxTurn)];
        else
            outputNameCorrected = outputName;
        end
%         Save US, USF or US2D in .mat
        if ~isfield(handles.saveInfo,'sliceToSave'); handles.saveInfo.sliceToSave = 1; end
        nImages = length(handles.saveInfo.turnToSave)*length(handles.saveInfo.sliceToSave);
        %Uncomment to make a 3D matrix. Helpful to save in DICOM for example.
%         if ~isfield(handles,'USTemp'); handles.USTemp = int32(zeros(nImages,size(US2D,1),size(US2D,2))); handles.USTempIdx = 1; end
%         handles.USTemp(handles.USTempIdx,:,:) = US2D; %US, USF or US2D
%         if handles.USTempIdx == nImages %Last image: Save
%             US = int16(handles.USTemp);
%             save([outputName '.mat'],'US','-v7.3'); %Saving data
%             handles.USTempIdx = 1;
%             handles = rmfield(handles,'USTemp');
%         else
%             handles.USTempIdx = handles.USTempIdx+1;
%         end        
        %Check if image or video file exists and ask to continue (ask only once)
        if (exist([outputNameCorrected '.tiff'],'file') == 2 && strcmp(handles.saveInfo.saveType,'Image') == 1 ) || (exist([outputName '.avi'],'file') == 2 && strcmp(handles.saveInfo.saveType,'Image') == 0)
            if handles.saveInfo.overwrite == 0
                answer = questdlgNonModal('The file already exists. Do you want to continue?','File exists','Yes','No','Yes');
                if ~strcmp(answer,'Yes')
                    handles.saveInfo.overwrite = 2; %Don't continue
                    guidata(hObject, handles); % Update handles at the end of a function 
                    return    
                else
                    handles.saveInfo.overwrite = 1; %Continue
                end
            elseif handles.saveInfo.overwrite == 2
                guidata(hObject, handles); % Update handles at the end of a function 
                return                  
            end
        end
        %Adapt image dimension
        if handles.displayInfo.USDisplaySignal==1 %RF signal display
            origPos = get(handles.parentFigUS, 'Position');
            set(handles.parentFigUS, 'Position', [origPos(1) origPos(2) 400 400]) %Make sure to have 150 pixels/inch (3 inches)
            drawnow;
            set(handles.parentFigUS, 'PaperPosition', [1 1 handles.displayInfo.nPixels/150 handles.displayInfo.nPixels/150])       
            drawnow;
        else
            set(handles.parentFigUS, 'PaperPosition', [1 1 handles.displayInfo.nPixels/150 handles.displayInfo.nPixels/150])     
        end
        oneLineComment = reshape(handles.acqInfo.userComments',1,size(handles.acqInfo.userComments,1)*size(handles.acqInfo.userComments,2));
        [p,f,e] = fileparts(outputNameCorrected);
        shortOutputName = [f e];        
        if strcmp(handles.saveInfo.saveType,'Image') == 1
            print(handles.parentFigUS,[outputNameCorrected '.tiff'],'-dtiff','-r150'); %Image of 150 pixels/inch
            %Add important information in the tilte of the tiff image
            t = Tiff([outputNameCorrected '.tiff'], 'r+');
            listUSPullback = get(handles.pmUSPullback,'String'); sliceName = char(listUSPullback(get(handles.pmUSPullback,'Value'))); 
            t.setTag('ImageDescription',['Original filename: ' outputNameCorrected ' Turn: ' num2str(handles.idxTurn) ' Slice: ' sliceName ' User comments: ' oneLineComment]); 
            t.close();
        else
            %If signal, frame is figure. If 2D, frame is axes.
            if handles.displayInfo.USDisplaySignal==1
                frame = handles.parentFigUS; frameSize = 450;%handles.displayInfo.nPixels;
            else %2D display
                frame = handles.parentAxesUS; frameSize = handles.displayInfo.nPixels;
            end
            if isempty(handles.saveInfo.VideoDataTemp)
                handles.saveInfo.VideoDataTemp = getframe(frame, [1 1 frameSize frameSize]);
            else
                handles.saveInfo.VideoDataTemp(end+1) = getframe(frame, [1 1 frameSize frameSize]);
            end
            %Check if it is the last turn and last slice to save
            if handles.idxTurn == handles.saveInfo.lastTurnToSave && handles.displayInfo.USPullback == handles.saveInfo.lastSliceToSave
                videoName = ['F: ' shortOutputName ' T: ' num2str(handles.saveInfo.turnToSave(1)) ' S: ' num2str(handles.saveInfo.sliceToSave(1)) ' C: ' oneLineComment];
                if length(videoName) >= 64; videoName = videoName(1:63); end;
                movie2avi(handles.saveInfo.VideoDataTemp,[outputName '.avi'], 'compression', 'none', 'fps', handles.saveInfo.videoFps,'videoname', videoName);
            end
        end
        set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), 'Content of US image saved.')); set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))

    elseif strcmp(signalType,'Fluo') == 1 %Save Fluo
        %Check if the data is displayed correctly
        if ~isfield(handles,'parentFigFluo'); handles.parentFigFluo = -1; end
        if ~ishandle(handles.parentFigFluo)
            if isfield(handles,'warningUndock')
                if ishandle(handles.warningUndock)
                    guidata(hObject, handles); % Update handles at the end of a function 
                    return
                end
            end
            handles.warningUndock = warndlg('Make sure Undock is activated and an external figure is opened.','Cannot save Fluo image','non-modal');            
            guidata(hObject, handles); % Update handles at the end of a function 
            return
        end     
        %Adapt filename
        if handles.displayInfo.fluoDisplaySignal==1
            imageType = 'Signal';
            outputName = [handles.acqInfo.currentPath '\Images\Fluo_' acqInfo.outputFileName '_' scanDate];
        else %2D display
            if handles.displayInfo.fluoDisplayFull2D == 0
                imageType = '2D';
                outputName = [handles.acqInfo.currentPath '\Images\Fluo2D_' acqInfo.outputFileName '_' scanDate];
            else
                imageType = '2DFull';
                outputName = [handles.acqInfo.currentPath '\Images\FluoFull2D_' acqInfo.outputFileName '_' scanDate];
            end
        end   
        if length(handles.saveInfo.turnToSave) ~= 1
            outputNameCorrected = [outputName '_' num2str(handles.idxTurn)];
        else
            outputNameCorrected = outputName;
        end   
%         Save fluo, fluoF or fluo2D in .mat
        if ~isfield(handles.saveInfo,'sliceToSave'); handles.saveInfo.sliceToSave = 1; end
        nImages = length(handles.saveInfo.turnToSave)*length(handles.saveInfo.sliceToSave);
        if ~isfield(handles,'fluoTemp'); handles.fluoTemp = double(zeros(nImages,size(fluoF,1),size(fluoF,2))); handles.fluoTempIdx = 1; end
        handles.fluoTemp(handles.fluoTempIdx,:,:) = fluoF; %fluo, fluoF or fluo2D
        if handles.fluoTempIdx == nImages %Last image: Save
            fluo = double(handles.fluoTemp);
            save([outputName '.mat'],'fluo','-v7.3'); %Saving data
            handles.fluoTempIdx = 1;
            handles = rmfield(handles,'fluoTemp');
        else
            handles.fluoTempIdx = handles.fluoTempIdx+1;
        end          
        %Check if image or video file exists and ask to continue (ask only once)
        if (exist([outputNameCorrected '.tiff'],'file') == 2 && strcmp(handles.saveInfo.saveType,'Image') == 1 ) || (exist([outputName '.avi'],'file') == 2 && strcmp(handles.saveInfo.saveType,'Image') == 0)
            if handles.saveInfo.overwrite == 0
                answer = questdlgNonModal('The file already exists. Do you want to continue?','File exists','Yes','No','Yes');
                if ~strcmp(answer,'Yes')
                    handles.saveInfo.overwrite = 2; %Don't continue
                    guidata(hObject, handles); % Update handles at the end of a function 
                    return    
                else
                    handles.saveInfo.overwrite = 1; %Continue
                end
            elseif handles.saveInfo.overwrite == 2
                guidata(hObject, handles); % Update handles at the end of a function 
                return                  
            end
        end
        %Adapt image dimension
        if handles.displayInfo.fluoDisplaySignal==1 || handles.displayInfo.fluoDisplayFull2D == 1 %RF signal display
            origPos = get(handles.parentFigFluo, 'Position');
            %TODO: Something is not working with and without these lines
            set(handles.parentFigFluo, 'Position', [origPos(1) origPos(2) 500 500]) %Make sure to have 150 pixels/inch (3 inches)
            set(handles.parentFigFluo, 'PaperPosition', [1 1 3 3])   
            drawnow;
        else
            set(handles.parentFigFluo, 'PaperPosition', [1 1 handles.displayInfo.nPixels/150 handles.displayInfo.nPixels/150])     
        end  
        oneLineComment = reshape(handles.acqInfo.userComments',1,size(handles.acqInfo.userComments,1)*size(handles.acqInfo.userComments,2));
        [p,f,e] = fileparts(outputNameCorrected);
        shortOutputName = [f e];          
        if strcmp(handles.saveInfo.saveType,'Image') == 1
            print(handles.parentFigFluo,[outputNameCorrected '.tiff'],'-dtiff','-r150'); %Image of 150 pixels/inch
            %Add important information in the tilte of the tiff image
            t = Tiff([outputNameCorrected '.tiff'], 'r+');
            listFluoPullback = get(handles.pmFluoPullback,'String'); sliceName = char(listFluoPullback(get(handles.pmFluoPullback,'Value'))); 
            t.setTag('ImageDescription',['Original filename: ' shortOutputName ' Turn: ' num2str(handles.idxTurn) ' Slice: ' sliceName ' User comments: ' oneLineComment]); 
            t.close();
        else
            %If signal, frame is figure. If 2D, frame is axes.
            if handles.displayInfo.fluoDisplaySignal==1
                frame = handles.parentFigFluo; frameSize = 450;%handles.displayInfo.nPixels;
            else %2D display
                frame = handles.parentAxesFluo; frameSize = handles.displayInfo.nPixels;
            end
            if isempty(handles.saveInfo.VideoDataTemp)
                handles.saveInfo.VideoDataTemp = getframe(frame, [1 1 frameSize frameSize]);
            else
                handles.saveInfo.VideoDataTemp(end+1) = getframe(frame, [1 1 frameSize frameSize]);
            end
            %Check if it is the last turn and last slice to save
            if handles.idxTurn == handles.saveInfo.lastTurnToSave && handles.displayInfo.fluoPullback == handles.saveInfo.lastSliceToSave
                videoName = ['F: ' shortOutputName ' T: ' num2str(handles.saveInfo.turnToSave) ' S: ' num2str(handles.saveInfo.sliceToSave) ' C: ' oneLineComment];
                if length(videoName) > 64; videoName = videoName(1:64); end;
                movie2avi(handles.saveInfo.VideoDataTemp,[outputName '.avi'], 'compression', 'none', 'fps', handles.saveInfo.videoFps, 'videoname', videoName);
            end
        end
        set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), 'Content of Fluo image saved.')); set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))
    end
    
    %Copy corresponding data
    if strcmp(handles.saveInfo.saveCorrespondingData,'Yes')
        %Check if the data already exists, will only copy once
        if ~(exist([outputName '_PA.dat'],'file') == 2) 
            save([outputName '.mat'],'imageType','signalType','scanDate','param','results','acqInfo','displayInfo','-v7.3'); %Saving data
            %     tic
            %     copyfile([handles.acqInfo.outputFullFileName '_PA.dat'], [outputName '_PAM.dat']);
            %     copyfile([handles.acqInfo.outputFullFileName '_US.dat'], [outputName '_USM.dat']);
            %     copyfile([handles.acqInfo.outputFullFileName '_Fluo.dat'], [outputName '_FluoM.dat']);
            %     toc    
            %     tic
            dos(['copy "' handles.acqInfo.outputFullFileName '_PA.dat" "' [outputName '_PA.dat"']]);
            dos(['copy "' handles.acqInfo.outputFullFileName '_US.dat" "' [outputName '_US.dat"']]);
            dos(['copy "' handles.acqInfo.outputFullFileName '_Fluo.dat" "' [outputName '_Fluo.dat"']]);
            set(handles.lbInfo, 'String', strvcat(get(handles.lbInfo, 'String'), 'Corresponding data copied.')) 
            set(handles.lbInfo,'Value',size(get(handles.lbInfo, 'String'),1))    
            %     toc  
        end
    end
end
guidata(hObject, handles); % Update handles at the end of a function 